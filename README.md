Dairy Health Manager (DHM): Simulation-based evaluation of strategies for controlling health and reproductive problems in dairy cattle farming
======================

![Version](https://img.shields.io/badge/version-1.2.2-blue) ![License](https://img.shields.io/badge/license-GNU%20LGPL-blue)

# Part A - General information

Health is a  prerequisite for the economic profitability of a dairy farm. Predicting the return on investment in health management strategies is a complex task for farmers and their advisers. Simulation is the most appropriate tool for assessing the a priori benefits of a technological innovation envisaged by a project leader, whether public or private.

A pure UMR BIOEPAR product, the Dairy Health Manager (DHM) simulator is a high-performance, scalable virtual laboratory that accurately represents the herd in all its diversity and economic context. The software can be customised to provide health and economic results based on the health strategies envisaged for a given farm.

**Simulation feature**: dynamics with a time-dependent, discrete-time mechanistic, stochastic and finite-horizon engine.
**Model**: intra-herd centred individual (digital twin).
**Modules**: Reproduction, production, health, genetics, population management, feeding and accounting report. 
**Settings**: The simulator is able to use various zootechnical and health management levers to stochastically determine the effects on dairy farm results. DHM offers a total of 354 different levers spread across the 7 functional modules for fine-tuning the configuration of a farm for simulation purposes. Product and cost values can also be configured to take account of market conditions.

**Contributors and contact:**
- Philippe Gontier (`philippe.gontier@oniris-nantes.fr`)
- Nathalie Bareille (`nathalie.bareille@oniris-nantes.fr`)

**How to cite:**
  Gontier P., Bareille N. Dairy Health Manager : Description fonctionnelle et modalités d'emploi. 2023, pp.124. `DOI: 10.17180/NPMC-KH96`
  
  BibTeX:
@techreport{gontier:hal-03922685,
  TITLE = {{Dairy Health Manager : Description fonctionnelle et modalit{\'e}s d'emploi}},
  AUTHOR = {Gontier, Philippe and Bareille, Nathalie},
  URL = {https://hal.inrae.fr/hal-03922685},
  PAGES = {124},
  INSTITUTION = {{Inrae ; Oniris, Ecole Nationale V{\'e}t{\'e}rinaire, Agroalimentaire et de l'Alimentation Nantes Atlantique}},
  YEAR = {2023},
  MONTH = Jan,
  DOI = {10.17180/NPMC-KH96},
  KEYWORDS = {Simulator ; Husbandry ; Dairy cattle ; ECOMAST ; Health ; Production disease ; DHM ; Reproduction ; Genetics ; Performance ; Economic impact ; Simulateur ; Bovin laitier ; ECOMAST ; Sant{\'e} ; Maladie de production ; DHM ; Reproduction ; G{\'e}n{\'e}tique ; Performances ; Impact {\'e}conomique ; Elevage},
  PDF = {https://hal.inrae.fr/hal-03922685v4/file/DescriptionFonctionnelleEtModalitesDEmploi.pdf},
  HAL_ID = {hal-03922685},
  HAL_VERSION = {v4},
}
# Part B - Getting Started
Please refer to	[DHM french on-line documentation](./documentation/DescriptionFonctionnelleEtModalitesDEmploi.pdf) or [DHM english on-line documentation](./documentation/FunctionalDescriptionAndTermsOfUse.pdf)


# Part C - Content of the repository

  | File name           | Role                                                          |
  |---------------------|---------------------------------------------------------------|
  | `README.md`         | This file                                                     |
  | `LICENSE`           | The license                                                   |
  | `data/`             | Location for technical and functional example data            |
  | `documentation/`    | Location french and english documentation                     |
  | `src/`              | Location of all DHM source files                              |


-----
Last update: 2024-01-16, P. Gontier (`philippe.gontier@oniris-nantes.fr`)_
