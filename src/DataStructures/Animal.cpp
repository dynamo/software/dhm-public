#include "Animal.h"

// boost

// project
#include "../Genetic/Breed/DairyBreed.h"
#include "./Population/Batch/Batch.h"
#include "DairyFarm.h"
#include "../Genetic/GeneticManagement.h"
#include "../Genetic/BreedInheritance.h"
#include "../ExchangeInfoStructures/TechnicalConstants.h"
#include "../ResultStructures/DairyFarmResult.h"
#include "../Reproduction/DairyHerdReproductionStates/DairyHerdReproductionState.h"
#include "Herd.h"
#include "Foots.h"

BOOST_CLASS_EXPORT(DataStructures::Animal) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(_dataLog); \
    ar & BOOST_SERIALIZATION_NVP(_uniqueId); \
    ar & BOOST_SERIALIZATION_NVP(_strBirthdate); \
    ar & BOOST_SERIALIZATION_NVP(_age); \
    ar & BOOST_SERIALIZATION_NVP(_sex); \
    ar & BOOST_SERIALIZATION_NVP(_pIsInHerd); \
    ar & BOOST_SERIALIZATION_NVP(_bornInHerd); \
    ar & BOOST_SERIALIZATION_NVP(_pIsCurrentlyInBatch); \
    ar & BOOST_SERIALIZATION_NVP(_vTemporalEvents); \
    ar & BOOST_SERIALIZATION_NVP(_pHasBreedInheritance); \
    ar & BOOST_SERIALIZATION_NVP(_birth); \
    ar & BOOST_SERIALIZATION_NVP(_pHasFoots); \
    ar & BOOST_SERIALIZATION_NVP(_dead); \
    ar & BOOST_SERIALIZATION_NVP(_cullingStatus); \
    ar & BOOST_SERIALIZATION_NVP(_cullingDecisionDate); \

    IMPLEMENT_SERIALIZE_STD_METHODS(Animal);

    // Generated or bought animal
    Animal::Animal(const boost::gregorian::date &simDate, const boost::gregorian::date &birth, Genetic::Breed::Breed* pBreed, Herd* pHerd, FunctionalEnumerations::Genetic::Sex sex)
    {
        commonInit(birth, sex, simDate, pHerd
#ifdef _LOG
                  , false
#endif // _LOG
                  ); // general init
         
        // Genetic
        _pHasBreedInheritance = new Genetic::BreedInheritance(pBreed); // 100% for the selected breed : pure breed
        
    } /* End of constructor */
    
     // Constructor based with the dam
    Animal::Animal(const boost::gregorian::date &birth, Animal* pDam, Genetic::Breed::Breed* pSireBreed, FunctionalEnumerations::Genetic::Sex sex)
    {
        commonInit(birth, sex, birth, pDam->getHerd()
#ifdef _LOG
                  , true
#endif // _LOG
                  ); // general init
        
        _pHasBreedInheritance = new Genetic::BreedInheritance(); // empty genetic inheritance
        Genetic::GeneticManagement::setFarrowedBreeds(_pHasBreedInheritance, pDam->getBreeds(), pSireBreed); // Set the empty genetic inheritance
    }
    
    Animal::~Animal()
    {
        delete _pHasBreedInheritance;
        _pHasBreedInheritance = nullptr; 
        
        delete _pHasFoots;
        _pHasFoots = nullptr;
    }

    void Animal::commonInit(const boost::gregorian::date &birth, FunctionalEnumerations::Genetic::Sex sex, const boost::gregorian::date &date, Herd* pHerd
#ifdef _LOG
                        , bool bornInHerd
#endif // _LOG
                        )
    {
        _uniqueId = pHerd->getNewIdUnique();
        _birth = birth;
        _sex = sex;
        setAge(date);       
        _pHasFoots = new Foots(this, birth); // Creating foots
        _pIsInHerd = pHerd;

        
#ifdef _LOG
        _bornInHerd = bornInHerd;
#endif // _LOG
    }
    
    unsigned int Animal::getUniqueId()
    {
        return _uniqueId;
    }

    std::map<Genetic::Breed::Breed*, float> &Animal::getBreeds()
    {
        return _pHasBreedInheritance->getBreeds();
    } // End of method getBreeds
    
    void Animal::setAge(const boost::gregorian::date &simDate)
    {
        boost::gregorian::date_period period(_birth, simDate);
        _age = period.length().days(); 
    }
    
    unsigned int Animal::getAge()
    {
        return _age;
    } // End of method getAge
    
    FunctionalEnumerations::Genetic::Sex Animal::getSex()
    {
        return _sex;
    }

    std::string Animal::getStrBirthDate()
    {
        _strBirthdate = boost::gregorian::to_iso_extended_string(_birth);
        return _strBirthdate;
    }
    const boost::gregorian::date &Animal::getBirthDate()
    {
        return _birth;
    }
         
    void Animal::progress(const boost::gregorian::date &simDate)
    {
        // Age
        setAge(simDate);
        
        // Foots
        _pHasFoots->progress(simDate); 
        
        updateTemporalEventList(simDate);
        
        
#ifdef _LOG
        getDataLog().batch = getBatch()->getType();
        getDataLog().location = getBatch()->getCurrentLocation();
#endif // _LOG   
         
        // implement here all temporal result events
        // -----------------------------------------
               
    }
    
#ifdef _LOG    
    void Animal::closeResults(const boost::gregorian::date &finalDate)
    {
        getCurrentResults()->getFinalAnimalLogData()[getUniqueId()] = _dataLog;
    }
    
    bool Animal::isBornInHerd()
    {
        return _bornInHerd;
    }
   
    void Animal::addMiscellaneousLog(const boost::gregorian::date &simDate, std::string log)
    {
       getDataLog().miscellaneous += log + " - ";
    }
    
    Results::DayAnimalLogInformations &Animal::getDataLog()
    {
        std::map<unsigned int, Results::DayAnimalLogInformations>::iterator it = _dataLog.find(getAge());
        if (it == _dataLog.end())
        {
            _dataLog.insert( std::pair<unsigned int, Results::DayAnimalLogInformations>(getAge(),Results::DayAnimalLogInformations()));
            it = _dataLog.find(getAge());
        }
        return it->second;
    }
#endif // _LOG        
    
    void Animal::goInBatch(Population::Batch* pBatch)
    {
        _pIsCurrentlyInBatch = pBatch;      
    }
    
    void Animal::exitFromBatch()
    {
        _pIsCurrentlyInBatch = nullptr;
    }
    
    Herd* Animal::getHerd()
    {
        return _pIsInHerd;
    }
    
    Population::Batch* Animal::getBatch()
    {
        return _pIsCurrentlyInBatch;
    }
    
    Results::DairyFarmResult* Animal::getCurrentResults()
    {
        return getDairyFarm()->getCurrentResults();
    }

    DairyFarm* Animal::getDairyFarm()
    {
        return getHerd()->getDairyFarm();
    }

    unsigned int Animal::getLactationRank()
    {
        return 0;
    }

    void Animal::addTemporalEvent(ExchangeInfoStructures::TemporalEvent theTemporalEvent)
    {
        _vTemporalEvents.push_back(theTemporalEvent);
    }
    
    // Is the given date corresponding to a planned temporal event for a given event type ?
    bool Animal::isItTheDayForPlannedTemporalEvent(ExchangeInfoStructures::TemporalEvent::TemporalEventType theTypeEvent, const boost::gregorian::date &theDate)
    {
        bool res = false;
        for (std::vector<ExchangeInfoStructures::TemporalEvent>::iterator it = _vTemporalEvents.begin(); it != _vTemporalEvents.end() and !res; it++)
        {
            ExchangeInfoStructures::TemporalEvent &evt = *it;        
            res = evt.type == theTypeEvent and evt.eventDate == theDate;
        }
        return res;
    }
    
    // Is the given date in a planned period for a given event type ?
    bool Animal::hasTemporalEventPeriodPlanned(ExchangeInfoStructures::TemporalEvent::TemporalEventType theTypeEvent, const boost::gregorian::date &theDate)
    {
        bool res = false;
        for (std::vector<ExchangeInfoStructures::TemporalEvent>::iterator it = _vTemporalEvents.begin(); it != _vTemporalEvents.end() and !res; it++)
        {
            ExchangeInfoStructures::TemporalEvent &evt = *it;        
            res = evt.type == theTypeEvent and theDate >= evt.beginEventPeriod and theDate <= evt.endEventPeriod;
        }
        return res;
    }
    
    void Animal::removeTemporalEventType(ExchangeInfoStructures::TemporalEvent::TemporalEventType theType)
    {
        for (std::vector<ExchangeInfoStructures::TemporalEvent>::iterator it = _vTemporalEvents.begin(); it != _vTemporalEvents.end();)
        {
            ExchangeInfoStructures::TemporalEvent &evt = *it;        
            if (evt.type == theType)
            {
                it = _vTemporalEvents.erase(it);
            }
            else
            {
               it++; 
            }
        }
    }
    
    void Animal::updateTemporalEventList(const boost::gregorian::date &currentDate)
    {
        for (std::vector<ExchangeInfoStructures::TemporalEvent>::iterator it = _vTemporalEvents.begin(); it != _vTemporalEvents.end();)
        {
            ExchangeInfoStructures::TemporalEvent &evt = *it;        
            if (evt.isOutOfTime(currentDate))
            {
                it = _vTemporalEvents.erase(it);
            }
            else
            {
               it++; 
            }
        }
    }
       
    Genetic::BreedInheritance*  Animal::getBreedInheritance()
    {
        return _pHasBreedInheritance;
    }
    
    void Animal::dies(
#ifdef _LOG
                      const boost::gregorian::date &date, 
#endif // _LOG            
                     FunctionalEnumerations::Population::DeathReason deathReason)
    {
        // Sometime, two quarters kills the animal the same day, so we have to test...
        if (not _dead)
        {
            _dead = true;
            getDairyFarm()->appendAnimalToDeathOfTheDay(this, deathReason);
        }
#ifdef _LOG
        // for Death results
        Results::DeathResult dr;
        dr.id = getUniqueId();
        dr.date = date;
        dr.age = getAge();
        dr.sex = getSex();
        dr.bornInHerd = isBornInHerd();
        dr.reason = deathReason;
        getCurrentResults()->getPopulationResults().DeathResults.push_back(dr);      
#endif // _LOG            
    }
    
    FunctionalEnumerations::Population::CullingStatus Animal::getCullingStatus()
    {
        return _cullingStatus;      
    }
    
    boost::gregorian::date &Animal::getCullingDecisionDate()
    {
        return _cullingDecisionDate;      
    }
       
    void Animal::cullingIsDecided (FunctionalEnumerations::Population::CullingStatus newCullingStatus, bool today, const boost::gregorian::date &date)
    {
#ifdef _LOG
        std::string text;
        if (today)
        {
           text = "today culling is decided for " + Tools::toString(newCullingStatus);
        }
        else
        {
           text = "delayed culling is decided for " + Tools::toString(newCullingStatus);            
        }
        addMiscellaneousLog(date, text);
#endif // _LOG
        _cullingDecisionDate = date;
        _cullingStatus = newCullingStatus;
        removeTemporalEventType(ExchangeInfoStructures::TemporalEvent::IAMM); // Remove the potential IAMM
        
        if (today)
        {
            // Culling today
            getHerd()->addAnimalToBeCulledToday(this);
        }
        else
        {
            // Culling later
            getHerd()->addAnimalToBeCulledLater(this);
        }
    }

} /* End of namespace DataStructures */