#ifndef DataStructures_Animal_h
#define DataStructures_Animal_h

// Standard
#include <string>
#include <vector>

// Boost
#include <boost/date_time/gregorian/gregorian.hpp>

// Project
#include "../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "../ExchangeInfoStructures/TemporalEvent.h"
#include "../Genetic/Breed/Breed.h"
#include "../ResultStructures/DairyFarmResult.h"


namespace Genetic
{
    class BreedInheritance;
    class Caracteristics;
}

namespace ExchangeInfoStructures
{
    struct MilkProductCharacteristic;
}
namespace Results
{
    namespace DairyFarmResults
    {
        class DairyFarmResult;
    }
}

namespace DataStructures
{
    class DairyFarm;
    class Herd;
    class Foots;
    
    namespace Population
    {
        class Batch;
    }
        
    /// Basis bred element in Herd
    class Animal
    {        
        
    private :
        // log
#ifdef _LOG
        std::map<unsigned int, Results::DayAnimalLogInformations> _dataLog;
#else // _LOG 
        std::map<unsigned int, unsigned int> _dataLog;
#endif // _LOG 

        // Attributes
        unsigned int _uniqueId;
        std::string _strBirthdate; // For external communication (I_Animal)
        unsigned int _age; // Age of the animal, witch can increase by run
        FunctionalEnumerations::Genetic::Sex _sex;
        
        Herd* _pIsInHerd = nullptr;
        
        bool _bornInHerd = true; // Log mode, but always serialized...
        

        Population::Batch* _pIsCurrentlyInBatch = nullptr; // Along the simulation 

        Genetic::BreedInheritance*  _pHasBreedInheritance;
        boost::gregorian::date _birth; // Birth date of the animal,
        
         // Temporal events (same elements in lists, by not identically grouped)
        std::vector<ExchangeInfoStructures::TemporalEvent> _vTemporalEvents;

    protected:
        Foots* _pHasFoots = nullptr;
        bool _dead = false;
        boost::gregorian::date _cullingDecisionDate;
        FunctionalEnumerations::Population::CullingStatus _cullingStatus = FunctionalEnumerations::Population::CullingStatus::notToCull;

    private:

        DECLARE_SERIALIZE_STD_METHODS;
        
        void commonInit(const boost::gregorian::date &birth, FunctionalEnumerations::Genetic::Sex sex, const boost::gregorian::date &date, Herd* pHerd
#ifdef _LOG
                        , bool bornInHerd
#endif // _LOG
                        );
        void setAge(const boost::gregorian::date &simDate);

    protected:
        virtual void concrete() = 0; // To ensure that class will not be instantiated


        Animal(){} // For serialization
        
        Animal(const boost::gregorian::date &simDate, const boost::gregorian::date &birth, Genetic::Breed::Breed* pBreed, Herd* pHerd, FunctionalEnumerations::Genetic::Sex sex); // Animal Generation constructor
        Animal(const boost::gregorian::date &birth, Animal* pDam, Genetic::Breed::Breed* pSireBreed, FunctionalEnumerations::Genetic::Sex sex); // Animal farrow constructor

        bool isItTheDayForPlannedTemporalEvent(ExchangeInfoStructures::TemporalEvent::TemporalEventType theTypeEvent, const boost::gregorian::date &theDate); // Is the given date corresponding to a planned temporal event for a given event type ?
        bool hasTemporalEventPeriodPlanned(ExchangeInfoStructures::TemporalEvent::TemporalEventType theTypeEvent, const boost::gregorian::date &theDate); // Is the given date in a planned period for a given event type ?
        void updateTemporalEventList(const boost::gregorian::date &currentDate);

    public :
        DairyFarm* getDairyFarm();
        Results::DairyFarmResult* getCurrentResults();

        virtual ~Animal();
        void addTemporalEvent(ExchangeInfoStructures::TemporalEvent theTemporalEvent);
        void removeTemporalEventType(ExchangeInfoStructures::TemporalEvent::TemporalEventType theType);
        unsigned int getUniqueId(); // Get the unique id of the animal
        std::map<Genetic::Breed::Breed*, float> &getBreeds();

        Genetic::BreedInheritance*  getBreedInheritance();
        unsigned int getAge();
        FunctionalEnumerations::Genetic::Sex getSex();
        virtual inline bool isMilking(){return false;}
        virtual inline bool getMilkControlInformations(bool useCetodetect, bool &controled, int &lactationStage, unsigned int &lactationRank, bool &SCCLessThanRefInPreviousMilkControl, bool &SCCLessThanRefNow, const boost::gregorian::date &simDate, ExchangeInfoStructures::MilkProductCharacteristic &cumulativeIndividualMilkSamples
                                                                                                                                        , bool &isDetectedInfectiousLameness
                                                                                                                                        , bool &isInfectiousG1Lameness
                                                                                                                                        , bool &isInfectiousG2Lameness
                                                                                                                                        , bool &isDetectedNonInfectiousLameness
                                                                                                                                        , bool &isNonInfectiousG1Lameness
                                                                                                                                        , bool &isNonInfectiousG2Lameness
#ifdef _LOG
                                                                                                                                        , FunctionalEnumerations::Health::KetosisSeverity &ketosisSeverity
                                                                                                                                        , FunctionalEnumerations::Global::Month &lastCalvingMonth    
                                                                                                                                        , bool &newKetosisCase
#endif // _LOG
                                                                                    ){return false;}
         std::string getStrBirthDate();
        const boost::gregorian::date &getBirthDate();
#ifdef _LOG
        bool isBornInHerd();
        void closeResults(const boost::gregorian::date &finalDate);
        void addMiscellaneousLog(const boost::gregorian::date &simDate, std::string);
        Results::DayAnimalLogInformations &getDataLog();
#endif // _LOG 

        virtual void progress(const boost::gregorian::date &simDate); // progress of duration time and unit

        void goInBatch(Population::Batch* pBatch);
        void exitFromBatch();
        Herd* getHerd();
        Population::Batch* getBatch();       
        virtual FunctionalEnumerations::Population::SaleStatus getSaleStatus() = 0;

        virtual unsigned int getLactationRank();
        virtual float getExitAmount() = 0;
        
        void dies(
#ifdef _LOG                    
                  const boost::gregorian::date &date,
#endif // _LOG            
                  FunctionalEnumerations::Population::DeathReason deathReason);
        FunctionalEnumerations::Population::CullingStatus getCullingStatus();
        boost::gregorian::date &getCullingDecisionDate();
        virtual void cullingIsDecided (FunctionalEnumerations::Population::CullingStatus newCullingStatus, bool today, const boost::gregorian::date &date); 
        virtual float getTodayCullingScore() = 0;
        
    };/* End of class Animal */

} /* End of namespace DataStructures */
#endif // DataStructures_Animal_h
