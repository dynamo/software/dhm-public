#include "ArtificialInsemination.h"

// stantard
#include <vector>

// project
#include "../Genetic/BreedInheritance.h"

BOOST_CLASS_EXPORT(DataStructures::ArtificialInsemination) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Insemination); \

    IMPLEMENT_SERIALIZE_STD_METHODS(ArtificialInsemination);
    
    ArtificialInsemination::ArtificialInsemination( Genetic::Breed::Breed* pSireBreed,
                                                    Genetic::GeneticValue &sireGeneticValue,
                                                    FunctionalEnumerations::Reproduction::TypeInseminationSemen typeSemen,
                                                    const boost::gregorian::date& theDate,
                                                    bool success,
                                                    unsigned int number,
                                                    Results::Result* pRes,
                                                    ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &am,
                                                    DataStructures::DairyMammal* pCow,
                                                    bool manageCost) : Insemination(pSireBreed, sireGeneticValue, typeSemen, theDate, success, number, pRes, am, pCow, manageCost)
    {
    }
} /* End of namespace DataStructures */