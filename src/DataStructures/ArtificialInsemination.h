#ifndef DataStructures_ArtificialInsemination_h
#define DataStructures_ArtificialInsemination_h

// boost

// standard
#include <string>

// project
#include "Insemination.h"

namespace Results
{
    class Result;
}

namespace DataStructures
{
    /// Insemination made manually with bull semen
    class ArtificialInsemination : public Insemination
    {
        DECLARE_SERIALIZE_STD_METHODS;
        
    protected:
        ArtificialInsemination(){}; // For serialization
        void concrete(){}; // To allow instanciation
        
    public:
        ArtificialInsemination( Genetic::Breed::Breed* pSireBreed,
                                Genetic::GeneticValue &sireGeneticValue,
                                FunctionalEnumerations::Reproduction::TypeInseminationSemen typeSemen,
                                const boost::gregorian::date& theDate,
                                bool success,
                                unsigned int number,
                                Results::Result* pRes,
                                ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &am,
                                DataStructures::DairyMammal* pCow,
                                bool manageCost = true);
        virtual ~ArtificialInsemination(){};
        inline bool isArtificial() override {return true;}

        std::map<FunctionalEnumerations::Genetic::Breed, float> &getBreeds();
    };
} /* End of namespace DataStructures */
#endif // DataStructures_ArtificialInsemination_h
