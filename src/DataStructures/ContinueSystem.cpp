////////////////////////////////////////
//                                    //
// File : ContinueSystem.cpp          //
//                                    //
// Author : Philippe Gontier          //
//                                    //
// Version 1.0                        //
//                                    //
// Date : 10/03/2022                  //
//                                    //
////////////////////////////////////////

#include "ContinueSystem.h"

BOOST_CLASS_EXPORT(DataStructures::ContinueSystem)

namespace DataStructures
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(SystemToSimulate); \

    IMPLEMENT_SERIALIZE_STD_METHODS(ContinueSystem);
        
    ContinueSystem::~ContinueSystem()
    {
    }
    
    void ContinueSystem::action()
    {
    }
}