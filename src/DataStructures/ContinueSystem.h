////////////////////////////////////////
//                                    //
// File : ContinueSystem.h            //
//                                    //
// Author : Philippe Gontier          //
//                                    //
// Version 1.0                        //
//                                    //
// Date : 10/03/2022                  //
//                                    //
////////////////////////////////////////

#ifndef DataStructures_ContinueSystem_h
#define DataStructures_ContinueSystem_h

// project
#include "./SystemToSimulate.h"

namespace DataStructures
{
    /// Generic Farm 
    class ContinueSystem : public SystemToSimulate
    {
        DECLARE_SERIALIZE_STD_METHODS;
        
    protected:
        ContinueSystem(){} // serialization constructor
    
    public:
        virtual ~ContinueSystem();
        virtual void action();
    }; /* End of class ContinueSystem */
} /* End of namespace DataStructures */
#endif // DataStructures_ContinueSystem_h
