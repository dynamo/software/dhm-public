#include "DairyCow.h"

// Project
#include "../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(DataStructures::DairyCow) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyMammal);

    IMPLEMENT_SERIALIZE_STD_METHODS(DairyCow);
	
    DairyCow::DairyCow(const boost::gregorian::date &birth, Mammal* pDam, Genetic::Breed::Breed* pSireBreed, bool sterile, FunctionalEnumerations::Genetic::Sex sex) : DairyMammal(birth, pDam, pSireBreed, sterile, sex, FunctionalConstants::Global::COW_QUARTER_NB)
    {
    }
	
    DairyCow::DairyCow(const boost::gregorian::date &simDate, const boost::gregorian::date &birth, Genetic::Breed::Breed* pBreed, Herd* pHerd, FunctionalEnumerations::Genetic::Sex sex) : DairyMammal(simDate, birth, pBreed, pHerd, sex, FunctionalConstants::Global::COW_QUARTER_NB)
    {
    }
    
    DairyCow::~DairyCow()
    {
    }
}