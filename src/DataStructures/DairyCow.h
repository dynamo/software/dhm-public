#ifndef DataStructures_DairyCow_h
#define DataStructures_DairyCow_h

// project
#include "DairyMammal.h"
#include "../Tools/Serialization.h"

namespace DataStructures
{
    /// Specific cow as Dairy Mammal
    class DairyCow : public DairyMammal
    {
        DECLARE_SERIALIZE_STD_METHODS;
		
    private:
    
    protected:
        DairyCow(){} // serialization constructor
        void concrete(){}; // To allow instanciation

    
    public:
        virtual ~DairyCow();
        DairyCow(const boost::gregorian::date &birth, Mammal* pDam, Genetic::Breed::Breed* pSireBreed, bool sterile, FunctionalEnumerations::Genetic::Sex sex); // Constructor based with the dam (simulated farrowing)
        DairyCow(const boost::gregorian::date &simDate, const boost::gregorian::date &birth, Genetic::Breed::Breed* pBreed, Herd* pHerd, FunctionalEnumerations::Genetic::Sex sex); // constructor based on a purchase
    }; /* End of class DairyCow */
} /* End of namespace DataStructures */
#endif // DataStructures_DairyCow_h
