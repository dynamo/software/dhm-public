#include "DairyFarm.h"

// standard
#include <memory>

// boost

// Internal
// -------
#include "DairyHerd.h"
#include "Herd.h"
#include "Animal.h"

// Module dependencies
// -------------------
// Reproduction

// Lactation
#include "../Lactation/LactationManagement.h"

// Genetic
#include "../Genetic/GeneticManagement.h"
#include "../Genetic/MaleCatalog.h"

// Health
#include "../Health/HealthManagement.h"

// Population
#include "./Population/Moves/Sale.h"
#include "./Population/Moves/Death.h"
#include "./Population/PopulationManagement.h"
#include "./Population/Strategies/FemaleCalveManagementStrategies/BalanceManagementStrategy.h"
#include "./Population/Strategies/FemaleCalveManagementStrategies/KeepAllFemaleCalvesStrategy.h"
#include "./Population/Strategies/FemaleCalveManagementStrategies/SaleAllFemaleCalvesStrategy.h"
#include "./Population/Strategies/CalveGroupingStrategies/SpreadOutCalvingStrategy.h"
#include "./Population/Strategies/CalveGroupingStrategies/GroupedCalvingStrategy.h"

// Miscellaneous
#include "../ResultStructures/DairyFarmResult.h"
#include "../Tools/Tools.h"
#include "../Tools/Random.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"
#include "../ExchangeInfoStructures/AccountingModelParameters.h"


BOOST_CLASS_EXPORT(DataStructures::DairyFarm) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DiscreteTimeSystem); \
    ar & BOOST_SERIALIZATION_NVP(_monthsOfTheCampaign); \
    ar & BOOST_SERIALIZATION_NVP(_lastIdUnique); \
    ar & BOOST_SERIALIZATION_NVP(_resultBeginDate); \
    ar & BOOST_SERIALIZATION_NVP(_longLactationMilkQuantityLevelBeforeCulling); \
    ar & BOOST_SERIALIZATION_NVP(_hasActiveVetCareContract); \
    ar & BOOST_SERIALIZATION_NVP(_ketosisDayDurationValuesToUse.currentIndex); \
    ar & BOOST_SERIALIZATION_NVP(_ketosisDayDurationValuesToUse.distribuableValues); \
    ar & BOOST_SERIALIZATION_NVP(_vToCareDueToSporadicLamenessDetectionOrVerification); \
    ar & BOOST_SERIALIZATION_NVP(_vToTrimForLamenessPrevention); \
    ar & BOOST_SERIALIZATION_NVP(_originMastitisPreventionFactor); \
    ar & BOOST_SERIALIZATION_NVP(_currentMastitisPreventionFactor); \
    ar & BOOST_SERIALIZATION_NVP(_currentKetosisPreventionFactor); \
    ar & BOOST_SERIALIZATION_NVP(_mortalityProbaToApply); \
    ar & BOOST_SERIALIZATION_NVP(_mDeathOfTheDay); \
    ar & BOOST_SERIALIZATION_NVP(_vpHadMove); \
    ar & BOOST_SERIALIZATION_NVP(_pHasFemaleCalveManagementStrategy); \
    ar & BOOST_SERIALIZATION_NVP(_pHasCalvingGroupingStrategy); \
    ar & BOOST_SERIALIZATION_NVP(_cullingAnticipationValue); \
    ar & BOOST_SERIALIZATION_NVP(_currentAnnualHerdRenewablePart); \
    ar & BOOST_SERIALIZATION_NVP(_currentAnnualHeiferTarget); \
    ar & BOOST_SERIALIZATION_NVP(_pMaleCatalog); \
    ar & BOOST_SERIALIZATION_NVP(_annualStrawConsumption); \
    ar & BOOST_SERIALIZATION_NVP(_mpBreed); \
    ar & BOOST_SERIALIZATION_NVP(_pIncludesHerd); \
    
    IMPLEMENT_SERIALIZE_STD_METHODS(DairyFarm);

    void DairyFarm::resultBegins(const boost::gregorian::date &date)
    {
        // Active the potential vet care contract
        _hasActiveVetCareContract = getOriginDairyFarmParameterInfo().vetCareContract != FunctionalEnumerations::Health::VeterinaryCareContractType::noCareContract;
        _resultBeginDate = date;
    }

    void DairyFarm::setBeginPresimulationDate(const boost::gregorian::date &beginPreDate)
    {
        // Generic method
        DiscreteTimeSystem::setBeginPresimulationDate(beginPreDate);
        
        // Method to be considered as instance initialization
        
        _pIncludesHerd->setInitialBatchLocation(beginPreDate);
        
        // Set the list of months of the campaign 
        int theMonth = beginPreDate.month().as_number();
        _monthsOfTheCampaign.push_back((FunctionalEnumerations::Global::Month)theMonth);
        while(theMonth != FunctionalEnumerations::Global::Month::dec)
        {
            theMonth += 1;
            _monthsOfTheCampaign.push_back((FunctionalEnumerations::Global::Month)theMonth);
        } 
        theMonth = FunctionalEnumerations::Global::Month::jan;
        while (_monthsOfTheCampaign.size() != 12)
        {
            _monthsOfTheCampaign.push_back((FunctionalEnumerations::Global::Month)theMonth);
            theMonth += 1;
        }
        // Set the ketosis durations to use
        Health::HealthManagement::getketosisDayDurationValues(_ketosisDayDurationValuesToUse, getRandom());
    }
            
    void DairyFarm::setBeginSimulationDate(const boost::gregorian::date &beginDate)
    {
        // Generic method
        DiscreteTimeSystem::setBeginSimulationDate(beginDate);
    }
            
    unsigned int DairyFarm::getYearNumberForPresimulation()
    {
        return TechnicalConstants::PRESIMULATION_DURATION;
    }
    
    std::vector<FunctionalEnumerations::Global::Month> &DairyFarm::getMonthsOfTheCampaign()
    {
        return _monthsOfTheCampaign;
    }
    
    ExchangeInfoStructures::Parameters::DairyFarmParameters &DairyFarm::getOriginDairyFarmParameterInfo()
    {
        //return *((ExchangeInfoStructures::Parameters::DairyFarmParameters*)_pHasOriginSystemToSimulateInfo);
        return _hasOriginSystemToSimulateInfo;
    }
    
    Results::DairyFarmResult* DairyFarm::getCurrentResults()
    {
        return (Results::DairyFarmResult*)_pCurrentResults;
    }

    DairyFarm::DairyFarm(ExchangeInfoStructures::Parameters::DairyFarmParameters &info) : DiscreteTimeSystem(info)
    {        
        // Creating the breeds
        Genetic::GeneticManagement::getNewBreeds(_mpBreed);

        _pIncludesHerd = new DataStructures::DairyHerd(this, _mpBreed.find(getOriginDairyFarmParameterInfo().initialBreed)->second, getOriginDairyFarmParameterInfo().herdProductionDelta, getOriginDairyFarmParameterInfo().minimumMilkQuantityFactorForRentability); // creating the dairy herd
        
        // Creating the male catalog
        _pMaleCatalog = new Genetic::MaleCatalog(info.maleDataFile, info.geneticStrategy, _mpBreed);
        
        // Population
        _currentAnnualHerdRenewablePart = info.initialAnnualHerdRenewablePart * TechnicalConstants::ANNUAL_HERD_RENEWABLE_PART_CALIBRATION;
        _currentAnnualHeiferTarget = getMeanAdultNumberTarget() * _currentAnnualHerdRenewablePart;
        
        // Female calve management strategy
        if (getOriginDairyFarmParameterInfo().femaleCalveManagement == FunctionalEnumerations::Population::FemaleCalveManagement::balanceManagement)
        {
            _pHasFemaleCalveManagementStrategy = new Population::Strategies::BalanceManagementStrategy(this);
        }
        else if (getOriginDairyFarmParameterInfo().femaleCalveManagement == FunctionalEnumerations::Population::FemaleCalveManagement::keepAllFemaleCalves)
        {
            _pHasFemaleCalveManagementStrategy = new Population::Strategies::KeepAllFemaleCalvesStrategy(this);
        }
        else if (getOriginDairyFarmParameterInfo().femaleCalveManagement == FunctionalEnumerations::Population::FemaleCalveManagement::saleAllFemaleCalves)
        {
            _pHasFemaleCalveManagementStrategy = new Population::Strategies::SaleAllFemaleCalvesStrategy(this);
        }
        // Calving grouping strategy
        if (getOriginDairyFarmParameterInfo().matingPlan.excludedHeiferCalvingPeriod.firstMonthForCalvingExculsion == 0)
        {
            _pHasCalvingGroupingStrategy = new Population::Strategies::SpreadOutCalvingStrategy(this);
        }
        else 
        {
            _pHasCalvingGroupingStrategy = new Population::Strategies::GroupedCalvingStrategy(this);
        }
        
        // Long lactation milk quantity culling decision
//        float a = (FunctionalConstants::Population::LOW_LEVEL_MILK_PRODUCTION_RENTABILITY - FunctionalConstants::Population::HIGH_LEVEL_MILK_PRODUCTION_RENTABILITY) / (FunctionalConstants::Population::LOW_LEVEL_MILK_PRODUCTION_CULLING - FunctionalConstants::Population::HIGH_LEVEL_MILK_PRODUCTION_CULLING); // a = (Y2 - Y1) / 5(X2 - X1)
//        float b = FunctionalConstants::Population::HIGH_LEVEL_MILK_PRODUCTION_RENTABILITY - a * FunctionalConstants::Population::HIGH_LEVEL_MILK_PRODUCTION_CULLING; // b = Y1 - aX1
//
//        float herdMilkQuantityObjective = (_mpBreed.find(getOriginDairyFarmParameterInfo().initialBreed)->second->getBreedMeanProductions().find(FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait)->second + getOriginDairyFarmParameterInfo().herdProductionDelta.quantity);
//
//        _longLactationMilkQuantityLevelBeforeCulling = a * herdMilkQuantityObjective + b;
//        std::cout << "_longLactationMilkQuantityLevelBeforeCulling = " << _longLactationMilkQuantityLevelBeforeCulling << std::endl;
        
        // Health prevention factors
        _originMastitisPreventionFactor = (getOriginDairyFarmParameterInfo().individualMastitisIncidencePreventionFactor * getOriginDairyFarmParameterInfo().herdMastitisIncidencePreventionFactor);
        _currentMastitisPreventionFactor = _originMastitisPreventionFactor;
        _currentKetosisPreventionFactor = getOriginKetosisPreventionFactor();
//        _pMastitisPreventionModulationMatrix = new Tools::TwoDimentionalModulationMatrix(FunctionalConstants::Health::PREVENTION_FACTOR_VALUES, FunctionalConstants::Health::DEAD_LINE_VALUES, FunctionalConstants::Health::MASTITIS_PREVENTION_MODULATION_FACTOR_VALUES);
//        _pKetosisPreventionModulationMatrix = new Tools::TwoDimentionalModulationMatrix(FunctionalConstants::Health::PREVENTION_FACTOR_VALUES, FunctionalConstants::Health::DEAD_LINE_VALUES, FunctionalConstants::Health::KETOSIS_PREVENTION_MODULATION_FACTOR_VALUES);

    } /* End of constructor */
        
    DairyFarm::~DairyFarm() // Destructor
    {
        // Strategies
        delete _pHasFemaleCalveManagementStrategy;

        delete _pHasCalvingGroupingStrategy;
        
        // Male catalog
        delete _pMaleCatalog;        
                
        // Has moves
        for (unsigned int i = 0; i < _vpHadMove.size(); i++)
        {
            delete _vpHadMove[i];
        }
        _vpHadMove.clear();
        
        delete _pIncludesHerd;
        _pIncludesHerd = nullptr;
        
        // Breeds
        for (std::map<FunctionalEnumerations::Genetic::Breed, Genetic::Breed::Breed*>::iterator it = _mpBreed.begin(); it != _mpBreed.end(); it++)
        {
            delete it->second;
        }
        _mpBreed.clear();
        
        // Health
//        delete _pMastitisPreventionModulationMatrix;
//        delete _pKetosisPreventionModulationMatrix;
    }
    
    void DairyFarm::updateRenewalRatio()
    {
        _currentAnnualHerdRenewablePart += getOriginDairyFarmParameterInfo().annualHerdRenewableDelta * TechnicalConstants::ANNUAL_HERD_RENEWABLE_PART_CALIBRATION;

        float ponderedMin = FunctionalConstants::Population::MIN_ANNUAL_HERD_RENEWAL_PART * TechnicalConstants::ANNUAL_HERD_RENEWABLE_PART_CALIBRATION;
        float ponderedMax = FunctionalConstants::Population::MAX_ANNUAL_HERD_RENEWAL_PART * TechnicalConstants::ANNUAL_HERD_RENEWABLE_PART_CALIBRATION;
         if (_currentAnnualHerdRenewablePart <  ponderedMin) _currentAnnualHerdRenewablePart = ponderedMin;
         if (_currentAnnualHerdRenewablePart >  ponderedMax) _currentAnnualHerdRenewablePart = ponderedMax;
//         std::cout << "_currentAnnualHerdRenewablePart = " << _currentAnnualHerdRenewablePart << std::endl;
         _currentAnnualHeiferTarget = getMeanAdultNumberTarget() * _currentAnnualHerdRenewablePart;
         _pHasFemaleCalveManagementStrategy->updateRenewalRatio();
    }
    
    std::map<FunctionalEnumerations::Genetic::Sex, std::map<FunctionalEnumerations::Genetic::BreedType, std::map<FunctionalEnumerations::Population::MortalityRiskAge, float>>> &DairyFarm::getGlobalMortalityProba()
    {
        return _mortalityProbaToApply;
    }
    
    Population::Strategies::FemaleCalveManagementStrategy* DairyFarm::getFemaleCalveManagementStrategy()
    {
        return _pHasFemaleCalveManagementStrategy;
    }
    
    unsigned int DairyFarm::getNewIdUnique()
    {
        return ++_lastIdUnique;
    }
 
    float DairyFarm::getCurrentAnnualHerdRenewablePart()
    {
        return _currentAnnualHerdRenewablePart;
    }
    
    unsigned int DairyFarm::getMeanAdultNumberTarget()
    {
        return getOriginDairyFarmParameterInfo().meanAdultNumberTarget * TechnicalConstants::FACTOR_POPULATION_NUMBER_CALIBRATION;
    }
    
    unsigned int DairyFarm::getAnnualHeiferTarget()
    {
        return getCurrentAnnualHerdRenewablePart() * getMeanAdultNumberTarget();
    }
     
    unsigned int DairyFarm::getMaxLactationRankForEndInseminationDecision()
    {
        return getOriginDairyFarmParameterInfo().maxLactationRankForEndInseminationDecision;
    }
        
    float DairyFarm::getMinMilkProductionHerdRatioForEndInseminationDecision()
    {
        return getOriginDairyFarmParameterInfo().minMilkProductionHerdRatioForEndInseminationDecision;
    }
        
    unsigned int DairyFarm::getMaxSCCLevelForEndInseminationDecision()
    {
        return getOriginDairyFarmParameterInfo().maxSCCLevelForEndInseminationDecision;
    }
         
    Genetic::Breed::Breed* DairyFarm::getBreed(FunctionalEnumerations::Genetic::Breed theBreed)
    {
        std::map<FunctionalEnumerations::Genetic::Breed,Genetic::Breed::Breed*>::iterator it = _mpBreed.find(theBreed);
#ifdef _LOG
        assert(it != _mpBreed.end()); // The request breed must be in the collection
#endif // _LOG
        return it->second;
    }
    
    Genetic::Breed::Breed* DairyFarm::getInitialBreed()
    {
        return _mpBreed.find(getOriginDairyFarmParameterInfo().initialBreed)->second;
    }
        
    void DairyFarm::getInseminationParameters(unsigned int lactationRank, unsigned int inseminationRank, FunctionalEnumerations::Reproduction::TypeInseminationSemen &typeSemen, float &typeRatio, FunctionalEnumerations::Genetic::Breed &breedSemen, float &breedRatio)
    {
        FunctionalEnumerations::Reproduction::InseminationRank ir;
        FunctionalEnumerations::Reproduction::LactationRankForInsemination la;
        
        if (lactationRank == 0)
        {
            la = FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination;
        }
        else
        {
            la = FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination;
        }

        if (inseminationRank == 0)
        {
            ir = FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination;
        }
        else if (inseminationRank > 0 and inseminationRank <= 3)
        {
            ir = FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3;
        }
        else
        {
            ir = FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver;
        }
        getOriginDairyFarmParameterInfo().matingPlan.getTypeInseminationSemen(ir, la, typeSemen, typeRatio);
        getOriginDairyFarmParameterInfo().matingPlan.getBreedInseminationSemen(ir, la, breedSemen, breedRatio);
    }
    
    FunctionalEnumerations::Reproduction::DetectionMode DairyFarm::getDetectionMode()
    {
        return getOriginDairyFarmParameterInfo().detectionMode;
    }
    
    FunctionalEnumerations::Genetic::GeneticStrategy DairyFarm::getGeneticStrategy()
    {
        return getOriginDairyFarmParameterInfo().geneticStrategy;
    }
    
    ExchangeInfoStructures::Parameters::ExcludedCalvingPeriodParameters &DairyFarm::getHeiferCalvingExclusionPeriod()
    {
        return getOriginDairyFarmParameterInfo().matingPlan.excludedHeiferCalvingPeriod;
    }
    
    bool DairyFarm::floorIsSlipery()
    {
        return getOriginDairyFarmParameterInfo().slipperyFloor;
    }

    ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &DairyFarm::getCurrentAccountingModel()
    {
        //return *((ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters*)_pCurrentEvoluatingAccountingModel);
        return _currentEvoluatingAccountingModel;   
    }

    float DairyFarm::getVetCareContractCost(
#ifdef _LOG              
                                            const boost::gregorian::date &simDate
#endif // _LOG                
                                            )
    {
        float res = 0.0f;
        if (hasActiveVetCareContract())
        {
             // Care contract cost calculation
            float amount = 0.0f;

#ifdef _LOG              
            std::string comp;
#endif // _LOG                
            if (getOriginDairyFarmParameterInfo().vetCareContract == FunctionalEnumerations::Health::VeterinaryCareContractType::careContractBasedOnCowNumber)
            {
                amount = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_VET_CARE_CONTRACT_BASED_ON_COW_COST) * getDairyHerd()->getPreviousCampaignMeanAdultCowCount();
#ifdef _LOG              
                comp = FunctionalConstants::AccountingModel::VET_CARE_CONTRACT_BASED_ON_COW_TRANSACTION_TYPE;
#endif // _LOG                
            }
            else if (getOriginDairyFarmParameterInfo().vetCareContract == FunctionalEnumerations::Health::VeterinaryCareContractType::careContractBasedOnCalvingNumber)
            {
                amount = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_VET_CARE_CONTRACT_BASED_ON_CALVING_COST) * getDairyHerd()->getPreviousCampaignCalvingCount();
#ifdef _LOG                
                comp = FunctionalConstants::AccountingModel::VET_CARE_CONTRACT_BASED_ON_CALVING_TRANSACTION_TYPE;
#endif // _LOG                
            }
            else if (getOriginDairyFarmParameterInfo().vetCareContract == FunctionalEnumerations::Health::VeterinaryCareContractType::careContractBasedOnKiloliterNumber)
            {
                amount = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_VET_CARE_CONTRACT_BASED_ON_KILOLITER_COST) * getDairyHerd()->getPreviousCampaignDeliveredKiloliterCount();
#ifdef _LOG                
                comp = FunctionalConstants::AccountingModel::VET_CARE_CONTRACT_BASED_ON_KILOLITER_TRANSACTION_TYPE;
#endif // _LOG                
            }
            else
            {
                assert (false);
            }
            getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    simDate, comp, 
#endif // _LOG                
                                    amount, res);
#ifdef _LOG                   
//            addManagedHealthCost(amount); // add to the annual managed health costs
#endif // _LOG                  
//            std::cout << "DairyFarm::progress, simDate = " << simDate << ", vetCareContract = " << getOriginDairyFarmParameterInfo().vetCareContract << ", amount = " << amount << ", getOtherHealthCostsForUpdate = " << getDairyHerd()->getOtherHealthCostsForUpdate() << std::endl;
        }      
        
        return res;
    }
    
    float DairyFarm::getVetReproductionContractCost(
#ifdef _LOG              
                                                    const boost::gregorian::date &simDate
#endif // _LOG                
                                                    )
    {
        float res = 0.0f;
        if (hasVetReproductionContract())
        {
            getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    simDate, FunctionalConstants::AccountingModel::VET_REPRODUCTION_CONTRACT_TRANSACTION_TYPE, 
#endif // _LOG                
                                    getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_VET_REPRODUCTION_CONTRACT_COST) * getDairyHerd()->getMeanAdultCowCount(), res);
#ifdef _LOG                   
//            addManagedHealthCost(getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_VET_REPRODUCTION_CONTRACT_COST) * getDairyHerd()->getMeanAdultCowCount()); // add to the annual managed health costs
#endif // _LOG                  
        }      
        return res;
    }
    
        float DairyFarm::getFootBathCost(
#ifdef _LOG              
                                                    const boost::gregorian::date &simDate
#endif // _LOG                
                                                    )
    {
        float res = 0.0f;
        if (useFootBath())
        {
            getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    simDate, FunctionalConstants::AccountingModel::ANNUAL_FOOT_BATH_COST_TRANSACTION_TYPE, 
#endif // _LOG                
                                    getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_LAMENESS_ANNUAL_FOOT_BATH_COST), res);
#ifdef _LOG                   
//            addManagedHealthCost(getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_LAMENESS_ANNUAL_FOOT_BATH_COST)); // add to the annual managed health costs
#endif // _LOG                  
        }           
        return res;
    }
    
    float DairyFarm::getMastitisPercentPreventionProgress()
    {
#ifdef _SENSIBILITY_ANALYSIS
//        std::cout << "getMastitisPercentPreventionProgress = " << getOriginDairyFarmParameterInfo().SA_mastitisPercentPreventionProgress << std::endl;
        return getOriginDairyFarmParameterInfo().SA_mastitisPercentPreventionProgress;
#else // _SENSIBILITY_ANALYSIS
        return FunctionalConstants::Health::PERCENT_PREVENTION_PROGRESS;  
#endif // _SENSIBILITY_ANALYSIS
    }

    float DairyFarm::getKetosisPercentPreventionProgress()
    {
#ifdef _SENSIBILITY_ANALYSIS
//        std::cout << "getKetosisPercentPreventionProgress = " << getOriginDairyFarmParameterInfo().SA_ketosisPercentPreventionProgress << std::endl;
        return getOriginDairyFarmParameterInfo().SA_ketosisPercentPreventionProgress;
#else // _SENSIBILITY_ANALYSIS
        return FunctionalConstants::Health::PERCENT_PREVENTION_PROGRESS;  
#endif // _SENSIBILITY_ANALYSIS
    }
    
    float DairyFarm::getAdditionalMastitisHealingIfCareVetContract()
    {
#ifdef _SENSIBILITY_ANALYSIS
//        std::cout << "getAdditionalMastitisHealingIfCareVetContract = " << getOriginDairyFarmParameterInfo().SA_additionalMastitisHealingIfCareVetContract << std::endl;
        return getOriginDairyFarmParameterInfo().SA_additionalMastitisHealingIfCareVetContract;
#else // _SENSIBILITY_ANALYSIS
        return FunctionalConstants::Health::ADDITIONAL_MASTITIS_HEALING_PROBABILITY_IF_CARE_VET_CONTRACT;  
#endif // _SENSIBILITY_ANALYSIS
    }
        
    float DairyFarm::getAdditionalKetosisHealingIfCareVetContract()
    {
#ifdef _SENSIBILITY_ANALYSIS
//        std::cout << "getAdditionalKetosisHealingIfCareVetContract = " << getOriginDairyFarmParameterInfo().SA_additionalKetosisHealingIfCareVetContract << std::endl;
        return getOriginDairyFarmParameterInfo().SA_additionalKetosisHealingIfCareVetContract;
#else // _SENSIBILITY_ANALYSIS
        return FunctionalConstants::Health::ADDITIONAL_KETOSIS_HEALING_PROBABILITY_IF_CARE_VET_CONTRACT;  
#endif // _SENSIBILITY_ANALYSIS
    }
        
    float DairyFarm::getNonManagedMortalityFactorWithVetCareContract()
    {
#ifdef _SENSIBILITY_ANALYSIS
//        std::cout << "getNonManagedMortalityFactorWithVetCareContract = " << getOriginDairyFarmParameterInfo().SA_nonManagedMortalityFactorWithVetCareContract << std::endl;
        return getOriginDairyFarmParameterInfo().SA_nonManagedMortalityFactorWithVetCareContract;
#else // _SENSIBILITY_ANALYSIS
        return FunctionalConstants::Population::NON_MANAGED_MORTALITY_FACTOR_WITH_VET_CARE_CONTRACT;  
#endif // _SENSIBILITY_ANALYSIS
    }
    
    void DairyFarm::progress(const boost::gregorian::date &simDate)
    {
        // General progress
        DiscreteTimeSystem::progress(simDate);
        
        // progress initializations :
        // ...
        
#ifdef _LOG
        _calveSoldOfTheDay = 0;
        _heifersSoldOfTheDay = 0;
        _adultsSoldOfTheDay = 0;
#endif // _LOG    
        
        if (isFirstDayOfTheCampaign(simDate))
        {
            // It is the first day of the campain, so we have to set some values
            
            // update renewal ratio ?
            if (not _resultBeginDate.is_not_a_date() and getOriginDairyFarmParameterInfo().annualHerdRenewableDelta != 0.0f)
            {
                boost::gregorian::date dateForUpdateRenewalRatio = _resultBeginDate + boost::gregorian::years(1); // Year + 1
                if (simDate >= dateForUpdateRenewalRatio)
                {
                    updateRenewalRatio();
                }
            }
            
            // Vet contracts
            if (hasActiveVetCareContract())
            {
                if (simDate == _resultBeginDate)
                {
                    // Mastitis healing probability depending on vet contract option
                    for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++)
                    {
                        float &healingProbability = _hasOriginSystemToSimulateInfo.mastitisTreatmentPlan.mastitisLactationTreatmentTypes.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second.successProbabilityAllCasesOrFirst;
                        //healingProbability += FunctionalConstants::Health::ADDITIONAL_MASTITIS_HEALING_PROBABILITY_IF_CARE_VET_CONTRACT; 
                        healingProbability += getAdditionalMastitisHealingIfCareVetContract(); 
                   }

                    // Ketosis healing probability depending on vet contract option
                    //_hasOriginSystemToSimulateInfo.ketosisTreatmentPlan.G2ketosisTreatmentType.successProbabilityAllCasesOrFirst += FunctionalConstants::Health::ADDITIONAL_KETOSIS_HEALING_PROBABILITY_IF_CARE_VET_CONTRACT; 
                    _hasOriginSystemToSimulateInfo.ketosisTreatmentPlan.G2ketosisTreatmentType.successProbabilityAllCasesOrFirst += getAdditionalKetosisHealingIfCareVetContract(); 
                }
                                          
                // Cetodetect use (2.2.3.2.1.5.2)
                _hasOriginSystemToSimulateInfo.cetoDetectUsage = true;

                // Mortality probability
                boost::gregorian::date beginContractDelayDate = _resultBeginDate + boost::gregorian::years(FunctionalConstants::Population::VET_CARE_CONTRACT_DURATION_FOR_FACTOR_APPLICATION);
                if (simDate == beginContractDelayDate)
                {
                    // New mortality proba to apply : we have to ponderate the unmanaged deseases mortality due to vet care contract
                    for (std::map<FunctionalEnumerations::Genetic::Sex, std::map<FunctionalEnumerations::Genetic::BreedType,std::map<FunctionalEnumerations::Population::MortalityRiskAge, float>>>::iterator itSex = _mortalityProbaToApply.begin(); itSex != _mortalityProbaToApply.end(); itSex++)
                    {
                        for (std::map<FunctionalEnumerations::Genetic::BreedType,std::map<FunctionalEnumerations::Population::MortalityRiskAge, float>>::iterator itBreedType = itSex->second.begin(); itBreedType != itSex->second.end(); itBreedType++)
                        {
                            for (std::map<FunctionalEnumerations::Population::MortalityRiskAge, float>::iterator itAge = itBreedType->second.begin(); itAge != itBreedType->second.end(); itAge++)
                            {
                                //itAge->second *= FunctionalConstants::Population::NON_MANAGED_MORTALITY_FACTOR_WITH_VET_CARE_CONTRACT;
                                itAge->second *= getNonManagedMortalityFactorWithVetCareContract();
                            }
                        }
                    }
                }
                
                // Trimming option
                beginContractDelayDate = _resultBeginDate + boost::gregorian::years(FunctionalConstants::Health::VET_CARE_CONTRACT_DURATION_FOR_LAMENESS_PREVENTION_ACTION);
                if (simDate == beginContractDelayDate)
                {
                    // Clear the planed preventive trimming group
                    _vToTrimForLamenessPrevention.clear();
                    
                    // New trimmming option to apply
                    _hasOriginSystemToSimulateInfo.preventiveTrimmingOption = FunctionalEnumerations::Health::LamenessPreventiveTrimmingOption::preventiveTrimmingTwiceAYear;
                }
            }      
        }
        
        // Care vet contract
        if (hasActiveVetCareContract())
        {        
            // G1 mastitis detection probability
            if (_hasOriginSystemToSimulateInfo.G1MastitisDetectionSensibility != FunctionalConstants::Health::G1_MASTITIS_DETECTION_PROBABILITY_IF_CARE_VET_CONTRACT)
            {
                boost::gregorian::date beginContractDelayDate = _resultBeginDate + boost::gregorian::months(FunctionalConstants::Health::VET_CARE_CONTRACT_DURATION_FOR_FARMER_G1_MASTITIS_DETECTION_PROBABILITY);
                if (simDate == beginContractDelayDate)
                {
                    // New G1 mastitis detection probability
                    _hasOriginSystemToSimulateInfo.G1MastitisDetectionSensibility = FunctionalConstants::Health::G1_MASTITIS_DETECTION_PROBABILITY_IF_CARE_VET_CONTRACT;
                }
            }
            
            // Lameness detection sensitivity
            if (_hasOriginSystemToSimulateInfo.lamenessDetectionMode == FunctionalEnumerations::Health::LamenessDetectionMode::lowSensitiveFarmer)
            {
                boost::gregorian::date beginContractDelayDate = _resultBeginDate + boost::gregorian::months(FunctionalConstants::Health::VET_CARE_CONTRACT_DURATION_FOR_FARMER_LAMENESS_DETECTION_SENSITIVITY);
                if (simDate == beginContractDelayDate)
                {
                    // New lameness detection sensitivity
                    _hasOriginSystemToSimulateInfo.lamenessDetectionMode = FunctionalEnumerations::Health::LamenessDetectionMode::hiSensitiveFarmer;
                }
            }
            
            // Curative lameness group resizing
            if (_hasOriginSystemToSimulateInfo.cowCountForSmallTrimmingOrCareGroup > FunctionalConstants::Health::VET_CARE_CONTRACT_TRIMMING_GROUP_SIZE)
            {
                boost::gregorian::date beginContractDelayDate = _resultBeginDate + boost::gregorian::months(FunctionalConstants::Health::VET_CARE_CONTRACT_DURATION_FOR_CURATIVE_TRIMMING_GROUP_RESIZING);
                if (simDate == beginContractDelayDate)
                {
                    // New curative lameness group size
                    _hasOriginSystemToSimulateInfo.cowCountForSmallTrimmingOrCareGroup = FunctionalConstants::Health::VET_CARE_CONTRACT_TRIMMING_GROUP_SIZE;
                }
            }
            
            if (simDate.day() == 1)
            {
                // We have to modulate the prevention factor for mastitis and ketosis because of vet care contract
                // See "DefaultFarmCalibrage.xlsx"
                
                unsigned int firstMonth = FunctionalConstants::Health::MONTHS_FOR_PREVENTION_EVOLUTION[0];
                unsigned int lastMonth = FunctionalConstants::Health::MONTHS_FOR_PREVENTION_EVOLUTION[1];
                unsigned int month = (simDate -_resultBeginDate).days()/30;
                float baseToConsiderForMastitisPrevention = _originMastitisPreventionFactor;
                float baseToConsiderForKetosisPrevention = getOriginKetosisPreventionFactor();
                if (month >  lastMonth)
                {
                    firstMonth = FunctionalConstants::Health::MONTHS_FOR_PREVENTION_EVOLUTION[1];
                    lastMonth = FunctionalConstants::Health::MONTHS_FOR_PREVENTION_EVOLUTION[2];
                    baseToConsiderForMastitisPrevention /= getMastitisPercentPreventionProgress();
                    baseToConsiderForKetosisPrevention /= getKetosisPercentPreventionProgress();
                    if (month > lastMonth) month = lastMonth;
                }
                
                // =L3+(L3*(1/0,75-1))*(A27-A19)/(A43-A19) si avant 24
                // ou
                // =B43+(B43*(1/0,75-1))*(A49-A43)/(A61-A43) sinon
                // Donc :
                // = baseToConsider + (baseToConsider * (1/getPercentPreventionProgress() - 1)) * (months  -  firstMonth) / (lastMonth - firstMonth)
                
                _currentMastitisPreventionFactor  = baseToConsiderForMastitisPrevention + (baseToConsiderForMastitisPrevention * (1/getMastitisPercentPreventionProgress() - 1)) * (month  -  firstMonth) / (lastMonth - firstMonth);
                _currentKetosisPreventionFactor  = baseToConsiderForKetosisPrevention + (baseToConsiderForKetosisPrevention * (1/getKetosisPercentPreventionProgress() - 1)) * (month  -  firstMonth) / (lastMonth - firstMonth);
//                std::cout << "month = " << month << ", baseToConsiderForMastitisPrevention = " << baseToConsiderForMastitisPrevention << ", _currentMastitisPreventionFactor = " << _currentMastitisPreventionFactor << std::endl;
//                std::cout << "month = " << month << ", baseToConsiderForKetosisPrevention = " << baseToConsiderForKetosisPrevention << ", _currentKetosisPreventionFactor = " << _currentKetosisPreventionFactor << std::endl;
            }
        }
        
        // Date for genetic progress ?
        if (simDate.day_of_year() == FunctionalConstants::Genetic::DAY_OF_THE_YEAR_FOR_GENETIC_PROGRESS and simDate > _beginSimulationDate)
        {
            // Breed progress
            for (auto itBreed : _mpBreed)
            {
                (itBreed.second)->appendAnnualProgress();
            }     
            
            // Male catalog
            _pMaleCatalog->appendMaleAnnualProgress(getGeneticStrategy());
        }
        
        // Curative or preventive care ? 
        // Animals to care for detected lameness ?
        if (_vToCareDueToSporadicLamenessDetectionOrVerification.size() >= getCowCountForSmallTrimmingOrCareGroup())
        {
            careCowsTodayForDetectedLamenessCase(
#ifdef _LOG                
                                    simDate
#endif // _LOG                
                                   );
        }

        // Animals to trim preventively for lameness ?  
        unsigned int sizeOfGroupForPreventiveTrim = 1; // General case
        if (getTrimmingOption() == FunctionalEnumerations::Health::LamenessPreventiveTrimmingOption::preventiveTrimmingSmallGroup)
        {
            // Small group option
            sizeOfGroupForPreventiveTrim = getCowCountForSmallTrimmingOrCareGroup();
        }
        
        if (_vToTrimForLamenessPrevention.size() >= sizeOfGroupForPreventiveTrim)
        {
            trimCowsTodayForLamenessPrevention(
#ifdef _LOG                
                                    simDate
#endif // _LOG                
                                    );
        }
        
        // Progression of the herd
        _pIncludesHerd->progress(simDate);
        
        // Deaths ?
#ifdef _LOG
        _deathOfTheWeekNumber += _mDeathOfTheDay.size();
#endif // _LOG        
        while (_mDeathOfTheDay.size()>0)
        { 
            auto it = _mDeathOfTheDay.begin();
            Animal* pAnim = (getDairyHerd()->getPresentAnimals().find(it->first))->second;
#ifdef _LOG
            assert (pAnim != nullptr);
#endif // _LOG        
            addMove(new Population::Death(pAnim, simDate, getDairyHerd()->getOtherCostsForUpdate(), it->second)); // Creating the move          
        }
        
        if (isLastDayOfTheCampaign(simDate))
        {
            // It is the last day of the campain, so we have to produce some results
                   
            // Miscellaneous month costs
            float adultCowCount = getDairyHerd()->getMeanAdultCowCount();
            
            if (hasActiveVetCareContract())
            {
                // Cow preventive action cost
                getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    simDate, FunctionalConstants::AccountingModel::MASTITIS_PREVENTIVE_ACTION_TRANSACTION_TYPE, 
#endif // _LOG                
                                    getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_MASTITIS_PREVENTIVE_ACTION_COST) * adultCowCount, getDairyHerd()->getMastitisHealthCostsForUpdate());

                getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    simDate, FunctionalConstants::AccountingModel::KETOSIS_PREVENTIVE_ACTION_TRANSACTION_TYPE, 
#endif // _LOG                
                                    getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_KETOSIS_PREVENTIVE_ACTION_COST) * adultCowCount, getDairyHerd()->getKetosisHealthCostsForUpdate());
                
                //  Monthly scheduled vet activity count
                boost::gregorian::date beginSecondYearContractDate = _resultBeginDate + boost::gregorian::years(1);
                if (simDate < beginSecondYearContractDate)
                {

                    getDairyHerd()->increaseScheduledFarmerActivityCount(FunctionalConstants::Health::MONTHLY_SCHEDULED_VET_ACTIVITY_FIRST_YEAR_COUNT * 12);
                    getDairyHerd()->increaseScheduledVetActivityCount(FunctionalConstants::Health::MONTHLY_SCHEDULED_VET_ACTIVITY_FIRST_YEAR_COUNT * 12);
                }
                else
                {
                    getDairyHerd()->increaseScheduledFarmerActivityCount(FunctionalConstants::Health::MONTHLY_SCHEDULED_VET_ACTIVITY_NEXT_YEAR_COUNT * 12);
                    getDairyHerd()->increaseScheduledVetActivityCount(FunctionalConstants::Health::MONTHLY_SCHEDULED_VET_ACTIVITY_NEXT_YEAR_COUNT * 12);
                }
            }
                    
            // Cow bedding cost
            getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    simDate, FunctionalConstants::AccountingModel::COW_BEDDING_TRANSACTION_TYPE, 
#endif // _LOG                
                                    getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_ANNUAL_COW_BEDDING_PRICE) * adultCowCount, getDairyHerd()->getOtherCostsForUpdate());

            
           
            // Cow miscellaneous vet cost
#ifdef _LOG
//            float annualManagedHealthCostParCow = 0.0f;
//            if (adultCowCount > 0)
//            {
//                annualManagedHealthCostParCow = _annualManagedHealthCost / adultCowCount;
//            }
//            getCurrentResults()->getManagedHealthCostsForCalibration().push_back(annualManagedHealthCostParCow);
//            _annualManagedHealthCost = 0.0f;
#endif // _LOG        
            getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    simDate, FunctionalConstants::AccountingModel::COW_MISCELLANEOUS_VET_TRANSACTION_TYPE, 
#endif // _LOG                
                                    getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_ANNUAL_COW_MISCELLANEOUS_VET_COST) * adultCowCount, getDairyHerd()->getOtherCostsForUpdate());
                        
            // Other cow breeding cost
            getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    simDate, FunctionalConstants::AccountingModel::OTHER_COW_BREEDING_TRANSACTION_TYPE, 
#endif // _LOG                
                                    getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_OTHER_ANNUAL_COW_BREEDING_COST) * adultCowCount, getDairyHerd()->getOtherCostsForUpdate());
            
            // Straw consumption
            getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    simDate, FunctionalConstants::AccountingModel::STRAW_CONSUMPTION_TYPE, 
#endif // _LOG                
                                    getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_STRAW_TON_PRICE) * (float)_annualStrawConsumption / 1000, getDairyHerd()->getOtherCostsForUpdate());
            _annualStrawConsumption = 0.0f;
            
            // Progression of the herd
            _pIncludesHerd->produceEndCampaignResults(simDate);
        }

// Only in debug mode
#ifdef _LOG
        _calveSoldOfTheWeekNumber += _calveSoldOfTheDay;
        _heifersSoldOfTheWeekNumber += _heifersSoldOfTheDay;
        _adultsSoldOfTheWeekNumber += _adultsSoldOfTheDay;

        // Week after operations
        if (simDate.day_of_week().as_short_string() == FunctionalConstants::Global::DAY_OF_THE_WEEK_FOR_OPERATIONS)
        {
            // Results
            Results::WeekAnimalPopulationInformations weekRes;
            weekRes.simulationDate = Tools::toString(simDate);
            
            // Calve sold
            weekRes.calveSold = _calveSoldOfTheWeekNumber;
            _calveSoldOfTheWeekNumber = 0;
            
            // Heifers sold
            weekRes.heifersSold = _heifersSoldOfTheWeekNumber;
            _heifersSoldOfTheWeekNumber = 0;
            
            // Adults sold
            weekRes.adultsSold = _adultsSoldOfTheWeekNumber;
            _adultsSoldOfTheWeekNumber = 0;
            
            // deaths
            weekRes.deathNumber = _deathOfTheWeekNumber;
            _deathOfTheWeekNumber = 0;
            
            // herd week informations
            getDairyHerd()->updateWeekResults(weekRes);

            // Results
            std::vector<Results::WeekAnimalPopulationInformations> &rvWeekAnimalPopulationInformations = getCurrentResults()->getWeekAnimalPopulationInformations();
            if (rvWeekAnimalPopulationInformations.size() == 0)
            {
                // it is the first time, no results yet
                Results::WeekAnimalPopulationInformations theoricalRes;
                theoricalRes.simulationDate = "popTarget";
               getDairyHerd()->getPopulationTargetValue(theoricalRes);
                rvWeekAnimalPopulationInformations.push_back(theoricalRes);
            }
            rvWeekAnimalPopulationInformations.push_back(weekRes);
        }
#endif // _LOG        
    }
    
    float DairyFarm::getCalfDayStrawConsumption()
    {
        return getOriginDairyFarmParameterInfo().calfStrawConsumption;
    }
        
    float DairyFarm::getHeiferDayStrawConsumption()
    {
        return getOriginDairyFarmParameterInfo().heiferStrawConsumption;
    }
        
    float DairyFarm::getAdultDayStrawConsumption()
    {
        return getOriginDairyFarmParameterInfo().adultStrawConsumption;
    }
        
    void DairyFarm::addStrawConsomption(float strawConsumptionQuantity)
    {
        _annualStrawConsumption += strawConsumptionQuantity;
    }
    
#ifdef _LOG
    void DairyFarm::increaseCalvesSoldOfTheDay(unsigned int number)
    {
        _calveSoldOfTheDay += number;
    }
    
    void DairyFarm::increaseHeifersSoldOfTheDay(unsigned int number)
    {
        _heifersSoldOfTheDay += number;
    }
    
    void DairyFarm::increaseAdultsSoldOfTheDay(unsigned int number)
    {
        _adultsSoldOfTheDay += number;
    }
#endif // _LOG                

    Genetic::GeneticValue &DairyFarm::getASireGeneticValueFromCatalog(FunctionalEnumerations::Genetic::Breed br, const boost::gregorian::date &simDate, Results::DairyFarmResult* pRes)
    {
        // Get a random sire in the bale catalog, depending on the breed
        return _pMaleCatalog->getASireGeneticValue(br, simDate, pRes, getRandom());
    }
     
    // Duration since the predicted simulation date
    boost::gregorian::date_period DairyFarm::getSimulationPeriod(const boost::gregorian::date &consideredDate)
    {
        boost::gregorian::date_period period(_beginSimulationDate, consideredDate);
        return period;
    }
    
    void DairyFarm::appendAnimalToDeathOfTheDay(Animal* pAnim, FunctionalEnumerations::Population::DeathReason dr)
    {
        _mDeathOfTheDay[pAnim->getUniqueId()] = dr;
    }
    
    void DairyFarm::removeAnimalFromDeathOfTheDay(Animal* pAnim)
    {
        auto it = _mDeathOfTheDay.find(pAnim->getUniqueId());
        if (it != _mDeathOfTheDay.end())
        {
            _mDeathOfTheDay.erase(it);
        }
    }
    
    DairyHerd* DairyFarm::getDairyHerd()
    {
        return (DairyHerd*)_pIncludesHerd;
    }
        
    void DairyFarm::getTypeInseminationAndBreedForInsemination(unsigned int lactationRank, unsigned int inseminationRank, FunctionalEnumerations::Reproduction::TypeInseminationSemen &typeSemen, Genetic::Breed::Breed* &pBreed)
    {
        FunctionalEnumerations::Reproduction::TypeInseminationSemen basicTypeSemen;
        FunctionalEnumerations::Genetic::Breed  basicBreedSemen;
        float typeRatio, breedRatio;
        getInseminationParameters(lactationRank, inseminationRank, basicTypeSemen, typeRatio, basicBreedSemen, breedRatio);

        // Bernoulli choice for semen type
        if (getRandom().ran_bernoulli(typeRatio))
        {
            typeSemen = basicTypeSemen; // Specific indsemination type
        }
        else
        {
            typeSemen = FunctionalConstants::Reproduction::COMPLEMENT_INSEMINATION_TYPE; // Constant default insemination type
        }
        // Bernoulli choice for breed
        if (getRandom().ran_bernoulli(breedRatio))
        {
            pBreed = getBreed(basicBreedSemen); // Specific breed
        }
        else
        {
            pBreed = getInitialBreed(); // Intial herd breed
        }
    }

    void DairyFarm::getTypeInseminationAndBreedForBoughtHeifer(FunctionalEnumerations::Reproduction::TypeInseminationSemen &semendType, Genetic::Breed::Breed* &pInseminationBreed)
    {
        getTypeInseminationAndBreedForInsemination(0, 0, semendType, pInseminationBreed);
    }
    
    unsigned int DairyFarm::getWeaningAge()
    {
        return getOriginDairyFarmParameterInfo().feedingPlan.weaningAge;
    }
    
    // Feed for calves
    float DairyFarm::getCalfMilkQuantity()
    {
        return getOriginDairyFarmParameterInfo().feedingPlan.calfMilkQuantity;
    }
    
    // Feed for calves
    FunctionalEnumerations::Feeding::CalfMilkType DairyFarm::getCalfMilkType()
    {
        return getOriginDairyFarmParameterInfo().feedingPlan.calfMilkType;
    }
            
    void DairyFarm::useUnweanedCalfMilk()
    {
        FunctionalEnumerations::Feeding::CalfMilkType type = getCalfMilkType();
        if (type == FunctionalEnumerations::Feeding::CalfMilkType::naturalMilk)
        {
            getDairyHerd()->useBulkMilkForCalf(getCalfMilkQuantity());
        }
        else if (type == FunctionalEnumerations::Feeding::CalfMilkType::discardedMilkAndNaturalMilk or type == FunctionalEnumerations::Feeding::CalfMilkType::discardedMilkAndDehydratedMilk)
        {
            float need = getCalfMilkQuantity();
            getDairyHerd()->useDiscardedMilkForCalf(need); // Use avalaible discarded milk
            if (need > 0.0f)
            {
                // We still need some
                if (type == FunctionalEnumerations::Feeding::CalfMilkType::discardedMilkAndNaturalMilk)
                {
                    getDairyHerd()->useBulkMilkForCalf(need);
                }
                else
                {
                    getDairyHerd()->useDehydratedMilkForCalf(need);
                }
            }
        }
        else if (type == FunctionalEnumerations::Feeding::CalfMilkType::dehydratedMilk)
        {
            getDairyHerd()->useDehydratedMilkForCalf(getCalfMilkQuantity());
        }
        else
        {
            std::string errorMessage = "Unknown unweaned calf milk : " + Tools::toString(type);
            throw std::runtime_error(errorMessage);
        }
    }

    void DairyFarm::useUnweanedFemaleCalfConcentrateFeed(ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &concentrateMixture)
    {
        // Concentrate
        concentrateMixture.quantity = getOriginDairyFarmParameterInfo().feedingPlan.unweanedFemaleCalfConcentrateMixture.quantity;
        concentrateMixture.typeRatios = getOriginDairyFarmParameterInfo().feedingPlan.unweanedFemaleCalfConcentrateMixture.typeRatios;
        
        // Consumption
        getDairyHerd()->useConcentrateForCalf(concentrateMixture);
    }
    
    void DairyFarm::useWeanedFemaleCalfForageConcentrateAndMineralVitaminFeed(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &forageMixture, ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &concentrateMixture, float &mineralVitamin)
    {
        // Forage
        forageMixture.quantity = getOriginDairyFarmParameterInfo().feedingPlan.weanedFemaleCalfForageMixture.quantity;
        forageMixture.typeRatios = getOriginDairyFarmParameterInfo().feedingPlan.weanedFemaleCalfForageMixture.typeRatios;
        
        // Concentrate
        concentrateMixture.quantity = getOriginDairyFarmParameterInfo().feedingPlan.weanedFemaleCalfConcentrateMixture.quantity;
        concentrateMixture.typeRatios = getOriginDairyFarmParameterInfo().feedingPlan.weanedFemaleCalfConcentrateMixture.typeRatios;
        
        // Minerals and vitamins
        mineralVitamin = getOriginDairyFarmParameterInfo().feedingPlan.weanedFemaleCalfMineralVitamins;
        
        // Consumption
        getDairyHerd()->useForageForCalf(forageMixture);
        getDairyHerd()->useConcentrateForCalf(concentrateMixture);
        getDairyHerd()->useMineralVitaminForCalf(mineralVitamin);
    }
    
    void DairyFarm::useYoungHeiferForageConcentrateAndMineralVitaminFeed(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &forageMixture, ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &concentrateMixture, float &mineralVitamin, FunctionalEnumerations::Population::Location location)
    {
        // Forage
        forageMixture = getOriginDairyFarmParameterInfo().feedingPlan.youngUnbredHeiferForageMixture.find(location)->second;
        
        // Concentrate
        concentrateMixture = getOriginDairyFarmParameterInfo().feedingPlan.youngUnbredHeiferConcentrateMixture.find(location)->second;
        
        // Minerals and vitamins
        mineralVitamin = getOriginDairyFarmParameterInfo().feedingPlan.youngUnbredHeiferMineralVitamins.find(location)->second;
        
        // Consumption
        getDairyHerd()->useForageForHeifer(forageMixture);
        getDairyHerd()->useConcentrateForHeifer(concentrateMixture);
        getDairyHerd()->useMineralVitaminForHeifer(mineralVitamin);
    }
    
    void DairyFarm::useOldHeiferForageConcentrateAndMineralVitaminFeed(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &forageMixture, ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &concentrateMixture, float &mineralVitamin, FunctionalEnumerations::Population::Location location)
    {       
        // Forage
        forageMixture = getOriginDairyFarmParameterInfo().feedingPlan.oldUnbredHeiferForageMixture.find(location)->second;
        
        // Concentrate
        concentrateMixture = getOriginDairyFarmParameterInfo().feedingPlan.oldUnbredHeiferConcentrateMixture.find(location)->second;
        
        // Minerals and vitamins
        mineralVitamin = getOriginDairyFarmParameterInfo().feedingPlan.oldUnbredHeiferMineralVitamins.find(location)->second;
        
        // Consumption
        getDairyHerd()->useForageForHeifer(forageMixture);
        getDairyHerd()->useConcentrateForHeifer(concentrateMixture);
        getDairyHerd()->useMineralVitaminForHeifer(mineralVitamin);
    }
    
    void DairyFarm::useBredHeiferForageConcentrateAndMineralVitaminFeed(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &forageMixture, ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &concentrateMixture, float &mineralVitamin, FunctionalEnumerations::Population::Location location)
    {
        // Forage
        forageMixture = getOriginDairyFarmParameterInfo().feedingPlan.bredHeiferForageMixture.find(location)->second;
        
        // Concentrate
        concentrateMixture = getOriginDairyFarmParameterInfo().feedingPlan.bredHeiferConcentrateMixture.find(location)->second;
        
        // Minerals and vitamins
        mineralVitamin = getOriginDairyFarmParameterInfo().feedingPlan.bredHeiferMineralVitamins.find(location)->second;
        
        // Consumption
        getDairyHerd()->useForageForHeifer(forageMixture);
        getDairyHerd()->useConcentrateForHeifer(concentrateMixture);
        getDairyHerd()->useMineralVitaminForHeifer(mineralVitamin);
    }
    
    void DairyFarm::useDriedCowForageConcentrateAndMineralVitaminFeed(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &forageMixture, ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &concentrateMixture, float &mineralVitamin, FunctionalEnumerations::Population::Location location)
    {
        // Forage
        forageMixture = getOriginDairyFarmParameterInfo().feedingPlan.lactationForageMixture.find(location)->second;
        forageMixture *= FunctionalConstants::Feeding::DRIED_COW_RBE_RATION_RATIO;
        if (location == FunctionalEnumerations::Population::Location::stabulation)
        {
            // Forage stabulation complement
            ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters stabulationForageComplement;
            stabulationForageComplement.quantity = FunctionalConstants::Feeding::DRIED_COW_FORAGE_INDOOR_COMPLEMENT_QUANTITY;
            stabulationForageComplement.typeRatios = FunctionalConstants::Feeding::DRIED_COW_FORAGE_INDOOR_COMPLEMENT_TYPE;
            forageMixture += stabulationForageComplement;
        }
        
        // Concentrate
        concentrateMixture = getOriginDairyFarmParameterInfo().feedingPlan.lactationConcentrateMixture.find(location)->second;
        concentrateMixture *= FunctionalConstants::Feeding::DRIED_COW_RBE_RATION_RATIO;
        
        // Minerals and vitamins
        mineralVitamin = getOriginDairyFarmParameterInfo().feedingPlan.lactationPeriodMineralVitamins.find(location)->second;
        mineralVitamin *= FunctionalConstants::Feeding::DRIED_COW_RBE_RATION_RATIO;
        
        // Consumption
        getDairyHerd()->useConcentrateForCow(concentrateMixture);
        getDairyHerd()->useForageForCow(forageMixture);
        getDairyHerd()->useMineralVitaminForCow(mineralVitamin);
    }
    
    void DairyFarm::usePeriParturientCowForageConcentrateAndMineralVitaminFeed(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &forageMixture, ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &concentrateMixture, float &mineralVitamin, bool nulliparous, FunctionalEnumerations::Population::Location location)
    {
        // Forage
        forageMixture = getOriginDairyFarmParameterInfo().feedingPlan.lactationForageMixture.find(location)->second;
        forageMixture *= FunctionalConstants::Feeding::DRIED_COW_RBE_RATION_RATIO;
        if (location == FunctionalEnumerations::Population::Location::stabulation)
        {
            // Forage stabulation complement
            ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters stabulationForageComplement;
            stabulationForageComplement.quantity = FunctionalConstants::Feeding::DRIED_COW_FORAGE_INDOOR_COMPLEMENT_QUANTITY;
            stabulationForageComplement.typeRatios = FunctionalConstants::Feeding::DRIED_COW_FORAGE_INDOOR_COMPLEMENT_TYPE;
            forageMixture += stabulationForageComplement;
        }
        
        // Concentrate
        concentrateMixture = getOriginDairyFarmParameterInfo().feedingPlan.lactationConcentrateMixture.find(location)->second;
        concentrateMixture *= FunctionalConstants::Feeding::DRIED_COW_RBE_RATION_RATIO;
        ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters concentrateComplement;
        concentrateComplement.quantity = FunctionalConstants::Feeding::PERIPARTUM_CONCENTRATE_COMPLEMENT_FEDDING_QUANTITY;
        concentrateComplement.typeRatios = FunctionalConstants::Feeding::PERIPARTUM_CONCENTRATE_COMPLEMENT_FEDDING_TYPE;
        concentrateMixture += concentrateComplement;
        
        // Minerals and vitamins
        mineralVitamin = getOriginDairyFarmParameterInfo().feedingPlan.lactationPeriodMineralVitamins.find(location)->second;
        mineralVitamin *= FunctionalConstants::Feeding::DRIED_COW_RBE_RATION_RATIO;
        
        // Nulliparous ajustments
        if (nulliparous)
        {
            forageMixture *= FunctionalConstants::Feeding::PERI_PARTUM_NULLIPARE_FEEDING_RATIO;
            concentrateMixture *= FunctionalConstants::Feeding::PERI_PARTUM_NULLIPARE_FEEDING_RATIO;
            mineralVitamin *= FunctionalConstants::Feeding::PERI_PARTUM_NULLIPARE_FEEDING_RATIO;            
        }
        
        // Consumption
        getDairyHerd()->useConcentrateForCow(concentrateMixture);
        getDairyHerd()->useForageForCow(forageMixture);
        getDairyHerd()->useMineralVitaminForCow(mineralVitamin);
    }
    
    void DairyFarm::useLactatingCowForageConcentrateAndMineralVitaminFeed(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &forageMixture, ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &concentrateMixture, float &mineralVitamin, bool primiparous, float dailyProduct, FunctionalEnumerations::Population::Location location, float feedQuantityNotConsumedDueToMilkQuantityLossDueToDisease)
    {
        // Forage
        forageMixture = getOriginDairyFarmParameterInfo().feedingPlan.lactationForageMixture.find(location)->second;
        
        // Concentrate
        concentrateMixture = getOriginDairyFarmParameterInfo().feedingPlan.lactationConcentrateMixture.find(location)->second;
        if (dailyProduct > getOriginDairyFarmParameterInfo().feedingPlan.RBE_MilkProduction)
        {
            // Need some product concentrate
            ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters productConcentrateMixture = getOriginDairyFarmParameterInfo().feedingPlan.productConcentrateMixture;
            productConcentrateMixture.quantity = (dailyProduct - getOriginDairyFarmParameterInfo().feedingPlan.RBE_MilkProduction)/getOriginDairyFarmParameterInfo().feedingPlan.gainForOneConcentrateKilo;
            concentrateMixture += productConcentrateMixture;
        }
        
        // Minerals and vitamins
        mineralVitamin = getOriginDairyFarmParameterInfo().feedingPlan.lactationPeriodMineralVitamins.find(location)->second;

        // Primiparous ajustments
        if (primiparous)
        {
            forageMixture *= FunctionalConstants::Feeding::PRIMIPARE_RBE_RATIO;
            concentrateMixture *= FunctionalConstants::Feeding::PRIMIPARE_RBE_RATIO;
            mineralVitamin *= FunctionalConstants::Feeding::PRIMIPARE_RBE_RATIO;            
        }
        
        // Milk production loss due to diseases ajustments
        if (feedQuantityNotConsumedDueToMilkQuantityLossDueToDisease > 0.0f)
        {
            // Milk production loss due to disease
            float totalQuantity = forageMixture.quantity + concentrateMixture.quantity;
            forageMixture -= (feedQuantityNotConsumedDueToMilkQuantityLossDueToDisease * (forageMixture.quantity / totalQuantity));
            concentrateMixture -= (feedQuantityNotConsumedDueToMilkQuantityLossDueToDisease * (concentrateMixture.quantity / totalQuantity));
            assert (forageMixture.quantity >= 0.0f and concentrateMixture.quantity >= 0.0f);
        }
        
        // Consumption
        getDairyHerd()->useForageForCow(forageMixture);
        getDairyHerd()->useConcentrateForCow(concentrateMixture);
        getDairyHerd()->useMineralVitaminForCow(mineralVitamin);
    }

    void DairyFarm::addMove(Population::Move* pMove)
    {
        _vpHadMove.push_back(pMove);
    }
    
    void DairyFarm::clearAllMoves()
    {
       for (unsigned int moveInd = 0; moveInd < _vpHadMove.size(); moveInd++)
       {
           delete _vpHadMove[moveInd];
       }
       _vpHadMove.clear();
    }
   
    unsigned int DairyFarm::getAnimalNumber()
    {
        return _pIncludesHerd->getAnimalNumber();
    }
        
#ifdef _LOG
    // Publishing the final results (at the end of a run)
    void DairyFarm::closeResults()
    {
        // We ask to herd to put the information in the result
        _pIncludesHerd->closeResults(_endSimulationDate);
    }
#endif // _LOG

    unsigned int DairyFarm::getAgeToBreedingDecision()
    {
        return getOriginDairyFarmParameterInfo().ageToBreedingDecision;
    }

    unsigned int DairyFarm::getBreedingDelayAfterCalvingDecision()
    {
        return getOriginDairyFarmParameterInfo().breedingDelayAfterCalvingDecision;
    }
    
    unsigned int DairyFarm::getNumberOfDaysAfterAdultBreedingDecisionForStopInseminationDecision()
    {
        return getOriginDairyFarmParameterInfo().numberOfDaysAfterAdultBreedingDecisionForStopInseminationDecision;
    }
    
    unsigned int DairyFarm::getNumberOfDaysAfterAdultBreedingDecisionForStopFirstInseminationDecision()
    {
        return getOriginDairyFarmParameterInfo().numberOfDaysAfterAdultBreedingDecisionForStopFirstInseminationDecision;
    }

    unsigned int DairyFarm::getNumberOfDaysAfterHeiferBreedingDecisionForStopInseminationDecision()
    {
        return getOriginDairyFarmParameterInfo().numberOfDaysAfterHeiferBreedingDecisionForStopInseminationDecision;
    }
    
    unsigned int DairyFarm::getNumberOfDaysAfterHeiferBreedingDecisionForStopFirstInseminationDecision()
    {
        return getOriginDairyFarmParameterInfo().numberOfDaysAfterHeiferBreedingDecisionForStopFirstInseminationDecision;
    }

    bool DairyFarm::calvingIsAllowedDuringMonth(FunctionalEnumerations::Global::Month theMonth)
    {
        return getCalvingGroupingStrategy()->calvingIsAllowedDuringMonth(theMonth);
    }

    float DairyFarm::getFertilityFactor()
    {
        return getOriginDairyFarmParameterInfo().fertilityFactor;
    }
    
    FunctionalEnumerations::Lactation::MilkingFrequency &DairyFarm::getMilkingFrequency()
    {
        return getOriginDairyFarmParameterInfo().milkingFrequency;
    }
    
//    float DairyFarm::getMinimumMilkQuantityForRentability()
//    {
//        return getDairyHerd()->getMinimumMilkQuantityForRentability();
//    }
    
//    float DairyFarm::getMinimumMilkQuantityForCulling()
//    {
//        return _longLactationMilkQuantityLevelBeforeCulling;
//    }
//    
    unsigned int &DairyFarm::getDriedPeriodDuration()
    {
        return getOriginDairyFarmParameterInfo().driedPeriodDuration;
    }

    unsigned int &DairyFarm::getDayBeforeDeliverMilk()
    {
        return getOriginDairyFarmParameterInfo().dayBeforeDeliverMilk;
    }
    
    std::pair<unsigned int, FunctionalEnumerations::Global::Month> &DairyFarm::getYoungHeiferBeginPastureDate()
    {
        return getOriginDairyFarmParameterInfo().youngHeiferBeginPastureDate;
    }
    
    std::pair<unsigned int, FunctionalEnumerations::Global::Month> &DairyFarm::getYoungHeiferEndPastureDate()
    {
        return getOriginDairyFarmParameterInfo().youngHeiferEndPastureDate;
    }
    
    std::pair<unsigned int, FunctionalEnumerations::Global::Month> &DairyFarm::getOtherHeiferAndDriedCowBeginPastureDate()
    {
        return getOriginDairyFarmParameterInfo().otherHeiferAndDriedCowBeginPastureDate;
    }
    
    std::pair<unsigned int, FunctionalEnumerations::Global::Month> &DairyFarm::getOtherHeiferAndDriedCowEndPastureDate()
    {
        return getOriginDairyFarmParameterInfo().otherHeiferAndDriedCowEndPastureDate;
    }
    
    std::pair<unsigned int, FunctionalEnumerations::Global::Month> &DairyFarm::getLactatingCowBeginFullPastureDate()
    {
        return getOriginDairyFarmParameterInfo().lactatingCowBeginFullPastureDate;
    }
    
    std::pair<unsigned int, FunctionalEnumerations::Global::Month> &DairyFarm::getLactatingCowEndFullPastureDate()
    {
        return getOriginDairyFarmParameterInfo().lactatingCowEndFullPastureDate;
    }
 
    std::pair<unsigned int, FunctionalEnumerations::Global::Month> &DairyFarm::getLactatingCowBeginStabulationDate()
    {
        return getOriginDairyFarmParameterInfo().lactatingCowBeginStabulationDate;
    }
    
    std::pair<unsigned int, FunctionalEnumerations::Global::Month> &DairyFarm::getLactatingCowEndStabulationDate()
    {
        return getOriginDairyFarmParameterInfo().lactatingCowEndStabulationDate;
    }
 
    int DairyFarm::calculateMissingHeifers(const boost::gregorian::date &simDate)
    {
        return _pHasCalvingGroupingStrategy->calculateMissingHeifers(simDate);
    }
    
    Population::Strategies::CalvingGroupingStrategy* DairyFarm::getCalvingGroupingStrategy()
    {
        return _pHasCalvingGroupingStrategy;
    }
    
    void DairyFarm::setCullingAnticipationValue(float val)
    {
        _cullingAnticipationValue = val;
    }
        
    float DairyFarm::getCullingAnticipationValue()
    {
        return _cullingAnticipationValue;
    }

    bool DairyFarm::G1SeverityMastitisIsDetected()
    {
        return getRandom().ran_bernoulli(getOriginDairyFarmParameterInfo().G1MastitisDetectionSensibility);
    }
    
    ExchangeInfoStructures::Parameters::TreatmentTypeParameters &DairyFarm::getMastitisTreatmentType(FunctionalEnumerations::Health::MastitisRiskPeriodType periodType, FunctionalEnumerations::Health::BacteriumType bacterium)
    {
        // We don't care of the cost and, so of the severity
        if (periodType == FunctionalEnumerations::Health::MastitisRiskPeriodType::periPartumAndLactationBegin or periodType == FunctionalEnumerations::Health::MastitisRiskPeriodType::lactation)
        {
            return getOriginDairyFarmParameterInfo().mastitisTreatmentPlan.mastitisLactationTreatmentTypes.find(bacterium)->second;
        }
        else if (periodType == FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingOff or periodType == FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingInvoluted)
        {
            return getOriginDairyFarmParameterInfo().mastitisTreatmentPlan.mastitisDryTreatmentTypes.find(bacterium)->second;   
        }
        else
        {
            throw std::runtime_error("bad periodType : " + periodType);
        }
    }
    
    bool DairyFarm::hasActiveVetCareContract()
    {
        return _hasActiveVetCareContract;
    }
    
    bool DairyFarm::hasVetReproductionContract()
    {
        return getOriginDairyFarmParameterInfo().vetReproductionContract;
    }
        
    ExchangeInfoStructures::Parameters::TreatmentTypeParameters &DairyFarm::getMastitisTreatmentType(FunctionalEnumerations::Health::MastitisRiskPeriodType periodType, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity, float &cost)
    {
        if (periodType == FunctionalEnumerations::Health::MastitisRiskPeriodType::periPartumAndLactationBegin or periodType == FunctionalEnumerations::Health::MastitisRiskPeriodType::lactation)
        {
            if (severity == FunctionalEnumerations::Health::MastitisSeverity::G3)
            {
                if (hasActiveVetCareContract())
                {
                    cost = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_G3_MASTITIS_LACTATION_WITH_CONTRACT_TREATMENT_COST);
                }
                else
                {
                    cost = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_G3_MASTITIS_LACTATION_WITHOUT_CONTRACT_TREATMENT_COST);
                }
            }
            else if (severity == FunctionalEnumerations::Health::MastitisSeverity::G2)
            {
                if (hasActiveVetCareContract())
                {
                    cost = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_G2_MASTITIS_LACTATION_WITH_CONTRACT_TREATMENT_COST);
                }
                else
                {
                    cost = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_G2_MASTITIS_LACTATION_WITHOUT_CONTRACT_TREATMENT_COST);
                }
            }
            else // (severity == G1)
            {
                if (hasActiveVetCareContract())
                {
                    cost = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_G1_MASTITIS_LACTATION_WITH_CONTRACT_TREATMENT_COST);
                }
                else
                {
                    cost = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_G1_MASTITIS_LACTATION_WITHOUT_CONTRACT_TREATMENT_COST);
                }
            }
        }
        else if (periodType == FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingOff or periodType == FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingInvoluted)
        {
            if (hasActiveVetCareContract())
            {
                cost = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_MASTITIS_DRY_WITH_CONTRACT_TREATMENT_COST);
            }
            else
            {
                cost = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_MASTITIS_DRY_WITHOUT_CONTRACT_TREATMENT_COST);
            }
        }
        else
        {
            throw std::runtime_error("bad periodType : " + periodType);
        }
        return getMastitisTreatmentType(periodType, bacterium);
    }
    
    ExchangeInfoStructures::Parameters::TreatmentTypeParameters &DairyFarm::getPreventiveDryOffMastitisTreatmentType(float &cost)
    {
        if (hasActiveVetCareContract())
        {
            cost = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_MASTITIS_DRY_WITH_CONTRACT_TREATMENT_COST);
        }
        else
        {
            cost = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_MASTITIS_DRY_WITHOUT_CONTRACT_TREATMENT_COST);
        }
        return getMastitisTreatmentType(FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingOff, FunctionalEnumerations::Health::BacteriumType::StaphA); // We don't care for the bacterium for a preventive traitment
    }
    
    ExchangeInfoStructures::Parameters::TreatmentTypeParameters &DairyFarm::givePreventiveDryOffMastitisTreatmentType(
#ifdef _LOG
                                    const boost::gregorian::date &date 
#endif // _LOG        
                                    )
    {
        float cost = 0.0f;
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &mt = getPreventiveDryOffMastitisTreatmentType(cost);
        getDairyHerd()->increasePreventiveMastitisFarmerTreatmentCount();
        getDairyHerd()->increaseScheduledFarmerActivityCount(1);
        getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    date, mt.name, 
#endif // _LOG                
                                    cost, getDairyHerd()->getMastitisHealthCostsForUpdate());
#ifdef _LOG
//            addManagedHealthCost(cost); // add to the annual managed health costs
#endif // _LOG         
        return mt;
    }
    
    ExchangeInfoStructures::Parameters::TreatmentTypeParameters &DairyFarm::giveMastitisTreatment(
#ifdef _LOG
                                    const boost::gregorian::date &date, 
#endif // _LOG        
                                   FunctionalEnumerations::Health::MastitisRiskPeriodType periodType, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity)
    {
        // Always for clinical reason
        float cost = 0.0f;
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &mt = getMastitisTreatmentType(periodType, bacterium, severity, cost);
        getDairyHerd()->increaseUnscheduledFarmerActivityCount();
        if (severity == FunctionalEnumerations::Health::MastitisSeverity::G3)
        {
            getDairyHerd()->increaseCurativeClinicalMastitisVetTreatmentCount();
            getDairyHerd()->increaseUnscheduledVetActivityCount();
            getDairyHerd()->increaseUnscheduledCowVetCareCount();
        }
        else if (severity == FunctionalEnumerations::Health::MastitisSeverity::G0)
        {
            getDairyHerd()->increaseCurativeSubclinicalMastitisFarmerTreatmentCount();
        }
        else // (G1 to G2)
        {
            getDairyHerd()->increaseCurativeClinicalMastitisFarmerTreatmentCount();
        }
        getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    date, mt.name, 
#endif // _LOG                
                                    cost, getDairyHerd()->getMastitisHealthCostsForUpdate());
#ifdef _LOG
//            addManagedHealthCost(cost); // add to the annual managed health costs
#endif // _LOG         
        return mt;
    }

    float DairyFarm::getSaisonalityFactor(const boost::gregorian::date &date, FunctionalEnumerations::Health::BacteriumType bacterium)
    {
        return (getOriginDairyFarmParameterInfo().mastitisSaisonalityProbabilityFactor.find(bacterium)->second).find((FunctionalEnumerations::Global::Month)date.month().as_number())->second;
    }
    
    float DairyFarm::getIncidencePart(FunctionalEnumerations::Health::BacteriumType bacterium)
    {
        return (getOriginDairyFarmParameterInfo().bacteriumIncidencePart.find(bacterium)->second);
    }
    
    float DairyFarm::getOriginMastitisPreventionFactor()
    {
        return _originMastitisPreventionFactor;
    }
    
    float DairyFarm::getOriginKetosisPreventionFactor()
    {
        return getOriginDairyFarmParameterInfo().ketosisIncidencePreventionFactor;
    }
    
    float DairyFarm::getCurrentMastitisPreventionFactor()
    {
        return _currentMastitisPreventionFactor
                //* 3.0222f
                ;
    }
    
    float DairyFarm::getCurrentKetosisPreventionFactor()
    {
        return _currentKetosisPreventionFactor
                //* 3.0222f
                ;
    }
    
    bool DairyFarm::useMonensinBolusPreventiveTreatment()
    {
        return getOriginDairyFarmParameterInfo().monensinBolusUsage;
    }
    
    bool DairyFarm::useCetoDetect()
    {
        return getOriginDairyFarmParameterInfo().cetoDetectUsage;
    }
    
    bool DairyFarm::useHerdNavigator()
    {
        return getOriginDairyFarmParameterInfo().herdNavigatorOption;
    }

    unsigned int DairyFarm::getSubclinicalKetosisDuration()
    {
        return _ketosisDayDurationValuesToUse.getValue();
    }
    
    ExchangeInfoStructures::Parameters::TreatmentTypeParameters &DairyFarm::giveKetosisTreatment(
#ifdef _LOG
                                                                                                 const boost::gregorian::date &date, 
#endif // _LOG        
                                                                                                 FunctionalEnumerations::Health::KetosisSeverity severity)
    {
        getDairyHerd()->increaseUnscheduledFarmerActivityCount();
        if (severity == FunctionalEnumerations::Health::KetosisSeverity::G1Ketosis)
        {
            ExchangeInfoStructures::Parameters::TreatmentTypeParameters &mt = getOriginDairyFarmParameterInfo().ketosisTreatmentPlan.G1ketosisTreatmentType;

            // Treatment cost
            getDairyHerd()->increaseCurativeSubclinicalKetosisFarmerTreatmentCount();
            float cost;
            if (hasActiveVetCareContract())
            {
                cost = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_G1_KETOSIS_WITH_CONTRACT_TREATMENT_COST);
            }
            else
            {
                cost = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_G1_KETOSIS_WITHOUT_CONTRACT_TREATMENT_COST);
            }
            getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    date, mt.name, 
#endif // _LOG                
                                    cost, getDairyHerd()->getKetosisHealthCostsForUpdate());
#ifdef _LOG
//            addManagedHealthCost(cost); // add to the annual managed health costs
#endif // _LOG         

            return mt;
        }
        else if (severity == FunctionalEnumerations::Health::KetosisSeverity::G2Ketosis)
        {
            getDairyHerd()->increaseUnscheduledVetActivityCount();
            ExchangeInfoStructures::Parameters::TreatmentTypeParameters &mt = getOriginDairyFarmParameterInfo().ketosisTreatmentPlan.G2ketosisTreatmentType;

            // Treatment cost
            getDairyHerd()->increaseCurativeClinicalKetosisVetTreatmentCount();
            getDairyHerd()->increaseUnscheduledCowVetCareCount();
            float cost;
            if (hasActiveVetCareContract())
            {
                cost = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_G2_KETOSIS_WITH_CONTRACT_TREATMENT_COST);
            }
            else
            {
                cost = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_G2_KETOSIS_WITHOUT_CONTRACT_TREATMENT_COST);
            }
            getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    date, mt.name, 
#endif // _LOG                
                                    cost, getDairyHerd()->getKetosisHealthCostsForUpdate());
#ifdef _LOG
//            addManagedHealthCost(cost); // add to the annual managed health costs
#endif // _LOG         

            return mt;
        }
        else
        {
            throw std::runtime_error("DairyFarm::giveKetosisTreatment : not relevant severity");
        }
    }
    
    ExchangeInfoStructures::Parameters::TreatmentTypeParameters &DairyFarm::giveVetReproductionTreatment(ExchangeInfoStructures::Parameters::TreatmentTypeParameters &vrt, const std::string KEY
#ifdef _LOG
                                                                                                                  , const boost::gregorian::date &date 
#endif // _LOG        
                                                                                                                  )
    {
        // Treatment cost
        getDairyHerd()->increaseReproductionTroubleTreatmentCount();
        float cost = getCurrentAccountingModel().getCurrentPrice(KEY);
        getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                date, vrt.name, 
#endif // _LOG                
                                cost, getDairyHerd()->getReproductionCostsForUpdate());
        return vrt;
    }  
    
    ExchangeInfoStructures::Parameters::TreatmentTypeParameters &DairyFarm::giveAbnormalNoOestrusDetectionTreatment(
#ifdef _LOG
                                                                                                                  const boost::gregorian::date &date 
#endif // _LOG        
                                                                                                                  )
    {
        return giveVetReproductionTreatment(getOriginDairyFarmParameterInfo().reproductionTreatmentPlan.noObservedOestrusReproductionTreatmentType,
                                            FunctionalConstants::AccountingModel::KEY_VET_NO_OESTRUS_SEEN_TREATMENT_COST
#ifdef _LOG
                                            , date 
#endif // _LOG        
                                            );
    }
    
    ExchangeInfoStructures::Parameters::TreatmentTypeParameters &DairyFarm::giveAbnormalUnsuccessfulIATreatment(
#ifdef _LOG
                                                                                                                  const boost::gregorian::date &date 
#endif // _LOG        
                                                                                                                  )
    {
        return giveVetReproductionTreatment(getOriginDairyFarmParameterInfo().reproductionTreatmentPlan.tooMuchUnsucessfulInseminationReproductionTreatmentType,
                                            FunctionalConstants::AccountingModel::KEY_VET_UNSUCESSFUL_IA_TREATMENT_COST
#ifdef _LOG
                                            , date 
#endif // _LOG        
                                            );
    }
    
    ExchangeInfoStructures::Parameters::TreatmentTypeParameters &DairyFarm::giveNegativePregnancyDiagnosticTreatment(
#ifdef _LOG
                                                                                                                  const boost::gregorian::date &date 
#endif // _LOG        
                                                                                                                  )
    {
        return giveVetReproductionTreatment(getOriginDairyFarmParameterInfo().reproductionTreatmentPlan.negativePregnancyReproductionTreatmentType,
                                            FunctionalConstants::AccountingModel::KEY_VET_NEGATIVE_PREGNANCY_DIAGNOSTIC_TREATMENT_COST
#ifdef _LOG
                                            , date 
#endif // _LOG        
                                            );
    }
            
    ExchangeInfoStructures::Parameters::ReproductionTreatmentPlanParameters &DairyFarm::getReproductionTreatmentPlan()
    {
        return getOriginDairyFarmParameterInfo().reproductionTreatmentPlan;        
    }
    
    void DairyFarm::installFootBath(
#ifdef _LOG
                                    const boost::gregorian::date &date
#endif // _LOG
                                   )
    {
        float cost;
#ifdef _LOG
        std::string type;
#endif // _LOG
        
        if (getDairyHerd()->getMeanAdultCowCount() <= FunctionalConstants::AccountingModel::COW_NUMBER_FOR_MINIMUM_FOOT_BATH_COST)
        {
            cost = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_LAMENESS_FOOT_BATH_USAGE_MINIMUM_COST);
#ifdef _LOG
            type = FunctionalConstants::AccountingModel::MINIMUM_FOOT_BATH_USAGE_TRANSACTION_TYPE;
#endif // _LOG
        }
        else
        {
            cost = getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_LAMENESS_FOOT_BATH_USAGE_MAXIMUM_COST);
#ifdef _LOG
            type = FunctionalConstants::AccountingModel::MAXIMUM_FOOT_BATH_USAGE_TRANSACTION_TYPE;
#endif // _LOG
        }
        
        getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    date, type, 
#endif // _LOG                
                                    cost, getDairyHerd()->getFootBathCostsForUpdate());
#ifdef _LOG                   
//        addManagedHealthCost(cost); // add to the annual managed health costs
#endif // _LOG            
    }

    void DairyFarm::careCowsTodayForDetectedLamenessCase(
#ifdef _LOG                
                                    const boost::gregorian::date &date 
#endif // _LOG                
                                    )
    {
        getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                date, FunctionalConstants::AccountingModel::FOOT_TRIMMING_WORKSHOP_TRANSACTION_TYPE , 
#endif // _LOG                
                                getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_LAMENESS_FOOT_TRIMMING_WORKSHOP_COST ), getDairyHerd()->getCurativeTrimmingCostsForUpdate());
#ifdef _LOG                   
//        addManagedHealthCost(getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_LAMENESS_FOOT_TRIMMING_WORKSHOP_COST )); // add to the annual managed health costs
#endif // _LOG            
        
        // Unscheduled farmer and vet activity
        getDairyHerd()->increaseUnscheduledFarmerActivityCount();
        getDairyHerd()->increaseUnscheduledVetActivityCount();

        // Action on animals
        for (auto pAnimal : _vToCareDueToSporadicLamenessDetectionOrVerification)
        {
           ((DairyMammal*)pAnimal)->toCareIndividualTodayForDetectedLamenessCase();
        }
        _vToCareDueToSporadicLamenessDetectionOrVerification.clear();
    }

    void DairyFarm::trimCowsTodayForLamenessPrevention(
#ifdef _LOG                
                                    const boost::gregorian::date &date 
#endif // _LOG                
                                    )
    {

        if (_vToTrimForLamenessPrevention.size() > 0)
        {
            // Scheduled farmer and vet activity
            getDairyHerd()->increaseScheduledFarmerActivityCount(1);
            getDairyHerd()->increaseScheduledVetActivityCount(1);
            
            // Constant cost
            getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    date, FunctionalConstants::AccountingModel::FOOT_TRIMMING_WORKSHOP_TRANSACTION_TYPE , 
#endif // _LOG                
                                    getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_LAMENESS_FOOT_TRIMMING_WORKSHOP_COST ), getDairyHerd()->getPreventiveTrimmingCostsForUpdate());
#ifdef _LOG                   
//            addManagedHealthCost(getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_LAMENESS_FOOT_TRIMMING_WORKSHOP_COST )); // add to the annual managed health costs
#endif // _LOG            
            // Action on animals
            for (auto pAnimal : _vToTrimForLamenessPrevention)
            {
               ((DairyMammal*)pAnimal)->toTrimCollectiveTodayForLamenessPrevention();
            }
            _vToTrimForLamenessPrevention.clear();
        }
    }
    
    FunctionalEnumerations::Health::LamenessDetectionMode DairyFarm::getLamenessDetectionMode()
    {
        return getOriginDairyFarmParameterInfo().lamenessDetectionMode;
    }

    float DairyFarm::getCurrentLamenessPreventionFactor(FunctionalEnumerations::Health::LamenessInfectiousStateType type)
    {
        return getOriginDairyFarmParameterInfo().lamenessIncidencePreventionFactor.find(type)->second;
    }
    
    unsigned int DairyFarm::getFootBathFrequencyInDays(FunctionalEnumerations::Population::Location location)
    {
        return getOriginDairyFarmParameterInfo().footBathFrequency.find(location)->second;
    }
    
    unsigned int DairyFarm::getFootBathProtectionDurationForLamenessInDays()
    {
        return getOriginDairyFarmParameterInfo().footBathProtectionDurationForLameness;
    }
    
    bool DairyFarm::useFootBath()
    {
        return getOriginDairyFarmParameterInfo().footBath;
    }
   
    FunctionalEnumerations::Health::LamenessPreventiveTrimmingOption DairyFarm::getTrimmingOption()
    {
        return getOriginDairyFarmParameterInfo().preventiveTrimmingOption;
    }
   
    unsigned int DairyFarm::getCowCountForSmallTrimmingOrCareGroup()
    {
        return getOriginDairyFarmParameterInfo().cowCountForSmallTrimmingOrCareGroup;
    }
    
    std::vector<DataStructures::Animal*> &DairyFarm::getToCareDueToSporadicLamenessDetectionOrVerificationGroup()
    {
        return _vToCareDueToSporadicLamenessDetectionOrVerification;    
    }
    
    std::vector<DataStructures::Animal*> &DairyFarm::getToTrimForLamenessPreventionGroup()
    {
        return _vToTrimForLamenessPrevention;
    }

    void DairyFarm::getLamenessG1InfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability)
    {
        getLamenessTreatmentInformations(getOriginDairyFarmParameterInfo().lamenessTreatmentPlan.G1infectiousLamenessTreatmentType, minEffectDelay, maxEffectDelay, firstCaseHealingProbability, otherCaseHealingProbability);
    }
      
    void DairyFarm::getLamenessG1NonInfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability)
    {
        getLamenessTreatmentInformations(getOriginDairyFarmParameterInfo().lamenessTreatmentPlan.G1nonInfectiousLamenessTreatmentType, minEffectDelay, maxEffectDelay, firstCaseHealingProbability, otherCaseHealingProbability);
    }
      
    void DairyFarm::getLamenessG2InfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability)
    {
        getLamenessTreatmentInformations(getOriginDairyFarmParameterInfo().lamenessTreatmentPlan.G2infectiousLamenessTreatmentType, minEffectDelay, maxEffectDelay, firstCaseHealingProbability, otherCaseHealingProbability);
    }
      
    void DairyFarm::getLamenessG2NonInfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability)
    {
        getLamenessTreatmentInformations(getOriginDairyFarmParameterInfo().lamenessTreatmentPlan.G2nonInfectiousLamenessTreatmentType, minEffectDelay, maxEffectDelay, firstCaseHealingProbability, otherCaseHealingProbability);   
    }
      
     void DairyFarm::getLamenessTreatmentInformations(ExchangeInfoStructures::Parameters::TreatmentTypeParameters &treatment, unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability)
    {
        minEffectDelay = treatment.effectDelayMeanOrMin;
        maxEffectDelay = treatment.effectDelayMax;
        firstCaseHealingProbability = treatment.successProbabilityAllCasesOrFirst;
        otherCaseHealingProbability = treatment.successProbabilitySecondAndOtherCases;
    }
     
#ifdef _LOG
    void DairyFarm::saveData(const std::string &fileName)
    {
        
        DataStructures::DairyFarm* pDairyFarm = this;
        try
        {
            //std::string strFullFileName = fileName + TechnicalConstants::XML_ARCHIVE_EXTENSION;
            std::string strFullFileName = fileName + TechnicalConstants::BINARY_ARCHIVE_EXTENSION;
            std::ofstream ofs(strFullFileName, std::ios_base::binary); // Create the file stream
            {
                //boost::archive::xml_oarchive oa(ofs); // Declare the xml archive            
                boost::archive::binary_oarchive oa(ofs); // Declare the binary archive            
                oa << BOOST_SERIALIZATION_NVP(pDairyFarm);
            }
            ofs.close(); // Closing the file
        }
        catch (std::exception &e)
        {
            std::string displayMessage("->Save exception: ");
            displayMessage += e.what();
            Console::Display::displayLineInConsole(displayMessage);
        }
    } 
#endif // _LOG       



} /* End of namespace DataStructures */