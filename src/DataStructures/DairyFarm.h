#ifndef DataStructures_DairyFarm_h
#define DataStructures_DairyFarm_h

// standard
#include <vector>
#include <set>
#include <string>

// project
#include "../Tools/Serialization.h"
#include "../ExchangeInfoStructures/DairyFarmAccountingModelParameters.h"
#include "../ExchangeInfoStructures/LamenessTreatmentPlanParameters.h"
#include "../Genetic/GeneticValue.h"
#include "../Genetic/Breed/Breed.h"
#include "DiscreteTimeSystem.h"
#include "../ExchangeInfoStructures/DairyFarmParameters.h"
//#include "../Tools/TwoDimentionalModulationMatrix.h"

namespace Genetic
{
    class MaleCatalog;
}
namespace Results
{
    class DairyFarmResult;
}
namespace DataStructures
{
    class Herd;
    class Animal;
    class DairyHerd;
    
    namespace Population
    {
        class Move;
        namespace Strategies
        {
            class FemaleCalveManagementStrategy;
            class CalvingGroupingStrategy;
        }
    }

    /// Farm exploitation where Herd is raised and practices are set
    class DairyFarm : public DiscreteTimeSystem
    {
    private:
        DECLARE_SERIALIZE_STD_METHODS;

        std::vector<FunctionalEnumerations::Global::Month> _monthsOfTheCampaign;

        unsigned int _lastIdUnique = 0;

#ifdef _LOG
        // Moves
        unsigned int _calveSoldOfTheDay {0};
        unsigned int _calveSoldOfTheWeekNumber {0};
        unsigned int _heifersSoldOfTheDay {0};
        unsigned int _heifersSoldOfTheWeekNumber {0};
        unsigned int _adultsSoldOfTheDay {0};
        unsigned int _adultsSoldOfTheWeekNumber {0};       
        unsigned int _deathOfTheWeekNumber {0};
#endif // _LOG     
        
        boost::gregorian::date _resultBeginDate;
        
        // Lactation
        float _longLactationMilkQuantityLevelBeforeCulling;
        
        // Health :
        bool _hasActiveVetCareContract = false;
        Tools::Random::ValuesBasedOnMultinomialDistribution _ketosisDayDurationValuesToUse;
        std::vector<DataStructures::Animal*> _vToCareDueToSporadicLamenessDetectionOrVerification; // Potential animals to care for lameness
        std::vector<DataStructures::Animal*> _vToTrimForLamenessPrevention; // Potential animals to trim for lameness prevention
        float _originMastitisPreventionFactor = 1.0f;
        float _currentMastitisPreventionFactor = 1.0f;
        float _currentKetosisPreventionFactor = 1.0f;
        
//        Tools::TwoDimentionalModulationMatrix* _pMastitisPreventionModulationMatrix = nullptr; // See "DefaultFarmMastitisCalibration.xlsx" file to know how to set value
//        Tools::TwoDimentionalModulationMatrix* _pKetosisPreventionModulationMatrix = nullptr; // See "DefaultFarmMastitisCalibration.xlsx" file to know how to set value


//#ifdef _LOG
//        float _annualManagedHealthCost = 0.0f; // Currently for mastitis and ketosis costs
//#endif // _LOG      
        // Population
        std::map<FunctionalEnumerations::Genetic::Sex, std::map<FunctionalEnumerations::Genetic::BreedType,std::map<FunctionalEnumerations::Population::MortalityRiskAge, float>>> _mortalityProbaToApply = FunctionalConstants::Population::GLOBAL_MORTALITY_PROBA;
        std::map<unsigned int, FunctionalEnumerations::Population::DeathReason> _mDeathOfTheDay;
        std::vector<Population::Move*> _vpHadMove;
        Population::Strategies::FemaleCalveManagementStrategy* _pHasFemaleCalveManagementStrategy = nullptr;
        Population::Strategies::CalvingGroupingStrategy* _pHasCalvingGroupingStrategy = nullptr;
        float _cullingAnticipationValue;
        float _currentAnnualHerdRenewablePart;
        unsigned int _currentAnnualHeiferTarget = 0;
        
        // Male genetic value catalog
        Genetic::MaleCatalog* _pMaleCatalog;
        
        // Temporal values (not to serialize)
        
        // accounting
        float _annualStrawConsumption = 0.0f;
   
        // ...
    private:
        void getLamenessTreatmentInformations(ExchangeInfoStructures::Parameters::TreatmentTypeParameters &treatment, unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability);
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &giveVetReproductionTreatment(ExchangeInfoStructures::Parameters::TreatmentTypeParameters &vrt, const std::string KEY
#ifdef _LOG
                                                                                                                  , const boost::gregorian::date &date 
#endif // _LOG        
                                                                                                                  );

        float getMastitisPercentPreventionProgress();
        float getKetosisPercentPreventionProgress();
        float getAdditionalMastitisHealingIfCareVetContract();
        float getAdditionalKetosisHealingIfCareVetContract();
        float getNonManagedMortalityFactorWithVetCareContract();

        
    protected:            
        std::map<FunctionalEnumerations::Genetic::Breed, Genetic::Breed::Breed*> _mpBreed;
        DataStructures::Herd* _pIncludesHerd;

        void concrete(){}; // To allow instanciation

    public:
        void resultBegins(const boost::gregorian::date &date)override;
        void setBeginPresimulationDate(const boost::gregorian::date &beginPreDate) override;
        void setBeginSimulationDate(const boost::gregorian::date &beginDate) override;
        unsigned int getYearNumberForPresimulation() override;
        std::vector<FunctionalEnumerations::Global::Month> &getMonthsOfTheCampaign();
        DairyFarm(ExchangeInfoStructures::Parameters::DairyFarmParameters &info); // Constructor
        virtual ~DairyFarm(); // Destructor
        
        ExchangeInfoStructures::Parameters::DairyFarmParameters &getOriginDairyFarmParameterInfo();
        Results::DairyFarmResult* getCurrentResults();

        unsigned int getNewIdUnique();
        DairyHerd* getDairyHerd();

        DairyFarm(){} // serialization constructor
            
        void updateRenewalRatio();
        std::map<FunctionalEnumerations::Genetic::Sex, std::map<FunctionalEnumerations::Genetic::BreedType,std::map<FunctionalEnumerations::Population::MortalityRiskAge, float>>> &getGlobalMortalityProba();
        float getCurrentAnnualHerdRenewablePart();
        unsigned int getMaxLactationRankForEndInseminationDecision();
        float getMinMilkProductionHerdRatioForEndInseminationDecision();
        unsigned int getMaxSCCLevelForEndInseminationDecision();
        Genetic::Breed::Breed* getInitialBreed();        
        unsigned int getMeanAdultNumberTarget();
        unsigned int getAnnualHeiferTarget();
        
        ExchangeInfoStructures::Parameters::ExcludedCalvingPeriodParameters &getHeiferCalvingExclusionPeriod();

        bool hasVetReproductionContract();
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &giveAbnormalUnsuccessfulIATreatment(
#ifdef _LOG
                                                                                                         const boost::gregorian::date &date 
#endif // _LOG        
                                                                                                         );
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &giveNegativePregnancyDiagnosticTreatment(
#ifdef _LOG
                                                                                                              const boost::gregorian::date &date 
#endif // _LOG        
                                                                                                              );
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &giveAbnormalNoOestrusDetectionTreatment(
#ifdef _LOG
                                                                                                            const boost::gregorian::date &date 
#endif // _LOG        
                                                                                                            );
        ExchangeInfoStructures::Parameters::ReproductionTreatmentPlanParameters &getReproductionTreatmentPlan();

        FunctionalEnumerations::Genetic::GeneticStrategy getGeneticStrategy();
        void getInseminationParameters(unsigned int lactationRank, unsigned int inseminationRank, FunctionalEnumerations::Reproduction::TypeInseminationSemen &typeSemen, float &typeRatio, FunctionalEnumerations::Genetic::Breed &breedSemen, float &breedRatio);
        FunctionalEnumerations::Reproduction::DetectionMode getDetectionMode();
        unsigned int getAgeToBreedingDecision();
        unsigned int getBreedingDelayAfterCalvingDecision();
        unsigned int getNumberOfDaysAfterAdultBreedingDecisionForStopInseminationDecision();
        unsigned int getNumberOfDaysAfterAdultBreedingDecisionForStopFirstInseminationDecision();
        unsigned int getNumberOfDaysAfterHeiferBreedingDecisionForStopInseminationDecision();
        unsigned int getNumberOfDaysAfterHeiferBreedingDecisionForStopFirstInseminationDecision();  

        bool calvingIsAllowedDuringMonth(FunctionalEnumerations::Global::Month theMonth);
        float getFertilityFactor();
        
        FunctionalEnumerations::Lactation::MilkingFrequency &getMilkingFrequency();
        //float getMinimumMilkQuantityForRentability();
        unsigned int &getDriedPeriodDuration();
        unsigned int &getDayBeforeDeliverMilk();
        //float getMinimumMilkQuantityForCulling();
        
        boost::gregorian::date_period getSimulationPeriod(const boost::gregorian::date &consideredDate); // Period since the predicted simulation date

        // Genetic
        Genetic::Breed::Breed* getBreed(FunctionalEnumerations::Genetic::Breed theBreed);
        Genetic::GeneticValue &getASireGeneticValueFromCatalog(FunctionalEnumerations::Genetic::Breed br, const boost::gregorian::date &simDate, Results::DairyFarmResult* pRes);
        
        // Population
        std::pair<unsigned int, FunctionalEnumerations::Global::Month> &getYoungHeiferBeginPastureDate();
        std::pair<unsigned int, FunctionalEnumerations::Global::Month> &getYoungHeiferEndPastureDate();
        std::pair<unsigned int, FunctionalEnumerations::Global::Month> &getOtherHeiferAndDriedCowBeginPastureDate();
        std::pair<unsigned int, FunctionalEnumerations::Global::Month> &getOtherHeiferAndDriedCowEndPastureDate();
        std::pair<unsigned int, FunctionalEnumerations::Global::Month> &getLactatingCowBeginFullPastureDate();   
        std::pair<unsigned int, FunctionalEnumerations::Global::Month> &getLactatingCowEndFullPastureDate();
        std::pair<unsigned int, FunctionalEnumerations::Global::Month> &getLactatingCowBeginStabulationDate();
        std::pair<unsigned int, FunctionalEnumerations::Global::Month> &getLactatingCowEndStabulationDate();
        
        Population::Strategies::CalvingGroupingStrategy* getCalvingGroupingStrategy();
        int calculateMissingHeifers(const boost::gregorian::date &simDate);
        Population::Strategies::FemaleCalveManagementStrategy* getFemaleCalveManagementStrategy();
        void setCullingAnticipationValue(float val);
        float getCullingAnticipationValue();

        // Health
        bool hasActiveVetCareContract();

        // Mastitis
        float getOriginMastitisPreventionFactor();
        float getCurrentMastitisPreventionFactor();
        bool G1SeverityMastitisIsDetected();
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &getMastitisTreatmentType(FunctionalEnumerations::Health::MastitisRiskPeriodType periodType, FunctionalEnumerations::Health::BacteriumType bacterium);
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &getMastitisTreatmentType(FunctionalEnumerations::Health::MastitisRiskPeriodType periodType, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity, float &cost);
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &getPreventiveDryOffMastitisTreatmentType(float &cost);
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &givePreventiveDryOffMastitisTreatmentType(
#ifdef _LOG
                                    const boost::gregorian::date &date 
#endif // _LOG        
                                    );
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &giveMastitisTreatment(
#ifdef _LOG
                                    const boost::gregorian::date &date, 
#endif // _LOG        
                                    FunctionalEnumerations::Health::MastitisRiskPeriodType periodType, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity);
        float getSaisonalityFactor(const boost::gregorian::date &date, FunctionalEnumerations::Health::BacteriumType bacterium);
        float getIncidencePart(FunctionalEnumerations::Health::BacteriumType bacterium);
        //Ketosis
        float getOriginKetosisPreventionFactor();
        float getCurrentKetosisPreventionFactor();
        bool useMonensinBolusPreventiveTreatment();
        bool useCetoDetect();
        bool useHerdNavigator();
        unsigned int getSubclinicalKetosisDuration();
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &giveKetosisTreatment(
#ifdef _LOG
                                                                                          const boost::gregorian::date &date, 
#endif // _LOG        
                                                                                          FunctionalEnumerations::Health::KetosisSeverity severity);
        
        // Lameness
        void installFootBath(
#ifdef _LOG
                             const boost::gregorian::date &date
#endif // _LOG
                            );
        void careCowsTodayForDetectedLamenessCase(
#ifdef _LOG                
                                    const boost::gregorian::date &date 
#endif // _LOG                
                                    );
        void trimCowsTodayForLamenessPrevention(
#ifdef _LOG                
                                    const boost::gregorian::date &date 
#endif // _LOG                
                                    );
        FunctionalEnumerations::Health::LamenessDetectionMode getLamenessDetectionMode();
        float getCurrentLamenessPreventionFactor(FunctionalEnumerations::Health::LamenessInfectiousStateType type);
        unsigned int getFootBathFrequencyInDays(FunctionalEnumerations::Population::Location location);
        unsigned int getFootBathProtectionDurationForLamenessInDays();
        bool useFootBath();
        FunctionalEnumerations::Health::LamenessPreventiveTrimmingOption getTrimmingOption();
        unsigned int getCowCountForSmallTrimmingOrCareGroup();
        std::vector<DataStructures::Animal*> &getToCareDueToSporadicLamenessDetectionOrVerificationGroup();
        std::vector<DataStructures::Animal*> &getToTrimForLamenessPreventionGroup();
        void getLamenessG1InfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability);
        void getLamenessG1NonInfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability);
        void getLamenessG2InfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability);
        void getLamenessG2NonInfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability);

        // Feeding
        unsigned int getWeaningAge();
        float getCalfMilkQuantity();
        FunctionalEnumerations::Feeding::CalfMilkType getCalfMilkType(); // Feed for calves
        void useUnweanedCalfMilk();
        void useUnweanedFemaleCalfConcentrateFeed(ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &concentrateMixture);
        void useWeanedFemaleCalfForageConcentrateAndMineralVitaminFeed(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &forageMixture, ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &concentrateMixture, float &mineralVitamin);
        void useYoungHeiferForageConcentrateAndMineralVitaminFeed(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &forageMixture, ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &concentrateMixture, float &mineralVitamin, FunctionalEnumerations::Population::Location location);
        void useOldHeiferForageConcentrateAndMineralVitaminFeed(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &forageMixture, ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &concentrateMixture, float &mineralVitamin, FunctionalEnumerations::Population::Location location);
        void useBredHeiferForageConcentrateAndMineralVitaminFeed(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &forageMixture, ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &concentrateMixture, float &mineralVitamin, FunctionalEnumerations::Population::Location location);
        void useDriedCowForageConcentrateAndMineralVitaminFeed(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &forageMixture, ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &concentrateMixture, float &mineralVitamin, FunctionalEnumerations::Population::Location location);
        void usePeriParturientCowForageConcentrateAndMineralVitaminFeed(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &forageMixture, ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &concentrateMixture, float &mineralVitamin, bool nulliparous, FunctionalEnumerations::Population::Location location);
        void useLactatingCowForageConcentrateAndMineralVitaminFeed(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &forageMixture, ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &concentrateMixture, float &mineralVitamin, bool primiparous, float dailyProduct, FunctionalEnumerations::Population::Location location, float feedQuantityNotConsumedDueToMilkQuantityLossDueToDisease);

        // Accouting
        ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &getCurrentAccountingModel();
        float getCalfDayStrawConsumption();
        float getHeiferDayStrawConsumption();
        float getAdultDayStrawConsumption();
        void addStrawConsomption(float strawConsumptionQuantity);
        
        bool floorIsSlipery();
        unsigned int getAnimalNumber();
        void addMove(Population::Move* pMove);
        void clearAllMoves();
        void appendAnimalToDeathOfTheDay(Animal* pAnim, FunctionalEnumerations::Population::DeathReason dr);
        void removeAnimalFromDeathOfTheDay(Animal* pAnim);
        void getTypeInseminationAndBreedForInsemination(unsigned int lactationRank, unsigned int inseminationRank, FunctionalEnumerations::Reproduction::TypeInseminationSemen &typeSemen, Genetic::Breed::Breed* &pBreed);
        void getTypeInseminationAndBreedForBoughtHeifer(FunctionalEnumerations::Reproduction::TypeInseminationSemen &semendType, Genetic::Breed::Breed* &pInseminationBreed);
        float getVetCareContractCost(
#ifdef _LOG              
        const boost::gregorian::date &simDate
#endif // _LOG                
        );
        float getVetReproductionContractCost(
#ifdef _LOG              
        const boost::gregorian::date &simDate
#endif // _LOG                
        );
        float getFootBathCost(
#ifdef _LOG              
        const boost::gregorian::date &simDate
#endif // _LOG                
        );
#ifdef _LOG
//        void addManagedHealthCost(float theCost);
        void increaseCalvesSoldOfTheDay(unsigned int number);
        void increaseHeifersSoldOfTheDay(unsigned int number);
        void increaseAdultsSoldOfTheDay(unsigned int number);
#endif // _LOG        
                        
        virtual void progress(const boost::gregorian::date &simDate) override; // progress of duration time and unit
#ifdef _LOG
        void closeResults(); // optionnal publishing the final results (at the end of a run)
        void saveData(const std::string &fileName);
#endif // _LOG       
        
   }; /* End of class DairyFarm */
} /* End of namespace DataStructures */

#endif // DataStructures_DairyFarm_h
