#include "DairyHerd.h"

// Internal
// -------
#include "DairyMammal.h"
#include "DairyFarm.h"
#include "./Population/Batch/Batch.h"
#include "./Population/Batch/ToSaleCalvesBatch.h"
#include "./Population/Batch/FemaleCalvesBatch.h"
#include "./Population/Batch/WeanedCalvesBatch.h"
#include "./Population/Batch/YoungUnbredHeifersBatch.h"
#include "./Population/Batch/OldUnbredHeifersBatch.h"
#include "./Population/Batch/BredHeifersBatch.h"
#include "./Population/Batch/PeriParturientFemalesBatch.h"
#include "./Population/Batch/DriedCowsAndPregnantHeifersBatch.h"
#include "./Population/Batch/LactatingCowsBatch.h"
#include "../Lactation/DairyHerdLactationStates/DairyHerdLactationState.h"
#include "../Tools/Display.h"

// Tools
// -----
#include "../Tools/Random.h"

// Module dependencies
// -------------------
// Reproduction

// Lactation
#include "../Lactation/LactationManagement.h"

// Genetic

// Population
#include "./Population/PopulationManagement.h"
#include "./Population/Moves/Cull.h"
#include "./Population/Moves/Purchase.h"

// Miscellaneous
#include "../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(DataStructures::DairyHerd) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Herd); \
    ar & BOOST_SERIALIZATION_NVP(_minimumMilkQuantityForRentability); \
    ar & BOOST_SERIALIZATION_NVP(_milkProductionObjectiveFactor); \
       
    IMPLEMENT_SERIALIZE_STD_METHODS(DairyHerd);


    // default constructor
    DairyHerd::DairyHerd(DairyFarm* pFarmExpl, Genetic::Breed::Breed* pInitialBreed, ExchangeInfoStructures::MilkProductCharacteristic &herdProductionDeltaObjective, float minimumMilkQuantityFactorForRentability): Herd(pFarmExpl)
    {              
        // Calculation of the factor to take in account of the breed capability and the production objective
        float breedMilkQuantityPerCowObjective = pInitialBreed->getBreedMeanProductions().find(FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait)->second;
        float globalMilkQuantityPerCowObjective = breedMilkQuantityPerCowObjective + herdProductionDeltaObjective.quantity;
        _milkProductionObjectiveFactor.quantity = globalMilkQuantityPerCowObjective / pInitialBreed->getBreedMeanProductions().find(FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait)->second;
        _milkProductionObjectiveFactor.TB = (pInitialBreed->getBreedMeanProductions().find(FunctionalEnumerations::Genetic::PhenotypicCharacter::TB)->second + herdProductionDeltaObjective.TB) / pInitialBreed->getBreedMeanProductions().find(FunctionalEnumerations::Genetic::PhenotypicCharacter::TB)->second;
        _milkProductionObjectiveFactor.TP = (pInitialBreed->getBreedMeanProductions().find(FunctionalEnumerations::Genetic::PhenotypicCharacter::TP)->second + herdProductionDeltaObjective.TP) / pInitialBreed->getBreedMeanProductions().find(FunctionalEnumerations::Genetic::PhenotypicCharacter::TP)->second;
        _milkProductionObjectiveFactor.SCC = 1.0f;

        // Calibrating milk production
        _milkProductionObjectiveFactor.quantity *= TechnicalConstants::MILK_PRODUCTION_QUANTITY_FACTOR_CALIBRATION;
        _milkProductionObjectiveFactor.TB *= TechnicalConstants::MILK_PRODUCTION_TB_FACTOR_CALIBRATION;
        _milkProductionObjectiveFactor.TP *= TechnicalConstants::MILK_PRODUCTION_TP_FACTOR_CALIBRATION;   
        _milkProductionObjectiveFactor.SCC *= TechnicalConstants::MILK_PRODUCTION_SCC_FACTOR_CALIBRATION;          
        
        // Rentability level :
        float milkQuantityRentabilityLevelPerCow = globalMilkQuantityPerCowObjective;
        _minimumMilkQuantityForRentability = milkQuantityRentabilityLevelPerCow * FunctionalConstants::Population::COEFFICIENT_FOR_DETERMINING_LOW_QUANTITY_FOR_RENTABILITY * minimumMilkQuantityFactorForRentability;
        //_minimumMilkQuantityForRentability = milkQuantityRentabilityLevelPerCow * 1.0f/640.0f * minimumMilkQuantityFactorForRentability;
        //_minimumMilkQuantityForRentability *=1.1f;
        
                
         
//        const float HIGH_LEVEL_MILK_PRODUCTION_CULLING = 9500.0f;
//        const float HIGH_LEVEL_MILK_PRODUCTION_RENTABILITY = 13.0f;
//        const float LOW_LEVEL_MILK_PRODUCTION_CULLING = 6000.0f;
//        const float LOW_LEVEL_MILK_PRODUCTION_RENTABILITY = 9.0f;
//        float a = (LOW_LEVEL_MILK_PRODUCTION_RENTABILITY - HIGH_LEVEL_MILK_PRODUCTION_RENTABILITY) / (LOW_LEVEL_MILK_PRODUCTION_CULLING - HIGH_LEVEL_MILK_PRODUCTION_CULLING); // a = (Y2 - Y1) / (X2 - X1)
//        float b = HIGH_LEVEL_MILK_PRODUCTION_RENTABILITY - a * HIGH_LEVEL_MILK_PRODUCTION_CULLING; // b = Y1 - aX1
//        _minimumMilkQuantityForRentability = a * globalMilkQuantityPerCowObjective + b;
//        
        
        // Batch graph
        Population::Batch* pBatch = new Population::LactatingCowsBatch(this); // 8
        _mpIncludesBatch[FunctionalEnumerations::Population::BatchType::lactatingCows] = pBatch;
        pBatch = new Population::PeriParturientFemalesBatch(this, pBatch); // 6
        _mpIncludesBatch[FunctionalEnumerations::Population::BatchType::periParturientFemales] = pBatch;
        pBatch = new Population::DriedCowsAndPregnantHeifersBatch(this, pBatch); // 7
        _mpIncludesBatch[FunctionalEnumerations::Population::BatchType::driedCowsAndPregnantHeifers] = pBatch;
        pBatch = new Population::BredHeifersBatch(this, pBatch); // 5
        _mpIncludesBatch[FunctionalEnumerations::Population::BatchType::bredHeifers] = pBatch;
        Population::Batch* pBatchOldHeifer = new Population::OldUnbredHeifersBatch(this, pBatch); // 4 : Use depending on the heifer batch management
        _mpIncludesBatch[FunctionalEnumerations::Population::BatchType::oldUnbredHeifers] = pBatchOldHeifer;
        unsigned int ageToLeaveYoungUnbredHeifersBatch = pFarmExpl->getAgeToBreedingDecision(); // Base
        if (ageToLeaveYoungUnbredHeifersBatch > FunctionalConstants::Population::AGE_TO_GO_IN_OLD_UNBRED_HEIFERS_BATCH)
        {
            // We need to go in the Old Unbred Heifers Batch before the Bred Heifer Batch
            pBatch = pBatchOldHeifer;
            ageToLeaveYoungUnbredHeifersBatch = FunctionalConstants::Population::AGE_TO_GO_IN_OLD_UNBRED_HEIFERS_BATCH;
        }
        pBatch = new Population::YoungUnbredHeifersBatch(this, pBatch, ageToLeaveYoungUnbredHeifersBatch); // 3
        _mpIncludesBatch[FunctionalEnumerations::Population::BatchType::youngUnbredHeifers] = pBatch;
        pBatch = new Population::WeanedCalvesBatch(this, pBatch); // 2
        _mpIncludesBatch[FunctionalEnumerations::Population::BatchType::weanedCalves] = pBatch;
        _mpIncludesBatch[FunctionalEnumerations::Population::BatchType::femaleCalves] = new Population::FemaleCalvesBatch(this, pBatch); // 1
        _mpIncludesBatch[FunctionalEnumerations::Population::BatchType::toSaleCalves] = new Population::ToSaleCalvesBatch(this); // 0
        
    }
    
    DairyHerd::~DairyHerd() // Destructor
    {
    }
    
    float DairyHerd::getMinimumMilkQuantityForRentability()
    {
        return _minimumMilkQuantityForRentability;
    }
    
    ExchangeInfoStructures::MilkProductCharacteristic &DairyHerd::getMilkProductionObjectiveFactor()
    {
        return _milkProductionObjectiveFactor;
    }
    
    float DairyHerd::getDehydratedMilkKgFromMilkKg(float milkKg)
    {
        return (milkKg * FunctionalConstants::Feeding::DEHYDRATED_MILK_RATIO);
    }

    void DairyHerd::progress(const boost::gregorian::date &simDate)
    {
        if (getDairyFarm()->isFirstDayOfTheCampaign(simDate))
        {
            // Reset annual data
            // ---------------------
            
            // Reproduction
            _annualCalvingIA1Interval.clear();
            _annualSuccessfulIA1Count = 0;
            _annualCalvingFecundingIAInterval.clear();
            _annualCalvingCalvingInterval.clear();
            _annualFirstCalvingAge.clear();
            _annualIA1Count = 0;
            _annualIACount = 0;
            _annualReproductionTroubleTreatmentCount = 0;
            _annualReproductionTroubleCullingCount = 0;
            _annualCalvingCount = 0;
            
            // Lactation
            _annualAllLactationControledCows = 0;
            _annual4OrMoreLactationControledCows = 0;
            _deliveredMilkOfTheCampaign.init();
            _producedMilkOfTheCampaign.init();

            // Population
            _annualBirthCount = 0;
            _annualMaleSaleCount = 0;
            _annualBeefCrossBredSaleCount = 0;
            _annualSterileFemaleSaleCount = 0;
            _annualExtraFemaleSaleCount = 0;
            _annualExtraPregnantHeiferSaleCount = 0;
            _annualHeiferBoughtCount = 0;
            _annualNewComerInHerdCount = 0;
            _annualDeathCalveCount = 0;
            _annualDeathHeiferCount = 0;
            _annualMastitisDeathAdultCount = 0;
            _annualKetosisDeathAdultCount = 0;
            _annualNonInfectiousLamenessDeathAdultCount = 0;
            _annualInfectiousLamenessDeathAdultCount = 0;
            _annualOtherDeathAdultCount = 0;
            _annualNotBredBecauseOfInfertilityCount = 0;
            _annualNotBredBecauseOfLactationRankCount = 0;
            _annualNotBredBecauseOfLowMilkProductCount = 0;
            _annualCulledCows = 0;
            _annualInfertilityCulledAnimalCount = 0;
            _annualLactationRankCulledAnimalCount = 0;
            _annualLowMilkProductCulledAnimalCount = 0;
            _vAnnualDaysBetweenDecisionAndCullingAccumulator.clear();
            _vAnnualLactationStageAtCullingAccumulator.clear();
            _vAnnualMilkQuantityOfTheDayAtCullingAccumulator.clear();
            _annualMaxMilkQuantityOfTheDayAtCulling = 0.0f;
            _annualMinLactationStageAtCulling = 0;
            
             // Health
            _annualTotalHealthTreatmentCount = 0;
            _annualCurativeClinicalKetosisVetTreatmentCount = 0;
            _annualCurativeClinicalKetosisFarmerTreatmentCount = 0;
            _annualCurativeSubclinicalKetosisVetTreatmentCount = 0;
            _annualCurativeSubclinicalKetosisFarmerTreatmentCount = 0;
            _annualCurativeClinicalMastitisVetTreatmentCount = 0;
            _annualCurativeClinicalMastitisFarmerTreatmentCount = 0;
            _annualCurativeSubclinicalMastitisVetTreatmentCount = 0;
            _annualCurativeSubclinicalMastitisFarmerTreatmentCount = 0;
            //_annualPreventiveKetosisVetTreatmentCount = 0;
            _annualPreventiveKetosisFarmerTreatmentCount = 0;
            //_annualPreventiveMastitisVetTreatmentCount = 0;
            _annualPreventiveMastitisFarmerTreatmentCount = 0;
            _annualCurativeClinicalLamenessVetTreatmentCount = 0;
            _annualPreventiveLamenessVetTreatmentCount = 0;
            _annualUnscheduledCowVetCareCount = 0;
            _annualScheduledCowFarmerCareCount = 0;
            _annualUnscheduledCowFarmerCareCount = 0;
            _annualScheduledVetActivityCount = 0;
            _annualUnscheduledVetActivityCount = 0;
            _annualScheduledFarmerActivityCount = 0;
            _annualUnscheduledFarmerActivityCount = 0;

            // Mastitis
            _annualMastitisCullingCount = 0;
            _annualFirstClinicalMastitisCount = 0;
            _annualSubclinicalMastitisCount = 0;            
            _annualClinicalMastitisCount = 0;
            _vAnnualSCCAccumulator.clear();
            _vLowSCCControlResultOfTheYear = {{false, 0}, {true, 0}};
            // Ketosis
            _annualKetosisCullingCount = 0;
            _previousCampaignCalvingSubclinicalKetosisCount = 0;
            _thisCampaignCalvingSubclinicalKetosisCount = 0;
            _previousCampaignCalvingClinicalKetosisCount = 0;
            _thisCampaignCalvingClinicalKetosisCount = 0;
            _annualClinicalKetosisCount = 0;
            _annualFirstSubclinicalKetosisCount = 0;
            _annualFirstClinicalKetosisCount = 0;
            // Lameness
            _annualG1NonInfectiousLamenessCount = 0;
            _annualG2NonInfectiousLamenessCount = 0;
            _annualNonInfectiousLamenessCullingCount = 0;
            _annualInfectiousLamenessCullingCount = 0;
            _annualG1InfectiousLamenessCount = 0;
            _annualG2InfectiousLamenessCount = 0;

            // Population
            _vAnnualPresentAdultCowAccumulator.clear();
            
            // Feeding data
            _discardedMilkCalfConsumptionOfTheYear = 0.0f;
            _bulkMilkCalfConsumptionOfTheYear = 0.0f;
            _dehydratedMilkCalfConsumptionOfTheYear = 0.0f;
            _concentrateCalfConsumptionOfTheYear.init();
            _mineralVitaminCalfConsumptionOfTheYear = 0.0f;
            _forageHeiferConsumptionOfTheYear.init();
            _concentrateHeiferConsumptionOfTheYear.init();
            _mineralVitaminHeiferConsumptionOfTheYear = 0.0f;
            _forageCowConsumptionOfTheYear.init();
            _concentrateCowConsumptionOfTheYear.init();
            _mineralVitaminCowConsumptionOfTheYear = 0.0f;
            
            // Accounting
            _milkSales = 0.0f;
            _cullSales = 0.0f;
            _heiferSales = 0.0f;
            _calfSales = 0.0f;
            _feedCosts = 0.0f;
            _mastitisHealthCosts = 0.0f;
            _ketosisHealthCosts = 0.0f;
            _curativeIndividualInfectiousLamenessCosts = 0.0f;
            _curativeIndividualNonInfectiousLamenessCosts = 0.0f;
            _curativeCollectiveInfectiousLamenessCosts = 0.0f;
            _curativeCollectiveNonInfectiousLamenessCosts = 0.0f;
            _curativeTrimmingCosts = 0.0f;
            _preventiveTrimmingCosts = 0.0f;
            _footBathCosts = getDairyFarm()->getFootBathCost(
#ifdef _LOG              
                                                                        simDate
#endif // _LOG                
                                                                        );
            _otherHealthCosts = getDairyFarm()->getVetCareContractCost(
#ifdef _LOG              
                                                                        simDate
#endif // _LOG                
                                                                        );
            _reproductionCosts =  getDairyFarm()->getVetReproductionContractCost(
#ifdef _LOG              
                                                                        simDate
#endif // _LOG                
                                                                        );
            _populationCosts = 0.0f;
            _otherCosts = 0.0f;
        }
        
        // time laps simulation data 
        // init the daily production
        _grossDeliveredMilkOfTheDay.init();
        _grossDiscardedMilkOfTheDay.init();
       
        unsigned int day = simDate.day(); // Day of the month
        
        // Collected milk test result ? // Not yet implemented because actually 
        if (day == FunctionalConstants::Lactation::FIRST_DAY_FOR_COLLECTED_MILK_TEST + 1)
        {
            // ...
            _collectedMilkTestResult1 = _lastMilkTankCollected;
        }
        else if (day == FunctionalConstants::Lactation::SECOND_DAY_FOR_COLLECTED_MILK_TEST + 1)
        {
            // ...
            _collectedMilkTestResult2 = _lastMilkTankCollected;
        }
        else if (day == FunctionalConstants::Lactation::THIRD_DAY_FOR_COLLECTED_MILK_TEST + 1)
        {
            // ...
            _collectedMilkTestResult3 = _lastMilkTankCollected;
        }
        
        // Calf
        _discardedMilkCalfConsumptionOfTheDay = 0.0f;
        _bulkMilkCalfConsumptionOfTheDay = 0.0f;
        _dehydratedMilkCalfConsumptionOfTheDay = 0.0f;
        _forageCalfConsumptionOfTheDay.init();
        _concentrateCalfConsumptionOfTheDay.init();
        _mineralVitaminCalfConsumptionOfTheDay = 0.0f;

        // Heifer
        _forageHeiferConsumptionOfTheDay.init();
        _concentrateHeiferConsumptionOfTheDay.init();
        _mineralVitaminHeiferConsumptionOfTheDay = 0.0f;
        
        // Cow
        _forageCowConsumptionOfTheDay.init();
        _concentrateCowConsumptionOfTheDay.init();
        _mineralVitaminCowConsumptionOfTheDay = 0.0f;
       
        // Current population
        _heifersInDriedCowsAndPregnantHeifersBatchOfTheDay.clear();

        // set the sunny adjustment (called by the dairy cows when milking)
        Lactation::LactationManagement::setSunnyAdjustmentOfTheDay(_milkSunnyAdjustmentOfTheDay, simDate);
        
        Herd::progress(simDate);

        // Batch progressing of the day (for location)
        for (std::map<FunctionalEnumerations::Population::BatchType, Population::Batch* >::iterator it = _mpIncludesBatch.begin(); it !=  _mpIncludesBatch.end(); it++)
        {
            it->second->progress(simDate);
        }
        
        // Animal progressing of the day
        for (auto itAnim : getPresentAnimals())
        {
            (itAnim.second)->progress(simDate);

        }
        
        // Adult population
        Population::PopulationManagement::cullMandatored(this, simDate); // Mandatored culling
        if (day == FunctionalConstants::Population::DAY_OF_THE_MONTH_FOR_PREGNANT_HEIFER_NEED_CALCULATION)
        {
            Population::PopulationManagement::managePopulation(this, simDate, getDairyFarm()->calculateMissingHeifers(simDate));
        }
                
        // Week for operations
        if (simDate.day_of_week().as_short_string() ==  FunctionalConstants::Global::DAY_OF_THE_WEEK_FOR_OPERATIONS)
        {          
            // Sale calves ?
            Population::PopulationManagement::saleCalves(this, simDate);
        }
        
        if (day == 1)
        {
            // Nothing yet
            // ...

        }
        
        // Milk product
        _milkTank += _grossDeliveredMilkOfTheDay; // Add the product of the day in the milk tank
        _producedMilkOfTheCampaign += _grossDeliveredMilkOfTheDay;

        if (--_dayRemainingBeforeMilkCollecting == 0) // Day for collecting
        {
            // It is the day for delivery
            _deliveredMilkOfTheMonth += _milkTank;
            _lastMilkTankCollected = _milkTank; // To save the last milk quality for next collected milk test
            _milkTank.init();
            _dayRemainingBeforeMilkCollecting = FunctionalConstants::Lactation::MILK_DELIVERY_FREQUENCY;
        }

        _grossDiscardedMilkOfTheMonth += _grossDiscardedMilkOfTheDay; // Add the discarded product of the day
        _producedMilkOfTheCampaign += _grossDiscardedMilkOfTheDay;
        

        // Year feed consumption
        _discardedMilkCalfConsumptionOfTheYear += _discardedMilkCalfConsumptionOfTheDay;
        _bulkMilkCalfConsumptionOfTheYear += _bulkMilkCalfConsumptionOfTheDay;
        _dehydratedMilkCalfConsumptionOfTheYear += _dehydratedMilkCalfConsumptionOfTheDay;
        _forageCalfConsumptionOfTheYear += _forageCalfConsumptionOfTheDay;
        _concentrateCalfConsumptionOfTheYear += _concentrateCalfConsumptionOfTheDay;
        _mineralVitaminCalfConsumptionOfTheYear += _mineralVitaminCalfConsumptionOfTheDay;
        _forageHeiferConsumptionOfTheYear += _forageHeiferConsumptionOfTheDay;
        _concentrateHeiferConsumptionOfTheYear += _concentrateHeiferConsumptionOfTheDay;
        _mineralVitaminHeiferConsumptionOfTheYear += _mineralVitaminHeiferConsumptionOfTheDay;
        _forageCowConsumptionOfTheYear += _forageCowConsumptionOfTheDay;
        _concentrateCowConsumptionOfTheYear += _concentrateCowConsumptionOfTheDay;
        _mineralVitaminCowConsumptionOfTheYear += _mineralVitaminCowConsumptionOfTheDay;
        
        if (simDate == simDate.end_of_month())
        {
            // It is the last day of the month, so we have to produce some results
            
            // Month milk product :
            // ---------------------
            // delivered milk
#if defined _LOG || defined _FULL_RESULTS
            Results::MilkResult mr;
            mr.date = simDate;
            mr.literQuantity = _deliveredMilkOfTheMonth.quantity * FunctionalConstants::Lactation::MILK_KILO_TO_LITER_FACTOR; // to liter
            mr.literTB = _deliveredMilkOfTheMonth.TB / FunctionalConstants::Lactation::MILK_KILO_TO_LITER_FACTOR; // to liter
            mr.literTP = _deliveredMilkOfTheMonth.TP / FunctionalConstants::Lactation::MILK_KILO_TO_LITER_FACTOR; // to liter
            mr.SCC = _deliveredMilkOfTheMonth.SCC;
            getDairyFarm()->getCurrentResults()->getLactationResults().DeliveredMilkResults.push_back(mr);
#endif // _LOG || _FULL_RESULTS       
            
            _deliveredMilkOfTheCampaign += _deliveredMilkOfTheMonth;

            // Economic
            if (_deliveredMilkOfTheMonth.quantity > 0.0f)
            {
                ExchangeInfoStructures::MilkProductCharacteristic collectedMilkTestResults = _collectedMilkTestResult1 + _collectedMilkTestResult2 + _collectedMilkTestResult3;
                accountNewTransaction(
#ifdef _LOG                
                                    simDate, FunctionalConstants::AccountingModel::DELIVERED_MILK_TRANSACTION_TYPE, 
#endif // _LOG                
                                    Lactation::LactationManagement::calculateDeliveredMilkPrice(_deliveredMilkOfTheMonth, collectedMilkTestResults.SCC, getDairyFarm()->getCurrentAccountingModel()), _milkSales);
            }
            
            // Herd navigator
            if (getDairyFarm()->useHerdNavigator())
            {
                float amount = 0.0f;
                
                //for (unsigned int i = 0; i < getAnimalNumber(); i++)
                for (auto itAnim : getPresentAnimals())
                {
                    if ((itAnim.second)->isMilking())
                    {
                        //amount += (getDairyFarm()->getCurrentAccountingModel().herdNavigatorPrice.currentPrice) / 12.0f;
                        amount += (getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_HERD_NAVIGATOR_PRICE)) / 12.0f;
                    }
                }
                if (amount != 0.0f)
                {
                    accountNewTransaction(
#ifdef _LOG                
                                    simDate, FunctionalConstants::AccountingModel::HERD_NAVIGATOR_TRANSACTION_TYPE, 
#endif // _LOG                
                                    amount, _otherCosts);
                }
            }

#if defined _LOG || defined _FULL_RESULTS
            // Discarded milk
            mr.literQuantity = _grossDiscardedMilkOfTheMonth.quantity * FunctionalConstants::Lactation::MILK_KILO_TO_LITER_FACTOR; // to liter
            
            // We don't care of following results for discarded milk
            mr.literTB = 0.0f;
            mr.literTP = 0.0f;
            mr.SCC = 0.0f;    
            getDairyFarm()->getCurrentResults()->getLactationResults().DiscardedMilkResults.push_back(mr);        
#endif // _LOG || _FULL_RESULTS               
            
            // Next step, we'll begin a new month
            _deliveredMilkOfTheMonth.init();
            _grossDiscardedMilkOfTheMonth.init();
             
        }

        if (day == FunctionalConstants::Lactation::DAY_OF_THE_MONTH_FOR_MILK_CONTROL)
        {
#ifdef _LOG  
            // Maintenance
//            std::cout << "DairyHerd::progress : Milk control : " << simDate << std::endl;
//            Population::Batch* pBatch = getBatch(FunctionalEnumerations::Population::BatchType::lactatingCows);
//            unsigned int totalLS = 0;
//            unsigned int nbLS = 0;
//            for (auto it : getBatch(FunctionalEnumerations::Population::BatchType::lactatingCows)->getAnimalList())
//            {
//                DairyMammal* pCow = (DairyMammal*)it.second;
//                int ls = pCow->getLactationStage();
//                if (ls > 6)
//                {
//                    totalLS += ls;
//                    nbLS++;
//                }
//            }
//            if (nbLS > 0)
//            {
//                float moyenne = totalLS/nbLS;
//                std::cout << moyenne << std::endl;
//            }
                 
#endif // _LOG
            // Milk control
            unsigned int presentAdultCowCount = 0;
            unsigned int controledCowCount = 0;
            unsigned int totalLactationStage = 0;
            unsigned int totalCowSCCOverThanRef = 0; // For prevalence
            unsigned int BH = 0; // For incidence
            unsigned int BB = 0; // For incidence
            
            unsigned int detectedInfectiousLamenessCount = 0;
            unsigned int infectiousG1LamenessCount = 0;
            unsigned int infectiousG2LamenessCount = 0;
            unsigned int detectedNonInfectiousLamenessCount = 0;
            unsigned int nonInfectiousG1LamenessCount = 0;
            unsigned int nonInfectiousG2LamenessCount = 0;
            
#ifdef _LOG            
            std::vector<unsigned int> ketosisConcernedCowCount = std::vector<unsigned int> (FunctionalConstants::Health::DAY_RANGES_FOR_KETOSIS_CALIBRATION.size(), 0);
            std::vector<unsigned int> G1CurrentketosisCases = std::vector<unsigned int> (FunctionalConstants::Health::DAY_RANGES_FOR_KETOSIS_CALIBRATION.size(), 0); // for prevalence
            std::vector<unsigned int> G2CurrentketosisCases = std::vector<unsigned int> (FunctionalConstants::Health::DAY_RANGES_FOR_KETOSIS_CALIBRATION.size(), 0); // for prevalence
            std::vector<unsigned int> G1CurrentketosisNewCases = std::vector<unsigned int> (FunctionalConstants::Health::DAY_RANGES_FOR_KETOSIS_CALIBRATION.size(), 0); // for incidence
            std::vector<unsigned int> G2CurrentketosisNewCases = std::vector<unsigned int> (FunctionalConstants::Health::DAY_RANGES_FOR_KETOSIS_CALIBRATION.size(), 0); // for incidence
#endif // _LOG
            
            ExchangeInfoStructures::MilkProductCharacteristic cumulativeIndividualMilkSamples; // cumulative cow milk samples 
            // Individual informations
            //for (unsigned int i = 0; i < getAnimalNumber(); i++)
            
//            // Maintenance
//            std::cout << "stages : ";
//            unsigned int over305 = 0;
            
            for (auto itAnim : getPresentAnimals())
            {
                bool controled = false;
                int stage = -1;
                unsigned int lactationRank = 0;
                bool SCCLessThanRefInPreviousMilkControl = false;
                bool SCCLessThanRefNow = false;
                bool isDetectedInfectiousLameness = false;
                bool isInfectiousG1Lameness = false;
                bool isInfectiousG2Lameness = false;
                bool isDetectedNonInfectiousLameness = false;
                bool isNonInfectiousG1Lameness = false;
                bool isNonInfectiousG2Lameness = false;
                
#ifdef _LOG                
                FunctionalEnumerations::Health::KetosisSeverity ketosisSeverity;
                FunctionalEnumerations::Global::Month lastCalvingMonth;
                bool newKetosisCase = false;

#endif // _LOG                
                // Adult cow ?
                if ((itAnim.second)->getMilkControlInformations(getDairyFarm()->useCetoDetect(), controled, stage, lactationRank, SCCLessThanRefInPreviousMilkControl, SCCLessThanRefNow, simDate, cumulativeIndividualMilkSamples
                                                                                                                                                                                                                , isDetectedInfectiousLameness
                                                                                                                                                                                                                , isInfectiousG1Lameness
                                                                                                                                                                                                                , isInfectiousG2Lameness
                                                                                                                                                                                                                , isDetectedNonInfectiousLameness
                                                                                                                                                                                                                , isNonInfectiousG1Lameness
                                                                                                                                                                                                                , isNonInfectiousG2Lameness
#ifdef _LOG                
                                                                                                                                                                                                                , ketosisSeverity
                                                                                                                                                                                                                , lastCalvingMonth 
                                                                                                                                                                                                                , newKetosisCase
 #endif // _LOG
                                                                                                                                                                                                                ))
                {
                    presentAdultCowCount++;
                    if (controled)
                    {
//                        // maintenance
//                        std::cout << stage << std::endl;
//                        if (stage > 305)
//                        {
//                            over305++;
//                            std::cout<< " (id " << (itAnim.second)->getUniqueId() << ")";
//                        }
//                        std::cout << ", ";
                        
                        (_vLowSCCControlResultOfTheYear.find(SCCLessThanRefNow)->second)++; // Count the SCC control result (for annual mastitis results)
                        controledCowCount++;
                        
                        totalLactationStage += stage;
                        
                        // Prevalence
                        if (!SCCLessThanRefNow) totalCowSCCOverThanRef++;
                        
                        // Incidence
                        if (SCCLessThanRefInPreviousMilkControl)
                        {
                            if (SCCLessThanRefNow) BB++;
                            else BH++;
                        }
                        
                        // Data for annual report
                        _annualAllLactationControledCows++;
                        if (lactationRank >= 4)
                        {
                            _annual4OrMoreLactationControledCows++;
                        }
                        
                        // Lameness control results
                        if (isDetectedInfectiousLameness) detectedInfectiousLamenessCount++;
                        if (isInfectiousG1Lameness) infectiousG1LamenessCount++;                  
                        if (isInfectiousG2Lameness) infectiousG2LamenessCount++;                  
                        if (isDetectedNonInfectiousLameness) detectedNonInfectiousLamenessCount++;
                        if (isNonInfectiousG1Lameness) nonInfectiousG1LamenessCount++;                  
                        if (isNonInfectiousG2Lameness) nonInfectiousG2LamenessCount++;                  
                    }

#ifdef _LOG              
                    // Ketosis statistics for calibration
                    std::vector<unsigned int> *pKetosisSeverityToSetForPrevalence = nullptr;
                    std::vector<unsigned int> *pKetosisSeverityToSetForIncidence = nullptr;
                    if (ketosisSeverity == FunctionalEnumerations::Health::KetosisSeverity::G1Ketosis)
                    {
                        pKetosisSeverityToSetForPrevalence = &(G1CurrentketosisCases);
                        pKetosisSeverityToSetForIncidence = &(G1CurrentketosisNewCases);
                    }
                    else if (ketosisSeverity == FunctionalEnumerations::Health::KetosisSeverity::G2Ketosis)
                    {
                        pKetosisSeverityToSetForPrevalence = &(G2CurrentketosisCases);
                        pKetosisSeverityToSetForIncidence = &(G2CurrentketosisNewCases);
                    }
                    bool stop = false;
                    for (unsigned int i = 0; i < FunctionalConstants::Health::DAY_RANGES_FOR_KETOSIS_CALIBRATION.size() and not stop; i++)
                    {
                        if (stage <= ((int)FunctionalConstants::Health::DAY_RANGES_FOR_KETOSIS_CALIBRATION[i]))
                        {
                            if (pKetosisSeverityToSetForPrevalence != nullptr)
                            {
                                (*pKetosisSeverityToSetForPrevalence)[i]++; // Count the ketosis case
                                if (newKetosisCase and (   lastCalvingMonth >= FunctionalConstants::Health::MONTH_CALVING_BEGIN_FOR_KETOSIS_CALIBRATION or 
                                                    lastCalvingMonth <= FunctionalConstants::Health::MONTH_CALVING_END_FOR_KETOSIS_CALIBRATION))
                                {
                                    (*pKetosisSeverityToSetForIncidence)[i]++; // Count the new case for this controled cow (only if the good period
                                }      
                            }
                            ketosisConcernedCowCount[i]++; // Count the controled cow
                            stop = true;
                        }
                    }
#endif // _LOG                
                }
            }
            
//            // Maintenance
//            std::cout << std::endl;

            // Milk control results
            Results::MilkControlResult mc;
            mc.date = simDate;
            mc.meanGrossMilkProductOfTheControledDay = controledCowCount > 0 ? (_grossDeliveredMilkOfTheDay.quantity + _grossDiscardedMilkOfTheDay.quantity)/controledCowCount : 0.0f;
            mc.meanLactationStage = controledCowCount > 0 ? totalLactationStage/controledCowCount : 0;
//            std::cout << "totalLactationStage = " << totalLactationStage << ", controledCowCount = " << controledCowCount << ", mc.meanLactationStage = " << mc.meanLactationStage << std::endl;
            mc.milkTBOfTheControledDay = cumulativeIndividualMilkSamples.TB;
            mc.milkTPOfTheControledDay = cumulativeIndividualMilkSamples.TP;
            mc.milkSCCOfTheControledDay = cumulativeIndividualMilkSamples.SCC;
            mc.presentAdultCowCount = presentAdultCowCount;
            mc.presentMilkingCowCount = controledCowCount;
            mc.SCCPrevalenceRatio = controledCowCount > 0 ? 1.0f - ((float)totalCowSCCOverThanRef / (float)controledCowCount) : 0.0f;
            mc.SCCIncidence = BH > 0 ? (float)BH / ((float)BB + (float)BH) : 0.0f;
            mc.detectedInfectiousLamenessPercent = controledCowCount > 0 ? (float)detectedInfectiousLamenessCount * 100.0f / (float) controledCowCount : 0.0f;
            mc.infectiousG1LamenessPercent = controledCowCount > 0 ? (float)infectiousG1LamenessCount * 100.0f / (float) controledCowCount : 0.0f;
            mc.infectiousG2LamenessPercent = controledCowCount > 0 ? (float)infectiousG2LamenessCount * 100.0f / (float) controledCowCount : 0.0f;
            mc.detectedNonInfectiousLamenessPercent = controledCowCount > 0 ? (float)detectedNonInfectiousLamenessCount * 100.0f / (float) controledCowCount : 0.0f;
            mc.nonInfectiousG1LamenessPercent = controledCowCount > 0 ? (float)nonInfectiousG1LamenessCount * 100.0f / (float) controledCowCount : 0.0f;
            mc.nonInfectiousG2LamenessPercent = controledCowCount > 0 ? (float)nonInfectiousG2LamenessCount * 100.0f / (float) controledCowCount : 0.0f;
            
#ifdef _LOG   
            // For ketosis calibration
            for (unsigned int i = 0; i < FunctionalConstants::Health::DAY_RANGES_FOR_KETOSIS_CALIBRATION.size(); i++)
            {
                mc.concernedCowCount[i] = ketosisConcernedCowCount[i];
                mc.G1ketosisPrevalence[i] = G1CurrentketosisCases[i];
                mc.G2ketosisPrevalence[i] = G2CurrentketosisCases[i];
                mc.G1ketosisIncidence[i] = G1CurrentketosisNewCases[i];
                mc.G2ketosisIncidence[i] = G2CurrentketosisNewCases[i];
            }
            
#endif // _LOG                
            getDairyFarm()->getCurrentResults()->getLactationResults().MilkControlResults.push_back(mc);
            
            // For coming annual report
            _vAnnualPresentAdultCowAccumulator.push_back(presentAdultCowCount);
            _vAnnualSCCAccumulator.push_back(_grossDeliveredMilkOfTheDay.SCC);
            
            // Control cost
            if (controledCowCount > 0)
            {
#ifdef _LOG                
                std::string comp = FunctionalConstants::AccountingModel::MILK_CONTROL_TRANSACTION_TYPE;
#endif // _LOG                
                float amount = (getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_MILK_CONTROL_PRICE) * controledCowCount) / 12.0f;
                
                if (getDairyFarm()->useCetoDetect())
                {
#ifdef _LOG                
                    comp += FunctionalConstants::AccountingModel::CETODETECT_SUB_TRANSACTION_TYPE;
#endif // _LOG                
                    amount += (getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_CETO_DETECT_PRICE) * controledCowCount) / 12.0f;         
                }
                accountNewTransaction(
#ifdef _LOG                
                                    simDate, comp, 
#endif // _LOG                
                                    amount, _otherCosts);
            }
        }
        
        if (getDairyFarm()->hasVetReproductionContract() and day == FunctionalConstants::Reproduction::DAY_OF_THE_MONTH_FOR_VET_REPRO_CONTROL)
        {
            for (auto itAnim : getPresentAnimals())
            {
                DairyMammal* pAnim = (DairyMammal*)(itAnim.second);

                if (pAnim->needTreatmentForAbnormalNoOestrusDetection(simDate, true))
                {
                    // Case a
                    pAnim->takeAbnormalNoOestrusDetectionTreatment(simDate, getDairyFarm()->giveAbnormalNoOestrusDetectionTreatment(  
#ifdef _LOG
                                                                                                                            simDate 
#endif // _LOG        
                                                                                                                            ));
                }
                else if (pAnim->needTreatmentForAbnormalUnsuccessfulIA(simDate, true))
                {
                    // Case b
                    pAnim->takeAbnormalUnsuccessfulIATreatment(simDate, getDairyFarm()->giveAbnormalUnsuccessfulIATreatment( 
#ifdef _LOG
                                                                                                                            simDate 
#endif // _LOG        
                                                                                                                            ));
                }
                else if (pAnim->needTreatmentForNegativePregnancyDiagnostic(simDate, true))
                {
                    // Case c
                    pAnim->takeNegativePregnancyDiagnosticTreatment(simDate, getDairyFarm()->giveNegativePregnancyDiagnosticTreatment( 
#ifdef _LOG
                                                                                                                            simDate 
#endif // _LOG        
                                                                                                                            ));
                }
            }
        }
    }
    
    void DairyHerd::produceEndCampaignResults(const boost::gregorian::date &simDate)
    {
        float meanAdultCows = getMeanAdultCowCount();

        _previousCampaignMeanAdultCowCount = (unsigned int) meanAdultCows;
        _previousCampaignCalvingCount = _annualCalvingCount; 
        _previousCampaignDeliveredKiloliter = (unsigned int)(_deliveredMilkOfTheCampaign.quantity * FunctionalConstants::Lactation::MILK_KILO_TO_LITER_FACTOR)/1000.0f;

        // Year technical reproduction report
        // ----------------------------------
        Results::TechnicalReproductionResult trr;
        trr.date = simDate;
        trr.meanIVIA1 = _annualCalvingIA1Interval.size() > 0 ? std::accumulate(_annualCalvingIA1Interval.begin(), _annualCalvingIA1Interval.end(), 0.0f) / _annualCalvingIA1Interval.size() : 0.0f;
        trr.succesfulIA1Percent = _annualIA1Count > 0 ? (float)_annualSuccessfulIA1Count * 100.0f / (float) _annualIA1Count : 0.0f;
        trr.meanIVIAF = _annualCalvingFecundingIAInterval.size() > 0 ? std::accumulate(_annualCalvingFecundingIAInterval.begin(), _annualCalvingFecundingIAInterval.end(), 0.0f) / _annualCalvingFecundingIAInterval.size() : 0.0f;
        trr.meanIVV = _annualCalvingCalvingInterval.size() > 0 ? std::accumulate(_annualCalvingCalvingInterval.begin(), _annualCalvingCalvingInterval.end(), 0.0f) / _annualCalvingCalvingInterval.size() : 0.0f;
        trr.meanFirstCalvingAge = _annualFirstCalvingAge.size() > 0 ? std::accumulate(_annualFirstCalvingAge.begin(), _annualFirstCalvingAge.end(), 0.0f) / _annualFirstCalvingAge.size() : 0.0f;
        trr.nbIA1 = _annualIA1Count;
        trr.nbIA = _annualIACount;
        trr.reproductionTroubleTreatmentCount = _annualReproductionTroubleTreatmentCount;
        trr.reproductionTroubleTreatmentPercent = meanAdultCows > 0 ? (float)_annualReproductionTroubleTreatmentCount * 100.0f / (float) meanAdultCows : 0.0f;
        trr.reproductionTroubleCullingPercent = meanAdultCows > 0 ? (float)_annualReproductionTroubleCullingCount * 100.0f / (float) meanAdultCows : 0.0f;
        getDairyFarm()->getCurrentResults()->getReproductionResults().TechnicalReproductionResults.push_back(trr);

        // Year technical production report
        // --------------------------------
        Results::TechnicalProductionResult tlr;
        tlr.date = simDate;
        tlr.deliveredMilkLiterQuantity = _deliveredMilkOfTheCampaign.quantity * FunctionalConstants::Lactation::MILK_KILO_TO_LITER_FACTOR; // to liter
        tlr.deliveredMilkLiterQuantityByCow = meanAdultCows > 0 ? tlr.deliveredMilkLiterQuantity / (float) meanAdultCows : 0.0f;
        tlr.productedMilkLiterQuantityByCow = meanAdultCows > 0 ? (_producedMilkOfTheCampaign.quantity * FunctionalConstants::Lactation::MILK_KILO_TO_LITER_FACTOR) / (float) meanAdultCows : 0.0f;
        tlr.SCC = _deliveredMilkOfTheCampaign.SCC;
        tlr.TB = _deliveredMilkOfTheCampaign.TB / FunctionalConstants::Lactation::MILK_KILO_TO_LITER_FACTOR;
        tlr.TP = _deliveredMilkOfTheCampaign.TP / FunctionalConstants::Lactation::MILK_KILO_TO_LITER_FACTOR;

        getDairyFarm()->getCurrentResults()->getLactationResults().TechnicalProductionResults.push_back(tlr);

        // Year feed consumption :
        // -----------------------     
        
        // The calf concentrate consumption is to affect to the heifers
        _concentrateHeiferConsumptionOfTheYear += _concentrateCalfConsumptionOfTheYear;
        _concentrateCalfConsumptionOfTheYear.init();

#if defined _LOG || defined _FULL_RESULTS
        // calf
        Results::CalfFeedingResult cfr;
        cfr.date = simDate;
        cfr.discardedMilkQuantity = _discardedMilkCalfConsumptionOfTheYear * FunctionalConstants::Lactation::MILK_KILO_TO_LITER_FACTOR; // to liter
        cfr.bulkMilkQuantity = _bulkMilkCalfConsumptionOfTheYear * FunctionalConstants::Lactation::MILK_KILO_TO_LITER_FACTOR; // to liter
        cfr.dehydratedMilkQuantity = getDehydratedMilkKgFromMilkKg(_dehydratedMilkCalfConsumptionOfTheYear); // to kg powder
        cfr.maizeSilageQuantity = _forageCalfConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ForageType::maizePlant); // / meanNumber;
        cfr.grassQuantity = _forageCalfConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ForageType::grass); // / meanNumber;
        cfr.hayQuantity = _forageCalfConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ForageType::hay); // / meanNumber;
        cfr.grassSilageQuantity = _forageCalfConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ForageType::grassSilage); // / meanNumber;
        cfr.rapeseedQuantity = _concentrateCalfConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed); // / meanNumber;
        cfr.sojaQuantity = _concentrateCalfConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ConcentrateType::soja); // / meanNumber;
        cfr.barleyQuantity = _concentrateCalfConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ConcentrateType::barley); // / meanNumber;            
        cfr.mineralVitaminQuantity = _mineralVitaminCalfConsumptionOfTheYear; // / meanNumber;            
        getDairyFarm()->getCurrentResults()->getFeedingResults().calfFeedingResults.push_back(cfr);

        // heifer
        Results::FeedingResult fr;
        fr.date = simDate;
        fr.maizeSilageQuantity = _forageHeiferConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ForageType::maizePlant); // / meanNumber;
        fr.grassQuantity = _forageHeiferConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ForageType::grass); // / meanNumber;
        fr.hayQuantity = _forageHeiferConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ForageType::hay); // / meanNumber;
        fr.grassSilageQuantity = _forageHeiferConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ForageType::grassSilage); // / meanNumber;
        fr.rapeseedQuantity = _concentrateHeiferConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed); // / meanNumber;
        fr.sojaQuantity = _concentrateHeiferConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ConcentrateType::soja); // / meanNumber;
        fr.barleyQuantity = _concentrateHeiferConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ConcentrateType::barley); // / meanNumber;            
        fr.mineralVitaminQuantity = _mineralVitaminHeiferConsumptionOfTheYear; // / meanNumber;            
        getDairyFarm()->getCurrentResults()->getFeedingResults().heiferFeedingResults.push_back(fr);

        // cow
        fr.maizeSilageQuantity = _forageCowConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ForageType::maizePlant); // / meanNumber;
        fr.grassQuantity = _forageCowConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ForageType::grass); // / meanNumber;
        fr.hayQuantity = _forageCowConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ForageType::hay); // / meanNumber;
        fr.grassSilageQuantity = _forageCowConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ForageType::grassSilage); // / meanNumber;
        fr.rapeseedQuantity = _concentrateCowConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed); // / meanNumber;
        fr.sojaQuantity = _concentrateCowConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ConcentrateType::soja); // / meanNumber;
        fr.barleyQuantity = _concentrateCowConsumptionOfTheYear.getQuantity(FunctionalEnumerations::Feeding::ConcentrateType::barley); // / meanNumber;            
        fr.mineralVitaminQuantity = _mineralVitaminCowConsumptionOfTheYear; // / meanNumber;            
        getDairyFarm()->getCurrentResults()->getFeedingResults().cowFeedingResults.push_back(fr);
#endif // _LOG || _FULL_RESULTS                           

        // Next step, we'll begin a new year

        // Dehydrated milk
        float amount = _dehydratedMilkCalfConsumptionOfTheYear * getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_DEHYDRATED_MILK_TON_PRICE) / 1000;
        if (amount != 0.0f)
        {
            accountNewTransaction(
#ifdef _LOG                
                                simDate, FunctionalConstants::AccountingModel::DEHYDRATED_MILK_TRANSACTION_TYPE, 
#endif // _LOG                
                                amount, _feedCosts);
        }

        // Forages
        for (unsigned int i = 0; i <= FunctionalEnumerations::Feeding::ForageType::lastForageType; i++)
        {
            float quantity = _forageHeiferConsumptionOfTheYear.getQuantity((FunctionalEnumerations::Feeding::ForageType)i);
            quantity += _forageCowConsumptionOfTheYear.getQuantity((FunctionalEnumerations::Feeding::ForageType)i);
            amount = quantity * getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEYS_FORAGE.find((FunctionalEnumerations::Feeding::ForageType)i)->second) / 1000;
            if (amount != 0.0f)
            {
                accountNewTransaction(
#ifdef _LOG                
                                simDate, ExchangeInfoStructures::Parameters::MixtureCharacteristicParameters::getStrForageType((FunctionalEnumerations::Feeding::ForageType)i) + "_forage_purchase", 
#endif // _LOG                
                                amount, _feedCosts);
            }
        }

        // Concentrates
        for (unsigned int i = 0; i <= FunctionalEnumerations::Feeding::ConcentrateType::lastConcentrateType; i++)
        {
            float quantity = _concentrateCalfConsumptionOfTheYear.getQuantity((FunctionalEnumerations::Feeding::ConcentrateType)i);
            quantity += _concentrateHeiferConsumptionOfTheYear.getQuantity((FunctionalEnumerations::Feeding::ConcentrateType)i);
            quantity += _concentrateCowConsumptionOfTheYear.getQuantity((FunctionalEnumerations::Feeding::ConcentrateType)i);
            amount = quantity * getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEYS_CONCENTRATE.find((FunctionalEnumerations::Feeding::ConcentrateType)i)->second) / 1000;
            if (amount != 0.0f)
            {
                accountNewTransaction(
#ifdef _LOG                
                                simDate, ExchangeInfoStructures::Parameters::MixtureCharacteristicParameters::getStrConcentrateType((FunctionalEnumerations::Feeding::ConcentrateType)i) + "_concentrate_purchase", 
#endif // _LOG                
                                amount, _feedCosts);
            }
        }

        // Minerals and vitamins
        float quantity = _mineralVitaminCalfConsumptionOfTheYear + _mineralVitaminHeiferConsumptionOfTheYear + _mineralVitaminCowConsumptionOfTheYear;
        amount = quantity * getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_MINERAL_VITAMIN_TON_PRICE) / 1000;

        if (amount != 0.0f)
        {
            accountNewTransaction(
#ifdef _LOG                
                                simDate, "Mineral_vitamin_purchase", 
#endif // _LOG                
                                amount, _feedCosts);
        }
        

        // Year health report
        // ------------------

#if defined _LOG || defined _FULL_RESULTS
        // Mastitis
        Results::AnnualMastitisResult amr;
        amr.date = simDate;
        amr.meanPresentAdultCowCount = meanAdultCows;
        amr.clinicalMastitisPer100cows = meanAdultCows > 0 ? (float)_annualClinicalMastitisCount * 100.0f / (float) meanAdultCows : 0.0f;
        amr.firstClinicalMastitisPer100cows = meanAdultCows > 0 ? (float)_annualFirstClinicalMastitisCount * 100.0f / (float) meanAdultCows : 0.0f;
        amr.herdSCC = _vAnnualSCCAccumulator.size() > 0 ? std::accumulate(_vAnnualSCCAccumulator.begin(), _vAnnualSCCAccumulator.end(), 0.0f) / _vAnnualSCCAccumulator.size() : 0.0f;
        amr.underRefSCCper100Controls = (_vLowSCCControlResultOfTheYear.find(false)->second + _vLowSCCControlResultOfTheYear.find(true)->second) != 0 ? 100 * _vLowSCCControlResultOfTheYear.find(true)->second / (_vLowSCCControlResultOfTheYear.find(false)->second + _vLowSCCControlResultOfTheYear.find(true)->second) : 100;
        getDairyFarm()->getCurrentResults()->getHealthResults().AnnualMastitisResults.push_back(amr);

        // Ketosis
        Results::AnnualKetosisResult acr;
        acr.date = simDate;
        acr.meanPresentAdultCowCount = meanAdultCows;
        acr.clinicalKetosisPer100cows = meanAdultCows > 0 ? (float)_annualClinicalKetosisCount * 100.0f / (float) meanAdultCows : 0.0f;
        acr.firstSubclinicalKetosisPer100cows = meanAdultCows > 0 ? (float)_annualFirstSubclinicalKetosisCount * 100.0f / (float) meanAdultCows : 0.0f;
        acr.firstClinicalKetosisPer100cows = meanAdultCows > 0 ? (float)_annualFirstClinicalKetosisCount * 100.0f / (float) meanAdultCows : 0.0f;
        getDairyFarm()->getCurrentResults()->getHealthResults().AnnualKetosisResults.push_back(acr);

        // Lameness
        Results::AnnualLamenessResult alr;
        alr.date = simDate;
        alr.meanPresentAdultCowCount = meanAdultCows;
        alr.G1LamenessPer100cows = meanAdultCows > 0 ? (float)_annualG1NonInfectiousLamenessCount * 100.0f / (float) meanAdultCows : 0.0f;
        alr.G2LamenessPer100cows = meanAdultCows > 0 ? (float)_annualG2NonInfectiousLamenessCount * 100.0f / (float) meanAdultCows : 0.0f;
        getDairyFarm()->getCurrentResults()->getHealthResults().AnnualNonInfectiousLamenessResults.push_back(alr);
        alr.G1LamenessPer100cows = meanAdultCows > 0 ? (float)_annualG1InfectiousLamenessCount * 100.0f / (float) meanAdultCows : 0.0f;
        alr.G2LamenessPer100cows = meanAdultCows > 0 ? (float)_annualG2InfectiousLamenessCount * 100.0f / (float) meanAdultCows : 0.0f;
        getDairyFarm()->getCurrentResults()->getHealthResults().AnnualInfectiousLamenessResults.push_back(alr);
#endif // _LOG || _FULL_RESULTS                           

        // Technical health
        Results::TechnicalHealthResult thr;
        thr.date = simDate;
        thr.calvingCount = _annualCalvingCount;
        thr.G1ketosisOccurencyDueToCalvingCount = _thisCampaignCalvingSubclinicalKetosisCount;
        thr.G2ketosisOccurencyDueToCalvingCount = _thisCampaignCalvingClinicalKetosisCount;
        thr.G1ketosisOccurencyByCalvingCow =  thr.calvingCount > 0 ? (float)thr.G1ketosisOccurencyDueToCalvingCount * 100.0f / (float) thr.calvingCount : 0.0f; // See also the previous campaign calculation below
        thr.G2ketosisOccurencyByCalvingCow =  thr.calvingCount > 0 ? (float)thr.G2ketosisOccurencyDueToCalvingCount * 100.0f / (float) thr.calvingCount : 0.0f; // See also the previous campaign calculation below 
        thr.subclinicalMastitisOccurencyByCow = meanAdultCows > 0 ? (float)_annualSubclinicalMastitisCount * 100.0f / (float) meanAdultCows : 0.0f;
        thr.clinicalMastitisOccurencyByCow = meanAdultCows > 0 ? (float)_annualClinicalMastitisCount * 100.0f / (float) meanAdultCows : 0.0f;
        thr.G1NonInfectiousLamenessOccurencyByCow = meanAdultCows > 0 ? (float)_annualG1NonInfectiousLamenessCount * 100.0f / (float) meanAdultCows : 0.0f;
        thr.G2NonInfectiousLamenessOccurencyByCow = meanAdultCows > 0 ? (float)_annualG2NonInfectiousLamenessCount * 100.0f / (float) meanAdultCows : 0.0f;
        thr.G1InfectiousLamenessOccurencyByCow = meanAdultCows > 0 ? (float)_annualG1InfectiousLamenessCount * 100.0f / (float) meanAdultCows : 0.0f;
        thr.G2InfectiousLamenessOccurencyByCow = meanAdultCows > 0 ? (float)_annualG2InfectiousLamenessCount * 100.0f / (float) meanAdultCows : 0.0f;
        thr.mastitisMortalityByCow = meanAdultCows > 0 ? (float)_annualMastitisDeathAdultCount * 100.0f / (float) meanAdultCows : 0.0f;
        thr.ketosisMortalityByCow = meanAdultCows > 0 ? (float)_annualKetosisDeathAdultCount * 100.0f / (float) meanAdultCows : 0.0f;
        thr.nonInfectiousLamenessMortalityByCow = meanAdultCows > 0 ? (float)_annualNonInfectiousLamenessDeathAdultCount * 100.0f / (float) meanAdultCows : 0.0f;
        thr.infectiousLamenessMortalityByCow = meanAdultCows > 0 ? (float)_annualInfectiousLamenessDeathAdultCount * 100.0f / (float) meanAdultCows : 0.0f;
        thr.mastitisTroubleCullingPercent = meanAdultCows > 0 ? (float)_annualMastitisCullingCount * 100.0f / (float) meanAdultCows : 0.0f;
        thr.ketosisTroubleCullingPercent = meanAdultCows > 0 ? (float)_annualKetosisCullingCount * 100.0f / (float) meanAdultCows : 0.0f;
        thr.nonInfectiousLamenessTroubleCullingPercent = meanAdultCows > 0 ? (float)_annualNonInfectiousLamenessCullingCount * 100.0f / (float) meanAdultCows : 0.0f;
        thr.infectiousLamenessTroubleCullingPercent = meanAdultCows > 0 ? (float)_annualInfectiousLamenessCullingCount * 100.0f / (float) meanAdultCows : 0.0f;
        thr.totalHealthTreatmentCount = _annualTotalHealthTreatmentCount;
        thr.curativeClinicalKetosisVetTreatmentCount = _annualCurativeClinicalKetosisVetTreatmentCount;
        thr.curativeClinicalKetosisFarmerTreatmentCount = _annualCurativeClinicalKetosisFarmerTreatmentCount;
        thr.curativeSubclinicalKetosisVetTreatmentCount = _annualCurativeSubclinicalKetosisVetTreatmentCount;
        thr.curativeSubclinicalKetosisFarmerTreatmentCount = _annualCurativeSubclinicalKetosisFarmerTreatmentCount;
        thr.curativeClinicalMastitisVetTreatmentCount = _annualCurativeClinicalMastitisVetTreatmentCount;
        thr.curativeClinicalMastitisFarmerTreatmentCount = _annualCurativeClinicalMastitisFarmerTreatmentCount;
        thr.curativeSubclinicalMastitisVetTreatmentCount = _annualCurativeSubclinicalMastitisVetTreatmentCount;
        thr.curativeSubclinicalMastitisFarmerTreatmentCount = _annualCurativeSubclinicalMastitisFarmerTreatmentCount;
//        thr.preventiveKetosisVetTreatmentCount = _annualPreventiveKetosisVetTreatmentCount;
        thr.preventiveKetosisFarmerTreatmentCount = _annualPreventiveKetosisFarmerTreatmentCount;
//        thr.preventiveMastitisVetTreatmentCount = _annualPreventiveMastitisVetTreatmentCount;
        thr.preventiveMastitisFarmerTreatmentCount = _annualPreventiveMastitisFarmerTreatmentCount;       
        thr.curativeClinicalLamenessVetTreatmentCount = _annualCurativeClinicalLamenessVetTreatmentCount;
        thr.preventiveLamenessVetTreatmentCount = _annualPreventiveLamenessVetTreatmentCount;
        thr.unscheduledCowVetCareCount = _annualUnscheduledCowVetCareCount;
        thr.scheduledCowFarmerCareCount = _annualScheduledCowFarmerCareCount;
        thr.unscheduledCowFarmerCareCount = _annualUnscheduledCowFarmerCareCount;
        thr.scheduledVetActivityCount = _annualScheduledVetActivityCount;
        thr.unscheduledVetActivityCount = _annualUnscheduledVetActivityCount;
        thr.scheduledFarmerActivityCount = _annualScheduledFarmerActivityCount;
        thr.unscheduledFarmerActivityCount = _annualUnscheduledFarmerActivityCount;

        getDairyFarm()->getCurrentResults()->getHealthResults().TechnicalHealthResults[getDairyFarm()->getCurrentResults()->getStrCampaign(simDate)] = thr;
        if (_previousCampaignCalvingSubclinicalKetosisCount > 0 or _previousCampaignCalvingClinicalKetosisCount > 0)
        {
            // A ketosis case this campaign is due to a calving from the previous campaign, so we have to update the output of the past campaign
            boost::gregorian::date lastDayOfPreviousCampaign = getDairyFarm()->getFirstDayOfTheCampaign(simDate) - boost::gregorian::date_duration(1);
            std::map<std::string,Results::TechnicalHealthResult>::iterator it = getDairyFarm()->getCurrentResults()->getHealthResults().TechnicalHealthResults.find(getDairyFarm()->getCurrentResults()->getStrCampaign(lastDayOfPreviousCampaign));
            if (it != getDairyFarm()->getCurrentResults()->getHealthResults().TechnicalHealthResults.end())
            {
                // We have a result for this previous campaign
                Results::TechnicalHealthResult &tr = it->second;
                // Take into account the additional G1 ketosis case due to this additional calvings
                tr.G1ketosisOccurencyDueToCalvingCount += _previousCampaignCalvingSubclinicalKetosisCount;
                // Update the percent calculation for this past campaign
                tr.G1ketosisOccurencyByCalvingCow =  tr.calvingCount > 0 ? (float)tr.G1ketosisOccurencyDueToCalvingCount * 100.0f / (float) tr.calvingCount : 0.0f; // See also the current campaign calculation above

                // Take into account the additional G2 ketosis case due to this additional calvings
                tr.G2ketosisOccurencyDueToCalvingCount += _previousCampaignCalvingClinicalKetosisCount;
                // Update the percent calculation for this past campaign
                tr.G2ketosisOccurencyByCalvingCow =  tr.calvingCount > 0 ? (float)tr.G2ketosisOccurencyDueToCalvingCount * 100.0f / (float) tr.calvingCount : 0.0f; // See also the current campaign calculation above
            }
        }

        // Year technical population report
        // --------------------------------
        Results::TechnicalPopulationResult tpr;
        tpr.date = simDate;
        tpr.meanPresentAdultCowCount = meanAdultCows;
        tpr.birthCount = _annualBirthCount;
        tpr.malesSaleCount = _annualMaleSaleCount;
        tpr.beefCrossBredSaleCount = _annualBeefCrossBredSaleCount;
        tpr.sterileFemalesSaleCount = _annualSterileFemaleSaleCount;
        tpr.extraFemalesSaleCount = _annualExtraFemaleSaleCount;
        tpr.extraPregnantHeifersSaleCount = _annualExtraPregnantHeiferSaleCount;
        tpr.heiferBoughtCount = _annualHeiferBoughtCount;
        tpr.primipareInHerdCount = _annualNewComerInHerdCount;
        tpr.deathCalveCount = _annualDeathCalveCount;
        tpr.deathCalvePer100Calves = _annualBirthCount > 0 ? (float)_annualDeathCalveCount * 100.0f / (float) _annualBirthCount : 0.0f;;
        tpr.deathHeiferCount = _annualDeathHeiferCount;
        tpr.deathAdultCount = _annualMastitisDeathAdultCount + _annualKetosisDeathAdultCount + _annualNonInfectiousLamenessDeathAdultCount + _annualInfectiousLamenessDeathAdultCount + _annualOtherDeathAdultCount;
        tpr.deathAdultPer100Cows = meanAdultCows > 0 ? (float)tpr.deathAdultCount * 100.0f / (float) meanAdultCows : 0.0f;;
        tpr.notBredBecauseOfInfertilityCount = _annualNotBredBecauseOfInfertilityCount;
        tpr.notBredBecauseOfLactationRankCount = _annualNotBredBecauseOfLactationRankCount;
        tpr.notBredBecauseOfLowMilkProductCount = _annualNotBredBecauseOfLowMilkProductCount;
        tpr.cullingRate = meanAdultCows > 0 ? (float)_annualCulledCows * 100.0f / (float) meanAdultCows : 0.0f;
        tpr.healthCulledAnimalCount = _annualMastitisCullingCount + _annualKetosisCullingCount + _annualNonInfectiousLamenessCullingCount + _annualInfectiousLamenessCullingCount;
        tpr.infertilityCulledAnimalCount = _annualInfertilityCulledAnimalCount;
        tpr.lactationRankCulledAnimalCount = _annualLactationRankCulledAnimalCount;
        tpr.lowMilkProductCulledAnimalCount = _annualLowMilkProductCulledAnimalCount;
        tpr.longevity = _annualAllLactationControledCows > 0 ? (float)_annual4OrMoreLactationControledCows * 100.0f / (float) _annualAllLactationControledCows : 0.0f;
        tpr.meanDaysBetweenDecisionAndCulling = _vAnnualDaysBetweenDecisionAndCullingAccumulator.size() > 0 ? std::accumulate(_vAnnualDaysBetweenDecisionAndCullingAccumulator.begin(), _vAnnualDaysBetweenDecisionAndCullingAccumulator.end(), 0) / _vAnnualDaysBetweenDecisionAndCullingAccumulator.size() : 0;
        tpr.meanLactationStageAtCulling =  _vAnnualLactationStageAtCullingAccumulator.size() > 0 ? std::accumulate(_vAnnualLactationStageAtCullingAccumulator.begin(), _vAnnualLactationStageAtCullingAccumulator.end(), 0) / _vAnnualLactationStageAtCullingAccumulator.size() : 0;
        tpr.meanMilkQuantityOfTheDayAtCulling = _vAnnualMilkQuantityOfTheDayAtCullingAccumulator.size() > 0 ? std::accumulate(_vAnnualMilkQuantityOfTheDayAtCullingAccumulator.begin(), _vAnnualMilkQuantityOfTheDayAtCullingAccumulator.end(), 0.0f) / _vAnnualMilkQuantityOfTheDayAtCullingAccumulator.size() : 0.0f;
        tpr.maxMilkQuantityOfTheDayAtCulling = _annualMaxMilkQuantityOfTheDayAtCulling;
        tpr.minLactationStageAtCulling = _annualMinLactationStageAtCulling;
        getDairyFarm()->getCurrentResults()->getPopulationResults().TechnicalPopulationResults.push_back(tpr);

        // Year economical assessment
        // --------------------------
        Results::EconomicalResult er;
        er.date = simDate;         
        er.milkSales = _milkSales;
        er.cullSales = _cullSales;
        er.heiferSales = _heiferSales;
        er.calfSales = _calfSales;
        er.totalProducts =  er.milkSales +
                            er.cullSales +
                            er.heiferSales +
                            er.calfSales;
        er.feedCosts = _feedCosts;
        er.mastitisHealthCosts = _mastitisHealthCosts;
        er.ketosisHealthCosts = _ketosisHealthCosts;
        er.curativeIndividualInfectiousLamenessCosts = _curativeIndividualInfectiousLamenessCosts;
        er.curativeIndividualNonInfectiousLamenessCosts = _curativeIndividualNonInfectiousLamenessCosts;
        er.curativeCollectiveInfectiousLamenessCosts = _curativeCollectiveInfectiousLamenessCosts;
        er.curativeCollectiveNonInfectiousLamenessCosts = _curativeCollectiveNonInfectiousLamenessCosts;
        er.curativeTrimmingCosts = _curativeTrimmingCosts;
        er.overallCurativeLamenessCosts =   _curativeIndividualInfectiousLamenessCosts +
                                            _curativeIndividualNonInfectiousLamenessCosts +
                                            _curativeCollectiveInfectiousLamenessCosts +
                                            _curativeCollectiveNonInfectiousLamenessCosts +
                                            _curativeTrimmingCosts;
        er.preventiveTrimmingCosts = _preventiveTrimmingCosts;
        er.footBathCosts = _footBathCosts;
        er.otherHealthCosts = _otherHealthCosts;
        er.healthCosts =    er.mastitisHealthCosts +
                            er.ketosisHealthCosts +
                            er.overallCurativeLamenessCosts +
                            er.preventiveTrimmingCosts +
                            er.footBathCosts +
                            er.otherHealthCosts;                
        er.reproductionCosts = _reproductionCosts;
        er.populationCosts = _populationCosts;
        er.otherCosts = _otherCosts;
        er.totalExpenses =  er.feedCosts +
                            er.healthCosts +
                            er.reproductionCosts +
                            er.populationCosts +
                            er.otherCosts;
        er.grossMargin =    er.totalProducts -
                            er.totalExpenses;
        getDairyFarm()->getCurrentResults()->getAccountingResults().EconomicalResults.push_back(er);

#ifdef _LOG
        Console::Display::displayLineInConsole(to_simple_string(simDate) + ": end of the campaign");
#endif // _LOG
    
    }


    void DairyHerd::addAnnualCalvingIA1Interval(float ivia1)
    {
        _annualCalvingIA1Interval.push_back(ivia1);
    }

    void DairyHerd::addAnnualCalvingFecundingIAInterval(float icfia)
    {
        _annualCalvingFecundingIAInterval.push_back(icfia);
    }

    void DairyHerd::addAnnualCalvingCalvingInterval(float ci)
    {
        _annualCalvingCalvingInterval.push_back(ci);
    }
    
    void DairyHerd::addAnnualFirstCalvingAge(float fca)
    {
        _annualFirstCalvingAge.push_back(fca);
    } 

    void DairyHerd::increaseAnnualIA1Count(bool success)
    {
        _annualIA1Count++;
        if (success)
        {
            _annualSuccessfulIA1Count++;
        }
    }
    
    void DairyHerd::increaseAnnualIACount()
    {
        _annualIACount++;
    }
    
    void DairyHerd::increaseAnnualCalvingCount()
    {
        _annualCalvingCount++;
    }

    void DairyHerd::increaseReproductionTroubleTreatmentCount()
    {
        _annualReproductionTroubleTreatmentCount++;
    }

    void DairyHerd::increaseReproductionTroubleCullingCount()
    {
        _annualReproductionTroubleCullingCount++;
    }

    void DairyHerd::increaseMastitisCullingCount()
    {
        _annualMastitisCullingCount++;
    }
    
    void DairyHerd::increaseKetosisCullingCount()
    {
        _annualKetosisCullingCount++;
    }
    
    void DairyHerd::increaseNonInfectiousLamenessCullingCount()
    {
        _annualNonInfectiousLamenessCullingCount++;
    }
    
    void DairyHerd::increaseInfectiousLamenessCullingCount()
    {
        _annualInfectiousLamenessCullingCount++;
    }
    
    void DairyHerd::increaseCurativeClinicalKetosisVetTreatmentCount()
    {
        _annualCurativeClinicalKetosisVetTreatmentCount++;
        increaseTotalHealthTreatmentCount();
    }    
    
    void DairyHerd::increaseCurativeClinicalKetosisFarmerTreatmentCount()
    {
        _annualCurativeClinicalKetosisFarmerTreatmentCount++;
        increaseUnscheduledCowFarmerCareCount();
        increaseTotalHealthTreatmentCount();
    }    
    
    void DairyHerd::increaseCurativeSubclinicalKetosisVetTreatmentCount()
    {
        _annualCurativeSubclinicalKetosisVetTreatmentCount++;
        increaseTotalHealthTreatmentCount();
    }   
    
    void DairyHerd::increaseCurativeSubclinicalKetosisFarmerTreatmentCount()
    {
        _annualCurativeSubclinicalKetosisFarmerTreatmentCount++;
        increaseUnscheduledCowFarmerCareCount();
        increaseTotalHealthTreatmentCount();
    }   
    
    void DairyHerd::increaseCurativeClinicalMastitisVetTreatmentCount()
    {
        _annualCurativeClinicalMastitisVetTreatmentCount++;
        increaseTotalHealthTreatmentCount();
    }   
    
    void DairyHerd::increaseCurativeClinicalMastitisFarmerTreatmentCount()
    {
        _annualCurativeClinicalMastitisFarmerTreatmentCount++;
        increaseUnscheduledCowFarmerCareCount();
        increaseTotalHealthTreatmentCount();
    }   
    
    void DairyHerd::increaseCurativeSubclinicalMastitisVetTreatmentCount()
    {
        _annualCurativeSubclinicalMastitisVetTreatmentCount++;
        increaseTotalHealthTreatmentCount();
    }   
    
    void DairyHerd::increaseCurativeSubclinicalMastitisFarmerTreatmentCount()
    {
        _annualCurativeSubclinicalMastitisFarmerTreatmentCount++;
        increaseUnscheduledCowFarmerCareCount();
        increaseTotalHealthTreatmentCount();
    }   
    
//    void DairyHerd::increasePreventiveKetosisVetTreatmentCount()
//    {
//        _annualPreventiveKetosisVetTreatmentCount++;
//        increaseTotalHealthTreatmentCount();
//    }     
//
    void DairyHerd::increasePreventiveKetosisFarmerTreatmentCount()
    {
        _annualPreventiveKetosisFarmerTreatmentCount++;
        increaseScheduledCowFarmerCareCount();
        increaseTotalHealthTreatmentCount();
    }   
    
//    void DairyHerd::increasePreventiveMastitisVetTreatmentCount()
//    {
//        _annualPreventiveMastitisVetTreatmentCount++;
//        increaseTotalHealthTreatmentCount();
//    }   
//    
    void DairyHerd::increasePreventiveMastitisFarmerTreatmentCount()
    {
        _annualPreventiveMastitisFarmerTreatmentCount++;
        increaseScheduledCowFarmerCareCount();
        increaseTotalHealthTreatmentCount();
    }     
    
    void DairyHerd::increaseCurativeClinicalLamenessVetTreatmentCount()
    {
        _annualCurativeClinicalLamenessVetTreatmentCount++;
        increaseTotalHealthTreatmentCount();
    }   
    
    void DairyHerd::increasePreventiveLamenessVetTreatmentCount()
    {
        _annualPreventiveLamenessVetTreatmentCount++;
        increaseTotalHealthTreatmentCount();
    }   
    
    void DairyHerd::increaseUnscheduledCowVetCareCount()
    {
        _annualUnscheduledCowVetCareCount++;
    }   
    
    void DairyHerd::increaseScheduledCowFarmerCareCount()
    {
        _annualScheduledCowFarmerCareCount++;
    }   
    
    void DairyHerd::increaseUnscheduledCowFarmerCareCount()
    {
        _annualUnscheduledCowFarmerCareCount++;
    }   
    
    void DairyHerd::increaseTotalHealthTreatmentCount()
    {
        _annualTotalHealthTreatmentCount++;
    }    
    
    void DairyHerd::increaseScheduledFarmerActivityCount(unsigned int count)
    {
        _annualScheduledFarmerActivityCount += count;
    }
    
    void DairyHerd::increaseUnscheduledFarmerActivityCount()
    {
        _annualUnscheduledFarmerActivityCount++;
    }
    
    void DairyHerd::increaseScheduledVetActivityCount(unsigned int count)
    {
        _annualScheduledVetActivityCount += count;
    }
    
    void DairyHerd::increaseUnscheduledVetActivityCount()
    {
        _annualUnscheduledVetActivityCount++;
    }
    
    void DairyHerd::accountNewTransaction(
#ifdef _LOG                
                                    const boost::gregorian::date &transactionDate, std::string comp, 
#endif // _LOG                
                                    float amount, float &annualDataToSet)
    {
#ifdef _LOG                
        Results::Transaction t;
        t.date = transactionDate;
        t.amount = amount;
        t.comp = comp;
        getDairyFarm()->getCurrentResults()->getAccountingResults().Transactions.push_back(t);
#endif // _LOG                
        annualDataToSet += abs(amount);
    }
     
    float DairyHerd::getMeanAdultCowCount()
    {
        float mean = 0.0f;
        if (_vAnnualPresentAdultCowAccumulator.size() > 0)
        {
            for (auto it : _vAnnualPresentAdultCowAccumulator)
            {
                mean += it;
            }
            mean /= _vAnnualPresentAdultCowAccumulator.size();
        }
        return mean;
    }
    
#ifdef _LOG
    // Publishing the final results (at the end of a run)
    void DairyHerd::closeResults(const boost::gregorian::date &finalDate)
    {
        // Only in debug mode
        for (auto itAnim : _mpIsComposedAnimal)
        {
           (itAnim.second)->closeResults(finalDate);
        }
    }
#endif // _LOG
    
    unsigned int DairyHerd::getUnderOneYearOldFemaleCalfNumberForThisCampaign(const boost::gregorian::date &simDate)
    {
        return getBatch(FunctionalEnumerations::Population::BatchType::femaleCalves)->getBornAnimalDuringThisCampaign(simDate);
    }
    
    void DairyHerd::increaseAnnualControledCowsCount(bool parity4OrMoreLactations)
    {
        _annualAllLactationControledCows++;
        if (parity4OrMoreLactations)
        {
            _annual4OrMoreLactationControledCows++;
        }
    }
    
    void DairyHerd::increaseAnnualBirthCount()
    {
        _annualBirthCount++;
    }
    
    void DairyHerd::increaseAnnualMaleSaleCount()
    {
        _annualMaleSaleCount++;
    }

    void DairyHerd::increaseAnnualBeefCrossBredCount()
    {
        _annualBeefCrossBredSaleCount++;
    }
        
    void DairyHerd::increaseAnnualSterileFemaleSaleCount()
    {
        _annualSterileFemaleSaleCount++;
    }
        
    void DairyHerd::increaseAnnualExtraFemaleSaleCount()
    {
        _annualExtraFemaleSaleCount++;
    }
    
    void DairyHerd::increaseAnnualExtraPregnantHeiferSaleCount()
    {
        _annualExtraPregnantHeiferSaleCount++;
    }
  
    void DairyHerd::increaseAnnualHeiferBoughtCount()
    {
        _annualHeiferBoughtCount++;
    }
        
    void DairyHerd::increaseAnnualDeathCount(Population::Batch* pBatch, FunctionalEnumerations::Population::DeathReason deathReason)
    {
        if      (
                pBatch->getType() == FunctionalEnumerations::Population::BatchType::toSaleCalves or
                pBatch->getType() == FunctionalEnumerations::Population::BatchType::femaleCalves
                )
        {
            _annualDeathCalveCount++;           
        }
        else if (
                pBatch->getType() == FunctionalEnumerations::Population::BatchType::weanedCalves or
                pBatch->getType() == FunctionalEnumerations::Population::BatchType::youngUnbredHeifers or
                pBatch->getType() == FunctionalEnumerations::Population::BatchType::oldUnbredHeifers or
                pBatch->getType() == FunctionalEnumerations::Population::BatchType::bredHeifers
                )
        {
            _annualDeathHeiferCount++;           
        }
        else if (
                pBatch->getType() == FunctionalEnumerations::Population::BatchType::driedCowsAndPregnantHeifers or
                pBatch->getType() == FunctionalEnumerations::Population::BatchType::periParturientFemales or
                pBatch->getType() == FunctionalEnumerations::Population::BatchType::lactatingCows
                )
        {
            if (deathReason == FunctionalEnumerations::Population::DeathReason::mastitisDeathReason)
            {
                _annualMastitisDeathAdultCount++;   
            }
            else if (deathReason == FunctionalEnumerations::Population::DeathReason::ketosisDeathReason)
            {
                _annualKetosisDeathAdultCount++;   
            }
            else if (deathReason == FunctionalEnumerations::Population::DeathReason::nonInfectiousLamenessDeathReason)
            {
                _annualNonInfectiousLamenessDeathAdultCount++;   
            }
            else if (deathReason == FunctionalEnumerations::Population::DeathReason::infectiousLamenessDeathReason)
            {
                _annualInfectiousLamenessDeathAdultCount++;   
            }
            else
            {
                _annualOtherDeathAdultCount++;           
            }
        }
        else
        {
            throw std::runtime_error("DairyHerd::increaseAnnualDeathCount : not relevant Batch");
        }
    }
    
    void DairyHerd::increaseAnnualNotBredBecauseOfInfertilityCount()
    {
        _annualNotBredBecauseOfInfertilityCount++;
    }
    
    void DairyHerd::increaseAnnualNotBredBecauseOfLactationRankCount()
    {
        _annualNotBredBecauseOfLactationRankCount++;
    }
    
    void DairyHerd::increaseAnnualNotBredBecauseOfLowMilkProductCount()
    {
        _annualNotBredBecauseOfLowMilkProductCount++;
    }
    
    void DairyHerd::updateCullingResults(DataStructures::Animal* pAnim, const boost::gregorian::date &cullDate)
    {
        _annualCulledCows++;
        
        // Heath reason ?
        if (pAnim->getCullingStatus() == FunctionalEnumerations::Population::CullingStatus::toCullDueToMastitis)
        {
            _annualMastitisCullingCount++;;
        }
        else if (pAnim->getCullingStatus() == FunctionalEnumerations::Population::CullingStatus::toCullDueToKetosis)
        {
            _annualKetosisCullingCount++;;
        }
        else if (pAnim->getCullingStatus() == FunctionalEnumerations::Population::CullingStatus::toCullDueToNonInfectiousLameness)
        {
            _annualNonInfectiousLamenessCullingCount++;;
        }
        else if (pAnim->getCullingStatus() == FunctionalEnumerations::Population::CullingStatus::toCullDueToInfectiousLameness)
        {
            _annualInfectiousLamenessCullingCount++;;
        }
        else 
        {
            // Not a health reason
            if (pAnim->getCullingStatus() == FunctionalEnumerations::Population::CullingStatus::toCullDueToUnfertility)
            {
                _annualInfertilityCulledAnimalCount++;
            }
            else if (pAnim->getCullingStatus() == FunctionalEnumerations::Population::CullingStatus::toCullDueToLactationRank)
            {
               _annualLactationRankCulledAnimalCount++;
            }
            else if (pAnim->getCullingStatus() == FunctionalEnumerations::Population::CullingStatus::toCullDueToLowMilkProduct)
            {
                _annualLowMilkProductCulledAnimalCount++;
            }
            else if (pAnim->getCullingStatus() != FunctionalEnumerations::Population::CullingStatus::toCullDueToPopulationRegulation)
            {
                Console::Display::displayLineInConsole("DairyHerd::updateCullingResults() : unmanaged pAnim->getCullingStatus() : " + pAnim->getCullingStatus());
            }
            
            // Time between decision and culling
            _vAnnualDaysBetweenDecisionAndCullingAccumulator.push_back((cullDate - pAnim->getCullingDecisionDate()).days());
            
            DataStructures::DairyMammal* pDM = (DataStructures::DairyMammal*)pAnim;
            if (pDM->lactationIsOngoing())
            {
                // Lactation stage at culling
                _vAnnualLactationStageAtCullingAccumulator.push_back(pDM->getLactationStage());
                
                // Day milk quantity at culling
                float milkOfTheDay = pDM->getMilkQuantityOfTheDay();
                _vAnnualMilkQuantityOfTheDayAtCullingAccumulator.push_back(milkOfTheDay);
                
                // Max milk quantity at culling
                if (_annualMaxMilkQuantityOfTheDayAtCulling < milkOfTheDay) _annualMaxMilkQuantityOfTheDayAtCulling = milkOfTheDay;
                
                // Min lactation stage at culling
                if (_annualMinLactationStageAtCulling == 0 or pDM->getLactationStage() < (int)_annualMinLactationStageAtCulling)
                {
                    _annualMinLactationStageAtCulling = (unsigned int)pDM->getLactationStage();
                }
            }
        }
    }
    
    bool DairyHerd::cowIsToCullLaterRegardingPopulation(DairyMammal* pCow)
    {
        return Population::PopulationManagement::cowIsToCullLaterRegardingPopulation(pCow, this);
    }

    unsigned int DairyHerd::getHighterLactationRankFromNotCulledDecisionLactatingCows()
    {
        std::map<unsigned int, DataStructures::Animal*> &animals = getBatch(FunctionalEnumerations::Population::BatchType::lactatingCows)->getAnimalList();
        unsigned int res = 0;
        for (auto it = animals.begin(); it != animals.end(); it++)
        {
            DataStructures::DairyMammal* pCow = (DataStructures::DairyMammal*) (*it).second;
            if (_mpAnimalsToCullLater.find(pCow->getUniqueId()) == _mpAnimalsToCullLater.end())
            {
                unsigned int lr = pCow->getLactationRank();
                if (res < lr) res = lr;
            }
        }
        return res;
    }
    
    bool DairyHerd::isThisCowTheLowerGeneticValueCowFromNotCulledDecisionLactatingCows(DairyMammal* pCandidateCow)
    {
        std::map<unsigned int, DataStructures::Animal*> &animals = getBatch(FunctionalEnumerations::Population::BatchType::lactatingCows)->getAnimalList();
        for (auto it = animals.begin(); it != animals.end(); it++)
        {
            DataStructures::DairyMammal* pCow = (DataStructures::DairyMammal*) (*it).second;
            if (_mpAnimalsToCullLater.find(pCow->getUniqueId()) == _mpAnimalsToCullLater.end())
            {
                if (pCow->getGeneticValue() < pCandidateCow->getGeneticValue())
                {
                    return false;
                }
            }
        }
        return true;
    }

    bool DairyHerd::isThisCowNotToInseminateRegardingOtherNotCulledDecisionLactatingCows(DairyMammal* pCandidateCow)
    {
        // Cow not to inseminate decision, may depends on the genetic note regarding the other lactating and not culled decided cows of the herd
         //return isThisCowTheLowerGeneticValueCowFromNotCulledDecisionLactatingCows(pCandidateCow);
        std::map<unsigned int, DataStructures::Animal*> &animals = getBatch(FunctionalEnumerations::Population::BatchType::lactatingCows)->getAnimalList();
        
        float cumulativeNote = 0.0f;
        unsigned int concerned = 0;
        for (auto it = animals.begin(); it != animals.end(); it++)
        {
            DataStructures::DairyMammal* pCow = (DataStructures::DairyMammal*) (*it).second;
            if (_mpAnimalsToCullLater.find(pCow->getUniqueId()) == _mpAnimalsToCullLater.end())
            {
                cumulativeNote+= pCow->getGeneticValue().getGeneticNote();
                concerned++;
            }
        }
        if (concerned != 0)
        {
            float meanHerdGeneticNote = cumulativeNote / (float)concerned;
            return pCandidateCow->getGeneticValue().getGeneticNote() < meanHerdGeneticNote;
        }
        else
        {
            return false;
        }
    }
    
    void DairyHerd::increaseAnnualNewComerInHerdCount()
    {
        _annualNewComerInHerdCount++;
    }
    
    void DairyHerd::decreaseAnnualNewComerInHerdCount()
    {
        if (_annualNewComerInHerdCount > 0)
        {
            _annualNewComerInHerdCount--;
        }
    }
    
    unsigned int DairyHerd::getAnnualNewComerInHerdCount()
    {
        return _annualNewComerInHerdCount;
    }
    
    void DairyHerd::addHeifersInDriedCowsAndPregnantHeifersBatchOfTheDay(DairyMammal* pDairyMammal)
    {
        _heifersInDriedCowsAndPregnantHeifersBatchOfTheDay.push_back(pDairyMammal);
    }

    std::vector<DairyMammal*> &DairyHerd::getHeifersInDriedCowsAndPregnantHeifersBatchOfTheDay()
    {
        return _heifersInDriedCowsAndPregnantHeifersBatchOfTheDay;
    }
        
#ifdef _LOG
    void DairyHerd::updateWeekResults(Results::WeekAnimalPopulationInformations &weekRes)
    {
        // group count
        weekRes.calfPopulation = getCalvesCount();
        weekRes.heiferPopulation = getHeiferCount();
        weekRes.adultPopulation = getAdultCount();
    }
#endif // _LOG   
    
    // to know the target population in each group
    void DairyHerd::addMyMilkProductionForTheDay(   ExchangeInfoStructures::MilkProductCharacteristic &grossDeliveredMilkOfTheDay, ExchangeInfoStructures::MilkProductCharacteristic &grossDiscardedMilkOfTheDay)
            
    {
        _grossDeliveredMilkOfTheDay += grossDeliveredMilkOfTheDay;
        _grossDiscardedMilkOfTheDay += grossDiscardedMilkOfTheDay;
    }
    
    void DairyHerd::addPonderedMilkQuantityBetweenCalvingAndBreedDate(float ponderedMilkQuantity, boost::gregorian::date date)
    {
        _vPonderedMilkQuantityBetweenCalvingAndBreedDate.push_back(std::make_tuple(ponderedMilkQuantity, date));        
    }

    float DairyHerd::getMeanPonderedMilkQuantityBetweenCalvingAndBreedDate(boost::gregorian::date simDate)
    {
        // First we clean the old values (over 1 year)
        for (std::vector<std::tuple<float, boost::gregorian::date>>::iterator it = _vPonderedMilkQuantityBetweenCalvingAndBreedDate.begin(); it != _vPonderedMilkQuantityBetweenCalvingAndBreedDate.end(); )
        {
            boost::gregorian::date dateYearBefore = simDate - boost::gregorian::years(1);
            if (std::get<1>(*it) < dateYearBefore)
            {
                it = _vPonderedMilkQuantityBetweenCalvingAndBreedDate.erase(it);
            }
            else
            {
                break;
            }
        }        
        
        // Now, we calculate the mean value
        float sum = 0.0f;
        for (std::vector<std::tuple<float, boost::gregorian::date>>::iterator it = _vPonderedMilkQuantityBetweenCalvingAndBreedDate.begin(); it != _vPonderedMilkQuantityBetweenCalvingAndBreedDate.end(); it++)
        {
            sum += std::get<0>(*it);
        }
        if (_vPonderedMilkQuantityBetweenCalvingAndBreedDate.size() > 0)
        {
            return sum / _vPonderedMilkQuantityBetweenCalvingAndBreedDate.size();
        }
        else
        {
            return 0.0f;
        }
    }

    ExchangeInfoStructures::MilkProductCharacteristic &DairyHerd::getMilkSunnyAdjustmentOfTheDay()
    {
        return _milkSunnyAdjustmentOfTheDay;
    }
    
    void DairyHerd::useBulkMilkForCalf(float consumption)
    {
        _grossDeliveredMilkOfTheDay -= consumption;
        _bulkMilkCalfConsumptionOfTheDay += consumption;
    }
    
    void DairyHerd::useDiscardedMilkForCalf(float &need)
    {
        if (_grossDiscardedMilkOfTheDay.quantity > need)
        {
            // Enought quantity
            _grossDiscardedMilkOfTheDay -= need;
            _discardedMilkCalfConsumptionOfTheDay += need;
            need = 0.0f; // We don't need anymore
        }
        else
        {
            _discardedMilkCalfConsumptionOfTheDay = _grossDiscardedMilkOfTheDay.quantity; // Consume all
            need -= _grossDiscardedMilkOfTheDay.quantity; // We need more
            _grossDiscardedMilkOfTheDay.init(); // Nothing remaining
        }
    }
    
    void DairyHerd::useDehydratedMilkForCalf(float consumption)
    
    {
        _dehydratedMilkCalfConsumptionOfTheDay += getDehydratedMilkKgFromMilkKg(consumption);
    }

    void DairyHerd::useForageForCalf(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &consumption)
    {
        _forageCalfConsumptionOfTheDay += consumption;
    }
    
    void DairyHerd::useConcentrateForCalf(ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &consumption)
    {
        _concentrateCalfConsumptionOfTheDay += consumption;
    }
    
    void DairyHerd::useMineralVitaminForCalf(float consumption)
    {
        _mineralVitaminCalfConsumptionOfTheDay += consumption;
    }
    
    void DairyHerd::useForageForHeifer(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &consumption)
    {
        _forageHeiferConsumptionOfTheDay += consumption;
    }
    
    void DairyHerd::useConcentrateForHeifer(ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &consumption)
    {
        _concentrateHeiferConsumptionOfTheDay += consumption;
    }

    void DairyHerd::useMineralVitaminForHeifer(float consumption)
    {
        _mineralVitaminHeiferConsumptionOfTheDay += consumption;
    }
    
    void DairyHerd::useForageForCow(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &consumption)
    {
        _forageCowConsumptionOfTheDay += consumption;
    }
    
    void DairyHerd::useConcentrateForCow(ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &consumption)
    {
        _concentrateCowConsumptionOfTheDay += consumption;
    }

    void DairyHerd::useMineralVitaminForCow(float consumption)
    {
        _mineralVitaminCowConsumptionOfTheDay += consumption;
    }

    void DairyHerd::notifyNewClinicalMastitis(bool alreadyHadOne)
    {
        _annualClinicalMastitisCount++;
        if (!alreadyHadOne)
        {
            _annualFirstClinicalMastitisCount++;
        }
    }
    
    void DairyHerd::notifyNewSubclinicalMastitis()
    {
        _annualSubclinicalMastitisCount++;
    }
    
    void DairyHerd::notifyNewSubclinicalKetosis(bool alreadyHadOne, const boost::gregorian::date &date, const boost::gregorian::date lastCalvingDate)
    {
        assert (!lastCalvingDate.is_not_a_date());
        if (getDairyFarm()->getCurrentResults()->getStrCampaign(lastCalvingDate) != getDairyFarm()->getCurrentResults()->getStrCampaign(date))
        {
            // The last calving was during previous campaign
            _previousCampaignCalvingSubclinicalKetosisCount++;
        }
        else
        {
            // The last calving was during this campaign
            _thisCampaignCalvingSubclinicalKetosisCount++;            
        }
        if (!alreadyHadOne)
        {
            _annualFirstSubclinicalKetosisCount++;
        }
    }
    
    void DairyHerd::notifyNewClinicalKetosis(bool alreadyHadOne, const boost::gregorian::date &date, const boost::gregorian::date lastCalvingDate)
    {
        _annualClinicalKetosisCount++;
        if (getDairyFarm()->getCurrentResults()->getStrCampaign(lastCalvingDate) != getDairyFarm()->getCurrentResults()->getStrCampaign(date))
        {
            // The last calving was during previous campaign
            _previousCampaignCalvingClinicalKetosisCount++;
        }
        else
        {
            // The last calving was during this campaign
            _thisCampaignCalvingClinicalKetosisCount++;            
        }
        if (!alreadyHadOne)
        {
            _annualFirstClinicalKetosisCount++;
        }
    }
        
    void DairyHerd::notifyNewG1InfectiousLamenessOccurence(const boost::gregorian::date &date)
    {
        _annualG1InfectiousLamenessCount++;
    }
        
    void DairyHerd::notifyNewG1NonInfectiousLamenessOccurence(const boost::gregorian::date &date)
    {
        _annualG1NonInfectiousLamenessCount++;
    }
        
    void DairyHerd::notifyNewG2InfectiousLamenessOccurence(const boost::gregorian::date &date)
    {
        _annualG2InfectiousLamenessCount++;
    }
        
    void DairyHerd::notifyNewG2NonInfectiousLamenessOccurence(const boost::gregorian::date &date)
    {
        _annualG2NonInfectiousLamenessCount++;
    }
} /* End of namespace DataStructures */
