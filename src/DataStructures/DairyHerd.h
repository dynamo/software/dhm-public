#ifndef DataStructures_DairyHerd_h
#define DataStructures_DairyHerd_h

// project
#include "Herd.h"
#include "../Tools/Serialization.h"
#include "../ExchangeInfoStructures/MilkProductCharacteristic.h"
#include "../ExchangeInfoStructures/MixtureCharacteristicParameters.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"
#include "../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "DairyMammal.h"

namespace DataStructures
{
    class DairyMammal;
    class DairyFarm;
    
    namespace Population
    {
        class Batch;   
    }

    /// Specialized Herd for dairy product
    class DairyHerd : public Herd
    {        
    private:
        
        DECLARE_SERIALIZE_STD_METHODS;
        DairyHerd(){}; // serialization constructor
                
        // Daily informations
        // ------------------
        // Population
        std::vector<DairyMammal*> _heifersInDriedCowsAndPregnantHeifersBatchOfTheDay; // Heifers in the FunctionalEnumerations::Population::BatchType::driedCowsAndPregnantHeifers
        
        // Milk production
        float _minimumMilkQuantityForRentability = 0.0f;
        ExchangeInfoStructures::MilkProductCharacteristic _milkProductionObjectiveFactor; // Factor depending on the initial breed production capability and the farm production decision
        ExchangeInfoStructures::MilkProductCharacteristic _grossDeliveredMilkOfTheDay;
        ExchangeInfoStructures::MilkProductCharacteristic _grossDiscardedMilkOfTheDay;
        ExchangeInfoStructures::MilkProductCharacteristic _milkSunnyAdjustmentOfTheDay;
        
        // Feeding
        float _discardedMilkCalfConsumptionOfTheDay = 0.0f;
        float _bulkMilkCalfConsumptionOfTheDay = 0.0f;
        float _dehydratedMilkCalfConsumptionOfTheDay = 0.0f;
        ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters _forageCalfConsumptionOfTheDay;
        ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters _concentrateCalfConsumptionOfTheDay;
        float _mineralVitaminCalfConsumptionOfTheDay = 0.0f;
        
        ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters _forageHeiferConsumptionOfTheDay;
        ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters _concentrateHeiferConsumptionOfTheDay;
        float _mineralVitaminHeiferConsumptionOfTheDay = 0.0f;
        
        ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters _forageCowConsumptionOfTheDay;
        ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters _concentrateCowConsumptionOfTheDay;
        float _mineralVitaminCowConsumptionOfTheDay = 0.0f;
        
        // Monthly informations
        // --------------------
        // Milk production
        ExchangeInfoStructures::MilkProductCharacteristic _milkTank;
        unsigned int _dayRemainingBeforeMilkCollecting = FunctionalConstants::Lactation::MILK_DELIVERY_FREQUENCY;
        ExchangeInfoStructures::MilkProductCharacteristic _lastMilkTankCollected;
        ExchangeInfoStructures::MilkProductCharacteristic _collectedMilkTestResult1;
        ExchangeInfoStructures::MilkProductCharacteristic _collectedMilkTestResult2;
        ExchangeInfoStructures::MilkProductCharacteristic _collectedMilkTestResult3;
        ExchangeInfoStructures::MilkProductCharacteristic _deliveredMilkOfTheMonth;
        ExchangeInfoStructures::MilkProductCharacteristic _grossDiscardedMilkOfTheMonth;
        
        // Annual informations
        // -------------------
        
             
        // Reproduction
        std::vector<float> _annualCalvingIA1Interval;
        unsigned int _annualSuccessfulIA1Count = 0;
        std::vector<float> _annualCalvingFecundingIAInterval;
        std::vector<float> _annualCalvingCalvingInterval;
        std::vector<float> _annualFirstCalvingAge;
        unsigned int _annualIA1Count = 0;
        unsigned int _annualIACount = 0;
        unsigned int _annualReproductionTroubleTreatmentCount = 0;
        unsigned int _annualReproductionTroubleCullingCount = 0;
        unsigned int _annualCalvingCount = 0;
        unsigned int _previousCampaignCalvingCount = 0; 
        
        // Lactation
        unsigned int _annualAllLactationControledCows = 0;
        unsigned int _annual4OrMoreLactationControledCows = 0;
        ExchangeInfoStructures::MilkProductCharacteristic _deliveredMilkOfTheCampaign;
        ExchangeInfoStructures::MilkProductCharacteristic _producedMilkOfTheCampaign;    
        unsigned int _previousCampaignDeliveredKiloliter = 0; 
        
        // Population
        unsigned int _annualBirthCount = 0;
        unsigned int _annualMaleSaleCount = 0;
        unsigned int _annualBeefCrossBredSaleCount = 0;
        unsigned int _annualSterileFemaleSaleCount = 0;
        unsigned int _annualExtraFemaleSaleCount = 0;
        unsigned int _annualExtraPregnantHeiferSaleCount = 0;
        unsigned int _annualHeiferBoughtCount = 0;
        unsigned int _annualDeathCalveCount = 0;
        unsigned int _annualDeathHeiferCount = 0;
        unsigned int _annualMastitisDeathAdultCount = 0;
        unsigned int _annualKetosisDeathAdultCount = 0;
        unsigned int _annualNonInfectiousLamenessDeathAdultCount = 0;
        unsigned int _annualInfectiousLamenessDeathAdultCount = 0;
        unsigned int _annualOtherDeathAdultCount = 0;
        unsigned int _annualNotBredBecauseOfInfertilityCount = 0;
        unsigned int _annualNotBredBecauseOfLactationRankCount = 0;
        unsigned int _annualNotBredBecauseOfLowMilkProductCount = 0;
        unsigned int _annualCulledCows = 0;
        unsigned int _annualInfertilityCulledAnimalCount = 0;        
        unsigned int _annualLactationRankCulledAnimalCount = 0;
        unsigned int _annualLowMilkProductCulledAnimalCount = 0;
        unsigned int _annualNewComerInHerdCount = 0; // count of pregnant heifer entered in the herd, to regulate the renewal ratio
        std::vector<unsigned int> _vAnnualDaysBetweenDecisionAndCullingAccumulator; // Except for health reasons
        std::vector<unsigned int> _vAnnualLactationStageAtCullingAccumulator; // Except for health reasons
        std::vector<float> _vAnnualMilkQuantityOfTheDayAtCullingAccumulator; // Except for health reasons
        float _annualMaxMilkQuantityOfTheDayAtCulling = 0.0f; // Except for health reasons
        unsigned int _annualMinLactationStageAtCulling = 0; // Except for health reasons
        unsigned int _previousCampaignMeanAdultCowCount = 0; 
        
        // Health
        unsigned int _annualTotalHealthTreatmentCount = 0;
        unsigned int _annualCurativeClinicalKetosisVetTreatmentCount = 0;
        unsigned int _annualCurativeClinicalKetosisFarmerTreatmentCount = 0;
        unsigned int _annualCurativeSubclinicalKetosisVetTreatmentCount = 0;
        unsigned int _annualCurativeSubclinicalKetosisFarmerTreatmentCount = 0;
        unsigned int _annualCurativeClinicalMastitisVetTreatmentCount = 0;
        unsigned int _annualCurativeClinicalMastitisFarmerTreatmentCount = 0;
        unsigned int _annualCurativeSubclinicalMastitisVetTreatmentCount = 0;
        unsigned int _annualCurativeSubclinicalMastitisFarmerTreatmentCount = 0;
        //unsigned int _annualPreventiveKetosisVetTreatmentCount = 0;
        unsigned int _annualPreventiveKetosisFarmerTreatmentCount = 0;
        //unsigned int _annualPreventiveMastitisVetTreatmentCount = 0;
        unsigned int _annualPreventiveMastitisFarmerTreatmentCount = 0;
        unsigned int _annualCurativeClinicalLamenessVetTreatmentCount = 0;
        unsigned int _annualPreventiveLamenessVetTreatmentCount = 0;
        unsigned int _annualUnscheduledCowVetCareCount = 0;
        unsigned int _annualScheduledCowFarmerCareCount = 0;
        unsigned int _annualUnscheduledCowFarmerCareCount = 0;
        unsigned int _annualScheduledVetActivityCount = 0;
        unsigned int _annualUnscheduledVetActivityCount = 0;
        unsigned int _annualScheduledFarmerActivityCount = 0;
        unsigned int _annualUnscheduledFarmerActivityCount = 0;
        
        // Mastitis
        std::map<bool, unsigned int> _vLowSCCControlResultOfTheYear {{false, 0}, {true, 0}};
        std::vector<unsigned int> _vAnnualPresentAdultCowAccumulator;
        std::vector<float> _vAnnualSCCAccumulator;
        unsigned int _annualSubclinicalMastitisCount = 0;
        unsigned int _annualClinicalMastitisCount = 0;
        unsigned int _annualFirstClinicalMastitisCount = 0;
        unsigned int _annualMastitisCullingCount = 0;
        
        // Ketosis
        unsigned int _previousCampaignCalvingSubclinicalKetosisCount = 0;
        unsigned int _thisCampaignCalvingSubclinicalKetosisCount = 0;
        unsigned int _annualClinicalKetosisCount = 0;
        unsigned int _previousCampaignCalvingClinicalKetosisCount = 0;
        unsigned int _thisCampaignCalvingClinicalKetosisCount = 0;
        unsigned int _annualFirstSubclinicalKetosisCount = 0;
        unsigned int _annualFirstClinicalKetosisCount = 0;
        unsigned int _annualKetosisCullingCount = 0;
                        
        // Lameness
        unsigned int _annualG1NonInfectiousLamenessCount = 0;
        unsigned int _annualG2NonInfectiousLamenessCount = 0;
        unsigned int _annualG1InfectiousLamenessCount = 0;
        unsigned int _annualG2InfectiousLamenessCount = 0;
        unsigned int _annualNonInfectiousLamenessCullingCount = 0;
        unsigned int _annualInfectiousLamenessCullingCount = 0;
       
        // Feeding
        float _discardedMilkCalfConsumptionOfTheYear = 0.0f;
        float _bulkMilkCalfConsumptionOfTheYear = 0.0f;
        float _dehydratedMilkCalfConsumptionOfTheYear = 0.0f;
        ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters _forageCalfConsumptionOfTheYear;
        ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters _concentrateCalfConsumptionOfTheYear;
        float _mineralVitaminCalfConsumptionOfTheYear = 0.0f;
        ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters _forageHeiferConsumptionOfTheYear;
        ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters _concentrateHeiferConsumptionOfTheYear;
        float _mineralVitaminHeiferConsumptionOfTheYear = 0.0f;
        ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters _forageCowConsumptionOfTheYear;
        ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters _concentrateCowConsumptionOfTheYear;
        float _mineralVitaminCowConsumptionOfTheYear = 0.0f;
        
        // Accounting
        float _milkSales = 0.0f;
        float _cullSales = 0.0f;
        float _heiferSales = 0.0f;
        float _calfSales = 0.0f;
        float _feedCosts = 0.0f;
        float _mastitisHealthCosts = 0.0f;
        float _ketosisHealthCosts = 0.0f;
        float _curativeIndividualInfectiousLamenessCosts = 0.0f;
        float _curativeIndividualNonInfectiousLamenessCosts = 0.0f;
        float _curativeCollectiveInfectiousLamenessCosts = 0.0f;
        float _curativeCollectiveNonInfectiousLamenessCosts = 0.0f;
        float _curativeTrimmingCosts = 0.0f;
        float _preventiveTrimmingCosts = 0.0f;
        float _footBathCosts = 0.0f;
        float _otherHealthCosts = 0.0f;
        float _reproductionCosts = 0.0f;
        float _populationCosts = 0.0f;
        float _otherCosts = 0.0f;
        
        // Others :
        // --------
        std::vector<std::tuple<float, boost::gregorian::date>> _vPonderedMilkQuantityBetweenCalvingAndBreedDate; // To know begin lactation level to study insemination opportunity
        
    private:
        unsigned int getHighterLactationRankFromNotCulledDecisionLactatingCows();
        bool isThisCowTheLowerGeneticValueCowFromNotCulledDecisionLactatingCows(DairyMammal* pCandidateCow);

    protected:
        void concrete(){}; // To allow instanciation

    public :
        virtual ~DairyHerd(); // Destructor
        DairyHerd(DairyFarm* pFarmExpl, Genetic::Breed::Breed* pInitialBreed, ExchangeInfoStructures::MilkProductCharacteristic &herdProductionDeltaObjective, float minimumMilkQuantityFactorForRentability); // default constructor
        float getMinimumMilkQuantityForRentability();
        ExchangeInfoStructures::MilkProductCharacteristic &getMilkProductionObjectiveFactor();
        void progress(const boost::gregorian::date &simDate) override; // progress of duration time and unit
        void produceEndCampaignResults(const boost::gregorian::date &simDate) override;
        float getMeanAdultCowCount();
#ifdef _LOG
        void closeResults(const boost::gregorian::date &finalDate) override;
        void updateWeekResults(Results::WeekAnimalPopulationInformations &weekRes);
#endif // _LOG
        
        // Reproduction methods
        void addAnnualCalvingIA1Interval(float ivia1);
        void addAnnualCalvingFecundingIAInterval(float icfia);
        void addAnnualCalvingCalvingInterval(float ci);
        void addAnnualFirstCalvingAge(float fca);
        void increaseAnnualIA1Count(bool success);
        void increaseAnnualIACount();
        void increaseAnnualCalvingCount();
        void increaseReproductionTroubleTreatmentCount();
        void increaseReproductionTroubleCullingCount();
        inline unsigned int getPreviousCampaignCalvingCount() {return _previousCampaignCalvingCount;} 
        
        // Milking methods
        ExchangeInfoStructures::MilkProductCharacteristic &getMilkSunnyAdjustmentOfTheDay();
        void addMyMilkProductionForTheDay(   ExchangeInfoStructures::MilkProductCharacteristic &grossDeliveredMilkOfTheDay, ExchangeInfoStructures::MilkProductCharacteristic &grossDiscardedMilkOfTheDay);
        void addPonderedMilkQuantityBetweenCalvingAndBreedDate(float ponderedMilkQuantity, boost::gregorian::date date);
        float getMeanPonderedMilkQuantityBetweenCalvingAndBreedDate(boost::gregorian::date simDate);
        void increaseAnnualControledCowsCount(bool parity4OrMoreLactations);
        inline unsigned int getPreviousCampaignDeliveredKiloliterCount() {return _previousCampaignDeliveredKiloliter;} 
        
        // Population methods
        unsigned int getUnderOneYearOldFemaleCalfNumberForThisCampaign(const boost::gregorian::date &simDate);
        void increaseAnnualBirthCount() override;
        void increaseAnnualMaleSaleCount() override;
        void increaseAnnualBeefCrossBredCount() override;
        void increaseAnnualSterileFemaleSaleCount() override;
        void increaseAnnualExtraFemaleSaleCount() override;
        void increaseAnnualExtraPregnantHeiferSaleCount() override;
        void increaseAnnualHeiferBoughtCount() override;
        void increaseAnnualDeathCount(Population::Batch* pBatch, FunctionalEnumerations::Population::DeathReason deathReason) override;
        void increaseAnnualNotBredBecauseOfInfertilityCount();
        void increaseAnnualNotBredBecauseOfLactationRankCount();
        void increaseAnnualNotBredBecauseOfLowMilkProductCount();
        void updateCullingResults(DataStructures::Animal* pAnim, const boost::gregorian::date &cullDate) override;
        void increaseAnnualNewComerInHerdCount();
        void decreaseAnnualNewComerInHerdCount();
        unsigned int getAnnualNewComerInHerdCount();
        bool cowIsToCullLaterRegardingPopulation(DairyMammal* pCow);
        bool isThisCowNotToInseminateRegardingOtherNotCulledDecisionLactatingCows(DairyMammal* pCandidateCow);
        void addHeifersInDriedCowsAndPregnantHeifersBatchOfTheDay(DairyMammal* pDairyMammal);
        std::vector<DairyMammal*> &getHeifersInDriedCowsAndPregnantHeifersBatchOfTheDay();
        inline unsigned int getPreviousCampaignMeanAdultCowCount() {return _previousCampaignMeanAdultCowCount;} 
        
        // Feeding methods
        static float getDehydratedMilkKgFromMilkKg(float milkKg);
        void useBulkMilkForCalf(float consumption);
        void useDiscardedMilkForCalf(float &need);
        void useDehydratedMilkForCalf(float consumption);
        void useForageForCalf(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &consumption);
        void useConcentrateForCalf(ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &consumption);
        void useMineralVitaminForCalf(float consumption);
        void useForageForHeifer(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &consumption);
        void useConcentrateForHeifer(ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &consumptionconsumption);
        void useMineralVitaminForHeifer(float consumption);
        void useForageForCow(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters &consumption);
        void useConcentrateForCow(ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters &consumptionconsumption);
        void useMineralVitaminForCow(float consumption);
        
        // Health methods
        void notifyNewSubclinicalMastitis() override;
        void notifyNewClinicalMastitis(bool alreadyHadOne) override;
        void notifyNewSubclinicalKetosis(bool alreadyHadOne, const boost::gregorian::date &date, const boost::gregorian::date lastCalvingDate) override;
        void notifyNewClinicalKetosis(bool alreadyHadOne, const boost::gregorian::date &date, const boost::gregorian::date lastCalvingDate) override;
        void notifyNewG1InfectiousLamenessOccurence(const boost::gregorian::date &date) override;
        void notifyNewG1NonInfectiousLamenessOccurence(const boost::gregorian::date &date) override;
        void notifyNewG2InfectiousLamenessOccurence(const boost::gregorian::date &date) override;
        void notifyNewG2NonInfectiousLamenessOccurence(const boost::gregorian::date &date) override;
        void increaseMastitisCullingCount();
        void increaseKetosisCullingCount();
        void increaseNonInfectiousLamenessCullingCount();
        void increaseInfectiousLamenessCullingCount();
        void increaseCurativeClinicalKetosisVetTreatmentCount();
        void increaseCurativeClinicalKetosisFarmerTreatmentCount();
        void increaseCurativeSubclinicalKetosisVetTreatmentCount() ;
        void increaseCurativeSubclinicalKetosisFarmerTreatmentCount();
        void increaseCurativeClinicalMastitisVetTreatmentCount();
        void increaseCurativeClinicalMastitisFarmerTreatmentCount();
        void increaseCurativeSubclinicalMastitisVetTreatmentCount();
        void increaseCurativeSubclinicalMastitisFarmerTreatmentCount();
        //void increasePreventiveKetosisVetTreatmentCount();
        void increasePreventiveKetosisFarmerTreatmentCount();
        //void increasePreventiveMastitisVetTreatmentCount();
        void increasePreventiveMastitisFarmerTreatmentCount();
        void increaseCurativeClinicalLamenessVetTreatmentCount();
        void increasePreventiveLamenessVetTreatmentCount();
        void increaseUnscheduledCowVetCareCount();
    private :
        void increaseTotalHealthTreatmentCount();
        void increaseScheduledCowFarmerCareCount();    
        void increaseUnscheduledCowFarmerCareCount();
    public:
        void increaseScheduledFarmerActivityCount(unsigned int count);
        void increaseUnscheduledFarmerActivityCount();
        void increaseScheduledVetActivityCount(unsigned int count);
        void increaseUnscheduledVetActivityCount();
        // Accounting methods
        void accountNewTransaction(
#ifdef _LOG                
                                    const boost::gregorian::date &transactionDate, std::string comp, 
#endif // _LOG                
                                    float amount, float &annualDataToSet);
        float inline &getCullSalesForUpdate(){return _cullSales;}
        float inline &getHeiferSalesForUpdate(){return _heiferSales;}
        float inline &getCalfSalesForUpdate(){return _calfSales;}
        float inline &getFeedCostsForUpdate(){return _feedCosts;}
        float inline &getMastitisHealthCostsForUpdate(){return _mastitisHealthCosts;}
        float inline &getKetosisHealthCostsForUpdate(){return _ketosisHealthCosts;}
        float inline &getCurativeIndividualInfectiousLamenessCostsForUpdate(){return _curativeIndividualInfectiousLamenessCosts;}
        float inline &getCurativeIndividualNonInfectiousLamenessCostsForUpdate(){return _curativeIndividualNonInfectiousLamenessCosts;}
        float inline &getCurativeCollectiveInfectiousLamenessCostsForUpdate(){return _curativeCollectiveInfectiousLamenessCosts;}
        float inline &getCurativeCollectiveNonInfectiousLamenessCostsForUpdate(){return _curativeCollectiveNonInfectiousLamenessCosts;}
        float inline &getCurativeTrimmingCostsForUpdate(){return _curativeTrimmingCosts;}
        float inline &getPreventiveTrimmingCostsForUpdate(){return _preventiveTrimmingCosts;}
        float inline &getFootBathCostsForUpdate(){return _footBathCosts;}
        float inline &getOtherHealthCostsForUpdate(){return _otherHealthCosts;}
        float inline &getReproductionCostsForUpdate(){return _reproductionCosts;}
        float inline &getPopulationCostsForUpdate(){return _populationCosts;}
        float inline &getOtherCostsForUpdate(){return _otherCosts;}
        
    }; /* End of class DairyHerd */
} /* End of namespace DataStructures */
#endif // DataStructures_DairyHerd_h
