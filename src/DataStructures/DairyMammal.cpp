#include "DairyMammal.h"

// Standard
#include <vector>

// Internal
// --------
#include "PregnancyProcess.h"
#include "Insemination.h"
#include "DairyFarm.h"
#include "Ovulation.h"
#include "DairyHerd.h"
#include "ArtificialInsemination.h"
#include "NaturalInsemination.h"
#include "DairyUdder.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"
#include "../ExchangeInfoStructures/TechnicalConstants.h"
#include "Foots.h"

// Module dependencies
// -------------------

// Reproduction
#include "../Reproduction/ReproductionManagement.h"
#include "../Reproduction/DairyHerdReproductionStates/PrePuberState.h"
#include "../Reproduction/DairyHerdReproductionStates/BeefCrossBredState.h"
#include "../Reproduction/DairyHerdReproductionStates/MaleState.h"
#include "../Reproduction/DairyHerdReproductionStates/SterileState.h"
#include "../Reproduction/DairyHerdReproductionStates/OvulationState.h"


// Lactation
#include "../Lactation/LactationManagement.h"
#include "../Lactation/DairyHerdLactationStates/MilkProducingState.h"
#include "../Lactation/DairyHerdLactationStates/NoMilkProducingState.h"
#include "../Lactation/DairyHerdLactationStates/DryState.h"

// Genetic
#include "../Genetic/GeneticManagement.h"
#include "../Genetic/BreedInheritance.h"
#include "../Genetic/Breed/DairyBreed.h"

// Health
#include "../Health/Diseases/Mastitis/DairyCowMastitisRiskPeriodStates/OutOfRiskPeriodDairyCowState.h"
#include "../Health/Diseases/Mastitis/DairyCowMastitisRiskPeriodStates/PeriPartumRiskPeriodDairyCowState.h"
#include "../Health/Diseases/Mastitis/DairyCowMastitisRiskPeriodStates/DryingOffRiskPeriodDairyCowState.h"
#include "../Health/Diseases/Mastitis/DairyCowMastitisRiskPeriodStates/DryingInvolutedRiskPeriodDairyCowState.h"
#include "../Health/Diseases/Mastitis/DairyCowMastitisRiskPeriodStates/LactationRiskPeriodDairyCowState.h"
#include "../Health/HealthManagement.h"
#include "../Health/Diseases/Ketosis/KetosisStates/HeiferKetosisState.h"

// Population
#include "./Population/Batch/Batch.h"
#include "./Population/PopulationManagement.h"
#include "./Population/Moves/Birth.h"
#include "./Population/Moves/Cull.h"
#include "./Population/Moves/Sale.h"

// Miscellaneous
#include "../ResultStructures/DairyFarmResult.h"
#include "../ExchangeInfoStructures/DetectionConditions.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"
#include "../ExchangeInfoStructures/TechnicalConstants.h"
#include "../Tools/Random.h"
#include "../Tools/Tools.h"

BOOST_CLASS_EXPORT(DataStructures::DairyMammal) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Mammal); \
    ar & BOOST_SERIALIZATION_NVP(_hasMortalityRisk); \
    ar & BOOST_SERIALIZATION_NVP(_pHasCurrentReproductionState); \
    ar & BOOST_SERIALIZATION_NVP(_hasSpecificPostPartumOvulationDuration); \
    ar & BOOST_SERIALIZATION_NVP(_pPostPartumOvulationDuration); \
    ar & BOOST_SERIALIZATION_NVP(_breedingDate); \
    ar & BOOST_SERIALIZATION_NVP(_endFirstInseminationDate); \
    ar & BOOST_SERIALIZATION_NVP(_endInseminationDate); \
    ar & BOOST_SERIALIZATION_NVP(_lastCalvingIntervalInMonth); \
    ar & BOOST_SERIALIZATION_NVP(_consecutiveUnsucessfulInseminationCount); \
    ar & BOOST_SERIALIZATION_NVP(_lastOestrusDetectionDate); \
    ar & BOOST_SERIALIZATION_NVP(_pFuturForcedOvulationState); \
    ar & BOOST_SERIALIZATION_NVP(_pHasCurrentLactationState); \
    ar & BOOST_SERIALIZATION_NVP(_lactationRank); \
    ar & BOOST_SERIALIZATION_NVP(_lactationStage); \
    ar & BOOST_SERIALIZATION_NVP(_hasSpecificLactationCurves); \
    ar & BOOST_SERIALIZATION_NVP(_pNormalizedLactationCurves); \
    ar & BOOST_SERIALIZATION_NVP(_mMilkQuantityPeakYield); \
    ar & BOOST_SERIALIZATION_NVP(_mMilkQuantityPeakDay); \
    ar & BOOST_SERIALIZATION_NVP(_deliveredMilkOfTheDay); \
    ar & BOOST_SERIALIZATION_NVP(_discardedMilkOfTheDay); \
    ar & BOOST_SERIALIZATION_NVP(_lastControledMilk); \
    ar & BOOST_SERIALIZATION_NVP(_milkQuantityLossTodayDueToMastitis); \
    ar & BOOST_SERIALIZATION_NVP(_milkQuantityLossTodayDueToKetosis); \
    ar & BOOST_SERIALIZATION_NVP(_milkQuantityLossTodayDueToLameness); \
    ar & BOOST_SERIALIZATION_NVP(_lastDryOffDate); \
    ar & BOOST_SERIALIZATION_NVP(_lastSCCWasLessThanRef); \
    ar & BOOST_SERIALIZATION_NVP(_lastFullLactationPonderedMilkQuantity); \
    ar & BOOST_SERIALIZATION_NVP(_lastScorePeriodSCC); \
    ar & BOOST_SERIALIZATION_NVP(_lastScorePeriodMilkQuantity); \
    ar & BOOST_SERIALIZATION_NVP(_consecutiveDaysUnderMinimumMilkProduction); \
    ar & BOOST_SERIALIZATION_NVP(_lastSCCControlWasOverRef); \
    ar & BOOST_SERIALIZATION_NVP(_beforeLastSCCControlWasOverRef); \
    ar & BOOST_SERIALIZATION_NVP(_notYetControledForThisLactation); \
    ar & BOOST_SERIALIZATION_NVP(_pHasCurrentMastitisRiskPeriodState); \
    ar & BOOST_SERIALIZATION_NVP(_milkPotentialFactor); \
    ar & BOOST_SERIALIZATION_NVP(_pHasCurrentKetosisState); \
    ar & BOOST_SERIALIZATION_NVP(_ketosisTestSinceLastCalvingDone); \
    ar & BOOST_SERIALIZATION_NVP(_dateToTakeMonensinBolus); \
    ar & BOOST_SERIALIZATION_NVP(_endEffectDateOfMonensinBolusCare); \
    ar & BOOST_SERIALIZATION_NVP(_crossedLactationRankKetosisIncidence); \
    ar & BOOST_SERIALIZATION_NVP(_G1ketosisCumulateDurationInTheLactation); \
    ar & BOOST_SERIALIZATION_NVP(_G2ketosisCumulateDurationInTheLactation); \
    ar & BOOST_SERIALIZATION_NVP(_G1ketosisSuccessfulTreatmentDuringTheLactation); \
    ar & BOOST_SERIALIZATION_NVP(_ketosisCaseDuringPreviousLactation); \
    ar & BOOST_SERIALIZATION_NVP(_endKetosisMilkQuantityEffect); \
    ar & BOOST_SERIALIZATION_NVP(_ketosisMilkQuantityFactorEffect); \
    ar & BOOST_SERIALIZATION_NVP(_toCareIndividualTodayDueToSporadicLamenessDetection); \
    ar & BOOST_SERIALIZATION_NVP(_toTrimCollectiveTodayForLamenessPrevention); \
    ar & BOOST_SERIALIZATION_NVP(_longNonInfectiousLammenessWhenLastDryoff); \
    ar & BOOST_SERIALIZATION_NVP(_mSpecificDryDaysAllPeriodDependingRankLamenessIncidenceByDay); \
    ar & BOOST_SERIALIZATION_NVP(_mSpecificLactationDaysAllPeriodDependingRankLamenessIncidenceByDay); \
    ar & BOOST_SERIALIZATION_NVP(_mSpecificLactationPeriodDependingRankLamenessIncidence); \
    ar & BOOST_SERIALIZATION_NVP(_forageRationOfTheDay); \
    ar & BOOST_SERIALIZATION_NVP(_concentrateRationOfTheDay); \
    ar & BOOST_SERIALIZATION_NVP(_mineralVitaminRationOfTheDay); \
    ar & BOOST_SERIALIZATION_NVP(_saleStatus); \
    ar & BOOST_SERIALIZATION_NVP(_damLastFullLactationPonderedMilkQuantity); \
    ar & BOOST_SERIALIZATION_NVP(_hasGeneticValue); \

    IMPLEMENT_SERIALIZE_STD_METHODS(DairyMammal);

    // Constructor based with the dam (simulated farrowing)
    DairyMammal::DairyMammal(const boost::gregorian::date &birth, Mammal* pDam, Genetic::Breed::Breed* pSireBreed, bool sterile, FunctionalEnumerations::Genetic::Sex sex, unsigned int dairyQuarterNumber) : Mammal(birth, pDam, pSireBreed, sex)
    {
        // Genetic value (as calf)
        Genetic::GeneticManagement::setGeneticValueToCalf(this, (DairyMammal*)pDam, pSireBreed, getDairyFarm()->getGeneticStrategy(), getDairyFarm()->getRandom());
        
        // Contribute to potential sale as female calf
        _damLastFullLactationPonderedMilkQuantity = round(((DairyMammal*)pDam)->getLastFullLactationPonderedMilkQuantity());
                
        commonInit(sex, sterile, birth, dairyQuarterNumber);                
    }
    
    // This is the herd generation constructor or buy cow
    DairyMammal::DairyMammal(const boost::gregorian::date &simDate, const boost::gregorian::date &birth, Genetic::Breed::Breed* pBreed, Herd* pHerd, FunctionalEnumerations::Genetic::Sex sex, unsigned int dairyQuarterNumber) : Mammal(simDate, birth, pBreed, pHerd, sex)
    {                     
        // Genetic value
        _hasGeneticValue = pBreed->getACowGeneticValue(getDairyFarm()->getGeneticStrategy(), getDairyFarm()->getRandom());  
        
        commonInit(sex, false, simDate, dairyQuarterNumber);
    }
    
    DairyMammal::~DairyMammal()
    {         
        delete _pHasCurrentReproductionState;
        _pHasCurrentReproductionState = nullptr;
                        
        delete _pHasCurrentLactationState;
        _pHasCurrentLactationState = nullptr;
                        
        delete _pHasCurrentMastitisRiskPeriodState;
        _pHasCurrentMastitisRiskPeriodState = nullptr;
        
        delete _pHasCurrentKetosisState;
        _pHasCurrentKetosisState = nullptr;
                        
        getDairyFarm()->removeAnimalFromDeathOfTheDay(this);
    }
        
    DairyHerd* DairyMammal::getDairyHerd()
    {
        return (DairyHerd*)getHerd();
    }

    Genetic::GeneticValue &DairyMammal::getGeneticValue()
    {
        return _hasGeneticValue;
    }
    
    Genetic::GeneticValue &DairyMammal::getSirGeneticValueOfTheLastFertilyInsemination()
    {
        return getLastPregnancyProcess()->getFertilyInsemination()->getSirGeneticValue();
    }
    
    Genetic::BreedInheritance* DairyMammal::getCowBreedInheritance()
    {
        return getBreedInheritance();
    }
    
//    float DairyMammal::getGeneticIndex(FunctionalEnumerations::Genetic::GeneticStrategy strategy)
//    {
//        if (strategy == FunctionalEnumerations::Genetic::GeneticStrategy::wellBalanced) return getGeneticValue().getINEL();
//        if (strategy == FunctionalEnumerations::Genetic::GeneticStrategy::milkQuantityPriority) return getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait).getCorrectedPerformance();
//        if (strategy == FunctionalEnumerations::Genetic::GeneticStrategy::functionalLongevityPriority) return getGeneticValue().getLGF();
//    }
//
            
    void DairyMammal::initAgeToMortalityRiskAgeMatrix()
    {
        if (s_ageToMortalityRiskAgeMatrix.size() == 0)
        {
            unsigned int age;
            for (age = 0; age < FunctionalEnumerations::Population::MortalityRiskAge::underSevenDays; age++)
            {
                s_ageToMortalityRiskAgeMatrix[age] = FunctionalEnumerations::Population::MortalityRiskAge::underSevenDays;
            }
            for (age = FunctionalEnumerations::Population::MortalityRiskAge::underSevenDays; age < FunctionalEnumerations::Population::MortalityRiskAge::sevenDaysToOneMonth; age++)
            {
                s_ageToMortalityRiskAgeMatrix[age] = FunctionalEnumerations::Population::MortalityRiskAge::sevenDaysToOneMonth;
            }
            for (age = FunctionalEnumerations::Population::MortalityRiskAge::sevenDaysToOneMonth; age < FunctionalEnumerations::Population::MortalityRiskAge::oneToTwoMonths; age++)
            {
                s_ageToMortalityRiskAgeMatrix[age] = FunctionalEnumerations::Population::MortalityRiskAge::oneToTwoMonths;
            }
            for (age = FunctionalEnumerations::Population::MortalityRiskAge::oneToTwoMonths; age < FunctionalEnumerations::Population::MortalityRiskAge::twoToSixMonths; age++)
            {
                s_ageToMortalityRiskAgeMatrix[age] = FunctionalEnumerations::Population::MortalityRiskAge::twoToSixMonths;
            }
            for (age = FunctionalEnumerations::Population::MortalityRiskAge::twoToSixMonths; age < FunctionalEnumerations::Population::MortalityRiskAge::sixMonthsToOneYear; age++)
            {
                s_ageToMortalityRiskAgeMatrix[age] = FunctionalEnumerations::Population::MortalityRiskAge::sixMonthsToOneYear;
            }
            for (age = FunctionalEnumerations::Population::MortalityRiskAge::sixMonthsToOneYear; age < FunctionalEnumerations::Population::MortalityRiskAge::oneToTwoYears; age++)
            {
                s_ageToMortalityRiskAgeMatrix[age] = FunctionalEnumerations::Population::MortalityRiskAge::oneToTwoYears;
            }
            for (age = FunctionalEnumerations::Population::MortalityRiskAge::oneToTwoYears; age < FunctionalEnumerations::Population::MortalityRiskAge::twoToThreeAndHalfYears; age++)
            {
                s_ageToMortalityRiskAgeMatrix[age] = FunctionalEnumerations::Population::MortalityRiskAge::twoToThreeAndHalfYears;
            }
            for (age = FunctionalEnumerations::Population::MortalityRiskAge::twoToThreeAndHalfYears; age < FunctionalEnumerations::Population::MortalityRiskAge::threeAndHalfToFiveYears; age++)
            {
                s_ageToMortalityRiskAgeMatrix[age] = FunctionalEnumerations::Population::MortalityRiskAge::threeAndHalfToFiveYears;
            }
            for (age = FunctionalEnumerations::Population::MortalityRiskAge::threeAndHalfToFiveYears; age <= FunctionalEnumerations::Population::MortalityRiskAge::fiveToTenYears; age++)
            {
                s_ageToMortalityRiskAgeMatrix[age] = FunctionalEnumerations::Population::MortalityRiskAge::fiveToTenYears;
            }
            s_ageToMortalityRiskAgeMatrix[age] = FunctionalEnumerations::Population::MortalityRiskAge::OverTenYears; 
        }
    }
    
    FunctionalEnumerations::Population::MortalityRiskAge DairyMammal::getAgeToMortalityRiskFromAgeMatrix()
    {
        unsigned int age = getAge();
        if (age > FunctionalEnumerations::Population::MortalityRiskAge::OverTenYears) age = FunctionalEnumerations::Population::MortalityRiskAge::OverTenYears;
        return s_ageToMortalityRiskAgeMatrix.find(age)->second;
    }

    void DairyMammal::commonInit(FunctionalEnumerations::Genetic::Sex  sex, bool sterile, const boost::gregorian::date &date, unsigned int dairyQuarterNumber)
    {
        _pHasUdder = new DairyUdder(this, dairyQuarterNumber, date); // Creating udder

        _pubertyAge = FunctionalConstants::Reproduction::PUBERTY_MEAN_AGE; // May be one day a variability ?// Genetic::GeneticManagement::calculatePubertyAge(getBreeds());
        _lateEmbryonicMortalityRisk = getBreedInheritance()->getCrossedValue(FunctionalConstants::Reproduction::LATE_EMBRYONIC_MORTALITY_RISK);
        _abortionRisk = getBreedInheritance()->getCrossedValue(FunctionalConstants::Reproduction::ABORTION_RISK);     
        
        
        // Ketosis specific incidences
        for (unsigned int lactationRank = 1; lactationRank <= 3; lactationRank++ )
        {
            _crossedLactationRankKetosisIncidence[lactationRank] = getBreedInheritance()->getCrossedValue(FunctionalConstants::Health::LACTATION_RANK_KETOSIS_INCIDENCE.find(lactationRank)->second) * getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait).getCorrectedPerformance();
        }        
        
        // Specific lameness incidences (genetic)
        for (unsigned int i_ni = FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType; i_ni <= (unsigned int)FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType; i_ni++)
        {
            FunctionalEnumerations::Health::LamenessInfectiousStateType i_ni_enum = (FunctionalEnumerations::Health::LamenessInfectiousStateType)i_ni;
            FunctionalEnumerations::Genetic::PhenotypicCharacter pc_RB_i_ni  = (i_ni_enum == FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)
                                                                                                       ? FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni
                                                                                                       : FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi;
            float preventionFactor = getDairyFarm()->getCurrentLamenessPreventionFactor(i_ni_enum);
            float geneticFactor = getGeneticValue().getCharacterGeneticValue(pc_RB_i_ni).getCorrectedPerformance();
            
            std::map<unsigned int, float> specificDryDaysAllPeriodDependingRankLamenessIncidenceByDay;
            std::map<unsigned int, float> specificLactationDaysAllPeriodDependingRankLamenessIncidenceByDay;
            std::map<unsigned int, float> specificLactationPeriodDependingRankLamenessIncidence;
            for (unsigned int lactationRank = 1; lactationRank <= 4; lactationRank++ )
            {
                float lactationRankFactor = FunctionalConstants::Health::LACTATION_RANK_LAMENESS_INCIDENCE_FACTOR.find(i_ni_enum)->second[lactationRank-1];
                // All periods
                specificDryDaysAllPeriodDependingRankLamenessIncidenceByDay[lactationRank] =   FunctionalConstants::Health::LAMENESS_BASIS_INCIDENCE_ALL_PERIODS.find(i_ni_enum)->second
                                                                                               * lactationRankFactor
                                                                                               * geneticFactor
                                                                                               / (preventionFactor * ((float)FunctionalConstants::Lactation::REFERENCE_DRY_DURATION));
               specificLactationDaysAllPeriodDependingRankLamenessIncidenceByDay[lactationRank] =   FunctionalConstants::Health::LAMENESS_BASIS_INCIDENCE_ALL_PERIODS.find(i_ni_enum)->second
                                                                                               * lactationRankFactor
                                                                                               * geneticFactor
                                                                                               / (preventionFactor * ((float)FunctionalConstants::Lactation::REFERENCE_LACTATION_DURATION));
                // Lactation incidence   
                specificLactationPeriodDependingRankLamenessIncidence[lactationRank] =  FunctionalConstants::Health::LAMENESS_BASIS_INCIDENCE_LACTATION_PERIOD.find(i_ni_enum)->second
                                                                                        * lactationRankFactor
                                                                                        * geneticFactor
                                                                                        / preventionFactor;
            }
            _mSpecificDryDaysAllPeriodDependingRankLamenessIncidenceByDay[i_ni_enum] = specificDryDaysAllPeriodDependingRankLamenessIncidenceByDay;
            _mSpecificLactationDaysAllPeriodDependingRankLamenessIncidenceByDay[i_ni_enum] = specificLactationDaysAllPeriodDependingRankLamenessIncidenceByDay;
            _mSpecificLactationPeriodDependingRankLamenessIncidence[i_ni_enum] = specificLactationPeriodDependingRankLamenessIncidence;
        }        
        
        // Reproduction durations
        Genetic::GeneticManagement::calculateTheoricPregnancyDurations(getBreedInheritance(), _mTheoricPregnancyDurations);
        Genetic::GeneticManagement::calculateTheoricBetweenOvulationDurations(getBreedInheritance(), _mTheoricBetweenOvulationDurations, getDairyFarm()->getRandom());
        
        // Set the breeding date
        boost::gregorian::date breedDate = getBirthDate() +  boost::gregorian::days(getDairyFarm()->getAgeToBreedingDecision());
        setBreedingDate(breedDate, FunctionalEnumerations::Reproduction::DairyCowParity::heifer);        
        // --------------------------------------
        // Setting the initial reproduction state 
        // --------------------------------------
        // Birth states
        if (getBreedInheritance()->isBeefCrossed()) // cross-bred
        {
            setCurrentReproductionState(new Reproduction::DairyHerdReproductionStates::BeefCrossBredState(this, getBirthDate()), date);
        }
        else if (sex == FunctionalEnumerations::Genetic::Sex::male) // Male
        {
            setCurrentReproductionState(new Reproduction::DairyHerdReproductionStates::MaleState(this, getBirthDate()), date);
        }
        else if (sterile)
        {
            setCurrentReproductionState(new Reproduction::DairyHerdReproductionStates::SterileState(this, getBirthDate()), date);
        }
        else
        {
            setCurrentReproductionState(new Reproduction::DairyHerdReproductionStates::PrePuberState(this, getBirthDate()), date);
        }
        _lastCalvingIntervalInMonth = 0;
        
        // ----------------------------------------------------
        // Setting the eventually individual breed dependencies
        // ----------------------------------------------------
        //_pHasMortalityRisk = Genetic::GeneticManagement::generateAndGetMortalityRiskMatrix(getBreedInheritance(), getSex(), _hasSpecificMortalityRisk, getDairyFarm()->getGlobalMortalityProba());
        _hasMortalityRisk = Genetic::GeneticManagement::generateAndGetMortalityRiskMatrix(getBreedInheritance(), getSex(), getDairyFarm()->getGlobalMortalityProba());
        _pPostPartumOvulationDuration = Genetic::GeneticManagement::generateAndGetPostPartumOvulationDurationMatrix(getBreedInheritance(), _hasSpecificPostPartumOvulationDuration);
        
        std::map<unsigned int, float> mMilkQuantityPeakYield;
        _pNormalizedLactationCurves = Genetic::GeneticManagement::generateAndGetNormalizedLactationCurves(getBreedInheritance(), _hasSpecificLactationCurves, mMilkQuantityPeakYield, _mMilkQuantityPeakDay);
        for (unsigned int currentLactationNumber = 1; currentLactationNumber <= 3;  currentLactationNumber++)
        {
            _mMilkQuantityPeakYield[currentLactationNumber] = mMilkQuantityPeakYield.find(currentLactationNumber)->second * _hasGeneticValue.getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait).getCorrectedPerformance();
        }
        
        // Ketosis state 
        setCurrentKetosisState(new Health::Ketosis::KetosisStates::HeiferKetosisState(date, this));
        
        // Production state
        setCurrentLactationState(new Lactation::DairyHerdLactationStates::NoMilkProducingState(this, getBirthDate()));
        
        // IIM Risk
        setCurrentMastitisRiskPeriodState(new Health::Mastitis::RiskPeriodStates::OutOfRiskPeriodDairyCowState(this, date)); // Basic risk is out of risk period
        initMilkPotentialFactor(); // Calculate the Milk Potential Factor to be used in health module for mastitis   
    }
    
    float DairyMammal::getMilkPeakQuantity()
    {
        assert (getParity() != FunctionalEnumerations::Reproduction::DairyCowParity::heifer);
        return _mMilkQuantityPeakYield.find(getLactationRankWithLimit(_mMilkQuantityPeakYield.size()))->second;
    }
    
    bool DairyMammal::getBabyIsToSale()
    {
        return getSaleStatus() != FunctionalEnumerations::Population::SaleStatus::notToSale;
    }
    
    bool DairyMammal::isToBeFed()
    {
        return getAge() > FunctionalConstants::Feeding::COLOSTRUM_CONSUMPTION_DURATION; // First days are only colostrum drinking
    }

    float DairyMammal::getMortalityRisk()
    {
        // Base risk depending age
        //return _pHasMortalityRisk->find(getAgeToMortalityRiskFromAgeMatrix())->second;;
        return _hasMortalityRisk.find(getAgeToMortalityRiskFromAgeMatrix())->second;;
    }
    
    void DairyMammal::progress(const boost::gregorian::date &simDate)
    {
        Mammal::progress(simDate);
        
        // Death ?
        if (getDairyFarm()->getRandom().ran_bernoulli(getMortalityRisk()))
        {
            // Death
            dies(
#ifdef _LOG
                    simDate, 
#endif // _LOG            
                    FunctionalEnumerations::Population::DeathReason::otherDeathReason);
        }
        else
        {

            // To trim for collective lameness prevention ?
            if (_toTrimCollectiveTodayForLamenessPrevention)
            {
                trimCollectiveTodayForLamenessPrevention(simDate);
            }

            // To trim for indivual lameness care ?
            if (_toCareIndividualTodayDueToSporadicLamenessDetection)
            {
                careIndividualTodayForDetectedLamenessCase(simDate);
            }

            // Lactation progress
            // ------------------
            // Update Milk production of the day
            _deliveredMilkOfTheDay.init();
            _discardedMilkOfTheDay.init();
            _milkQuantityLossTodayDueToLameness = 0.0f;
            Lactation::DairyHerdLactationStates::DairyHerdLactationState* pNewState = nullptr;
            getCurrentLactationState()->progress(this, simDate, pNewState); // Milk production State progress
            setCurrentLactationState(pNewState);
            
            // Ketosis progress
            // ----------------
                    
            // Take monensin bolus ?
            if (simDate == _dateToTakeMonensinBolus)
            {
               takeMonensinBolus(simDate);
            }
#ifdef _LOG
            bool wasKetosisIll = _pHasCurrentKetosisState->isIll();
#endif // _LOG            
            Health::Ketosis::KetosisStates::KetosisState* pNewKetosisState = nullptr;
            _pHasCurrentKetosisState->progress(simDate, getDairyFarm()->getRandom(), pNewKetosisState
#ifdef _LOG
                                                          , getDataLog()
#endif // _LOG            
                                                          );
            setCurrentKetosisState(pNewKetosisState);

#ifdef _LOG
            if (_pHasCurrentKetosisState->isIll() and not wasKetosisIll)
            {
                // This is a new illness
                // New Ketosis : Log this new occurence
                Results::KetosisResult cr;
                cr.id = getUniqueId();
                cr.date = simDate;
                cr.ketosisSeverity = _pHasCurrentKetosisState->getSeverity();
                getCurrentResults()->getHealthResults().KetosisResults.push_back(cr);
            }
#endif // _LOG          
            
            // Day for bood test ?
            if (simDate.day() == FunctionalConstants::Health::DAY_OF_MONTH_FOR_KETOSIS_BLOOD_TEST and getDairyFarm()->hasVetReproductionContract())
            {
                if ((not _ketosisTestSinceLastCalvingDone) and isMilking())
                {
                   Health::Ketosis::KetosisStates::KetosisState* pNewState = nullptr;
                   _pHasCurrentKetosisState->considerKetosisBloodTest(simDate, getDairyFarm()->getRandom(), pNewState);
                   setCurrentKetosisState(pNewState);
                   _ketosisTestSinceLastCalvingDone = true;
#ifdef _LOG
                    addMiscellaneousLog(simDate, "Ketosis blood test");
#endif // _LOG            
                }
            }
            

            // Health udder progress 
            // ---------------------
            _pHasUdder->progress(simDate
#ifdef _LOG
                                , getDataLog()
#endif // _LOG
                                , getDairyFarm()->getRandom());

            // Reproduction progress
            // ---------------------
            Reproduction::DairyHerdReproductionStates::DairyHerdReproductionState* pNewReproductionState = nullptr;
            getCurrentReproductionState()->progress(simDate, getDairyFarm()->getRandom(), pNewReproductionState); // Reproduction State progress
            setCurrentReproductionState(pNewReproductionState, simDate);

            if (isToCullForInfertility(simDate))
            {
                getDairyHerd()->increaseAnnualNotBredBecauseOfInfertilityCount();
                cullingIsDecided(FunctionalEnumerations::Population::CullingStatus::toCullDueToUnfertility, not getCurrentLactationState()->isMilking(), simDate);
            }

            // Abnormal no oestrus detection ?
            else if (not getDairyFarm()->hasVetReproductionContract())
            {
                if (needTreatmentForAbnormalNoOestrusDetection(simDate, false))
                {
//                    std::cout << "Id = " << getUniqueId() << ", simDate =  " << simDate << ", takeAbnormalNoOestrusDetectionTreatment" << std::endl;
                    // Case a
                    takeAbnormalNoOestrusDetectionTreatment(simDate, getDairyFarm()->giveAbnormalNoOestrusDetectionTreatment(  
#ifdef _LOG
                                                                                                                            simDate 
#endif // _LOG        
                                                                                                                           ));
                }
            }
            
            // IAMM planned ? 
            else if (isItTheDayForPlannedTemporalEvent(ExchangeInfoStructures::TemporalEvent::TemporalEventType::IAMM, simDate))
            {
                if (hasToGoToReproduce(simDate))
                {
                    // IAMM
                    goToInsemination(simDate, false, 0.0f);
                }
                
                // Oestrus detection false positive
                updateLastOestrusDetectionDate(simDate);
            }
            
            // Feeding progress
            // ----------------
            getBatch()->manageAnimalFeedConsumption(this); // My feed consumption
            
            // Herd pregnancy counting update
            if (getParity() == FunctionalEnumerations::Reproduction::DairyCowParity::heifer and 
                    getBatch()->getType() == FunctionalEnumerations::Population::BatchType::driedCowsAndPregnantHeifers)
            {
                // I am a pregnant heifer near to the calving
                getDairyHerd()->addHeifersInDriedCowsAndPregnantHeifersBatchOfTheDay(this);
            }       
            
            if (getAge() == 730) // 2 years old
            {
                // 2 years old cumulate costs
                // --------------------------
                // Calf bedding cost
                getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    simDate, FunctionalConstants::AccountingModel::CALF_BEDDING_TRANSACTION_TYPE, 
#endif // _LOG                
                                    getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_CALF_BEDDING_PRICE), getDairyHerd()->getOtherCostsForUpdate());

                // Heifer miscellaneous vet cost
                getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    simDate, FunctionalConstants::AccountingModel::HEIFER_MISCELLANEOUS_VET_TRANSACTION_TYPE, 
#endif // _LOG                
                                    getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_HEIFER_MISCELLANEOUS_VET_COST), getDairyHerd()->getOtherCostsForUpdate());

                // Other calf breeding cost
                 getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    simDate, FunctionalConstants::AccountingModel::OTHER_CALF_BREEDING_TRANSACTION_TYPE, 
#endif // _LOG                
                                    getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_OTHER_CALF_BREEDING_COST), getDairyHerd()->getOtherCostsForUpdate());
            }
        }

#ifdef _LOG
        Results::DayAnimalLogInformations &log = getDataLog();
        log.simDate = simDate;
        log.age = getAge();
        log.pregnant = pregnancyIsOngoing();
        log.mastitisRisk = _pHasCurrentMastitisRiskPeriodState->getStrMastitisRiskPeriodType();
#endif // _LOG
    }
    
    void DairyMammal::setCurrentReproductionState (Reproduction::DairyHerdReproductionStates::DairyHerdReproductionState* pState, const boost::gregorian::date &date)
    {
        if (pState != _pHasCurrentReproductionState and pState != nullptr)
        {
            if (_pHasCurrentReproductionState != nullptr)
            {
               delete _pHasCurrentReproductionState;
            }
            _pHasCurrentReproductionState = pState;
            _pHasCurrentReproductionState->notifyMammalEnteringInNewState(date);
        }
    }
        
    void DairyMammal::setCurrentMastitisRiskPeriodState(Health::Mastitis::RiskPeriodStates::DairyCowMastitisRiskPeriodState* pState)
    {
        if (_pHasCurrentMastitisRiskPeriodState != nullptr)
        {
           delete _pHasCurrentMastitisRiskPeriodState;
        }
        _pHasCurrentMastitisRiskPeriodState = pState;
    }
            
    Health::Mastitis::RiskPeriodStates::DairyCowMastitisRiskPeriodState* DairyMammal::getCurrentMastitisRiskPeriodState()
    {
        return _pHasCurrentMastitisRiskPeriodState; 
    }

    void DairyMammal::ensureUdderIsNowMastitisSensitive(const boost::gregorian::date &date)
    {
        getDairyUdder()->ensureIsNowMastitisSensitive(date);
    }
    
    unsigned int DairyMammal::getLactationRankForMastitisSeverityDegree()
    {
        return getLactationRank();
    }

    void DairyMammal::setPeriPartumMastitisRiskPeriod(const boost::gregorian::date &date, bool calving)
    {
        // Change mastitis risk period
        setCurrentMastitisRiskPeriodState(new Health::Mastitis::RiskPeriodStates::PeriPartumRiskPeriodDairyCowState(this, date, calving));
    }
    
    void DairyMammal::setDryingOffMastitisRiskPeriod(const boost::gregorian::date &date)
    {
        setCurrentMastitisRiskPeriodState(new Health::Mastitis::RiskPeriodStates::DryingOffRiskPeriodDairyCowState(this, date));
    }
    
    void DairyMammal::setDryingInvolutedMastitisRiskPeriod(const boost::gregorian::date &date)
    {

        // Change mastitis risk period
        setCurrentMastitisRiskPeriodState(new Health::Mastitis::RiskPeriodStates::DryingInvolutedRiskPeriodDairyCowState(this, date));
    }
    
    void DairyMammal::setLactationMastitisRiskPeriod(const boost::gregorian::date &date)
    {

        // Change mastitis risk period
        setCurrentMastitisRiskPeriodState(new Health::Mastitis::RiskPeriodStates::LactationRiskPeriodDairyCowState(this, date));
    }

    Reproduction::DairyHerdReproductionStates::DairyHerdReproductionState* DairyMammal::getCurrentReproductionState()
    {
        return _pHasCurrentReproductionState;
    }
        
    unsigned int DairyMammal::getLactationRank()
    {
        return _lactationRank;
    }
    
    void DairyMammal::setLactationStage(int lactationStage)
    {
        _lactationStage = lactationStage;
    }
    
    void DairyMammal::setNotYetControledForThisLactation()
    {
        _notYetControledForThisLactation = true;
    }

    int DairyMammal::getLactationStage()
    {
        return _lactationStage;
    }
    
    bool DairyMammal::lactationStageIsAfterPeak()
    {
        assert (lactationIsOngoing());    
        return (((unsigned int)getLactationStage()) > _mMilkQuantityPeakDay.find(getLactationRankWithLimit(3))->second);
    }

    void DairyMammal::belongingOvulatingGroup(bool val)
    {
        if (val)
        {
            getBatch()->appendOvulatingAnimal(this);   
        }
        else
        {
            getBatch()->removeOvulatingAnimal(this);   
        }
    }
    
    void DairyMammal::addOvulation(const boost::gregorian::date& ovulationDate)
    {
#ifdef _LOG
        // Only in debug mode
        if (!getLastFarrowingDate().is_not_a_date())
        {
            // We have a last calving date
            if (getLastOvulationDate() < getLastFarrowingDate())
            {
                // We don't have ovulation since last Farrowing
                Results::Interval it;
                it.date1 = getLastFarrowingDate();
                it.date2 = ovulationDate;
                it.comp = getUniqueId();
                if (_G2ketosisCumulateDurationInTheLactation > 0) it.comp2 = FunctionalEnumerations::Health::KetosisSeverity::G2Ketosis;
                else if (_G1ketosisCumulateDurationInTheLactation > 0) it.comp2 = FunctionalEnumerations::Health::KetosisSeverity::G1Ketosis;
                else it.comp2 = FunctionalEnumerations::Health::KetosisSeverity::G0Ketosis;
                getCurrentResults()->getReproductionResults().IVC1.push_back(it);         
            }
        }
#endif // _LOG
        
        // Creating the new ovulation
        Mammal::addOvulation(ovulationDate);
    }
    
    void DairyMammal::hasNewOvulation(const boost::gregorian::date &ovulationDate)
    {
        addOvulation(ovulationDate);
    }
    
#if defined _LOG || defined _FULL_RESULTS
    void DairyMammal::addInsemination(Insemination* pIns)
    {
        Mammal::addInsemination(pIns);
        if (pIns->isSuccess())
        {
            _lastFertilizingInseminationDate = pIns->getDate();
            _lastFertilizingInseminationType = pIns->getTypeSemen();
            _lastFertilizingInseminationMaleBreed = pIns->getBreed()->getEnumerationBreed();
            _lastFertilizingInseminationNumber = _inseminationQuantitySinceLastFarrowing;;
            if (_firstFertilizingInseminationAge == 0)
            {
                _firstFertilizingInseminationAge = getAge();
            }  
        }
    }
#endif // _LOG || _FULL_RESULTS

    bool DairyMammal::goToInsemination(const boost::gregorian::date &inseminationDate, bool rightTime, float medicFertilityDelta)
    {
        // Oestrus detection true positive
        updateLastOestrusDetectionDate(inseminationDate);

        Insemination *pIns = nullptr;
        
        FunctionalEnumerations::Reproduction::DairyCowParity parity = getParity();

        FunctionalEnumerations::Reproduction::TypeInseminationSemen typeSemen;
        Genetic::Breed::Breed* pSireBreed = nullptr;
        
        getDairyFarm()->getTypeInseminationAndBreedForInsemination(getLactationRank(), getInseminationRank(), typeSemen, pSireBreed);

        _lastInseminationDate = inseminationDate;

        // Basis probability first
        float inseminationSuccessProbability = Genetic::GeneticManagement::getBasisInseminationSuccessProbability(getBreedInheritance(), parity, typeSemen, getDairyFarm()->getFertilityFactor(), getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer).getCorrectedPerformance(), medicFertilityDelta);

        // Production ponderation
        float milkProductionFactor = 1.0f;
        if (getParity() != FunctionalEnumerations::Reproduction::DairyCowParity::heifer)
        {
            milkProductionFactor = Reproduction::ReproductionManagement::getMilkProductionFertilityFactor(getMilkPeakQuantity());
        }
        inseminationSuccessProbability *= milkProductionFactor;
        
        // Ketosis
        Health::HealthManagement::getPonderedInseminationSuccessProbability(_G1ketosisCumulateDurationInTheLactation + _G2ketosisCumulateDurationInTheLactation, _G1ketosisSuccessfulTreatmentDuringTheLactation,
                                                                                Health::HealthManagement::getLactationRankBasisIncidence(getLactationRank(), _crossedLactationRankKetosisIncidence),
                                                                                inseminationSuccessProbability);        
        bool success = getDairyFarm()->getRandom().ran_bernoulli(inseminationSuccessProbability);
                
        if (not success)
        {
            _consecutiveUnsucessfulInseminationCount++;
        }

        if (typeSemen == FunctionalEnumerations::Reproduction::TypeInseminationSemen::natural)
        {
            // Male service
            pIns = new DataStructures::NaturalInsemination(pSireBreed, getDairyFarm()->getASireGeneticValueFromCatalog(pSireBreed->getEnumerationBreed(), inseminationDate,  getCurrentResults()), inseminationDate, success, getCommingInseminationNumber(), getCurrentResults(), getDairyFarm()->getCurrentAccountingModel(), this);
            addInsemination(pIns);
#ifdef _LOG
            if (success)
            {
                addMiscellaneousLog(inseminationDate, "Successful natural insemination by breed " + Tools::toString(pSireBreed->getEnumerationBreed()));
            }
            else
            {
                addMiscellaneousLog(inseminationDate, "Unsuccessful natural insemination by breed " + pSireBreed->getEnumerationBreed());
            }
#endif // _LOG                
        }
        else
        {
            // Artificial Insemination
            // Remove planned IA
            removeTemporalEventType(ExchangeInfoStructures::TemporalEvent::TemporalEventType::IAMM);
            
            // For IA_count
            getDairyHerd()->increaseAnnualIACount();
                     
            // Record the insemination for results
            if (rightTime)
            {
                // At the good time
                pIns = new DataStructures::ArtificialInsemination(pSireBreed, getDairyFarm()->getASireGeneticValueFromCatalog(pSireBreed->getEnumerationBreed(), inseminationDate, getCurrentResults()), typeSemen, inseminationDate, success, getCommingInseminationNumber(), getCurrentResults(), getDairyFarm()->getCurrentAccountingModel(), this);
                addInsemination(pIns);
#ifdef _LOG
                if (success)
                {
                    addMiscellaneousLog(inseminationDate, "Successful IA type " + Tools::toString(typeSemen) + ", based on detection (or lucky specificity error) by breed " + Tools::toString(pSireBreed->getEnumerationBreed()));
                }
                else
                {
                    addMiscellaneousLog(inseminationDate, "Unsuccessful IA type " + Tools::toString(typeSemen) + ", based on detection (or lucky specificity error) by breed " + Tools::toString(pSireBreed->getEnumerationBreed()));
                }
#endif // _LOG                
            }
            else
            {
                // Not at the good time
                pIns = new DataStructures::ArtificialInsemination(pSireBreed, getDairyFarm()->getASireGeneticValueFromCatalog(pSireBreed->getEnumerationBreed(), inseminationDate, getCurrentResults()), typeSemen, inseminationDate, false, getCommingInseminationNumber(), getCurrentResults(), getDairyFarm()->getCurrentAccountingModel(), this);
                addInsemination(pIns);
#ifdef _LOG
                addMiscellaneousLog(inseminationDate, "Unsuccessful IA type " + Tools::toString(typeSemen) + " (bad moment), based on specificity error by breed " + Tools::toString(pSireBreed->getEnumerationBreed()));
#endif // _LOG                
            }
            
            if (rightTime && success)
            {
                // plann a IAMM after IA (specificity error) ?
                planPotentialIAReturnSpecificityError(inseminationDate);
            }
            
            // IA results
            const boost::gregorian::date lastCalvingDate = getLastFarrowingDate();
            bool hasCalving = !lastCalvingDate.is_not_a_date();
            if (pIns->getNumber()== 1)
            {
                // For IA1_count
                getDairyHerd()->increaseAnnualIA1Count(success);
                if (hasCalving)
                {
                    // For IV_IA1
                    getDairyHerd()->addAnnualCalvingIA1Interval((float)(boost::gregorian::date_duration(inseminationDate - lastCalvingDate).days()));
#ifdef _LOG
                    // for IVIA1
                    Results::InseminationCalvingInterval it;
                    it.calvingDate = lastCalvingDate;
                    it.inseminationdate = inseminationDate;
                    it.typeInsemination = typeSemen;
                    it.id = getUniqueId();
                    if (_G2ketosisCumulateDurationInTheLactation > 0) it.ketosis = FunctionalEnumerations::Health::KetosisSeverity::G2Ketosis;
                    else if (_G1ketosisCumulateDurationInTheLactation > 0) it.ketosis = FunctionalEnumerations::Health::KetosisSeverity::G1Ketosis;
                    else it.ketosis = FunctionalEnumerations::Health::KetosisSeverity::G0Ketosis;
                    getCurrentResults()->getReproductionResults().IVI1.push_back(it);
#endif // _LOG                
                }
            }
            
#ifdef _LOG
            // Only in debug mode
            // IA2
            else if (pIns->getNumber() == 2)
            {
                // for IIA1Return
                Results::Interval it;
                it.date1 = _lastFirstInseminationDate;
                it.date2 = inseminationDate;
                it.comp = getUniqueId();
                if (_G2ketosisCumulateDurationInTheLactation > 0) it.comp2 = FunctionalEnumerations::Health::KetosisSeverity::G2Ketosis;
                else if (_G1ketosisCumulateDurationInTheLactation > 0) it.comp2 = FunctionalEnumerations::Health::KetosisSeverity::G1Ketosis;
                else it.comp2 = FunctionalEnumerations::Health::KetosisSeverity::G0Ketosis;
                getCurrentResults()->getReproductionResults().IIA1Returns.push_back(it);
            }
#endif // _LOG      
            // For IVIAF
            if (hasCalving and success)
            {
                getDairyHerd()->addAnnualCalvingFecundingIAInterval((float)(boost::gregorian::date_duration(inseminationDate - lastCalvingDate).days()));
            }
        }
                            
#ifdef _LOG
            // Only in debug mode
        // for TRI
        Results::InseminationResult ir;
        ir.id = getUniqueId();
        ir.date = inseminationDate;
        ir.parity = getParity();
        ir.typeInsemination = typeSemen;
        ir.maleBreed = pSireBreed->getEnumerationBreed();
        ir.number = pIns->getNumber();
        ir.result = success;
        if (_G2ketosisCumulateDurationInTheLactation > 0) ir.ketosis = FunctionalEnumerations::Health::KetosisSeverity::G2Ketosis;
        else if (_G1ketosisCumulateDurationInTheLactation > 0) ir.ketosis = FunctionalEnumerations::Health::KetosisSeverity::G1Ketosis;
        else ir.ketosis = FunctionalEnumerations::Health::KetosisSeverity::G0Ketosis;
        getCurrentResults()->getReproductionResults().InseminationResults.push_back(ir);
#endif // _LOG    
        
#if defined _LOG || defined _FULL_RESULTS
        // Result based on insemination
        // ----------------------------
        if (parity == FunctionalEnumerations::Reproduction::DairyCowParity::heifer)
        {
            // Heifers
            Results::NullipareReproductionResultBasedOnInsemination data;
            data.id = getUniqueId();
            data.firstInseminationAge = _firstFirstInseminationAge;
            data.firstInseminationDate = _lastFirstInseminationDate;
            data.inseminationDate = inseminationDate;
            data.inseminationType = typeSemen;
            data.inseminationMaleBreed = pSireBreed->getEnumerationBreed();
            data.inseminationQuantity = _inseminationQuantitySinceLastFarrowing;
            getCurrentResults()->getReproductionResults().NullipareReproductionResultsBasedOnInsemination.push_back(data);
        }
        else
        {
            // primi and multipare
            Results::PrimiMultipareReproductionResultBasedOnInsemination data;
            data.id = getUniqueId();
            data.previousCalvingDate = _lastFarrowingDate;
            data.previousCalvingRank = getFarrowingNumber();
            data.firstInseminationDate = _lastFirstInseminationDate;
            data.inseminationDate = inseminationDate;
            data.inseminationType = typeSemen;
            data.inseminationMaleBreed = pSireBreed->getEnumerationBreed();
            data.inseminationQuantity = _inseminationQuantitySinceLastFarrowing;
            getCurrentResults()->getReproductionResults().PrimiMultipareReproductionResultsBasedOnInsemination.push_back(data);
        }
#endif // _LOG || _FULL_RESULTS
        return success;
    }
   
    void DairyMammal::enterInPregnancy(const boost::gregorian::date &beginDate)
    {
        // Create the new gestation for the mammal
        addPregnancyProcess(new PregnancyProcess(beginDate, getLastInsemination())); // Creating the gestation
        
        // Delete IAMM planing 
        removeTemporalEventType(ExchangeInfoStructures::TemporalEvent::TemporalEventType::IAMM);
        
        if (not _longNonInfectiousLammenessWhenLastDryoff)
        {
            // Plan the date for dried period
            boost::gregorian::date_duration dryOffdurationDecision (getDairyFarm()->getDriedPeriodDuration()); 
            boost::gregorian::date_duration theoricalPregnancyDuration (_mTheoricPregnancyDurations.find(getParity())->second); 
            _pHasCurrentLactationState->planDryOffDate(boost::gregorian::date(beginDate + theoricalPregnancyDuration - dryOffdurationDecision));
        }
        // else, the lactation is stopped when milk quantity is insufisant
    }
    
    boost::gregorian::date DairyMammal::getPredictedEndPregnancyDate()
    {
        return getCurrentReproductionState()->getPredictedEndPregnancyDate();
    }
    
    void DairyMammal::notifyPredictedEndPregnancy(const boost::gregorian::date &predictedEndDate)
    {
        getBatch()->notifyPredictedEndSuccessfullPregnancy(this, predictedEndDate);
    }
        
    bool DairyMammal::pregnancyIsOngoing()
    {
        return getCurrentReproductionState()->isPregnancy();
    }
    
    bool DairyMammal::lactationIsOngoing()
    {
        return getCurrentLactationState()->isMilking();
    }
      
    boost::gregorian::date_duration DairyMammal::getDurationSinceBeginPregnancy(const boost::gregorian::date& date)
    {
        return getCurrentPregnancy()->getDurationSinceBegin(date);
    }
    
    unsigned int DairyMammal::getDayDurationUntilPredictedEndPregnancyDate(const boost::gregorian::date& date)
    {
        return getCurrentReproductionState()->getDayDurationUntilPredictedEndPregnancyDate(date);
    }
    
    unsigned int DairyMammal::getTheoricPregnancyDuration()
    {
        return _mTheoricPregnancyDurations.find(getParity())->second;
    }
    
    float DairyMammal::getLateEmbryonicMortalityRisk()
    {
        return _lateEmbryonicMortalityRisk;
    }
    
    float DairyMammal::getAbortionRisk()
    {
        return _abortionRisk;
    }
    
    unsigned int DairyMammal::getPubertyAge()
    {
        return _pubertyAge;
    }
    
    unsigned int DairyMammal::getPostPartumDuration(
#ifdef _LOG
                                                    const boost::gregorian::date& date, 
#endif // _LOG
                                                     bool ketosisPlaned, bool &interrupted)
    {
        unsigned int partumDuration = 0;

        float pcTest = getDairyFarm()->getRandom().ran_flat(0.0f, 1.0f);
        std::vector<float> basisPostPartumDurationProbabilities = (_pPostPartumOvulationDuration->find(getParity()))->second;
        
        bool standardCycle = pcTest <= basisPostPartumDurationProbabilities[0] + basisPostPartumDurationProbabilities[1];
        bool delayedCycle = not standardCycle and pcTest <= basisPostPartumDurationProbabilities[0] + basisPostPartumDurationProbabilities[1] + basisPostPartumDurationProbabilities[2] + basisPostPartumDurationProbabilities[3];
        
        // Depending on deseases occurency
        int deseaseDelayToAdd = 0;
        
#ifdef _LOG
        assert (getParity() != FunctionalEnumerations::Reproduction::DairyCowParity::heifer);
#endif // _LOG
        
        // Ketosis effect
        std::vector<float> ketosisPostPartumDurationProbabilitie = basisPostPartumDurationProbabilities;
        Health::HealthManagement::getKetosisEffectOnPostPartumDelay(standardCycle, Health::HealthManagement::getLactationRankBasisIncidence(getLactationRank(), _crossedLactationRankKetosisIncidence), deseaseDelayToAdd, ketosisPostPartumDurationProbabilitie, ketosisPlaned);
        // Duration treatments
        // --------------
        std::vector<float> finalDurations = ketosisPostPartumDurationProbabilitie; // Just copy for the moment, but if some other deseases are to compute too, finalDurations must be the mean of the specific desease calculated durations
        
        float pcStandardNormal = finalDurations[0];
        float pcStandardInterrupted = finalDurations[1];
        float pcDelayedNormal = finalDurations[2];
        if (standardCycle)
        {
            // Standard cycle
            partumDuration = FunctionalConstants::Reproduction::NORMAL_CYCLE_FIRST_DAY + getDairyFarm()->getRandom().rng_uniform_int(FunctionalConstants::Reproduction::NORMAL_CYCLE_LAST_DAY - FunctionalConstants::Reproduction::NORMAL_CYCLE_FIRST_DAY);
            interrupted = pcTest > pcStandardNormal; // with interrupt ?
#ifdef _LOG
            if (!interrupted)
            {
                addMiscellaneousLog(date, "Standard cycle, and not interrupted");
            }
            else
            {
                addMiscellaneousLog(date, "Standard cycle, and interrupted");
            }
#endif // _LOG

        }
        else if (delayedCycle)
        {
            // Delayed cycle
            partumDuration = FunctionalConstants::Reproduction::NORMAL_CYCLE_LAST_DAY + getDairyFarm()->getRandom().rng_uniform_int(FunctionalConstants::Reproduction::DELAYED_CYCLE_LAST_DAY - FunctionalConstants::Reproduction::NORMAL_CYCLE_LAST_DAY);
            interrupted = pcTest > pcStandardNormal + pcStandardInterrupted + pcDelayedNormal; // with interrupt ?
#ifdef _LOG
            if (!interrupted)
            {
                addMiscellaneousLog(date, "Delayed cycle, and not interrupted");
            }
            else
            {
                addMiscellaneousLog(date, "Delayed cycle, and interrupted");
            }
#endif // _LOG
        }
        else
        {
            // INO
            partumDuration = (FunctionalConstants::Reproduction::DELAYED_CYCLE_LAST_DAY + 1) * (1 + pow(getDairyFarm()->getRandom().ran_flat(0.0f, 1.0f),2));
#ifdef _LOG
            addMiscellaneousLog(date, "INO cycle");
#endif // _LOG 
        }
        
        
        // Potential delay due to deseases
        partumDuration += deseaseDelayToAdd;
        
        return partumDuration;
    }
    
    unsigned int DairyMammal::getPostEarlyInterruptedDuration(
#ifdef _LOG
                                                    const boost::gregorian::date& date, 
#endif // _LOG
                                                    bool &interrupted)
    {
        // It depends on the realized pregnancy period 

        unsigned int postEarlyInterruptedDuration = 0;
        
        unsigned int effectivePregnancyDuration = getLastPregnancyProcess()->getEffectiveDuration().days();
               
        if (effectivePregnancyDuration < FunctionalConstants::Lactation::FIRST_DAY_FOR_ABORTION_COUNTING_LACTATION)
        {
            // we are before the day conting for abortion, the duration increase
            postEarlyInterruptedDuration = FunctionalConstants::Reproduction::MAX_DAY_INCREASE_FOR_INTERRUPTED_PREGNANCY * effectivePregnancyDuration / FunctionalConstants::Lactation::FIRST_DAY_FOR_ABORTION_COUNTING_LACTATION;
            
        }
        else
        {
            // We are at the day counting for abortion, the duration decrease
            postEarlyInterruptedDuration = getPostPartumDuration(
#ifdef _LOG
                                                    date, 
#endif // _LOG
                                                    false, interrupted);
            unsigned int dayForFirstCalving = getDayForFirstCalvings();
            if (effectivePregnancyDuration < dayForFirstCalving)
            {
                // calculate the strait line equation to go to the random post partum duration y = ax + b
                // a = (YB - YA)/(XB - XA)
                float a = (postEarlyInterruptedDuration - FunctionalConstants::Reproduction::MAX_DAY_INCREASE_FOR_INTERRUPTED_PREGNANCY)/(dayForFirstCalving - FunctionalConstants::Lactation::FIRST_DAY_FOR_ABORTION_COUNTING_LACTATION);
                // b = YA - a XA
                float b = FunctionalConstants::Reproduction::MAX_DAY_INCREASE_FOR_INTERRUPTED_PREGNANCY - a *  FunctionalConstants::Lactation::FIRST_DAY_FOR_ABORTION_COUNTING_LACTATION;

                postEarlyInterruptedDuration = a * effectivePregnancyDuration + b;
            }
        }
        return postEarlyInterruptedDuration;
    }
    
    unsigned int DairyMammal::getDayForFirstCalvings()
    {
        return getTheoricPregnancyDuration() - FunctionalConstants::Reproduction::DAY_BEFORE_STANDARD_PREGNANCY_DURATION_FOR_FIRST_CALVINGS;
    }
     
    unsigned int DairyMammal::getTheoricBetweenOvulationDuration()
    {
        return _mTheoricBetweenOvulationDurations.find(getParity())->second;
    }
            
    unsigned int DairyMammal::getOvulationDuration()
    {
        return FunctionalConstants::Reproduction::OVULATION_DURATION;
    }
            
    bool DairyMammal::oestrusIsManaged()
    {
        return getBatch()->oestrusIsManaged();
    }
    
    void DairyMammal::setBreedingDate(const boost::gregorian::date &breedDate, FunctionalEnumerations::Reproduction::DairyCowParity parity)
    {
        boost::gregorian::date calculationBreedDate = breedDate;
        // For heifer, we have to be sure we can set a correct breeding date, regarding the potential grouping calving periods
        if (parity == FunctionalEnumerations::Reproduction::DairyCowParity::heifer)
        {
            boost::gregorian::date potentialCalvingDate = calculationBreedDate + boost::gregorian::date_duration(getTheoricPregnancyDuration());    
            while (not getDairyFarm()->calvingIsAllowedDuringMonth((FunctionalEnumerations::Global::Month)potentialCalvingDate.month().as_number()))
            {
                // It is not the case, we repel of one month
                calculationBreedDate += boost::gregorian::months(1);
                potentialCalvingDate = calculationBreedDate + boost::gregorian::date_duration(getTheoricPregnancyDuration());    
            }  
        }
        _breedingDate = calculationBreedDate;
        
        unsigned int endDay = parity == FunctionalEnumerations::Reproduction::DairyCowParity::heifer ? getDairyFarm()->getNumberOfDaysAfterHeiferBreedingDecisionForStopFirstInseminationDecision() : getDairyFarm()->getNumberOfDaysAfterAdultBreedingDecisionForStopFirstInseminationDecision();
        // We do the same for _endFirstInseminationDate 
        boost::gregorian::date endFirstInseminationDate = _breedingDate + boost::gregorian::days(endDay);
        if (parity == FunctionalEnumerations::Reproduction::DairyCowParity::heifer)
        {
            boost::gregorian::date potentialCalvingDate = endFirstInseminationDate + boost::gregorian::date_duration(getTheoricPregnancyDuration());    
            while (not getDairyFarm()->calvingIsAllowedDuringMonth((FunctionalEnumerations::Global::Month)potentialCalvingDate.month().as_number()))
            {
                // It is not the case, we repel of one month
                endFirstInseminationDate += boost::gregorian::months(1);
                potentialCalvingDate = endFirstInseminationDate + boost::gregorian::date_duration(getTheoricPregnancyDuration());    
            }  
        }
        _endFirstInseminationDate = endFirstInseminationDate;
        
        endDay = parity == FunctionalEnumerations::Reproduction::DairyCowParity::heifer ? getDairyFarm()->getNumberOfDaysAfterHeiferBreedingDecisionForStopInseminationDecision() : getDairyFarm()->getNumberOfDaysAfterAdultBreedingDecisionForStopInseminationDecision();
        // And the same for _endInseminationDate 
        boost::gregorian::date endInseminationDate = _breedingDate + boost::gregorian::days(endDay);;
        if (parity == FunctionalEnumerations::Reproduction::DairyCowParity::heifer)
        {
            boost::gregorian::date potentialCalvingDate = endInseminationDate + boost::gregorian::date_duration(getTheoricPregnancyDuration());    
            while (not getDairyFarm()->calvingIsAllowedDuringMonth((FunctionalEnumerations::Global::Month)potentialCalvingDate.month().as_number()))
            {
                // It is not the case, we repel of one month
                endInseminationDate += boost::gregorian::months(1);
                potentialCalvingDate = endInseminationDate + boost::gregorian::date_duration(getTheoricPregnancyDuration());    
            }  
        }
        _endInseminationDate = endInseminationDate;
    }

    bool DairyMammal::hasToGoToReproduce(const boost::gregorian::date &simDate)
    {
        return      !isUndergoingCull()
                and oestrusIsManaged()
                and simDate >= _breedingDate; // isn't being culled and oestrus is managed and the decision period is now
    }
    
    float DairyMammal::getMilkQuantityOfTheDay()
    {
        return _deliveredMilkOfTheDay.quantity + _discardedMilkOfTheDay.quantity;
    }

    bool DairyMammal::isOvulationDetected(const boost::gregorian::date &simDate, bool first)
    {
        bool res = false;
        if (!hasTemporalEventPeriodPlanned(ExchangeInfoStructures::TemporalEvent::TemporalEventType::IAMM, simDate)) // Is there a IAMM period planned ?
        {
            ExchangeInfoStructures::DetectionConditions dc = ExchangeInfoStructures::DetectionConditions();
            dc.detectionMode = getDetectionMode();
            dc.parity = getParity();
            dc.lastUnfecundedOvulationNumber = getLastUnfecundedOvulationNumber();
            dc.returnInHeatNumber = getReturnInHeatNumber();
            dc.slipperyFloor = getDairyFarm()->floorIsSlipery();
            dc.simultaneousOvulation = getBatch()->getSimultaneousOvulations(); // concomitance ?
            dc.dayMilkProduction = getMilkQuantityOfTheDay();
            dc.lamenessDetectionFactor = getLamenessOestrusDetectionFactor(getDetectionMode(), first);
            res = Genetic::GeneticManagement::isOvulationDetected(getBreedInheritance(), dc, getDairyFarm()->getRandom());                   
        }
        return res;
    }
    
    float DairyMammal::getLamenessOestrusDetectionFactor(FunctionalEnumerations::Reproduction::DetectionMode dm, bool first)
    {
        return _pHasFoots->getLamenessOestrusDetectionFactor(dm, first);
    }
    
    FunctionalEnumerations::Reproduction::DetectionMode DairyMammal::getDetectionMode()
    {
        FunctionalEnumerations::Reproduction::TypeInseminationSemen typeSemen;
        FunctionalEnumerations::Genetic::Breed breedSemen;
        float typeRatio, breedRatio;
        getDairyFarm()->getInseminationParameters(getLactationRank(), getInseminationRank(), typeSemen, typeRatio, breedSemen, breedRatio);

        if (typeSemen ==  FunctionalEnumerations::Reproduction::TypeInseminationSemen::natural)
           return FunctionalEnumerations::Reproduction::DetectionMode::male;
        else return getDairyFarm()->getDetectionMode();
    }

    unsigned int DairyMammal::getUniqueIdForReproductionStates()
    {
        return getUniqueId();
    }

    FunctionalEnumerations::Reproduction::DairyCowParity DairyMammal::getParity()
    {
        if (getLactationRank() == 0) return FunctionalEnumerations::Reproduction::DairyCowParity::heifer;
        else if (getLactationRank() == 1) return FunctionalEnumerations::Reproduction::DairyCowParity::primipare;
        else return FunctionalEnumerations::Reproduction::DairyCowParity::multipare;
    }
    
    void DairyMammal::planPotentialSpecificityError(const boost::gregorian::date &date)
    {
        // If not natural insemination
        if (getDairyFarm()->getDetectionMode() != FunctionalEnumerations::Reproduction::DetectionMode::male)
        {   
            // potentially planned date of IAMM
            boost::gregorian::date plannedIAMMDate;

            if (getParity() == FunctionalEnumerations::Reproduction::DairyCowParity::heifer)
            {
                // Heifer (nulli)

                // selection of the good detection mode array (row)
                const float *pDetectionModeToCompare = nullptr;
                if (getDairyFarm()->getDetectionMode() == FunctionalEnumerations::Reproduction::DetectionMode::detector)
                {
                    pDetectionModeToCompare = &FunctionalConstants::Reproduction::PROBA_HEIFER_SPECIFICITY_ERROR_DETECTOR[0];
                }
                else if (getDairyFarm()->getDetectionMode() == FunctionalEnumerations::Reproduction::DetectionMode::farmer)
                {
                    pDetectionModeToCompare =  &FunctionalConstants::Reproduction::PROBA_HEIFER_SPECIFICITY_ERROR_FARMER[0];
                }
                else if (getDairyFarm()->getDetectionMode() == FunctionalEnumerations::Reproduction::DetectionMode::robot)
                {
                    pDetectionModeToCompare =  &FunctionalConstants::Reproduction::PROBA_HEIFER_SPECIFICITY_ERROR_ROBOT[0];
                }
#ifdef _LOG
                assert (pDetectionModeToCompare != nullptr); // We must have a detection mode
#endif // _LOG

                // Do we have to plan a IAMM in the considered specificity period ?
                for (unsigned int i = 0; i < 2 && plannedIAMMDate.is_not_a_date(); i++)
                {
                    float proba = *(pDetectionModeToCompare + i);
                    if (getDairyFarm()->getRandom().ran_bernoulli(proba))
                    {
                        // Yes, we have to plan a IAMM in the period : calculation of the date of the IAMM
                        unsigned int beginPeriod;
                        unsigned int endPeriod;
                        if (i == 0)
                        {
                            // first period
                            beginPeriod = FunctionalConstants::Reproduction::FIRST_DAY_FIRST_HEIFER_SPECIFICITY_PERIOD;
                            endPeriod = FunctionalConstants::Reproduction::FIRST_DAY_LAST_HEIFER_SPECIFICITY_PERIOD-1;
                        }
                        else
                        {
                            // last period
                            beginPeriod = FunctionalConstants::Reproduction::FIRST_DAY_LAST_HEIFER_SPECIFICITY_PERIOD;
                            endPeriod = beginPeriod + getDairyFarm()->getNumberOfDaysAfterHeiferBreedingDecisionForStopInseminationDecision();
                        }
                        unsigned int iRand = beginPeriod + getDairyFarm()->getRandom().rng_uniform_int((endPeriod - beginPeriod)); // Generate a number in the period 

                        // Creating the planned event
                        plannedIAMMDate = date + boost::gregorian::days(iRand);
                        
                        // We have to verify if the specificity error is possible regarding potentional calving date exclusion
                        boost::gregorian::date potentialCalvingDate = plannedIAMMDate + boost::gregorian::date_duration(getTheoricPregnancyDuration());    
                        if (getDairyFarm()->calvingIsAllowedDuringMonth((FunctionalEnumerations::Global::Month)potentialCalvingDate.month().as_number()))
                        {
                            // Specificity error is consistent
                            boost::gregorian::date beginPlannedIAMMPeriod = date + boost::gregorian::days(beginPeriod);
                            boost::gregorian::date endPlannedIAMMPeriod = date + boost::gregorian::days(endPeriod);
                            ExchangeInfoStructures::TemporalEvent event(ExchangeInfoStructures::TemporalEvent::TemporalEventType::IAMM, beginPlannedIAMMPeriod, plannedIAMMDate, endPlannedIAMMPeriod);
                            addTemporalEvent(event);
                        }
                    }
                }
            }
            else
            { 
                // Cow (primi and multi)
                // selection of the good detection mode array (row)
                const float *pDetectionModeToCompare = nullptr;
                if (getDairyFarm()->getDetectionMode() == FunctionalEnumerations::Reproduction::DetectionMode::detector)
                {
                    pDetectionModeToCompare = &FunctionalConstants::Reproduction::PROBA_COW_SPECIFICITY_ERROR_DETECTOR[0];
                }
                else if (getDairyFarm()->getDetectionMode() == FunctionalEnumerations::Reproduction::DetectionMode::farmer)
                {
                    pDetectionModeToCompare =  &FunctionalConstants::Reproduction::PROBA_COW_SPECIFICITY_ERROR_FARMER[0];
                }
                else if (getDairyFarm()->getDetectionMode() == FunctionalEnumerations::Reproduction::DetectionMode::robot)
                {
                    pDetectionModeToCompare =  &FunctionalConstants::Reproduction::PROBA_COW_SPECIFICITY_ERROR_ROBOT[0];
                }
#ifdef _LOG
                assert (pDetectionModeToCompare != nullptr); // We must have a detection mode
#endif // _LOG

                // Selection of the good reproduction state column
                FunctionalEnumerations::Reproduction::DairyCowParity parity = getParity();
                int baseConcernedColumn = ((int)parity-1)*3;

                // Do we have to plan a IAMM in the considered specificity period ?
                for (unsigned int i = 0; i < 3 && plannedIAMMDate.is_not_a_date(); i++)
                {
                    float proba = *(pDetectionModeToCompare + baseConcernedColumn + i);
                    if (getDairyFarm()->getRandom().ran_bernoulli(proba))
                    {
                        // Yes, we have to plan a IAMM in the period : calculation of the date of the IAMM
                        unsigned int beginPeriod;
                        unsigned int endPeriod;
                        if (i == 0)
                        {
                            // first period
                            beginPeriod = FunctionalConstants::Reproduction::FIRST_DAY_FIRST_COW_SPECIFICITY_PERIOD;
                            endPeriod = FunctionalConstants::Reproduction::FIRST_DAY_SECUND_COW_SPECIFICITY_PERIOD-1;
                        }
                        else if (i == 1)
                        {
                            // second period
                            beginPeriod = FunctionalConstants::Reproduction::FIRST_DAY_SECUND_COW_SPECIFICITY_PERIOD;
                            endPeriod = FunctionalConstants::Reproduction::FIRST_DAY_LAST_COW_SPECIFICITY_PERIOD-1;
                        }
                        else
                        {
                            // last period
                            beginPeriod = FunctionalConstants::Reproduction::FIRST_DAY_LAST_COW_SPECIFICITY_PERIOD;
                            endPeriod = beginPeriod + getBreedingDelayAfterCalvingDecision() + getDairyFarm()->getNumberOfDaysAfterAdultBreedingDecisionForStopInseminationDecision();
                        }
                        unsigned int iRand = beginPeriod + getDairyFarm()->getRandom().rng_uniform_int((endPeriod - beginPeriod)); // Generate a number in the period     

                        unsigned int delay = getBreedingDelayAfterCalvingDecision();

                        if (iRand >= delay)
                        {
                            // Creating the planned event
                            boost::gregorian::date beginPlannedIAMMPeriod = date + boost::gregorian::days(beginPeriod);
                            plannedIAMMDate = date + boost::gregorian::days(iRand);
                            boost::gregorian::date endPlannedIAMMPeriod = date + boost::gregorian::days(endPeriod);

                            ExchangeInfoStructures::TemporalEvent event(ExchangeInfoStructures::TemporalEvent::TemporalEventType::IAMM, beginPlannedIAMMPeriod, plannedIAMMDate, endPlannedIAMMPeriod);
                            addTemporalEvent(event);
                        }
                    }
                }
            }
        }
    }
    
    float DairyMammal::getTwinningProba()
    {
        return FunctionalConstants::Reproduction::TWINNING_PROBABILITY.find(getParity())->second;
    }
    
    void DairyMammal::predictEndPregnancyConditions(const boost::gregorian::date &pregnancyDate, boost::gregorian::date &predictedEndPregnancyDate, FunctionalEnumerations::Reproduction::EndPregnancyReason &predictedEndPregnancyReason, std::vector<FunctionalEnumerations::Genetic::Sex> &vSexes, bool onlyForCalving)
    {        
        // Late embryonic mortality ?
        if (!onlyForCalving)
        {
            if (getDairyFarm()->getRandom().ran_bernoulli(getLateEmbryonicMortalityRisk()))
            {
                // We'll have a late embryonic mortality
                unsigned int iRand = getDairyFarm()->getRandom().betaPertDistribution((double)FunctionalConstants::Reproduction::FIRST_DAY_FOR_LATE_EMBRYONIC_PERIOD, (double)FunctionalConstants::Reproduction::FIRST_DAY_FOR_ABORTION_PERIOD-1, (double)FunctionalConstants::Reproduction::PEAK_DAY_IN_LATE_EMBRYONIC_PERIOD, (double)4);
                predictedEndPregnancyDate = pregnancyDate + boost::gregorian::days(iRand);
                predictedEndPregnancyReason = FunctionalEnumerations::Reproduction::EndPregnancyReason::embryoLoose;
                return;
            }
            // Abortion ?
            else if (getDairyFarm()->getRandom().ran_bernoulli(getAbortionRisk()))
            {
                // We'll have an abortion or premature calving
                unsigned int beginPeriod = FunctionalConstants::Reproduction::FIRST_DAY_FOR_ABORTION_PERIOD;
                unsigned int endPeriod = getDayForFirstCalvings() - 1;
                unsigned int iRand = beginPeriod + getDairyFarm()->getRandom().rng_uniform_int((endPeriod - beginPeriod)); // Generate a number in the period
                predictedEndPregnancyDate = pregnancyDate + boost::gregorian::days(iRand);
                predictedEndPregnancyReason = FunctionalEnumerations::Reproduction::EndPregnancyReason::abortion;

                // Farmer estimation of when to leave the heifer batch
                boost::gregorian::date theoricEndPregnancyDate = pregnancyDate + boost::gregorian::days(getTheoricPregnancyDuration());
                notifyPredictedEndPregnancy(theoricEndPregnancyDate);
                return;
            }
        }
 
        // Calving
        // predict number and sexes for duration calculation
        unsigned int numberOfBirth = 1; // Default one birth
        if (getDairyFarm()->getRandom().ran_bernoulli(getTwinningProba()))
        {
            // two births
            numberOfBirth = 2;
        }

        // Determining the sex
        for (unsigned int i = 0; i < numberOfBirth; i++) 
        {
            // sex
            FunctionalEnumerations::Genetic::Sex sex;
            if (birthIsFemale())
            {
                // female
                sex = FunctionalEnumerations::Genetic::Sex::female;
            }
            else
            {
                // male
                sex = FunctionalEnumerations::Genetic::Sex::male;
            }
            vSexes.push_back(sex);
        }

        // pregnancy duration reductions ?
        unsigned int durationDecrease = 0;

        if (numberOfBirth == 2) durationDecrease += FunctionalConstants::Reproduction::PREGNANCY_DURATION_DECREASE_FOR_TWINS;
        else if (vSexes[0] == FunctionalEnumerations::Genetic::Sex::male) durationDecrease += FunctionalConstants::Reproduction::PREGNANCY_DURATION_DECREASE_FOR_MALE;
        boost::gregorian::date_duration pregnancyDuration(getTheoricPregnancyDuration() - durationDecrease + getDairyFarm()->getRandom().ran_gaussian(0.0f, FunctionalConstants::Reproduction::STANDARD_DEVIATION_PREGNANCY_DURATION));
        predictedEndPregnancyDate = pregnancyDate + pregnancyDuration;
        predictedEndPregnancyReason = FunctionalEnumerations::Reproduction::EndPregnancyReason::calving;

        // Plan the potential exit of the Bred heifer batch 
        notifyPredictedEndPregnancy(predictedEndPregnancyDate);
    }
    
    bool DairyMammal::birthIsFemale()
    {
        // get the type of insemination
        return getDairyFarm()->getRandom().ran_bernoulli(FunctionalConstants::Reproduction::SEMEN_FEMALE_BIRTH_PROBA.find(getLastInsemination()->getTypeSemen())->second);
    }

    void DairyMammal::calve(const boost::gregorian::date &calvingDate, std::vector<FunctionalEnumerations::Genetic::Sex> &sexes, FunctionalEnumerations::Reproduction::EndPregnancyReason term)
    {
        const boost::gregorian::date lastCalvingDate = getLastFarrowingDate();
        
        if(!lastCalvingDate.is_not_a_date())
        {
            // Multiparous
            // IVV calculation
            boost::gregorian::date_duration ivv = calvingDate - lastCalvingDate;
            _lastCalvingIntervalInMonth = ivv.days() / 30; // days to months
            
            // For IVV
            getDairyHerd()->addAnnualCalvingCalvingInterval((float)ivv.days());

#ifdef _LOG
            Results::Interval it;
            it.date1 = lastCalvingDate;
            it.date2 = calvingDate;
            it.comp = getUniqueId();
            if (_G2ketosisCumulateDurationInTheLactation > 0) it.comp2 = FunctionalEnumerations::Health::KetosisSeverity::G2Ketosis;
            else if (_G1ketosisCumulateDurationInTheLactation > 0) it.comp2 = FunctionalEnumerations::Health::KetosisSeverity::G1Ketosis;
            else it.comp2 = FunctionalEnumerations::Health::KetosisSeverity::G0Ketosis;
            getCurrentResults()->getReproductionResults().IVV.push_back(it);
#endif // _LOG      
        }
#ifdef _LOG
        std::string log;
        log = "Calving ";   
        log += Tools::toString(sexes.size()) + " calf(ves)";
#endif // _LOG      
        
        // Determining if a female is sterile
        bool femaleSterile = false;
        for (unsigned int i = 0; i < sexes.size(); i++)
        {
            if (sexes[i] == FunctionalEnumerations::Genetic::Sex::male)
            {
                femaleSterile = true; // When a male is gemal with a female, the female is sterile
#ifdef _LOG
                log += " male";
#else 
                break;
#endif // _LOG
            }
#ifdef _LOG
            else
            {
                log += " female";
            }
#endif // _LOG        
        }

        // Make the birthes with the sterile states
        DairyHerd* pHerd = getDairyHerd();
        for (unsigned int i = 0; i < sexes.size(); i++)
        {
            DairyMammal* pNewBorn = nullptr;
            
            if (sexes[i] == FunctionalEnumerations::Genetic::Sex::female)
            {
                pNewBorn = (DairyMammal*)farrowing(calvingDate, Population::PopulationManagement::getNewDairyMammal(calvingDate, this, getCurrentPregnancy()->getFertilyInsemination()->getBreed(), femaleSterile, FunctionalEnumerations::Genetic::Sex::female));
            }
            else
            {
                pNewBorn = (DairyMammal*)farrowing(calvingDate, Population::PopulationManagement::getNewDairyMammal(calvingDate, this, getCurrentPregnancy()->getFertilyInsemination()->getBreed(), false, FunctionalEnumerations::Genetic::Sex::male));
            }
                                        
            // Calf batch affectation
            Population::PopulationManagement::affectNewBornCalfToBatch(pNewBorn, calvingDate, getDairyFarm()->getFemaleCalveManagementStrategy());

            // Creating the entry
            float cost = 0.0f;
            if (not getDairyFarm()->hasVetReproductionContract())
            {
                cost = getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_CALF_BIRTH_PRICE);
            }
            pHerd->getDairyFarm()->addMove(new Population::Birth(pNewBorn
#ifdef _LOG
                                            , calvingDate 
#endif // _LOG
                                            , cost, getDairyHerd()->getReproductionCostsForUpdate()));
            
            _pHasCurrentMastitisRiskPeriodState->calvingIsDone();
                                    
#ifdef _LOG
            // Only in debug mode
            
            // for SexResult
            Results::RatioResult sr;
            sr.id = getUniqueId();
            sr.date = calvingDate;
            sr.parity = getParity();
            sr.val = pNewBorn->getSex() == FunctionalEnumerations::Genetic::Sex::female;
            getCurrentResults()->getReproductionResults().SexResults.push_back(sr);
            
            // Only in debug mode, to display the VGLait and the simulation day of the new born
            Results::GeneticResult gr;
            gr.date = calvingDate;
            gr.idOrBreed = pNewBorn->getUniqueId();
            gr.birth = true; // Born in the herd
            gr.VGLait = ((DairyMammal*)pNewBorn)->getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait).getCorrectedPerformance();
            gr.VGTB = ((DairyMammal*)pNewBorn)->getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::TB).getCorrectedPerformance();
            gr.VGTP = ((DairyMammal*)pNewBorn)->getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::TP).getCorrectedPerformance();
            gr.VGFer = ((DairyMammal*)pNewBorn)->getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer).getCorrectedPerformance();
            gr.VGMACL = ((DairyMammal*)pNewBorn)->getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL).getCorrectedPerformance();
            gr.VGBHBlait = ((DairyMammal*)pNewBorn)->getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait).getCorrectedPerformance();
            gr.VGRBi = ((DairyMammal*)pNewBorn)->getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi).getCorrectedPerformance();
            gr.VGRBni = ((DairyMammal*)pNewBorn)->getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni).getCorrectedPerformance();
            getCurrentResults()->getGeneticResults().FemaleVGResults.push_back(gr);
#endif // _LOG        
        }
        
#ifdef _LOG
        addMiscellaneousLog(calvingDate, log);
#endif // _LOG        
        
        setEndPregnancyDate(lastCalvingDate, calvingDate, term);
        increaseLactationRank(pHerd, calvingDate);
        pHerd->increaseAnnualCalvingCount();
        _ketosisTestSinceLastCalvingDone = false;
        
        // Result based on a calving
        // -------------------------
        if (getParity() == FunctionalEnumerations::Reproduction::DairyCowParity::primipare)
        {
            getDairyHerd()->addAnnualFirstCalvingAge((float)_firstFarrowingAge);
#if defined _LOG || defined _FULL_RESULTS
            // Primipare
            Results::PrimipareReproductionResultBasedOnCalving data;
            data.id = getUniqueId();
            data.obtainedCalvingDate = _lastFarrowingDate;
            data.firstInseminationAge = _firstFirstInseminationAge;
            data.fertilizingInseminationAge = _firstFertilizingInseminationAge;
            data.obtainedCalvingAge = _firstFarrowingAge;
            data.firstInseminationDate = _lastFirstInseminationDate;
            data.firstInseminationType = _lastFirstInseminationType;
            data.firstInseminationMaleBreed = _lastFirstInseminationMaleBreed;
            data.inseminationQuantity = _inseminationQuantitySinceLastFarrowing;
            data.fertilizingInseminationDate = _lastFertilizingInseminationDate;
            data.fertilizingInseminationNumber = _lastFertilizingInseminationNumber;
            data.fertilizingInseminationType = _lastFertilizingInseminationType;
            data.fertilizingInseminationMaleBreed = _lastFertilizingInseminationMaleBreed;
            getCurrentResults()->getReproductionResults().PrimipareReproductionResultsBasedOnCalving.push_back(data);
#endif // _LOG || _FULL_RESULTS
        }
#if defined _LOG || defined _FULL_RESULTS
        else
        {
            // multipare
            Results::MultipareReproductionResultBasedOnCalving data;
            data.id = getUniqueId();
            data.obtainedCalvingDate = _lastFarrowingDate;
            data.previousCalvingDate = _previousFarrowingDate;
            data.previousCalvingRank = getFarrowingNumber()-1;
            data.firstInseminationDate = _lastFirstInseminationDate;
            data.firstInseminationType = _lastFirstInseminationType;
            data.firstInseminationMaleBreed = _lastFirstInseminationMaleBreed;
            data.inseminationQuantity = _inseminationQuantitySinceLastFarrowing;
            data.fertilizingInseminationDate = _lastFertilizingInseminationDate;
            data.fertilizingInseminationNumber = _lastFertilizingInseminationNumber;
            data.fertilizingInseminationType = _lastFertilizingInseminationType;
            data.fertilizingInseminationMaleBreed = _lastFertilizingInseminationMaleBreed;
            getCurrentResults()->getReproductionResults().MultipareReproductionResultsBasedOnCalving.push_back(data);
        }
#endif // _LOG || _FULL_RESULTS
    }
    
    void DairyMammal::increaseLactationRank(DairyHerd* pHerd, const boost::gregorian::date &date)
    {
        _lactationRank++;
        
        if (_lactationRank == 1)
        {
            // Become adult
            getHerd()->appendPresentAdult(this);    
        }
        
        // Start lactation
        setCurrentLactationState(new Lactation::DairyHerdLactationStates::MilkProducingState(this, date, Lactation::LactationManagement::getDriedPeriodEffectToMilk(date, _lastDryOffDate, lactationIsOngoing(), _lactationRank)));
        
        if (getLactationRank() >= getDairyFarm()->getMaxLactationRankForEndInseminationDecision())
        {
#ifdef _LOG
            addMiscellaneousLog(date, "Non insemination decision(Lactation rank)");
#endif // _LOG        

            getDairyHerd()->increaseAnnualNotBredBecauseOfLactationRankCount();
            cullingIsDecided(FunctionalEnumerations::Population::CullingStatus::toCullDueToLactationRank, false, date);
        }
        else if (_longNonInfectiousLammenessWhenLastDryoff)
        {
#ifdef _LOG
            addMiscellaneousLog(date, "Non insemination decision (durable non infectious lameness)");
#endif // _LOG        

            cullingIsDecided(FunctionalEnumerations::Population::CullingStatus::toCullDueToNonInfectiousLameness, false, date);
        }
        else if (getDairyHerd()->cowIsToCullLaterRegardingPopulation(this))
        {
#ifdef _LOG
            addMiscellaneousLog(date, "Non insemination decision (population regulation)");
#endif // _LOG        

            cullingIsDecided(FunctionalEnumerations::Population::CullingStatus::toCullDueToPopulationRegulation, false, date);
        }
        else
        {
            // Set the next breeding date
            boost::gregorian::date nextBreedDate = date + boost::gregorian::days(getDairyFarm()->getBreedingDelayAfterCalvingDecision());
            setBreedingDate(nextBreedDate, getParity());    
        }
    }
     
    void DairyMammal::looseEmbryo(const boost::gregorian::date &date)
    {
        Mammal::looseEmbryo(date);
    }

    void DairyMammal::abort(const boost::gregorian::date &date, bool increaseLactationRk)
    {
        Mammal::abort(date);    
        if (increaseLactationRk)
        {
            farrowing(date, nullptr); // false calving
            increaseLactationRank(getDairyHerd(), date);                  
        }
        else
        {
            // Notify batch 
            getBatch()->notifyUnsuccessfulPregnancyEnd(this);   
        }
    }
    
    void DairyMammal::setEndPregnancyDate(const boost::gregorian::date& lastCalvingDate, const boost::gregorian::date& endDate, FunctionalEnumerations::Reproduction::EndPregnancyReason reason)
    {
#ifdef _LOG
        // Only in debug mode
        if (reason != FunctionalEnumerations::Reproduction::EndPregnancyReason::embryoLoose and reason != FunctionalEnumerations::Reproduction::EndPregnancyReason::abortion)
        {
            // Results
            
            if (!lastCalvingDate.is_not_a_date()) // We had a previous calving
            {
                // IVIAF
                Results::Interval it;
                it.date1 = lastCalvingDate;
                it.date2 = _lastFertilizingInseminationDate;
                it.comp = getUniqueId();
                if (_G2ketosisCumulateDurationInTheLactation > 0) it.comp2 = FunctionalEnumerations::Health::KetosisSeverity::G2Ketosis;
                else if (_G1ketosisCumulateDurationInTheLactation > 0) it.comp2 = FunctionalEnumerations::Health::KetosisSeverity::G1Ketosis;
                else it.comp2 = FunctionalEnumerations::Health::KetosisSeverity::G0Ketosis;
                getCurrentResults()->getReproductionResults().IVIAF.push_back(it);
            }
            else
            {
                // It is the first farrowing
            
                // V1Age
                Results::Age a;
                a.date = endDate;
                a.age = getAge();
                a.id = getUniqueId();
                getCurrentResults()->getReproductionResults().V1Age.push_back(a);
            }         
        }
#endif // _LOG            // If get only the IVIAF for not embryo loose
        
        Mammal::setEndPregnancyDate(lastCalvingDate, endDate, reason);

        // Cancel the potentiel planned dry off date
        getCurrentLactationState()->cancelPlanifiedDryOffDate();
        
        // init unsucessful insemination counter
        _consecutiveUnsucessfulInseminationCount = 0;
    }

    bool DairyMammal::hasTemporalEventPlanned(ExchangeInfoStructures::TemporalEvent::TemporalEventType theTypeEvent, const boost::gregorian::date &theDate)
    {
        return isItTheDayForPlannedTemporalEvent(theTypeEvent, theDate);
    }

    DairyUdder* DairyMammal::getDairyUdder()
    {
        return (DairyUdder*) _pHasUdder;
    }

    void DairyMammal::planPotentialIAReturnSpecificityError(const boost::gregorian::date &lastIADate)
    {
        // Potentially planned date of IAMM
        if (getDairyFarm()->getRandom().ran_bernoulli(FunctionalConstants::Reproduction::PROBA_SPECIFICITY_ERROR_IA_RETURN))
        {
            // Yes, we have to plan a IAMM in the period : calculation of the date of the IAMM
            unsigned int iRand = getTheoricBetweenOvulationDuration() + getDairyFarm()->getRandom().ran_gaussian(0.0f, FunctionalConstants::Reproduction::STANDARD_DEVIATION_FOR_SPECIFICITY_ERROR_IA_RETURN);
            
            // Creating the planned event
            boost::gregorian::date beginPlannedIAMMPeriod = lastIADate + boost::gregorian::days(1); // following day of the last insemination
            boost::gregorian::date plannedIAMMDate = lastIADate + boost::gregorian::days(iRand); // The day of the IAMM (specificity error))
            boost::gregorian::date endPlannedIAMMPeriod = plannedIAMMDate; // end of the specificity error period

            ExchangeInfoStructures::TemporalEvent event(ExchangeInfoStructures::TemporalEvent::TemporalEventType::IAMM, beginPlannedIAMMPeriod, plannedIAMMDate, endPlannedIAMMPeriod);
            addTemporalEvent(event);
        }
    }
    
    bool DairyMammal::needTreatmentForAbnormalNoOestrusDetection(const boost::gregorian::date &date, bool reproVetContract)
    {
       return _pHasCurrentReproductionState->needTreatmentForAbnormalNoOestrusDetection(date, reproVetContract);
    }
    
    bool DairyMammal::needTreatmentForAbnormalUnsuccessfulIA(const boost::gregorian::date &date, bool reproVetContract)
    {
       return  _pHasCurrentReproductionState->needTreatmentForAbnormalUnsuccessfulIA(date, reproVetContract);
    }
    
    bool DairyMammal::needTreatmentForNegativePregnancyDiagnostic(const boost::gregorian::date &date, bool reproVetContract)
    {
       return  _pHasCurrentReproductionState->needTreatmentForNegativePregnancyDiagnostic(date, reproVetContract);
    }
    
    bool DairyMammal::isInAbnormalNoOestrusDetectionSituation(const boost::gregorian::date &date, bool reproVetContract)
    {
#ifdef _LOG
        assert(isAdult() and not isUndergoingCull()); // Done before, but just to be sure
        assert(not _lastOestrusDetectionDate.is_not_a_date()); // if adult, an oestrus had normally already been detected...
        assert(not _pHasCurrentReproductionState->isPregnancy()); // Done before, but just to be sure
#endif // _LOG
        unsigned int lastCalvingDuration = boost::gregorian::date_period(_lastPregnancyEndDate, date).length().days();    
                
        unsigned int dayNumber = reproVetContract ? FunctionalConstants::Reproduction::DAY_NUMBER_FOR_NO_OBSERVED_OESTRUS_VET_CONTRACT : FunctionalConstants::Reproduction::DAY_NUMBER_FOR_NO_OBSERVED_OESTRUS_NO_VET_CONTRACT;
        
        bool notYetDetected = not (_lastOestrusDetectionDate > _lastPregnancyEndDate);
        
        if (reproVetContract)
        {
            return (lastCalvingDuration >= dayNumber) and notYetDetected;
        }
        else
        {
            return (lastCalvingDuration == dayNumber) and notYetDetected;
        }
    }

    bool DairyMammal::isInAbnormalUnsuccessfulIASituation(const boost::gregorian::date &date, bool reproVetContract)
    {
#ifdef _LOG
        assert(isAdult() and not isUndergoingCull()); // Done before, but just to be sure
        assert(not _pHasCurrentReproductionState->isPregnancy()); // Done before, but just to be sure
#endif // _LOG
        return _consecutiveUnsucessfulInseminationCount >= FunctionalConstants::Reproduction::NON_FECUNDANT_INSEMINATION_NUMBER;
    }

    bool DairyMammal::isInNegativePregnancyDiagnosticSituation(const boost::gregorian::date &date, bool reproVetContract)
    {
#ifdef _LOG
        assert(isAdult() and not isUndergoingCull()); // Done before, but just to be sure
#endif // _LOG
        unsigned int lastInseminationDuration = boost::gregorian::date_period(_lastInseminationDate, date).length().days();
        if (_lastInseminationDate > _lastPregnancyEndDate and lastInseminationDuration > FunctionalConstants::Reproduction::DAY_NUMBER_FOR_NEGATIVE_PREGNANCY_DIAGNOSTIC)
        {
            return getDairyFarm()->getRandom().ran_bernoulli(_pHasCurrentReproductionState->getNegativeDGProba());
        }
        return false;
    }
    
    void DairyMammal::takeAbnormalNoOestrusDetectionTreatment(const boost::gregorian::date &date, ExchangeInfoStructures::Parameters::TreatmentTypeParameters &treatment)
    {
#ifdef _LOG
        addMiscellaneousLog(date, "takeAbnormalNoOestrusDetectionTreatment");
#endif // _LOG
        Reproduction::ReproductionManagement::getForcedOvulation(date, this, getDairyFarm()->getReproductionTreatmentPlan().noObservedOestrusReproductionTreatmentType);   
    }
    
    void DairyMammal::takeAbnormalUnsuccessfulIATreatment(const boost::gregorian::date &date, ExchangeInfoStructures::Parameters::TreatmentTypeParameters &treatment)
    {
#ifdef _LOG
        addMiscellaneousLog(date, "takeAbnormalUnsuccessfulIATreatment");
#endif // _LOG
        Reproduction::ReproductionManagement::getForcedOvulation(date, this, getDairyFarm()->getReproductionTreatmentPlan().tooMuchUnsucessfulInseminationReproductionTreatmentType);
    }
    
    void DairyMammal::takeNegativePregnancyDiagnosticTreatment(const boost::gregorian::date &date, ExchangeInfoStructures::Parameters::TreatmentTypeParameters &treatment)
    {
#ifdef _LOG
        addMiscellaneousLog(date, "takeNegativePregnancyDiagnosticTreatment");
#endif // _LOG
        Reproduction::ReproductionManagement::getForcedOvulation(date, this, getDairyFarm()->getReproductionTreatmentPlan().negativePregnancyReproductionTreatmentType);
    }
    
    void DairyMammal::updateLastOestrusDetectionDate(const boost::gregorian::date &detectionDate)
    {
        _lastOestrusDetectionDate = detectionDate;
    }
    
    void DairyMammal::setFuturForcedOvulation(Reproduction::DairyHerdReproductionStates::OvulationState* _pForcedOvulationState)
    {
#ifdef _LOG
        assert (_pFuturForcedOvulationState == nullptr);
#endif // _LOG
        _pFuturForcedOvulationState = _pForcedOvulationState;
    }
    
    Reproduction::DairyHerdReproductionStates::OvulationState* DairyMammal::getForcedOvulation(const boost::gregorian::date &date)
    {
        if (_pFuturForcedOvulationState != nullptr)
        {
            if (_pFuturForcedOvulationState->getBeginDate() == date)
            {
                Reproduction::DairyHerdReproductionStates::OvulationState* pOvulationState = _pFuturForcedOvulationState;
                _pFuturForcedOvulationState = nullptr;
                return pOvulationState;
            }
            return nullptr;
        }
        return nullptr;
    }
    
    float DairyMammal::getDeltaFertilityVetContract()
    {
        return getDairyFarm()->hasVetReproductionContract() ? FunctionalConstants::Reproduction::VET_CONTRACT_FERTILITY_DELTA : 0.0f;
    }

#ifdef _LOG
    void DairyMammal::addDairyHerdReproductionAnimalLog(const boost::gregorian::date &date, const std::string &log)
    {
        addMiscellaneousLog(date, log);
    }
    void DairyMammal::addDairyHerdFeedingAnimalLog(const boost::gregorian::date &date, const std::string &log)
    {
        addMiscellaneousLog(date, log);
    }
#endif // _LOG 
    
    bool DairyMammal::isAdult()
    {
        return _lactationRank > 0;
    }

    // Will be done when conditions allow it (canBeDelayedlyCulled() method)
    bool DairyMammal::isUndergoingCull()
    {
        return _cullingStatus != FunctionalEnumerations::Population::CullingStatus::notToCull;
    }

    float DairyMammal::getDamLastFullLactationMilkQuantity()
    {
        return _damLastFullLactationPonderedMilkQuantity;
    }
     
    void DairyMammal::setSaleStatus(FunctionalEnumerations::Population::SaleStatus saleStatus)
    {
        _saleStatus = saleStatus;
    }

    FunctionalEnumerations::Population::SaleStatus DairyMammal::getSaleStatus()
    {
        return _saleStatus;
    }
           
    float DairyMammal::getTodayCullingScore()
    {
        return Population::PopulationManagement::calculateTodayCullingScoreOfDairyMammal(this);
    }
    
    void DairyMammal::cullingIsDecided (FunctionalEnumerations::Population::CullingStatus newCullingStatus, bool today, const boost::gregorian::date &date)
    {
        Animal::cullingIsDecided(newCullingStatus, today, date);
        getCurrentLactationState()->cancelPlanifiedDryOffDate(); // Let the lactation continue
    }

    float DairyMammal::getExitAmount()
    {
        // Todo : use batch to know exit price, and not anymore if statements
        float amount;
        if (_dead)
        {
            // dead
            if (getAge() < 365)
            {
                amount = getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_UNDER_ONE_YEAR_DEATH_CALF_PRICE);
            }
            else if (getAge() < 730)
            {
                amount = getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_UNDER_TWO_YEAR_DEATH_CALF_PRICE);
            }
            else
            {
                amount = getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_DEATH_COW_PRICE); 
            }
        }
        else
        {
            // alive
            if (getAge() <= FunctionalConstants::Population::AGE_TO_GO_IN_YOUNG_UNBRED_HEIFERS_BATCH)
            {
                if (getSex() == FunctionalEnumerations::Genetic::Sex::female)
                {
                    // female
                    if (getBreedInheritance()->isPureDairy())
                    {
                        // Dairy
                        amount = getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_FEMALE_DAIRY_CALF_PRICE);
                    }
                    else
                    {
                        // Beef
                        amount = getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_FEMALE_BEEF_BRED_CALF_PRICE);
                    }
                }
                else
                {
                    // male
                    if (getBreedInheritance()->isPureDairy())
                    {
                        // Dairy
                        amount = getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_MALE_DAIRY_CALF_PRICE);
                    }
                    else
                    {
                        // Beef
                        amount = getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_MALE_BEEF_BRED_CALF_PRICE);
                    }
                }
            }
            else if (pregnancyIsOngoing())
            {
                amount = getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_PREGNANT_HEIFER_PRICE);
            }
            else    
            {
                amount = getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_CULLED_COW_PRICE);
            }
        }
        return amount; 
    }

    bool DairyMammal::isItTheDayForAnimalPlannedTemporalEvent(ExchangeInfoStructures::TemporalEvent::TemporalEventType theTypeEvent, const boost::gregorian::date &theDate)
    {
        return Animal::isItTheDayForPlannedTemporalEvent(theTypeEvent, theDate);
    }
    
    // Production
    // ----------
#ifdef _LOG
    void DairyMammal::addDairyHerdLactationAnimalLog(const boost::gregorian::date &date, const std::string &log)
    {
        addMiscellaneousLog(date, log);
    }
#endif // _LOG 
    
    void DairyMammal::setCurrentLactationState(Lactation::DairyHerdLactationStates::DairyHerdLactationState* pState)
    {
        if (pState != nullptr)
        {
            if (_pHasCurrentLactationState != nullptr)
            {
               delete _pHasCurrentLactationState;
            }
            _pHasCurrentLactationState = pState;
        }
    }

    unsigned int DairyMammal::getWeaningAge()
    {
        return getDairyFarm()->getWeaningAge();
    }
    
    Lactation::DairyHerdLactationStates::DairyHerdLactationState* DairyMammal::getCurrentLactationState()
    {
        return _pHasCurrentLactationState;
    }

    bool DairyMammal::isMilking()
    {
        return getCurrentLactationState()->isMilking();
    }
    
    unsigned int DairyMammal::getUniqueIdForLactationState()
    {
        return getUniqueId();
    }

     bool DairyMammal::getMilkControlInformations(bool useCetodetect, bool &controled, int &lactationStage, unsigned int &lactationRank, bool &SCCLessThanRefInPreviousMilkControl, bool &SCCLessThanRefNow, const boost::gregorian::date &simDate, ExchangeInfoStructures::MilkProductCharacteristic &cumulativeIndividualMilkSamples
                                                                                                                                        , bool &isDetectedInfectiousLameness
                                                                                                                                        , bool &isInfectiousG1Lameness
                                                                                                                                        , bool &isInfectiousG2Lameness
                                                                                                                                        , bool &isDetectedNonInfectiousLameness
                                                                                                                                        , bool &isNonInfectiousG1Lameness
                                                                                                                                        , bool &isNonInfectiousG2Lameness
#ifdef _LOG
                                                                                                                                        , FunctionalEnumerations::Health::KetosisSeverity &ketosisSeverity
                                                                                                                                        , FunctionalEnumerations::Global::Month &lastCalvingMonth    
                                                                                                                                        , bool &newKetosisCase
#endif // _LOG
                                                                                    )
    {
        bool isAdult = getLactationRank() > 0;
        controled = false;
        if (isAdult)
        {
            if (isMilking())
            {
                assert(getLactationStage() >= 0);
                if (((unsigned int)getLactationStage()) > getDairyFarm()->getDayBeforeDeliverMilk())
                {
                    // The milk of this cow is controled
                    controled = true;
#ifdef _LOG
                    addMiscellaneousLog(simDate, "Milk controled");
#endif // _LOG

//                    std::cout << _lactationStage << std::endl;
                    lactationStage = _lactationStage;
                    lactationRank = getLactationRank();
                    _lastControledMilk  = _deliveredMilkOfTheDay + _discardedMilkOfTheDay;
                    _lastControledMilk.quantity = FunctionalConstants::Lactation::MILK_QUANTITY_FOR_CONTROL;
                    cumulativeIndividualMilkSamples += _lastControledMilk;
                    SCCLessThanRefNow = _lastControledMilk.SCC < FunctionalConstants::Lactation::SCC_REFERENCE_FOR_MILK_CONTROL;
                    SCCLessThanRefInPreviousMilkControl = _lastSCCWasLessThanRef;
                    _lastSCCWasLessThanRef = SCCLessThanRefNow;

                    // For know if we have to cull regarding SCC
                    bool SCCControlIsOverRef = _lastControledMilk.SCC > getDairyFarm()->getMaxSCCLevelForEndInseminationDecision();
#ifdef _LOG
                    if (SCCControlIsOverRef) addMiscellaneousLog(simDate, "SCC is over ref");
#endif // _LOG
                    if (_notYetControledForThisLactation and  // It is the first control for this lactation
                        _lastSCCControlWasOverRef and _beforeLastSCCControlWasOverRef and // 2 last controls was over
                        SCCControlIsOverRef   // This control is over
                        )
                    {
                        // Culling decision
                        cullingIsDecided(FunctionalEnumerations::Population::CullingStatus::toCullDueToMastitis, false, simDate);
                    }
                    _beforeLastSCCControlWasOverRef = _lastSCCControlWasOverRef;
                    _lastSCCControlWasOverRef = SCCControlIsOverRef;
                    _notYetControledForThisLactation = false;
                    
#ifdef _LOG
//                    std::string diag = "";
//                    ((DairyUdder*)_pHasUdder)->getStrCurrentMastitisDiagnostic(diag);
//                    std::cout << "DairyMammal::getMilkControlInformations, diag = " << diag << ", id = " << getUniqueId() << ", scc = " << controledMilk.SCC << ", lactation stage = " << getLactationStage() << std::endl;
#endif // _LOG

                    // Subclinical ketosis case ?
                    if (useCetodetect)
                    {
                        Health::Ketosis::KetosisStates::KetosisState* pNewState = nullptr;
                        _pHasCurrentKetosisState->considerCetodetectTest(simDate, getDairyFarm()->getRandom(), pNewState); // The ketosis state can change
                        setCurrentKetosisState(pNewState);
                    }
                    
                    // Lameness informations
                    _pHasFoots->isLamenessIll(isDetectedInfectiousLameness, isInfectiousG1Lameness, isInfectiousG2Lameness, isDetectedNonInfectiousLameness, isNonInfectiousG1Lameness, isNonInfectiousG2Lameness);
                }
            }
            else
            {
                _lastSCCWasLessThanRef = false;
            }
#ifdef _LOG
            // Ketosis informations
            ketosisSeverity = _pHasCurrentKetosisState->getSeverity();
            lastCalvingMonth = (FunctionalEnumerations::Global::Month)getLastFarrowingDate().month().as_number();
            // For incidence
            if (_lastKetosisNoteAtMilkControl < ketosisSeverity)
            {
                _lastKetosisNoteAtMilkControl = ketosisSeverity;
                newKetosisCase = true;
            }
            
#endif // _LOG                                                                                
        }
        return isAdult;
    }
    
    void DairyMammal::considerHerdNavigatorTest(const boost::gregorian::date &simDate)
    {
        Health::Ketosis::KetosisStates::KetosisState* pNewState = nullptr;
        _pHasCurrentKetosisState->considerHerdNavigatorTest(simDate, getDairyFarm()->getRandom(), pNewState);
        setCurrentKetosisState(pNewState);
    }

    const std::vector<ExchangeInfoStructures::MilkProductCharacteristic> &DairyMammal::getNormalizedMilkProductionCurve()
    {        
        return (_pNormalizedLactationCurves->find(getLactationRankWithLimit(3))->second);
    }

    float DairyMammal::produceMilkForTheDay(unsigned int day, const boost::gregorian::date &date, ExchangeInfoStructures::MilkProductCharacteristic &currentDriedPeriodEffectToMilk, Lactation::DairyHerdLactationStates::DairyHerdLactationState* &pNewState)
    {
        // Create the theoric production of the day
        ExchangeInfoStructures::MilkProductCharacteristic theoricUdderMp; // theoric udder production of the day
        
        // Calculate the production
        // Individual genetic value
        Genetic::GeneticManagement::getMilkGeneticProductionOfTheDay(getNormalizedMilkProductionCurve(), _hasGeneticValue, theoricUdderMp, day, getLactationRankWithLimit(3));
        float ponderedTodayMilkQuantity = 0.0f;
        // Context modulation
        
        Lactation::LactationManagement::getMilkModulatedProductionOfTheDay( theoricUdderMp,
                                                                            day <= getDairyFarm()->getDayBeforeDeliverMilk(),
                                                                            _deliveredMilkOfTheDay,
                                                                            _discardedMilkOfTheDay,
                                                                            getCurrentReproductionState()->isOestrus(),
                                                                            day,
                                                                            (getDairyHerd())->getMilkSunnyAdjustmentOfTheDay(),
                                                                            getDairyFarm()->getMilkingFrequency(),
                                                                            currentDriedPeriodEffectToMilk,
                                                                            pregnancyIsOngoing(),
                                                                            getDairyUdder(),
                                                                            _pHasFoots,
                                                                            date,
                                                                            getLactationRank(), 
                                                                            getLactationStage(), 
                                                                            ponderedTodayMilkQuantity,
                                                                            getDairyHerd()->getMilkProductionObjectiveFactor(),
                                                                            getDairyFarm()->getRandom()
                                                                            );

        // Note the product
        getDairyHerd()->addMyMilkProductionForTheDay(_deliveredMilkOfTheDay, _discardedMilkOfTheDay);
        
        // Store the production for culling score calculation
        if (_lastScorePeriodMilkQuantity.size() == FunctionalConstants::Population::LAST_PERIOD_DURATION_FOR_SCORE)
        {
            _lastScorePeriodMilkQuantity.erase(_lastScorePeriodMilkQuantity.begin()); // We erase the older one ...
        }
        _lastScorePeriodMilkQuantity.push_back(getMilkQuantityOfTheDay());
                
        // Store the SCC for culling score calculation
        if (_lastScorePeriodSCC.size() == FunctionalConstants::Population::LAST_PERIOD_DURATION_FOR_SCORE)
        {
            _lastScorePeriodSCC.erase(_lastScorePeriodSCC.begin()); // We erase the older one ...
        }
        _lastScorePeriodSCC.push_back(_deliveredMilkOfTheDay.SCC + _discardedMilkOfTheDay.SCC); // ... to push this one
        
#ifdef _LOG

        Results::DayAnimalLogInformations &log = getDataLog();
        log.PLDelivered = _deliveredMilkOfTheDay.quantity;
        log.PLDiscarded = _discardedMilkOfTheDay.quantity;
        log.TB = _deliveredMilkOfTheDay.TB + _discardedMilkOfTheDay.TB;
        log.TP = _deliveredMilkOfTheDay.TP + _discardedMilkOfTheDay.TP;
        log.SCC = _deliveredMilkOfTheDay.SCC + _discardedMilkOfTheDay.SCC;
#endif // _LOG


        // Stop milk product ?
        float minimumQuantity = getDairyHerd()->getMinimumMilkQuantityForRentability();
//                                    _cullingStatus == FunctionalEnumerations::Population::CullingStatus::notToCull ?
//                                    getDairyFarm()->getMinimumMilkQuantityForDriedPeriod() : // Not to cull, so for dryd period
//                                    getDairyFarm()->getMinimumMilkQuantityForCulling(); // To cull, so for quantity before culling
        
        if (ponderedTodayMilkQuantity < minimumQuantity and day > _mMilkQuantityPeakDay[getLactationRankWithLimit(3)])
        {
            _consecutiveDaysUnderMinimumMilkProduction++;
        }
        else
        {
            _consecutiveDaysUnderMinimumMilkProduction = 0;
        }
        
        if (_consecutiveDaysUnderMinimumMilkProduction == FunctionalConstants::Population::DURATION_FOR_MINIMUM_MILK_QUANTITY_FOR_DECISION) // Low production level)
        {
            if (_cullingStatus == FunctionalEnumerations::Population::CullingStatus::notToCull)
            {
                if (pregnancyIsOngoing())
                {
                    //std::cout << getLactationStage() << std::endl;
                    pNewState = new Lactation::DairyHerdLactationStates::DryState(this, date);

                }
                else
                {
                    if (_longNonInfectiousLammenessWhenLastDryoff)
                    {
                        // Very low milk production, not pregnant because off long non infectious lammenes case, so culled today
                        cullingIsDecided(FunctionalEnumerations::Population::CullingStatus::toCullDueToNonInfectiousLameness, true, date);         
                    }
                    else
                    {
                        // Very low milk production, not yet pregnant, so culled today
                        cullingIsDecided(FunctionalEnumerations::Population::CullingStatus::toCullDueToLowMilkProduct, true, date);         
                    }
                }
            }
            else
            {
                // Culling today
                getHerd()->addAnimalToBeCulledToday(this);
            }
        }
        return ponderedTodayMilkQuantity;
    }
    
    void DairyMammal::setMilkLossQuantityDueMastitis(float milkLossQuantityDueMastitis)
    {
        _milkQuantityLossTodayDueToMastitis = milkLossQuantityDueMastitis;
    }

    bool DairyMammal::isToCullForInfertility(const boost::gregorian::date &theDate)
    {
        if (isUndergoingCull())
        {
            // Already to cull
            return false;
        }
        else
        {
            // To cull
            return (     (theDate >= _endInseminationDate or (theDate >= _endFirstInseminationDate and getInseminationRank() == 0))
                     and  not pregnancyIsOngoing());
        }
    }
    
    void DairyMammal::takeMonensinBolus(const boost::gregorian::date &date)
    {
        // Cost
        getDairyHerd()->increasePreventiveKetosisFarmerTreatmentCount();
        getDairyHerd()->increaseScheduledFarmerActivityCount(1);
        getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    date, FunctionalConstants::AccountingModel::MONENSIN_BOLUS_TRANSACTION_TYPE, 
#endif // _LOG                
                                    getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_MONENSIN_BOLUS_COST), getDairyHerd()->getKetosisHealthCostsForUpdate());
        
#ifdef _LOG
//        getDairyFarm()->addManagedHealthCost(getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_MONENSIN_BOLUS_COST)); // add to the annual managed health costs
#endif // _LOG
                
        // Log
#ifdef _LOG
        addMiscellaneousLog(date, "Monensin bolus"); 
#endif // _LOG            
        
        // Effect
        _endEffectDateOfMonensinBolusCare = date + boost::gregorian::date_duration(FunctionalConstants::Health::PROTECT_DURATION_OF_MONENSIN_BOLUS_TRAITMENT);
    }

    void DairyMammal::dryOff(const boost::gregorian::date &date)
    {
        // Record the last lactation production quantity for itself and for the dairy herd
        _lastFullLactationPonderedMilkQuantity = getCurrentLactationState()->getFullLactationPonderedMilkQuantity();
                
#ifdef _LOG
        // Because if not pregnant, it's for a reason (decision for infertility, ...), so dry off has been canceled replaced by low production detection...
        assert(pregnancyIsOngoing());
#endif // _LOG
        
        // Lactation state
        _lastDryOffDate = date;
                
        // Mastitis risk period
        setDryingOffMastitisRiskPeriod(date);
        
        // Give preventive mastitis treatment regarding scc level
        if (_lastControledMilk.SCC >= FunctionalConstants::Health::SCC_LEVEL_FOR_PREVENTIVE_DRYOFF_TREATMENT)
        {
            takePreventiveDryOffMastitisTreatmentType(
#ifdef _LOG
                                                                date 
#endif // _LOG
                                                               );
            getDairyUdder()->notifyMastitisTreatmentToAllDairyQuarters(date, getDairyFarm()->getRandom()); // Make dairy quarters take into account off preventive effects
        }
        
        // Ketosis risk period
        exitFromKetosisRiskPeriod(date);
            
        // Lameness consequences
        assert (not _longNonInfectiousLammenessWhenLastDryoff); // No dry off now if long non infectious lameness occured previous one dryoff
        if (hasCurrentTooLongLammenessCaseForEngageInsemination(date, FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType))
        {
            _longNonInfectiousLammenessWhenLastDryoff = true;
        }
        
        // Monensin bolus use ?
        if (getDairyFarm()->useMonensinBolusPreventiveTreatment())
        {
            boost::gregorian::date dateToTake = _pHasCurrentReproductionState->getPredictedEndPregnancyDate() - boost::gregorian::date_duration(FunctionalConstants::Health::DELAY_BEFORE_CALVING_FOR_MONENSIN_BOLUS_USE);
            if (dateToTake <= date)
            {
                // Take now
                takeMonensinBolus(date);
            }
            else
            {
                // Plan
                _dateToTakeMonensinBolus = dateToTake;
            }
        }

        // Population
        Population::PopulationManagement::affectDairyCowToBatch(this, FunctionalEnumerations::Population::BatchType::driedCowsAndPregnantHeifers, date);
        
    }
    
    float DairyMammal::getLastFullLactationPonderedMilkQuantity()
    {
        if (_lastFullLactationPonderedMilkQuantity == 0.0f)
        {
            // If I have not, I give the one of my mother
            return getDamLastFullLactationMilkQuantity();;            
        }
        else
        {
            return _lastFullLactationPonderedMilkQuantity;
        }
    }
    
    unsigned int DairyMammal::getBreedingDelayAfterCalvingDecision()
    {
        return getDairyFarm()->getBreedingDelayAfterCalvingDecision();
    }
    
    void DairyMammal::setLastBeginLactationPonderedMilkQuantity(float ponderedQuantity, const boost::gregorian::date &date)
    {
        
        if (ponderedQuantity < getDairyHerd()->getMeanPonderedMilkQuantityBetweenCalvingAndBreedDate(date) * getDairyFarm()->getMinMilkProductionHerdRatioForEndInseminationDecision()
                and not getBreedInheritance()->isMilkCrossed()) // We protect the milk crossed cows
        {
            getDairyHerd()->increaseAnnualNotBredBecauseOfLowMilkProductCount();
            cullingIsDecided(FunctionalEnumerations::Population::CullingStatus::toCullDueToLowMilkProduct, false, date);
        }
        getDairyHerd()->addPonderedMilkQuantityBetweenCalvingAndBreedDate(ponderedQuantity, date);
    }

    void DairyMammal::goInLactatingCowsBatch(const boost::gregorian::date &simDate)
    {       
        Population::PopulationManagement::affectDairyCowToBatch(this, FunctionalEnumerations::Population::BatchType::lactatingCows, simDate);
    }
    
    const boost::gregorian::date &DairyMammal::getLastDryingDate()
    {
        return _lastDryOffDate;
    }
    
    float DairyMammal::getLastMeanPeriodSCC()
    {
        assert(_lastScorePeriodSCC.size() == FunctionalConstants::Population::LAST_PERIOD_DURATION_FOR_SCORE);
        float res = std::accumulate(_lastScorePeriodSCC.begin(), _lastScorePeriodSCC.end(), 0.0f) / FunctionalConstants::Population::LAST_PERIOD_DURATION_FOR_SCORE;
        return res ;
    }
    
    float DairyMammal::getLastMeanPeriodMilkQuantity()
    {
        assert(_lastScorePeriodMilkQuantity.size() == FunctionalConstants::Population::LAST_PERIOD_DURATION_FOR_SCORE);
        float res = std::accumulate(_lastScorePeriodMilkQuantity.begin(), _lastScorePeriodMilkQuantity.end(), 0.0f) / FunctionalConstants::Population::LAST_PERIOD_DURATION_FOR_SCORE;
        return res ;
    }
    
    void DairyMammal::consumeUnweanedCalfMilk()
    {
        if (isToBeFed())
        {
            getDairyFarm()->useUnweanedCalfMilk();
#ifdef _LOG
            getDataLog().milkRation = getDairyFarm()->getCalfMilkQuantity();
#endif // _LOG                
        } 
    }
    
    void DairyMammal::consumeConcentrateUnweanedFemaleCalf()
    {
        getDairyFarm()->useUnweanedFemaleCalfConcentrateFeed(_concentrateRationOfTheDay);
    }
    
    void DairyMammal::consumeForageConcentrateAndMineralVitaminWeanedFemaleCalf()
    {
        getDairyFarm()->useWeanedFemaleCalfForageConcentrateAndMineralVitaminFeed(_forageRationOfTheDay, _concentrateRationOfTheDay, _mineralVitaminRationOfTheDay);
#ifdef _LOG
        updateLogFeedConsumption();
#endif // _LOG
    }

    void DairyMammal::consumeForageConcentrateAndMineralVitaminYoungHeifer(FunctionalEnumerations::Population::Location location)
    {
        getDairyFarm()->useYoungHeiferForageConcentrateAndMineralVitaminFeed(_forageRationOfTheDay, _concentrateRationOfTheDay, _mineralVitaminRationOfTheDay, location);
#ifdef _LOG
        updateLogFeedConsumption();
#endif // _LOG
    }
    
    void DairyMammal::consumeForageConcentrateAndMineralVitaminOldHeifer(FunctionalEnumerations::Population::Location location)
    {
        getDairyFarm()->useOldHeiferForageConcentrateAndMineralVitaminFeed(_forageRationOfTheDay, _concentrateRationOfTheDay, _mineralVitaminRationOfTheDay, location);
#ifdef _LOG
        updateLogFeedConsumption();
#endif // _LOG
    }
    
    void DairyMammal::consumeForageConcentrateAndMineralVitaminBredHeifer(FunctionalEnumerations::Population::Location location)
    {
        getDairyFarm()->useBredHeiferForageConcentrateAndMineralVitaminFeed(_forageRationOfTheDay, _concentrateRationOfTheDay, _mineralVitaminRationOfTheDay, location);
#ifdef _LOG
        updateLogFeedConsumption();
#endif // _LOG
    }
    
    void DairyMammal::consumeForageConcentrateAndMineralVitaminDriedCow(FunctionalEnumerations::Population::Location location)
    {
        getDairyFarm()->useDriedCowForageConcentrateAndMineralVitaminFeed(_forageRationOfTheDay, _concentrateRationOfTheDay, _mineralVitaminRationOfTheDay, location);
#ifdef _LOG
        updateLogFeedConsumption();
#endif // _LOG
    }
    
    void DairyMammal::consumeForageConcentrateAndMineralVitaminPeriParturientCow(FunctionalEnumerations::Population::Location location)
    {
        getDairyFarm()->usePeriParturientCowForageConcentrateAndMineralVitaminFeed(_forageRationOfTheDay, _concentrateRationOfTheDay, _mineralVitaminRationOfTheDay, isNulliparous(), location);
#ifdef _LOG
       updateLogFeedConsumption();
#endif  // _LOG
    }
    
    void DairyMammal::consumeForageConcentrateAndMineralVitaminLactatingCow(FunctionalEnumerations::Population::Location location)
    {
        // Lower consumption due to milk quantity loss due to diseases ?
        float feedQuantityNotConsumedDueToMilkQuantityLossDueToDisease =  _milkQuantityLossTodayDueToMastitis / FunctionalConstants::Health::MASTITIS_MILK_LOSS_QUANTITY_FOR_ONE_KG_FEED
                                                                        + _milkQuantityLossTodayDueToKetosis  / FunctionalConstants::Health::KETOSIS_MILK_LOSS_QUANTITY_FOR_ONE_KG_FEED
                                                                        + _milkQuantityLossTodayDueToLameness / FunctionalConstants::Health::LAMENESS_MILK_LOSS_QUANTITY_FOR_ONE_KG_FEED;

        getDairyFarm()->useLactatingCowForageConcentrateAndMineralVitaminFeed(_forageRationOfTheDay, _concentrateRationOfTheDay, _mineralVitaminRationOfTheDay, isPrimiparous(), getMilkQuantityOfTheDay(), location, feedQuantityNotConsumedDueToMilkQuantityLossDueToDisease);

#ifdef _LOG
        updateLogFeedConsumption();
#endif // _LOG
    }
    
#ifdef _LOG
    void DairyMammal::updateLogFeedConsumption()
    {
        Results::DayAnimalLogInformations &log = getDataLog();
        log.forageRation = _forageRationOfTheDay.quantity;
        log.concentrateRation = _concentrateRationOfTheDay.quantity;
        log.mineralVitaminRation = _mineralVitaminRationOfTheDay;
    }
#endif // _LOG

    bool DairyMammal::isPrimiparous()
    {
        return getParity() == FunctionalEnumerations::Reproduction::DairyCowParity::primipare; 
    }
    
    bool DairyMammal::isNulliparous()
    {
        return getLactationRank() == 0;
    }
    
    FunctionalEnumerations::Population::Location DairyMammal::getCurrentLocation()
    {
        return getBatch()->getCurrentLocation(); 
    }
    
    bool DairyMammal::needToStudyDairyUdderHealth()
    {
        // For the moment, just mastitis state justify the health actions
        return _pHasCurrentMastitisRiskPeriodState->needToStudyMastitisOccurenceProbability();
    }
        
    void DairyMammal::updateDayMastitisOccurenceProbability(std::map<FunctionalEnumerations::Health::BacteriumType, double> &mastitisOccurenceProbabilityToUpdate, const boost::gregorian::date &date)
    {
        
        if (_pHasCurrentMastitisRiskPeriodState->needToStudyMastitisOccurenceProbability())
        {
            ExchangeInfoStructures::MilkProductCharacteristic milkOfTheDay = _deliveredMilkOfTheDay + _discardedMilkOfTheDay;

            for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++)
            {
                FunctionalEnumerations::Health::BacteriumType bacterium = (FunctionalEnumerations::Health::BacteriumType)iBact;

                mastitisOccurenceProbabilityToUpdate[bacterium] =   Health::HealthManagement::getMastitisOccurenceProbability(   
                                                                        bacterium,                                                                  // Concerned bacterium
                                                                        date,                                                                       // Date
                                                                        getCurrentMastitisRiskPeriodState(),                                        // Current risk period
                                                                        getDairyFarm()->getSaisonalityFactor(date, bacterium),               // Saisonality factor
                                                                        getDairyFarm()->getIncidencePart(bacterium),
                                                                        getBatch()->getMastitisHistory(bacterium, date),                            // Batch prevalence
                                                                        lactationIsOngoing(),                                                       // Is currently in lactation
                                                                        milkOfTheDay.quantity,                                                      // Product milk quantity of the day
                                                                        milkOfTheDay.SCC,                                                           // SCC of the day
                                                                        getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL).getCorrectedPerformance(), // MACL genetic factor
                                                                        _pHasCurrentMastitisRiskPeriodState->getMastitisLactationRankModulation(),  // Lactation rank for modulation
                                                                        getMilkPotentialFactor(),                                                   // Potential milk product
                                                                        getDairyUdder()->isCurrentlyInfectedByMastitisBacterium(bacterium))         // Udder already infected
                                                                * _pHasCurrentKetosisState->getMastitisRisk() // Additional mastitis risk due to potential ketosis occurency 
                                                                * _pHasFoots->getMastitisRiskDueToLameness() // Additional mastitis risk due to potential lameness occurency 
                                                                ;
            }
        }
    }
    
    FunctionalEnumerations::Health::MastitisSeverity DairyMammal::getRandomMastitisSeverity(FunctionalEnumerations::Health::BacteriumType bacterium)
    {
        return Health::HealthManagement::getRandomMastitisSeverity(bacterium, _pHasCurrentMastitisRiskPeriodState->getMastitisSeverityDegree(), getDairyFarm()->getRandom());            
    }
    
    // Set the milk potential factor for the life of the cow
    void DairyMammal::initMilkPotentialFactor()
    {
        _milkPotentialFactor = Tools::getProportionalFactor(_hasGeneticValue.getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait).getCorrectedPerformance(), FunctionalConstants::Health::MASTITIS_MILK_POTENTIAL_MODULATION);
    }
    
    float DairyMammal::getMilkPotentialFactor()
    {
        return _milkPotentialFactor;
    }

    void DairyMammal::notifyNewMastitisOccurence(FunctionalEnumerations::Health::BacteriumType bacterium, const boost::gregorian::date &date, bool newCase, bool newSubclinical, bool newClinical)
    {
        // add the new mastitis occurence for the batch
        if (newCase)
        {
            getBatch()->addMastitisOccurence(bacterium, date);
        }
        
        if (newSubclinical)
        {
            getHerd()->notifyNewSubclinicalMastitis();
        }
        else if (newClinical)
        {
            getHerd()->notifyNewClinicalMastitis(_alreadyHadClinicalMastitis);
            _alreadyHadClinicalMastitis = true;
        }
    }
    
    ExchangeInfoStructures::Parameters::TreatmentTypeParameters &DairyMammal::takePreventiveDryOffMastitisTreatmentType(
#ifdef _LOG
                                                                                                                const boost::gregorian::date &date 
#endif // _LOG
                                                                                                                ) 
    {
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &treatment = getDairyFarm()->givePreventiveDryOffMastitisTreatmentType(
#ifdef _LOG
                                                            date 
#endif // _LOG
                                                            );
#ifdef _LOG
        // Treatment log
            addMiscellaneousLog(date, treatment.name); 
#endif // _LOG            
        return treatment;
    }

    ExchangeInfoStructures::Parameters::TreatmentTypeParameters &DairyMammal::takeCurrentMastitisTreatmentType(
#ifdef _LOG
                                                                                                                const boost::gregorian::date &date, 
#endif // _LOG
                                                                                                                FunctionalEnumerations::Health::BacteriumType bacterium,
                                                                                                                FunctionalEnumerations::Health::MastitisSeverity severity) 
    {
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &treatment = getDairyFarm()->giveMastitisTreatment(
#ifdef _LOG
                                                            date, 
#endif // _LOG
                                                            getCurrentMastitisRiskPeriodState()->getMastitisRiskPeriodType(), bacterium, severity);
#ifdef _LOG
        // Treatment log
            addMiscellaneousLog(date, treatment.name); 
#endif // _LOG            
        return treatment;
    }

    bool DairyMammal::G1SeverityMastitisIsDetected()
    {
        return getDairyFarm()->G1SeverityMastitisIsDetected();
    }
         
   float DairyMammal::getCurrentMastitisPreventionFactor()
   {
        return getDairyFarm()->getCurrentMastitisPreventionFactor();
       
   }
        
   ExchangeInfoStructures::Parameters::TreatmentTypeParameters &DairyMammal::getMastitisTreatmentType(FunctionalEnumerations::Health::BacteriumType bacterium)
   {
       return getDairyFarm()->getMastitisTreatmentType(getCurrentMastitisRiskPeriodState()->getMastitisRiskPeriodType(), bacterium);
   }
   
   bool DairyMammal::isCurrentlyMastitisInfected(bool onlyClinical)
   {
       return getDairyUdder()->isCurrentlyMastitisInfected(onlyClinical);
   }

#ifdef _LOG
    unsigned int DairyMammal::getDayOfTheMastitisRiskReferenceCycle(const boost::gregorian::date &date)
    {
        return _pHasCurrentMastitisRiskPeriodState->getDayOfTheMastitisRiskReferenceCycle(date);
    }
#endif // _LOG
        
    float DairyMammal::getKetosisRisk(const boost::gregorian::date &beginDate)
    {
        return Health::HealthManagement::getModulatedCowKetosisIncidenceOfTheDay(getLactationStage(), getLactationRank(), _crossedLactationRankKetosisIncidence, getDairyFarm()->getCurrentKetosisPreventionFactor(), _lastCalvingIntervalInMonth, beginDate <= _endEffectDateOfMonensinBolusCare, getDairyUdder()->getClinicalMastitisSinceLactationBegin(), _pHasFoots->hasCurrentLamenesCases());
    }
    
    float DairyMammal::getBasisKetosisIncidence()
    {
        return Health::HealthManagement::getLactationRankBasisIncidence(getLactationRank(), _crossedLactationRankKetosisIncidence);
    }
    
    unsigned int DairyMammal::getSubclinicalKetosisDuration()
    {
        return getDairyFarm()->getSubclinicalKetosisDuration();
    }
    
    bool DairyMammal::willHaveKetosisRecurrency()
    {
        if (_pHasCurrentKetosisState->recurrencyIsPossible())
        {
            return getDairyFarm()->getRandom().ran_bernoulli(FunctionalConstants::Health::KETOSIS_RECURRENCY_PROBABILITY * getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait).getCorrectedPerformance());
        }
        else
            return false;
        }

    unsigned int DairyMammal::getLactationRankWithLimitForKetosis(unsigned int limit)
    {
        return getLactationRankWithLimit(limit);
    }
    
    unsigned int DairyMammal::getLactationRankWithLimit(unsigned int limit)
    {
        unsigned int lactationRank = getLactationRank();
        if (lactationRank > limit)
        {
            lactationRank = limit;
        }
        return lactationRank;
    }
        
    FunctionalEnumerations::Reproduction::DairyCowParity DairyMammal::getParityForKetosis()
    {
        return getParity();   
    }
      
    void DairyMammal::setCurrentKetosisState(Health::Ketosis::KetosisStates::KetosisState* pState)
    {
        if (pState != _pHasCurrentKetosisState and pState != nullptr)
        {
            if (_pHasCurrentKetosisState != nullptr)
            {
               delete _pHasCurrentKetosisState;
            }
            _pHasCurrentKetosisState = pState;
        }
    }

    void DairyMammal::enterInKetosisRiskPeriod(const boost::gregorian::date &date)
    {
        // Is now sensible to ketosis
        Health::Ketosis::KetosisStates::KetosisState* pNewState = nullptr;
        _pHasCurrentKetosisState->enterInKetosisRiskPeriod(date, pNewState);
        setCurrentKetosisState(pNewState);
        
        // And risk depends on a potential comming mastitis occurency, we have to detect it
        getDairyUdder()->setClinicalMastitisSinceLactationBegin(false);

    }
    
    void DairyMammal::exitFromKetosisRiskPeriod(const boost::gregorian::date &date)
    {
        // Is now unsensible to ketosis
        Health::Ketosis::KetosisStates::KetosisState* pNewState = nullptr;
        _pHasCurrentKetosisState->exitFromKetosisRiskPeriod(date, pNewState);
        setCurrentKetosisState(pNewState);
    }

    void DairyMammal::becomeLamenessSensitive(const boost::gregorian::date &date)
    {
        // Is now sensible to lameness
        _pHasFoots->becomeLamenessSensitive(date);
    }
 
    void DairyMammal::diesNowBecauseOfLameness(
#ifdef _LOG                   
            const boost::gregorian::date &date, 
#endif // _LOG            
                                            FunctionalEnumerations::Population::DeathReason deathReason)
    {
        dies(
#ifdef _LOG                   
            date, 
#endif // _LOG            
            deathReason);
    }
    
    bool DairyMammal::isLame(bool &infectious, bool &nonInfectious)
    {
        infectious = _pHasFoots->isInfectiousLamenessIll();
        nonInfectious = _pHasFoots->isNonInfectiousLamenessIll();
        return infectious or nonInfectious;
    }
    
    bool DairyMammal::hasCurrentTooLongLammenessCaseForEngageInsemination(const boost::gregorian::date &date, FunctionalEnumerations::Health::LamenessInfectiousStateType list)
    {
        unsigned int currentLamenessDuration = _pHasFoots->getCurrentDurationLamenessIll(date, list);
        return currentLamenessDuration >= FunctionalConstants::Health::LAMENESS_DURATION_WHEN_DRYING_TO_NOT_ENGAGE_INSEMINATION;
    }

    void DairyMammal::addLamenessHealthTransaction(
#ifdef _LOG                
                                                    const boost::gregorian::date &transactionDate, const std::string &TYPE, 
#endif // _LOG                
                                                    const std::string &KEY, float &annualDataToSet,
                                                    float amontFactor)
    {
        getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    transactionDate, TYPE, 
#endif // _LOG                
                                    getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(KEY) * amontFactor, annualDataToSet);
#ifdef _LOG                   
//        getDairyFarm()->addManagedHealthCost(getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(KEY)); // add to the annual managed health costs
        addMiscellaneousLog(transactionDate, TYPE); 
#endif // _LOG            
    }

    bool DairyMammal::hasCurrentlyEfficientFootbath(const boost::gregorian::date &date, float &dayRatio)
    {
        bool has = getBatch()->hasEfficientFootBathToday(date);
        if (has)
        {
            dayRatio = 1.0f / ((float)getDairyFarm()->getFootBathProtectionDurationForLamenessInDays());           
        }
        return has;
    }
   
    void DairyMammal::notifyForLamenessSporadicDetectionOrVerificationToday(
#ifdef _LOG                
                                                    const boost::gregorian::date &date
#endif // _LOG                
                                                    )
    {
#ifdef _LOG
        // Treatment log
            addMiscellaneousLog(date, "Lameness sporadic detection or verification"); 
#endif // _LOG            

        getDairyHerd()->addAnimalInGroup(this, getDairyFarm()->getToCareDueToSporadicLamenessDetectionOrVerificationGroup());
    }
   
    void DairyMammal::toCareIndividualTodayForDetectedLamenessCase()
    {
        _toCareIndividualTodayDueToSporadicLamenessDetection = true;
    }

    void DairyMammal::careIndividualTodayForDetectedLamenessCase(const boost::gregorian::date &careDate)
    {
        _pHasFoots->careIndividualTodayForLameness(careDate); // The care,
        getDairyHerd()->increaseUnscheduledCowVetCareCount(); // The vet action on the cow
        getDairyHerd()->increaseCurativeClinicalLamenessVetTreatmentCount(); // The treatment count
        _toCareIndividualTodayDueToSporadicLamenessDetection = false;
    }
    
    bool DairyMammal::hadRecentTrimming(const boost::gregorian::date &date)
    {
        return _pHasFoots->hadRecentTrimming(date);
    }

    void DairyMammal::toTrimCollectiveTodayForLamenessPrevention()
    {
        _toTrimCollectiveTodayForLamenessPrevention = true;
    }

    void DairyMammal::trimCollectiveTodayForLamenessPrevention(const boost::gregorian::date &trimDate)
    {
        getDairyHerd()->increasePreventiveLamenessVetTreatmentCount();
        _pHasFoots->trimCollectiveTodayForLamenessPrevention(trimDate);
        _toTrimCollectiveTodayForLamenessPrevention = false;
    }


    FunctionalEnumerations::Health::LamenessDetectionMode DairyMammal::getLamenessDetectionMode()
    {
        return getDairyFarm()->getLamenessDetectionMode();
    }

    float DairyMammal::getLamenessRisk(const boost::gregorian::date &simDate, FunctionalEnumerations::Health::LamenessInfectiousStateType infectiousType)
    {
        float dayIncidence = 0.0f;
        
        if (isMilking())
        {
            // The cow is in lactation period
            assert (getLactationStage() >= 0);
            
             // Incidence for the lactation period of the day = lactation stage curve application  + All period day part
            float lactationIncidence = Health::HealthManagement::getLactationRankBasisIncidence(getLactationRank(), _mSpecificLactationPeriodDependingRankLamenessIncidence.find(infectiousType)->second);
            dayIncidence = lactationIncidence * Health::HealthManagement::getLactationLamenessCurveFactor(getLactationStage())
                           + (Health::HealthManagement::getLactationRankBasisIncidence(getLactationRank(), _mSpecificLactationDaysAllPeriodDependingRankLamenessIncidenceByDay.find(infectiousType)->second));
        }
        else
        {
            // The cow is not in lactation period
            // Incidence for the lactation period of the day = All period day part
            dayIncidence = (Health::HealthManagement::getLactationRankBasisIncidence(getLactationRank(), _mSpecificDryDaysAllPeriodDependingRankLamenessIncidenceByDay.find(infectiousType)->second));
        }
        return Health::HealthManagement::getModulatedCowLamenessIncidenceOfTheDay(dayIncidence, getDairyUdder()->isCurrentlyG1ToG3MastitisInfected(), hasCurrentlyG1ToG2KetosisCase(), infectiousType, getBatch()->getCurrentLocation(), getBatch()->getDayCountSinceLocation(simDate), getBatch()->getInfectiousLamenessPrevalence());
    }
    
    bool DairyMammal::needToTrimForLamenessPreventionWhenLactation()
    {
        return getDairyFarm()->getTrimmingOption() == FunctionalEnumerations::Health::LamenessPreventiveTrimmingOption::preventiveTrimmingSmallGroup;
    }
       
    void DairyMammal::toTrimForLamenessPrevention(const boost::gregorian::date &date)
    {
        if (not hadRecentTrimming(date))
        {
            getDairyHerd()->addAnimalInGroup(this, getDairyFarm()->getToTrimForLamenessPreventionGroup());
        }
    }

    void DairyMammal::resetDataDependingLastLactation()
    {
        // Mastitis
        _alreadyHadClinicalMastitis = false;
        
        // Ketosis
        _alreadyHadSubclinicalKetosis = false;
        _alreadyHadClinicalKetosis = false;
        _ketosisCaseDuringPreviousLactation = _G1ketosisCumulateDurationInTheLactation > 0 or _G2ketosisCumulateDurationInTheLactation > 0;
        _G1ketosisCumulateDurationInTheLactation = 0;
        _G2ketosisCumulateDurationInTheLactation = 0;
        _G1ketosisSuccessfulTreatmentDuringTheLactation = false;
#ifdef _LOG
        _lastKetosisNoteAtMilkControl = FunctionalEnumerations::Health::KetosisSeverity::G0Ketosis;
#endif // _LOG
    }
    
    bool DairyMammal::isInKetosisRiskPeriod()
    {
        return getLactationStage() > 0 and (unsigned int)getLactationStage() < FunctionalConstants::Health::KETOSIS_RISK_PERIOD_DURATION_SINCE_BEGIN_LACTATION;
    }
    
    unsigned int DairyMammal::getDurationUntilEndKetosisRisk()
    {
        if (getLactationStage() >= 0)
        {
            int duration = FunctionalConstants::Health::KETOSIS_RISK_PERIOD_DURATION_SINCE_BEGIN_LACTATION - getLactationStage();
            if (duration > 0)
            {
                return (unsigned int) duration;
            }
        }
        return 0;
    }
            
    bool DairyMammal::useHerdNavigator()
    {
        return getDairyFarm()->useHerdNavigator();
    }
    
    void DairyMammal::notifyNewKetosisOccurence(const boost::gregorian::date &date, FunctionalEnumerations::Health::KetosisSeverity severity)
    {
        if (severity == FunctionalEnumerations::Health::KetosisSeverity::G2Ketosis)
        {
            getHerd()->notifyNewClinicalKetosis(_alreadyHadClinicalKetosis, date, getLastFarrowingDate());
            _alreadyHadClinicalKetosis = true;
        }
        else
        {
            getHerd()->notifyNewSubclinicalKetosis(_alreadyHadSubclinicalKetosis, date, getLastFarrowingDate());
            _alreadyHadSubclinicalKetosis = true;
        }

        // For ketosis effects
        _pHasCurrentReproductionState->considerKetosisCaseForDuration(
#ifdef _LOG
                                                    date 
#endif // _LOG
                                                    );
    }

    void DairyMammal::increaseG1ketosisCumulateDurationInTheLactation()
    {
        _G1ketosisCumulateDurationInTheLactation++;
    }
    
    void DairyMammal::increaseG2ketosisCumulateDurationInTheLactation()
    {
        _G2ketosisCumulateDurationInTheLactation++;
    }
    
    void DairyMammal::considereG1KetosisSuccessfulTreatmentDuringTheLactation()
    {
        _G1ketosisSuccessfulTreatmentDuringTheLactation = true;
    }
    
    void DairyMammal::getKetosisEffectOnMilkProduction(const boost::gregorian::date &date, ExchangeInfoStructures::MilkProductCharacteristic &mc)
    {
        // Ketosis milk quantity effect to take into account ?
        if (date < _endKetosisMilkQuantityEffect)
        {
            // There was a ketosis effet to take into account
            float milkQuantityBefore = mc.quantity;
            mc.quantity *= _ketosisMilkQuantityFactorEffect;
            _milkQuantityLossTodayDueToKetosis = milkQuantityBefore - mc.quantity;        
        }
        
        // Potential ketosis effect on TB and/or TP
        float TBKetosisDelta, TPKetosisDelta;
        _pHasCurrentKetosisState->getKetosisTBTPEffect(TBKetosisDelta, TPKetosisDelta);
        mc.TB += TBKetosisDelta;
        mc.TP += TPKetosisDelta;
    }
    
    boost::gregorian::date &DairyMammal::getEndKetosisMilkQuantityEffect()
    {
        return _endKetosisMilkQuantityEffect;
    }

    float &DairyMammal::getKetosisMilkQuantityFactorEffect()
    {
        return _ketosisMilkQuantityFactorEffect;
    }

    bool DairyMammal::hadKetosisCaseDuringPreviousLactation()
    {
        return _ketosisCaseDuringPreviousLactation;
    }
    
    bool DairyMammal::hasCurrentlyG1ToG2KetosisCase()
    {
        return _pHasCurrentKetosisState->isCurrentlyG1ToG2KetosisCase();
    }
    
#ifdef _LOG
    unsigned int DairyMammal::getIdForKetosis()
    {
        return getUniqueId();
    }
#endif // _LOG            
    void DairyMammal::diesNowBecauseOfKetosis(
#ifdef _LOG                   
            const boost::gregorian::date &date 
#endif // _LOG            
                                            )
    {
        dies(
#ifdef _LOG                   
            date,
#endif // _LOG            
            FunctionalEnumerations::Population::DeathReason::ketosisDeathReason);
    }
    
    ExchangeInfoStructures::Parameters::TreatmentTypeParameters &DairyMammal::takeKetosisTreatment(const boost::gregorian::date &date, FunctionalEnumerations::Health::KetosisSeverity severity, bool &success
#ifdef _LOG
                                                                                                    , std::string origin
#endif // _LOG            
                                                                                                    )
    {
        // Treatment cost
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &ct = getDairyFarm()->giveKetosisTreatment(
#ifdef _LOG
                                                                                                                date,
#endif // _LOG        
                                                                                                                severity);
        
        // Treatment log
#ifdef _LOG
        addMiscellaneousLog(date, ct.name + origin); 
#endif // _LOG            
        const boost::gregorian::date &milkWaitingDate = date + boost::gregorian::days(ct.milkWaitingTime + 1); // Milk to be discarded
        getDairyUdder()->updateEndMilkWaitingDate(milkWaitingDate);
        
        // Will be successful ?
        success = getDairyFarm()->getRandom().ran_bernoulli(ct.successProbabilityAllCasesOrFirst);

        return ct;
    }

    // Static initializations
    std::map<unsigned int, FunctionalEnumerations::Population::MortalityRiskAge> DairyMammal::s_ageToMortalityRiskAgeMatrix; // from age to MortalityRiskAge matrix;
    
} /* End of namespace DataStructures */