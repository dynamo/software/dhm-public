#ifndef DataStructures_DairyMammal_h
#define DataStructures_DairyMammal_h

// standard
#include <string>
#include <vector>

// project
#include "Mammal.h"
#include "../ResultStructures/DairyFarmResult.h"
//#include "../Reproduction/DairyHerdReproductionStates/I_DairyHerdReproductionAnimal.h"
//#include "../Lactation/DairyHerdLactationStates/I_DairyHerdLactationAnimal.h"
#include "../Genetic/I_GeneticDairyCow.h"
//#include "../Health/Diseases/Mastitis/DairyCowMastitisRiskPeriodStates/I_MastitisRiskPeriodDairyCow.h"
//#include "../Health/Diseases/Ketosis/KetosisStates/I_KetosisProneDairyCow.h"
#include "../ExchangeInfoStructures/MilkProductCharacteristic.h"
#include "../ExchangeInfoStructures/MixtureCharacteristicParameters.h"
#include "../ExchangeInfoStructures/TreatmentTypeParameters.h"

namespace Tools
{
    class Random;
}
namespace Health
{
    namespace Mastitis
    {
        namespace RiskPeriodStates
        {
            class DairyCowMastitisRiskPeriodState;
        }
    }
    
    namespace Ketosis
    {
        namespace KetosisStates
        {
            class KetosisState;
        }
    }
}

namespace Reproduction
{
    namespace DairyHerdReproductionStates
    {
        class OvulationState;
        class DairyHerdReproductionState;
    }
}
namespace Lactation
{
    namespace DairyHerdLactationStates
    {
        class DairyHerdLactationState;
    }
}
namespace DataStructures
{    
    class DairyHerd;
    class DairyUdder;

    /// Mammal bred for the ability to produce large quantities of milk
    class DairyMammal :    public Mammal
//                                    , public Reproduction::DairyHerdReproductionStates::I_DairyHerdReproductionAnimal
//                                    , public Lactation::DairyHerdLactationStates::I_DairyHerdLactationAnimal
//                                    , public Health::Mastitis::RiskPeriodStates::I_MastitisRiskPeriodDairyCow
//                                    , public Health::Ketosis::KetosisStates::I_KetosisProneDairyCow
                                    , public Genetic::I_GeneticDairyCow
    {
        
    private:
        
        
        // Life
        // ----
        static std::map<unsigned int, FunctionalEnumerations::Population::MortalityRiskAge> s_ageToMortalityRiskAgeMatrix; // from age to MortalityRiskAge matrix;        
//        std::map<FunctionalEnumerations::Population::MortalityRiskAge, float> _hasSpecificMortalityRisk;   // eventual specific proba distrib of mortality risk
//        const std::map<FunctionalEnumerations::Population::MortalityRiskAge, float> *_pHasMortalityRisk; // reference to the (eventual specific overwhise breed) proba distrib of mortality risk tu use
        std::map<FunctionalEnumerations::Population::MortalityRiskAge, float> _hasMortalityRisk;
        // Reproduction
        // ------------
        Reproduction::DairyHerdReproductionStates::DairyHerdReproductionState* _pHasCurrentReproductionState = nullptr;
        std::map<FunctionalEnumerations::Reproduction::DairyCowParity, std::vector<float>> _hasSpecificPostPartumOvulationDuration;   // eventual specific duration, only for a breed multiplicity;
        const std::map<FunctionalEnumerations::Reproduction::DairyCowParity, std::vector<float>> *_pPostPartumOvulationDuration; // reference to the (eventual specific overwhise breed) duration to use
        boost::gregorian::date _breedingDate;
        boost::gregorian::date _endFirstInseminationDate;
        boost::gregorian::date _endInseminationDate;
        unsigned int _lastCalvingIntervalInMonth = 0;
        unsigned int _consecutiveUnsucessfulInseminationCount = 0;
        boost::gregorian::date _lastOestrusDetectionDate;
        Reproduction::DairyHerdReproductionStates::OvulationState* _pFuturForcedOvulationState = nullptr;
        
        // Production
        // ----------
        Lactation::DairyHerdLactationStates::DairyHerdLactationState* _pHasCurrentLactationState = nullptr;
        unsigned int _lactationRank = 0;
        int _lactationStage = -1;
        std::map<unsigned int, std::vector<ExchangeInfoStructures::MilkProductCharacteristic>> _hasSpecificLactationCurves;  // eventual specific curves, only for a breed multiplicity;
        const std::map<unsigned int, std::vector<ExchangeInfoStructures::MilkProductCharacteristic>> *_pNormalizedLactationCurves; // reference to the (eventual specific overwhise breed) normalized curves to use
        std::map<unsigned int, float> _mMilkQuantityPeakYield; // Milk quantity peak yield
        std::map<unsigned int, unsigned int> _mMilkQuantityPeakDay; // Milk quantity peak yield
        ExchangeInfoStructures::MilkProductCharacteristic _deliveredMilkOfTheDay;
        ExchangeInfoStructures::MilkProductCharacteristic _discardedMilkOfTheDay;
        ExchangeInfoStructures::MilkProductCharacteristic _lastControledMilk;
        float _milkQuantityLossTodayDueToMastitis = 0.0f;
        float _milkQuantityLossTodayDueToKetosis = 0.0f;
        float _milkQuantityLossTodayDueToLameness = 0.0f;
        boost::gregorian::date _lastDryOffDate;
        bool _lastSCCWasLessThanRef = false; // true only if was milking and last sensored SCC in milk control > ref
        float _lastFullLactationPonderedMilkQuantity = 0.0f; // recording pondered produced milk of the last full lactation
        std::vector<float> _lastScorePeriodSCC;
        std::vector<float> _lastScorePeriodMilkQuantity;
        unsigned int _consecutiveDaysUnderMinimumMilkProduction = 0;
        bool _lastSCCControlWasOverRef = false;
        bool _beforeLastSCCControlWasOverRef = false;
        bool _notYetControledForThisLactation = true;
        
        // Health
        // ------
        // Mastitis
        Health::Mastitis::RiskPeriodStates::DairyCowMastitisRiskPeriodState* _pHasCurrentMastitisRiskPeriodState = nullptr;
        float _milkPotentialFactor;
        // Ketosis
        Health::Ketosis::KetosisStates::KetosisState* _pHasCurrentKetosisState = nullptr;
        bool _ketosisTestSinceLastCalvingDone = false;
        boost::gregorian::date _dateToTakeMonensinBolus = boost::gregorian::date(boost::gregorian::pos_infin);
        boost::gregorian::date _endEffectDateOfMonensinBolusCare = boost::gregorian::date(boost::gregorian::neg_infin);
        std::map<unsigned int, float> _crossedLactationRankKetosisIncidence;
        unsigned int _G1ketosisCumulateDurationInTheLactation = 0;
        unsigned int _G2ketosisCumulateDurationInTheLactation = 0;
        bool _G1ketosisSuccessfulTreatmentDuringTheLactation = false;
        bool _ketosisCaseDuringPreviousLactation = false;
#ifdef _LOG
        FunctionalEnumerations::Health::KetosisSeverity _lastKetosisNoteAtMilkControl = FunctionalEnumerations::Health::KetosisSeverity::G0Ketosis;
#endif // _LOG
        
        boost::gregorian::date _endKetosisMilkQuantityEffect = boost::gregorian::date(boost::gregorian::neg_infin); // Date of the milk quantity effects to apply when and after ketosis
        float _ketosisMilkQuantityFactorEffect = 0.0f; // Calendar of the effects to apply when ketosis
        
        // Lameness
        bool _toCareIndividualTodayDueToSporadicLamenessDetection = false;
        bool _toTrimCollectiveTodayForLamenessPrevention = false;
        bool _longNonInfectiousLammenessWhenLastDryoff = false;
        std::map<FunctionalEnumerations::Health::LamenessInfectiousStateType, std::map<unsigned int, float>> _mSpecificDryDaysAllPeriodDependingRankLamenessIncidenceByDay;
        std::map<FunctionalEnumerations::Health::LamenessInfectiousStateType, std::map<unsigned int, float>> _mSpecificLactationDaysAllPeriodDependingRankLamenessIncidenceByDay;
        std::map<FunctionalEnumerations::Health::LamenessInfectiousStateType, std::map<unsigned int, float>> _mSpecificLactationPeriodDependingRankLamenessIncidence;
        
        // Feeding
        // -------
        ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters _forageRationOfTheDay;
        ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters _concentrateRationOfTheDay;
        float _mineralVitaminRationOfTheDay;
        
        // Population
        // ----------
        FunctionalEnumerations::Population::SaleStatus _saleStatus = FunctionalEnumerations::Population::SaleStatus::notToSale;
        float _damLastFullLactationPonderedMilkQuantity = 0.0f;

        // Genetic
        // -------
        Genetic::GeneticValue _hasGeneticValue;
        
        DECLARE_SERIALIZE_STD_METHODS;
        
        void commonInit(FunctionalEnumerations::Genetic::Sex sex, bool sterile, const boost::gregorian::date &date, unsigned int dairyQuarterNumber);
        float getMortalityRisk();

        FunctionalEnumerations::Population::MortalityRiskAge getAgeToMortalityRiskFromAgeMatrix();
        
        void increaseLactationRank(DairyHerd* pHerd, const boost::gregorian::date &date);        
        
        Reproduction::DairyHerdReproductionStates::DairyHerdReproductionState* getCurrentReproductionState();
        
    protected:            
        DairyMammal(const boost::gregorian::date &birth, Mammal* pDam, Genetic::Breed::Breed* pSireBreed, bool sterile, FunctionalEnumerations::Genetic::Sex sex, unsigned int dairyQuarterNumber); // Constructor based with the dam (simulated farrowing)
        virtual void concrete() = 0; // To ensure that class will not be instantiated


    public :
        DairyMammal(){} // serialization constructor
        virtual ~DairyMammal();
        
        DairyMammal(const boost::gregorian::date &simDate, const boost::gregorian::date &birth, Genetic::Breed::Breed* pBreed, Herd* pHerd, FunctionalEnumerations::Genetic::Sex sex, unsigned int dairyQuarterNumber);
        virtual void progress(const boost::gregorian::date &simDate) override; // progress of duration time and unit       

        DairyHerd* getDairyHerd();

        bool isItTheDayForAnimalPlannedTemporalEvent(ExchangeInfoStructures::TemporalEvent::TemporalEventType theTypeEvent, const boost::gregorian::date &theDate); // Is the given date corresponding to a planned temporal event for a given event type ?
        bool hasTemporalEventPlanned(ExchangeInfoStructures::TemporalEvent::TemporalEventType theTypeEvent, const boost::gregorian::date &theDate);
        
        DairyUdder* getDairyUdder();

        // Reproduction
        // ----------
        unsigned int getUniqueIdForReproductionStates();
        FunctionalEnumerations::Reproduction::DairyCowParity getParity();
        unsigned int getLactationRank() override;
        void setCurrentReproductionState(Reproduction::DairyHerdReproductionStates::DairyHerdReproductionState* pState, const boost::gregorian::date &date);
        bool getBabyIsToSale();
        unsigned int getPubertyAge();
        void hasNewOvulation(const boost::gregorian::date &ovulationDate);
        unsigned int getOvulationDuration();
        void belongingOvulatingGroup(bool val);
        unsigned int getTheoricBetweenOvulationDuration();
        float getLamenessOestrusDetectionFactor(FunctionalEnumerations::Reproduction::DetectionMode dm, bool first);
        FunctionalEnumerations::Reproduction::DetectionMode getDetectionMode();
        void planPotentialSpecificityError(const boost::gregorian::date &date);
        virtual void addOvulation(const boost::gregorian::date& ovulationDate) override;
        bool isOvulationDetected(const boost::gregorian::date &simDate, bool first);
        bool oestrusIsManaged();
        void setBreedingDate(const boost::gregorian::date &breedDate, FunctionalEnumerations::Reproduction::DairyCowParity parity);
        bool hasToGoToReproduce(const boost::gregorian::date &simDate);
#if defined _LOG || defined _FULL_RESULTS
        void addInsemination(Insemination* pIns) override;
#endif // _LOG || _FULL_RESULTS      
        bool goToInsemination(const boost::gregorian::date &inseminationDate, bool rightTime, float medicFertilityDelta);
        void enterInPregnancy(const boost::gregorian::date &beginDate);
        boost::gregorian::date getPredictedEndPregnancyDate();
        void notifyPredictedEndPregnancy(const boost::gregorian::date &predictedEndDate);
        boost::gregorian::date_duration getDurationSinceBeginPregnancy(const boost::gregorian::date& date);
        unsigned int getDayDurationUntilPredictedEndPregnancyDate(const boost::gregorian::date& date);
        void setEndPregnancyDate(const boost::gregorian::date& lastCalvingDate, const boost::gregorian::date& endDate, FunctionalEnumerations::Reproduction::EndPregnancyReason reason) override;
        bool pregnancyIsOngoing() override;
        bool isPrimiparous();
        bool isNulliparous();
        unsigned int getTheoricPregnancyDuration();
        unsigned int getDayForFirstCalvings();
        unsigned int getPostEarlyInterruptedDuration(
#ifdef _LOG
                                                    const boost::gregorian::date& date, 
#endif // _LOG
                                                    bool &interrupted);
        unsigned int getPostPartumDuration(
#ifdef _LOG
                                                    const boost::gregorian::date& date, 
#endif // _LOG
                                                     bool ketosisPlaned, bool &interrupted);
        float getLateEmbryonicMortalityRisk();
        float getAbortionRisk();
        void predictEndPregnancyConditions(const boost::gregorian::date &pregnancyDate, boost::gregorian::date &predictedEndPregnancyDate, FunctionalEnumerations::Reproduction::EndPregnancyReason &predictedEndPregnancyReason, std::vector<FunctionalEnumerations::Genetic::Sex> &vSexes, bool onlyForCalving);
        void looseEmbryo (const boost::gregorian::date &date) override;
        void abort(const boost::gregorian::date &date, bool increaseLactationRk);
        float getTwinningProba();
        void calve(const boost::gregorian::date &calvingDate, std::vector<FunctionalEnumerations::Genetic::Sex> &sexes, FunctionalEnumerations::Reproduction::EndPregnancyReason term);
        bool birthIsFemale();
        void planPotentialIAReturnSpecificityError(const boost::gregorian::date &lastIADate);
#ifdef _LOG
        void addDairyHerdReproductionAnimalLog(const boost::gregorian::date &date, const std::string &log);           
#endif // _LOG 
        bool needTreatmentForAbnormalNoOestrusDetection(const boost::gregorian::date &date, bool reproVetContract);
        bool needTreatmentForAbnormalUnsuccessfulIA(const boost::gregorian::date &date, bool reproVetContract);
        bool needTreatmentForNegativePregnancyDiagnostic(const boost::gregorian::date &date, bool reproVetContract);
        bool isInAbnormalNoOestrusDetectionSituation(const boost::gregorian::date &date, bool reproVetContract);
        bool isInAbnormalUnsuccessfulIASituation(const boost::gregorian::date &date, bool reproVetContract);
        bool isInNegativePregnancyDiagnosticSituation(const boost::gregorian::date &date, bool reproVetContract);
        void takeAbnormalNoOestrusDetectionTreatment(const boost::gregorian::date &date, ExchangeInfoStructures::Parameters::TreatmentTypeParameters &treatment);
        void takeAbnormalUnsuccessfulIATreatment(const boost::gregorian::date &date, ExchangeInfoStructures::Parameters::TreatmentTypeParameters &treatment);
        void takeNegativePregnancyDiagnosticTreatment(const boost::gregorian::date &date, ExchangeInfoStructures::Parameters::TreatmentTypeParameters &treatment);
        void updateLastOestrusDetectionDate(const boost::gregorian::date &detectionDate);
        void setFuturForcedOvulation(Reproduction::DairyHerdReproductionStates::OvulationState* _pForcedOvulationState);
        Reproduction::DairyHerdReproductionStates::OvulationState* getForcedOvulation(const boost::gregorian::date &date);
        float getDeltaFertilityVetContract();

        
        // Production
        // ----------
        void setCurrentLactationState(Lactation::DairyHerdLactationStates::DairyHerdLactationState* pState);
        unsigned int getUniqueIdForLactationState();
        float getMilkPeakQuantity();
        bool lactationIsOngoing();
        float getMilkQuantityOfTheDay();
        int getLactationStage();
        bool lactationStageIsAfterPeak();
        void setLactationStage(int lactationStage);
        void setNotYetControledForThisLactation();
        Lactation::DairyHerdLactationStates::DairyHerdLactationState* getCurrentLactationState();
        void considerHerdNavigatorTest(const boost::gregorian::date &simDate);
        bool isMilking() override;
        bool getMilkControlInformations(bool useCetodetect, bool &controled, int &lactationStage, unsigned int &lactationRank, bool &SCCLessThanRefInPreviousMilkControl, bool &SCCLessThanRefNow, const boost::gregorian::date &simDate, ExchangeInfoStructures::MilkProductCharacteristic &cumulativeIndividualMilkSamples
                                                                                                                                        , bool &isDetectedInfectiousLameness
                                                                                                                                        , bool &isInfectiousG1Lameness
                                                                                                                                        , bool &isInfectiousG2Lameness
                                                                                                                                        , bool &isDetectedNonInfectiousLameness
                                                                                                                                        , bool &isNonInfectiousG1Lameness
                                                                                                                                        , bool &isNonInfectiousG2Lameness
#ifdef _LOG
                                                                                                                                        , FunctionalEnumerations::Health::KetosisSeverity &ketosisSeverity
                                                                                                                                        , FunctionalEnumerations::Global::Month &lastCalvingMonth    
                                                                                                                                        , bool &newKetosisCase
#endif // _LOG
                                                                                    ) override;
       
        
        const std::vector<ExchangeInfoStructures::MilkProductCharacteristic> &getNormalizedMilkProductionCurve();
        float produceMilkForTheDay(unsigned int day, const boost::gregorian::date &date, ExchangeInfoStructures::MilkProductCharacteristic &currentDriedPeriodEffectToMilk, Lactation::DairyHerdLactationStates::DairyHerdLactationState* &pNewState);
        void setMilkLossQuantityDueMastitis(float milkLossQuantityDueMastitis);
        void dryOff(const boost::gregorian::date &date);
        void setLastBeginLactationPonderedMilkQuantity(float ponderedQuantity, const boost::gregorian::date &date);
        float getLastFullLactationPonderedMilkQuantity();
        unsigned int getBreedingDelayAfterCalvingDecision();    
        const boost::gregorian::date &getLastDryingDate();
        float getLastMeanPeriodSCC();
        float getLastMeanPeriodMilkQuantity();

#ifdef _LOG
        void addDairyHerdLactationAnimalLog(const boost::gregorian::date &date, const std::string &log);
#endif // _LOG 
        
        // Feeding
        // -------     
        unsigned int getWeaningAge();
        bool isToBeFed();
        
        void consumeUnweanedCalfMilk();
        void consumeBulkMilkFeed(const boost::gregorian::date &simDate, int periodDay);
        void consumeDiscardedMilkFeed(const boost::gregorian::date &simDate, int periodDay);
        void consumeDehydratedMilkFeed(const boost::gregorian::date &simDate, int periodDay);
        void consumeConcentrateUnweanedFemaleCalf();
        void consumeForageConcentrateAndMineralVitaminWeanedFemaleCalf();
        void consumeForageConcentrateAndMineralVitaminYoungHeifer(FunctionalEnumerations::Population::Location location);
        void consumeForageConcentrateAndMineralVitaminOldHeifer(FunctionalEnumerations::Population::Location location);
        void consumeForageConcentrateAndMineralVitaminBredHeifer(FunctionalEnumerations::Population::Location location);
        void consumeForageConcentrateAndMineralVitaminDriedCow(FunctionalEnumerations::Population::Location location);
        void consumeForageConcentrateAndMineralVitaminPeriParturientCow(FunctionalEnumerations::Population::Location location);
        void consumeForageConcentrateAndMineralVitaminLactatingCow(FunctionalEnumerations::Population::Location location);
#ifdef _LOG
        void updateLogFeedConsumption();
#endif // _LOG

#ifdef _LOG
        void addDairyHerdFeedingAnimalLog(const boost::gregorian::date &date, const std::string &log);           
#endif // _LOG 

        // Population
        // ----------
        void setSaleStatus(FunctionalEnumerations::Population::SaleStatus saleStatus);
        FunctionalEnumerations::Population::SaleStatus getSaleStatus();
        static void initAgeToMortalityRiskAgeMatrix();
        void goInLactatingCowsBatch(const boost::gregorian::date &simDate);
        bool isToCullForInfertility(const boost::gregorian::date &theDate);
        void cullingIsDecided(FunctionalEnumerations::Population::CullingStatus newCullingStatus, bool today, const boost::gregorian::date &date) override; 
        bool isAdult();              
        bool isUndergoingCull();
        float getDamLastFullLactationMilkQuantity();
        
        float getTodayCullingScore();
        
        FunctionalEnumerations::Population::Location getCurrentLocation(); 
        
        // Health
        // ------
        // Mastitis
        bool needToStudyDairyUdderHealth();
        void updateDayMastitisOccurenceProbability(std::map<FunctionalEnumerations::Health::BacteriumType, double> &mastitisOccurenceProbabilityToUpdate, const boost::gregorian::date &date);

        FunctionalEnumerations::Health::MastitisSeverity getRandomMastitisSeverity(FunctionalEnumerations::Health::BacteriumType bacterium);
        void ensureUdderIsNowMastitisSensitive(const boost::gregorian::date &date);
        unsigned int getLactationRankForMastitisSeverityDegree();
        void setPeriPartumMastitisRiskPeriod(const boost::gregorian::date &date, bool calving);
        void setDryingOffMastitisRiskPeriod(const boost::gregorian::date &date);
        void setDryingInvolutedMastitisRiskPeriod(const boost::gregorian::date &date);
        void setLactationMastitisRiskPeriod(const boost::gregorian::date &date);
        void setCurrentMastitisRiskPeriodState(Health::Mastitis::RiskPeriodStates::DairyCowMastitisRiskPeriodState* pState);
        Health::Mastitis::RiskPeriodStates::DairyCowMastitisRiskPeriodState* getCurrentMastitisRiskPeriodState();
        void initMilkPotentialFactor();
        float getMilkPotentialFactor();
        bool G1SeverityMastitisIsDetected();
        void notifyNewMastitisOccurence(FunctionalEnumerations::Health::BacteriumType bacterium, const boost::gregorian::date &date, bool newCase, bool newSubclinical, bool newClinical);
        unsigned int getClinicalMastitisCount(unsigned int lactationNumber);
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &takePreventiveDryOffMastitisTreatmentType(
#ifdef _LOG
                                                                                                                const boost::gregorian::date &date 
#endif // _LOG
                                                                                                                );
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &takeCurrentMastitisTreatmentType(
#ifdef _LOG
                                                                                                                const boost::gregorian::date &date, 
#endif // _LOG
                                                                                                                FunctionalEnumerations::Health::BacteriumType bacterium,
                                                                                                                FunctionalEnumerations::Health::MastitisSeverity severity);
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &getMastitisTreatmentType(FunctionalEnumerations::Health::BacteriumType bacterium);
        float getCurrentMastitisPreventionFactor();
        bool isCurrentlyMastitisInfected(bool onlyClinical);
#ifdef _LOG
        unsigned int getDayOfTheMastitisRiskReferenceCycle(const boost::gregorian::date &date);
#endif // _LOG
        
        // Ketosis
        void enterInKetosisRiskPeriod(const boost::gregorian::date &date);
        void exitFromKetosisRiskPeriod(const boost::gregorian::date &date);
        void resetDataDependingLastLactation();
        bool isInKetosisRiskPeriod();
        unsigned int getDurationUntilEndKetosisRisk();
        bool useHerdNavigator();
        void notifyNewKetosisOccurence(const boost::gregorian::date &date, FunctionalEnumerations::Health::KetosisSeverity severity);
        float getKetosisRisk(const boost::gregorian::date &beginDate);
        float getBasisKetosisIncidence();   
        unsigned int getSubclinicalKetosisDuration();
        bool willHaveKetosisRecurrency();
        void takeMonensinBolus(const boost::gregorian::date &date);
        unsigned int getLactationRankWithLimitForKetosis(unsigned int limit);
        unsigned int getLactationRankWithLimit(unsigned int limit);
        FunctionalEnumerations::Reproduction::DairyCowParity getParityForKetosis();
        void setCurrentKetosisState(Health::Ketosis::KetosisStates::KetosisState* pState);
        void increaseG1ketosisCumulateDurationInTheLactation();
        void increaseG2ketosisCumulateDurationInTheLactation();
        void considereG1KetosisSuccessfulTreatmentDuringTheLactation();
        void getKetosisEffectOnMilkProduction(const boost::gregorian::date &date, ExchangeInfoStructures::MilkProductCharacteristic &mc);
        boost::gregorian::date &getEndKetosisMilkQuantityEffect();
        float &getKetosisMilkQuantityFactorEffect();
        bool hadKetosisCaseDuringPreviousLactation();
        bool hasCurrentlyG1ToG2KetosisCase();

        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &takeKetosisTreatment(const boost::gregorian::date &date, FunctionalEnumerations::Health::KetosisSeverity severity, bool &success
#ifdef _LOG
                                                                                                    , std::string origin
#endif // _LOG            
                                                                                                    );
#ifdef _LOG
        unsigned int getIdForKetosis();
#endif // _LOG            
        void diesNowBecauseOfKetosis(
#ifdef _LOG                   
            const boost::gregorian::date &date 
#endif // _LOG            
                                            );
                
        // Lameness
        bool isLame(bool &infectious, bool &nonInfectious);
        bool hasCurrentTooLongLammenessCaseForEngageInsemination(const boost::gregorian::date &date, FunctionalEnumerations::Health::LamenessInfectiousStateType list);
        void addLamenessHealthTransaction(
#ifdef _LOG                
                                                    const boost::gregorian::date &transactionDate, const std::string &TYPE, 
#endif // _LOG                
                                                    const std::string &KEY, float &annualDataToSet,
                                                    float amontFactor = 1.0f);

        bool hasCurrentlyEfficientFootbath(const boost::gregorian::date &date, float &dayRatio);
        void notifyForLamenessSporadicDetectionOrVerificationToday(
#ifdef _LOG                
                                                    const boost::gregorian::date &date
#endif // _LOG                
                                                    );
        void toCareIndividualTodayForDetectedLamenessCase();
        void careIndividualTodayForDetectedLamenessCase(const boost::gregorian::date &careDate);
        bool hadRecentTrimming(const boost::gregorian::date &date);
        void toTrimCollectiveTodayForLamenessPrevention();
        void trimCollectiveTodayForLamenessPrevention(const boost::gregorian::date &trimDate);
        FunctionalEnumerations::Health::LamenessDetectionMode getLamenessDetectionMode();
        void becomeLamenessSensitive(const boost::gregorian::date &date);
        float getLamenessRisk(const boost::gregorian::date &beginDate, FunctionalEnumerations::Health::LamenessInfectiousStateType infectiousType);   
        bool needToTrimForLamenessPreventionWhenLactation();
        void toTrimForLamenessPrevention(const boost::gregorian::date &date);

        void diesNowBecauseOfLameness(
#ifdef _LOG                   
            const boost::gregorian::date &date, 
#endif // _LOG            
                                            FunctionalEnumerations::Population::DeathReason deathReason);

        // Genetic
        // -------
        Genetic::GeneticValue &getGeneticValue();
        Genetic::GeneticValue &getSirGeneticValueOfTheLastFertilyInsemination();
        Genetic::BreedInheritance* getCowBreedInheritance();
//        float getGeneticIndex(FunctionalEnumerations::Genetic::GeneticStrategy strategy);

        // Accounting
        // ----------
        float getExitAmount() override;

    }; /* End of class DairyMammal */
} /* End of namespace DataStructures */
#endif // DataStructures_DairyMammal_h
