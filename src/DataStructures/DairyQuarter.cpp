#include "DairyQuarter.h"

// Project
#include "../Health/Diseases/Mastitis/DairyQuarterMastitisInfectionStates/MastitisUnsensitiveDairyQuarterState.h"
#include "../Health/Diseases/Mastitis/DairyQuarterMastitisInfectionStates/MastitisSensitiveDairyQuarterState.h"
#include "../Health/Diseases/Mastitis/DairyQuarterMastitisInfectionStates/MastitisInfectedDairyQuarterState.h"
#include "DairyUdder.h"
#include "DairyHerd.h"
#include "../ResultStructures/DairyFarmResult.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(DataStructures::DairyQuarter) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Quarter); \
    ar & BOOST_SERIALIZATION_NVP(_mpHasCurrentMastitisStates); \
    ar & BOOST_SERIALIZATION_NVP(_mHasProgramedRelapseMastitis); \
    ar & BOOST_SERIALIZATION_NVP(_mMastitisTreatments); \
    ar & BOOST_SERIALIZATION_NVP(_newMastitisInfectionDuringTheStep); \

    IMPLEMENT_SERIALIZE_STD_METHODS(DairyQuarter);

    DairyQuarter::DairyQuarter(DairyUdder* pUdder, unsigned int number, const boost::gregorian::date &date) : Quarter (pUdder, number)
    {
        // Bacterium mastitis map initialization
        for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++)
        {
            // Basic state is unsensitive;
            _mpHasCurrentMastitisStates[(FunctionalEnumerations::Health::BacteriumType)iBact] = new Health::Mastitis::DairyQuarterInfectionStates::MastitisUnsensitiveDairyQuarterState(date, this, (FunctionalEnumerations::Health::BacteriumType)iBact);
        }
    }

    DairyQuarter::~DairyQuarter()
    {
        // Health states
        for (std::map<FunctionalEnumerations::Health::BacteriumType, Health::Mastitis::DairyQuarterInfectionStates::DairyHerdDairyQuarterMastitisInfectionState*>::iterator it = _mpHasCurrentMastitisStates.begin(); it != _mpHasCurrentMastitisStates.end(); it++)
        {
            delete it->second;
        }
        _mpHasCurrentMastitisStates.clear();
    }

    DairyUdder* DairyQuarter::getDairyUdder()
    {
        return (DairyUdder*) _pIsInUdder;
    }

    void DairyQuarter::setCurrentMastitisState(Health::Mastitis::DairyQuarterInfectionStates::DairyHerdDairyQuarterMastitisInfectionState* pState)
    {
        // New state
        if (pState != nullptr)
        {
            delete _mpHasCurrentMastitisStates[pState->getConcernedBacteriumType()];
            _mpHasCurrentMastitisStates[pState->getConcernedBacteriumType()] = pState;

            // update udder infectious status
            if (pState->isInfected())
            {
                // new infection
                _newMastitisInfectionDuringTheStep = true;
            }

            getDairyUdder()->notifyMastitisInfectionStateChanges();
        }
    }

    void DairyQuarter::progress(const boost::gregorian::date &simDate, Tools::Random &random)
    {

        Quarter::progress(simDate, random);

        // Mastitis
        // Updated by the states themselves, to know is a post mastitis is healed, to probably heal it too
        std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> healthToday;
        std::vector<Health::Mastitis::DairyQuarterInfectionStates::DairyHerdDairyQuarterMastitisInfectionState*> currentInfectiousStates;

        for (std::map<FunctionalEnumerations::Health::BacteriumType, Health::Mastitis::DairyQuarterInfectionStates::DairyHerdDairyQuarterMastitisInfectionState*>::iterator it = _mpHasCurrentMastitisStates.begin(); it != _mpHasCurrentMastitisStates.end(); it++)
        {
            FunctionalEnumerations::Health::BacteriumType bacterium = (it->first);
#ifdef _LOG
            bool wasInfected = isInfected(bacterium, false);
#endif // _LOG            

            Health::Mastitis::DairyQuarterInfectionStates::DairyHerdDairyQuarterMastitisInfectionState* pNewstate = nullptr;
            (it->second)->progress(simDate, healthToday, random, pNewstate);
            setCurrentMastitisState(pNewstate);

            if (isInfected(_mpHasCurrentMastitisStates.find(bacterium)->first, false))
            {
                currentInfectiousStates.push_back(_mpHasCurrentMastitisStates.find(bacterium)->second);
#ifdef _LOG
                if (!wasInfected)
                {
                    // This is a new infection
                    unsigned int dayOfTheMastitisRiskReferenceCycle = getDairyUdder()->getDayOfTheMastitisRiskReferenceCycle(simDate);
                    // Careful : we exclude here the mastitis not in the reference risk cycle (for example mastitis over 260 lactation days before drying off)
                    if (dayOfTheMastitisRiskReferenceCycle != FunctionalConstants::Health::REFERENCE_PERIPARTUM_MASTITIS_RISK_PERIOD_DURATION + FunctionalConstants::Health::REFERENCE_LACTATION_MASTITIS_RISK_PERIOD_DURATION - 1)
                    {
                        // New mastitis in the reference risk period : Log this new occurence
                        Results::IIMResult ir;
                        ir.id = getDairyUdder()->getMammalUniqueId();
                        ir.date = simDate;
                        ir.dayOfTheReferenceCycle = dayOfTheMastitisRiskReferenceCycle;
                        ir.dairyQuarterNumber = _number;
                        ir.bacteriumType = bacterium;
                        ir.mastitisSeverity = (_mpHasCurrentMastitisStates.find(bacterium)->second)->getMastitisSeverity();
                        _pIsInUdder->getCurrentResults()->getHealthResults().IIMResults.push_back(ir);
                    }
                }
#endif // _LOG            
            }
        }

        if (_newMastitisInfectionDuringTheStep)
        {
            // Refracting period begin for sensitives
            boost::gregorian::date_duration duration(FunctionalConstants::Health::MASTITIS_REFRACTORY_PERIOD_DURATION);
            mastitisSensitiveIsNowUnsensitive(simDate, duration);
            _newMastitisInfectionDuringTheStep = false;
        }

        // Here, we have 
        // - in healthToday all the begin dates of healed mastitis today, with their bacterium type, 
        // - in currentInfectiousStates the list of infections today,
        // May be some of heal of today can heal others tomorrow...
        if (healthToday.size() > 0 && currentInfectiousStates.size()>0) // At least a couple ?
        {     
            for (std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType>::iterator itDate = healthToday.begin(); itDate != healthToday.end(); itDate++)
            {
                FunctionalEnumerations::Health::BacteriumType healtBacterium = itDate->second;
                for (std::vector<Health::Mastitis::DairyQuarterInfectionStates::DairyHerdDairyQuarterMastitisInfectionState*>::iterator itInfection = currentInfectiousStates.begin(); itInfection != currentInfectiousStates.end(); itInfection++)
                {
                    if ((*itInfection)->getBeginDate() < itDate->first)
                    {
                        // The healed mastitis appears after the this current infection, so, may be healed too...
                        if (not random.ran_bernoulli((FunctionalConstants::Health::SUBSISTING_MASTITIS_WITH_OCCURRENCY_MASTITIS_HEALING_PROBABILITY.find(healtBacterium)->second).find((*itInfection)->getConcernedBacteriumType())->second))
                        {
                            // The current infection will healt tomorrow
                            ((Health::Mastitis::DairyQuarterInfectionStates::MastitisInfectedDairyQuarterState*)(*itInfection))->healTomorrowBecauseOfOccurencyHealth(simDate);
                        }
                    }
                }
            }
        }

        // Some mastitis treatment needed today ?
        std::map<boost::gregorian::date, FunctionalEnumerations::Health::MastitisRiskPeriodType>::iterator it = _mMastitisTreatments.find(simDate);
        if (it != _mMastitisTreatments.end())
        {
            getDairyUdder()->notifyMastitisTreatmentToAllDairyQuarters(simDate, random);
        }

        // Cleaning the mastitis relapse of the day
        std::map<boost::gregorian::date, std::map<FunctionalEnumerations::Health::BacteriumType, FunctionalEnumerations::Health::MastitisSeverity>>::iterator itDate = _mHasProgramedRelapseMastitis.find(simDate);
        if (itDate != _mHasProgramedRelapseMastitis.end())
        {
            _mHasProgramedRelapseMastitis.erase(itDate);
        }
    }
    
    void DairyQuarter::takeIntoAccountMastitisTreatment(const boost::gregorian::date &simDate, Tools::Random &random)
    {
        for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++)
        {
            FunctionalEnumerations::Health::BacteriumType bacterium = (FunctionalEnumerations::Health::BacteriumType)iBact;
            // We have to set the date of the expected effect of the treatment for each bacterium infection, or a protection for non infected.
            Health::Mastitis::DairyQuarterInfectionStates::DairyHerdDairyQuarterMastitisInfectionState* pNewstate = nullptr;
            ExchangeInfoStructures::Parameters::TreatmentTypeParameters &treatment = getDairyUdder()->getMastitisTreatmentType(bacterium);
            _mpHasCurrentMastitisStates[bacterium]->setTreatmentInformations(simDate, treatment.effectDelayMeanOrMin, treatment.successProbabilityAllCasesOrFirst, treatment.protectProbability, treatment.protectDuration, random, pNewstate);
            setCurrentMastitisState(pNewstate);
        }
    }

    double DairyQuarter::getMastitisOccurenceProbability(FunctionalEnumerations::Health::BacteriumType bacterium)
    {
        return getDairyUdder()->getDairyQuarterMastitisOccurenceProbability(bacterium);  
    }

    FunctionalEnumerations::Health::MastitisSeverity DairyQuarter::getRandomMastitisSeverity(FunctionalEnumerations::Health::BacteriumType bacterium)
    {
        return getDairyUdder()->getRandomMastitisSeverity(bacterium);            
    }

    void DairyQuarter::appendMastitis(FunctionalEnumerations::Health::BacteriumType bacterium, const boost::gregorian::date &beginDate, bool newCase, bool newSubclinical, bool newClinical)
    {
        // Informe the udder of the new mastitis occurence
        getDairyUdder()->notifyNewMastitisOccurence(bacterium, beginDate, newCase, newSubclinical, newClinical);
     }

    bool DairyQuarter::isSensitive(FunctionalEnumerations::Health::BacteriumType bacterium)
    {
        return _mpHasCurrentMastitisStates[bacterium]->isSensitive();     
    }

    bool DairyQuarter::isUnsensitive(FunctionalEnumerations::Health::BacteriumType bacterium)
    {
        return _mpHasCurrentMastitisStates[bacterium]->isUnsensitive();          
    }

    void DairyQuarter::mastitisUnsensitiveIsNowSensitive(const boost::gregorian::date &date)
    {
        for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++)
        {
            if (isUnsensitive((FunctionalEnumerations::Health::BacteriumType)iBact))
            {
                setCurrentMastitisState(new Health::Mastitis::DairyQuarterInfectionStates::MastitisSensitiveDairyQuarterState(date, this, (FunctionalEnumerations::Health::BacteriumType)iBact));
            }
        }
    }

    void DairyQuarter::mastitisSensitiveIsNowUnsensitive(const boost::gregorian::date &date, boost::gregorian::date_duration &duration)
    {
        for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++)
        {
            if (isSensitive((FunctionalEnumerations::Health::BacteriumType)iBact))
            {
                Health::Mastitis::DairyQuarterInfectionStates::MastitisUnsensitiveDairyQuarterState* pNewState = new Health::Mastitis::DairyQuarterInfectionStates::MastitisUnsensitiveDairyQuarterState(date, this, (FunctionalEnumerations::Health::BacteriumType)iBact);
                pNewState->setDuration(duration);
                setCurrentMastitisState(pNewState);
            }
        }
    }

    // True if infected by a given bacterium
    bool DairyQuarter::isInfected(FunctionalEnumerations::Health::BacteriumType bacterium, bool onlyClinical)
    {
        if (onlyClinical)
        {
            return _mpHasCurrentMastitisStates[bacterium]->isClinicalInfected();
        }
        else
        {
            return _mpHasCurrentMastitisStates[bacterium]->isInfected();
        }
        
    }
    
    // True if G1 toi G3 mastitis infected by a given bacterium
    bool DairyQuarter::isG1ToG3MastitisInfected(FunctionalEnumerations::Health::BacteriumType bacterium)
    {
            return _mpHasCurrentMastitisStates[bacterium]->isG1ToG3MastitisInfected();
    }

    /// Milk product reducing based on a current mastitis
    float DairyQuarter::getHightEffectMastitisMilkProductReduction(const boost::gregorian::date &theDate)
    {
        // We look at the most strong reduction of each bacterium
        float res = 0.0f;
        for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++)
        {
            float currentPercentLoss = _mpHasCurrentMastitisStates[(FunctionalEnumerations::Health::BacteriumType)iBact]->getHightEffectMastitisMilkProductReduction(theDate);
            if (currentPercentLoss > res)
            {
                res = currentPercentLoss;
            }
        }
        return res;
    }

    /// Additional SCC due to a current mastitis
    float DairyQuarter::getMastitisAdditionalSCC(Tools::Random &random)
    {
        float res = 0.0f;
        for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++)
        {
            res += _mpHasCurrentMastitisStates[(FunctionalEnumerations::Health::BacteriumType)iBact]->getMastitisAdditionalSCC(random);
        }
        return res;
    }

    void DairyQuarter::isLostNow()
    {
        getDairyUdder()->notifyDairyQuarterIsLostNow(this);
        getDairyUdder()->notifyMastitisInfectionStateChanges();
    }

    void DairyQuarter::programMastitisRelapse(boost::gregorian::date beginRelapseDate, FunctionalEnumerations::Health::MastitisSeverity severity, FunctionalEnumerations::Health::BacteriumType bacterium)
    {
        std::map<boost::gregorian::date, std::map<FunctionalEnumerations::Health::BacteriumType, FunctionalEnumerations::Health::MastitisSeverity>>::iterator itDate = _mHasProgramedRelapseMastitis.find(beginRelapseDate);
        if (itDate == _mHasProgramedRelapseMastitis.end())
        {
            // This date doesn't exists yet
            std::map<FunctionalEnumerations::Health::BacteriumType, FunctionalEnumerations::Health::MastitisSeverity> bacteriumMap;
            _mHasProgramedRelapseMastitis[beginRelapseDate] = bacteriumMap;
            itDate = _mHasProgramedRelapseMastitis.find(beginRelapseDate);
        }
        std::map<FunctionalEnumerations::Health::BacteriumType, FunctionalEnumerations::Health::MastitisSeverity>::iterator itbact = (itDate->second).find(bacterium);
        if (itbact == (itDate->second).end())
        {
            // This bacterium doesn't exists yet
            (itDate->second)[bacterium] = severity;
        }
        else
        {
            // This bacterium already exists
            if (severity > itbact->second)
            {
                // We keep the greater severity
                itbact->second = severity;
            }
        }
    }

    bool DairyQuarter::hasProgramedMastitis(boost::gregorian::date date, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity &severity)
    {
        std::map<boost::gregorian::date, std::map<FunctionalEnumerations::Health::BacteriumType, FunctionalEnumerations::Health::MastitisSeverity>>::iterator itDate = _mHasProgramedRelapseMastitis.find(date);
        if (itDate == _mHasProgramedRelapseMastitis.end())
        {
            return false;
        }
        else
        {
            std::map<FunctionalEnumerations::Health::BacteriumType, FunctionalEnumerations::Health::MastitisSeverity>::iterator itbact = (itDate->second).find(bacterium);
            if (itbact == (itDate->second).end())
            {
                return false;
            }           
            else
            {
                // A relapse is programed
                severity = itbact->second; // Programed severity
                return true;
            }
        }
    }

    bool DairyQuarter::hasBeenInfectedDuringTheStep()
    {
        return _newMastitisInfectionDuringTheStep;
    }
    
    DairyHerd* DairyQuarter::getDairyHerd()
    {
        return getDairyUdder()->getDairyHerd();
    }

    void DairyQuarter::needMastitisTreatment(const boost::gregorian::date &date, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity)
    {
        std::map<boost::gregorian::date, FunctionalEnumerations::Health::MastitisRiskPeriodType>::iterator treatmentIt = _mMastitisTreatments.find(date);
        if (treatmentIt == _mMastitisTreatments.end())
        {
            // We dont have a mastitis treatment today yet
            ExchangeInfoStructures::Parameters::TreatmentTypeParameters &mt = takeCurrentMastitisTreatmentType(
#ifdef _LOG
                                                                                                                date, 
#endif // _LOG
                                                                                                                bacterium,
                                                                                                                severity);
            _mMastitisTreatments[date] = getCurrentMastitisRiskPeriodType();
            const boost::gregorian::date &milkWaitingDate = date + boost::gregorian::days(mt.milkWaitingTime - 1); // Milk to be discarded
            updateUdderEndMilkWaitingDate(milkWaitingDate);
        }
    }

    bool DairyQuarter::G1SeverityMastitisIsDetected()
    {
        return getDairyUdder()->G1SeverityMastitisIsDetected();
    }

    void DairyQuarter::mammalDiesNowBecauseOfMastitis(
#ifdef _LOG
                      const boost::gregorian::date &date
#endif // _LOG            
                                            )
    {
        getDairyUdder()->notifyAnimalDiesNowBecauseOfMastitis(
                
#ifdef _LOG
                                                        date
#endif // _LOG            
                                                            );
    }

    FunctionalEnumerations::Health::MastitisRiskPeriodType DairyQuarter::getCurrentMastitisRiskPeriodType()
    {
        return getDairyUdder()->getCurrentMastitisRiskPeriodType();
    }

    ExchangeInfoStructures::Parameters::TreatmentTypeParameters &DairyQuarter::takeCurrentMastitisTreatmentType(
#ifdef _LOG
                                                                                                        const boost::gregorian::date &date, 
#endif // _LOG
                                                                                                        FunctionalEnumerations::Health::BacteriumType bacterium,
                                                                                                        FunctionalEnumerations::Health::MastitisSeverity severity)
    {
       return getDairyUdder()->takeCurrentMastitisTreatmentType(
#ifdef _LOG
                                                                date, 
#endif // _LOG
                                                                bacterium, severity);
    }

    void DairyQuarter::updateUdderEndMilkWaitingDate(const boost::gregorian::date &date)
    {
        getDairyUdder()->updateEndMilkWaitingDate(date);
    }

    void DairyQuarter::cowIsCulledForSevereMastitis(const boost::gregorian::date &date)
    {
        getDairyUdder()->notifyMammalIsCulledForSevereMastitis(date);
    }

    float DairyQuarter::getCurrentMastitisPreventionFactor()
    {
        return getDairyUdder()->getCurrentMastitisPreventionFactor();
    }
    
#ifdef _LOG
    std::string DairyQuarter::getStrCurrentMastitisDiagnostic()
    {
        std::string res = Tools::toString(_number) + ":";
        for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++)
        {
            res += "[" + Tools::toString(iBact) + _mpHasCurrentMastitisStates[(FunctionalEnumerations::Health::BacteriumType)iBact]->getStrCurrentMastitisDiagnostic();
        }
        return res;
    }

    unsigned int DairyQuarter::getMammalUniqueId()
    {
        return getDairyUdder()->getMammalUniqueId();
    }
#endif // _LOG            
}