#ifndef DataStructures_DairyQuarter_h
#define DataStructures_DairyQuarter_h

// project
#include "../Tools/Serialization.h"
#include "../Tools/Random.h"
#include "../Health/Diseases/Mastitis/DairyQuarterMastitisInfectionStates/I_DairyHerdMastitisProneDairyQuarter.h"
#include "../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "../ExchangeInfoStructures/TreatmentTypeParameters.h"
#include "Quarter.h"

namespace Health
{
    namespace Mastitis
    {
        namespace DairyQuarterInfectionStates
        {
            class DairyHerdDairyQuarterMastitisInfectionState;
        }
    }
}

namespace DataStructures
{
    class DairyUdder;
    class DairyHerd;

    /// Dairy quarter of dairy udder
    class DairyQuarter :    public Quarter
//                            , public Health::Mastitis::DairyQuarterInfectionStates::I_DairyHerdMastitisProneDairyQuarter
    {

        // Health
        // ------
        std::map<FunctionalEnumerations::Health::BacteriumType, Health::Mastitis::DairyQuarterInfectionStates::DairyHerdDairyQuarterMastitisInfectionState*> _mpHasCurrentMastitisStates;
        std::map<boost::gregorian::date, std::map<FunctionalEnumerations::Health::BacteriumType, FunctionalEnumerations::Health::MastitisSeverity>> _mHasProgramedRelapseMastitis;
        std::map<boost::gregorian::date, FunctionalEnumerations::Health::MastitisRiskPeriodType> _mMastitisTreatments;
        bool _newMastitisInfectionDuringTheStep = false;

        DECLARE_SERIALIZE_STD_METHODS;

    protected:
        DairyQuarter(){}; // For serialization
        void concrete(){}; // To allow instanciation

    public:
        DairyQuarter(DairyUdder* pUdder, unsigned int number, const boost::gregorian::date &date);
        virtual ~DairyQuarter (); // Destructor
        DairyUdder* getDairyUdder();
        DairyHerd* getDairyHerd();
        void setCurrentMastitisState(Health::Mastitis::DairyQuarterInfectionStates::DairyHerdDairyQuarterMastitisInfectionState* pState);
        void progress(const boost::gregorian::date &simDate, Tools::Random &random) override;
        void takeIntoAccountMastitisTreatment(const boost::gregorian::date &simDate, Tools::Random &random);
        double getMastitisOccurenceProbability(FunctionalEnumerations::Health::BacteriumType bacterium);
        FunctionalEnumerations::Health::MastitisSeverity getRandomMastitisSeverity(FunctionalEnumerations::Health::BacteriumType bacterium);
        void appendMastitis(FunctionalEnumerations::Health::BacteriumType bacterium, const boost::gregorian::date &beginDate, bool newCase, bool newSubclinical, bool newClinical);
        bool isSensitive(FunctionalEnumerations::Health::BacteriumType bacterium);
        bool isUnsensitive(FunctionalEnumerations::Health::BacteriumType bacterium);
        void mastitisUnsensitiveIsNowSensitive(const boost::gregorian::date &date);
        void mastitisSensitiveIsNowUnsensitive(const boost::gregorian::date &date, boost::gregorian::date_duration &duration);
        bool isInfected(FunctionalEnumerations::Health::BacteriumType bacterium, bool onlyClinical);
        bool isG1ToG3MastitisInfected(FunctionalEnumerations::Health::BacteriumType bacterium);
        float getHightEffectMastitisMilkProductReduction(const boost::gregorian::date &theDate); // Milk product reducing based on the hight effect of current mastitis
        float getMastitisAdditionalSCC(Tools::Random &random); // Additional SCC due to a current mastitis
        void isLostNow();
        bool G1SeverityMastitisIsDetected();
        void programMastitisRelapse(boost::gregorian::date beginRelapseDate, FunctionalEnumerations::Health::MastitisSeverity degree, FunctionalEnumerations::Health::BacteriumType bacterium);
        bool hasProgramedMastitis(boost::gregorian::date date, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity &severity);
        bool hasBeenInfectedDuringTheStep();
        void mammalDiesNowBecauseOfMastitis(
#ifdef _LOG
                  const boost::gregorian::date &date
#endif // _LOG            
                                            );
        FunctionalEnumerations::Health::MastitisRiskPeriodType getCurrentMastitisRiskPeriodType();
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &takeCurrentMastitisTreatmentType(
#ifdef _LOG
                                                                                                            const boost::gregorian::date &date, 
#endif // _LOG
                                                                                                            FunctionalEnumerations::Health::BacteriumType bacterium,
                                                                                                            FunctionalEnumerations::Health::MastitisSeverity severity);
        void updateUdderEndMilkWaitingDate(const boost::gregorian::date &date);
        void needMastitisTreatment(const boost::gregorian::date &date, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity);
        void cowIsCulledForSevereMastitis(const boost::gregorian::date &date);
       float getCurrentMastitisPreventionFactor();

#ifdef _LOG
        std::string getStrCurrentMastitisDiagnostic();
        unsigned int getMammalUniqueId();
#endif // _LOG            
    };
}
#endif // DataStructures_DairyQuarter_h
