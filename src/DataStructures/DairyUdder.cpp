#include <vector>

#include "DairyUdder.h"

// Project
#include "DairyMammal.h"
#include "DairyQuarter.h"
#include "../Health/Diseases/Mastitis/DairyCowMastitisRiskPeriodStates/DairyCowMastitisRiskPeriodState.h"
#include "../Health/Diseases/Mastitis/DairyQuarterMastitisInfectionStates/MastitisSensitiveDairyQuarterState.h"
#include "../ResultStructures/DairyFarmResult.h"
#include "../Lactation/LactationManagement.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(DataStructures::DairyUdder) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Udder); \
    ar & BOOST_SERIALIZATION_NVP(_ripeToMastitis); \
    ar & BOOST_SERIALIZATION_NVP(_endMilkWaitingDate); \
    ar & BOOST_SERIALIZATION_NVP(_isCurrentlyMastitisInfected); \
    ar & BOOST_SERIALIZATION_NVP(_isCurrentlyClinicalMastitisInfected); \

    IMPLEMENT_SERIALIZE_STD_METHODS(DairyUdder);

    DairyUdder::DairyUdder(Mammal* pMammal, unsigned int dairyQuarterNumber, const boost::gregorian::date &date) : Udder (pMammal, dairyQuarterNumber)
    {
        _ripeToMastitis = false;
        _endMilkWaitingDate = pMammal->getBirthDate(); // No waiting date for milk for the moment

        // current infection map initialization
        for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++)
        {
            _isCurrentlyMastitisInfected[(FunctionalEnumerations::Health::BacteriumType)iBact] = false;;
            _isCurrentlyClinicalMastitisInfected[(FunctionalEnumerations::Health::BacteriumType)iBact] = false;;
        }
        setClinicalMastitisSinceLactationBegin(false);

        for (unsigned int i = 0; i < dairyQuarterNumber; i++)
        {
            _vpIsComposedByQuarters.push_back(new DairyQuarter(this, i, date));
        }
    }

    DairyUdder::~DairyUdder ()
    {
    }

    DairyMammal* DairyUdder::getDairyMammal()
    {
        return (DairyMammal*)_pIsInMammal;
    }
        
    DairyHerd* DairyUdder::getDairyHerd()
    {
        return getDairyMammal()->getDairyHerd();
    }

    DairyQuarter* DairyUdder::getDairyQuarter(std::vector<Quarter*>::iterator &it)
    {
        return (DairyQuarter*) (*it);
    }

    DairyQuarter* DairyUdder::getDairyQuarter(unsigned int ind)
    {
        return (DairyQuarter*) _vpIsComposedByQuarters[ind];
    }

    void DairyUdder::ensureIsNowMastitisSensitive(const boost::gregorian::date &date)
    {
        if (!_ripeToMastitis)
        {
            // Not mastitis sensitive yet, all quarters must be now
            for (std::vector<Quarter*>::iterator it = _vpIsComposedByQuarters.begin(); it != _vpIsComposedByQuarters.end(); it++)
            {
                getDairyQuarter(it)->mastitisUnsensitiveIsNowSensitive(date);
            }
            _ripeToMastitis = true;
        }
    }

    bool DairyUdder::G1SeverityMastitisIsDetected()
    {
        return getDairyMammal()->G1SeverityMastitisIsDetected();
    }

    void DairyUdder::progress(const boost::gregorian::date &simDate
#ifdef _LOG
                            , Results::DayAnimalLogInformations &log
#endif // _LOG
                            , Tools::Random &random)
    {
        Udder::progress(simDate
#ifdef _LOG
                            , log
#endif // _LOG
                            , random);

        if (getDairyMammal()->needToStudyDairyUdderHealth())
        {
            // Dairy Udder Health actions
            // --------------------------
            // Update the mastitis occurence probability of the day
            getDairyMammal()->updateDayMastitisOccurenceProbability(_dayMastitisOccurenceProbability, simDate);

            _mastitisInfectiousStatesChanges = false;

            for (unsigned int i = 0; i < _vpIsComposedByQuarters.size(); i++)
            {
                _vpIsComposedByQuarters[i]->progress(simDate, random);
#ifdef _LOG
                if (i > 0) log.mastitisInfection += Tools::toString("> <");
                log.mastitisInfection += getDairyQuarter(i)->getStrCurrentMastitisDiagnostic();
            }
#else // _LOG
            }
#endif // _LOG

            // Dairy quarter loss ?
            for (std::vector<Quarter*>::iterator it = _vpIsComposedByQuarters.begin(); it != _vpIsComposedByQuarters.end() and _mpJustLostDairyQuarters.size()>0;)
            {
                std::map<DairyQuarter*, DairyQuarter*>::iterator itLost = _mpJustLostDairyQuarters.find(getDairyQuarter(it));
                if (itLost != _mpJustLostDairyQuarters.end())
                {
                    delete *it; // We delete the non functional dairy quarter
                    _vpIsComposedByQuarters.erase(it); // remove from the fonctional quarter list
                    _mpJustLostDairyQuarters.erase(itLost);
                }
                else
                {
                    it++;
                }
            }

            // May be some state changes ?
            if (_mastitisInfectiousStatesChanges)
            {
                updateCurrentInfections();
            }
#ifdef _LOG
            log.clinicalMastitisSinceLactationBegin = getClinicalMastitisSinceLactationBegin();
#endif // _LOG
        }
    }

    double DairyUdder::getDairyQuarterMastitisOccurenceProbability(FunctionalEnumerations::Health::BacteriumType bacterium)
    {
        return (_dayMastitisOccurenceProbability.find(bacterium)->second)/_vpIsComposedByQuarters.size();            
    }

    FunctionalEnumerations::Health::MastitisSeverity DairyUdder::getRandomMastitisSeverity(FunctionalEnumerations::Health::BacteriumType bacterium)
    {
        return getDairyMammal()->getRandomMastitisSeverity(bacterium);            
    }

    void DairyUdder::updateCurrentInfections()
    {
//        std::cout << "DairyUdder::updateCurrentInfections, lactation stage = " << ((DairyMammal*)_pIsInMammal)->getLactationStage() << std::endl;
        // current infection map update, depending on each quartiers
        _isCurrentlyG1ToG3MastitisInfected = false;
        for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++)
        {
            bool infected = false;
            bool clinicalInfected = false;
            for (unsigned int iQ = 0; iQ < _vpIsComposedByQuarters.size() and !clinicalInfected; iQ++)
            {
                DairyQuarter* pQ = getDairyQuarter(iQ);
                infected = pQ->isInfected((FunctionalEnumerations::Health::BacteriumType)iBact, false);
                clinicalInfected = pQ->isInfected((FunctionalEnumerations::Health::BacteriumType)iBact, true);
                if (not _isCurrentlyG1ToG3MastitisInfected and pQ->isG1ToG3MastitisInfected((FunctionalEnumerations::Health::BacteriumType)iBact)) _isCurrentlyG1ToG3MastitisInfected = true;
                updateClinicalMastitisSinceLactationBegin(clinicalInfected);
            }
            _isCurrentlyMastitisInfected[(FunctionalEnumerations::Health::BacteriumType)iBact] = infected;
            _isCurrentlyClinicalMastitisInfected[(FunctionalEnumerations::Health::BacteriumType)iBact] = clinicalInfected;
        }
    }

    bool DairyUdder::isCurrentlyInfectedByMastitisBacterium(FunctionalEnumerations::Health::BacteriumType bacterium)
    {
        return _isCurrentlyMastitisInfected.find(bacterium)->second;
    }
    
    bool DairyUdder::isCurrentlyG1ToG3MastitisInfected()
    {
        return _isCurrentlyG1ToG3MastitisInfected;
    }
    
    void DairyUdder::notifyMastitisInfectionStateChanges()
    {
        _mastitisInfectiousStatesChanges = true;
    }

    void DairyUdder::notifyNewMastitisOccurence(FunctionalEnumerations::Health::BacteriumType bacterium, const boost::gregorian::date &date, bool newCase, bool newSubclinical, bool newClinical)
    {
        // Inform the mammal of the new mastitis occurence
        getDairyMammal()->notifyNewMastitisOccurence(bacterium, date, newCase, newSubclinical, newClinical);
    }

    void DairyUdder::getRealDairyQuartersProduct(ExchangeInfoStructures::MilkProductCharacteristic &baseMilkProduction, Tools::Random &random, const boost::gregorian::date &theDate, unsigned int lactationRank, int lactationStage, std::vector<ExchangeInfoStructures::MilkProductCharacteristic> &vDairyQuarterMp, bool &mustWaitToDeliver)
    {
        float realUdderMilkQuantity = 0.0f;

        // For each functional quarter, the product is increased if some quarters are lost
        float otherDairyQuarterLossFactor = 1.0f + (_originQuarterNumber - _vpIsComposedByQuarters.size()) * FunctionalConstants::Lactation::MILK_PRODUCT_INCREASE_PERCENT_FOR_OTHER_QUARTERS_AFTER_QUARTER_LOSS;
        float baseDairyQuarterMilkQuantity = (baseMilkProduction.quantity * otherDairyQuarterLossFactor) / _originQuarterNumber; // Individual base quantity

        // New real quantity calculation
        float overDairyQuarterReduction = 0.0f;
        unsigned int overDairyQuarterReductionOccurNumber = 0;
        for (unsigned int iDairyQuarter = 0; iDairyQuarter < _vpIsComposedByQuarters.size(); iDairyQuarter++)
        {
            ExchangeInfoStructures::MilkProductCharacteristic dairyQuarterMilkProduction = baseMilkProduction;

            // With potential mastitis affects :
            float hightMastitisEffectFactor = getDairyQuarter(iDairyQuarter)->getHightEffectMastitisMilkProductReduction(theDate);

            if (hightMastitisEffectFactor != 0.0f)
            {
                // Hight mastitis effect
                float milkProductFactor = 1.0f - hightMastitisEffectFactor;
                if (milkProductFactor < 0.0f)
                {
                    // It may occurs when a reduction is over 100% for the dairy quarter, so no product for it
                    overDairyQuarterReduction -= milkProductFactor;
                    overDairyQuarterReductionOccurNumber++;
                    dairyQuarterMilkProduction.quantity = 0.0f;
                }
                else
                {
                    dairyQuarterMilkProduction.SCC += getDairyQuarter(iDairyQuarter)->getMastitisAdditionalSCC(random);
                    dairyQuarterMilkProduction.quantity = baseDairyQuarterMilkQuantity * milkProductFactor;
                }
            }
            else
            {
                // No hight mastitis effect, so milk loss calculation just based on the SCC
                dairyQuarterMilkProduction.SCC += getDairyQuarter(iDairyQuarter)->getMastitisAdditionalSCC(random);
                float dairyQuarterQuantityLoss = Lactation::LactationManagement::getSCCMilkProductReductionQuantity(dairyQuarterMilkProduction.SCC, lactationRank, lactationStage)/_originQuarterNumber;
                dairyQuarterMilkProduction.quantity = dairyQuarterQuantityLoss < baseDairyQuarterMilkQuantity ? baseDairyQuarterMilkQuantity - dairyQuarterQuantityLoss : 0.0f;
            }
            vDairyQuarterMp.push_back(dairyQuarterMilkProduction);
            realUdderMilkQuantity += dairyQuarterMilkProduction.quantity;
        }
        
        // If some dairy quarter had a reduction over 100%
        float milkLossQuantityDueMastitis = 0.0f;
        if (overDairyQuarterReduction > 0.0f)
        {
            realUdderMilkQuantity = 0.0f;
            float mpCorrection = (1.0f - overDairyQuarterReduction / (_originQuarterNumber - overDairyQuarterReductionOccurNumber));
            for (unsigned int i = 0; i < vDairyQuarterMp.size(); i++)
            {
                ExchangeInfoStructures::MilkProductCharacteristic &qmp = vDairyQuarterMp[i];
                qmp.quantity *= mpCorrection;
                realUdderMilkQuantity += qmp.quantity;
            }
            milkLossQuantityDueMastitis = (baseMilkProduction.quantity - realUdderMilkQuantity) * FunctionalConstants::Health::MASTITIS_MILK_LOSS_FACTOR_DUE_QUARTER_LOSS;        
        }
        else
        {
            milkLossQuantityDueMastitis = baseMilkProduction.quantity - realUdderMilkQuantity;        
        }
        getDairyMammal()->setMilkLossQuantityDueMastitis(milkLossQuantityDueMastitis);

        // Update real production quantity
        baseMilkProduction.quantity = realUdderMilkQuantity;
        
        // Current wait period for milk delivery ?
        mustWaitToDeliver = theDate < _endMilkWaitingDate;
    }

    void DairyUdder::notifyDairyQuarterIsLostNow(DairyQuarter* pDairyQuarter)
    {
        _mpJustLostDairyQuarters[pDairyQuarter] = pDairyQuarter;
    }

    void DairyUdder::notifyAnimalDiesNowBecauseOfMastitis(
#ifdef _LOG
                      const boost::gregorian::date &date
#endif // _LOG            
                            )
    {
        getDairyMammal()->dies  (
#ifdef _LOG
                                date,
#endif // _LOG            
                                FunctionalEnumerations::Population::DeathReason::mastitisDeathReason);
    }

    FunctionalEnumerations::Health::MastitisRiskPeriodType DairyUdder::getCurrentMastitisRiskPeriodType()
    {
        return getDairyMammal()->getCurrentMastitisRiskPeriodState()->getMastitisRiskPeriodType();
    }

   ExchangeInfoStructures::Parameters::TreatmentTypeParameters &DairyUdder::takeCurrentMastitisTreatmentType(
#ifdef _LOG
                                                                                                                const boost::gregorian::date &date, 
#endif // _LOG
                                                                                                                FunctionalEnumerations::Health::BacteriumType bacterium,
                                                                                                                FunctionalEnumerations::Health::MastitisSeverity severity)
    {
       return getDairyMammal()->takeCurrentMastitisTreatmentType(
#ifdef _LOG
                                                                 date, 
#endif // _LOG
                                                                 bacterium, severity);
    }

    void DairyUdder::updateEndMilkWaitingDate(const boost::gregorian::date &date)
    {
        if (date > _endMilkWaitingDate)
        {
            _endMilkWaitingDate = date;
        }           
    }

    void DairyUdder::notifyMammalIsCulledForSevereMastitis(const boost::gregorian::date &date)
    {
        getDairyMammal()->cullingIsDecided(FunctionalEnumerations::Population::CullingStatus::toCullDueToMastitis, true, date);
    }

    float DairyUdder::getCurrentMastitisPreventionFactor()
    {
        return getDairyMammal()->getCurrentMastitisPreventionFactor();
    }

    bool DairyUdder::isCurrentlyMastitisInfected(bool onlyClinical)
    {
        bool infected = false;
        std::map<FunctionalEnumerations::Health::BacteriumType, bool> *pToScan = onlyClinical ? &_isCurrentlyClinicalMastitisInfected : &_isCurrentlyMastitisInfected;
        for (std::map<FunctionalEnumerations::Health::BacteriumType, bool>::iterator it = pToScan->begin(); it != pToScan->begin() and not infected; it++)
        {
            infected = it->second;
        }
        return infected;
    }
    
    void DairyUdder::notifyMastitisTreatmentToAllDairyQuarters(const boost::gregorian::date &simDate, Tools::Random &random)
    {
        for (unsigned int i = 0; i < _vpIsComposedByQuarters.size(); i++)
        {
            ((DairyQuarter*)_vpIsComposedByQuarters[i])->takeIntoAccountMastitisTreatment(simDate, random);
        }
    }

    ExchangeInfoStructures::Parameters::TreatmentTypeParameters &DairyUdder::getMastitisTreatmentType(FunctionalEnumerations::Health::BacteriumType bacterium)
    {
        return getDairyMammal()->getMastitisTreatmentType(bacterium);
    }

    void DairyUdder::getKetosisEffectOnMilkProduction(const boost::gregorian::date &date, ExchangeInfoStructures::MilkProductCharacteristic &mc)
    {
        return getDairyMammal()->getKetosisEffectOnMilkProduction(date, mc);
    }
    
    void DairyUdder::setClinicalMastitisSinceLactationBegin(bool val)
    {
        _hadClinicalMastitisSinceLactationBegin = val;
    }
    
    void DairyUdder::updateClinicalMastitisSinceLactationBegin(bool val)
    {
        if (not _hadClinicalMastitisSinceLactationBegin and val) _hadClinicalMastitisSinceLactationBegin = true;
    }
    
    bool DairyUdder::getClinicalMastitisSinceLactationBegin()
    {
        return _hadClinicalMastitisSinceLactationBegin;
    }

#ifdef _LOG
    void DairyUdder::addMiscellaneousLog(const boost::gregorian::date &simDate, std::string log)
    {
        _pIsInMammal->getDataLog().miscellaneous += log + " - ";
    }
    
    void DairyUdder::getStrCurrentMastitisDiagnostic(std::string &diag)
    {
        for (unsigned int i = 0; i < _vpIsComposedByQuarters.size(); i++)
        {
            diag += "<";
            diag += getDairyQuarter(i)->getStrCurrentMastitisDiagnostic();
            diag += ">";
        }
    }

    unsigned int DairyUdder::getDayOfTheMastitisRiskReferenceCycle(const boost::gregorian::date &date)
    {
        return getDairyMammal()->getDayOfTheMastitisRiskReferenceCycle(date);
    }

#endif // _LOG

}