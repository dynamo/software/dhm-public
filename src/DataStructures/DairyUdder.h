#ifndef DataStructures_DairyUdder_h
#define DataStructures_DairyUdder_h

// project
#include "../Tools/Serialization.h"
#include "../Tools/Random.h"
#include "../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "../ExchangeInfoStructures/MilkProductCharacteristic.h"
#include "../ExchangeInfoStructures/TreatmentTypeParameters.h"
#include "../Lactation/I_UdderMilkModulation.h"
#include "Udder.h"

namespace DataStructures
{
    class Mammal;
    class DairyQuarter;
    class DairyMammal;
    class DairyHerd;

    /// Udder able to produce large quantities of milk
    class DairyUdder :  public Udder,
                        public Lactation::I_UdderMilkModulation
    {
    private:

        bool _ripeToMastitis;
        boost::gregorian::date _endMilkWaitingDate;
        std::map<FunctionalEnumerations::Health::BacteriumType, bool> _isCurrentlyMastitisInfected;
        std::map<FunctionalEnumerations::Health::BacteriumType, bool> _isCurrentlyClinicalMastitisInfected;
        bool _hadClinicalMastitisSinceLactationBegin = false;
        bool _isCurrentlyG1ToG3MastitisInfected = false;;

        DECLARE_SERIALIZE_STD_METHODS;

        // Simulation data
        bool _mastitisInfectiousStatesChanges;
        std::map<DairyQuarter*,DairyQuarter*> _mpJustLostDairyQuarters;
        std::map<FunctionalEnumerations::Health::BacteriumType, double> _dayMastitisOccurenceProbability;

    private:
        DairyMammal* getDairyMammal();
        DairyQuarter* getDairyQuarter(std::vector<Quarter*>::iterator &it);
        DairyQuarter* getDairyQuarter(unsigned int ind);

    protected:
        DairyUdder(){} // For serialization
        void concrete(){}; // To allow instanciation

    public:
        DairyUdder(Mammal* pMammal, unsigned int dairyQuarterNumber, const boost::gregorian::date &date);
        virtual ~DairyUdder (); // Destructor

        DairyHerd* getDairyHerd();


        void progress(const boost::gregorian::date &simDate
#ifdef _LOG
                            , Results::DayAnimalLogInformations &log
#endif // _LOG
                            , Tools::Random &random) override;
        void ensureIsNowMastitisSensitive(const boost::gregorian::date &date);
        double getDairyQuarterMastitisOccurenceProbability(FunctionalEnumerations::Health::BacteriumType bacterium);
        FunctionalEnumerations::Health::MastitisSeverity getRandomMastitisSeverity(FunctionalEnumerations::Health::BacteriumType bacterium);
        void updateCurrentInfections();
        bool isCurrentlyInfectedByMastitisBacterium(FunctionalEnumerations::Health::BacteriumType bacterium);
        bool isCurrentlyG1ToG3MastitisInfected();
        void getRealDairyQuartersProduct(ExchangeInfoStructures::MilkProductCharacteristic &baseMilkProduction, Tools::Random &random, const boost::gregorian::date &theDate, unsigned int lactationRank, int lactationStage, std::vector<ExchangeInfoStructures::MilkProductCharacteristic> &vDairyQuarterMp, bool &mustWaitToDeliver);
        void notifyMastitisInfectionStateChanges();
        bool G1SeverityMastitisIsDetected();
        void notifyNewMastitisOccurence(FunctionalEnumerations::Health::BacteriumType bacterium, const boost::gregorian::date &date, bool newCase, bool newSubclinical, bool newClinical);
        void notifyDairyQuarterIsLostNow(DairyQuarter* pDairyQuarter);
        void notifyAnimalDiesNowBecauseOfMastitis(
#ifdef _LOG
                                                const boost::gregorian::date &date
#endif // _LOG            
                                                );
        FunctionalEnumerations::Health::MastitisRiskPeriodType getCurrentMastitisRiskPeriodType();
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &takeCurrentMastitisTreatmentType(
#ifdef _LOG
                                                                                                                const boost::gregorian::date &date, 
#endif // _LOG
                                                                                                                FunctionalEnumerations::Health::BacteriumType bacterium,
                                                                                                                FunctionalEnumerations::Health::MastitisSeverity severity);
        void updateEndMilkWaitingDate(const boost::gregorian::date &date);
        void notifyMammalIsCulledForSevereMastitis(const boost::gregorian::date &date);
        float getCurrentMastitisPreventionFactor();
        bool isCurrentlyMastitisInfected(bool onlyClinical);
        void notifyMastitisTreatmentToAllDairyQuarters(const boost::gregorian::date &simDate, Tools::Random &random);
        ExchangeInfoStructures::Parameters::TreatmentTypeParameters &getMastitisTreatmentType(FunctionalEnumerations::Health::BacteriumType bacterium);

        void getKetosisEffectOnMilkProduction(const boost::gregorian::date &date, ExchangeInfoStructures::MilkProductCharacteristic &mc);
        void setClinicalMastitisSinceLactationBegin(bool val);
        void updateClinicalMastitisSinceLactationBegin(bool val);
        bool getClinicalMastitisSinceLactationBegin();

#ifdef _LOG
        void addMiscellaneousLog(const boost::gregorian::date &simDate, std::string log);
        unsigned int getDayOfTheMastitisRiskReferenceCycle(const boost::gregorian::date &date);
        void getStrCurrentMastitisDiagnostic(std::string &diag);
#endif // _LOG
    };
}
#endif //DataStructures_DairyUdder_h