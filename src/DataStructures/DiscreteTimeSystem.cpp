////////////////////////////////////////
//                                    //
// File : DiscreteTimeSystem.cpp      //
//                                    //
// Author : Philippe Gontier          //
//                                    //
// Version 1.0                        //
//                                    //
// Date : 10/03/2022                  //
//                                    //
////////////////////////////////////////

#include "DiscreteTimeSystem.h"

// project
#include "../ExchangeInfoStructures/DairyFarmAccountingModelParameters.h"
#include "../ResultStructures/Result.h"

BOOST_CLASS_EXPORT(DataStructures::DiscreteTimeSystem) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(SystemToSimulate); \
    ar & BOOST_SERIALIZATION_NVP(_currentSimulationDate); \

    IMPLEMENT_SERIALIZE_STD_METHODS(DiscreteTimeSystem);
    
    DiscreteTimeSystem::DiscreteTimeSystem(ExchangeInfoStructures::Parameters::DairyFarmParameters &info) : SystemToSimulate(info)
    {
    }
      
    DiscreteTimeSystem::~DiscreteTimeSystem()
    {
    }
    
    void DiscreteTimeSystem::action()
    {
        // Init simulation
#ifdef _LOG
        if (_presimulationHasToBeDone)
        {
            std::cout << "presimulation has to be done" << std::endl;
#endif // _LOG
            _currentSimulationDate = _beginPresimulationDate;
#ifdef _LOG
        }
        else
        {
            std::cout << "no presimulation has to be done, _currentSimulationDate = " << _currentSimulationDate << std::endl;
        }
#endif // _LOG
 
#ifdef _LOG
        _pCurrentResults = _pSimulationResults;
#else
        _pCurrentResults = _pPresimulationResults;
#endif // _LOG

        // Do simulation
        while (_currentSimulationDate <= _endSimulationDate)
        {
            // update temporal data of the results
            // +-------------------------------------------+
            // | Here are the treatments for each lap time |
            // +-------------------------------------------+
            if (_currentSimulationDate == _beginResultDate)
            {
                if (_presimulationHasToBeDone)
                {
#ifndef _LOG
                    // It is time to store results
                   _pCurrentResults = _pSimulationResults;
                    delete _pPresimulationResults;
                    _pPresimulationResults = nullptr;
#else
                    // Provisoire : Save intermediate situation
                    std::string strSaveSituationFileName = "D:/Simulateur/ForgeMIA/dhm-dev/data/BufferizedRuns/irun" + Tools::toString(getNum());
                    saveData(strSaveSituationFileName);      
#endif // _LOG
                }
                resultBegins(_currentSimulationDate);
            }

            // Calculate here the effects of a cycle of time
            progress(_currentSimulationDate); // Make the object progressing
            // +-------------------------------------------+
            _currentSimulationDate = _currentSimulationDate + _timeLapsduration;
        } // for currentTimeLapsIndex
        
#ifdef _LOG
        // Provisoire : Save final situation
        std::string strSaveSituationFileName = "D:/Simulateur/ForgeMIA/dhm-dev/data/BufferizedRuns/frun" + Tools::toString(getNum());
//        saveData(strSaveSituationFileName);
#endif // _LOG

    }
        
    void DiscreteTimeSystem::progress(const boost::gregorian::date &simDate)
    {    
        // Accounting model progress ?
        if (simDate.day() == 1 and simDate > _beginSimulationDate)
        {
            // First day of the month, need to actualize prices regarding their tendency ?
            _currentEvoluatingAccountingModel.actualizePricesForNewMonth();
        }
    }
    
}