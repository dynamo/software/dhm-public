////////////////////////////////////////
//                                    //
// File : DiscreteTimeSystem.h        //
//                                    //
// Author : Philippe Gontier          //
//                                    //
// Version 1.0                        //
//                                    //
// Date : 10/03/2022                  //
//                                    //
////////////////////////////////////////

#ifndef DataStructures_DiscreteTimeSystem_h
#define DataStructures_DiscreteTimeSystem_h

// project
#include "./SystemToSimulate.h"

namespace DataStructures
{
    /// Generic Farm 
    class DiscreteTimeSystem : public SystemToSimulate
    {
        DECLARE_SERIALIZE_STD_METHODS;
        
        boost::gregorian::date _currentSimulationDate; // Current begin date for the simulation

    protected:
        DiscreteTimeSystem(){} // serialization constructor
    
    public:
        virtual ~DiscreteTimeSystem();
        DiscreteTimeSystem(ExchangeInfoStructures::Parameters::DairyFarmParameters &info);
        virtual void displayInitialSystemInformations(){}
        virtual void action();
        virtual void progress(const boost::gregorian::date &simDate); // progress of duration time and unit
        virtual void resultBegins(const boost::gregorian::date &date){} // If set in mandatored when result begins
#ifdef _LOG
    virtual void saveData(const std::string &fileName) = 0;
#endif // _LOG       



    }; /* End of class DiscreteTimeSystem */
} /* End of namespace DataStructures */
#endif // DataStructures_DiscreteTimeSystem_h
