#include <vector>

#include "Foots.h"

// Project
#include "Animal.h"
#include "Herd.h"
#include "DairyFarm.h"
#include "DairyHerd.h"
#include "DairyMammal.h"
#include "Population/Batch/Batch.h"
#include "../Health/Diseases/Lameness/LamenessStates/G0UnsensitiveLamenessState.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"

#ifdef _LOG
#include "../ResultStructures/DairyFarmResult.h"
#endif // _LOG

BOOST_CLASS_EXPORT(DataStructures::Foots) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(_pIsInAnimal); \
    ar & BOOST_SERIALIZATION_NVP(_pHasCurrentLamenessStates); \
    ar & BOOST_SERIALIZATION_NVP(_footBathEndProtectionDate); \
    ar & BOOST_SERIALIZATION_NVP(_trimmingEndInfectiousProtectionDate); \
    ar & BOOST_SERIALIZATION_NVP(_trimmingEndNonInfectiousProtectionDate); \

    IMPLEMENT_SERIALIZE_STD_METHODS(Foots);

    Foots::Foots(Animal* pAnimal, const boost::gregorian::date &date)
    {
        _pIsInAnimal = pAnimal;
        
        // Protection end dates (defaut = birth date to avoid not_a_date tests)
        _footBathEndProtectionDate = pAnimal->getBirthDate();
        _trimmingEndInfectiousProtectionDate = pAnimal->getBirthDate();
        _trimmingEndNonInfectiousProtectionDate = pAnimal->getBirthDate();
        
        // Lameness State
        _pHasCurrentLamenessStates[FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType] = new Health::Lameness::LamenessStates::G0UnsensitiveLamenessState(date, this, FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType, INT_MAX);
        _pHasCurrentLamenessStates[FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType] = new Health::Lameness::LamenessStates::G0UnsensitiveLamenessState(date, this, FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType, INT_MAX);
    }

    Foots::~Foots()
    {
        _pIsInAnimal = nullptr;
        
        for (auto itLs : _pHasCurrentLamenessStates)
        {
            delete (itLs.second);
        }
        _pHasCurrentLamenessStates.clear();
    }

    void Foots::progress(const boost::gregorian::date &simDate)
    {
#ifdef _LOG
        Results::DayAnimalLogInformations &log = _pIsInAnimal->getDataLog();
#endif // _LOG            
        
        // Foot bath new effect
        if (getBatch()->hasEfficientFootBathToday(simDate))
        {
            // Preventive effect
            _footBathEndProtectionDate = simDate + boost::gregorian::date_duration(getDairyMammal()->getDairyFarm()->getFootBathProtectionDurationForLamenessInDays());
            
#ifdef _LOG
        getDairyMammal()->addMiscellaneousLog(simDate, "Foot bath use"); 
#endif // _LOG            
        }
        
        for (auto itHCLS : _pHasCurrentLamenessStates)
        {
#ifdef _LOG
            bool wasLamenessIll = (itHCLS.second)->isIll();
#endif // _LOG            
            Health::Lameness::LamenessStates::LamenessState* pNewLamenessState = nullptr;
            (itHCLS.second)->progress(simDate, _pIsInAnimal->getDairyFarm()->getRandom(), pNewLamenessState
#ifdef _LOG
                                                          , log
#endif // _LOG            
                                                          );
            setCurrentLamenessState(itHCLS.first, pNewLamenessState);
#ifdef _LOG
            Health::Lameness::LamenessStates::LamenessState* pCurrentState = _pHasCurrentLamenessStates.find(itHCLS.first)->second;
            if (pCurrentState->isIll() and not wasLamenessIll)
            {
                // This is a new illness
                // New Lameness : Log this new occurence
                Results::LamenessResult lr;
                lr.id = getAnimalUniqueId();
                lr.date = simDate;
                lr.lamenessSeverity = pCurrentState->getSeverity();
                lr.lactationRank = getDairyMammal()->getLactationRank();
                lr.lactationStage = getDairyMammal()->getLactationStage();
                if (itHCLS.first == FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType) getCurrentResults()->getHealthResults().NonInfectiousLamenessResults.push_back(lr);
                else if (itHCLS.first == FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType) getCurrentResults()->getHealthResults().InfectiousLamenessResults.push_back(lr);
                else
                {
                    std::string errorMessage = "Unadequate LamenessInfectiousStateType : " + Tools::toString(itHCLS.first);
                    throw std::runtime_error(errorMessage);
                }
            }
            if (itHCLS.first == FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)
            {
                log.nonInfectiousLamenessState = pCurrentState->getStrLamenessState();
            }
            else
            {
                log.infectiousLamenessState = pCurrentState->getStrLamenessState();
            }
#endif // _LOG        
        }
        _isInfectiousLame = _pHasCurrentLamenessStates.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second->isIll();
        _isNonInfectiousLame = _pHasCurrentLamenessStates.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second->isIll();
    }
    
    void Foots::careIndividualTodayForLameness(const boost::gregorian::date &careDate)
    {
        bool isDetectedInfectiousLameness;
        bool isInfectiousG1Lameness;
        bool isInfectiousG2Lameness;
        bool isDetectedNonInfectiousLameness;
        bool isNonInfectiousG1Lameness;
        bool isNonInfectiousG2Lameness;
        isLamenessIll(isDetectedInfectiousLameness, isInfectiousG1Lameness, isInfectiousG2Lameness, isDetectedNonInfectiousLameness, isNonInfectiousG1Lameness, isNonInfectiousG2Lameness);
        bool infectious = isInfectiousG1Lameness or isInfectiousG2Lameness;
        bool nonInfectious = isNonInfectiousG1Lameness or isNonInfectiousG2Lameness;
        
        // Trimming
        if ((infectious and nonInfectious) or (not infectious and not nonInfectious))
        {      
            // Trimming for two kind of lammeness (or none because of healing), half infectious and half non infectious
            getDairyMammal()->addLamenessHealthTransaction(
#ifdef _LOG 
                            careDate, FunctionalConstants::AccountingModel::LAMENESS_COW_FOOT_INDIVIDUAL_CURATIVE_TRIMMING_TRANSACTION_TYPE, 
#endif // _LOG            
                            FunctionalConstants::AccountingModel::KEY_LAMENESS_COW_FOOT_TRIMMING_COST, getDairyMammal()->getDairyHerd()->getCurativeIndividualNonInfectiousLamenessCostsForUpdate(), 0.5f);
            getDairyMammal()->addLamenessHealthTransaction(
#ifdef _LOG 
                            careDate, FunctionalConstants::AccountingModel::LAMENESS_COW_FOOT_INDIVIDUAL_CURATIVE_TRIMMING_TRANSACTION_TYPE, 
#endif // _LOG            
                            FunctionalConstants::AccountingModel::KEY_LAMENESS_COW_FOOT_TRIMMING_COST, getDairyMammal()->getDairyHerd()->getCurativeIndividualInfectiousLamenessCostsForUpdate(), 0.5f);
        }
        else if (nonInfectious)
        {
            getDairyMammal()->addLamenessHealthTransaction(
#ifdef _LOG 
                            careDate, FunctionalConstants::AccountingModel::LAMENESS_COW_FOOT_INDIVIDUAL_CURATIVE_TRIMMING_TRANSACTION_TYPE, 
#endif // _LOG            
                            FunctionalConstants::AccountingModel::KEY_LAMENESS_COW_FOOT_TRIMMING_COST, getDairyMammal()->getDairyHerd()->getCurativeIndividualNonInfectiousLamenessCostsForUpdate());
        }
        else // (infectious)
        {
            getDairyMammal()->addLamenessHealthTransaction(
#ifdef _LOG 
                            careDate, FunctionalConstants::AccountingModel::LAMENESS_COW_FOOT_INDIVIDUAL_CURATIVE_TRIMMING_TRANSACTION_TYPE, 
#endif // _LOG            
                            FunctionalConstants::AccountingModel::KEY_LAMENESS_COW_FOOT_TRIMMING_COST, getDairyMammal()->getDairyHerd()->getCurativeIndividualInfectiousLamenessCostsForUpdate());  
        }
        // Triming Effects         
        applyTrimmingProtectionEffects(careDate);
#ifdef _LOG
        // Trimming log
        std::string comment = "Curative individual trimming";
        getDairyMammal()->addMiscellaneousLog(careDate, comment); 
#endif // _LOG            
        
        // Treatment
        if (nonInfectious)
        {
            _pHasCurrentLamenessStates.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second->takeTreatment(careDate, getRandom());
            getDairyMammal()->addLamenessHealthTransaction(
#ifdef _LOG 
                                careDate, FunctionalConstants::AccountingModel::NON_INFECTIOUS_LAMENESS_COW_FOOT_INDIVIDUAL_TREATMENT_TRANSACTION_TYPE, 
#endif // _LOG            
                                FunctionalConstants::AccountingModel::KEY_NON_INFECTIOUS_LAMENESS_COW_FOOT_TREATMENT_COST, getDairyMammal()->getDairyHerd()->getCurativeIndividualNonInfectiousLamenessCostsForUpdate());
#ifdef _LOG
            // Traitement log
            std::string comment = "Curative individual non infectious lameness traitment";
            getDairyMammal()->addMiscellaneousLog(careDate, comment); 
#endif // _LOG            
        }
        if (infectious)
        {
            _pHasCurrentLamenessStates.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second->takeTreatment(careDate, getRandom());
            getDairyMammal()->addLamenessHealthTransaction(
#ifdef _LOG 
                                careDate, FunctionalConstants::AccountingModel::INFECTIOUS_LAMENESS_COW_FOOT_INDIVIDUAL_TREATMENT_TRANSACTION_TYPE, 
#endif // _LOG            
                                FunctionalConstants::AccountingModel::KEY_INFECTIOUS_LAMENESS_COW_FOOT_TREATMENT_COST, getDairyMammal()->getDairyHerd()->getCurativeIndividualInfectiousLamenessCostsForUpdate());
#ifdef _LOG
            // Traitement log
            std::string comment = "Curative individual infectious lameness traitment";
            getDairyMammal()->addMiscellaneousLog(careDate, comment); 
#endif // _LOG            
        }
 
        // Triming Effects         
        applyTrimmingProtectionEffects(careDate);
    }
    
    void Foots::trimCollectiveTodayForLamenessPrevention(const boost::gregorian::date &trimDate)
    {
        // Standard trimming
        getDairyMammal()->addLamenessHealthTransaction(
#ifdef _LOG 
                            trimDate, FunctionalConstants::AccountingModel::LAMENESS_COW_FOOT_COLLECTIVE_PREVENTIVE_TRIMMING_TRANSACTION_TYPE, 
#endif // _LOG            
                            FunctionalConstants::AccountingModel::KEY_LAMENESS_COW_FOOT_TRIMMING_COST, getDairyMammal()->getDairyHerd()->getPreventiveTrimmingCostsForUpdate());
        // Triming Effects         
        applyTrimmingProtectionEffects(trimDate);
#ifdef _LOG
        std::string comment = "Preventive collective trimming";
        getDairyMammal()->addMiscellaneousLog(trimDate, comment); 
#endif // _LOG            
              
        // Potential traitments
        bool isDetectedInfectiousLameness;
        bool isInfectiousG1Lameness;
        bool isInfectiousG2Lameness;
        bool isDetectedNonInfectiousLameness;
        bool isNonInfectiousG1Lameness;
        bool isNonInfectiousG2Lameness;
        isLamenessIll(isDetectedInfectiousLameness, isInfectiousG1Lameness, isInfectiousG2Lameness, isDetectedNonInfectiousLameness, isNonInfectiousG1Lameness, isNonInfectiousG2Lameness);
        
        // Traitment
        if ((isNonInfectiousG1Lameness or isNonInfectiousG2Lameness))
        {
            // Non infectious lameness case
            _pHasCurrentLamenessStates.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second->takeTreatment(trimDate, getRandom());
            getDairyMammal()->addLamenessHealthTransaction(
#ifdef _LOG 
                                trimDate, FunctionalConstants::AccountingModel::NON_INFECTIOUS_LAMENESS_COW_FOOT_COLLECTIVE_TREATMENT_TRANSACTION_TYPE, 
#endif // _LOG            
                                FunctionalConstants::AccountingModel::KEY_NON_INFECTIOUS_LAMENESS_COW_FOOT_TREATMENT_COST, getDairyMammal()->getDairyHerd()->getCurativeCollectiveNonInfectiousLamenessCostsForUpdate());
#ifdef _LOG
            // Traitement log
            std::string comment = "Curative collective non infectious lameness traitment";
            getDairyMammal()->addMiscellaneousLog(trimDate, comment); 
#endif // _LOG            
        }
        if ((isInfectiousG1Lameness or isInfectiousG2Lameness))
        {
            // Infectious lameness case, curative treatment
            _pHasCurrentLamenessStates.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second->takeTreatment(trimDate, getRandom());
            getDairyMammal()->addLamenessHealthTransaction(
#ifdef _LOG 
                                trimDate, FunctionalConstants::AccountingModel::INFECTIOUS_LAMENESS_COW_FOOT_COLLECTIVE_TREATMENT_TRANSACTION_TYPE, 
#endif // _LOG            
                                FunctionalConstants::AccountingModel::KEY_INFECTIOUS_LAMENESS_COW_FOOT_TREATMENT_COST, getDairyMammal()->getDairyHerd()->getCurativeCollectiveInfectiousLamenessCostsForUpdate());
#ifdef _LOG
            // Traitement log
            std::string comment = "Curative collective infectious lameness traitment";
            getDairyMammal()->addMiscellaneousLog(trimDate, comment); 
#endif // _LOG            
        }
    }
    
    void Foots::applyTrimmingProtectionEffects(const boost::gregorian::date &trimDate)
    {
       // Update trimming protection end date
        _trimmingEndInfectiousProtectionDate = trimDate + boost::gregorian::date_duration(FunctionalConstants::Health::TRIMMING_PREVENTION_INFECTIOUS_LAMENESS_INCIDENCE_DURATION_IN_DAYS); 
        _trimmingEndNonInfectiousProtectionDate = trimDate + boost::gregorian::date_duration(FunctionalConstants::Health::TRIMMING_PREVENTION_NON_INFECTIOUS_LAMENESS_INCIDENCE_DURATION_IN_DAYS);       
    }
    Tools::Random &Foots::getRandom()
    {
        return getDairyMammal()->getDairyFarm()->getRandom();
    }

    DairyMammal* Foots::getDairyMammal()
    {
        return (DairyMammal*)_pIsInAnimal;
    }
    
    Population::Batch* Foots::getBatch()
    {
        return _pIsInAnimal->getBatch();
    }

#ifdef _LOG
    unsigned int Foots::getUniqueIdForLameness()
    {
        return getDairyMammal()->getUniqueId();
    }
#endif // _LOG            

        
    float Foots::getYearInfectiousLamenessIncidence(FunctionalEnumerations::Health::LamenessInfectiousStateType lamenessInfectiousStatus)
    {
        return FunctionalConstants::Health::MEAN_YEAR_LAMENESS_BASIS_INCIDENCE.find(lamenessInfectiousStatus)->second;
    }

    float Foots::getLamenessOestrusDetectionFactor(FunctionalEnumerations::Reproduction::DetectionMode dm, bool first)
    {
        float res1 = _pHasCurrentLamenessStates.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second->getLamenessOestrusDetectionFactor(dm, first);
        float res2 = _pHasCurrentLamenessStates.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second->getLamenessOestrusDetectionFactor(dm, first);
        return res1 * res2;
    }
    
    void Foots::setCurrentLamenessState(FunctionalEnumerations::Health::LamenessInfectiousStateType LIST, Health::Lameness::LamenessStates::LamenessState* pState)
    {
        Health::Lameness::LamenessStates::LamenessState* pHasCurrentLamenessState = _pHasCurrentLamenessStates.find(LIST)->second;
        if (pState != pHasCurrentLamenessState and pState != nullptr)
        {
#ifdef _LOG
            assert (pHasCurrentLamenessState->getLamenessInfectiousStatus() == LIST);
            assert (LIST == pState->getLamenessInfectiousStatus());
#endif // _LOG    
            if (pHasCurrentLamenessState != nullptr)
            {
               delete pHasCurrentLamenessState;
            }
            _pHasCurrentLamenessStates[LIST] = pState;
        }
    }

#ifdef _LOG
    unsigned int Foots::getAnimalUniqueId()
    {
        return _pIsInAnimal->getUniqueId();
    }
    
    unsigned int Foots::getIdForLameness()
    {
        return getAnimalUniqueId();
    }
#endif // _LOG    
    
    Results::DairyFarmResult* Foots::getCurrentResults()
    {
        return _pIsInAnimal->getCurrentResults();
    }
    
    void Foots::becomeLamenessSensitive(const boost::gregorian::date &date)
    {
        // Is now sensible to lameness
        for (auto itLamenessState : _pHasCurrentLamenessStates)
        {
            Health::Lameness::LamenessStates::LamenessState* pNewState = nullptr;
            (itLamenessState.second)->becomeSensitive(date, pNewState);
            if (pNewState != nullptr)
            {
                setCurrentLamenessState(itLamenessState.first, pNewState);
            }
        }
    }
    
    void Foots::isLamenessIll(bool &isDetectedInfectiousLameness, bool &isInfectiousG1Lameness, bool &isInfectiousG2Lameness, bool &isDetectedNonInfectiousLameness, bool &isNonInfectiousG1Lameness, bool &isNonInfectiousG2Lameness)
    {
        _pHasCurrentLamenessStates.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second->isIll(isInfectiousG1Lameness, isInfectiousG2Lameness, isDetectedInfectiousLameness);
        _pHasCurrentLamenessStates.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second->isIll(isNonInfectiousG1Lameness, isNonInfectiousG2Lameness, isDetectedNonInfectiousLameness);
    }
    
    bool Foots::hadRecentTrimming(const boost::gregorian::date &date)
    {
        return date < _trimmingEndNonInfectiousProtectionDate;
    }

    bool Foots::hasCurrentLamenesCases()
    {
        bool isDetectedInfectiousLameness;
        bool isInfectiousG1Lameness;
        bool isInfectiousG2Lameness;
        bool isDetectedNonInfectiousLameness;
        bool isNonInfectiousG1Lameness;
        bool isNonInfectiousG2Lameness;
        isLamenessIll(isDetectedInfectiousLameness, isInfectiousG1Lameness, isInfectiousG2Lameness, isDetectedNonInfectiousLameness, isNonInfectiousG1Lameness, isNonInfectiousG2Lameness);
        return isInfectiousG1Lameness or isInfectiousG2Lameness or isNonInfectiousG1Lameness or isNonInfectiousG2Lameness;
    }
    
    float Foots::getMastitisRiskDueToLameness()
    {
        float IMastitisRiskFactor = _pHasCurrentLamenessStates.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second->getMastitisRiskFactor();
        float NIMastitisRiskFactor = _pHasCurrentLamenessStates.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second->getMastitisRiskFactor();
        return IMastitisRiskFactor > NIMastitisRiskFactor ? IMastitisRiskFactor : NIMastitisRiskFactor;
    }

    float Foots::getLamenessRisk(const boost::gregorian::date &simDate, FunctionalEnumerations::Health::LamenessInfectiousStateType infectiousType)
    {
        // Animal risk :
        // -------------
        float risk = getDairyMammal()->getLamenessRisk(simDate, infectiousType);
        
//        // Maintenance
//        if (infectiousType == FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)
//        {
//            std::cout << risk << std::endl;
//        }
//

        // Foot prevention modulation :
        // ----------------------------

        // Foot bath protection ?
        if (infectiousType == FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType and simDate <= _footBathEndProtectionDate)
        {
            risk *= FunctionalConstants::Health::FOOT_BATH_PREVENTION_INFECTIOUS_LAMENESS_INCIDENCE_FACTOR;
        }

        // Trimming protection ?
        if (infectiousType == FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType and simDate <= _trimmingEndInfectiousProtectionDate)
        {
            risk *= FunctionalConstants::Health::TRIMMING_PREVENTION_INFECTIOUS_LAMENESS_INCIDENCE_FACTOR;
        }
        if (infectiousType == FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType and simDate <= _trimmingEndNonInfectiousProtectionDate)
        {
            risk *= FunctionalConstants::Health::TRIMMING_PREVENTION_NON_INFECTIOUS_LAMENESS_INCIDENCE_FACTOR;
        }
        return risk;
    }
    
    void Foots::notifyForLamenessSporadicDetectionOrVerificationToday(
#ifdef _LOG                
                                                    const boost::gregorian::date &date
#endif // _LOG                
                                                    )
    {
        getDairyMammal()->notifyForLamenessSporadicDetectionOrVerificationToday(
#ifdef _LOG                
                                                    date
#endif // _LOG                
                                                    );
    }
    
    unsigned int Foots::getCurrentDurationLamenessIll(const boost::gregorian::date &date, FunctionalEnumerations::Health::LamenessInfectiousStateType list)
    {
        return _pHasCurrentLamenessStates.find(list)->second->getCurrentDurationLamenessIll(date);
    }
            
    bool Foots::hasCurrentlyEfficientFootbath(const boost::gregorian::date &date, float &dayRatio)
    {
        return getDairyMammal()->hasCurrentlyEfficientFootbath(date, dayRatio);
    }
   
    FunctionalEnumerations::Population::Location Foots::getCurrentLocation()
    {
        return getDairyMammal()->getCurrentLocation();
    }
    
    void Foots::diesNowBecauseOfLameness(
#ifdef _LOG                   
    const boost::gregorian::date &date, 
#endif // _LOG            
                                    FunctionalEnumerations::Population::DeathReason deathReason)
    {
        getDairyMammal()->diesNowBecauseOfLameness(
#ifdef _LOG                   
    date, 
#endif // _LOG            
                                    deathReason);
    }
    
    void Foots::notifyMammalIsCulledForSevereLameness(const boost::gregorian::date &date, FunctionalEnumerations::Population::CullingStatus cs)
    {
        getDairyMammal()->cullingIsDecided(cs, true, date);
    }
    
    void Foots::getLamenessG1InfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability)
    {
        getDairyMammal()->getDairyFarm()->getLamenessG1InfectiousTreatmentInformations(minEffectDelay, maxEffectDelay, firstCaseHealingProbability, otherCaseHealingProbability);
    }
    
    void Foots::getLamenessG1NonInfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability)
    {
        getDairyMammal()->getDairyFarm()->getLamenessG1NonInfectiousTreatmentInformations(minEffectDelay, maxEffectDelay, firstCaseHealingProbability, otherCaseHealingProbability);
    }
    
    void Foots::getLamenessG2InfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability)
    {
        getDairyMammal()->getDairyFarm()->getLamenessG2InfectiousTreatmentInformations(minEffectDelay, maxEffectDelay, firstCaseHealingProbability, otherCaseHealingProbability);
    }
    
    void Foots::getLamenessG2NonInfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability)
    {
        getDairyMammal()->getDairyFarm()->getLamenessG2NonInfectiousTreatmentInformations(minEffectDelay, maxEffectDelay, firstCaseHealingProbability, otherCaseHealingProbability);
    }
       
    FunctionalEnumerations::Health::LamenessDetectionMode Foots::getLamenessDetectionMode()
    {
        return getDairyMammal()->getLamenessDetectionMode();
    }

    void Foots::getLamenessEffectOnMilkProduction(const boost::gregorian::date &date, ExchangeInfoStructures::MilkProductCharacteristic &mc)
    {
        bool effectToApply = false;
        float quantityLamenessDelta = 0.0f;
        float TBLamenessDelta = 0.0f;
        float TPLamenessDelta = 0.0f;
        for (auto it : _pHasCurrentLamenessStates)
        {
            if ((it.second)->isIll())
            {
                effectToApply = true;
                (it.second)->getLamenessHighestMilkEffect(quantityLamenessDelta, TBLamenessDelta, TPLamenessDelta, getCurrentLocation());
            }
        }
        if (effectToApply)
        {
            mc.quantity += quantityLamenessDelta;
            mc.TB += TBLamenessDelta;
            mc.TP += TPLamenessDelta;        
        }
    }

    void Foots::notifyNewG1InfectiousLamenessOccurence(const boost::gregorian::date &date)
    {
        getDairyMammal()->getHerd()->notifyNewG1InfectiousLamenessOccurence(date);
    }
    
    void Foots::notifyNewG1NonInfectiousLamenessOccurence(const boost::gregorian::date &date)
    {
        getDairyMammal()->getHerd()->notifyNewG1NonInfectiousLamenessOccurence(date);
    }
    
    void Foots::notifyNewG2InfectiousLamenessOccurence(const boost::gregorian::date &date)
    {
        getDairyMammal()->getHerd()->notifyNewG2InfectiousLamenessOccurence(date);
    }
    
    void Foots::notifyNewG2NonInfectiousLamenessOccurence(const boost::gregorian::date &date)
    {
        getDairyMammal()->getHerd()->notifyNewG2NonInfectiousLamenessOccurence(date);
    }
}