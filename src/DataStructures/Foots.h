#ifndef DataStructures_Foots_h
#define DataStructures_Foots_h

// project
//#include "../Health/Diseases/Lameness/LamenessStates/I_LamenessProneFoots.h"
#include "../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "../Tools/Random.h"
#include "../Tools/Serialization.h"
#include "../ExchangeInfoStructures/MilkProductCharacteristic.h"

namespace Results
{
#ifdef _LOG
    struct DayAnimalLogInformations;
#endif // _LOG
    class DairyFarmResult;
}

namespace Health
{
    namespace Lameness
    {
        namespace LamenessStates
        {
            class LamenessState;
        }
    }
}

namespace DataStructures
{
    namespace Population
    {
        class Batch;
    }
    class Animal;
    class DairyMammal;

    /// Global foot notion for an animal
    class Foots // : public Health::Lameness::LamenessStates::I_LamenessProneFoots
    {
    private:

        DECLARE_SERIALIZE_STD_METHODS;
        
    protected:
        Animal* _pIsInAnimal = nullptr;
        
        // Lameness        
        boost::gregorian::date _footBathEndProtectionDate;
        boost::gregorian::date _trimmingEndInfectiousProtectionDate;
        boost::gregorian::date _trimmingEndNonInfectiousProtectionDate;
        std::map<FunctionalEnumerations::Health::LamenessInfectiousStateType, Health::Lameness::LamenessStates::LamenessState*> _pHasCurrentLamenessStates;
        bool _isInfectiousLame = false;
        bool _isNonInfectiousLame = false;

        Foots(){} // For serialization
    public:
        Foots(Animal* pAnimal, const boost::gregorian::date &date);
        virtual ~Foots (); // Destructor
#ifdef _LOG
        unsigned int getAnimalUniqueId();
        unsigned int getIdForLameness();          
#endif // _LOG    
        virtual void progress(const boost::gregorian::date &simDate);
        DairyMammal* getDairyMammal();
        Population::Batch* getBatch();
        Tools::Random &getRandom();

        
        // Lameness
#ifdef _LOG
         unsigned int getUniqueIdForLameness();
#endif // _LOG            
        float getYearInfectiousLamenessIncidence(FunctionalEnumerations::Health::LamenessInfectiousStateType lamenessInfectiousStatus);
        float getLamenessOestrusDetectionFactor(FunctionalEnumerations::Reproduction::DetectionMode dm, bool first);
        void setCurrentLamenessState(FunctionalEnumerations::Health::LamenessInfectiousStateType LIST, Health::Lameness::LamenessStates::LamenessState* pState);
        float getLamenessRisk(const boost::gregorian::date &simDate, FunctionalEnumerations::Health::LamenessInfectiousStateType infectiousType); 
        void notifyForLamenessSporadicDetectionOrVerificationToday(
#ifdef _LOG                
                                                    const boost::gregorian::date &date
#endif // _LOG                
                                                    );
        unsigned int getCurrentDurationLamenessIll(const boost::gregorian::date &date, FunctionalEnumerations::Health::LamenessInfectiousStateType list);
        bool hasCurrentlyEfficientFootbath(const boost::gregorian::date &date, float &dayRatio);
        FunctionalEnumerations::Population::Location getCurrentLocation();
        void notifyNewG1InfectiousLamenessOccurence(const boost::gregorian::date &date);
        void notifyNewG1NonInfectiousLamenessOccurence(const boost::gregorian::date &date);
        void notifyNewG2InfectiousLamenessOccurence(const boost::gregorian::date &date);
        void notifyNewG2NonInfectiousLamenessOccurence(const boost::gregorian::date &date);
        void careIndividualTodayForLameness(const boost::gregorian::date &careDate);
        void trimCollectiveTodayForLamenessPrevention(const boost::gregorian::date &trimDate);
        void applyTrimmingProtectionEffects(const boost::gregorian::date &trimDate);
        void becomeLamenessSensitive(const boost::gregorian::date &date);
        void isLamenessIll(bool &isDetectedInfectiousLameness, bool &isInfectiousG1Lameness, bool &isInfectiousG2Lameness, bool &isDetectedNonInfectiousLameness, bool &isNonInfectiousG1Lameness, bool &isNonInfectiousG2Lameness);
        bool inline isInfectiousLamenessIll(){return _isInfectiousLame;}
        bool inline isNonInfectiousLamenessIll(){return _isNonInfectiousLame;}
        bool hadRecentTrimming(const boost::gregorian::date &date);
        bool hasCurrentLamenesCases();
        float getMastitisRiskDueToLameness();
        void getLamenessEffectOnMilkProduction(const boost::gregorian::date &date, ExchangeInfoStructures::MilkProductCharacteristic &mc);
        FunctionalEnumerations::Health::LamenessDetectionMode getLamenessDetectionMode();
        void diesNowBecauseOfLameness(
#ifdef _LOG                   
            const boost::gregorian::date &date, 
#endif // _LOG            
                                      FunctionalEnumerations::Population::DeathReason deathReason);
        void notifyMammalIsCulledForSevereLameness(const boost::gregorian::date &date, FunctionalEnumerations::Population::CullingStatus cs);
        void getLamenessG1InfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability);
        void getLamenessG1NonInfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability);
        void getLamenessG2InfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability);
        void getLamenessG2NonInfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability);
        Results::DairyFarmResult* getCurrentResults();
    };
}
#endif //DataStructures_Foots_h