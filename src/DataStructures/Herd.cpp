#include "Herd.h"

// project
#include "Mammal.h"
#include "./Population/Batch/Batch.h"
#include "DairyFarm.h"
#include "../Genetic/Breed/Breed.h"
#include "../ResultStructures/DairyFarmResult.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(DataStructures::Herd) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(_pIsInDairyFarm); \
    ar & BOOST_SERIALIZATION_NVP(_mpIncludesBatch); \
    ar & BOOST_SERIALIZATION_NVP(_mpIsComposedAnimal); \
    ar & BOOST_SERIALIZATION_NVP(_mpAnimalsToCullLater); \

    IMPLEMENT_SERIALIZE_STD_METHODS(Herd);

    Herd::Herd(DairyFarm* pFarmExpl)
    {
        _pIsInDairyFarm = pFarmExpl;
         
    } /* End of constructor */
      
    void Herd::progress(const boost::gregorian::date &simDate)
    {
    }
    
    unsigned int Herd::getAnimalNumber()
    {
        return _mpIsComposedAnimal.size();
    }
    
    std::map<unsigned int, Animal*> &Herd::getPresentAnimals()
    {
        return _mpIsComposedAnimal;
    }
       
    std::map<unsigned int, Animal*> &Herd::getCurrentPresentAdults()
    {
        return _mpCurrentPresentAdults;
    }
       
    Population::Batch* Herd::getBatch(FunctionalEnumerations::Population::BatchType theBatchType)
    {
        return _mpIncludesBatch.find(theBatchType)->second;
    }
    
    unsigned int Herd::getNewIdUnique()
    {
        return getDairyFarm()->getNewIdUnique();
    }

    DairyFarm* Herd::getDairyFarm()
    {
        return _pIsInDairyFarm;
    }
    
    void Herd::setInitialBatchLocation(const boost::gregorian::date &beginDate)
    {
        for (std::map<FunctionalEnumerations::Population::BatchType, Population::Batch*>::iterator itB = _mpIncludesBatch.begin(); itB != _mpIncludesBatch.end(); itB++)
        {
            (itB->second)->changeCurrentLocation(beginDate);
        }
    }

    Herd::~Herd()
    {
        // Animal Composed
        for (auto itAnim : _mpIsComposedAnimal)
        {
            delete (itAnim.second);
        }
        _mpIsComposedAnimal.clear();

        // Batch Composed
        for (std::map<FunctionalEnumerations::Population::BatchType, Population::Batch* >::iterator itb = _mpIncludesBatch.begin(); itb !=  _mpIncludesBatch.end(); itb++)
        {
            delete itb->second;
        }
        _mpIncludesBatch.clear();
    }
    
    unsigned int Herd::getMeanAdultNumberTarget()
    {
        return getDairyFarm()->getMeanAdultNumberTarget();
    }
    
    unsigned int Herd::getAdultCount()
    {
        return    getBatch(FunctionalEnumerations::Population::BatchType::driedCowsAndPregnantHeifers)->getAnimalCount()
                + getBatch(FunctionalEnumerations::Population::BatchType::periParturientFemales)->getAnimalCount()
                + getBatch(FunctionalEnumerations::Population::BatchType::lactatingCows)->getAnimalCount();
    }
    
    unsigned int Herd::getBredHeiferCount()
    {
        return    getBatch(FunctionalEnumerations::Population::BatchType::bredHeifers)->getAnimalCount();
    }

    Genetic::Breed::Breed* Herd::getInitialBreed()
    {
        return getDairyFarm()->getInitialBreed();
    }
    
    void Herd::includeAnimal(Animal* pAnim)
    {
        _mpIsComposedAnimal[pAnim->getUniqueId()] = pAnim;
    }
     
    void Herd::excludeAnimal(Animal* pAnim)
    {
        pAnim->getBatch()->excludeAnimal(pAnim, true); // Leave it's batch
        
        pAnim->getHerd()->removeAnimalToBeCulledLater(pAnim);
        
        // Exit from lameness care and prevention group
        removeAnimalFromGroup(pAnim, getDairyFarm()->getToCareDueToSporadicLamenessDetectionOrVerificationGroup());
        removeAnimalFromGroup(pAnim, getDairyFarm()->getToTrimForLamenessPreventionGroup());
        
        _mpIsComposedAnimal.erase(pAnim->getUniqueId());
        _mpCurrentPresentAdults.erase(pAnim->getUniqueId());
    }
    
    void Herd::addAnimalInGroup(DataStructures::Animal* pAnim,  std::vector<DataStructures::Animal*> &vGroup)
    {
        std::vector<DataStructures::Animal*>::iterator it = std::find(vGroup.begin(), vGroup.end(), pAnim);
        if (it == vGroup.end())
        {
            vGroup.push_back(pAnim);
        }
    }

    void Herd::removeAnimalFromGroup(DataStructures::Animal* pAnim,  std::vector<DataStructures::Animal*> &vGroup)
    {
        std::vector<DataStructures::Animal*>::iterator it = std::find(vGroup.begin(), vGroup.end(), pAnim);
        if (it != vGroup.end())
        {
            vGroup.erase(it);
        }
    }

    void Herd::appendPresentAdult(Animal* pAnim)
    {
        _mpCurrentPresentAdults[pAnim->getUniqueId()] = pAnim;
    }

    void Herd::addAnimalToBeCulledLater(Animal* pAnim)
    {
        _mpAnimalsToCullLater[pAnim->getUniqueId()] = pAnim;
    }
        
    std::map<unsigned int,Animal*> &Herd::getAnimalsToBeCulledLater()
    {
        return _mpAnimalsToCullLater;
    }
    
    void Herd::removeAnimalToBeCulledLater(Animal* pAnim)
    {
        std::map<unsigned int,Animal*>::iterator it = _mpAnimalsToCullLater.find(pAnim->getUniqueId());
        if (it != _mpAnimalsToCullLater.end())
        {
            _mpAnimalsToCullLater.erase(it);
        }
    }

    void Herd::addAnimalToBeCulledToday(Animal* pAnim)
    {
        _mpAnimalsToCullToday[pAnim->getUniqueId()] = pAnim;
    }
        
    std::map<unsigned int,Animal*> &Herd::getAnimalsToBeCulledToday()
    {
        return _mpAnimalsToCullToday;
    }

    void Herd::clearAnimalsToBeCulledToday()
    {
        _mpAnimalsToCullToday.clear();
    }

#ifdef _LOG
    unsigned int Herd::getCalvesCount()
    {
        return    getBatch(FunctionalEnumerations::Population::BatchType::femaleCalves)->getAnimalCount()
                + getBatch(FunctionalEnumerations::Population::BatchType::weanedCalves)->getAnimalCount();
    }
    
    unsigned int Herd::getHeiferCount()
    {
        return    getBatch(FunctionalEnumerations::Population::BatchType::youngUnbredHeifers)->getAnimalCount()
                + getBatch(FunctionalEnumerations::Population::BatchType::oldUnbredHeifers)->getAnimalCount()
                + getBatch(FunctionalEnumerations::Population::BatchType::bredHeifers)->getAnimalCount();
    }
    
    void Herd::getPopulationTargetValue(Results::WeekAnimalPopulationInformations &info)
    {
        info.adultPopulation = getDairyFarm()->getMeanAdultNumberTarget();
        info.calfPopulation = getCalvesCount();
        info.heiferPopulation = getHeiferCount();
    }
#endif // _LOG

} /* End of namespace DataStructures */