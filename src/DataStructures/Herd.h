#ifndef DataStructures_Herd_h
#define DataStructures_Herd_h

// standard
#include <vector>

// project
#include "../Tools/Serialization.h"
#include "../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "../Genetic/Breed/Breed.h"
#include "../ResultStructures/DairyFarmResult.h"
#include "../ExchangeInfoStructures/MilkProductCharacteristic.h"

namespace Results
{
    class Result;
}

namespace DataStructures
{
    class Mammal;
    class Animal;
    class DairyFarm;
    
    namespace Population
    {
        class Batch;
    }
    
    /// Domesticated Animal group raised to produce commodities such as food or milk
    class Herd
    {
    private:

        DECLARE_SERIALIZE_STD_METHODS;

        DairyFarm* _pIsInDairyFarm;
        
        ExchangeInfoStructures::MilkProductCharacteristic _hasMeanCorrectedProductionLevel;
                
    protected:

        // Temporal values (not to serialize)
        std::map<unsigned int, Animal*> _mpAnimalsToCullToday; 
        std::map<unsigned int, Animal*> _mpAnimalsToCullLater; 
        std::map<unsigned int, Animal*> _mpCurrentPresentAdults;
        // ...

    protected:
        std::map<FunctionalEnumerations::Population::BatchType, Population::Batch* > _mpIncludesBatch; // There is at least one batch : it is the arrival batch
        std::map<unsigned int, DataStructures::Animal*> _mpIsComposedAnimal;

        Herd(){}; // serialization constructor
        Herd(DairyFarm* pFarmExpl); // Constructor
        virtual void concrete() = 0; // To ensure that it will not be instantiated
        
    public:
        virtual ~Herd(); // Destructor
        unsigned int getAnimalNumber();
        std::map<unsigned int, Animal*> &getPresentAnimals();
        std::map<unsigned int, Animal*> &getCurrentPresentAdults();

        unsigned int getNewIdUnique();
        DairyFarm* getDairyFarm();
        void setInitialBatchLocation(const boost::gregorian::date &beginDate);
        
#ifdef _LOG
        virtual void closeResults(const boost::gregorian::date &finalDate){};
#endif // _LOG

        virtual void progress(const boost::gregorian::date &simDate); // progress of duration time and unit
        virtual void produceEndCampaignResults(const boost::gregorian::date &simDate){}
        Population::Batch* getBatch(FunctionalEnumerations::Population::BatchType theBatchType);  
        
        Genetic::Breed::Breed* getInitialBreed();
        
        // Health
        virtual void notifyNewSubclinicalMastitis(){}; // don't care
        virtual void notifyNewClinicalMastitis(bool alreadyHadOne){}; // don't care
        virtual void notifyNewSubclinicalKetosis(bool alreadyHadOne, const boost::gregorian::date &date, const boost::gregorian::date lastCalvingDate){}; // don't care
        virtual void notifyNewClinicalKetosis(bool alreadyHadOne, const boost::gregorian::date &date, const boost::gregorian::date lastCalvingDate){}; // don't care
        virtual void notifyNewG1InfectiousLamenessOccurence(const boost::gregorian::date &date){}; // don't care
        virtual void notifyNewG1NonInfectiousLamenessOccurence(const boost::gregorian::date &date){}; // don't care
        virtual void notifyNewG2InfectiousLamenessOccurence(const boost::gregorian::date &date){}; // don't care
        virtual void notifyNewG2NonInfectiousLamenessOccurence(const boost::gregorian::date &date){}; // don't care

        // Population
        void includeAnimal(Animal* pAnim);
        void excludeAnimal(Animal* pAnim);
        void addAnimalInGroup(DataStructures::Animal* pAnim,  std::vector<DataStructures::Animal*> &group);
        void removeAnimalFromGroup(DataStructures::Animal* pAnim,  std::vector<DataStructures::Animal*> &group);
        void appendPresentAdult(Animal* pAnim);

        void addAnimalToBeCulledToday(Animal* pAnim);
        std::map<unsigned int,Animal*> &getAnimalsToBeCulledToday();
        void clearAnimalsToBeCulledToday();
        void addAnimalToBeCulledLater(Animal* pAnim);
        std::map<unsigned int,Animal*> &getAnimalsToBeCulledLater();
        void removeAnimalToBeCulledLater(Animal* pAnim);
        virtual void increaseAnnualBirthCount(){};
        virtual void increaseAnnualMaleSaleCount(){};
        virtual void increaseAnnualBeefCrossBredCount(){};
        virtual void increaseAnnualSterileFemaleSaleCount(){};
        virtual void increaseAnnualExtraFemaleSaleCount(){};
        virtual void increaseAnnualExtraPregnantHeiferSaleCount(){};
        virtual void increaseAnnualHeiferBoughtCount(){};
        virtual void increaseAnnualDeathCount(Population::Batch* pBatch, FunctionalEnumerations::Population::DeathReason deathReason){};
        virtual void updateCullingResults(DataStructures::Animal* pAnim, const boost::gregorian::date &cullDate){};
        
#ifdef _LOG
        unsigned int getCalvesCount();
        unsigned int getHeiferCount();
        void getPopulationTargetValue( Results::WeekAnimalPopulationInformations &info);    // to know the target population in each group
#endif // _LOG

        unsigned int getMeanAdultNumberTarget();
        unsigned int getAdultCount();
        unsigned int getBredHeiferCount();
    }; /* End of class Herd */
} /* End of namespace DataStructures */

#endif // DataStructures_Herd_h
