#include "Insemination.h"

// project
#include "../ExchangeInfoStructures/TechnicalConstants.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"
#include "../ResultStructures/DairyFarmResult.h"
#include "../Tools/Tools.h"
#include "../Genetic/Breed/Breed.h"
#include "DairyMammal.h"
#include "DairyHerd.h"

BOOST_CLASS_EXPORT(DataStructures::Insemination) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    // Macro to code statement for serialization methods
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(_pSireBreed); \
    ar & BOOST_SERIALIZATION_NVP(_typeSemen); \
    ar & BOOST_SERIALIZATION_NVP(_date); \
    ar & BOOST_SERIALIZATION_NVP(_success); \
    ar & BOOST_SERIALIZATION_NVP(_number); \
    ar & BOOST_SERIALIZATION_NVP(_sireGeneticValue); \

    IMPLEMENT_SERIALIZE_STD_METHODS(Insemination);
    
    Insemination::Insemination(Genetic::Breed::Breed* pSireBreed, Genetic::GeneticValue &sireGeneticValue, FunctionalEnumerations::Reproduction::TypeInseminationSemen typeSemen, const boost::gregorian::date& theDate, bool success, unsigned int number, Results::Result* pRes, ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &am, DataStructures::DairyMammal* pCow, bool manageCost)
    {
        _pSireBreed = pSireBreed;
        _typeSemen = typeSemen;
        _sireGeneticValue = sireGeneticValue;
        _date = theDate;
        _success = success;
        _number = number;
        float amount = getAmount(am);
        if (amount != 0.0f and manageCost)
        {
#ifdef _LOG                
            std::string comp =  _typeSemen == FunctionalEnumerations::Reproduction::TypeInseminationSemen::natural ? FunctionalConstants::AccountingModel::NATURAL_INSEMINATION_TRANSACTION_TYPE :
                                _typeSemen == FunctionalEnumerations::Reproduction::TypeInseminationSemen::conventionalIA ? FunctionalConstants::AccountingModel::CONVENTIONAL_IA_TRANSACTION_TYPE :
                                _typeSemen == FunctionalEnumerations::Reproduction::TypeInseminationSemen::maleSexedIA ? FunctionalConstants::AccountingModel::MALE_SEXED_IA_TRANSACTION_TYPE :
                                _typeSemen == FunctionalEnumerations::Reproduction::TypeInseminationSemen::femaleSexedIA ? FunctionalConstants::AccountingModel::FEMALE_SEXED_IA_TRANSACTION_TYPE :
                                                                                            "Unwnown insemination" ; // hope it never appears
#endif // _LOG                
            pCow->getDairyHerd()->accountNewTransaction(
#ifdef _LOG                
                                    theDate, comp, 
#endif // _LOG                
                                    amount, pCow->getDairyHerd()->getReproductionCostsForUpdate());
        }
    }
    
    boost::gregorian::date &Insemination::getDate()
    {
        return _date;
    }
    bool Insemination::isSuccess()
    {
        return _success;
    }
    
    unsigned int Insemination::getNumber()
    {
        return _number;
    }
    
    bool Insemination::isArtificial()
    {
        return false;
    }
    
    FunctionalEnumerations::Reproduction::TypeInseminationSemen Insemination::getTypeSemen()
    {
        return _typeSemen;
    }
        
    Genetic::Breed::Breed* Insemination::getBreed()
    {
        return _pSireBreed;
    } // End of method getBreed


    float Insemination::getAmount(ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &am)
    {
        return am.getCurrentPrice(FunctionalConstants::AccountingModel::INSEMINATION_PRICES.find(_typeSemen)->second);
    }
    
    Genetic::GeneticValue &Insemination::getSirGeneticValue()
    {
        return _sireGeneticValue;
    }
    
} /* End of namespace DataStructures */
