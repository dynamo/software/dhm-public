#ifndef DataStructures_Insemination_h
#define DataStructures_Insemination_h

/// project
#include "../ExchangeInfoStructures/DairyFarmAccountingModelParameters.h"
#include "../Tools/Serialization.h"
#include "../Genetic/GeneticValue.h"
#include "../Genetic/Breed/Breed.h"


namespace Results
{
    class Result;
}

namespace DataStructures
{
    class DairyMammal;
    
    /// Operation to fertilize female Mammal
    class Insemination
    {
    private:
        boost::gregorian::date _date;
        FunctionalEnumerations::Reproduction::TypeInseminationSemen _typeSemen;
        Genetic::Breed::Breed* _pSireBreed;
        Genetic::GeneticValue _sireGeneticValue;
        bool _success;
        unsigned int _number; // of the life or since last calving

        DECLARE_SERIALIZE_STD_METHODS;
    protected:
        Insemination(){}; // For serialization
        virtual void concrete() = 0; // To ensure that it will not be instantiated
        
    public:
        Insemination(Genetic::Breed::Breed* pSireBreed, Genetic::GeneticValue &sireGeneticValue, FunctionalEnumerations::Reproduction::TypeInseminationSemen typeSemen, const boost::gregorian::date& theDate, bool success, unsigned int number, Results::Result* pRes, ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &am, DataStructures::DairyMammal* pCow, bool manageCost);
        virtual ~Insemination(){};
        float getAmount(ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &am);
        boost::gregorian::date &getDate();
        bool isSuccess();
        unsigned int getNumber();
        Genetic::Breed::Breed* getBreed();
        virtual bool isArtificial();
        FunctionalEnumerations::Reproduction::TypeInseminationSemen getTypeSemen();
        Genetic::GeneticValue &getSirGeneticValue();
    };
} /* End of namespace DataStructures */

#endif // DataStructures_Insemination_h



