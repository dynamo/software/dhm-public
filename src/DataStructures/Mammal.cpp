#include "Mammal.h"

// standard
#include <chrono>

// boost
#include <boost/foreach.hpp>

// Project
#include "./PregnancyProcess.h"
#include "./Insemination.h"
#include "./Ovulation.h"
#include "./Population/Batch/Batch.h"
#include "./Population/PopulationManagement.h"
#include "../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "../Tools/Tools.h"
#include "../ResultStructures/DairyFarmResult.h"
#include "./DairyUdder.h"
#include "./Herd.h"
#include "../DataStructures/DairyFarm.h"

BOOST_CLASS_EXPORT(DataStructures::Mammal) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Animal); \
    ar & BOOST_SERIALIZATION_NVP(_vpHadOvulations); \
    ar & BOOST_SERIALIZATION_NVP(_vpHadInseminations); \
    ar & BOOST_SERIALIZATION_NVP(_vpHadPregnancyProcess); \
    ar & BOOST_SERIALIZATION_NVP(_pHasUdder); \
    ar & BOOST_SERIALIZATION_NVP(_pubertyAge); \
    ar & BOOST_SERIALIZATION_NVP(_lateEmbryonicMortalityRisk); \
    ar & BOOST_SERIALIZATION_NVP(_abortionRisk); \
    ar & BOOST_SERIALIZATION_NVP(_mTheoricPregnancyDurations); \
    ar & BOOST_SERIALIZATION_NVP(_mTheoricBetweenOvulationDurations); \
    ar & BOOST_SERIALIZATION_NVP(_lastPregnancyEndDate); \
    ar & BOOST_SERIALIZATION_NVP(_lastFarrowingDate); \
    ar & BOOST_SERIALIZATION_NVP(_previousFarrowingDate); \
    ar & BOOST_SERIALIZATION_NVP(_lastInseminationDate); \
    ar & BOOST_SERIALIZATION_NVP(_inseminationQuantitySinceLastFarrowing); \
    ar & BOOST_SERIALIZATION_NVP(_lastFirstInseminationDate); \
    ar & BOOST_SERIALIZATION_NVP(_lastFirstInseminationType); \
    ar & BOOST_SERIALIZATION_NVP(_lastFirstInseminationMaleBreed); \
    ar & BOOST_SERIALIZATION_NVP(_lastFertilizingInseminationDate); \
    ar & BOOST_SERIALIZATION_NVP(_lastFertilizingInseminationType); \
    ar & BOOST_SERIALIZATION_NVP(_lastFertilizingInseminationMaleBreed); \
    ar & BOOST_SERIALIZATION_NVP(_lastFertilizingInseminationNumber); \
    ar & BOOST_SERIALIZATION_NVP(_firstFirstInseminationAge); \
    ar & BOOST_SERIALIZATION_NVP(_firstFertilizingInseminationAge); \
    ar & BOOST_SERIALIZATION_NVP(_firstFarrowingAge); \
    ar & BOOST_SERIALIZATION_NVP(_alreadyHadClinicalMastitis); \
    ar & BOOST_SERIALIZATION_NVP(_alreadyHadSubclinicalKetosis); \
    ar & BOOST_SERIALIZATION_NVP(_alreadyHadClinicalKetosis); \

    
    IMPLEMENT_SERIALIZE_STD_METHODS(Mammal);

    Mammal::Mammal(const boost::gregorian::date &simDate, const boost::gregorian::date &birth, Genetic::Breed::Breed* pSireBreed, Herd* pHerd, FunctionalEnumerations::Genetic::Sex sex) : Animal(simDate, birth, pSireBreed, pHerd, sex)
    {
    }
    
    // Constructor based with the dam (simulated birth)
    Mammal::Mammal(const boost::gregorian::date &birth, Mammal* pDam, Genetic::Breed::Breed* pSireBreed, FunctionalEnumerations::Genetic::Sex sex) : Animal(birth, pDam, pSireBreed, sex)
    {
    }

    unsigned int Mammal::getPubertyAge()
    {
        return _pubertyAge;
    }
    
    void Mammal::progress(const boost::gregorian::date &simDate)
    {
        Animal::progress(simDate);
        
        // ...
    }
    
    // Destructor
    Mammal::~Mammal ()
    {
        for (unsigned int i = 0; i < _vpHadInseminations.size(); i++)
        {
            delete _vpHadInseminations[i];
        }
        _vpHadInseminations.clear();

        for (unsigned int i = 0; i < _vpHadPregnancyProcess.size(); i++)
        {
            delete _vpHadPregnancyProcess[i];
        }
        _vpHadPregnancyProcess.clear();

        for (unsigned int i = 0; i < _vpHadOvulations.size(); i++)
        {
            delete _vpHadOvulations[i];
        }
        _vpHadOvulations.clear();
        
        delete _pHasUdder;
        _pHasUdder = nullptr;
    }
    
    void Mammal::addOvulation(const boost::gregorian::date& ovulationDate)
    {
        // Creating the new ovulation
        _vpHadOvulations.push_back(new Ovulation(ovulationDate));
    }
    
    unsigned int Mammal::getLastUnfecundedOvulationNumber()
    {
        unsigned int ovulationNumber = 0;
        if (_vpHadPregnancyProcess.size() == 0)
        {
            // None gestation, so oestrus number
            return ovulationNumber = _vpHadOvulations.size();            
        }
        else
        {
            // at least one gestation, counting the oestrus number after it
            boost::gregorian::date lastEndPregnancyDate = getLastEndPregnancyDate();
            bool finish = false;
            for (std::vector<Ovulation*>::iterator it = _vpHadOvulations.end(); it != _vpHadOvulations.begin() && !finish;)
            {
                Ovulation* pOvulation = *(--it);
                if ( pOvulation->getBeginDate() >= lastEndPregnancyDate)
                {
                    ovulationNumber++;
                }
                else 
                {
                    finish = true;
                }
            }
        }
        return ovulationNumber;
    }
    
    unsigned int Mammal::getReturnInHeatNumber()
    {
        unsigned int returnInHeatNumber = 0;
        boost::gregorian::date lastInseminationDate = getLastInseminationDate();
        bool toCompute = !(lastInseminationDate.is_not_a_date());
        if (toCompute && _vpHadPregnancyProcess.size() != 0) // We have at least an insemination, but we have pregnancies too
        {
            if (lastInseminationDate < getLastEndPregnancyDate())
            {
                // No result to compute, because there was a gestation after last insemination
                toCompute = false;
            }
        }
        if (toCompute)
        {
            // The result is good to compute, there was no gestation after last existing insemination
            bool finish = false;
            for (std::vector<Ovulation*>::iterator it = _vpHadOvulations.end(); it != _vpHadOvulations.begin() && !finish;)
            {
                Ovulation* pOvulation = *(--it);
                if ( pOvulation->getBeginDate() > lastInseminationDate)
                {
                    returnInHeatNumber++;
                }
                else 
                {
                    finish = true;
                }
            }
        }
        return returnInHeatNumber;
    }

    void Mammal::addInsemination(Insemination* pIns)
    {
        _vpHadInseminations.push_back(pIns);
        _lastInseminationDate = pIns->getDate();
#if defined _LOG || defined _FULL_RESULTS
        if (pIns->getNumber() == 1)
        {
            _lastFirstInseminationDate = _lastInseminationDate;
            _lastFirstInseminationType = pIns->getTypeSemen();
            _lastFirstInseminationMaleBreed = pIns->getBreed()->getEnumerationBreed();
            _inseminationQuantitySinceLastFarrowing = 1;
            if (_firstFirstInseminationAge == 0)
            {
                _firstFirstInseminationAge = getAge();
            }
        }
        else
        {
            _inseminationQuantitySinceLastFarrowing++;
        }
#endif // _LOG || _FULL_RESULTS
    }

    bool Mammal::hasAlreadyBeenInseminated()
    {
        return _vpHadInseminations.size() != 0;    
    }

    unsigned int Mammal::getInseminationRank()
    {
        unsigned int inseminationRank = 0;
        boost::gregorian::date lastInseminationDate = getLastInseminationDate();
        bool toCompute = !(lastInseminationDate.is_not_a_date());
        
        if (toCompute && _vpHadPregnancyProcess.size() != 0) // We have at least an insemination, but we have pregnancies too
        {
            if (lastInseminationDate < getLastEndPregnancyDate())
            {
                // No result to compute, because there was a gestation after last insemination
                toCompute = false;
            }
        }
        if (toCompute)
        {
            // The result is good to compute, there was no gestation after last existing insemination
            bool finish = false;
            boost::gregorian::date endPregnancyDate = getLastEndPregnancyDate();
            for (std::vector<Insemination*>::iterator it = _vpHadInseminations.end(); it != _vpHadInseminations.begin() && !finish;)
            {
                Insemination* pInsemination = *(--it);
                if ( pInsemination->getDate() > endPregnancyDate)
                {
                    inseminationRank++;
                }
                else 
                {
                    finish = true;
                }
            }
        }
        return inseminationRank;
    }
    
    unsigned int Mammal::getFarrowingNumber()
    {
        unsigned int res = 0;
        for (auto pGest : _vpHadPregnancyProcess)
        {
            res += pGest->getBirthNumber();           
        }
        return res;       
    }
    
    Insemination* Mammal::getLastInsemination()
    {
        Insemination* res = nullptr;
        unsigned int nb = _vpHadInseminations.size();
        if (nb > 0)
        {
            res = _vpHadInseminations[nb-1];
        }
        return res;
    }
    
    void Mammal::addPregnancyProcess(PregnancyProcess* pGest)
    {
        _vpHadPregnancyProcess.push_back(pGest);
    }

    unsigned int Mammal::getOvulationNumber()
    {
        return _vpHadOvulations.size();
    }

    PregnancyProcess* Mammal::getCurrentPregnancy()
    {
        PregnancyProcess* res = nullptr;
        if (pregnancyIsOngoing())
        {
            res = _vpHadPregnancyProcess[_vpHadPregnancyProcess.size()-1];
        }
        return res;
    }
    
    PregnancyProcess* Mammal::getLastPregnancyProcess()
    {
        PregnancyProcess* res = nullptr;
        if (_vpHadPregnancyProcess.size() > 0)
        {
            res = _vpHadPregnancyProcess[_vpHadPregnancyProcess.size()-1];
        }
        return res;
    }
    
    Mammal* Mammal::farrowing(const boost::gregorian::date& farrowingDate, Mammal* pMammal) //, Enumerations::EndPregnancyReason reason)
    {
        if (nullptr != pMammal)
        {
            getCurrentPregnancy()->recordFarrow(pMammal);

        }
        if (farrowingDate != _lastFarrowingDate) // case of tweens
        {
            _previousFarrowingDate = _lastFarrowingDate;
            _lastFarrowingDate = farrowingDate;
            if (_firstFarrowingAge == 0) // first farrowing ?
            {
                _firstFarrowingAge = getAge();
            }
        }
        return pMammal;
    }
    
    void Mammal::setEndPregnancyDate(const boost::gregorian::date& lastCalvingDate, const boost::gregorian::date& endDate, FunctionalEnumerations::Reproduction::EndPregnancyReason reason)
    {
        
        getCurrentPregnancy()->setEndDate(endDate, reason
#ifdef _LOG
                                                        , getCurrentResults()
#endif // _LOG                
                );
        _lastPregnancyEndDate = endDate;    
    }
    
    boost::gregorian::date Mammal::getLastOvulationDate()
    {
        boost::gregorian::date res(boost::date_time::not_a_date_time);
        unsigned int nb = _vpHadOvulations.size();
        if (nb > 0)
        {
            res = _vpHadOvulations[nb-1]->getBeginDate();
        }
        return res;
    }
           
    const boost::gregorian::date &Mammal::getLastInseminationDate()
    {
        return _lastInseminationDate;
    }
    
//    const boost::gregorian::date &Mammal::getLastFertilizingInseminationDate()
//    {
//        return _lastFertilizingInseminationDate;
//    }
    
//    const boost::gregorian::date &Mammal::getLastFirstInseminationDate()
//    {
//        return _lastFirstInseminationDate;
//    }
//
    const boost::gregorian::date &Mammal::getLastFarrowingDate()
    {
        return _lastFarrowingDate; 
    }
    
    boost::gregorian::date Mammal::getLastEndPregnancyDate()
    {
        return _lastPregnancyEndDate;
    }
 
    bool Mammal::pregnancyIsOngoing()
    {
        bool res = false;    
        unsigned int nb = _vpHadPregnancyProcess.size();
        if (nb >0) res = !_vpHadPregnancyProcess[nb-1]->isFinished();

        return res;
    }
    
    unsigned int Mammal::getCommingInseminationNumber()
    {
        if (_vpHadPregnancyProcess.size() == 0)
        {
            // It never had pregnancy (heifer)
            if (_vpHadInseminations.size() == 0)
            {
                // it never had articial insemination
                return 1;
            }
            else
            {
                // it is not the first insemination
                return _vpHadInseminations[_vpHadInseminations.size()-1]->getNumber()+1;
            }
        }
        else
        {
            // It had at least one pregnancy
            Insemination *pLastIns = _vpHadInseminations[_vpHadInseminations.size()-1];
            
            // Find the previous calving (and not ambryo loose or abortion
            const boost::gregorian::date lastCalvingDate = getLastFarrowingDate();
        
            if(lastCalvingDate.is_not_a_date())
            {
                return 1;
            }
            else
            {
                if (pLastIns->getDate() < lastCalvingDate)
                {
                    // if the last was before the last pregnancy
                    return 1;
                }
                else
                {
                    return pLastIns->getNumber()+1;
                }  
            }           
        }
    }

    void Mammal::looseEmbryo(const boost::gregorian::date &date)
    {
        closePregnancy(date, FunctionalEnumerations::Reproduction::EndPregnancyReason::embryoLoose);
                
        // Notify batch 
        getBatch()->notifyUnsuccessfulPregnancyEnd(this);   
    }
    
    void Mammal::abort(const boost::gregorian::date &date)
    {
        closePregnancy(date, FunctionalEnumerations::Reproduction::EndPregnancyReason::abortion);
    }
    
    void Mammal::closePregnancy(const boost::gregorian::date &date, FunctionalEnumerations::Reproduction::EndPregnancyReason endPregnancyReason)
    {
        // Ends the current pregnancy
        setEndPregnancyDate(getLastFarrowingDate(), date, endPregnancyReason);
#ifdef _LOG
        if (endPregnancyReason == FunctionalEnumerations::Reproduction::EndPregnancyReason::embryoLoose)
        {
            addMiscellaneousLog(date, "EmbryoLoose ");
        }
        else if (endPregnancyReason == FunctionalEnumerations::Reproduction::EndPregnancyReason::abortion)
        {
            addMiscellaneousLog(date, "Abortion ");
        }
        else
        {
            addMiscellaneousLog(date, "Unknown end pregnancy reason");
        }
#endif // _LOG                
    }  
    // Static initializations
    
} /* End of namespace DataStructures */
