#ifndef DataStructures_Mammal_h
#define DataStructures_Mammal_h

// boost

// project
#include "Animal.h"
#include "../ExchangeInfoStructures/FunctionalEnumerations.h"

namespace Tools
{
    class Random;
}
namespace DataStructures
{
    class Udder;
    class Ovulation;
    class Insemination;
    class PregnancyProcess;
    class ReproductionPerformance;
    
    /// Animal distinguished from others by the possession of Udder
    class Mammal : public Animal
    {
    private:
        std::vector<Ovulation*> _vpHadOvulations;
        std::vector<Insemination*> _vpHadInseminations;
        std::vector<PregnancyProcess*> _vpHadPregnancyProcess;
        

    protected:
        Udder* _pHasUdder;
        
        unsigned int _pubertyAge;        
        float _lateEmbryonicMortalityRisk;
        float _abortionRisk;
        std::map<FunctionalEnumerations::Reproduction::DairyCowParity, unsigned int> _mTheoricPregnancyDurations;
        std::map<FunctionalEnumerations::Reproduction::DairyCowParity, unsigned int> _mTheoricBetweenOvulationDurations;
        
        // Dynamic data
        boost::gregorian::date _lastPregnancyEndDate; // could be interrupted

        boost::gregorian::date _lastFarrowingDate;
        boost::gregorian::date _previousFarrowingDate;
        
        boost::gregorian::date _lastInseminationDate;
        
        boost::gregorian::date _lastFirstInseminationDate;
        FunctionalEnumerations::Reproduction::TypeInseminationSemen _lastFirstInseminationType;
        FunctionalEnumerations::Genetic::Breed _lastFirstInseminationMaleBreed;
        unsigned int _firstFirstInseminationAge = 0;
        unsigned int _inseminationQuantitySinceLastFarrowing = 0;
        boost::gregorian::date _lastFertilizingInseminationDate;
        FunctionalEnumerations::Reproduction::TypeInseminationSemen _lastFertilizingInseminationType;
        FunctionalEnumerations::Genetic::Breed _lastFertilizingInseminationMaleBreed;
        unsigned int _lastFertilizingInseminationNumber = 0;    
        unsigned int _firstFertilizingInseminationAge = 0;
        
        unsigned int _firstFarrowingAge = 0;
        
        // Lactation
        
        // Health
        bool _alreadyHadClinicalMastitis = false;
        bool _alreadyHadSubclinicalKetosis = false;
        bool _alreadyHadClinicalKetosis = false;
        
        DECLARE_SERIALIZE_STD_METHODS;
    protected:
        Mammal(){} // For serialization
        void concrete(){}; // To allow instanciation
       
    private:
        
    protected:
        Mammal(const boost::gregorian::date &birth, Mammal* pDam, Genetic::Breed::Breed* pSireBreed, FunctionalEnumerations::Genetic::Sex sex); // Constructor based with the dam
        Mammal(const boost::gregorian::date &simDate, const boost::gregorian::date &birth, Genetic::Breed::Breed* pSireBreed, Herd* pHerd, FunctionalEnumerations::Genetic::Sex sex); // Constructor based initial values
        
    public:

        virtual ~Mammal (); // Destructor
        virtual void progress(const boost::gregorian::date &simDate) override; // progress of duration time and unit

        // Reproduction
        virtual void addOvulation(const boost::gregorian::date& ovulationDate);
        virtual void addInsemination(Insemination* pIns);
        unsigned int getInseminationRank();
        bool hasAlreadyBeenInseminated();
        unsigned int getCommingInseminationNumber();
        
        Insemination* getLastInsemination();
        unsigned int getLastUnfecundedOvulationNumber();
        unsigned int getReturnInHeatNumber(); // After unsuccessfull insemination 
        Mammal* farrowing(const boost::gregorian::date& farrowingDate, Mammal* pMammal); //, Enumerations::EndPregnancyReason term);
        virtual void looseEmbryo(const boost::gregorian::date &date);
        void abort(const boost::gregorian::date &date);
        void closePregnancy(const boost::gregorian::date &date, FunctionalEnumerations::Reproduction::EndPregnancyReason endPregnancyReason);
        virtual void setEndPregnancyDate(const boost::gregorian::date& lastCalvingDate, const boost::gregorian::date& endDate, FunctionalEnumerations::Reproduction::EndPregnancyReason reason);

        void addPregnancyProcess(PregnancyProcess* pGest);
        unsigned int getOvulationNumber();
        unsigned int getFarrowingNumber();
        PregnancyProcess* getCurrentPregnancy();
        PregnancyProcess* getLastPregnancyProcess();
        const boost::gregorian::date &getLastInseminationDate();
//        const boost::gregorian::date &getLastFirstInseminationDate();
//        const boost::gregorian::date &getLastFertilizingInseminationDate();
        boost::gregorian::date getLastOvulationDate();
        const boost::gregorian::date &getLastFarrowingDate();
        boost::gregorian::date getLastEndPregnancyDate();
        virtual bool pregnancyIsOngoing();
        unsigned int getPubertyAge();
        
    }; /* End of class Mammal */
} /* End of namespace DataStructures */
#endif // DataStructures_Mammal_h
