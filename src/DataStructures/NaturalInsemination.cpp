#include "NaturalInsemination.h"

// project

BOOST_CLASS_EXPORT(DataStructures::NaturalInsemination) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Insemination); \

    IMPLEMENT_SERIALIZE_STD_METHODS(NaturalInsemination);
    
    float NaturalInsemination::getAmount(ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &am)
    {
        return 0.0f;
    }
    
} /* End of namespace DataStructures */

