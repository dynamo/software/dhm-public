#ifndef DataStructures_NaturalInsemination_h
#define DataStructures_NaturalInsemination_h

// boost

// project
#include "Insemination.h"

namespace Results
{
    class Result;
}
namespace DataStructures
{
    class Mammal;
    
    /// Insemination by bull service
    class NaturalInsemination : public Insemination
    {
        DECLARE_SERIALIZE_STD_METHODS;
        
    protected:
        void concrete(){}; // To allow instanciation
 
    public:
        NaturalInsemination(){}; // For serialization
        NaturalInsemination(Genetic::Breed::Breed* pSireBreed, Genetic::GeneticValue sireGeneticValue, const boost::gregorian::date& theDate, bool success, unsigned int number, Results::Result* pRes, ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &am, DataStructures::DairyMammal* pCow) : Insemination(pSireBreed, sireGeneticValue, FunctionalEnumerations::Reproduction::TypeInseminationSemen::natural, theDate, success, number, pRes, am, pCow, true){}
        virtual ~NaturalInsemination(){};
        float getAmount(ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &am);
    };
} /* End of namespace DataStructures */

#endif // DataStructures_NaturalInsemination_h
