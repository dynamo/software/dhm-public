#include "Ovulation.h"

BOOST_CLASS_EXPORT(DataStructures::Ovulation) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(_beginDate);

    IMPLEMENT_SERIALIZE_STD_METHODS(Ovulation);
    
    Ovulation::Ovulation(const boost::gregorian::date& beginDate)
    {
        _beginDate = beginDate;
    }
          
    boost::gregorian::date &Ovulation::getBeginDate()
    {
        return _beginDate;
    }
    
    Ovulation::~Ovulation()
    {
    }

} /* End of namespace DataStructures */
