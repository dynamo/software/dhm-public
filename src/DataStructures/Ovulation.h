#ifndef DataStructures_Ovulation_h
#define DataStructures_Ovulation_h

// boost
#include <boost/date_time/gregorian/gregorian.hpp>

// project
#include "../Tools/Serialization.h"

namespace DataStructures
{
    /// Female Mammal process triggering the release of an ovule from the ovaries
    class Ovulation
    {
    private:
        boost::gregorian::date _beginDate;
        
        DECLARE_SERIALIZE_STD_METHODS;

        Ovulation(){} // For serialization
        
    public:  
        Ovulation(const boost::gregorian::date& beginDate);
        virtual ~Ovulation();
        boost::gregorian::date &getBeginDate();
    };
} /* End of namespace DataStructures */
#endif // DataStructures_Ovulation_h
