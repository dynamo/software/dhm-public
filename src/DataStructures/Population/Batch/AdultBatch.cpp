#include "AdultBatch.h"

// boost

// project
#include "../../DairyFarm.h"
#include "../LocationStates/BatchLocationState.h"
#include "../../DairyMammal.h"

BOOST_CLASS_EXPORT(DataStructures::Population::AdultBatch) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Batch); \

    IMPLEMENT_SERIALIZE_STD_METHODS(AdultBatch);

    AdultBatch::AdultBatch(DataStructures::Herd* pHerd, Batch* pNextBatch) : Batch(pHerd, pNextBatch)
    {
        initLastMastitisOccurences();
    }
        
    AdultBatch::~AdultBatch()
    {
    }
	
    float AdultBatch::getStrawConsumptionOfTheDay()
    {
        return _pHasCurrentLocationState->getRealStrawConsumption(getDairyFarm()->getAdultDayStrawConsumption(), getAnimalCount());
    }
    
    void AdultBatch::progress(const boost::gregorian::date &simDate)
    {
        // Base part
        Batch::progress(simDate);

        // Specific part
        // -------------
        
        // Infectious lameness prevalence update
        unsigned int infectionLamenessCase = 0;
        for (auto it = getAnimalList().begin(); it != getAnimalList().end(); it++)
        {
            DataStructures::DairyMammal* pCow = (DataStructures::DairyMammal*) (*it).second;
            bool infectious, nonInfectious;
            pCow->isLame(infectious, nonInfectious);
            if (infectious)
            {
                infectionLamenessCase++;
            }
        }
        _infectiousLamenessPrevalence = ((float)infectionLamenessCase)/((float)getAnimalList().size());
    }
}


} /* End of namespace Population */
