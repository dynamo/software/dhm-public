#ifndef Population_AdultBatch_h
#define Population_AdultBatch_h

// boost

// standard

// Project
#include "Batch.h"

namespace DataStructures
{
namespace Population
{
    class AdultBatch : public Batch
    {       
        DECLARE_SERIALIZE_STD_METHODS;
        
        float _infectiousLamenessPrevalence = 0.0f;
        
    protected:
       
        AdultBatch(){} // serialization constructor
        
    public:
        AdultBatch(DataStructures::Herd* pHerd, Batch* pNextBatch); // standard constructor
        virtual ~AdultBatch();

        float getStrawConsumptionOfTheDay();
        virtual void progress(const boost::gregorian::date &simDate) override;
        inline float getInfectiousLamenessPrevalence() override {return _infectiousLamenessPrevalence;}


	
   };
} /* End of namespace Population */
}
#endif // Population_AdultBatch_h
