#include "Batch.h"

// boost

// project
#include "../../Animal.h"
#include "../../DairyFarm.h"
#include "../../Herd.h"
#include "../PopulationManagement.h"
#include "../LocationStates/BatchLocationState.h"
#include "../../../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(DataStructures::Population::Batch) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(_pIsInHerd); \
    ar & BOOST_SERIALIZATION_NVP(_pHasNextBatch); \
    ar & BOOST_SERIALIZATION_NVP(_mIncludeCurrentlyAnimal); \
    ar & BOOST_SERIALIZATION_NVP(_mIncludeCurrentlyOvulatingAnimal); \
    ar & BOOST_SERIALIZATION_NVP(_pHasCurrentLocationState); \
    ar & BOOST_SERIALIZATION_NVP(_lastMastitisOccurences); \

    IMPLEMENT_SERIALIZE_STD_METHODS(Batch);

    Batch::Batch(DataStructures::Herd* pHerd, Batch* pNextBatch) // standard constructor
    {
        _pIsInHerd = pHerd;
        _pHasNextBatch = pNextBatch;
    }
        
    Batch::~Batch()
    {
        // clean
        setCurrentBatchLocationState(nullptr);
    }
    
    void Batch::progress(const boost::gregorian::date &simDate)
    {
        // Is it time to change location ?
        _pHasCurrentLocationState->update(this, simDate);
        
        // Remove obsolete mastitis history
        removeMastitisOccurencesObsoleteHistory(simDate);
        
        // Is there some animals to exits today ?
        std::map<boost::gregorian::gregorian_calendar::date_int_type, std::vector<DataStructures::Animal*>>::iterator itm = _toExitAnimals.find(simDate.modjulian_day());
        if (itm != _toExitAnimals.end())
        {
            // yes
            std::vector<DataStructures::Animal*> tmpExitList = itm->second;
            for (std::vector<DataStructures::Animal*>::iterator itv = tmpExitList.begin(); itv != tmpExitList.end(); itv++)
            {
                PopulationManagement::changeAnimalBatch(*itv, simDate);
            }
        }
       // Straw consumption
        getDairyFarm()->addStrawConsomption(getStrawConsumptionOfTheDay());
    } 
    
    DataStructures::DairyFarm* Batch::getDairyFarm()
    {
        return getHerd()->getDairyFarm();
    }
    
    DataStructures::DairyHerd* Batch::getDairyHerd()
    {
        return (DataStructures::DairyHerd*)getHerd();
    }
    
    void Batch::initLastMastitisOccurences()
    {
        std::map<const boost::gregorian::date, unsigned int> emptyMap;
        for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++)
        {
            // Basic state is unsensitive;
            _lastMastitisOccurences[(FunctionalEnumerations::Health::BacteriumType)iBact] = emptyMap;
        }
    }
        
    void Batch::addMastitisOccurence(FunctionalEnumerations::Health::BacteriumType bacterium, const boost::gregorian::date &date)
    {
        std::map<const boost::gregorian::date, unsigned int> &bacteriumCollection = _lastMastitisOccurences.find(bacterium)->second;
        std::map<const boost::gregorian::date, unsigned int>::const_iterator itDay = bacteriumCollection.find(date);
        if (itDay == bacteriumCollection.end())
        {
            // Not yet mastitis this day
            bacteriumCollection[date] = 1; // We take account of the first mastitis occurence of the day 
        }
        else
        { 
            // We add the new mastitis occurence
            unsigned int newQ = itDay->second + 1;
            bacteriumCollection[date] = newQ;
        }
    }
    
    unsigned int Batch::getMastitisHistory(FunctionalEnumerations::Health::BacteriumType bacterium, const boost::gregorian::date &date)
    {
        unsigned int res = 0;
        std::map<const boost::gregorian::date, unsigned int> &bacteriumCollection = _lastMastitisOccurences.find(bacterium)->second;
        for (std::map<const boost::gregorian::date, unsigned int>::const_iterator itDay = bacteriumCollection.begin(); itDay != bacteriumCollection.end(); itDay++)
        {
            if (itDay->first < date)
            {
                res += itDay->second;
            }
        }
        return res;
    }
        
    void Batch::removeMastitisOccurencesObsoleteHistory(const boost::gregorian::date &date)
    {
        doRemoveMastitisOccurencesObsoleteHistory(date);
    }
    
    /// Remove obsolete history before the useful date
    void Batch::doRemoveMastitisOccurencesObsoleteHistory(const boost::gregorian::date &date)
    {
        for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++)
        {
            std::map<const boost::gregorian::date, unsigned int> &bacteriumCollection = _lastMastitisOccurences.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second;
            for (std::map<const boost::gregorian::date, unsigned int>::const_iterator itDay = bacteriumCollection.begin(); itDay != bacteriumCollection.end(); )
            {
                if (itDay->first < (date - boost::gregorian::days(FunctionalConstants::Health::PREVIOUS_DAY_FOR_MASTITIS_BATCH_CONTAGION)))
                {
                    std::map<const boost::gregorian::date, unsigned int>::const_iterator itNextDay = itDay;
                    itNextDay++;
                    bacteriumCollection.erase(itDay); // Obsolete date
                    itDay = itNextDay;
                }
                else
                {
                    break; // We keep the other dates
                }
            }
        }
    }

    void Batch::changeCurrentLocation(const boost::gregorian::date &simDate)
    {
        setCurrentBatchLocationState(Population::PopulationManagement::getNewBatchStabulationState(simDate)); // Generally, no pasture movements
    }
    
    FunctionalEnumerations::Population::Location Batch::getCurrentLocation()
    {
        return _pHasCurrentLocationState->getLocation();
    }
    
    unsigned int Batch::getDayCountSinceLocation(const boost::gregorian::date &simDate)
    {
        return _pHasCurrentLocationState->getDayDurationSinceBegin(simDate);
    }

    std::map<unsigned int, DataStructures::Animal*> &Batch::getAnimalList()
    {
        return _mIncludeCurrentlyAnimal;
    }
    
    void Batch::includeAnimal(DataStructures::Animal* pAnim, const boost::gregorian::date &date)
    {
        // release the precedent assigment
        Batch* pCurrentBatch = pAnim->getBatch();
        if (nullptr != pCurrentBatch)
        {
            pCurrentBatch->excludeAnimal(pAnim, false);
        }
        _mIncludeCurrentlyAnimal[pAnim->getUniqueId()] = pAnim;
        pAnim->goInBatch(this);
    }
    
    void Batch::excludeAnimal(DataStructures::Animal* pAnim, bool exit)
    {
        removeOvulatingAnimal(pAnim);
        removePlannedExit(pAnim);
        pAnim->exitFromBatch();
//        removeAnimalFromVector(pAnim, getDairyFarm()->getToCareDueToSporadicLamenessDetectionOrVerificationGroup());
//        removeAnimalFromVector(pAnim, getDairyFarm()->getToTrimForLamenessPreventionGroup());
        _mIncludeCurrentlyAnimal.erase(_mIncludeCurrentlyAnimal.find(pAnim->getUniqueId()));
    }
    
//    void Batch::removeAnimalFromVector(DataStructures::Animal* pAnim,  std::vector<DataStructures::Animal*> &vector)
//    {
//        std::vector<DataStructures::Animal*>::iterator it = std::find(vector.begin(), vector.end(), pAnim);
//        if (it != vector.end())
//        {
//            vector.erase(it);
//        }
//    }
//    
    void Batch::planExit(DataStructures::Animal* pAnim, const boost::gregorian::date &exitDate)
    { 
        std::map<boost::gregorian::gregorian_calendar::date_int_type, std::vector<DataStructures::Animal*>>::iterator it = _toExitAnimals.find(exitDate.modjulian_day());
        if (it == _toExitAnimals.end())
        {
            // not yet exit this day
            _toExitAnimals[exitDate.modjulian_day()] = std::vector<DataStructures::Animal*>();
            it = _toExitAnimals.find(exitDate.modjulian_day());
        }
        it->second.push_back(pAnim);
    }
    
    void Batch::removePlannedExit(DataStructures::Animal* pAnim)
    {
        for (std::map<boost::gregorian::gregorian_calendar::date_int_type, std::vector<DataStructures::Animal*>>::iterator itm = _toExitAnimals.begin(); itm != _toExitAnimals.end(); itm++)
        {
            for (std::vector<DataStructures::Animal*>::iterator itv = itm->second.begin(); itv != itm->second.end(); itv++)
            {
                if (*itv == pAnim)
                {
                    itm->second.erase(itv);
                    if (itm->second.size() == 0)
                    {
                       _toExitAnimals.erase(itm);
                    }
                    return;
                }
            }
        }
    }

    unsigned int Batch::getAnimalCount()
    {
        return _mIncludeCurrentlyAnimal.size();
    }

    DataStructures::Herd* Batch::getHerd()
    {
        return _pIsInHerd;
    }

    unsigned int Batch::getSimultaneousOvulations()
    {
        return _mIncludeCurrentlyOvulatingAnimal.size();
    }
    
    void Batch::appendOvulatingAnimal(DataStructures::Animal* pAnim)
    {
        _mIncludeCurrentlyOvulatingAnimal[pAnim->getUniqueId()] = pAnim;
    }
    
    void Batch::removeOvulatingAnimal(DataStructures::Animal* pAnim)
    {
        auto it = _mIncludeCurrentlyOvulatingAnimal.find(pAnim->getUniqueId());
        if (it != _mIncludeCurrentlyOvulatingAnimal.end()) _mIncludeCurrentlyOvulatingAnimal.erase(it);
    }
    
    void Batch::setCurrentBatchLocationState(Population::BatchLocationStates::BatchLocationState* pState)
    {
        if (_pHasCurrentLocationState != nullptr)
        {
           delete _pHasCurrentLocationState;
        }
        _pHasCurrentLocationState = pState;
    }
    
    unsigned int Batch::getBornAnimalDuringThisCampaign(const boost::gregorian::date &date)
    {
        unsigned int res = 0;
        
        boost::gregorian::date_period currentCampaign = getDairyFarm()->getCampaignPeriod(date);
        for (auto it : _mIncludeCurrentlyAnimal)
        {
            if (currentCampaign.contains((it.second)->getBirthDate()))
            {
                res++;
            }
        }
        return res;
    }
        
//    void Batch::notifyForLamenessDetectionOrVerificationToday(DataStructures::Animal* pAnim)
//    {
//        getDairyFarm()->getToCareDueToSporadicLamenessDetectionOrVerificationGroup().push_back(pAnim);
//    }
}

} /* End of namespace Population */
