#ifndef Population_Batch_h
#define Population_Batch_h

// boost
#include "boost/date_time/gregorian/gregorian.hpp"
#include <boost/date_time/posix_time/posix_time_io.hpp>

// standard
#include <vector>
#include <set>

// Project
#include "../../../Tools/Serialization.h"
#include "../../../ResultStructures/DairyFarmResult.h"
#include "../LocationStates/I_LocationBatch.h"

namespace DataStructures
{
    class Animal;
    class Herd;
    class DairyFarm;
    class DairyHerd;
namespace Population
{
    namespace BatchLocationStates
    {
        class BatchLocationState;
    }
    
    /// Group of jointly managed Animals
    class Batch : public Population::BatchLocationStates::I_LocationBatch
    {       
        std::map<unsigned int, DataStructures::Animal*> _mIncludeCurrentlyAnimal;
        std::map<unsigned int, DataStructures::Animal*> _mIncludeCurrentlyOvulatingAnimal;
        
        DataStructures::Herd* _pIsInHerd = nullptr;
        
        // Population
        std::map<boost::gregorian::gregorian_calendar::date_int_type, std::vector<DataStructures::Animal*>> _toExitAnimals;
        
        std::map<FunctionalEnumerations::Health::BacteriumType, std::map<const boost::gregorian::date, unsigned int>> _lastMastitisOccurences;
        

 
        DECLARE_SERIALIZE_STD_METHODS;
        
    protected:
        Population::BatchLocationStates::BatchLocationState* _pHasCurrentLocationState = nullptr;
        Batch* _pHasNextBatch = nullptr; // If systematic
        
        DataStructures::DairyFarm* getDairyFarm();
        DataStructures::DairyHerd* getDairyHerd();
       
        Batch(){} // serialization constructor
        virtual void concrete() = 0; // To ensure that it will not be instantiated
        
    public:
        
        Batch(DataStructures::Herd* pHerd, Batch* pNextBatch);
        virtual ~Batch();
        virtual void changeCurrentLocation(const boost::gregorian::date &simDate);
        FunctionalEnumerations::Population::Location getCurrentLocation();
        unsigned int getDayCountSinceLocation(const boost::gregorian::date &simDate);
        virtual void progress(const boost::gregorian::date &simDate); // progress of duration time and unit

        std::map<unsigned int, DataStructures::Animal*> &getAnimalList();
        virtual FunctionalEnumerations::Population::BatchType getType() = 0;
        DataStructures::Herd* getHerd();
        virtual inline bool oestrusIsManaged(){return false;}
        virtual inline unsigned int getComingPregnantHeiferCountFromBredHeiferBatch(const boost::gregorian::date &date, unsigned int adultTarget){return 0;}
       
        // Population
        virtual void includeAnimal(DataStructures::Animal* pAnim, const boost::gregorian::date &date);
        virtual void excludeAnimal(DataStructures::Animal* pAnim, bool exit);
//        void removeAnimalFromVector(DataStructures::Animal* pAnim,  std::vector<DataStructures::Animal*> &vector);
        void planExit(DataStructures::Animal* pAnim, const boost::gregorian::date &exitDate);
        void removePlannedExit(DataStructures::Animal* pAnim);
        inline Batch* getNextBatch(){return _pHasNextBatch;}
        virtual void notifyPredictedEndSuccessfullPregnancy(DataStructures::Animal* pAnim, const boost::gregorian::date &predictedEndDate){};
        virtual void notifyUnsuccessfulPregnancyEnd(DataStructures::Animal* pAnim){} // Generaly don't care        
        unsigned int getAnimalCount();
        
        // Reproduction
        unsigned int getSimultaneousOvulations();
        void appendOvulatingAnimal(DataStructures::Animal* pAnim);
        void removeOvulatingAnimal(DataStructures::Animal* pAnim);
    protected:
        void setCurrentBatchLocationState(Population::BatchLocationStates::BatchLocationState* pState);
        
        // Health
    protected:
        void initLastMastitisOccurences();
        void doRemoveMastitisOccurencesObsoleteHistory(const boost::gregorian::date &date);
    public:
        virtual void removeMastitisOccurencesObsoleteHistory(const boost::gregorian::date &date);
        void addMastitisOccurence(FunctionalEnumerations::Health::BacteriumType bacterium, const boost::gregorian::date &date);
        unsigned int getMastitisHistory(FunctionalEnumerations::Health::BacteriumType bacterium, const boost::gregorian::date &date);
        unsigned int getBornAnimalDuringThisCampaign(const boost::gregorian::date &date);
        virtual inline bool hasEfficientFootBathToday(const boost::gregorian::date &simDate) {return false;}
        virtual inline float getInfectiousLamenessPrevalence(){return 0.0f;}
//        void notifyForLamenessDetectionOrVerificationToday(DataStructures::Animal* pAnim);
		
        // Accounting
        virtual float getStrawConsumptionOfTheDay() = 0;
        
        // Feeding
        virtual void manageAnimalFeedConsumption(DataStructures::Animal* pAnim)=0;

   };
} /* End of namespace Population */
}
#endif // Population_Batch_h
