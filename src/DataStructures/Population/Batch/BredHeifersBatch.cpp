#include "BredHeifersBatch.h"

// boost

// project
#include "../../DairyMammal.h"
#include "../../DairyFarm.h"
#include "../../PregnancyProcess.h"
#include "../../../ExchangeInfoStructures/FunctionalConstants.h"
#include "../PopulationManagement.h"

BOOST_CLASS_EXPORT(DataStructures::Population::BredHeifersBatch) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(HeiferBatch); \

    IMPLEMENT_SERIALIZE_STD_METHODS(BredHeifersBatch);

    BredHeifersBatch::BredHeifersBatch(DataStructures::Herd* pHerd, Batch* pNextBatch) : HeiferBatch(pHerd, pNextBatch)
    {
    }
        
    BredHeifersBatch::~BredHeifersBatch()
    {
    }
	
    FunctionalEnumerations::Population::BatchType BredHeifersBatch::getType()
    {
        return FunctionalEnumerations::Population::BatchType::bredHeifers;
    }
    
    void BredHeifersBatch::notifyPredictedEndSuccessfullPregnancy(DataStructures::Animal* pAnim, const boost::gregorian::date &predictedEndDate)
    {
        // Plan the exit  
        boost::gregorian::date exitDate = predictedEndDate - boost::gregorian::days(FunctionalConstants::Population::DELAY_BEFORE_CALVING_TO_GO_IN_DRIED_COWS_BATCH_FOR_PREGNANT_HEIFERS);
        planExit(pAnim, exitDate);
    }
    
    void BredHeifersBatch::notifyUnsuccessfulPregnancyEnd(DataStructures::Animal* pAnim)
    {
        // Unsuccess pregnancy, we have to wait to the next one
        removePlannedExit(pAnim);
    }
    
    void BredHeifersBatch::includeAnimal(DataStructures::Animal* pAnim, const boost::gregorian::date &date)
    {         
        // Make the change
        Batch::includeAnimal(pAnim, date);
        
        // Plan a specificity error ?  
        ((DataStructures::DairyMammal*)pAnim)->planPotentialSpecificityError(date);
    }
    
    unsigned int BredHeifersBatch::getComingPregnantHeiferCountFromBredHeiferBatch(const boost::gregorian::date &date, unsigned int adultTarget)
    {
        unsigned int res = 0;
        unsigned int waitingDelay = (float)FunctionalConstants::Population::WAITING_DELAY_FOR_COMING_PREGNANT_HEIFER_BEFORE_PURCHASE_NEW_ONE * (float)adultTarget / (float)(100) + FunctionalConstants::Population::DELAY_BEFORE_CALVING_TO_GO_IN_DRIED_COWS_BATCH_FOR_PREGNANT_HEIFERS;
        for (auto it : getAnimalList())
        {
            DataStructures::DairyMammal* pDairyMammal = (DataStructures::DairyMammal*) it.second;
            if (pDairyMammal->pregnancyIsOngoing())
            {
                if (pDairyMammal->getDayDurationUntilPredictedEndPregnancyDate(date) <= waitingDelay)
                {
                   res++; 
                }
            }
        }
        return res;
    }
       
    void BredHeifersBatch::manageAnimalFeedConsumption(DataStructures::Animal* pAnim)
    {
        ((DataStructures::DairyMammal*)pAnim)->consumeForageConcentrateAndMineralVitaminBredHeifer(getCurrentLocation());
    }
    
    void BredHeifersBatch::changeCurrentLocation(const boost::gregorian::date &simDate)
    {
        setCurrentBatchLocationState(Population::PopulationManagement::getNewBatchLocationState(simDate, getDairyFarm()->getOtherHeiferAndDriedCowBeginPastureDate(), getDairyFarm()->getOtherHeiferAndDriedCowEndPastureDate()));
    }
}
} /* End of namespace Population */
