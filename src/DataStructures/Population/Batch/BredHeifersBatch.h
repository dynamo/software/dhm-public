#ifndef Population_BredHeifersBatch_h
#define Population_BredHeifersBatch_h

// Project
#include "HeiferBatch.h"

namespace DataStructures
{
namespace Population
{
    class BredHeifersBatch : public HeiferBatch
    {       
        DECLARE_SERIALIZE_STD_METHODS;
        
    protected:
        BredHeifersBatch(){} // serialization constructor
        void concrete(){}; // To allow instanciation
        
    public:
        
        BredHeifersBatch(DataStructures::Herd* pHerd, Batch* pNextBatch); // standard constructor
        virtual ~BredHeifersBatch();
        FunctionalEnumerations::Population::BatchType getType();
        void notifyPredictedEndSuccessfullPregnancy(DataStructures::Animal* pAnim, const boost::gregorian::date &predictedEndDate) override;
        void notifyUnsuccessfulPregnancyEnd(DataStructures::Animal* pAnim) override;        
        void includeAnimal(DataStructures::Animal* pAnim, const boost::gregorian::date &date) override;
        inline bool oestrusIsManaged() override {return true;}        
        unsigned int getComingPregnantHeiferCountFromBredHeiferBatch(const boost::gregorian::date &date, unsigned int adultTarget) override;
        void changeCurrentLocation(const boost::gregorian::date &simDate) override;
        
        // Feeding
        void manageAnimalFeedConsumption(DataStructures::Animal* pAnim);
        
   };
} /* End of namespace Population */
}
#endif // Population_BredHeifersBatch_h
