#include "CalfBatch.h"

// boost

// project
#include "../../DairyFarm.h"
#include "../LocationStates/BatchLocationState.h"

BOOST_CLASS_EXPORT(DataStructures::Population::CalfBatch) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Batch); \

    IMPLEMENT_SERIALIZE_STD_METHODS(CalfBatch);

    CalfBatch::CalfBatch(DataStructures::Herd* pHerd, Batch* pNextBatch) : Batch(pHerd, pNextBatch)
    {
    }
        
    CalfBatch::~CalfBatch()
    {
    }
	
    float CalfBatch::getStrawConsumptionOfTheDay()
    {
        return _pHasCurrentLocationState->getRealStrawConsumption(getDairyFarm()->getCalfDayStrawConsumption(), getAnimalCount());
    }

}


} /* End of namespace Population */
