#ifndef Population_CalfBatch_h
#define Population_CalfBatch_h

// boost

// standard

// Project
#include "Batch.h"

namespace DataStructures
{
namespace Population
{
    class CalfBatch : public Batch
    {       
        DECLARE_SERIALIZE_STD_METHODS;
    protected:
       
        CalfBatch(){} // serialization constructor
        
    public:
        CalfBatch(DataStructures::Herd* pHerd, Batch* pNextBatch); // standard constructor
        virtual ~CalfBatch();

        float getStrawConsumptionOfTheDay();
        void removeMastitisOccurencesObsoleteHistory(const boost::gregorian::date &date) override {};

	
	
   };
} /* End of namespace Population */
}
#endif // Population_CalfBatch_h
