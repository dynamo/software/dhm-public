#include "DriedCowsAndPregnantHeifersBatch.h"

// boost

// project
#include "../../DairyMammal.h"
#include "../../DairyHerd.h"
#include "../../DairyFarm.h"
#include "../../PregnancyProcess.h"
#include "../../../ExchangeInfoStructures/FunctionalConstants.h"
#include "../PopulationManagement.h"

BOOST_CLASS_EXPORT(DataStructures::Population::DriedCowsAndPregnantHeifersBatch) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(AdultBatch); \

    IMPLEMENT_SERIALIZE_STD_METHODS(DriedCowsAndPregnantHeifersBatch);

    DriedCowsAndPregnantHeifersBatch::DriedCowsAndPregnantHeifersBatch(DataStructures::Herd* pHerd, Batch* pNextBatch) : AdultBatch(pHerd, pNextBatch)
    {
    }
        
    DriedCowsAndPregnantHeifersBatch::~DriedCowsAndPregnantHeifersBatch()
    {
    }
	
    FunctionalEnumerations::Population::BatchType DriedCowsAndPregnantHeifersBatch::getType()
    {
        return FunctionalEnumerations::Population::BatchType::driedCowsAndPregnantHeifers;
    }
               
    void DriedCowsAndPregnantHeifersBatch::includeAnimal(DataStructures::Animal* pAnim, const boost::gregorian::date &date)
    { 
        
        // Make the change
        Batch::includeAnimal(pAnim, date);
              
        DataStructures::DairyMammal* pDairyMammal = (DataStructures::DairyMammal*)(pAnim);
        
        // Take account of the new commer
        if (pDairyMammal->getParity() == FunctionalEnumerations::Reproduction::DairyCowParity::heifer)
        {             
            pDairyMammal->getDairyHerd()->increaseAnnualNewComerInHerdCount();
        }

        // Plan the exit  
        assert (pDairyMammal->pregnancyIsOngoing());
        
        boost::gregorian::date exitDate = pDairyMammal->getPredictedEndPregnancyDate() - boost::gregorian::days(FunctionalConstants::Population::DELAY_BEFORE_CALVING_TO_GO_IN_PERIPARTURIENT_BATCH);
        
        planExit(pAnim, exitDate);
        
        // To trim if option is small groups and cow is not an heifer
        if (getDairyFarm()->getTrimmingOption() == FunctionalEnumerations::Health::LamenessPreventiveTrimmingOption::preventiveTrimmingSmallGroup and pDairyMammal->getParity() != FunctionalEnumerations::Reproduction::DairyCowParity::heifer)
        {
            if (not ((DairyMammal*)pAnim)->hadRecentTrimming(date))
            {
                //getDairyFarm()->getToTrimForLamenessPreventionGroup().push_back(pAnim);
                getDairyHerd()->addAnimalInGroup(pAnim, getDairyFarm()->getToTrimForLamenessPreventionGroup());
            }
        }
    }
        
    void DriedCowsAndPregnantHeifersBatch::excludeAnimal(DataStructures::Animal* pAnim, bool exit)
    {
        DataStructures::DairyMammal* pDairyMammal = (DataStructures::DairyMammal*)(pAnim);
        // Take account of the exit
        if (pDairyMammal->getParity() == FunctionalEnumerations::Reproduction::DairyCowParity::heifer and exit)
        {                
            pDairyMammal->getDairyHerd()->decreaseAnnualNewComerInHerdCount();
        }
        Batch::excludeAnimal(pAnim, exit);
    }

    void DriedCowsAndPregnantHeifersBatch::manageAnimalFeedConsumption(DataStructures::Animal* pAnim)
    {
        ((DataStructures::DairyMammal*)pAnim)->consumeForageConcentrateAndMineralVitaminDriedCow(getCurrentLocation());
    }
    
    void DriedCowsAndPregnantHeifersBatch::changeCurrentLocation(const boost::gregorian::date &simDate)
    {
        setCurrentBatchLocationState(Population::PopulationManagement::getNewBatchLocationState(simDate, getDairyFarm()->getOtherHeiferAndDriedCowBeginPastureDate(), getDairyFarm()->getOtherHeiferAndDriedCowEndPastureDate()));
    }
}

} /* End of namespace Population */
