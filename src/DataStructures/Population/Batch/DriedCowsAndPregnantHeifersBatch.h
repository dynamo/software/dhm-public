#ifndef Population_DriedCowsAndPregnantHeifersBatch_h
#define Population_DriedCowsAndPregnantHeifersBatch_h

// Project
#include "AdultBatch.h"

namespace DataStructures
{
namespace Population
{
    class DriedCowsAndPregnantHeifersBatch : public AdultBatch
    {       
        DECLARE_SERIALIZE_STD_METHODS;
        
    protected:
        DriedCowsAndPregnantHeifersBatch(){} // serialization constructor
        void concrete(){}; // To allow instanciation
        
    public:
        
        DriedCowsAndPregnantHeifersBatch(DataStructures::Herd* pHerd, Batch* pNextBatch); // standard constructor
        virtual ~DriedCowsAndPregnantHeifersBatch();
        FunctionalEnumerations::Population::BatchType getType();
        void includeAnimal(DataStructures::Animal* pAnim, const boost::gregorian::date &date) override;
        void excludeAnimal(DataStructures::Animal* pAnim, bool exit) override;
        void changeCurrentLocation(const boost::gregorian::date &simDate) override;
        
        // Feeding
        void manageAnimalFeedConsumption(DataStructures::Animal* pAnim);
        
   };
} /* End of namespace Population */
}
#endif // Population_DriedCowsAndPregnantHeifersBatch_h
