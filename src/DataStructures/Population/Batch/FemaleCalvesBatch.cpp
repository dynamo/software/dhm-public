#include "FemaleCalvesBatch.h"

// boost

// project
#include "../../DairyMammal.h"
#include "../../Herd.h"
#include "../../DairyFarm.h"

BOOST_CLASS_EXPORT(DataStructures::Population::FemaleCalvesBatch) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CalfBatch); \

    IMPLEMENT_SERIALIZE_STD_METHODS(FemaleCalvesBatch);

    FemaleCalvesBatch::FemaleCalvesBatch(DataStructures::Herd* pHerd, Batch* pNextBatch) : CalfBatch(pHerd, pNextBatch) // standard constructor
    {
    }
        
    FemaleCalvesBatch::~FemaleCalvesBatch()
    {
    }
    
    FunctionalEnumerations::Population::BatchType FemaleCalvesBatch::getType()
    {
        return FunctionalEnumerations::Population::BatchType::femaleCalves;
    }
    
    void FemaleCalvesBatch::includeAnimal(DataStructures::Animal* pAnim, const boost::gregorian::date &date)
    { 
        // Make the change
        Batch::includeAnimal(pAnim, date);
        
        // Plan the exit   
        boost::gregorian::date exitDate = pAnim->getBirthDate() + boost::gregorian::days(getHerd()->getDairyFarm()->getWeaningAge());
        
        planExit(pAnim, exitDate);
    }

    void FemaleCalvesBatch::manageAnimalFeedConsumption(DataStructures::Animal* pAnim)
    {
        DataStructures::DairyMammal* pMammal = ((DataStructures::DairyMammal*)pAnim);
        pMammal->consumeUnweanedCalfMilk();
        pMammal->consumeConcentrateUnweanedFemaleCalf();
    }
}

} /* End of namespace Population */
