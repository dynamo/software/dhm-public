#ifndef Population_FemaleCalvesBatch_h
#define Population_FemaleCalvesBatch_h

// Project
#include "CalfBatch.h"

namespace DataStructures
{
namespace Population
{
    class FemaleCalvesBatch : public CalfBatch
    {       
        DECLARE_SERIALIZE_STD_METHODS;
        
    protected:
        FemaleCalvesBatch(){} // serialization constructor
        void concrete(){}; // To allow instanciation
        
    public:
        
        FemaleCalvesBatch(DataStructures::Herd* pHerd, Batch* pNextBatch);
        virtual ~FemaleCalvesBatch();
        FunctionalEnumerations::Population::BatchType getType();
        void includeAnimal(DataStructures::Animal* pAnim, const boost::gregorian::date &date) override;
        
        // Feeding
        void manageAnimalFeedConsumption(DataStructures::Animal* pAnim);
        
   };
} /* End of namespace Population */
}
#endif // Population_FemaleCalvesBatch_h
