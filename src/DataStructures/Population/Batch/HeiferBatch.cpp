#include <regex>

#include "HeiferBatch.h"

// boost

// project
#include "../../DairyFarm.h"
#include "../LocationStates/BatchLocationState.h"

BOOST_CLASS_EXPORT(DataStructures::Population::HeiferBatch) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Batch); \

    IMPLEMENT_SERIALIZE_STD_METHODS(HeiferBatch);

    HeiferBatch::HeiferBatch(DataStructures::Herd* pHerd, Batch* pNextBatch) : Batch(pHerd, pNextBatch)
    {
        initLastMastitisOccurences();
    }
        
    HeiferBatch::~HeiferBatch()
    {
    }
	
    float HeiferBatch::getStrawConsumptionOfTheDay()
    {
        return _pHasCurrentLocationState->getRealStrawConsumption(getDairyFarm()->getHeiferDayStrawConsumption(), getAnimalCount());
    }

}
} /* End of namespace Population */
