#ifndef Population_HeiferBatch_h
#define Population_HeiferBatch_h

// boost

// standard

// Project
#include "Batch.h"

namespace DataStructures
{
namespace Population
{
    class HeiferBatch : public Batch
    {       
        DECLARE_SERIALIZE_STD_METHODS;
    protected:
       
        HeiferBatch(){} // serialization constructor
        
    public:
        HeiferBatch(DataStructures::Herd* pHerd, Batch* pNextBatch); // standard constructor
        virtual ~HeiferBatch();

        float getStrawConsumptionOfTheDay();
   };
} /* End of namespace Population */
}
#endif // Population_HeiferBatch_h
