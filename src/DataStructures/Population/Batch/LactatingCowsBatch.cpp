#include "LactatingCowsBatch.h"

// boost

// project
#include "../PopulationManagement.h"
#include "../../DairyFarm.h"
#include "../../DairyMammal.h"
#include "../../DairyHerd.h"

BOOST_CLASS_EXPORT(DataStructures::Population::LactatingCowsBatch) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(AdultBatch); \
    ar & BOOST_SERIALIZATION_NVP(_footBathDate); \

    IMPLEMENT_SERIALIZE_STD_METHODS(LactatingCowsBatch);

    LactatingCowsBatch::LactatingCowsBatch(DataStructures::Herd* pHerd) : AdultBatch(pHerd, nullptr) // No automatic next batch
    {
    }
        
    LactatingCowsBatch::~LactatingCowsBatch()
    {
    }
    
    void LactatingCowsBatch::progress(const boost::gregorian::date &simDate)
    {
         // Base part
        AdultBatch::progress(simDate);

        // Foot bath to refresh ?
        if (not _footBathDate.is_not_a_date())
        {
            // We have currently a foot bath, is it to refresh ?
            boost::gregorian::date_period footBathPeriod(_footBathDate, simDate);
            if (((unsigned int)footBathPeriod.length().days()) >= getDairyFarm()->getFootBathFrequencyInDays(getCurrentLocation()))
            {
                installFootBathToday(simDate);
            }
        }
    }

    bool LactatingCowsBatch::hasEfficientFootBathToday(const boost::gregorian::date &simDate)
    {
        return simDate == _footBathDate;
    }
    
    FunctionalEnumerations::Population::BatchType LactatingCowsBatch::getType()
    {
        return FunctionalEnumerations::Population::BatchType::lactatingCows;
    }
    
    void LactatingCowsBatch::manageAnimalFeedConsumption(DataStructures::Animal* pAnim)
    {        
        ((DataStructures::DairyMammal*)pAnim)->consumeForageConcentrateAndMineralVitaminLactatingCow(pAnim->getBatch()->getCurrentLocation());
    }
    
    void LactatingCowsBatch::changeCurrentLocation(const boost::gregorian::date &simDate)
    {
        bool fullPasture, fullStabulation;
        setCurrentBatchLocationState(Population::PopulationManagement::getNewLactatingCowBatchLocationState(simDate, getDairyFarm(), fullPasture, fullStabulation));
        
        // Preventive trimming ?
        FunctionalEnumerations::Health::LamenessPreventiveTrimmingOption lto = getDairyFarm()->getTrimmingOption();
        if (
            (fullStabulation and (lto == FunctionalEnumerations::Health::LamenessPreventiveTrimmingOption::preventiveTrimmingOnceAYear or lto == FunctionalEnumerations::Health::LamenessPreventiveTrimmingOption::preventiveTrimmingTwiceAYear))
            or
            (fullPasture and lto == FunctionalEnumerations::Health::LamenessPreventiveTrimmingOption::preventiveTrimmingTwiceAYear)
            )
        {
            // Trimming all the lactating cows
            for (auto it : getAnimalList())
            {
                DairyMammal* pAnim = (DairyMammal*) it.second;
                if (not pAnim->hadRecentTrimming(simDate))
                {
                    //getDairyFarm()->getToTrimForLamenessPreventionGroup().push_back(pAnim);
                    getDairyHerd()->addAnimalInGroup(pAnim, getDairyFarm()->getToTrimForLamenessPreventionGroup());

                }
            }
        }
    }
    
    void LactatingCowsBatch::includeAnimal(DataStructures::Animal* pAnim, const boost::gregorian::date &date)
    {
        // Base part
        Batch::includeAnimal(pAnim, date);
        
        // Foot batch to install
        if (getAnimalCount() == 1)
        {
            // First animal in the batch
            if (getDairyFarm()->useFootBath() and _footBathDate.is_not_a_date())
            {
                // The farm use foot bath and we don't have yet
                installFootBathToday(date);
            }
        }
    }
    
    void LactatingCowsBatch::excludeAnimal(DataStructures::Animal* pAnim, bool exit)
    {
        // Base part
        Batch::excludeAnimal(pAnim, exit);
    }
    
    void LactatingCowsBatch::installFootBathToday(const boost::gregorian::date &date)
    {
        getHerd()->getDairyFarm()->installFootBath(
#ifdef _LOG
                                    date
#endif // _LOG
                                   );
        _footBathDate = date; // We install a foot bath today
    }
    
}
} /* End of namespace Population */
