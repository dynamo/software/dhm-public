#ifndef Population_LactatingCowsBatch_h
#define Population_LactatingCowsBatch_h

// Project
#include "AdultBatch.h"
#include "../../DairyMammal.h"

namespace DataStructures
{
namespace Population
{
    class LactatingCowsBatch : public AdultBatch
    {       
        DECLARE_SERIALIZE_STD_METHODS;

        // Foot bath date (not_a_date_time = no current efficient foot bath
        boost::gregorian::date _footBathDate;
         
        void installFootBathToday(const boost::gregorian::date &date);

    protected:
        LactatingCowsBatch(){} // serialization constructor
        void concrete(){}; // To allow instanciation
        
    public:
        
        LactatingCowsBatch(DataStructures::Herd* pHerd); // standard constructor
        virtual ~LactatingCowsBatch();
        FunctionalEnumerations::Population::BatchType getType();
        bool hasEfficientFootBathToday(const boost::gregorian::date &simDate) override;

        inline bool oestrusIsManaged() override {return true;}        
        void changeCurrentLocation(const boost::gregorian::date &simDate) override;
        void progress(const boost::gregorian::date &simDate) override;
               
        void includeAnimal(DataStructures::Animal* pAnim, const boost::gregorian::date &date) override;
        void excludeAnimal(DataStructures::Animal* pAnim, bool exit) override;
       
        // Feeding
        void manageAnimalFeedConsumption(DataStructures::Animal* pAnim);
        
   };
} /* End of namespace Population */
}
#endif // Population_LactatingCowsBatch_h
