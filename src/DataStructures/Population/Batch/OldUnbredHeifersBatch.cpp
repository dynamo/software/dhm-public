#include "OldUnbredHeifersBatch.h"

// boost

// project
#include "../../Herd.h"
#include "../../DairyFarm.h"
#include "../../DairyMammal.h"
#include "../PopulationManagement.h"


BOOST_CLASS_EXPORT(DataStructures::Population::OldUnbredHeifersBatch) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(HeiferBatch); \

    IMPLEMENT_SERIALIZE_STD_METHODS(OldUnbredHeifersBatch);

    OldUnbredHeifersBatch::OldUnbredHeifersBatch(DataStructures::Herd* pHerd, Batch* pNextBatch) : HeiferBatch(pHerd, pNextBatch)
    {
    }
        
    OldUnbredHeifersBatch::~OldUnbredHeifersBatch()
    {
    }
	
    FunctionalEnumerations::Population::BatchType OldUnbredHeifersBatch::getType()
    {
        return FunctionalEnumerations::Population::BatchType::oldUnbredHeifers;
    }
    
    void OldUnbredHeifersBatch::includeAnimal(DataStructures::Animal* pAnim, const boost::gregorian::date &date)
    { 
        // Make the change
        Batch::includeAnimal(pAnim, date);
        
        // Plan the exit
        boost::gregorian::date exitDate = pAnim->getBirthDate() + boost::gregorian::days(getHerd()->getDairyFarm()->getAgeToBreedingDecision());
        planExit(pAnim, exitDate);
    }

    void OldUnbredHeifersBatch::manageAnimalFeedConsumption(DataStructures::Animal* pAnim)
    {
        ((DataStructures::DairyMammal*)pAnim)->consumeForageConcentrateAndMineralVitaminOldHeifer(getCurrentLocation());
    }
    
    void OldUnbredHeifersBatch::changeCurrentLocation(const boost::gregorian::date &simDate)
    {
        setCurrentBatchLocationState(Population::PopulationManagement::getNewBatchLocationState(simDate, getDairyFarm()->getOtherHeiferAndDriedCowBeginPastureDate(), getDairyFarm()->getOtherHeiferAndDriedCowEndPastureDate()));
    }
}
} /* End of namespace Population */
