#ifndef Population_OldUnbredHeifersBatch_h
#define Population_OldUnbredHeifersBatch_h

// Project
#include "HeiferBatch.h"

namespace DataStructures
{
namespace Population
{
    class OldUnbredHeifersBatch : public HeiferBatch
    {       
        DECLARE_SERIALIZE_STD_METHODS;
        
    protected:
        OldUnbredHeifersBatch(){} // serialization constructor
        void concrete(){}; // To allow instanciation
        
    public:
        
        OldUnbredHeifersBatch(DataStructures::Herd* pHerd, Batch* pNextBatch); // standard constructor
        virtual ~OldUnbredHeifersBatch();
        FunctionalEnumerations::Population::BatchType getType();
        void includeAnimal(DataStructures::Animal* pAnim, const boost::gregorian::date &date) override;
        void changeCurrentLocation(const boost::gregorian::date &simDate) override;
        
        // Feeding
        void manageAnimalFeedConsumption(DataStructures::Animal* pAnim);
   };
} /* End of namespace Population */
}
#endif // Population_OldUnbredHeifersBatch_h
