#include "PeriParturientFemalesBatch.h"

// boost

// project
#include "../../DairyMammal.h"
#include "../../DairyHerd.h"
#include "../../DairyFarm.h"
#include "../PopulationManagement.h"

BOOST_CLASS_EXPORT(DataStructures::Population::PeriParturientFemalesBatch) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(AdultBatch); \

    IMPLEMENT_SERIALIZE_STD_METHODS(PeriParturientFemalesBatch);

    PeriParturientFemalesBatch::PeriParturientFemalesBatch(DataStructures::Herd* pHerd, Batch* pNextBatch) : AdultBatch(pHerd, pNextBatch)
    {
    }
        
    PeriParturientFemalesBatch::~PeriParturientFemalesBatch()
    {
    }
	
    FunctionalEnumerations::Population::BatchType PeriParturientFemalesBatch::getType()
    {
        return FunctionalEnumerations::Population::BatchType::periParturientFemales;
    }
    
    void PeriParturientFemalesBatch::manageAnimalFeedConsumption(DataStructures::Animal* pAnim)
    {
        ((DataStructures::DairyMammal*)pAnim)->consumeForageConcentrateAndMineralVitaminPeriParturientCow(getCurrentLocation());
    }
    
    void PeriParturientFemalesBatch::changeCurrentLocation(const boost::gregorian::date &simDate)
    {
        setCurrentBatchLocationState(Population::PopulationManagement::getNewBatchLocationState(simDate, getDairyFarm()->getOtherHeiferAndDriedCowBeginPastureDate(), getDairyFarm()->getOtherHeiferAndDriedCowEndPastureDate()));
    }
}
} /* End of namespace Population */
