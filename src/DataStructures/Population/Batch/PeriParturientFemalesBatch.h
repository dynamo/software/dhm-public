#ifndef Population_PeriParturientFemalesBatch_h
#define Population_PeriParturientFemalesBatch_h

// Project
#include "AdultBatch.h"

namespace DataStructures
{
namespace Population
{
    class PeriParturientFemalesBatch : public AdultBatch
    {       
        DECLARE_SERIALIZE_STD_METHODS;
        
    protected:
        PeriParturientFemalesBatch(){} // serialization constructor
        void concrete(){}; // To allow instanciation
        
    public:
        
        PeriParturientFemalesBatch(DataStructures::Herd* pHerd, Batch* pNextBatch); // standard constructor
        virtual ~PeriParturientFemalesBatch();
        FunctionalEnumerations::Population::BatchType getType();
        void changeCurrentLocation(const boost::gregorian::date &simDate) override;

        // Feeding
        void manageAnimalFeedConsumption(DataStructures::Animal* pAnim);
};
} /* End of namespace Population */
}
#endif // Population_PeriParturientFemalesBatch_h
