#include "ToSaleCalvesBatch.h"

// boost

// project
#include "../../DairyMammal.h"

BOOST_CLASS_EXPORT(DataStructures::Population::ToSaleCalvesBatch) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CalfBatch); \

    IMPLEMENT_SERIALIZE_STD_METHODS(ToSaleCalvesBatch);

    ToSaleCalvesBatch::ToSaleCalvesBatch(DataStructures::Herd* pHerd) : CalfBatch(pHerd, nullptr) // No automatic next batch
    {
    }
        
    ToSaleCalvesBatch::~ToSaleCalvesBatch()
    {
    }
    
    FunctionalEnumerations::Population::BatchType ToSaleCalvesBatch::getType()
    {
        return FunctionalEnumerations::Population::BatchType::toSaleCalves;
    }
        
    void ToSaleCalvesBatch::manageAnimalFeedConsumption(DataStructures::Animal* pAnim)
    {
        ((DataStructures::DairyMammal*)pAnim)->consumeUnweanedCalfMilk();
    }
}
} /* End of namespace Population */
