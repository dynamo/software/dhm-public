#ifndef Population_ToSaleCalvesBatch_h
#define Population_ToSaleCalvesBatch_h

// Project
#include "CalfBatch.h"

namespace DataStructures
{
namespace Population
{
    class ToSaleCalvesBatch : public CalfBatch
    {       
        DECLARE_SERIALIZE_STD_METHODS;
        
    protected:
        ToSaleCalvesBatch(){} // serialization constructor
        void concrete(){}; // To allow instanciation
        
    public:

        ToSaleCalvesBatch(DataStructures::Herd* pHerd); //, const std::string name); // standard constructor
        virtual ~ToSaleCalvesBatch();
        FunctionalEnumerations::Population::BatchType getType();
                
        // Feeding
        void manageAnimalFeedConsumption(DataStructures::Animal* pAnim);
   };
} /* End of namespace Population */
}
#endif // Population_ToSaleCalvesBatch_h
