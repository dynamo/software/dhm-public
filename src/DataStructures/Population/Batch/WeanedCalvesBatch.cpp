#include "WeanedCalvesBatch.h"

// boost

// project
#include "../../DairyMammal.h"
#include "../../../ExchangeInfoStructures/FunctionalConstants.h"


BOOST_CLASS_EXPORT(DataStructures::Population::WeanedCalvesBatch) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CalfBatch); \

    IMPLEMENT_SERIALIZE_STD_METHODS(WeanedCalvesBatch);

    WeanedCalvesBatch::WeanedCalvesBatch(DataStructures::Herd* pHerd, Batch* pNextBatch) : CalfBatch(pHerd, pNextBatch)//, const std::string name) // standard constructor
    {
    }
        
    WeanedCalvesBatch::~WeanedCalvesBatch()
    {
    }
    
    FunctionalEnumerations::Population::BatchType WeanedCalvesBatch::getType()
    {
        return FunctionalEnumerations::Population::BatchType::weanedCalves;
    }
        
    void WeanedCalvesBatch::includeAnimal(DataStructures::Animal* pAnim, const boost::gregorian::date &date)
    { 
        // Make the change
        Batch::includeAnimal(pAnim, date);
        
        // Plan the exit   
        boost::gregorian::date exitDate = pAnim->getBirthDate() + boost::gregorian::days(FunctionalConstants::Population::AGE_TO_GO_IN_YOUNG_UNBRED_HEIFERS_BATCH);
        planExit(pAnim, exitDate);
    }

    void WeanedCalvesBatch::manageAnimalFeedConsumption(DataStructures::Animal* pAnim)
    {
        ((DataStructures::DairyMammal*)pAnim)->consumeForageConcentrateAndMineralVitaminWeanedFemaleCalf();
    }
}
} /* End of namespace Population */
