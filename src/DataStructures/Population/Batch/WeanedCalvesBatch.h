#ifndef Population_WeanedCalvesBatch_h
#define Population_WeanedCalvesBatch_h

// Project
#include "CalfBatch.h"

namespace DataStructures
{
namespace Population
{
    class WeanedCalvesBatch : public CalfBatch
    {       
        DECLARE_SERIALIZE_STD_METHODS;
        
    protected:
        WeanedCalvesBatch(){} // serialization constructor
        void concrete(){}; // To allow instanciation
        
    public:
        
        WeanedCalvesBatch(DataStructures::Herd* pHerd, Batch* pNextBatch); //, const std::string name); // standard constructor
        virtual ~WeanedCalvesBatch();
        FunctionalEnumerations::Population::BatchType getType();
        void includeAnimal(DataStructures::Animal* pAnim, const boost::gregorian::date &date) override;
        
        // Feeding
        void manageAnimalFeedConsumption(DataStructures::Animal* pAnim);
        
   };
} /* End of namespace Population */
}
#endif // Population_WeanedCalvesBatch_h
