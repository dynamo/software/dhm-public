#include "YoungUnbredHeifersBatch.h"

// boost

// project
#include "../../DairyMammal.h"
#include "../../DairyFarm.h"
#include "../PopulationManagement.h"

BOOST_CLASS_EXPORT(DataStructures::Population::YoungUnbredHeifersBatch) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(HeiferBatch); \
    ar & BOOST_SERIALIZATION_NVP(_ageToLeave); \

    IMPLEMENT_SERIALIZE_STD_METHODS(YoungUnbredHeifersBatch);

    YoungUnbredHeifersBatch::YoungUnbredHeifersBatch(DataStructures::Herd* pHerd, Batch* pNextBatch, unsigned int ageToLeave) : HeiferBatch(pHerd, pNextBatch)
    {
        _ageToLeave = ageToLeave;
    }
        
    YoungUnbredHeifersBatch::~YoungUnbredHeifersBatch()
    {
    }
	
    FunctionalEnumerations::Population::BatchType YoungUnbredHeifersBatch::getType()
    {
        return FunctionalEnumerations::Population::BatchType::youngUnbredHeifers;
    }
       
    void YoungUnbredHeifersBatch::includeAnimal(DataStructures::Animal* pAnim, const boost::gregorian::date &date)
    { 
        // Make the change
        Batch::includeAnimal(pAnim, date);
        
        // Plan the exit   
        boost::gregorian::date exitDate = pAnim->getBirthDate() + boost::gregorian::days(_ageToLeave);
        planExit(pAnim, exitDate);
    }

    void YoungUnbredHeifersBatch::manageAnimalFeedConsumption(DataStructures::Animal* pAnim)
    {
        ((DataStructures::DairyMammal*)pAnim)->consumeForageConcentrateAndMineralVitaminYoungHeifer(getCurrentLocation());
    }
    
    void YoungUnbredHeifersBatch::changeCurrentLocation(const boost::gregorian::date &simDate)
    {
        setCurrentBatchLocationState(Population::PopulationManagement::getNewBatchLocationState(simDate, getDairyFarm()->getYoungHeiferBeginPastureDate(), getDairyFarm()->getYoungHeiferEndPastureDate()));
    }
}
} /* End of namespace Population */
