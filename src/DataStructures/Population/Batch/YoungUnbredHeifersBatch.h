#ifndef Population_YoungUnbredHeifersBatch_h
#define Population_YoungUnbredHeifersBatch_h

// Project
#include "HeiferBatch.h"

namespace DataStructures
{
namespace Population
{
    class YoungUnbredHeifersBatch : public HeiferBatch
    {   
        unsigned int _ageToLeave = 0;
        
        DECLARE_SERIALIZE_STD_METHODS;
        
    protected:
        YoungUnbredHeifersBatch(){} // serialization constructor
        void concrete(){}; // To allow instanciation
        
    public:
        
        YoungUnbredHeifersBatch(DataStructures::Herd* pHerd, Batch* pNextBatch, unsigned int ageToLeave); // standard constructor
        virtual ~YoungUnbredHeifersBatch();
        FunctionalEnumerations::Population::BatchType getType();
        void includeAnimal(DataStructures::Animal* pAnim, const boost::gregorian::date &date) override;
        void changeCurrentLocation(const boost::gregorian::date &simDate) override;
              
        // Feeding
        void manageAnimalFeedConsumption(DataStructures::Animal* pAnim);
    };
} /* End of namespace Population */
}
#endif // Population_YoungUnbredHeifersBatch_h
