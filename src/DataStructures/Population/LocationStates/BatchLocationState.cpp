#include "BatchLocationState.h"

// boost

// project
#include "I_LocationBatch.h"

BOOST_CLASS_EXPORT(DataStructures::Population::BatchLocationStates::BatchLocationState) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    namespace BatchLocationStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_NVP(_location); \
        ar & BOOST_SERIALIZATION_NVP(_beginDate); \
        ar & BOOST_SERIALIZATION_NVP(_predictedEndDate); \
//        
        /* To do        */
 
        IMPLEMENT_SERIALIZE_STD_METHODS(BatchLocationState);
        
        BatchLocationState::BatchLocationState(const boost::gregorian::date &simDate, const boost::gregorian::date &endDate, FunctionalEnumerations::Population::Location location)
        {
            _beginDate = simDate;
            _predictedEndDate = endDate;
            _location = location;
        }
        
        bool BatchLocationState::isOutOfTime(const boost::gregorian::date &baseDate)
        {
            return baseDate > _predictedEndDate;
        }
        
        BatchLocationState::~BatchLocationState()
        {
        }
            
        unsigned int BatchLocationState::getDayDurationSinceBegin(const boost::gregorian::date &simDate)
        {
            boost::gregorian::date_period period(_beginDate, simDate);
            return period.length().days(); 
        }

        void BatchLocationState::update(I_LocationBatch* pBatch, const boost::gregorian::date &simDate)
        {
            if (isOutOfTime(simDate))
            {
                // Next state
                pBatch->changeCurrentLocation(simDate);
            }
        }
        
        FunctionalEnumerations::Population::Location BatchLocationState::getLocation()
        {
            return _location;
        }

    } /* End of namespace DataStructures::States */
}           
} /* End of namespace DataStructures */
