#ifndef Population_BatchLocationStates_BatchLocationState_h
#define Population_BatchLocationStates_BatchLocationState_h

// boost
#include <boost/date_time/gregorian/gregorian.hpp>

// project
#include "../../../Tools/Serialization.h"
#include "../../../ExchangeInfoStructures/FunctionalEnumerations.h"

namespace DataStructures
{
namespace Population
{
    namespace BatchLocationStates
    {
        class I_LocationBatch;
        
        class BatchLocationState
        {
        DECLARE_SERIALIZE_STD_METHODS;

            boost::gregorian::date _beginDate;
            boost::gregorian::date _predictedEndDate;
            FunctionalEnumerations::Population::Location _location;

            bool isOutOfTime(const boost::gregorian::date &simDate);
        protected:
            BatchLocationState(){};
            virtual void concrete() = 0; // To ensure that it will not be instantiated

        public:
            BatchLocationState(const boost::gregorian::date &simDate, const boost::gregorian::date &endDate, FunctionalEnumerations::Population::Location location);
            virtual ~BatchLocationState();
            FunctionalEnumerations::Population::Location getLocation();
            unsigned int getDayDurationSinceBegin(const boost::gregorian::date &simDate);
            void update(I_LocationBatch* pBatch, const boost::gregorian::date &simDate);
            virtual float getRealStrawConsumption(float theoricIndividualConsumption, unsigned int animalCount) = 0;
 
        };
    }
} /* End of namespace Production */
}
#endif // Population_BatchLocationStates_BatchLocationState_h
