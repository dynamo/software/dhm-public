#include "FullPastureBatchLocationState.h"

// project
#include "I_LocationBatch.h"

BOOST_CLASS_EXPORT(DataStructures::Population::BatchLocationStates::FullPastureBatchLocationState) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    namespace BatchLocationStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(BatchLocationState); \

        IMPLEMENT_SERIALIZE_STD_METHODS(FullPastureBatchLocationState);

        FullPastureBatchLocationState::FullPastureBatchLocationState(const boost::gregorian::date &simDate, const boost::gregorian::date &endDate) : BatchLocationState(simDate, endDate, FunctionalEnumerations::Population::Location::fullPasture)
        {
        }

        FullPastureBatchLocationState::~FullPastureBatchLocationState()
        {
        }
    }
}
}