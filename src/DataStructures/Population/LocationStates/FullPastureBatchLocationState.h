#ifndef Population_BatchLocationStates_FullPastureBatchLocationState_h
#define Population_BatchLocationStates_FullPastureBatchLocationState_h

// project
#include "BatchLocationState.h"

namespace DataStructures
{
namespace Population
{
    namespace BatchLocationStates
    {
        class FullPastureBatchLocationState : public BatchLocationState
        {

        DECLARE_SERIALIZE_STD_METHODS;

        protected:
            void concrete(){}; // To allow instanciation

        public:
            FullPastureBatchLocationState(){};
            FullPastureBatchLocationState(const boost::gregorian::date &simDate, const boost::gregorian::date &endDate);
            virtual ~FullPastureBatchLocationState();
            float getRealStrawConsumption(float theoricIndividualConsumption, unsigned int animalCount) {return 0.0f;}

        };
    }
}
}
#endif // Population_BatchLocationStates_FullPastureBatchLocationState_h
