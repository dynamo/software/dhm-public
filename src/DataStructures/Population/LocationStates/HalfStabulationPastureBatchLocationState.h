#ifndef Population_BatchLocationStates_HalfStabulationPastureBatchLocationState_h
#define Population_BatchLocationStates_HalfStabulationPastureBatchLocationState_h

// project
#include "BatchLocationState.h"

namespace DataStructures
{
namespace Population
{
    namespace BatchLocationStates
    {
        class HalfStabulationPastureBatchLocationState : public BatchLocationState
        {

        DECLARE_SERIALIZE_STD_METHODS;

        protected:
            void concrete(){}; // To allow instanciation

        public:
            HalfStabulationPastureBatchLocationState(){};
            HalfStabulationPastureBatchLocationState(const boost::gregorian::date &simDate, const boost::gregorian::date &endDate);
            virtual ~HalfStabulationPastureBatchLocationState();
            float getRealStrawConsumption(float theoricIndividualConsumption, unsigned int animalCount) {return theoricIndividualConsumption * animalCount / 2;}

        };
    }
}
}
#endif // Population_BatchLocationStates_HalfStabulationPastureBatchLocationState_h
