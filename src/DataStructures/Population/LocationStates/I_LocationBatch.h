#ifndef Population_BatchLocationStates_I_LocationBatch_h
#define Population_BatchLocationStates_I_LocationBatch_h

// boost

// project

namespace DataStructures
{
namespace Population
{
    namespace BatchLocationStates
    {
        class BatchLocationState;
   
        class I_LocationBatch
        {
        public:
            
            // virtual destructor for interface 
            virtual ~I_LocationBatch() { }
            virtual void setCurrentBatchLocationState(Population::BatchLocationStates::BatchLocationState* pState) = 0;
            virtual void changeCurrentLocation(const boost::gregorian::date &simDate) = 0;

        };

    }
}
}
#endif // Population_BatchLocationStates_I_LocationBatch_h
