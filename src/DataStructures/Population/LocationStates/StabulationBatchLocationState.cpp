#include "StabulationBatchLocationState.h"

// project
#include "I_LocationBatch.h"
//#include "PastureBatchLocationState.h"

BOOST_CLASS_EXPORT(DataStructures::Population::BatchLocationStates::StabulationBatchLocationState) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    namespace BatchLocationStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(BatchLocationState); \


        IMPLEMENT_SERIALIZE_STD_METHODS(StabulationBatchLocationState);

        StabulationBatchLocationState::StabulationBatchLocationState(const boost::gregorian::date &simDate, const boost::gregorian::date &endDate) : BatchLocationState(simDate, endDate, FunctionalEnumerations::Population::Location::stabulation)
        {
        }

        StabulationBatchLocationState::~StabulationBatchLocationState()
        {
        }
    }
}
}