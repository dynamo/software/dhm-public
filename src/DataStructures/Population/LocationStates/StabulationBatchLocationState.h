#ifndef Population_BatchLocationStates_StabulationBatchLocationState_h
#define Population_BatchLocationStates_StabulationBatchLocationState_h

// project
#include "BatchLocationState.h"

namespace DataStructures
{
namespace Population
{
    namespace BatchLocationStates
    {
        class StabulationBatchLocationState : public BatchLocationState
        {

        DECLARE_SERIALIZE_STD_METHODS;

        protected:
            void concrete(){}; // To allow instanciation

        public:
            StabulationBatchLocationState(){};
            StabulationBatchLocationState(const boost::gregorian::date &simDate, const boost::gregorian::date &endDate);
            virtual ~StabulationBatchLocationState();
            float getRealStrawConsumption(float theoricIndividualConsumption, unsigned int animalCount) {return theoricIndividualConsumption * animalCount;}
        };
    }
}
}
#endif // Population_BatchLocationStates_StabulationBatchLocationState_h
