#include "Birth.h"

// project
#include "../../Animal.h"
#include "../../Herd.h"

BOOST_CLASS_EXPORT(DataStructures::Population::Birth) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Entrance);

    IMPLEMENT_SERIALIZE_STD_METHODS(Birth);
    
    Birth::Birth(DataStructures::Animal* pAnim
#ifdef _LOG
                                            , const boost::gregorian::date &birthDate
    
#endif // _LOG
                                            , float amount, float &accountingValueToUpdate) : Entrance(pAnim
#ifdef _LOG
                                            , birthDate, FunctionalConstants::AccountingModel::BIRTH_TRANSACTION_TYPE
    
#endif // _LOG
                                            , amount, accountingValueToUpdate)
    {
        pAnim->getHerd()->increaseAnnualBirthCount();
    }
}

} /* End of namespace Population */