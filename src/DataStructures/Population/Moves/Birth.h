#ifndef Population_Birth_h
#define Population_Birth_h

// project
#include "Entrance.h"

namespace DataStructures
{
    class Animal;

namespace Population
{
    class Birth : public Entrance
    {
        DECLARE_SERIALIZE_STD_METHODS;

    protected:
        Birth(){} // serialization constructor
        void concrete(){}; // To allow instanciation

    public:
        Birth(DataStructures::Animal* pAnim
#ifdef _LOG
                                            , const boost::gregorian::date &birthDate 
#endif // _LOG
                                            , float amount, float &accountingValueToUpdate);
        virtual ~Birth(){}; // Destructor

    };
} /* End of namespace Population*/
}
#endif // Population_Birth_h
