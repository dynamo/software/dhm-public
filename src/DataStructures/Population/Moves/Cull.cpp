#include "Cull.h"

// project
#include "../../Animal.h"
#include "../../Herd.h"
#include "../../DairyHerd.h"

BOOST_CLASS_EXPORT(DataStructures::Population::Cull) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Exit);

    IMPLEMENT_SERIALIZE_STD_METHODS(Cull);
    
    Cull::Cull(DataStructures::Animal* pAnim, const boost::gregorian::date &cullDate, float &accountingValueToUpdate)
    {
        float priceFactor = 1.0f;
        pAnim->getHerd()->updateCullingResults(pAnim, cullDate);
        
        if (pAnim->getCullingStatus() == FunctionalEnumerations::Population::CullingStatus::toCullDueToUnfertility)
        {
            ((DataStructures::DairyHerd*)pAnim->getHerd())->increaseReproductionTroubleCullingCount();
        }
        else if (pAnim->getCullingStatus() == FunctionalEnumerations::Population::CullingStatus::toCullDueToMastitis)
        {
            ((DataStructures::DairyHerd*)pAnim->getHerd())->increaseMastitisCullingCount();
        }
        else if (pAnim->getCullingStatus() == FunctionalEnumerations::Population::CullingStatus::toCullDueToKetosis)
        {
            ((DataStructures::DairyHerd*)pAnim->getHerd())->increaseKetosisCullingCount();
        }
        else if (pAnim->getCullingStatus() == FunctionalEnumerations::Population::CullingStatus::toCullDueToNonInfectiousLameness)
        {
            priceFactor = FunctionalConstants::Health::LAMENESS_CULLING_PRICE_FACTOR;
            ((DataStructures::DairyHerd*)pAnim->getHerd())->increaseNonInfectiousLamenessCullingCount();
         }
        else if (pAnim->getCullingStatus() == FunctionalEnumerations::Population::CullingStatus::toCullDueToInfectiousLameness)
        {
            ((DataStructures::DairyHerd*)pAnim->getHerd())->increaseInfectiousLamenessCullingCount();
        }
        
        // Generic part
        takeOut(pAnim
#ifdef _LOG
                , cullDate, FunctionalConstants::AccountingModel::CULLING_TRANSACTION_TYPE
#endif // _LOG      
                , accountingValueToUpdate, priceFactor);
    }    
} /* End of namespace Population*/
}