#ifndef Population_Cull_h
#define Population_Cull_h

// project
#include "Exit.h"

namespace DataStructures
{
    class Animal;

namespace Population
{
    class Cull : public Exit
    {
        DECLARE_SERIALIZE_STD_METHODS;

    protected:
        Cull(){} // serialization constructor
        void concrete(){}; // To allow instanciation

    public:
        Cull(DataStructures::Animal* pAnim, const boost::gregorian::date &cullDate, float &accountingValueToUpdate);
        virtual ~Cull(){}; // Destructor

    };
} /* End of namespace Population */
}
#endif // Population_Cull_h
