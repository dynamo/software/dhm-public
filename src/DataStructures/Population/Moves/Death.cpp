#include "Death.h"

// project
#include "../../Animal.h"
#include "../../Herd.h"

BOOST_CLASS_EXPORT(DataStructures::Population::Death) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Exit);

    IMPLEMENT_SERIALIZE_STD_METHODS(Death);
    
    Death::Death(DataStructures::Animal* pAnim, const boost::gregorian::date &deathDate, float &accountingValueToUpdate, FunctionalEnumerations::Population::DeathReason deathReason)
    {
        pAnim->getHerd()->increaseAnnualDeathCount(pAnim->getBatch(), deathReason);

        // Generic part
        takeOut(pAnim
#ifdef _LOG
                , deathDate, FunctionalConstants::AccountingModel::DEATH_TRANSACTION_TYPE
#endif // _LOG
               , accountingValueToUpdate);
    }   
} /* End of namespace Population */
}