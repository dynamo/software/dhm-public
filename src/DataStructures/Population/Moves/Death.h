#ifndef Population_Death_h
#define Population_Death_h

// project
#include "Exit.h"

namespace DataStructures
{
    class Animal;

namespace Population
{
    class Death : public Exit
    {
        DECLARE_SERIALIZE_STD_METHODS;

    protected:
        Death(){} // serialization constructor
        void concrete(){}; // To allow instanciation

    public:
        Death(DataStructures::Animal* pAnim, const boost::gregorian::date &deathDate, float &accountingValueToUpdate, FunctionalEnumerations::Population::DeathReason deathReason);
        virtual ~Death(){}; // Destructor
    };
} /* End of namespace Population*/
}

#endif // Population_Death_h
