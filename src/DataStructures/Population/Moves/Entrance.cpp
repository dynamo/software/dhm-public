#include "Entrance.h"

// project
#include "../../Animal.h"
#include "../../Herd.h"

BOOST_CLASS_EXPORT(DataStructures::Population::Entrance) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Move);

    IMPLEMENT_SERIALIZE_STD_METHODS(Entrance);
    
    Entrance::Entrance(DataStructures::Animal* pAnim
#ifdef _LOG
                                            , const boost::gregorian::date &entranceDate, const std::string &log
    
#endif // _LOG
                                            , float amount, float &accountingValueToUpdate)
    {
        pAnim->getHerd()->includeAnimal(pAnim);
        
        performsAccountingTransaction(pAnim
#ifdef _LOG
                                            , entranceDate, log 
#endif // _LOG
                                            , amount, accountingValueToUpdate);
    }
} /* End of namespace Population*/
}