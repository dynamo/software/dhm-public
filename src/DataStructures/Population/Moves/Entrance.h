#ifndef Population_Entrance_h
#define Population_Entrance_h

// project
#include "Move.h"

namespace DataStructures
{
    class Animal;

namespace Population
{
    class Entrance : public Move
    {
        DECLARE_SERIALIZE_STD_METHODS;
    protected:
        Entrance(){} // serialization constructor
    public:
        Entrance(DataStructures::Animal* pAnim
#ifdef _LOG
                                            , const boost::gregorian::date &entranceDate, const std::string &log
    
#endif // _LOG
                                            , float amount, float &accountingValueToUpdate);
        virtual ~Entrance(){}; // Destructor
    };
} /* End of namespace Population */
}
#endif // Population_Entrance_h
