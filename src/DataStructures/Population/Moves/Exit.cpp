#include "Exit.h"

// project
#include "../../Animal.h"
#include "../../DairyFarm.h"
#include "../../Herd.h"

BOOST_CLASS_EXPORT(DataStructures::Population::Exit) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Move);

    IMPLEMENT_SERIALIZE_STD_METHODS(Exit);

    void Exit::takeOut(DataStructures::Animal* pAnim
#ifdef _LOG
                                            , const boost::gregorian::date &exitDate, const std::string &log
#endif // _LOG
                                            , float &accountingValueToUpdate, float priceFactor) 
    {
        pAnim->getHerd()->excludeAnimal(pAnim);   

#ifdef _LOG                
        // for exit results
        Results::ExitResult er;
        er.id = pAnim->getUniqueId();
        er.date = exitDate;
        er.age = pAnim->getAge();
        er.sex = pAnim->getSex();
        er.bornInHerd = pAnim->isBornInHerd();
        er.reason = log;
        pAnim->getDairyFarm()->getCurrentResults()->getPopulationResults().ExitResults.push_back(er);      
#endif // _LOG        
        
        performsAccountingTransaction(pAnim
#ifdef _LOG
                                            , exitDate, log 
#endif // _LOG
                                            , pAnim->getExitAmount(), accountingValueToUpdate, priceFactor);
#ifdef _LOG                
        pAnim->closeResults(exitDate);
#endif // _LOG        
        delete pAnim;
    }

    Exit::~Exit()
    {

    }
 
} /* End of namespace Population*/
}
