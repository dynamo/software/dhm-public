#ifndef Population_Exit_h
#define Population_Exit_h

// project
#include "Move.h"
//#include "../../../ExchangeInfoStructures/AccountingModelParameters.h"

namespace DataStructures
{
    class Animal;

namespace Population
{
    class Exit : public Move
    {
        DECLARE_SERIALIZE_STD_METHODS;
    protected:
        Exit(){} // serialization constructor
        void takeOut(DataStructures::Animal* pAnim
#ifdef _LOG
                                            , const boost::gregorian::date &exitDate, const std::string &log 
#endif // _LOG
                                            , float &accountingValueToUpdate, float priceFactor = 1.0f);
    public:
        virtual ~Exit();
    };
} /* End of namespace DataStructures */
}

#endif // Population_Exit_h
