#include "Move.h"

// Project
#include "../../Animal.h"
#include "../../../ResultStructures/DairyFarmResult.h"
#include "../../DairyHerd.h"

BOOST_CLASS_EXPORT(DataStructures::Population::Move) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \

//    ar & BOOST_SERIALIZATION_NVP(_date);
//    ar & BOOST_SERIALIZATION_NVP(_amount);
//    ar & BOOST_SERIALIZATION_NVP(_AnimalId);

    IMPLEMENT_SERIALIZE_STD_METHODS(Move);

//    Move::Move(DataStructures::Animal* pAnim
    void Move::performsAccountingTransaction(DataStructures::Animal* pAnim
#ifdef _LOG
                                            , const boost::gregorian::date &moveDate, const std::string &log
    
#endif // _LOG
                                            , float amount, float &accountingValueToUpdate, float priceFactor) 
    {
#ifdef _LOG
        pAnim->getDataLog().move = log;
#endif // _LOG 
        if (amount != 0.0f)
        {
            ((DataStructures::DairyHerd*)pAnim->getHerd())->accountNewTransaction(
#ifdef _LOG                
                                    moveDate, log, 
#endif // _LOG                
                                    amount * priceFactor, accountingValueToUpdate);
        }
    }

} /* End of namespace Population*/
}
