#ifndef Population_Move_h
#define Population_Move_h

// standard
#include <vector>

// boost
#include "boost/date_time/gregorian/gregorian.hpp"

// project
#include "../../../ExchangeInfoStructures/FunctionalConstants.h"
#include "../../../Tools/Serialization.h"

namespace DataStructures
{
    class Animal;

namespace Population
{
    class Move
    {
    private:
                
        DECLARE_SERIALIZE_STD_METHODS;
    protected:
        Move(){} // serialization constructor

    public:
        //Move(DataStructures::Animal* pAnim
        
        void performsAccountingTransaction(DataStructures::Animal* pAnim
#ifdef _LOG
                                            , const boost::gregorian::date &moveDate, const std::string &log
    
#endif // _LOG
                                            , float amount, float &accountingValueToUpdate, float priceFactor = 1.0f);
        virtual ~Move(){} 
    };
}
} /* End of namespace Population*/
#endif // Population_Move_h
