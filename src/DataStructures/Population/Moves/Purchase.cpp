#include "Purchase.h"

// project
#include "../../Animal.h"
#include "../../Herd.h"

BOOST_CLASS_EXPORT(DataStructures::Population::Purchase) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Entrance);

    IMPLEMENT_SERIALIZE_STD_METHODS(Purchase);

    Purchase::Purchase(DataStructures::Animal* pAnim
#ifdef _LOG
                                            , const boost::gregorian::date &purchaseDate
    
#endif // _LOG
                                            , float amount, float &accountingValueToUpdate) : Entrance(pAnim
#ifdef _LOG
                                            , purchaseDate, FunctionalConstants::AccountingModel::PURCHASE_TRANSACTION_TYPE 
    
#endif // _LOG
                                            , amount, accountingValueToUpdate)
    {
        pAnim->getHerd()->increaseAnnualHeiferBoughtCount();
    }


} /* End of namespace Population*/
}