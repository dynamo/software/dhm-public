#ifndef Population_Purchase_h
#define Population_Purchase_h

// project
#include "Entrance.h"

namespace DataStructures
{
namespace Population
{
    class Purchase : public Entrance
    {
        DECLARE_SERIALIZE_STD_METHODS;

    protected:
        Purchase(){} // serialization constructor
        void concrete(){}; // To allow instanciation

    public:
        Purchase(DataStructures::Animal* pAnim
#ifdef _LOG
                                            , const boost::gregorian::date &purchaseDate
    
#endif // _LOG
                                            , float amount, float &accountingValueToUpdate); 
        virtual ~Purchase(){}; // Destructor

    };
} /* End of namespace Population*/
}
#endif // Population_Purchase_h
