#include "Sale.h"

// project
#include "../../Animal.h"
#include "../../Herd.h"

BOOST_CLASS_EXPORT(DataStructures::Population::Sale) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Exit);

    IMPLEMENT_SERIALIZE_STD_METHODS(Sale);
    
    Sale::Sale(DataStructures::Animal* pAnim,
#ifdef _LOG
                                            const boost::gregorian::date &saleDate, 
#endif // _LOG
                                            float &accountingValueToUpdate) : Sale(pAnim,  
#ifdef _LOG
                                            saleDate,
#endif // _LOG
                                            pAnim->getSaleStatus(), accountingValueToUpdate)
    {
    }
    
    Sale::Sale(DataStructures::Animal* pAnim,
#ifdef _LOG
                                            const boost::gregorian::date &saleDate,
#endif // _LOG
                                            FunctionalEnumerations::Population::SaleStatus status, float &accountingValueToUpdate)
                    {
        if (status == FunctionalEnumerations::Population::SaleStatus::male)
        {
            pAnim->getHerd()->increaseAnnualMaleSaleCount();  
        }
        else if (status == FunctionalEnumerations::Population::SaleStatus::beefCrossBred)
        {
            pAnim->getHerd()->increaseAnnualBeefCrossBredCount();  
        }
        else if (status == FunctionalEnumerations::Population::SaleStatus::sterileFemale)
        {
            pAnim->getHerd()->increaseAnnualSterileFemaleSaleCount();  
        }
        else if (status == FunctionalEnumerations::Population::SaleStatus::extraFemale)
        {
            pAnim->getHerd()->increaseAnnualExtraFemaleSaleCount();  
        }
        else if (status == FunctionalEnumerations::Population::SaleStatus::extraPregnantHeifer)
        {
            pAnim->getHerd()->increaseAnnualExtraPregnantHeiferSaleCount();  
        }
        else
        {
            std::string errorMessage = "Unadequate Sale Status" + Tools::toString(status);
            throw std::runtime_error(errorMessage);
        }
        
        // Generic part
        takeOut(pAnim
#ifdef _LOG
                
                , saleDate, FunctionalConstants::AccountingModel::SALE_TRANSACTION_TYPE
#endif // _LOG
                   
                , accountingValueToUpdate);
    }
} /* End of namespace Population*/
}
