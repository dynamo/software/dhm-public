#ifndef Population_Sale_h
#define Population_Sale_h

// project
#include "Exit.h"
#include "../../../ExchangeInfoStructures/FunctionalEnumerations.h"

namespace DataStructures
{
    class Animal;

namespace Population
{
    class Sale : public Exit
    {
        DECLARE_SERIALIZE_STD_METHODS;

    protected:
        Sale(){} // serialization constructor
        void concrete(){}; // To allow instanciation

    public:
        Sale(DataStructures::Animal* pAnim, 
#ifdef _LOG
                                           const boost::gregorian::date &saleDate,
#endif // _LOG
                                           float &accountingValueToUpdate);
        Sale(DataStructures::Animal* pAnim, 
#ifdef _LOG
                                           const boost::gregorian::date &saleDate,
#endif // _LOG
                                           FunctionalEnumerations::Population::SaleStatus status, float &accountingValueToUpdate);
        virtual ~Sale(){}; // Destructor

    };
} /* End of namespace Population*/
}
#endif // Population_Sale_h
