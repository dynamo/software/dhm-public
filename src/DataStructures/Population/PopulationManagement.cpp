#include "PopulationManagement.h"

// standard

// Project
#include "./Strategies/FemaleCalveManagementStrategies/FemaleCalveManagementStrategy.h"
#include "../DairyFarm.h"
#include "../DairyHerd.h"
#include "../DairyCow.h"
#include "../../Tools/Random.h"
#include "LocationStates/StabulationBatchLocationState.h"
#include "LocationStates/FullPastureBatchLocationState.h"
#include "LocationStates/HalfStabulationPastureBatchLocationState.h"
#include "./Moves/Purchase.h"
#include "./Moves/Sale.h"
#include "./Moves/Cull.h"
#include "../../Genetic/Breed/DairyBreed.h"
#include "../../Reproduction/DairyHerdReproductionStates/PregnancyState.h"
#include "../ArtificialInsemination.h"
#include "../../ExchangeInfoStructures/FunctionalConstants.h"


namespace DataStructures
{
namespace Population
{
    
    void PopulationManagement::saleCalves(DataStructures::DairyHerd* pHerd, const boost::gregorian::date &simDate)
    {
        DataStructures::DairyFarm* pFarm = pHerd->getDairyFarm();
        std::map<unsigned int, DataStructures::Animal*> mCavesToSale = pHerd->getBatch(FunctionalEnumerations::Population::BatchType::toSaleCalves)->getAnimalList();
            
        for(auto it : mCavesToSale)
        {
            if (it.second->getAge() >= FunctionalConstants::Population::MINIMUM_AGE_FOR_SALE)
            {
                pFarm->addMove(new Sale(it.second, 
#ifdef _LOG
                                            simDate,
#endif // _LOG
                                            pHerd->getCalfSalesForUpdate())); // We sale the animal   
#ifdef _LOG
                pFarm->increaseCalvesSoldOfTheDay(1);
#endif // _LOG                    
            }
        }
    }
    
    // Mandatored culling
    void PopulationManagement::cullMandatored(DataStructures::DairyHerd* pHerd, const boost::gregorian::date &simDate)
    {  
        for (std::map<unsigned int,DataStructures::Animal*>::iterator it = pHerd->getAnimalsToBeCulledToday().begin(); it != pHerd->getAnimalsToBeCulledToday().end(); it++)
        {
            cullImmediatly(pHerd->getDairyFarm(), it->second, simDate);
        }
        pHerd->clearAnimalsToBeCulledToday();
    }

    void PopulationManagement::managePopulation(DataStructures::DairyHerd* pHerd, const boost::gregorian::date &simDate, int missingHeifers)
    {  
        // Heifers
        manageHeiferNumber(pHerd, simDate, missingHeifers);
        
        // Adults
        manageAdultNumber(pHerd, simDate);
        
    }

    void PopulationManagement::manageHeiferNumber(DataStructures::DairyHerd* pHerd, const boost::gregorian::date &simDate, int missingHeifers)
    {  
        DataStructures::DairyFarm* pFarm = pHerd->getDairyFarm();

//        // Adult population
//        cullMandatored(pHerd, simDate); // Mandatored culling
        
        if (missingHeifers > 0)
        {
            // Not enought new pregnant heifer near calving actually in the herd, we have to buy some
            for (int iPurch = 0; iPurch < missingHeifers; iPurch++)
            {
                buyHeifer(simDate, pHerd);
            }
        }
        else if (missingHeifers < 0)
        {
            // Too much pregnant heifers, we must sale some, 
            unsigned int pregnantHeiferToSaleNumber = -missingHeifers;
            
            // First regarding the dam performance of the heifer in DriedCowsAndPregnantHeifers batch
            std::vector<DataStructures::DairyMammal*> heifersToSale;
            for (unsigned int i = 0; i < pregnantHeiferToSaleNumber; i++)
            {
                float min = FLT_MAX;
                DataStructures::DairyMammal* pHeiferToSale = nullptr;
                for (std::vector<DataStructures::DairyMammal*>::iterator itHeifer = pHerd->getHeifersInDriedCowsAndPregnantHeifersBatchOfTheDay().begin(); itHeifer != pHerd->getHeifersInDriedCowsAndPregnantHeifersBatchOfTheDay().end(); itHeifer++)
                {
                    DataStructures::DairyMammal* pHeifer = *itHeifer;
                    bool toSelect = true;
                    for (unsigned int i = 0; i < heifersToSale.size() and toSelect; i++)
                    {
                        // Yet in the selected heifers ?
                        toSelect = (heifersToSale[i] != pHeifer);
                    }
                    if (toSelect)
                    {
                        // Not yet int the selected heifers
                        float milkQty = pHeifer->getDamLastFullLactationMilkQuantity();
                        if (milkQty > 0.0f and milkQty < min) // If we know the performance of the dam, and this performance is the worst
                        {
                            min = milkQty;
                            pHeiferToSale = pHeifer; 
                        }
                    }
                }
                if (pHeiferToSale != nullptr)
                {
                    heifersToSale.push_back(pHeiferToSale);
                }
            }
            
            pregnantHeiferToSaleNumber -= heifersToSale.size();
            
            // Second, we sale the farthest of the calving
            for (unsigned int i = 0; i < pregnantHeiferToSaleNumber; i++)
            {
                unsigned int max = 0;
                DataStructures::DairyMammal* pHeiferToSale = nullptr;
                for (std::vector<DataStructures::DairyMammal*>::iterator itHeifer = pHerd->getHeifersInDriedCowsAndPregnantHeifersBatchOfTheDay().begin(); itHeifer != pHerd->getHeifersInDriedCowsAndPregnantHeifersBatchOfTheDay().end(); itHeifer++)
                {
                    DataStructures::DairyMammal* pHeifer = *itHeifer;
                    bool toSelect = true;
                    for (unsigned int i = 0; i < heifersToSale.size() and toSelect; i++)
                    {
                        toSelect = (heifersToSale[i] != pHeifer);
                    }
                    if (toSelect)
                    {
                        unsigned int saleScore = pHeifer->getDayDurationUntilPredictedEndPregnancyDate(simDate);
                        if (saleScore > max)
                        {
                            // Get the farthest heifer
                            max = saleScore;
                            pHeiferToSale = pHeifer;
                        }
                    }
                }
                if (pHeiferToSale != nullptr)
                {
                    heifersToSale.push_back(pHeiferToSale);
                }
            }

            // Sale
            for (unsigned int iSale = 0; iSale < heifersToSale.size(); iSale++)
            {
                pFarm->addMove(new Sale(heifersToSale[iSale],  
#ifdef _LOG
                                            simDate,
#endif // _LOG
                                            FunctionalEnumerations::Population::SaleStatus::extraPregnantHeifer, pHerd->getHeiferSalesForUpdate())); // We sale the heifer  
#ifdef _LOG
                pFarm->increaseHeifersSoldOfTheDay(1);
#endif // _LOG                    
            }
        }
    }

    void PopulationManagement::manageAdultNumber(DataStructures::DairyHerd* pHerd, const boost::gregorian::date &simDate)
    {
        DataStructures::DairyFarm* pFarm = pHerd->getDairyFarm();
        
        int adultOverage = (int)pHerd->getAdultCount() - (int)pHerd->getMeanAdultNumberTarget();
        
        // Some adults are to cull  
        for (int i = 0; i < adultOverage; i++)
        {
            std::map<unsigned int, DataStructures::Animal*> &toCheck = pHerd->getAnimalsToBeCulledLater(); // potential
            float max = 0.0f;
            DataStructures::Animal* pCowToCull = nullptr;
            for (std::map<unsigned int, DataStructures::Animal*>::iterator it = toCheck.begin(); it != toCheck.end(); it++)
            {
                DataStructures::Animal* pCow = it->second;
                float cullingScore = pCow->getTodayCullingScore();
                if (cullingScore > max)
                {
                    max = cullingScore;
                    pCowToCull = pCow;
                }
            }
            if (pCowToCull != nullptr)
            {
                pHerd->removeAnimalToBeCulledLater(pCowToCull); // This cows is not more to cull later
                cullImmediatly(pFarm, pCowToCull, simDate); // Cull the concerned cow
            }
        }
    }
    
    bool PopulationManagement::cowIsToCullLaterRegardingPopulation(DairyMammal* pCow, DataStructures::DairyHerd* pHerd)
    {
        
        int adultOverage = ((pHerd->getAdultCount() - pHerd->getMeanAdultNumberTarget()) - pHerd->getAnimalsToBeCulledLater().size()) + (int)((float)pHerd->getMeanAdultNumberTarget() / pHerd->getDairyFarm()->getCullingAnticipationValue());
        if (adultOverage > 0 and pHerd->getAnimalsToBeCulledLater().find(pCow->getUniqueId()) == pHerd->getAnimalsToBeCulledLater().end())
        {
            // Not already known as animal to cull
//            // If my lactation rank is the greater in the herd
//            unsigned int highterLactatingRank = pHerd->getHighterLactationRankFromNotCulledDecisionLactatingCows();
//            return pCow->getLactationRank() == highterLactatingRank;
            
            // If my genetic value is the lower in the herd
            return pHerd->isThisCowNotToInseminateRegardingOtherNotCulledDecisionLactatingCows(pCow);
        }
        return false;
    }
    
    void PopulationManagement::cullImmediatly(DataStructures::DairyFarm* pFarm, DataStructures::Animal* pCow, const boost::gregorian::date &simDate)
    {   
        // Maintenance
//        DataStructures::DairyMammal* pDairyCow = ((DataStructures::DairyMammal*)pCow);
//        if (pDairyCow->getLactationStage() > 6)
//        {
//            std::cout << pDairyCow->getLactationStage() << std::endl;
//        }
        
        
        pFarm->addMove(new Cull(pCow, simDate, pFarm->getDairyHerd()->getCullSalesForUpdate()));
    }
    
    DataStructures::DairyMammal* PopulationManagement::getNewDairyMammal(const boost::gregorian::date &birth, DataStructures::Mammal* pDam, Genetic::Breed::Breed* pSireBreed, bool sterile, FunctionalEnumerations::Genetic::Sex sex)
    {
        DataStructures::DairyMammal* pNew = new DataStructures::DairyCow(birth, pDam, pSireBreed, sterile, sex);
        return pNew;
    }

    void PopulationManagement::buyHeifer(const boost::gregorian::date &simDate, DataStructures::DairyHerd* pHerd)
    {
        // Buy a pregnant heifer, calving in 2 weeks
        // Age = age to breeding + mean pregnancy duration - delay before calving
        unsigned int age = pHerd->getDairyFarm()->getAgeToBreedingDecision()
                            + pHerd->getInitialBreed()->getPregnancyDuration(FunctionalEnumerations::Reproduction::DairyCowParity::heifer)
                            - FunctionalConstants::Population::DELAY_BEFORE_CALVING_FOR_BOUGHT_PREGNANT_HEIFERS;
        boost::gregorian::date birthDate = simDate;
        birthDate -= (boost::gregorian::days(age));

        // Creating the cow
        DataStructures::DairyCow* pCow = new DataStructures::DairyCow(simDate, birthDate, pHerd->getInitialBreed(), pHerd, FunctionalEnumerations::Genetic::Sex::female);
        
        // Set batch affectation 1
        affectDairyCowToBatch(pCow, FunctionalEnumerations::Population::BatchType::bredHeifers, simDate);
        
        // Fecunding insemination
        boost::gregorian::date inseminationDate = birthDate + (boost::gregorian::days(pHerd->getDairyFarm()->getAgeToBreedingDecision()));
        pCow->updateLastOestrusDetectionDate(inseminationDate);
        Genetic::Breed::Breed* pInseminationBreed;
        FunctionalEnumerations::Reproduction::TypeInseminationSemen semendType;
        pHerd->getDairyFarm()->getTypeInseminationAndBreedForBoughtHeifer(semendType, pInseminationBreed); // Get the insemination parameters
        DataStructures::Insemination* pIns = new DataStructures::ArtificialInsemination(pInseminationBreed, pHerd->getDairyFarm()->getASireGeneticValueFromCatalog(pInseminationBreed->getEnumerationBreed(), inseminationDate, pHerd->getDairyFarm()->getCurrentResults()), semendType, inseminationDate, true, 1, pHerd->getDairyFarm()->getCurrentResults(), pHerd->getDairyFarm()->getCurrentAccountingModel(), pCow, false);
        pCow->addInsemination(pIns);

        pCow->setCurrentReproductionState(new Reproduction::DairyHerdReproductionStates::PregnancyState(pCow, inseminationDate, pHerd->getDairyFarm()->getRandom(), true), simDate); // Set the state                              
        
        // Set batch affectation 2
        affectDairyCowToBatch(pCow, FunctionalEnumerations::Population::BatchType::driedCowsAndPregnantHeifers, simDate);

        // Update cow informations
        pCow->progress(simDate); // Progressing after herd progress
        
        // Create the move
        pHerd->getDairyFarm()->addMove(new Purchase(pCow
#ifdef _LOG
                                            , simDate
#endif // _LOG
                                            , -pHerd->getDairyFarm()->getCurrentAccountingModel().getCurrentPrice(FunctionalConstants::AccountingModel::KEY_PREGNANT_HEIFER_PRICE), pHerd->getPopulationCostsForUpdate()));
        
        
#ifdef _LOG
        // Only in debug mode, to display the VG and the birth day of the bought heifer
        Results::GeneticResult gr;
        gr.date = pCow->getBirthDate();
        gr.idOrBreed = pCow->getUniqueId();
        gr.birth = false;
        gr.VGLait = pCow->getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait).getCorrectedPerformance();
        gr.VGTB = pCow->getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::TB).getCorrectedPerformance();
        gr.VGTP = pCow->getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::TP).getCorrectedPerformance();
        gr.VGFer = pCow->getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer).getCorrectedPerformance();
        gr.VGMACL = pCow->getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL).getCorrectedPerformance();
        gr.VGBHBlait = pCow->getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait).getCorrectedPerformance();
        gr.VGRBi = pCow->getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi).getCorrectedPerformance();
        gr.VGRBni = pCow->getGeneticValue().getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni).getCorrectedPerformance();
        pHerd->getDairyFarm()->getCurrentResults()->getGeneticResults().FemaleVGResults.push_back(gr);
#endif // _LOG        
    }
    
    // Location state only stabulation
    BatchLocationStates::BatchLocationState* PopulationManagement::getNewBatchStabulationState(const boost::gregorian::date &simDate)
    {
        return new BatchLocationStates::StabulationBatchLocationState(simDate, boost::gregorian::date(boost::gregorian::pos_infin));
    }
    
    // Location state depending on pasture context
    BatchLocationStates::BatchLocationState* PopulationManagement::getNewBatchLocationState(   const boost::gregorian::date &simDate,
                                                                                               std::pair<unsigned int, FunctionalEnumerations::Global::Month> &beginPastureDate,
                                                                                               std::pair<unsigned int, FunctionalEnumerations::Global::Month> &endPastureDate)
    {
        boost::gregorian::date begin(simDate.year(), beginPastureDate.second, beginPastureDate.first);
        boost::gregorian::date end(simDate.year(), endPastureDate.second, endPastureDate.first);

        if (simDate < begin)
        {
            // Begin of the year, stabulation
            boost::gregorian::date_duration oneDay(1);
            return new BatchLocationStates::StabulationBatchLocationState(simDate, begin - oneDay);
        }
        else if (simDate > end)
        {
            // End of the year, stabulation
            boost::gregorian::date_duration oneDay(1);
            return new BatchLocationStates::StabulationBatchLocationState(simDate, boost::gregorian::date(begin.year()+1, begin.month(), begin.day()) - oneDay);
        }
        else
        {
            // Pasture
            return new BatchLocationStates::FullPastureBatchLocationState(simDate, end);
        }
    }
    
    BatchLocationStates::BatchLocationState* PopulationManagement::getNewLactatingCowBatchLocationState(const boost::gregorian::date &simDate, DataStructures::DairyFarm* pFarm, bool &fullPasture, bool &fullStabulation)
    {
        boost::gregorian::date pastureBegin(simDate.year(), pFarm->getLactatingCowBeginFullPastureDate().second, pFarm->getLactatingCowBeginFullPastureDate().first);
        boost::gregorian::date pastureEnd(simDate.year(), pFarm->getLactatingCowEndFullPastureDate().second, pFarm->getLactatingCowEndFullPastureDate().first);
        fullPasture = false;
        fullStabulation = false;

        if (simDate == pastureBegin)
        {
            // Full pasture
            fullPasture = true;
            return new BatchLocationStates::FullPastureBatchLocationState(simDate, pastureEnd);
        }
        else 
        {
            boost::gregorian::date stabulationBegin(simDate.year(), pFarm->getLactatingCowBeginStabulationDate().second, pFarm->getLactatingCowBeginStabulationDate().first);
            if (simDate == stabulationBegin)
            {
                // Full stabulation
                fullStabulation = true;
                unsigned int endYear = simDate.year();
                if (pFarm->getLactatingCowEndStabulationDate().second < pFarm->getLactatingCowBeginStabulationDate().second) endYear++;
                boost::gregorian::date stabulationEnd(endYear, pFarm->getLactatingCowEndStabulationDate().second, pFarm->getLactatingCowEndStabulationDate().first);
                return new BatchLocationStates::StabulationBatchLocationState(simDate, stabulationEnd);
            }
            else
            {
                // Half stabulation and pasture
                boost::gregorian::date_duration oneDay(1);
                if (simDate < pastureBegin)
                {
                    return new BatchLocationStates::HalfStabulationPastureBatchLocationState(simDate, pastureBegin - oneDay);
                }
                else // simDate > pastureEnd
                {
                    return new BatchLocationStates::HalfStabulationPastureBatchLocationState(simDate, stabulationBegin - oneDay);
                }
            }
        }
    }

    void PopulationManagement::affectDairyCowToBatch(DataStructures::DairyMammal* pDairyCow, FunctionalEnumerations::Population::BatchType bt, const boost::gregorian::date &date)
    {
        Population::Batch* pTargetBatch = pDairyCow->getHerd()->getBatch(bt);
        assert (pTargetBatch != nullptr);
        pTargetBatch->includeAnimal(pDairyCow, date);
    }
    
    void PopulationManagement::changeAnimalBatch(DataStructures::Animal* pAnim, const boost::gregorian::date &date)
    {
        Population::Batch* pTargetBatch = pAnim->getBatch()->getNextBatch();
        assert (pTargetBatch != nullptr);
        pTargetBatch->includeAnimal(pAnim, date);
    }

    void PopulationManagement::affectNewBornCalfToBatch(DataStructures::DairyMammal* pCalf, const boost::gregorian::date &date, Population::Strategies::FemaleCalveManagementStrategy* pFemaleStrategy)
    {
        // The question is : do we have to put the new born calf in the female calves batch, or do we have to sale it, or another instead ?
        if (not pCalf->getBabyIsToSale()) // Not due to birth circonstances, but may be for management reason
        {
            pFemaleStrategy->setNewBornCalfSaleStatus(pCalf, date);
        }
        // Final decision
        if (pCalf->getBabyIsToSale())
        {
            affectDairyCowToBatch(pCalf, FunctionalEnumerations::Population::BatchType::toSaleCalves, date);
        }
        else
        {
            affectDairyCowToBatch(pCalf, FunctionalEnumerations::Population::BatchType::femaleCalves, date);
        }
    }

    float PopulationManagement::calculateTodayCullingScoreOfDairyMammal(DataStructures::DairyMammal* pDairyCow)
    {
#ifdef _LOG
        // To protect a cow, return 0.0f as culling note
        assert (pDairyCow->lactationIsOngoing());     
        assert (not pDairyCow->pregnancyIsOngoing());     
#endif // _LOG        
        // Begin of the laction period, too early
        if (pDairyCow->getLactationStage() < (int)FunctionalConstants::Population::LACTATION_STAGE_PROTECTION_DURATION_BEFORE_CULLING)
        {
            return 0.0f;
        }       
        return pDairyCow->getLastMeanPeriodSCC() / pDairyCow->getLastMeanPeriodMilkQuantity();
    }
}
}
    