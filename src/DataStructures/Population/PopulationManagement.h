#ifndef PopulationManagement_h
#define PopulationManagement_h

// standard

// boost
#include <boost/date_time/gregorian/gregorian.hpp>

// project
#include "../../ExchangeInfoStructures/FunctionalEnumerations.h"
//#include "../../ExchangeInfoStructures/AccountingModelParameters.h"
#include "../../ExchangeInfoStructures/ExcludedCalvingPeriodParameters.h"
#include "../../Genetic/Breed/Breed.h"
#include "Batch/Batch.h"

namespace Results
{
    class Result;
}

namespace DataStructures
{
    class DairyHerd;
    class DairyMammal;
    class Mammal;
    class Animal;
    class DairyFarm;
namespace Population
{
    class Batch;
    namespace BatchLocationStates
    {
        class BatchLocationState;
    }
    namespace Strategies
    {
        class FemaleCalveManagementStrategy;
    }
    
    class PopulationManagement
    {
    private:
        
    protected:
    virtual void concrete() = 0; // To ensure that it will not be instantiated
        
    public:
        
    static void saleCalves(DataStructures::DairyHerd* pHerd, const boost::gregorian::date &simDate);

    static DataStructures::DairyMammal* getNewDairyMammal(const boost::gregorian::date &birth, DataStructures::Mammal* pDam, Genetic::Breed::Breed* pSireBreed, bool sterile, FunctionalEnumerations::Genetic::Sex sex);

    static void buyHeifer(const boost::gregorian::date &simDate, DataStructures::DairyHerd* pHerd);
    static void cullImmediatly(DataStructures::DairyFarm* pFarm, DataStructures::Animal* pCow, const boost::gregorian::date &simDate);
    static bool cowIsToCullLaterRegardingPopulation(DairyMammal* pCow, DataStructures::DairyHerd* pHerd);
    static void cullMandatored(DataStructures::DairyHerd* pHerd, const boost::gregorian::date &simDate);
    
    // Location state only stabulation
    static BatchLocationStates::BatchLocationState* getNewBatchStabulationState(const boost::gregorian::date &simDate);// Location state only stabulation
    
    // Location state depending on pasture context
    static BatchLocationStates::BatchLocationState* getNewBatchLocationState(   const boost::gregorian::date &simDate,
                                                                                               std::pair<unsigned int, FunctionalEnumerations::Global::Month> &beginPastureDate,
                                                                                               std::pair<unsigned int, FunctionalEnumerations::Global::Month> &endPastureDate);
    
    static BatchLocationStates::BatchLocationState* getNewLactatingCowBatchLocationState(const boost::gregorian::date &simDate, DataStructures::DairyFarm* pFarm, bool &fullPasture, bool &fullStabulation);
    static void affectNewBornCalfToBatch(DataStructures::DairyMammal* pCalf, const boost::gregorian::date &date, Population::Strategies::FemaleCalveManagementStrategy* pFemaleStrategy);
    static void affectDairyCowToBatch(DataStructures::DairyMammal* pDairyCow, FunctionalEnumerations::Population::BatchType bt, const boost::gregorian::date &date);
    static void changeAnimalBatch(DataStructures::Animal* pAnim, const boost::gregorian::date &date);
    
    // Depending on trategies

    static void managePopulation(DataStructures::DairyHerd* pHerd, const boost::gregorian::date &simDate, int missingHeifers);
    static void manageHeiferNumber(DataStructures::DairyHerd* pHerd, const boost::gregorian::date &simDate, int missingHeifers);
    static void manageAdultNumber(DataStructures::DairyHerd* pHerd, const boost::gregorian::date &simDate);
    static float calculateTodayCullingScoreOfDairyMammal(DataStructures::DairyMammal* pDairyCow);
    
  };

} /* End of namespace PopulationManagement */
}
#endif // PopulationManagement_h
