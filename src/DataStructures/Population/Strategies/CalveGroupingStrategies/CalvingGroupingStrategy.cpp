#include "CalvingGroupingStrategy.h"

// boost

// project
#include "../../../DairyFarm.h"
#include "../../../DairyHerd.h"
#include "../../Batch/Batch.h"

BOOST_CLASS_EXPORT(DataStructures::Population::Strategies::CalvingGroupingStrategy) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    namespace Strategies
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Strategy);

        IMPLEMENT_SERIALIZE_STD_METHODS(CalvingGroupingStrategy);
        
        CalvingGroupingStrategy::CalvingGroupingStrategy(DataStructures::DairyFarm* pFarm) : Strategy(pFarm)
        {
            
        }
        
        CalvingGroupingStrategy::~CalvingGroupingStrategy()
        {
        }
        
        void CalvingGroupingStrategy::updateMissingHeiferCountByCheckingComing(const boost::gregorian::date &simDate, int &delta)
        {           
            if (delta < 0)
            {
                // Some are extra, but will we need heifer soon ?
                boost::gregorian::date_duration dd(FunctionalConstants::Population::PREGNANT_HEIFER_COUNT_PERIOD);      
                boost::gregorian::date overCountPeriodDate = simDate + dd;
                boost::gregorian::date lastDayOfThisCampaign = _pAppliesToFarm->getLastDayOfTheCampaign(simDate);
                if (overCountPeriodDate > lastDayOfThisCampaign)
                {
                    overCountPeriodDate = lastDayOfThisCampaign;
                }
                delta = getTheoricalNewComersInHerdCountRegardingDate(overCountPeriodDate, 0) - _pAppliesToFarm->getDairyHerd()->getAnnualNewComerInHerdCount();
                if (delta > 0) delta = 0;
            }
            else if (delta > 0)
            {
                delta = getTheoricalNewComersInHerdCountRegardingDate(simDate, 0) - _pAppliesToFarm->getDairyHerd()->getAnnualNewComerInHerdCount();
                // Some heifer are missing, are there some near calving actually in the herd ?
                unsigned int heiferNearCalvingInBredHeiferCount = _pAppliesToFarm->getDairyHerd()->getBatch(FunctionalEnumerations::Population::BatchType::bredHeifers)->getComingPregnantHeiferCountFromBredHeiferBatch(simDate, _pAppliesToFarm->getMeanAdultNumberTarget());
                if ((unsigned int)delta > heiferNearCalvingInBredHeiferCount)
                {
                    // no enought pregnant heifer coming from the bred heifer batch, so we have to buy some 
                    delta -= heiferNearCalvingInBredHeiferCount;
                }
                else
                {
                    // More than needed are coming in the herd
                    delta = 0;
                }
            }
        } 
    }
} /* End of namespace Strategies */
}