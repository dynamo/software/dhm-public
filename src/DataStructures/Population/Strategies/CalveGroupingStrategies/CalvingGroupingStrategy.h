#ifndef Population_Strategies_CalvingGroupingStrategy_h
#define Population_Strategies_CalvingGroupingStrategy_h

// boost

// project
#include "../Strategy.h"
#include "../../../../ExchangeInfoStructures/FunctionalEnumerations.h"

namespace ExchangeInfoStructures
{
    namespace Parameters
    {
        class ExcludedCalvingPeriodParameters;
    }
}

namespace DataStructures
{
namespace Population
{
    namespace Strategies
    {
        class CalvingGroupingStrategy : public Strategy
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        protected:
            CalvingGroupingStrategy(){}
            CalvingGroupingStrategy(DataStructures::DairyFarm* pFarm);
            virtual void concrete() = 0; // To ensure that it will not be instantiated
        
            void updateMissingHeiferCountByCheckingComing(const boost::gregorian::date &simDate, int &delta);
        
        public:
            virtual ~CalvingGroupingStrategy();
     
            virtual unsigned int getTheoricalNewComersInHerdCountRegardingDate(const boost::gregorian::date &theDate, unsigned int securityMargingQuantity) = 0;
            virtual int calculateMissingHeifers(const boost::gregorian::date &simDate) = 0;
            virtual bool calvingIsAllowedDuringMonth(FunctionalEnumerations::Global::Month theMonth) {return true;}


        };
    }
} /* End of namespace Health */
}
#endif // Population_Strategies_CalvingGroupingStrategy_h
