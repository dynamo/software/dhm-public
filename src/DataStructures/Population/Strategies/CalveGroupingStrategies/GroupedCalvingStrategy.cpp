#include "GroupedCalvingStrategy.h"

// boost

// project
#include "../../PopulationManagement.h"
#include "../../../DairyFarm.h"
#include "../../../DairyHerd.h"

BOOST_CLASS_EXPORT(DataStructures::Population::Strategies::GroupedCalvingStrategy) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    namespace Strategies
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CalvingGroupingStrategy); \
        ar & BOOST_SERIALIZATION_NVP(_allowedCalvingPeriodDurationInMonths); \

        IMPLEMENT_SERIALIZE_STD_METHODS(GroupedCalvingStrategy);
        
        GroupedCalvingStrategy::GroupedCalvingStrategy(DataStructures::DairyFarm* pFarm) : CalvingGroupingStrategy(pFarm)
        {
            if (_pAppliesToFarm->getHeiferCalvingExclusionPeriod().firstMonthForCalvingExculsion <= _pAppliesToFarm->getHeiferCalvingExclusionPeriod().lastMonthForCalvingExculsion)
            {
                _allowedCalvingPeriodDurationInMonths = 12 - (_pAppliesToFarm->getHeiferCalvingExclusionPeriod().lastMonthForCalvingExculsion - _pAppliesToFarm->getHeiferCalvingExclusionPeriod().firstMonthForCalvingExculsion + 1);
            }
            else
            {
                _allowedCalvingPeriodDurationInMonths = _pAppliesToFarm->getHeiferCalvingExclusionPeriod().firstMonthForCalvingExculsion - _pAppliesToFarm->getHeiferCalvingExclusionPeriod().lastMonthForCalvingExculsion - 1;
            }
        }
        
        GroupedCalvingStrategy::~GroupedCalvingStrategy()
        {
        }
        
        int GroupedCalvingStrategy::calculateMissingHeifers(const boost::gregorian::date &simDate)
        {
            int delta = 0;
            if (simDate.day() == FunctionalConstants::Population::DAY_OF_THE_MONTH_FOR_PREGNANT_HEIFER_NEED_CALCULATION)
            { 
                delta = getTheoricalNewComersInHerdCountRegardingDate(simDate, 0) - _pAppliesToFarm->getDairyHerd()->getAnnualNewComerInHerdCount();
                updateMissingHeiferCountByCheckingComing(simDate, delta);
            }
            return delta;
        }
          
	unsigned int GroupedCalvingStrategy::getTheoricalNewComersInHerdCountRegardingDate(const boost::gregorian::date &theDate, unsigned int securityMargingQuantity)
        {
            float count = 0.0f;
            for (unsigned int iMonths = 0; iMonths < _pAppliesToFarm->getMonthsOfTheCampaign().size(); iMonths++)
            {
                FunctionalEnumerations::Global::Month theMonth = _pAppliesToFarm->getMonthsOfTheCampaign()[iMonths];
                
//                if ((_pAppliesToFarm->getHeiferCalvingExclusionPeriod().firstMonthForCalvingExculsion < _pAppliesToFarm->getHeiferCalvingExclusionPeriod().lastMonthForCalvingExculsion and (theMonth < _pAppliesToFarm->getHeiferCalvingExclusionPeriod().firstMonthForCalvingExculsion or theMonth > _pAppliesToFarm->getHeiferCalvingExclusionPeriod().lastMonthForCalvingExculsion))
//                        or
//                    (_pAppliesToFarm->getHeiferCalvingExclusionPeriod().firstMonthForCalvingExculsion == _pAppliesToFarm->getHeiferCalvingExclusionPeriod().lastMonthForCalvingExculsion and theMonth != _pAppliesToFarm->getHeiferCalvingExclusionPeriod().firstMonthForCalvingExculsion)
//                        or
//                    (_pAppliesToFarm->getHeiferCalvingExclusionPeriod().firstMonthForCalvingExculsion > _pAppliesToFarm->getHeiferCalvingExclusionPeriod().lastMonthForCalvingExculsion and theMonth < _pAppliesToFarm->getHeiferCalvingExclusionPeriod().firstMonthForCalvingExculsion and theMonth > _pAppliesToFarm->getHeiferCalvingExclusionPeriod().lastMonthForCalvingExculsion))
                if (calvingIsAllowedDuringMonth(theMonth))
                {
                    count += (_pAppliesToFarm->getAnnualHeiferTarget() + securityMargingQuantity) /((float) _allowedCalvingPeriodDurationInMonths);
                }
                
                if (theMonth == theDate.month().as_number())
                {
                    break;
                }
            }
            return round(count);
        }
           
        bool GroupedCalvingStrategy::calvingIsAllowedDuringMonth(FunctionalEnumerations::Global::Month theMonth)
        {
            return ((_pAppliesToFarm->getHeiferCalvingExclusionPeriod().firstMonthForCalvingExculsion < _pAppliesToFarm->getHeiferCalvingExclusionPeriod().lastMonthForCalvingExculsion and (theMonth < _pAppliesToFarm->getHeiferCalvingExclusionPeriod().firstMonthForCalvingExculsion or theMonth > _pAppliesToFarm->getHeiferCalvingExclusionPeriod().lastMonthForCalvingExculsion))
                        or
                    (_pAppliesToFarm->getHeiferCalvingExclusionPeriod().firstMonthForCalvingExculsion == _pAppliesToFarm->getHeiferCalvingExclusionPeriod().lastMonthForCalvingExculsion and theMonth != _pAppliesToFarm->getHeiferCalvingExclusionPeriod().firstMonthForCalvingExculsion)
                        or
                    (_pAppliesToFarm->getHeiferCalvingExclusionPeriod().firstMonthForCalvingExculsion > _pAppliesToFarm->getHeiferCalvingExclusionPeriod().lastMonthForCalvingExculsion and theMonth < _pAppliesToFarm->getHeiferCalvingExclusionPeriod().firstMonthForCalvingExculsion and theMonth > _pAppliesToFarm->getHeiferCalvingExclusionPeriod().lastMonthForCalvingExculsion));
        }

    }
} /* End of namespace Strategies */
}