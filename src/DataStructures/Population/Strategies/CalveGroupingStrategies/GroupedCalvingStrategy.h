#ifndef Population_Strategies_GroupedCalvingStrategy_h
#define Population_Strategies_GroupedCalvingStrategy_h

// boost

// project
#include "./CalvingGroupingStrategy.h"

namespace DataStructures
{
namespace Population
{
    namespace Strategies
    {
        class GroupedCalvingStrategy : public CalvingGroupingStrategy
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        private:
            unsigned int _allowedCalvingPeriodDurationInMonths;
        
            //int calculateMissingHeifersDependingSeason(const boost::gregorian::date &simDate);
        
        protected:
            GroupedCalvingStrategy(){}
            void concrete(){}; // To allow instanciation
        
        public:
            GroupedCalvingStrategy(DataStructures::DairyFarm* pFarm);
            virtual ~GroupedCalvingStrategy();
            int calculateMissingHeifers(const boost::gregorian::date &simDate);
            unsigned int getTheoricalNewComersInHerdCountRegardingDate(const boost::gregorian::date &theDate, unsigned int securityMargingQuantity);
            bool calvingIsAllowedDuringMonth(FunctionalEnumerations::Global::Month theMonth) override;

        };
    }
}
}
#endif // Population_Strategies_GroupedCalvingStrategy_h
