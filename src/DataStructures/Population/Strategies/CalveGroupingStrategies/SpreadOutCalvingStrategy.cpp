#include "SpreadOutCalvingStrategy.h"
#include "../../../DairyFarm.h"
#include "../../../DairyHerd.h"

// boost

// project

BOOST_CLASS_EXPORT(DataStructures::Population::Strategies::SpreadOutCalvingStrategy) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    namespace Strategies
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CalvingGroupingStrategy);

        IMPLEMENT_SERIALIZE_STD_METHODS(SpreadOutCalvingStrategy);
        
        SpreadOutCalvingStrategy::SpreadOutCalvingStrategy(DataStructures::DairyFarm* pFarm) : CalvingGroupingStrategy(pFarm)
        {
            
        }
        
        SpreadOutCalvingStrategy::~SpreadOutCalvingStrategy()
        {
        }
        
        int SpreadOutCalvingStrategy::calculateMissingHeifers(const boost::gregorian::date &simDate)
        { 
            // First we check if we have enought pregnant heifer if the herd, regarding the renewal ratio and the campaign stage
            int delta = getTheoricalNewComersInHerdCountRegardingDate(simDate, 0) - _pAppliesToFarm->getDairyHerd()->getAnnualNewComerInHerdCount();

            updateMissingHeiferCountByCheckingComing(simDate, delta);
            return delta;
        }

        unsigned int SpreadOutCalvingStrategy::getTheoricalNewComersInHerdCountRegardingDate(const boost::gregorian::date &theDate, unsigned int securityMargingQuantity)
        {
            return _pAppliesToFarm->getDayOfCampaign(theDate) * (_pAppliesToFarm->getAnnualHeiferTarget() + securityMargingQuantity) / 365;
        }
    }
} /* End of namespace Strategies */
}