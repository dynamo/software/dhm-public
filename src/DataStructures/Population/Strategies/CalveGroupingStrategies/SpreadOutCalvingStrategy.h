#ifndef Population_Strategies_SpreadOutCalvingStrategy_h
#define Population_Strategies_SpreadOutCalvingStrategy_h

// boost

// project
#include "./CalvingGroupingStrategy.h"

namespace DataStructures
{
namespace Population
{
    namespace Strategies
    {
        class SpreadOutCalvingStrategy : public CalvingGroupingStrategy
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        private:
        
        protected:
            SpreadOutCalvingStrategy(){}
            void concrete(){}; // To allow instanciation
        
        public:
            SpreadOutCalvingStrategy(DataStructures::DairyFarm* pFarm);
            virtual ~SpreadOutCalvingStrategy();
            int calculateMissingHeifers(const boost::gregorian::date &simDate);
            unsigned int getTheoricalNewComersInHerdCountRegardingDate(const boost::gregorian::date &theDate, unsigned int securityMargingQuantity);
        };
    }
}
}
#endif // Population_SpreadOutCalvingStrategy_h
