#include "BalanceManagementStrategy.h"

// boost

// project
#include "../../Batch/Batch.h"
#include "../../../DairyMammal.h"
#include "../../../DairyHerd.h"
#include "../../PopulationManagement.h"
#include "../../../DairyFarm.h"
#include "../CalveGroupingStrategies/CalvingGroupingStrategy.h"


BOOST_CLASS_EXPORT(DataStructures::Population::Strategies::BalanceManagementStrategy) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    namespace Strategies
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(FemaleCalveManagementStrategy);

        IMPLEMENT_SERIALIZE_STD_METHODS(BalanceManagementStrategy);
        
        BalanceManagementStrategy::BalanceManagementStrategy(DataStructures::DairyFarm* pFarm) : FemaleCalveManagementStrategy(pFarm)
        {
        }
        
        BalanceManagementStrategy::~BalanceManagementStrategy()
        {
        }
        
        void BalanceManagementStrategy::setNewBornCalfSaleStatus(DataStructures::DairyMammal* pCalf, const boost::gregorian::date &date)
        {
            // Balance management
            DataStructures::DairyHerd* pHerd = pCalf->getDairyHerd();
            
            if (pHerd->getUnderOneYearOldFemaleCalfNumberForThisCampaign(date) >= _pAppliesToFarm->getCalvingGroupingStrategy()->getTheoricalNewComersInHerdCountRegardingDate(date, FunctionalConstants::Population::FEMALE_CALVES_SAFETY_MARGIN))
            {
                // Too much calves  
                float calfDamPerformance = pCalf->getDamLastFullLactationMilkQuantity();
                if (calfDamPerformance == 0.0f)
                {
                    // My dam have no performance yet, comparison with the others is not possible, we sale
                    pCalf->setSaleStatus(FunctionalEnumerations::Population::SaleStatus::extraFemale);
                }
                else
                {
                    // My dam have performance, so comparison with the others is possible, do we sale ?
                    std::vector<float> sortedHerdPerformanceList;
                    for (std::map<unsigned int, DataStructures::Animal*>::iterator it = pHerd->getCurrentPresentAdults().begin(); it != pHerd->getCurrentPresentAdults().end(); it++)
                    {
                        float currentCowPerformance = ((DataStructures::DairyMammal*)(it->second))->getLastFullLactationPonderedMilkQuantity();
                        if (currentCowPerformance != 0.0f)
                        {
                            sortedHerdPerformanceList.push_back(currentCowPerformance);
                        }
                    } 
                    if (sortedHerdPerformanceList.size() > 0)
                    {
                        // Sort
                        std::sort(sortedHerdPerformanceList.begin(), sortedHerdPerformanceList.end()); // Sort by performance

                        // Here, we have the sorted list of all the present dam milk performance, we have to check if we sale, or if we protect
                        unsigned int lowPos = ((unsigned int) (sortedHerdPerformanceList.size() * FunctionalConstants::Population::LOW_FEMALE_CALVES_DAM_MILK_SCORE_PERCENT));
                        float lowPerformanceLevel = sortedHerdPerformanceList[lowPos]; // Get the occurency
                        if (calfDamPerformance < lowPerformanceLevel)
                        {
                            // This calf is to sale, because of low calf dam performance
                            pCalf->setSaleStatus(FunctionalEnumerations::Population::SaleStatus::extraFemale);
                        }
                        else
                        {
                            int hightPos = ((int) (sortedHerdPerformanceList.size() * (1.0f - FunctionalConstants::Population::HIGHT_FEMALE_CALVES_DAM_MILK_SCORE_PERCENT))) - 1;
                            if (hightPos < 0)
                            {
                                hightPos = 0;
                            }                                
                            float hightPerformanceLevel = sortedHerdPerformanceList[hightPos]; // Get the occurency

                            // What about the other female calves ?
                            float lowCalfDamPerformance = std::numeric_limits<float>::max();
                            DataStructures::DairyMammal* pCurrentLowDamPerformanceCalf = nullptr;
                            for (auto it : pHerd->getBatch(FunctionalEnumerations::Population::BatchType::femaleCalves)->getAnimalList())
                            {
                                DataStructures::DairyMammal* pCurrentCalf = (DataStructures::DairyMammal*)it.second;
                                if (pCurrentCalf->getDamLastFullLactationMilkQuantity() < lowCalfDamPerformance)
                                {
                                    pCurrentLowDamPerformanceCalf = pCurrentCalf;
                                    lowCalfDamPerformance = pCurrentLowDamPerformanceCalf->getDamLastFullLactationMilkQuantity();
                                }
                            }
                            if (calfDamPerformance > hightPerformanceLevel // To protect
                                    or calfDamPerformance > lowCalfDamPerformance) // Better dam performance
                            {
                                // This calf is to protect, or his dam performance is up that the low one, this calf is to keep, and the worst one (if not to protect) is to sale
                                if (lowCalfDamPerformance < hightPerformanceLevel)
                                {
                                    pCurrentLowDamPerformanceCalf->setSaleStatus(FunctionalEnumerations::Population::SaleStatus::extraFemale);
                                    PopulationManagement::affectDairyCowToBatch(pCurrentLowDamPerformanceCalf, FunctionalEnumerations::Population::BatchType::toSaleCalves, date);
                                }
                            }
                            else
                            {
                                // The dam performance of this calf is under the low one, we sale
                                pCalf->setSaleStatus(FunctionalEnumerations::Population::SaleStatus::extraFemale);
                            }
                        }
                    }
                    else
                    {
                        // No dam performance yet in the herd, comparison with the others is not possible, we sale
                        pCalf->setSaleStatus(FunctionalEnumerations::Population::SaleStatus::extraFemale);
                    }
                }
            }
        }
    }
} /* End of namespace Strategies */
}