#ifndef Population_Strategies_BalanceManagementStrategy_h
#define Population_Strategies_BalanceManagementStrategy_h

// boost

// project
#include "./FemaleCalveManagementStrategy.h"

namespace DataStructures
{
namespace Population
{
    namespace Strategies
    {
        class BalanceManagementStrategy : public FemaleCalveManagementStrategy
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        protected:
            BalanceManagementStrategy(){}
            void concrete(){}; // To allow instanciation
        
        public:
            BalanceManagementStrategy(DataStructures::DairyFarm* pFarm);
            virtual ~BalanceManagementStrategy();
            void setNewBornCalfSaleStatus(DataStructures::DairyMammal* pCalf, const boost::gregorian::date &date);

        };
    }
}
}
#endif // Population_Strategies_keepAllFemaleCalvesStrategy_h
