#include "FemaleCalveManagementStrategy.h"
#include "../../../DairyFarm.h"
#include "../../../../ExchangeInfoStructures/TechnicalConstants.h"
// boost

// project

BOOST_CLASS_EXPORT(DataStructures::Population::Strategies::FemaleCalveManagementStrategy) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    namespace Strategies
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Strategy); \
        ar & BOOST_SERIALIZATION_NVP(_cullingAnticipationValue); \
        
        IMPLEMENT_SERIALIZE_STD_METHODS(FemaleCalveManagementStrategy);

        FemaleCalveManagementStrategy::FemaleCalveManagementStrategy(DataStructures::DairyFarm* pFarm) : Strategy(pFarm)
        {
             updateRenewalRatio();
//            float renewalRatio = pFarm->getCurrentAnnualHerdRenewablePart();
//
//            float rangeX, rangeY;
//
//            if (renewalRatio >= TechnicalConstants::MEDIUM_RENEWAL_PART_CALIBRATION)
//            {
//                rangeX = TechnicalConstants::MAX_RENEWAL_PART_CALIBRATION;
//                rangeY = TechnicalConstants::MAX_RENEWAL_PART_CALIBRATION_RES;
//            }
//            else
//            {
//                rangeX = TechnicalConstants::MIN_RENEWAL_PART_CALIBRATION;
//                rangeY = TechnicalConstants::MIN_RENEWAL_PART_CALIBRATION_RES;
//            }
//            
//            // Calculation of the line formula
//            float a =   (rangeY - TechnicalConstants::MEDIUM_RENEWAL_PART_CALIBRATION_RES)
//                                             /
//                        (rangeX - TechnicalConstants::MEDIUM_RENEWAL_PART_CALIBRATION);
//            float b =   TechnicalConstants::MEDIUM_RENEWAL_PART_CALIBRATION_RES - (a * TechnicalConstants::MEDIUM_RENEWAL_PART_CALIBRATION);
//            
//            pFarm->setCullingAnticipationValue(a * renewalRatio + b);
        }
        
        void FemaleCalveManagementStrategy::updateRenewalRatio()
        {
            float renewalRatio = _pAppliesToFarm->getCurrentAnnualHerdRenewablePart();

            float rangeX, rangeY;

            if (renewalRatio >= TechnicalConstants::MEDIUM_RENEWAL_PART_CALIBRATION)
            {
                rangeX = TechnicalConstants::MAX_RENEWAL_PART_CALIBRATION;
                rangeY = TechnicalConstants::MAX_RENEWAL_PART_CALIBRATION_RES;
            }
            else
            {
                rangeX = TechnicalConstants::MIN_RENEWAL_PART_CALIBRATION;
                rangeY = TechnicalConstants::MIN_RENEWAL_PART_CALIBRATION_RES;
            }
            
            // Calculation of the line formula
            float a =   (rangeY - TechnicalConstants::MEDIUM_RENEWAL_PART_CALIBRATION_RES)
                                             /
                        (rangeX - TechnicalConstants::MEDIUM_RENEWAL_PART_CALIBRATION);
            float b =   TechnicalConstants::MEDIUM_RENEWAL_PART_CALIBRATION_RES - (a * TechnicalConstants::MEDIUM_RENEWAL_PART_CALIBRATION);
            
            _pAppliesToFarm->setCullingAnticipationValue(a * renewalRatio + b);
        }


        FemaleCalveManagementStrategy::~FemaleCalveManagementStrategy()
        {
        }
    }
} /* End of namespace Strategies */
}