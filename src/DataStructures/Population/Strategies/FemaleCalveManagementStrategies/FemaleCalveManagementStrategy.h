#ifndef Population_Strategies_FemaleCalveManagementStrategy_h
#define Population_Strategies_FemaleCalveManagementStrategy_h

// boost

// project
#include "../Strategy.h"
#include "../../../../ExchangeInfoStructures/FunctionalEnumerations.h"

namespace DataStructures
{
    class DairyMammal;

namespace Population
{
    namespace Strategies
    {
        class FemaleCalveManagementStrategy : public Strategy
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        protected:
            float _cullingAnticipationValue; // For calibrage values of the culling anticipation value

        
        protected:
            FemaleCalveManagementStrategy(){}
            FemaleCalveManagementStrategy(DataStructures::DairyFarm* pFarm);
        
        public:
            virtual ~FemaleCalveManagementStrategy();
            virtual void setNewBornCalfSaleStatus(DataStructures::DairyMammal* pCalf, const boost::gregorian::date &date) = 0;
            void updateRenewalRatio();

        };
    }
} /* End of namespace Health */
}
#endif // Population_Strategies_FemaleCalveManagementStrategy_h
