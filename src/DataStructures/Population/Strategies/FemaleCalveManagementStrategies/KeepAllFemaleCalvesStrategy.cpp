#include "KeepAllFemaleCalvesStrategy.h"

// boost

// project

BOOST_CLASS_EXPORT(DataStructures::Population::Strategies::KeepAllFemaleCalvesStrategy) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    namespace Strategies
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(FemaleCalveManagementStrategy);

        IMPLEMENT_SERIALIZE_STD_METHODS(KeepAllFemaleCalvesStrategy);
        
        KeepAllFemaleCalvesStrategy::KeepAllFemaleCalvesStrategy(DataStructures::DairyFarm* pFarm) : FemaleCalveManagementStrategy(pFarm)
        {
        }
        
        KeepAllFemaleCalvesStrategy::~KeepAllFemaleCalvesStrategy()
        {
        }
        
        void KeepAllFemaleCalvesStrategy::setNewBornCalfSaleStatus(DataStructures::DairyMammal* pCalf, const boost::gregorian::date &date)
        {
            // Nothing to do there, we keep all the new female borns
        }
    }
} /* End of namespace Strategies */
}