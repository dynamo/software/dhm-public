#ifndef Population_Strategies_KeepAllFemaleCalvesStrategy_h
#define Population_Strategies_KeepAllFemaleCalvesStrategy_h

// boost

// project
#include "./FemaleCalveManagementStrategy.h"

namespace DataStructures
{
namespace Population
{
    namespace Strategies
    {
        class KeepAllFemaleCalvesStrategy : public FemaleCalveManagementStrategy
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        
        protected:
            KeepAllFemaleCalvesStrategy(){}
            void concrete(){}; // To allow instanciation
        
        public:
            KeepAllFemaleCalvesStrategy(DataStructures::DairyFarm* pFarm);
            virtual ~KeepAllFemaleCalvesStrategy();
            void setNewBornCalfSaleStatus(DataStructures::DairyMammal* pCalf, const boost::gregorian::date &date);
        };
    }
}
}
#endif // Population_Strategies_KeepAllFemaleCalvesStrategy_h
