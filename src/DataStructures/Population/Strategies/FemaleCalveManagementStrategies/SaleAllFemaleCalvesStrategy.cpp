#include "SaleAllFemaleCalvesStrategy.h"

// boost

// project
#include "../../../../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "../../../DairyMammal.h"

BOOST_CLASS_EXPORT(DataStructures::Population::Strategies::SaleAllFemaleCalvesStrategy) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    namespace Strategies
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(FemaleCalveManagementStrategy);

        IMPLEMENT_SERIALIZE_STD_METHODS(SaleAllFemaleCalvesStrategy);
        
        SaleAllFemaleCalvesStrategy::SaleAllFemaleCalvesStrategy(DataStructures::DairyFarm* pFarm) : FemaleCalveManagementStrategy(pFarm)
        {
        }
        
        SaleAllFemaleCalvesStrategy::~SaleAllFemaleCalvesStrategy()
        {
        }

        void SaleAllFemaleCalvesStrategy::setNewBornCalfSaleStatus(DataStructures::DairyMammal* pCalf, const boost::gregorian::date &date)
        {
            // All female to sale
            pCalf->setSaleStatus(FunctionalEnumerations::Population::SaleStatus::extraFemale);
        }
    }
} /* End of namespace Strategies */
}