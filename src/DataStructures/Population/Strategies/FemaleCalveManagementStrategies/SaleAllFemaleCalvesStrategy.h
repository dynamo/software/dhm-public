#ifndef Population_Strategies_SaleAllFemaleCalvesStrategy_h
#define Population_Strategies_SaleAllFemaleCalvesStrategy_h

// boost

// project
#include "./FemaleCalveManagementStrategy.h"

namespace DataStructures
{
namespace Population
{
    namespace Strategies
    {
        class SaleAllFemaleCalvesStrategy : public FemaleCalveManagementStrategy
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        
        protected:
            SaleAllFemaleCalvesStrategy(){}
            void concrete(){}; // To allow instanciation
        
        public:
            SaleAllFemaleCalvesStrategy(DataStructures::DairyFarm* pFarm);
            virtual ~SaleAllFemaleCalvesStrategy();
            void setNewBornCalfSaleStatus(DataStructures::DairyMammal* pCalf, const boost::gregorian::date &date);
        };
    }
}
}
#endif // Population_Strategies_SaleAllFemaleCalvesStrategy_h
