#include "Strategy.h"

// boost

// project
#include "../../DairyFarm.h"

BOOST_CLASS_EXPORT(DataStructures::Population::Strategies::Strategy) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
namespace Population
{
    namespace Strategies
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_NVP(_pAppliesToFarm); \

        IMPLEMENT_SERIALIZE_STD_METHODS(Strategy);
        
        Strategy::Strategy(DataStructures::DairyFarm* pFarm)
        {
            _pAppliesToFarm = pFarm;
            
        }
        Strategy::~Strategy()
        {
        }
    }
} /* End of namespace Strategies */
}