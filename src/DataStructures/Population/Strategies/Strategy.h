#ifndef Population_Strategies_Strategy_h
#define Population_Strategies_Strategy_h

// boost

// project
#include "../../../Tools/Serialization.h"

namespace DataStructures
{
    class DairyFarm;

namespace Population
{
    namespace Strategies
    {
        class Strategy
        {
        DECLARE_SERIALIZE_STD_METHODS;

        protected:
            DataStructures::DairyFarm* _pAppliesToFarm = nullptr;
        
            Strategy(){};
            Strategy(DataStructures::DairyFarm* pFarm);
            virtual void concrete() = 0; // To ensure that it will not be instantiated
        
        public:
            virtual ~Strategy();
        };
    }
} /* End of namespace Population */
}
#endif // Population_Strategies_Strategy_h
