#include "PregnancyProcess.h"

// project
#include "Mammal.h"
#include "Insemination.h"
#include "../ResultStructures/DairyFarmResult.h"

BOOST_CLASS_EXPORT(DataStructures::PregnancyProcess) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(_beginDate); \
    ar & BOOST_SERIALIZATION_NVP(_pIsResultOfInsemination); \
    ar & BOOST_SERIALIZATION_NVP(_giveBirthNumber); \
    ar & BOOST_SERIALIZATION_NVP(_vExpectedSexes); \
    ar & BOOST_SERIALIZATION_NVP(_endPregnancyReason); \
    ar & BOOST_SERIALIZATION_NVP(_endDate); \

    IMPLEMENT_SERIALIZE_STD_METHODS(PregnancyProcess);

    PregnancyProcess::PregnancyProcess (const boost::gregorian::date& beginDate, Insemination* pIns)
    {
        _giveBirthNumber = 0;
        _beginDate = boost::gregorian::date(beginDate);
        _pIsResultOfInsemination = pIns;
    }
    
    boost::gregorian::date &PregnancyProcess::getBeginDate()
    {
        return _beginDate;
    }

    void PregnancyProcess::setEndDate(const boost::gregorian::date& endDate, FunctionalEnumerations::Reproduction::EndPregnancyReason reason
#ifdef _LOG
                                                        , Results::DairyFarmResult* pRes
#endif // _LOG                
                                        )
    {
        _endPregnancyReason = reason;
        _endDate = endDate;
        
#ifdef _LOG
        // Only in debug mode
        Results::Interval it;
        it.date1 = _beginDate;
        it.date2 = _endDate;
        it.comp = _endPregnancyReason;
        it.comp2 = _giveBirthNumber;
        pRes->getReproductionResults().PregnancyDurations.push_back(it);    
#endif // _LOG        

    }
    
    void PregnancyProcess::recordFarrow(Mammal* pMam)
    {
        _giveBirthNumber++;
    }
    
    unsigned int PregnancyProcess::getBirthNumber()
    {
        return _giveBirthNumber;
    }

    boost::gregorian::date &PregnancyProcess::getEndDate()
    {
        return _endDate;
    }

    bool PregnancyProcess::isFinished()
    {
        return !_endDate.is_not_a_date();
    }
    
    PregnancyProcess::~PregnancyProcess()
    {
    }
    
    boost::gregorian::date_duration PregnancyProcess::getDurationSinceBegin(const boost::gregorian::date& date)
    {
        return date - _beginDate;
    }
    
    boost::gregorian::date_duration PregnancyProcess::getEffectiveDuration()
    {
#ifdef _LOG
        assert (isFinished());
#endif // _LOG
        return _endDate - _beginDate;
    }
    
    FunctionalEnumerations::Reproduction::EndPregnancyReason PregnancyProcess::getEndPregnancyReason()
    {
        return _endPregnancyReason;
    }
    
    Insemination* PregnancyProcess::getFertilyInsemination()
    {
        return _pIsResultOfInsemination;
    }

} /* End of namespace DataStructures */