#ifndef DataStructures_PregnancyProcess_h
#define DataStructures_PregnancyProcess_h

// boost
#include <boost/date_time/gregorian/gregorian.hpp>

// standard
#include <vector>

// project
#include "../Tools/Serialization.h"
#include "../ExchangeInfoStructures/FunctionalEnumerations.h"

namespace Results
{
    class DairyFarmResult;
}
namespace DataStructures
{
    class Mammal;
    class Insemination;

    /// Process following a successful Insemination for female Mammal
    class PregnancyProcess
    {
    private:
        boost::gregorian::date _beginDate;
        Insemination* _pIsResultOfInsemination;
        unsigned int _giveBirthNumber;
        std::vector<char> _vExpectedSexes;
        FunctionalEnumerations::Reproduction::EndPregnancyReason _endPregnancyReason = FunctionalEnumerations::Reproduction::EndPregnancyReason::notYetFinished;
        boost::gregorian::date _endDate;

        DECLARE_SERIALIZE_STD_METHODS;
 
    protected:
        PregnancyProcess(){} // For serialization

    public:
        PregnancyProcess(const boost::gregorian::date& beginDate, Insemination* pIns);
        virtual ~PregnancyProcess();
        boost::gregorian::date &getBeginDate();
        boost::gregorian::date &getEndDate();
        void setEndDate(const boost::gregorian::date& endDate, FunctionalEnumerations::Reproduction::EndPregnancyReason reason
#ifdef _LOG
                                                        , Results::DairyFarmResult* pRes
#endif // _LOG                
                        );
        void recordFarrow(Mammal* pMam);
        unsigned int getBirthNumber();
        bool isFinished();
        boost::gregorian::date_duration getDurationSinceBegin(const boost::gregorian::date& date);
        boost::gregorian::date_duration getEffectiveDuration();
        FunctionalEnumerations::Reproduction::EndPregnancyReason getEndPregnancyReason();
        Insemination* getFertilyInsemination();
    };
} /* End of namespace DataStructures */
#endif // DataStructures_GestationProcess_h
