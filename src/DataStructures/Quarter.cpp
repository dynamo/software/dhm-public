#include <vector>

#include "Quarter.h"

// Project
#include "Udder.h"

BOOST_CLASS_EXPORT(DataStructures::Quarter) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(_pIsInUdder); \
    ar & BOOST_SERIALIZATION_NVP(_number); \

    IMPLEMENT_SERIALIZE_STD_METHODS(Quarter);

    Quarter::Quarter(Udder* pUdder, unsigned int number)
    {
        _pIsInUdder = pUdder;
        _number = number;
    }

    Quarter::~Quarter ()
    {
        // Udder
        _pIsInUdder = nullptr;
    }

    void Quarter::progress(const boost::gregorian::date &simDate, Tools::Random &random)
    {

    }

#ifdef _LOG
    unsigned int Quarter::getMammalUniqueId()
    {
        return _pIsInUdder->getMammalUniqueId();
    }
#endif // _LOG

}