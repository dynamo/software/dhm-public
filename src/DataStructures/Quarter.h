#ifndef DataStructures_Quarter_h
#define DataStructures_Quarter_h

// project
#include "../Tools/Serialization.h"
#include "../Tools/Random.h"

namespace Results
{
#ifdef _LOG
    struct DayAnimalLogInformations;
#endif // _LOG
    class Result;
}

namespace DataStructures
{
    class Udder;

    /// Elementary part of Udder
    class Quarter
    {
    private:

        DECLARE_SERIALIZE_STD_METHODS;

    protected:
        Udder* _pIsInUdder = nullptr;
        unsigned int _number;

        Quarter(){} // For serialization
        virtual void concrete() = 0; // To ensure that it will not be instantiated

    public:
        Quarter(Udder* pUdder, unsigned int number);
        virtual ~Quarter (); // Destructor
        unsigned int getMammalUniqueId();
        virtual void progress(const boost::gregorian::date &simDate, Tools::Random &random);
    };
}
#endif // DataStructures_Quarter_h