////////////////////////////////////////
//                                    //
// File : SystemToSimulate.cpp        //
//                                    //
// Author : Philippe Gontier          //
//                                    //
// Version 1.0                        //
//                                    //
// Date : 10/03/2022                  //
//                                    //
////////////////////////////////////////

#include "SystemToSimulate.h"

// project
#include "../Tools/Random.h"
#include "../ResultStructures/Result.h"

BOOST_CLASS_EXPORT(DataStructures::SystemToSimulate) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(_beginPresimulationDate); \
    ar & BOOST_SERIALIZATION_NVP(_beginSimulationDate); \
    ar & BOOST_SERIALIZATION_NVP(_beginResultDate); \
    ar & BOOST_SERIALIZATION_NVP(_currentEvoluatingAccountingModel); \
    ar & BOOST_SERIALIZATION_NVP(_hasOriginSystemToSimulateInfo); \
    ar & BOOST_SERIALIZATION_NVP(_timeLapsduration); \
    ar & BOOST_SERIALIZATION_NVP(_presimulationHasToBeDone); \
    ar & BOOST_SERIALIZATION_NVP(_beginPresimulationDate); \
    ar & BOOST_SERIALIZATION_NVP(_beginSimulationDate); \
    ar & BOOST_SERIALIZATION_NVP(_beginResultDate); \
    ar & BOOST_SERIALIZATION_NVP(_endSimulationDate); \
    ar & BOOST_SERIALIZATION_NVP(_timeLapsduration); \


//    ar & BOOST_SERIALIZATION_NVP(_num); \
//    ar & BOOST_SERIALIZATION_NVP(_pRandom); \
//    ar & BOOST_SERIALIZATION_NVP(_pSimulationResults); \
//    ar & BOOST_SERIALIZATION_NVP(_pCurrentResults); \
//     ar & BOOST_SERIALIZATION_NVP(_pCurrentResults);


    IMPLEMENT_SERIALIZE_STD_METHODS(SystemToSimulate);
    
    SystemToSimulate::SystemToSimulate(ExchangeInfoStructures::Parameters::DairyFarmParameters &info)
    {
        _hasOriginSystemToSimulateInfo = info;
    }
        
    SystemToSimulate::~SystemToSimulate()
    {
        delete _pRandom;
    }
        
    std::string SystemToSimulate::getName()
    {
        return _hasOriginSystemToSimulateInfo.name;
    }

    void SystemToSimulate::setNum(unsigned int num)
    {
        assert (_pRandom == nullptr);
        // Random instance
        _num = num;
        _pRandom = new Tools::Random(num);
    }

    unsigned int SystemToSimulate::getNum()
    {
        return _num;
    }
    
    void SystemToSimulate::setPresimulationHasToBeDone(bool hasToBeDone)
    {
        _presimulationHasToBeDone = hasToBeDone;
    }
    
    unsigned int SystemToSimulate::getYearNumberForPresimulation()
    {
        return 0; // Default value
    }
        
    Tools::Random &SystemToSimulate::getRandom()
    {
        return *_pRandom;
    }

    void SystemToSimulate::setResults(Results::Result* pSimulationRes
#ifndef _LOG
                                                                                    , Results::Result* pPresimulationRes
#endif // _LOG      
                                                                                    )
    {
        _pSimulationResults = pSimulationRes;
#ifndef _LOG
        _pPresimulationResults = pPresimulationRes;
#endif // _LOG
    }
    
    unsigned int SystemToSimulate::getDayOfCampaign(const boost::gregorian::date &currentDate)
    {
        boost::gregorian::date tmp = boost::gregorian::date(currentDate.year(), _beginSimulationDate.month(), _beginSimulationDate.day());
        if (tmp > currentDate) tmp = boost::gregorian::date(currentDate.year()-1, _beginSimulationDate.month(), _beginSimulationDate.day());
        boost::gregorian::date_duration dd(currentDate - tmp);
        return dd.days();
    }
    
    boost::gregorian::date_period SystemToSimulate::getCampaignPeriod(const boost::gregorian::date &currentSimulationDate)
    {
        return boost::gregorian::date_period(getFirstDayOfTheCampaign(currentSimulationDate), getLastDayOfTheCampaign(currentSimulationDate) + boost::gregorian::date_duration(1));
    }
    
    boost::gregorian::date SystemToSimulate::getFirstDayOfTheCampaign(const boost::gregorian::date &currentDate)
    {
        boost::gregorian::date firstDayFolowingCampain = getLastDayOfTheCampaign(currentDate) + boost::gregorian::date_duration(1);
        return boost::gregorian::date(firstDayFolowingCampain.year() - 1, firstDayFolowingCampain.month(), firstDayFolowingCampain.day());
    }
    
    boost::gregorian::date SystemToSimulate::getLastDayOfTheCampaign(const boost::gregorian::date &currentDate)
    {
        unsigned int yearDelta = currentDate.month() < _beginSimulationDate.month() ? 0 : 1;
        return boost::gregorian::date(currentDate.year() + yearDelta, _beginSimulationDate.month(), _beginSimulationDate.day()) - boost::gregorian::date_duration(1);
    }
    
    bool SystemToSimulate::isFirstDayOfTheCampaign(const boost::gregorian::date &currentDate)
    {
        return currentDate == getFirstDayOfTheCampaign(currentDate);
    }
    
    bool SystemToSimulate::isLastDayOfTheCampaign(const boost::gregorian::date &currentDate)
    {
        return currentDate == getLastDayOfTheCampaign(currentDate);
    }

    void SystemToSimulate::setBeginPresimulationDate(const boost::gregorian::date &beginPreDate)
    {
        _beginPresimulationDate = beginPreDate;
    }
        
    void SystemToSimulate::setBeginSimulationDate(const boost::gregorian::date &beginDate)
    {
        _beginSimulationDate = beginDate;
    }
        
    void SystemToSimulate::setBeginResultDate(const boost::gregorian::date &beginDate)
    {
        _beginResultDate = beginDate;
    }
        
    void SystemToSimulate::setEndSimulationDate(const boost::gregorian::date &endDate)
    {
        _endSimulationDate = endDate;
    }
        
    void SystemToSimulate::setCurrentAccountingModel(ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &am)
    {
        _currentEvoluatingAccountingModel = am;
    }
    
    void SystemToSimulate::setTimeLapsduration(const boost::gregorian::date_duration &timeLapsduration)
    {
        _timeLapsduration = timeLapsduration;
    } 
}