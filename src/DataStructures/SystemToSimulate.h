////////////////////////////////////////
//                                    //
// File : SystemToSimulate.h          //
//                                    //
// Author : Philippe Gontier          //
//                                    //
// Version 1.0                        //
//                                    //
// Date : 10/03/2022                  //
//                                    //
////////////////////////////////////////

#ifndef DataStructures_SystemToSimulate_h
#define DataStructures_SystemToSimulate_h

// project
#include "../Tools/Serialization.h"
#include "../ExchangeInfoStructures/DairyFarmParameters.h"
#include "../ExchangeInfoStructures/DairyFarmAccountingModelParameters.h"

namespace Tools
{
    class Random;
}
namespace Results
{
    class Result;
}

namespace DataStructures
{
    /// Generic System to simulate
    class SystemToSimulate
    {
        DECLARE_SERIALIZE_STD_METHODS;
		
    private:       
        unsigned int _num;
        Tools::Random *_pRandom = nullptr; // Random instance

    protected:
#ifndef _LOG
        Results::Result* _pPresimulationResults = nullptr;
#endif // _LOG
        Results::Result* _pSimulationResults = nullptr;
        Results::Result* _pCurrentResults = nullptr;
        ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters _currentEvoluatingAccountingModel;
        boost::gregorian::date_duration _timeLapsduration;
        bool _presimulationHasToBeDone = true;
        boost::gregorian::date _beginPresimulationDate;
        boost::gregorian::date _beginSimulationDate;
        boost::gregorian::date _beginResultDate;
        boost::gregorian::date _endSimulationDate;
        ExchangeInfoStructures::Parameters::DairyFarmParameters _hasOriginSystemToSimulateInfo;
        
    protected:
        SystemToSimulate(){} // serialization constructor
        virtual void concrete() = 0; // To ensure that it will not be instantiated
    
    public:
        SystemToSimulate(ExchangeInfoStructures::Parameters::DairyFarmParameters &info);
        virtual ~SystemToSimulate();
        virtual void action() = 0;

        std::string getName();
        void setNum(unsigned int num);
        unsigned int getNum();
        void setPresimulationHasToBeDone(bool hasToBeDone);

        virtual unsigned int getYearNumberForPresimulation();
        Tools::Random &getRandom();
        unsigned int getDayOfCampaign(const boost::gregorian::date &currentDate);
        boost::gregorian::date_period getCampaignPeriod(const boost::gregorian::date &currentSimulationDate);
        boost::gregorian::date getFirstDayOfTheCampaign(const boost::gregorian::date &currentDate);
        boost::gregorian::date getLastDayOfTheCampaign(const boost::gregorian::date &currentDate);
        bool isFirstDayOfTheCampaign(const boost::gregorian::date &currentDate);
        bool isLastDayOfTheCampaign(const boost::gregorian::date &currentDate);
        void setResults(Results::Result* pSimulationRes
#ifndef _LOG
                                                                                    , Results::Result* pPresimulationRes
#endif // _LOG      
                                                                                    );
        virtual void setBeginPresimulationDate(const boost::gregorian::date &beginPreDate);
        virtual void setBeginSimulationDate(const boost::gregorian::date &beginDate);
        virtual void setBeginResultDate(const boost::gregorian::date &beginDate);
        virtual void setEndSimulationDate(const boost::gregorian::date &endDate);
        void setCurrentAccountingModel(ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &am);
        void setTimeLapsduration(const boost::gregorian::date_duration &timeLapsduration);
#ifdef _LOG
        virtual void closeResults() = 0; // End of simulation signal
#endif // _LOG
    }; /* End of class SystemToSimulate */
} /* End of namespace DataStructures */
#endif // DataStructures_SystemToSimulate_h
