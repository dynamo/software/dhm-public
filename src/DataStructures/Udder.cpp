#include <vector>

#include "Udder.h"

// Project
#include "Mammal.h"
#include "Quarter.h"

#ifdef _LOG
#include "../ResultStructures/DairyFarmResult.h"
#endif // _LOG

BOOST_CLASS_EXPORT(DataStructures::Udder) // Recommended (mandatory if virtual serialization)

namespace DataStructures
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(_pIsInMammal); \
    ar & BOOST_SERIALIZATION_NVP(_originQuarterNumber); \
    ar & BOOST_SERIALIZATION_NVP(_vpIsComposedByQuarters); \

    IMPLEMENT_SERIALIZE_STD_METHODS(Udder);

    Udder::Udder(Mammal* pMammal, unsigned int quarterNumber)
    {
        _pIsInMammal = pMammal;
        _originQuarterNumber = quarterNumber;
    }

    Udder::~Udder ()
    {
        for (unsigned int i = 0; i < _vpIsComposedByQuarters.size(); i++)
        {
            delete _vpIsComposedByQuarters[i];
        }
        _vpIsComposedByQuarters.clear();
        _pIsInMammal = nullptr;
    }

    void Udder::progress(const boost::gregorian::date &simDate
#ifdef _LOG
                            , Results::DayAnimalLogInformations &log
#endif // _LOG
                            , Tools::Random &random)
    {

    }

#ifdef _LOG
    unsigned int Udder::getMammalUniqueId()
    {
        return _pIsInMammal->getUniqueId();
    }
#endif // _LOG    
    
    Results::DairyFarmResult* Udder::getCurrentResults()
    {
        return _pIsInMammal->getCurrentResults();
    }
}