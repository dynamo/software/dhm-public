#ifndef DataStructures_Udder_h
#define DataStructures_Udder_h

// project
#include "../Tools/Serialization.h"
#include "../Tools/Random.h"

namespace Results
{
#ifdef _LOG
    struct DayAnimalLogInformations;
#endif // _LOG
    class DairyFarmResult;
}

namespace DataStructures
{
    class Mammal;
    class Quarter;

    /// Mammal Organ composed by Quarters
    class Udder
    {
    private:

        DECLARE_SERIALIZE_STD_METHODS;

    protected:
        Mammal* _pIsInMammal = nullptr;
        unsigned int _originQuarterNumber = 0;
        std::vector<Quarter*> _vpIsComposedByQuarters;

        Udder(){} // For serialization
        virtual void concrete() = 0; // To ensure that it will not be instantiated

    public:
        Udder(Mammal* pMammal, unsigned int quarterNumber);
        virtual ~Udder (); // Destructor
        unsigned int getMammalUniqueId();
        virtual void progress(const boost::gregorian::date &simDate
#ifdef _LOG
                            , Results::DayAnimalLogInformations &log
#endif // _LOG
                            , Tools::Random &random);
        Results::DairyFarmResult* getCurrentResults();
    };
}
#endif // DataStructures_Udder_h