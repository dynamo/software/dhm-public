//////////////////////////////////////////
//                                      //
// File : AccountingModelParameters.cpp //
//                                      //
// Author : Philippe Gontier            //
//                                      //
// Version 1.0                          //
//                                      //
// Date : 01/10/2015                    //
//                                      //
//////////////////////////////////////////

#include "AccountingModelParameters.h"

// project

BOOST_CLASS_EXPORT(ExchangeInfoStructures::Parameters::AccountingModelParameters) // Recommended (mandatory if virtual serialization)

namespace ExchangeInfoStructures
{
namespace Parameters
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(HeadParameters); \
        ar & BOOST_SERIALIZATION_NVP(prices); \

    IMPLEMENT_SERIALIZE_STD_METHODS(AccountingModelParameters);

    // Constructeur de copie   
    AccountingModelParameters::AccountingModelParameters (const AccountingModelParameters &obj) : HeadParameters(obj)
    {
        prices = obj.prices;
    }
    
    bool AccountingModelParameters::autoControl (std::string &message)
    {
        message = "";
        bool res = HeadParameters::autoControl(message);

        return res;
    } /* End of method AutoControl */   
    
    void AccountingModelParameters::actualizePricesForNewMonth()
    {
        for (auto price : prices)
        {
            price.second.actualizePricesForNewMonth();
        }
    }
    
    float AccountingModelParameters::getCurrentPrice(const std::string &key)
    {
        return (prices.find(key)->second).currentPrice;
    }
} /* End of namespace AccountingModelParameters */
}