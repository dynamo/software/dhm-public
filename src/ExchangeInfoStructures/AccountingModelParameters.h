//////////////////////////////////////////
//                                      //
// File : AccountingModelParameters.h   //
//                                      //
// Author : Philippe Gontier            //
//                                      //
// Version 1.0                          //
//                                      //
// Date : 01/10/2015                    //
//                                      //
//////////////////////////////////////////

#ifndef Parameters_AccountingModelParameters_h
#define Parameters_AccountingModelParameters_h

// standard
#include <string>
#include <map>

// project
#include "HeadParameters.h"
#include "PriceTendencyParameters.h"

// Dll import/export
namespace ExchangeInfoStructures
{
namespace Parameters
{
    
    /// This structure make it possible to define the cost and benefits for all accounting actions. 
    /// See 3.2.3 "Accounting model" of the "Functional description and terms of use" documentation.
    struct AccountingModelParameters : public HeadParameters
    {
    private:
        
        // serialization
        DECLARE_SERIALIZE_STD_METHODS;

    public:
        
        std::map<std::string, PriceTendencyParameters> prices;
        
        /// Value checking, errors in the std::string parameter reference
        virtual bool autoControl (
                                    /// Reference std::string to put the error message (empty value if no error)
                                    std::string &message
                                    ) override;
        AccountingModelParameters(){}
        virtual ~AccountingModelParameters(){}
        AccountingModelParameters (const AccountingModelParameters &obj);        //constructeur de copie   
        void actualizePricesForNewMonth();
        float getCurrentPrice(const std::string &key);
    }; /* End of class AccountingModelParameters */
} /* End of namespace Parameters */
}
#endif // Parameters_AccountingModelParameters_h
