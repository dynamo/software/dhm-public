/////////////////////////////////////////////////
//                                             //
// File : DairyFarmAccountingModelParameters.h //
//                                             //
// Author : Philippe Gontier                   //
//                                             //
// Date : 01/10/2015                           //
//                                             //
// Accounting model for dairy farm             //
//                                             //
/////////////////////////////////////////////////

#ifndef Parameters_DairyFarmAccountingModelParameters_h
#define Parameters_DairyFarmAccountingModelParameters_h

// standard
#include <string>
#include <map>

// project
#include "AccountingModelParameters.h"
#include "FunctionalConstants.h"
#include "PriceTendencyParameters.h"

namespace ExchangeInfoStructures
{
namespace Parameters
{
    
    /// This structure make it possible to define the cost and benefits for all accounting actions. 
    /// See 3.2.3 "Accounting model" of the "Functional description and terms of use" documentation.
    struct DairyFarmAccountingModelParameters : public AccountingModelParameters
    {
    private:

        // serialization
        DECLARE_SERIALIZE_STD_METHODS;

    protected:
        void concrete(){}; // To allow instanciation
        
    public:

        DairyFarmAccountingModelParameters();
        DairyFarmAccountingModelParameters(std::string &accountingModelName, std::string &fileName);
        virtual ~DairyFarmAccountingModelParameters(){};
        DairyFarmAccountingModelParameters (const DairyFarmAccountingModelParameters &obj);        //constructeur de copie   
#ifdef _LOG
        static void saveDefaultAccountingModelValues(const std::string &path);
#endif // _LOG

    }; /* End of class AccountingModelParameters */
} /* End of namespace Parameters */
}
#endif // Parameters_DairyFarmAccountingModelParameters_h
