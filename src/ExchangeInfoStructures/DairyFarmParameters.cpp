#include "DairyFarmParameters.h"

// standard
#include <stdlib.h>     /* atoi */

// boost
#include <boost/foreach.hpp>

// project
#include "../Tools/Tools.h"
#include "TechnicalConstants.h"
#include "FunctionalConstants.h"
#include "MilkProductCharacteristic.h"
#include "LanguageDictionary.h"
#include "../Tools/Display.h"

BOOST_CLASS_EXPORT(ExchangeInfoStructures::Parameters::DairyFarmParameters) // Recommended (mandatory if virtual serialization)

namespace ExchangeInfoStructures
{
namespace Parameters
{
#ifdef _SENSIBILITY_ANALYSIS
#define SA_SERIALIZATION \
        ar & BOOST_SERIALIZATION_NVP(SA_mastitisPercentPreventionProgress); \
        ar & BOOST_SERIALIZATION_NVP(SA_ketosisPercentPreventionProgress);  \
        ar & BOOST_SERIALIZATION_NVP(SA_additionalMastitisHealingIfCareVetContract);  \
        ar & BOOST_SERIALIZATION_NVP(SA_additionalKetosisHealingIfCareVetContract);  \
        ar & BOOST_SERIALIZATION_NVP(SA_nonManagedMortalityFactorWithVetCareContract);
#else // _SENSIBILITY_ANALYSIS
#define SA_SERIALIZATION
#endif // _SENSIBILITY_ANALYSIS

    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(SystemToSimulateParameters); \
        ar & BOOST_SERIALIZATION_NVP(vetReproductionContract); \
        ar & BOOST_SERIALIZATION_NVP(ageToBreedingDecision); \
        ar & BOOST_SERIALIZATION_NVP(detectionMode); \
        ar & BOOST_SERIALIZATION_NVP(slipperyFloor); \
        ar & BOOST_SERIALIZATION_NVP(matingPlan); \
        ar & BOOST_SERIALIZATION_NVP(fertilityFactor); \
        ar & BOOST_SERIALIZATION_NVP(breedingDelayAfterCalvingDecision); \
        ar & BOOST_SERIALIZATION_NVP(numberOfDaysAfterHeiferBreedingDecisionForStopFirstInseminationDecision); \
        ar & BOOST_SERIALIZATION_NVP(numberOfDaysAfterHeiferBreedingDecisionForStopInseminationDecision); \
        ar & BOOST_SERIALIZATION_NVP(numberOfDaysAfterAdultBreedingDecisionForStopFirstInseminationDecision); \
        ar & BOOST_SERIALIZATION_NVP(numberOfDaysAfterAdultBreedingDecisionForStopInseminationDecision); \
        ar & BOOST_SERIALIZATION_NVP(driedPeriodDuration); \
        ar & BOOST_SERIALIZATION_NVP(milkingFrequency); \
        ar & BOOST_SERIALIZATION_NVP(dayBeforeDeliverMilk); \
        ar & BOOST_SERIALIZATION_NVP(herdProductionDelta); \
        ar & BOOST_SERIALIZATION_NVP(G1MastitisDetectionSensibility); \
        ar & BOOST_SERIALIZATION_NVP(mastitisSaisonalityProbabilityFactor); \
        ar & BOOST_SERIALIZATION_NVP(bacteriumIncidencePart); \
        ar & BOOST_SERIALIZATION_NVP(mastitisTreatmentPlan); \
        ar & BOOST_SERIALIZATION_NVP(individualMastitisIncidencePreventionFactor); \
        ar & BOOST_SERIALIZATION_NVP(herdMastitisIncidencePreventionFactor); \
        ar & BOOST_SERIALIZATION_NVP(vetCareContract); \
        ar & BOOST_SERIALIZATION_NVP(ketosisIncidencePreventionFactor); \
        ar & BOOST_SERIALIZATION_NVP(monensinBolusUsage); \
        ar & BOOST_SERIALIZATION_NVP(cetoDetectUsage); \
        ar & BOOST_SERIALIZATION_NVP(herdNavigatorOption); \
        ar & BOOST_SERIALIZATION_NVP(ketosisTreatmentPlan); \
        ar & BOOST_SERIALIZATION_NVP(lamenessIncidencePreventionFactor); \
        ar & BOOST_SERIALIZATION_NVP(footBath); \
        ar & BOOST_SERIALIZATION_NVP(footBathProtectionDurationForLameness); \
        ar & BOOST_SERIALIZATION_NVP(footBathFrequency); \
        ar & BOOST_SERIALIZATION_NVP(preventiveTrimmingOption); \
        ar & BOOST_SERIALIZATION_NVP(cowCountForSmallTrimmingOrCareGroup); \
        ar & BOOST_SERIALIZATION_NVP(lamenessDetectionMode); \
        ar & BOOST_SERIALIZATION_NVP(lamenessTreatmentPlan); \
        ar & BOOST_SERIALIZATION_NVP(maleDataFile); \
        ar & BOOST_SERIALIZATION_NVP(geneticStrategy); \
        ar & BOOST_SERIALIZATION_NVP(initialBreed); \
        ar & BOOST_SERIALIZATION_NVP(meanAdultNumberTarget); \
        ar & BOOST_SERIALIZATION_NVP(initialAnnualHerdRenewablePart); \
        ar & BOOST_SERIALIZATION_NVP(annualHerdRenewableDelta); \
        ar & BOOST_SERIALIZATION_NVP(femaleCalveManagement); \
        ar & BOOST_SERIALIZATION_NVP(maxLactationRankForEndInseminationDecision); \
        ar & BOOST_SERIALIZATION_NVP(minMilkProductionHerdRatioForEndInseminationDecision); \
        ar & BOOST_SERIALIZATION_NVP(maxSCCLevelForEndInseminationDecision); \
        ar & BOOST_SERIALIZATION_NVP(minimumMilkQuantityFactorForRentability); \
        ar & BOOST_SERIALIZATION_NVP(youngHeiferBeginPastureDate); \
        ar & BOOST_SERIALIZATION_NVP(youngHeiferEndPastureDate); \
        ar & BOOST_SERIALIZATION_NVP(otherHeiferAndDriedCowBeginPastureDate); \
        ar & BOOST_SERIALIZATION_NVP(otherHeiferAndDriedCowEndPastureDate); \
        ar & BOOST_SERIALIZATION_NVP(lactatingCowBeginFullPastureDate); \
        ar & BOOST_SERIALIZATION_NVP(lactatingCowEndFullPastureDate); \
        ar & BOOST_SERIALIZATION_NVP(lactatingCowBeginStabulationDate); \
        ar & BOOST_SERIALIZATION_NVP(lactatingCowEndStabulationDate); \
        ar & BOOST_SERIALIZATION_NVP(calfStrawConsumption); \
        ar & BOOST_SERIALIZATION_NVP(heiferStrawConsumption); \
        ar & BOOST_SERIALIZATION_NVP(adultStrawConsumption); \
        ar & BOOST_SERIALIZATION_NVP(feedingPlan); \
        SA_SERIALIZATION
        
    IMPLEMENT_SERIALIZE_STD_METHODS(DairyFarmParameters);
    
    DairyFarmParameters::DairyFarmParameters()
    {
        // Reproduction decision
        vetReproductionContract = FunctionalConstants::Reproduction::DEFAULT_VET_REPRODUCTION_CONTRACT_OPTION;
        ageToBreedingDecision = FunctionalConstants::Reproduction::DEFAULT_AGE_TO_BREEDING_DECISION_VALUE;
        detectionMode = FunctionalConstants::Reproduction::DEFAULT_DETECTION_MODE_VALUE;
        slipperyFloor = FunctionalConstants::Reproduction::DEFAULT_SLIPPERY_FLOOR_VALUE;
        // (Mating plan) ...
        fertilityFactor = FunctionalConstants::Reproduction::DEFAULT_FERTILITY_FACTOR_VALUE;
        breedingDelayAfterCalvingDecision = FunctionalConstants::Reproduction::DEFAULT_BREEDING_DELAY_AFTER_CALVING_DECISION_VALUE;
        numberOfDaysAfterHeiferBreedingDecisionForStopFirstInseminationDecision = FunctionalConstants::Reproduction::DEFAULT_NUMBER_OF_DAYS_AFTER_HEIFER_BREEDING_DECISION_FOR_STOP_FIRST_INSEMINATION_DECISION_VALUE;
        numberOfDaysAfterHeiferBreedingDecisionForStopInseminationDecision = FunctionalConstants::Reproduction::DEFAULT_NUMBER_OF_DAYS_AFTER_HEIFER_BREEDING_DECISION_FOR_STOP_INSEMINATION_DECISION_VALUE;
        numberOfDaysAfterAdultBreedingDecisionForStopFirstInseminationDecision = FunctionalConstants::Reproduction::DEFAULT_NUMBER_OF_DAYS_AFTER_ADULT_BREEDING_DECISION_FOR_STOP_FIRST_INSEMINATION_DECISION_VALUE;
        numberOfDaysAfterAdultBreedingDecisionForStopInseminationDecision = FunctionalConstants::Reproduction::DEFAULT_NUMBER_OF_DAYS_AFTER_ADULT_BREEDING_DECISION_FOR_STOP_INSEMINATION_DECISION_VALUE;
        
        // Lactation decision
        driedPeriodDuration = FunctionalConstants::Lactation::DEFAULT_DRIED_PERIOD_DURATION_VALUE;        
        milkingFrequency = FunctionalConstants::Lactation::DEFAULT_MILKING_FREQUENCY_VALUE;
        dayBeforeDeliverMilk = FunctionalConstants::Lactation::DEFAULT_DELAY_BEFORE_DELIVER_MILK;
        herdProductionDelta.quantity = FunctionalConstants::Lactation::DEFAULT_HERD_PRODUCTION_DELTA_LAIT;
        herdProductionDelta.TB = FunctionalConstants::Lactation::DEFAULT_HERD_PRODUCTION_DELTA_TB;
        herdProductionDelta.TP = FunctionalConstants::Lactation::DEFAULT_HERD_PRODUCTION_DELTA_TP;
        
        // Health decision
        vetCareContract = FunctionalConstants::Health::DEFAULT_VET_CARE_CONTRACT_OPTION;
        // Mastitis
        G1MastitisDetectionSensibility = FunctionalConstants::Health::G1_MASTITIS_DEFAULT_DETECTION_SENSIBILITY;
        mastitisSaisonalityProbabilityFactor = FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR;
        bacteriumIncidencePart = FunctionalConstants::Health::DEFAULT_BACTERIUM_MASTITIS_INCIDENCE_PART;
        // (Mastitis treatment plan) ...
        individualMastitisIncidencePreventionFactor =  FunctionalConstants::Health::DEFAULT_COW_MASTITIS_INCIDENCE_PREVENTION_FACTOR;
        herdMastitisIncidencePreventionFactor =  FunctionalConstants::Health::DEFAULT_HERD_MASTITIS_INCIDENCE_PREVENTION_FACTOR;
        // Ketosis
        ketosisIncidencePreventionFactor = FunctionalConstants::Health::DEFAULT_KETOSIS_INCIDENCE_PREVENTION_FACTOR;
        monensinBolusUsage = FunctionalConstants::Health::DEFAULT_MONENSIN_BOLUS_USE;
        cetoDetectUsage = FunctionalConstants::Health::DEFAULT_CETODETECT_USAGE;
        herdNavigatorOption = FunctionalConstants::Health::DEFAULT_CETO_HERD_NAVIGATOR_OPTION;
        // (Ketosis treatment plan) ...
        // Lameness
        lamenessIncidencePreventionFactor = FunctionalConstants::Health::DEFAULT_LAMENESS_INCIDENCE_PREVENTION_FACTOR;
        footBath = FunctionalConstants::Health::DEFAULT_FOOT_BATH_OPTION;
        footBathProtectionDurationForLameness = FunctionalConstants::Health::DEFAULT_FOOT_BATH_EFFECT_DURATION;
        footBathFrequency = FunctionalConstants::Health::DEFAULT_FOOT_BATH_FREQUENCY;
        preventiveTrimmingOption = FunctionalConstants::Health::DEFAULT_PREVENTIVE_TRIMMING_OPTION;
        cowCountForSmallTrimmingOrCareGroup = FunctionalConstants::Health::DEFAULT_COW_COUNT_FOR_SMALL_TRIMMING_OR_CARE_GROUP;
        lamenessDetectionMode = FunctionalConstants::Health::DEFAULT_LAMENESS_DETECTION_MODE;

        // (Lameness treatment plan) ...
        
        // Genetic settings
        maleDataFile = "";
        geneticStrategy = FunctionalConstants::Genetic::DEFAULT_GENETIC_STRATEGY;
        
        // Population decision
        initialBreed = FunctionalConstants::Population::DEFAULT_INITIAL_COW_BREED;
        meanAdultNumberTarget = FunctionalConstants::Population::DEFAULT_MEAN_ADULT_NUMBER_TARGET;
        initialAnnualHerdRenewablePart = FunctionalConstants::Population::DEFAULT_ANNUAL_HERD_RENEWAL_PART;     
        annualHerdRenewableDelta = FunctionalConstants::Population::DEFAULT_ANNUAL_HERD_RENEWAL_DELTA;     
        femaleCalveManagement = FunctionalConstants::Population::DEFAULT_FEMALE_CALVE_MANAGEMENT;
        maxLactationRankForEndInseminationDecision = FunctionalConstants::Population::DEFAULT_MAX_LACTATION_RANK_FOR_END_INSEMINATION_DECISION;
        minMilkProductionHerdRatioForEndInseminationDecision = FunctionalConstants::Population::DEFAULT_MIN_MILK_PRODUCTION_HERD_RATIO_FOR_END_INSEMINATION_DECISION;
        maxSCCLevelForEndInseminationDecision = FunctionalConstants::Population::DEFAULT_MAX_SCC_LEVEL_FOR_END_INSEMINATION_DECISION;
        minimumMilkQuantityFactorForRentability = FunctionalConstants::Population::DEFAULT_MODULATION_FACTOR_FOR_LOW_QUANTITY_FOR_RENTABILITY;
        youngHeiferBeginPastureDate = FunctionalConstants::Population::DEFAULT_YOUNG_HEIFER_BEGIN_PASTURE_DATE;
        youngHeiferEndPastureDate = FunctionalConstants::Population::DEFAULT_YOUNG_HEIFER_END_PASTURE_DATE;
        otherHeiferAndDriedCowBeginPastureDate = FunctionalConstants::Population::DEFAULT_OTHER_HEIFER_AND_DRIED_COW_BEGIN_PASTURE_DATE;
        otherHeiferAndDriedCowEndPastureDate = FunctionalConstants::Population::DEFAULT_OTHER_HEIFER_AND_DRIED_COW_END_PASTURE_DATE;
        lactatingCowBeginFullPastureDate = FunctionalConstants::Population::DEFAULT_LACTATING_COW_BEGIN_FULL_PASTURE_DATE;
        lactatingCowEndFullPastureDate = FunctionalConstants::Population::DEFAULT_LACTATING_COW_END_FULL_PASTURE_DATE;
        lactatingCowBeginStabulationDate = FunctionalConstants::Population::DEFAULT_LACTATING_COW_BEGIN_STABULATION_DATE;
        lactatingCowEndStabulationDate = FunctionalConstants::Population::DEFAULT_LACTATING_COW_END_STABULATION_DATE;
        
        calfStrawConsumption = FunctionalConstants::Population::DEFAULT_CALF_STRAW_CONSUMPTION;
        heiferStrawConsumption = FunctionalConstants::Population::DEFAULT_HEIFER_STRAW_CONSUMPTION;
        adultStrawConsumption = FunctionalConstants::Population::DEFAULT_COW_STRAW_CONSUMPTION;
#ifdef _SENSIBILITY_ANALYSIS
        SA_mastitisPercentPreventionProgress = FunctionalConstants::Health::PERCENT_PREVENTION_PROGRESS;
        SA_ketosisPercentPreventionProgress = FunctionalConstants::Health::PERCENT_PREVENTION_PROGRESS;           
        SA_additionalMastitisHealingIfCareVetContract = FunctionalConstants::Health::ADDITIONAL_MASTITIS_HEALING_PROBABILITY_IF_CARE_VET_CONTRACT;
        SA_additionalKetosisHealingIfCareVetContract = FunctionalConstants::Health::ADDITIONAL_KETOSIS_HEALING_PROBABILITY_IF_CARE_VET_CONTRACT;
        SA_nonManagedMortalityFactorWithVetCareContract = FunctionalConstants::Population::NON_MANAGED_MORTALITY_FACTOR_WITH_VET_CARE_CONTRACT;
#endif // _SENSIBILITY_ANALYSIS
        
    }
    DairyFarmParameters::DairyFarmParameters(std::string &dairyFarmName) : DairyFarmParameters()
    {
        name = dairyFarmName;
        comment = FunctionalConstants::Global::DEFAULT_FARM_COMMENT;
    }

    // Constructeur de copie   
    DairyFarmParameters::DairyFarmParameters (const DairyFarmParameters &obj) : SystemToSimulateParameters(obj)
    {
        // Reproduction decision
        vetReproductionContract = obj.vetReproductionContract;
        ageToBreedingDecision = obj.ageToBreedingDecision;
        detectionMode = obj.detectionMode;
        slipperyFloor = obj.slipperyFloor;
        matingPlan = obj.matingPlan;
        fertilityFactor = obj.fertilityFactor;       
        breedingDelayAfterCalvingDecision = obj.breedingDelayAfterCalvingDecision;
        numberOfDaysAfterHeiferBreedingDecisionForStopFirstInseminationDecision = obj.numberOfDaysAfterHeiferBreedingDecisionForStopFirstInseminationDecision;      
        numberOfDaysAfterHeiferBreedingDecisionForStopInseminationDecision = obj.numberOfDaysAfterHeiferBreedingDecisionForStopInseminationDecision;      
        numberOfDaysAfterAdultBreedingDecisionForStopFirstInseminationDecision = obj.numberOfDaysAfterAdultBreedingDecisionForStopFirstInseminationDecision;
        numberOfDaysAfterAdultBreedingDecisionForStopInseminationDecision = obj.numberOfDaysAfterAdultBreedingDecisionForStopInseminationDecision;      
        
        // Lactation decision
        driedPeriodDuration = obj.driedPeriodDuration;
        milkingFrequency = obj.milkingFrequency;
        dayBeforeDeliverMilk = obj.dayBeforeDeliverMilk;
        herdProductionDelta = obj.herdProductionDelta;
        
        // Health decision and settings
        G1MastitisDetectionSensibility = obj.G1MastitisDetectionSensibility;
        mastitisSaisonalityProbabilityFactor = obj.mastitisSaisonalityProbabilityFactor;
        bacteriumIncidencePart = obj.bacteriumIncidencePart;
        mastitisTreatmentPlan = obj.mastitisTreatmentPlan;
        individualMastitisIncidencePreventionFactor = obj.individualMastitisIncidencePreventionFactor;
        herdMastitisIncidencePreventionFactor = obj.herdMastitisIncidencePreventionFactor;
        vetCareContract = obj.vetCareContract;
        ketosisIncidencePreventionFactor = obj.ketosisIncidencePreventionFactor;
        monensinBolusUsage = obj.monensinBolusUsage;
        cetoDetectUsage = obj.cetoDetectUsage;
        herdNavigatorOption = obj.herdNavigatorOption;
        ketosisTreatmentPlan = obj.ketosisTreatmentPlan;
        lamenessIncidencePreventionFactor = obj.lamenessIncidencePreventionFactor;
        footBath = obj.footBath;
        footBathProtectionDurationForLameness = obj.footBathProtectionDurationForLameness;
        footBathFrequency = obj.footBathFrequency;
        preventiveTrimmingOption = obj.preventiveTrimmingOption;
        cowCountForSmallTrimmingOrCareGroup = obj.cowCountForSmallTrimmingOrCareGroup;
        lamenessDetectionMode = obj.lamenessDetectionMode;
        lamenessTreatmentPlan = obj.lamenessTreatmentPlan;
        
        // Genetic settings
        maleDataFile = obj.maleDataFile;
        geneticStrategy = obj.geneticStrategy;
        
        // Population decision
        initialBreed = obj.initialBreed;
        meanAdultNumberTarget = obj.meanAdultNumberTarget;
        initialAnnualHerdRenewablePart = obj.initialAnnualHerdRenewablePart;
        annualHerdRenewableDelta = obj.annualHerdRenewableDelta;
        femaleCalveManagement = obj.femaleCalveManagement;      
        maxLactationRankForEndInseminationDecision = obj.maxLactationRankForEndInseminationDecision;      
        minMilkProductionHerdRatioForEndInseminationDecision = obj.minMilkProductionHerdRatioForEndInseminationDecision;      
        maxSCCLevelForEndInseminationDecision = obj.maxSCCLevelForEndInseminationDecision;                 
        minimumMilkQuantityFactorForRentability = obj.minimumMilkQuantityFactorForRentability;
        youngHeiferBeginPastureDate = obj.youngHeiferBeginPastureDate;
        youngHeiferEndPastureDate = obj.youngHeiferEndPastureDate;
        otherHeiferAndDriedCowBeginPastureDate = obj.otherHeiferAndDriedCowBeginPastureDate;
        otherHeiferAndDriedCowEndPastureDate = obj.otherHeiferAndDriedCowEndPastureDate;
        lactatingCowBeginFullPastureDate = obj.lactatingCowBeginFullPastureDate;
        lactatingCowEndFullPastureDate = obj.lactatingCowEndFullPastureDate;
        lactatingCowBeginStabulationDate = obj.lactatingCowBeginStabulationDate;
        lactatingCowEndStabulationDate = obj.lactatingCowEndStabulationDate;
        calfStrawConsumption = obj.calfStrawConsumption;
        heiferStrawConsumption = obj.heiferStrawConsumption;
        adultStrawConsumption = obj.adultStrawConsumption;
#ifdef _SENSIBILITY_ANALYSIS
        SA_mastitisPercentPreventionProgress = obj.SA_mastitisPercentPreventionProgress;
        SA_ketosisPercentPreventionProgress = obj.SA_ketosisPercentPreventionProgress;    
        SA_additionalMastitisHealingIfCareVetContract = obj.SA_additionalMastitisHealingIfCareVetContract;
        SA_additionalKetosisHealingIfCareVetContract = obj.SA_additionalKetosisHealingIfCareVetContract;
        SA_nonManagedMortalityFactorWithVetCareContract = obj.SA_nonManagedMortalityFactorWithVetCareContract;
#endif // _SENSIBILITY_ANALYSIS

        // Feeding decision
        feedingPlan = obj.feedingPlan;
    }
    
    DairyFarmParameters::DairyFarmParameters(std::string &dairyFarmName, std::string &fileName, std::string &geneticCataloguePath) : DairyFarmParameters(dairyFarmName)
    {       
        // Loading data from file
        std::vector<std::vector<std::string>> vDairyFarmParameters;
        Tools::readFile(vDairyFarmParameters, fileName, TechnicalConstants::EXPORT_SEP);
        std::map<std::string, std::string> alreadyConsideredParam;
        for (std::vector<std::vector<std::string>>::iterator itLines = vDairyFarmParameters.begin(); itLines != vDairyFarmParameters.end(); itLines++)
        {
            std::string value0 = (*itLines).at(0);
            bool toConsider = true;
            // Empty line ?
            if (value0.size() == 0)
            {
                toConsider = false;;
            }
            else
            {
                // Comment line ?
                value0.resize(TechnicalConstants::COMMENT.size());
                toConsider = value0 != TechnicalConstants::COMMENT;
            }
            if (toConsider)
            {                
                if ((*itLines).size() == 2)
                {
                    std::string &key = (*itLines).at(0);
                    std::string &content = (*itLines).at(1);
                    
                    // check if already considered
                    auto it = alreadyConsideredParam.find(key.c_str());
                    if (it != alreadyConsideredParam.end())
                    {
                        // This parameter has already been considered
                        if (it->second == content)
                        {
                            // Two occurences of the same parameter
                            std::string warningMessage = "key = \"" + key + "\" with the value \""  + content + "\" has already been considered";
                            Console::Display::displayLineInConsole(warningMessage, Console::ANSI_YELLOW);
                        }
                        else
                        {
                            // Same key, different value !!
                            std::string warningMessage = "key = \"" + key + "\" with the value \""  + content + "\" has been considered before with the value \""  + it->second + "\", the new value is now considered";
                            Console::Display::displayLineInConsole(warningMessage, Console::ANSI_YELLOW);
                        }
                    }
                    
                    alreadyConsideredParam[key] = content; // Store this parameter
                    
                    if (setImportedValue(key, content)) continue;

                    // If we are here, the key haven't been found
                    std::string warningMessage = "key = \"" + key + "\" unknown, line not considered";
                    Console::Display::displayLineInConsole(warningMessage, Console::ANSI_YELLOW);
                }
                else
                {
                    // Error in the line
                   std::vector<std::string> line = (*itLines);
                   std::string originLine = "";
                   for (std::vector<std::string>::iterator it = line.begin(); it != line.end(); it++)
                   {
                       if (it!=line.begin()) originLine += TechnicalConstants::EXPORT_SEP;
                       originLine += *it;
                   }
                   Console::Display::displayLineInConsole("-> Error line in this file, line \"" + originLine + "\" not considered", Console::ANSI_YELLOW);
                }
            }
        }       
    }
    
    bool DairyFarmParameters::autoControl(std::string &message)
    {
        bool res = SystemToSimulateParameters::autoControl(message);
        if (res)
        {
            res = matingPlan.autoControl(message);
        }
        if (res)
        {
            res = mastitisTreatmentPlan.autoControl(message);
        }
        if (res)
        {
            res = ketosisTreatmentPlan.autoControl(message);
        }
        if (res)
        {
            res = lamenessTreatmentPlan.autoControl(message);
        }
        if (res)
        {
            res = reproductionTreatmentPlan.autoControl(message);
        }
        if (res)
        {
            res = feedingPlan.autoControl(message);
        }
        if (res and( meanAdultNumberTarget == 0))
        {
            res = false;
            message = LanguageDictionary::getMessage(KEY_THE_ANIMAL_NUMBER_IS_NOT_VALID);
        }
        
        if (res and (  initialAnnualHerdRenewablePart < FunctionalConstants::Population::MIN_ANNUAL_HERD_RENEWAL_PART 
                    or initialAnnualHerdRenewablePart > FunctionalConstants::Population::MAX_ANNUAL_HERD_RENEWAL_PART))
        {
            res = false;
            message = LanguageDictionary::getMessage(KEY_THE_RENEWAL_RATIO_IS_NOT_VALID);
        }

        if (res and (individualMastitisIncidencePreventionFactor < FunctionalConstants::Health::MIN_COW_MASTITIS_INCIDENCE_PREVENTION_FACTOR or individualMastitisIncidencePreventionFactor > FunctionalConstants::Health::MAX_COW_MASTITIS_INCIDENCE_PREVENTION_FACTOR))
        {
            res = false;
            message = LanguageDictionary::getMessage(KEY_THE_COW_MASTITIS_INCIDENCE_PREVENTION_FACTOR_IS_NOT_VALID);
        }
        if (res and (herdMastitisIncidencePreventionFactor < FunctionalConstants::Health::MIN_HERD_MASTITIS_INCIDENCE_PREVENTION_FACTOR or herdMastitisIncidencePreventionFactor > FunctionalConstants::Health::MAX_HERD_MASTITIS_INCIDENCE_PREVENTION_FACTOR))
        {
            res = false;
            message = LanguageDictionary::getMessage(KEY_THE_HERD_MASTITIS_INCIDENCE_PREVENTION_FACTOR_IS_NOT_VALID);
        }
        if (res and (ketosisIncidencePreventionFactor < FunctionalConstants::Health::MIN_KETOSIS_INCIDENCE_PREVENTION_FACTOR or ketosisIncidencePreventionFactor > FunctionalConstants::Health::MAX_KETOSIS_INCIDENCE_PREVENTION_FACTOR))
        {
            res = false;
            message = LanguageDictionary::getMessage(KEY_THE_KETOSIS_INCIDENCE_PREVENTION_FACTOR_IS_NOT_VALID);
        }
        if (res and (lamenessIncidencePreventionFactor.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second < FunctionalConstants::Health::MIN_LAMENESS_INCIDENCE_PREVENTION_FACTOR.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second or lamenessIncidencePreventionFactor.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second > FunctionalConstants::Health::MAX_LAMENESS_INCIDENCE_PREVENTION_FACTOR.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second))
        {
            res = false;
            message = LanguageDictionary::getMessage(KEY_THE_NI_LAMENESS_INCIDENCE_PREVENTION_FACTOR_IS_NOT_VALID);
        }
        if (res and (lamenessIncidencePreventionFactor.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second < FunctionalConstants::Health::MIN_LAMENESS_INCIDENCE_PREVENTION_FACTOR.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second or lamenessIncidencePreventionFactor.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second > FunctionalConstants::Health::MAX_LAMENESS_INCIDENCE_PREVENTION_FACTOR.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second))
        {
            res = false;
            message = LanguageDictionary::getMessage(KEY_THE_I_LAMENESS_INCIDENCE_PREVENTION_FACTOR_IS_NOT_VALID);
        }
        
        return res;
    } /* End of method AutoControl */    
    
    
    
    bool DairyFarmParameters::setImportedValue(const std::string &key, std::string &content)
    {
        if (key == COMMENT_PARAM_KEY) {comment = content; return true;}
        if (key == AGE_TO_BREEDING_DECISION_PARAM_KEY) {ageToBreedingDecision = std::atoi(content.c_str()); return true;}
        if (key == DETECTION_MODE_PARAM_KEY) {detectionMode = (FunctionalEnumerations::Reproduction::DetectionMode)std::atoi(content.c_str()); return true;}
        if (key == SLIPPERY_FLOOR_PARAM_KEY) {slipperyFloor = (bool)std::atoi(content.c_str()); return true;}
        if (key == FERTILITY_FACTOR_PARAM_KEY) {fertilityFactor = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == BREEDING_DELAY_AFTER_CALVING_DECISION_PARAM_KEY) {breedingDelayAfterCalvingDecision = std::atoi(content.c_str()); return true;}
        if (key == NUMBER_OF_DAYS_AFTER_HEIFER_BREEDING_DECISION_FOR_STOP_FIRST_INSEMINATION_DECISION_PARAM_KEY) {numberOfDaysAfterHeiferBreedingDecisionForStopFirstInseminationDecision = std::atoi(content.c_str()); return true;}
        if (key == NUMBER_OF_DAYS_AFTER_HEIFER_BREEDING_DECISION_FOR_STOP_INSEMINATION_DECISION_PARAM_KEY) {numberOfDaysAfterHeiferBreedingDecisionForStopInseminationDecision = std::atoi(content.c_str()); return true;}
        if (key == NUMBER_OF_DAYS_AFTER_ADULT_BREEDING_DECISION_FOR_STOP_FIRST_INSEMINATION_DECISION_PARAM_KEY) {numberOfDaysAfterAdultBreedingDecisionForStopFirstInseminationDecision = std::atoi(content.c_str()); return true;}
        if (key == NUMBER_OF_DAYS_AFTER_ADULT_BREEDING_DECISION_FOR_STOP_INSEMINATION_DECISION_PARAM_KEY) {numberOfDaysAfterAdultBreedingDecisionForStopInseminationDecision = std::atoi(content.c_str()); return true;}
        if (key == VET_REPRODUCTION_CONTRACT_PARAM_KEY) {vetReproductionContract = (bool)std::atoi(content.c_str()); return true;}
        if (key == DRIED_PERIOD_DURATION_PARAM_KEY) {driedPeriodDuration = std::atoi(content.c_str()); return true;}
        if (key == MILKING_FREQUENCY_PARAM_KEY) {milkingFrequency = (FunctionalEnumerations::Lactation::MilkingFrequency)std::atoi(content.c_str()); return true;}
        if (key == HERD_PRODUCTION_DELTA_LAIT_PARAM_KEY) {herdProductionDelta.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == DAY_BEFORE_DELIVER_MILK_PARAM_KEY) {dayBeforeDeliverMilk = std::atoi(content.c_str()); return true;}
        if (key == HERD_PRODUCTION_DELTA_LAIT_PARAM_KEY) {herdProductionDelta.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == HERD_PRODUCTION_DELTA_TB_PARAM_KEY) {herdProductionDelta.TB = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == HERD_PRODUCTION_DELTA_TP_PARAM_KEY) {herdProductionDelta.TP = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == G1_MASTITIS_DETECTION_SENSIBILITY_PARAM_KEY) {G1MastitisDetectionSensibility = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_JAN_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::jan)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_FEB_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::feb)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_MAR_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::mar)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_APR_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::apr)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_MAY_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::may)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_JUN_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::jun)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_JUL_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::jul)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_AUG_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::aug)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_SEP_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::sep)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_OCT_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::oct)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_NOV_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::nov)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_DEC_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::dec)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_JAN_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::jan)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_FEB_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::feb)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_MAR_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::mar)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_APR_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::apr)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_MAY_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::may)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_JUN_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::jun)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_JUL_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::jul)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_AUG_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::aug)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_SEP_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::sep)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_OCT_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::oct)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_NOV_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::nov)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_DEC_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::dec)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_JAN_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::jan)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_FEB_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::feb)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_MAR_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::mar)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_APR_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::apr)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_MAY_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::may)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_JUN_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::jun)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_JUL_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::jul)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_AUG_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::aug)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_SEP_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::sep)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_OCT_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::oct)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_NOV_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::nov)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_DEC_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::dec)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_JAN_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::jan)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_FEB_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::feb)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_MAR_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::mar)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_APR_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::apr)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_MAY_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::may)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_JUN_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::jun)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_JUL_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::jul)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_AUG_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::aug)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_SEP_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::sep)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_OCT_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::oct)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_NOV_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::nov)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_DEC_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::dec)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_JAN_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::jan)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_FEB_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::feb)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_MAR_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::mar)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_APR_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::apr)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_MAY_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::may)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_JUN_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::jun)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_JUL_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::jul)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_AUG_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::aug)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_SEP_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::sep)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_OCT_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::oct)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_NOV_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::nov)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_DEC_PARAM_KEY) {mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::dec)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_BACTERIUM_INCIDENCE_PART_STAPHA_PARAM_KEY) {bacteriumIncidencePart.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_BACTERIUM_INCIDENCE_PART_STREPTU_PARAM_KEY) {bacteriumIncidencePart.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_BACTERIUM_INCIDENCE_PART_GN_PARAM_KEY) {bacteriumIncidencePart.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_BACTERIUM_INCIDENCE_PART_CNS_PARAM_KEY) {bacteriumIncidencePart.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MASTITIS_BACTERIUM_INCIDENCE_PART_CB_PARAM_KEY) {bacteriumIncidencePart.find(FunctionalEnumerations::Health::BacteriumType::CB)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == INDIVIDUAL_MASTITIS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY) {individualMastitisIncidencePreventionFactor = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == HERD_MASTITIS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY) {herdMastitisIncidencePreventionFactor = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == VET_CARE_CONTRACT_PARAM_KEY) {vetCareContract = (FunctionalEnumerations::Health::VeterinaryCareContractType)std::atoi(content.c_str()); return true;}
        if (key == KETOSIS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY) {ketosisIncidencePreventionFactor = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MONENSIN_BOLUS_USAGE_PARAM_KEY) {monensinBolusUsage = (bool)std::atoi(content.c_str()); return true;}
        if (key == CETODETECT_USAGE_PARAM_KEY) {cetoDetectUsage = (bool)std::atoi(content.c_str()); return true;}
        if (key == HERD_NAVIGATOR_OPTION_PARAM_KEY) {herdNavigatorOption = (bool)std::atoi(content.c_str()); return true;}                   
        if (key == LAMENESS_NON_INFECTIOUS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY) {lamenessIncidencePreventionFactor[FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType] = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == LAMENESS_INFECTIOUS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY) {lamenessIncidencePreventionFactor[FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType] = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == FOOT_BATH_OPTION_PARAM_KEY) {footBath = (bool)std::atoi(content.c_str()); return true;}
        if (key == FOOT_BATH_EFFECT_DURATION_PARAM_KEY) {footBathProtectionDurationForLameness = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == FOOT_BATH_STABULATION_FREQUENCY_PARAM_KEY) {footBathFrequency[FunctionalEnumerations::Population::Location::stabulation] = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == FOOT_BATH_FULL_PASTURE_FREQUENCY_PARAM_KEY) {footBathFrequency[FunctionalEnumerations::Population::Location::fullPasture] = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == FOOT_BATH_HALF_STABULATION_PASTURE_FREQUENCY_PARAM_KEY) {footBathFrequency[FunctionalEnumerations::Population::Location::halfStabulationPasture] = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == PREVENTIVE_TRIMMING_OPTION_PARAM_KEY) {preventiveTrimmingOption = (FunctionalEnumerations::Health::LamenessPreventiveTrimmingOption)std::atoi(content.c_str()); return true;}
        if (key == LAMENESS_DETECTION_MODE_PARAM_KEY) {lamenessDetectionMode = (FunctionalEnumerations::Health::LamenessDetectionMode)std::atoi(content.c_str()); return true;}
        if (key == COW_COUNT_FOR_SMALL_TRIMMING_OR_CARE_GROUP_PARAM_KEY) {cowCountForSmallTrimmingOrCareGroup = std::atoi(content.c_str()); return true;}
        //if (key == MALE_DATA_FILE_PARAM_KEY) {maleDataFile = geneticCataloguePath + content; return true;}
        if (key == GENETIC_STRATEGY_PARAM_KEY) {geneticStrategy = (FunctionalEnumerations::Genetic::GeneticStrategy)std::atoi(content.c_str()); return true;}
        if (key == INITIAL_BREED_PARAM_KEY) {initialBreed = (FunctionalEnumerations::Genetic::Breed)std::atoi(content.c_str()); return true;}
        if (key == MEAN_ADULT_NUMBER_TARGET_PARAM_KEY) {meanAdultNumberTarget = std::atoi(content.c_str()); return true;}
        if (key == ANNUAL_HERD_RENEWABLE_PART_PARAM_KEY) {initialAnnualHerdRenewablePart = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == ANNUAL_HERD_RENEWABLE_DELTA_PARAM_KEY) {annualHerdRenewableDelta = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == FEMALE_CALVE_MANAGEMENT_PARAM_KEY) {femaleCalveManagement = (FunctionalEnumerations::Population::FemaleCalveManagement)std::atoi(content.c_str()); return true;}
        if (key == MAX_LACTATION_RANK_FOR_END_INSEMINATION_DECISION_PARAM_KEY) {maxLactationRankForEndInseminationDecision = std::atoi(content.c_str()); return true;}
        if (key == MIN_MILK_PRODUCTION_HERD_RATIO_FOR_END_INSEMINATION_DECISION_PARAM_KEY) {minMilkProductionHerdRatioForEndInseminationDecision = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MAX_SCC_LEVEL_FOR_END_INSEMINATION_DECISION_PARAM_KEY) {maxSCCLevelForEndInseminationDecision = std::atoi(content.c_str()); return true;}
        if (key == MINIMUM_MILK_QUANTITY_FACTOR_FOR_RENTABILITY_PARAM_KEY) {minimumMilkQuantityFactorForRentability = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == YOUNG_HEIFER_BEGIN_PASTURE_DATE_DAY_PARAM_KEY) {youngHeiferBeginPastureDate.first = std::atoi(content.c_str()); return true;}
        if (key == YOUNG_HEIFER_BEGIN_PASTURE_DATE_MONTH_PARAM_KEY) {youngHeiferBeginPastureDate.second = (FunctionalEnumerations::Global::Month)std::atoi(content.c_str()); return true;}
        if (key == YOUNG_HEIFER_END_PASTURE_DATE_DAY_PARAM_KEY) {youngHeiferEndPastureDate.first = std::atoi(content.c_str()); return true;}
        if (key == YOUNG_HEIFER_END_PASTURE_DATE_MONTH_PARAM_KEY) {youngHeiferEndPastureDate.second = (FunctionalEnumerations::Global::Month)std::atoi(content.c_str()); return true;}
        if (key == OTHER_HEIFER_AND_DRIED_COW_BEGIN_PASTURE_DATE_DAY_PARAM_KEY) {otherHeiferAndDriedCowBeginPastureDate.first = std::atoi(content.c_str()); return true;}
        if (key == OTHER_HEIFER_AND_DRIED_COW_BEGIN_PASTURE_DATE_MONTH_PARAM_KEY) {otherHeiferAndDriedCowBeginPastureDate.second = (FunctionalEnumerations::Global::Month)std::atoi(content.c_str()); return true;}
        if (key == OTHER_HEIFER_AND_DRIED_COW_END_PASTURE_DATE_DAY_PARAM_KEY) {otherHeiferAndDriedCowEndPastureDate.first = std::atoi(content.c_str()); return true;}
        if (key == OTHER_HEIFER_AND_DRIED_COW_END_PASTURE_DATE_MONTH_PARAM_KEY) {otherHeiferAndDriedCowEndPastureDate.second = (FunctionalEnumerations::Global::Month)std::atoi(content.c_str()); return true;}
        if (key == LACTATING_COW_BEGIN_FULL_PASTURE_DATE_DAY_PARAM_KEY) {lactatingCowBeginFullPastureDate.first = std::atoi(content.c_str()); return true;}
        if (key == LACTATING_COW_BEGIN_FULL_PASTURE_DATE_MONTH_PARAM_KEY) {lactatingCowBeginFullPastureDate.second = (FunctionalEnumerations::Global::Month)std::atoi(content.c_str()); return true;}
        if (key == LACTATING_COW_END_FULL_PASTURE_DATE_DAY_PARAM_KEY) {lactatingCowEndFullPastureDate.first = std::atoi(content.c_str()); return true;}
        if (key == LACTATING_COW_END_FULL_PASTURE_DATE_MONTH_PARAM_KEY) {lactatingCowEndFullPastureDate.second = (FunctionalEnumerations::Global::Month)std::atoi(content.c_str()); return true;}
        if (key == LACTATING_COW_BEGIN_STABULATION_DATE_DAY_PARAM_KEY) {lactatingCowBeginStabulationDate.first = std::atoi(content.c_str()); return true;}
        if (key == LACTATING_COW_BEGIN_STABULATION_DATE_MONTH_PARAM_KEY) {lactatingCowBeginStabulationDate.second = (FunctionalEnumerations::Global::Month)std::atoi(content.c_str()); return true;}
        if (key == LACTATING_COW_END_STABULATION_DATE_DAY_PARAM_KEY) {lactatingCowEndStabulationDate.first = std::atoi(content.c_str()); return true;}
        if (key == LACTATING_COW_END_STABULATION_DATE_MONTH_PARAM_KEY) {lactatingCowEndStabulationDate.second = (FunctionalEnumerations::Global::Month)std::atoi(content.c_str()); return true;}
        if (key == CALF_STRAW_CONSUMPTION_PARAM_KEY) {calfStrawConsumption = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == HEIFER_STRAW_CONSUMPTION_PARAM_KEY) {heiferStrawConsumption = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == ADULT_STRAW_CONSUMPTION_PARAM_KEY) {adultStrawConsumption = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        
        // Other sub-modules ?
        if (matingPlan.setImportedValue(key, content)) return true; // May be mating plan
        if (mastitisTreatmentPlan.setImportedValue(key, content)) return true; // May be mastitis treatment plan
        if (ketosisTreatmentPlan.setImportedValue(key, content)) return true; // May be ketosis treatment plan
        if (lamenessTreatmentPlan.setImportedValue(key, content)) return true; // May be lameness treatment plan
        if (reproductionTreatmentPlan.setImportedValue(key, content)) return true; // May be reproduction treatment plan
        if (feedingPlan.setImportedValue(key, content)) return true; // May be feeding plan
        
#ifdef _SENSIBILITY_ANALYSIS
        if (key == MASTITIS_PERCENT_PREVENTION_PROGRESS_SA_KEY) {SA_mastitisPercentPreventionProgress = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == KETOSIS_PERCENT_PREVENTION_PROGRESS_SA_KEY) {SA_ketosisPercentPreventionProgress = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}    
        if (key == ADDITIONAL_MASTITIS_HEALING_PROBABILITY_IF_CARE_VET_CONTRACT_SA_KEY) {SA_additionalMastitisHealingIfCareVetContract = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == ADDITIONAL_KETOSIS_HEALING_PROBABILITY_IF_CARE_VET_CONTRACT_SA_KEY) {SA_additionalKetosisHealingIfCareVetContract = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == NON_MANAGED_MORTALITY_FACTOR_WITH_VET_CARE_CONTRACT_SA_KEY) {SA_nonManagedMortalityFactorWithVetCareContract = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
 #endif // _SENSIBILITY_ANALYSIS


        return false; // Not here
    }

    bool DairyFarmParameters::getValueToExport(const std::string &key, std::string &content)
    {
        GET_STRING_VALUE_TO_IMPORT(COMMENT_PARAM_KEY, comment)
        GET_INTEGER_VALUE_TO_IMPORT(AGE_TO_BREEDING_DECISION_PARAM_KEY, ageToBreedingDecision)
        GET_INTEGER_VALUE_TO_IMPORT(DETECTION_MODE_PARAM_KEY, detectionMode)
        GET_INTEGER_VALUE_TO_IMPORT(SLIPPERY_FLOOR_PARAM_KEY, slipperyFloor)
        GET_FLOAT_VALUE_TO_IMPORT(FERTILITY_FACTOR_PARAM_KEY, fertilityFactor)
        GET_INTEGER_VALUE_TO_IMPORT(BREEDING_DELAY_AFTER_CALVING_DECISION_PARAM_KEY, breedingDelayAfterCalvingDecision)
        GET_INTEGER_VALUE_TO_IMPORT(NUMBER_OF_DAYS_AFTER_HEIFER_BREEDING_DECISION_FOR_STOP_FIRST_INSEMINATION_DECISION_PARAM_KEY, numberOfDaysAfterHeiferBreedingDecisionForStopFirstInseminationDecision)
        GET_INTEGER_VALUE_TO_IMPORT(NUMBER_OF_DAYS_AFTER_HEIFER_BREEDING_DECISION_FOR_STOP_INSEMINATION_DECISION_PARAM_KEY, numberOfDaysAfterHeiferBreedingDecisionForStopInseminationDecision)
        GET_INTEGER_VALUE_TO_IMPORT(NUMBER_OF_DAYS_AFTER_ADULT_BREEDING_DECISION_FOR_STOP_FIRST_INSEMINATION_DECISION_PARAM_KEY, numberOfDaysAfterAdultBreedingDecisionForStopFirstInseminationDecision)
        GET_INTEGER_VALUE_TO_IMPORT(NUMBER_OF_DAYS_AFTER_ADULT_BREEDING_DECISION_FOR_STOP_INSEMINATION_DECISION_PARAM_KEY, numberOfDaysAfterAdultBreedingDecisionForStopInseminationDecision)
        GET_INTEGER_VALUE_TO_IMPORT(VET_REPRODUCTION_CONTRACT_PARAM_KEY, vetReproductionContract)
        GET_INTEGER_VALUE_TO_IMPORT(DRIED_PERIOD_DURATION_PARAM_KEY, driedPeriodDuration)
        GET_INTEGER_VALUE_TO_IMPORT(MILKING_FREQUENCY_PARAM_KEY, milkingFrequency)
        GET_INTEGER_VALUE_TO_IMPORT(DAY_BEFORE_DELIVER_MILK_PARAM_KEY, dayBeforeDeliverMilk)
        GET_FLOAT_VALUE_TO_IMPORT(HERD_PRODUCTION_DELTA_LAIT_PARAM_KEY, herdProductionDelta.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(HERD_PRODUCTION_DELTA_TB_PARAM_KEY, herdProductionDelta.TB)
        GET_FLOAT_VALUE_TO_IMPORT(HERD_PRODUCTION_DELTA_TP_PARAM_KEY, herdProductionDelta.TP)
        GET_FLOAT_VALUE_TO_IMPORT(G1_MASTITIS_DETECTION_SENSIBILITY_PARAM_KEY, G1MastitisDetectionSensibility)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_JAN_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::jan)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_FEB_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::feb)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_MAR_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::mar)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_APR_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::apr)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_MAY_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::may)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_JUN_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::jun)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_JUL_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::jul)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_AUG_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::aug)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_SEP_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::sep)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_OCT_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::oct)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_NOV_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::nov)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_DEC_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::dec)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_JAN_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::jan)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_FEB_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::feb)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_MAR_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::mar)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_APR_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::apr)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_MAY_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::may)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_JUN_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::jun)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_JUL_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::jul)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_AUG_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::aug)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_SEP_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::sep)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_OCT_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::oct)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_NOV_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::nov)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_DEC_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::dec)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_JAN_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::jan)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_FEB_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::feb)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_MAR_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::mar)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_APR_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::apr)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_MAY_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::may)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_JUN_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::jun)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_JUL_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::jul)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_AUG_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::aug)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_SEP_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::sep)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_OCT_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::oct)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_NOV_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::nov)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_DEC_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::dec)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_JAN_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::jan)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_FEB_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::feb)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_MAR_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::mar)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_APR_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::apr)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_MAY_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::may)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_JUN_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::jun)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_JUL_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::jul)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_AUG_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::aug)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_SEP_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::sep)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_OCT_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::oct)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_NOV_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::nov)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_DEC_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::dec)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_JAN_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::jan)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_FEB_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::feb)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_MAR_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::mar)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_APR_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::apr)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_MAY_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::may)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_JUN_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::jun)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_JUL_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::jul)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_AUG_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::aug)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_SEP_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::sep)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_OCT_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::oct)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_NOV_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::nov)->second )
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_DEC_PARAM_KEY, mastitisSaisonalityProbabilityFactor.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::dec)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_BACTERIUM_INCIDENCE_PART_STAPHA_PARAM_KEY, bacteriumIncidencePart.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_BACTERIUM_INCIDENCE_PART_STREPTU_PARAM_KEY, bacteriumIncidencePart.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_BACTERIUM_INCIDENCE_PART_GN_PARAM_KEY, bacteriumIncidencePart.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_BACTERIUM_INCIDENCE_PART_CNS_PARAM_KEY, bacteriumIncidencePart.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_BACTERIUM_INCIDENCE_PART_CB_PARAM_KEY, bacteriumIncidencePart.find(FunctionalEnumerations::Health::BacteriumType::CB)->second)
        GET_FLOAT_VALUE_TO_IMPORT(INDIVIDUAL_MASTITIS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY, individualMastitisIncidencePreventionFactor)
        GET_FLOAT_VALUE_TO_IMPORT(HERD_MASTITIS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY, herdMastitisIncidencePreventionFactor)
        GET_INTEGER_VALUE_TO_IMPORT(VET_CARE_CONTRACT_PARAM_KEY, vetCareContract)
        GET_FLOAT_VALUE_TO_IMPORT(KETOSIS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY, ketosisIncidencePreventionFactor)
        GET_INTEGER_VALUE_TO_IMPORT(MONENSIN_BOLUS_USAGE_PARAM_KEY, monensinBolusUsage)
        GET_INTEGER_VALUE_TO_IMPORT(CETODETECT_USAGE_PARAM_KEY, cetoDetectUsage)
        GET_INTEGER_VALUE_TO_IMPORT(HERD_NAVIGATOR_OPTION_PARAM_KEY, herdNavigatorOption)                   
        GET_FLOAT_VALUE_TO_IMPORT(LAMENESS_NON_INFECTIOUS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY, lamenessIncidencePreventionFactor[FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType])
        GET_FLOAT_VALUE_TO_IMPORT(LAMENESS_INFECTIOUS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY, lamenessIncidencePreventionFactor[FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType])
        GET_INTEGER_VALUE_TO_IMPORT(FOOT_BATH_OPTION_PARAM_KEY, footBath)
        GET_INTEGER_VALUE_TO_IMPORT(FOOT_BATH_EFFECT_DURATION_PARAM_KEY, footBathProtectionDurationForLameness)
        GET_INTEGER_VALUE_TO_IMPORT(FOOT_BATH_STABULATION_FREQUENCY_PARAM_KEY, footBathFrequency[FunctionalEnumerations::Population::Location::stabulation])
        GET_INTEGER_VALUE_TO_IMPORT(FOOT_BATH_FULL_PASTURE_FREQUENCY_PARAM_KEY, footBathFrequency[FunctionalEnumerations::Population::Location::fullPasture])
        GET_INTEGER_VALUE_TO_IMPORT(FOOT_BATH_HALF_STABULATION_PASTURE_FREQUENCY_PARAM_KEY, footBathFrequency[FunctionalEnumerations::Population::Location::halfStabulationPasture])
        GET_INTEGER_VALUE_TO_IMPORT(PREVENTIVE_TRIMMING_OPTION_PARAM_KEY, preventiveTrimmingOption)
        GET_INTEGER_VALUE_TO_IMPORT(LAMENESS_DETECTION_MODE_PARAM_KEY, lamenessDetectionMode)
        GET_INTEGER_VALUE_TO_IMPORT(COW_COUNT_FOR_SMALL_TRIMMING_OR_CARE_GROUP_PARAM_KEY, cowCountForSmallTrimmingOrCareGroup)
        GET_INTEGER_VALUE_TO_IMPORT(GENETIC_STRATEGY_PARAM_KEY, geneticStrategy)
        GET_INTEGER_VALUE_TO_IMPORT(INITIAL_BREED_PARAM_KEY, initialBreed)
        GET_INTEGER_VALUE_TO_IMPORT(MEAN_ADULT_NUMBER_TARGET_PARAM_KEY, meanAdultNumberTarget)
        GET_FLOAT_VALUE_TO_IMPORT(ANNUAL_HERD_RENEWABLE_PART_PARAM_KEY, initialAnnualHerdRenewablePart)
        GET_FLOAT_VALUE_TO_IMPORT(ANNUAL_HERD_RENEWABLE_DELTA_PARAM_KEY, annualHerdRenewableDelta)
        GET_INTEGER_VALUE_TO_IMPORT(FEMALE_CALVE_MANAGEMENT_PARAM_KEY, femaleCalveManagement)
        GET_INTEGER_VALUE_TO_IMPORT(MAX_LACTATION_RANK_FOR_END_INSEMINATION_DECISION_PARAM_KEY, maxLactationRankForEndInseminationDecision)
        GET_INTEGER_VALUE_TO_IMPORT(MIN_MILK_PRODUCTION_HERD_RATIO_FOR_END_INSEMINATION_DECISION_PARAM_KEY, minMilkProductionHerdRatioForEndInseminationDecision)
        GET_FLOAT_VALUE_TO_IMPORT(MINIMUM_MILK_QUANTITY_FACTOR_FOR_RENTABILITY_PARAM_KEY, minimumMilkQuantityFactorForRentability)
        GET_INTEGER_VALUE_TO_IMPORT(MAX_SCC_LEVEL_FOR_END_INSEMINATION_DECISION_PARAM_KEY, maxSCCLevelForEndInseminationDecision)
        GET_INTEGER_VALUE_TO_IMPORT(YOUNG_HEIFER_BEGIN_PASTURE_DATE_DAY_PARAM_KEY, youngHeiferBeginPastureDate.first)
        GET_INTEGER_VALUE_TO_IMPORT(YOUNG_HEIFER_BEGIN_PASTURE_DATE_MONTH_PARAM_KEY, youngHeiferBeginPastureDate.second)
        GET_INTEGER_VALUE_TO_IMPORT(YOUNG_HEIFER_END_PASTURE_DATE_DAY_PARAM_KEY, youngHeiferEndPastureDate.first)
        GET_INTEGER_VALUE_TO_IMPORT(YOUNG_HEIFER_END_PASTURE_DATE_MONTH_PARAM_KEY, youngHeiferEndPastureDate.second)
        GET_INTEGER_VALUE_TO_IMPORT(OTHER_HEIFER_AND_DRIED_COW_BEGIN_PASTURE_DATE_DAY_PARAM_KEY, otherHeiferAndDriedCowBeginPastureDate.first)
        GET_INTEGER_VALUE_TO_IMPORT(OTHER_HEIFER_AND_DRIED_COW_BEGIN_PASTURE_DATE_MONTH_PARAM_KEY, otherHeiferAndDriedCowBeginPastureDate.second)
        GET_INTEGER_VALUE_TO_IMPORT(OTHER_HEIFER_AND_DRIED_COW_END_PASTURE_DATE_DAY_PARAM_KEY, otherHeiferAndDriedCowEndPastureDate.first)
        GET_INTEGER_VALUE_TO_IMPORT(OTHER_HEIFER_AND_DRIED_COW_END_PASTURE_DATE_MONTH_PARAM_KEY, otherHeiferAndDriedCowEndPastureDate.second)
        GET_INTEGER_VALUE_TO_IMPORT(LACTATING_COW_BEGIN_FULL_PASTURE_DATE_DAY_PARAM_KEY, lactatingCowBeginFullPastureDate.first)
        GET_INTEGER_VALUE_TO_IMPORT(LACTATING_COW_BEGIN_FULL_PASTURE_DATE_MONTH_PARAM_KEY, lactatingCowBeginFullPastureDate.second)
        GET_INTEGER_VALUE_TO_IMPORT(LACTATING_COW_END_FULL_PASTURE_DATE_DAY_PARAM_KEY, lactatingCowEndFullPastureDate.first)
        GET_INTEGER_VALUE_TO_IMPORT(LACTATING_COW_END_FULL_PASTURE_DATE_MONTH_PARAM_KEY, lactatingCowEndFullPastureDate.second)
        GET_INTEGER_VALUE_TO_IMPORT(LACTATING_COW_BEGIN_STABULATION_DATE_DAY_PARAM_KEY, lactatingCowBeginStabulationDate.first)
        GET_INTEGER_VALUE_TO_IMPORT(LACTATING_COW_BEGIN_STABULATION_DATE_MONTH_PARAM_KEY, lactatingCowBeginStabulationDate.second)
        GET_INTEGER_VALUE_TO_IMPORT(LACTATING_COW_END_STABULATION_DATE_DAY_PARAM_KEY, lactatingCowEndStabulationDate.first)
        GET_INTEGER_VALUE_TO_IMPORT(LACTATING_COW_END_STABULATION_DATE_MONTH_PARAM_KEY, lactatingCowEndStabulationDate.second)
        GET_FLOAT_VALUE_TO_IMPORT(CALF_STRAW_CONSUMPTION_PARAM_KEY, calfStrawConsumption)
        GET_FLOAT_VALUE_TO_IMPORT(HEIFER_STRAW_CONSUMPTION_PARAM_KEY, heiferStrawConsumption)
        GET_FLOAT_VALUE_TO_IMPORT(ADULT_STRAW_CONSUMPTION_PARAM_KEY, adultStrawConsumption)
        
        // Other sub-modules ?
        if (matingPlan.getValueToExport(key, content)) return true; // May be mating plan
        if (mastitisTreatmentPlan.getValueToExport(key, content)) return true; // May be mastitis treatment plan
        if (ketosisTreatmentPlan.getValueToExport(key, content)) return true; // May be ketosis treatment plan
        if (lamenessTreatmentPlan.getValueToExport(key, content)) return true; // May be lameness treatment plan
        if (reproductionTreatmentPlan.getValueToExport(key, content)) return true; // May be reproduction treatment plan
        if (feedingPlan.getValueToExport(key, content)) return true; // May be feeding plan

#ifdef _SENSIBILITY_ANALYSIS
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_PERCENT_PREVENTION_PROGRESS_SA_KEY, SA_mastitisPercentPreventionProgress)
        GET_FLOAT_VALUE_TO_IMPORT(KETOSIS_PERCENT_PREVENTION_PROGRESS_SA_KEY, SA_ketosisPercentPreventionProgress)      
        GET_FLOAT_VALUE_TO_IMPORT(ADDITIONAL_MASTITIS_HEALING_PROBABILITY_IF_CARE_VET_CONTRACT_SA_KEY, SA_additionalMastitisHealingIfCareVetContract)
        GET_FLOAT_VALUE_TO_IMPORT(ADDITIONAL_KETOSIS_HEALING_PROBABILITY_IF_CARE_VET_CONTRACT_SA_KEY, SA_additionalKetosisHealingIfCareVetContract)
        GET_FLOAT_VALUE_TO_IMPORT(NON_MANAGED_MORTALITY_FACTOR_WITH_VET_CARE_CONTRACT_SA_KEY, SA_nonManagedMortalityFactorWithVetCareContract)
#endif // _SENSIBILITY_ANALYSIS
                
        return false; // Not here
    }
    
#ifdef _LOG
    void DairyFarmParameters::saveDefaultDairyFarmValues(const std::string &path)
    {
        std::vector<std::string> vLines;
        std::string line = "";
        line = COMMENT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + FunctionalConstants::Global::DEFAULT_FARM_COMMENT; vLines.push_back(line);
        
        line = ""; vLines.push_back(line);
        line = TechnicalConstants::COMMENT + "Reproduction decisions (2.2.1.2) :"; vLines.push_back(line);
        line = AGE_TO_BREEDING_DECISION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_AGE_TO_BREEDING_DECISION_VALUE); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = DETECTION_MODE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_DETECTION_MODE_VALUE); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = SLIPPERY_FLOOR_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_SLIPPERY_FLOOR_VALUE); vLines.push_back(Tools::changeChar(line,'.', ','));
        MatingPlanParameters::getDefaultValues(vLines); // Mating plan
        line = FERTILITY_FACTOR_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_FERTILITY_FACTOR_VALUE); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BREEDING_DELAY_AFTER_CALVING_DECISION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_BREEDING_DELAY_AFTER_CALVING_DECISION_VALUE); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = NUMBER_OF_DAYS_AFTER_HEIFER_BREEDING_DECISION_FOR_STOP_FIRST_INSEMINATION_DECISION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_NUMBER_OF_DAYS_AFTER_HEIFER_BREEDING_DECISION_FOR_STOP_FIRST_INSEMINATION_DECISION_VALUE); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = NUMBER_OF_DAYS_AFTER_HEIFER_BREEDING_DECISION_FOR_STOP_INSEMINATION_DECISION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_NUMBER_OF_DAYS_AFTER_HEIFER_BREEDING_DECISION_FOR_STOP_INSEMINATION_DECISION_VALUE); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = NUMBER_OF_DAYS_AFTER_ADULT_BREEDING_DECISION_FOR_STOP_FIRST_INSEMINATION_DECISION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_NUMBER_OF_DAYS_AFTER_ADULT_BREEDING_DECISION_FOR_STOP_FIRST_INSEMINATION_DECISION_VALUE); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = NUMBER_OF_DAYS_AFTER_ADULT_BREEDING_DECISION_FOR_STOP_INSEMINATION_DECISION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_NUMBER_OF_DAYS_AFTER_ADULT_BREEDING_DECISION_FOR_STOP_INSEMINATION_DECISION_VALUE); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = VET_REPRODUCTION_CONTRACT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_VET_REPRODUCTION_CONTRACT_OPTION); vLines.push_back(Tools::changeChar(line,'.', ','));
        ReproductionTreatmentPlanParameters::getDefaultValues(vLines); // Reproduction treatment plan

        line = ""; vLines.push_back(line);
        line = TechnicalConstants::COMMENT + "Lactation decisions (2.2.2.2) :"; vLines.push_back(line);
        line = DRIED_PERIOD_DURATION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Lactation::DEFAULT_DRIED_PERIOD_DURATION_VALUE); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MILKING_FREQUENCY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Lactation::DEFAULT_MILKING_FREQUENCY_VALUE); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = DAY_BEFORE_DELIVER_MILK_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Lactation::DEFAULT_DELAY_BEFORE_DELIVER_MILK); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = HERD_PRODUCTION_DELTA_LAIT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Lactation::DEFAULT_HERD_PRODUCTION_DELTA_LAIT); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = HERD_PRODUCTION_DELTA_TB_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Lactation::DEFAULT_HERD_PRODUCTION_DELTA_TB); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = HERD_PRODUCTION_DELTA_TP_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Lactation::DEFAULT_HERD_PRODUCTION_DELTA_TP); vLines.push_back(Tools::changeChar(line,'.', ','));

        line = ""; vLines.push_back(line);
        line = TechnicalConstants::COMMENT + "Health decisions (2.2.3) :"; vLines.push_back(line);
        line = TechnicalConstants::COMMENT + "Mastitis (2.2.3.1.2) :"; vLines.push_back(line);
        line = G1_MASTITIS_DETECTION_SENSIBILITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::G1_MASTITIS_DEFAULT_DETECTION_SENSIBILITY); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_JAN_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::jan)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_FEB_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::feb)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_MAR_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::mar)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_APR_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::apr)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_MAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::may)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_JUN_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::jun)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_JUL_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::jul)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_AUG_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::aug)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_SEP_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::sep)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_OCT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::oct)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_NOV_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::nov)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_DEC_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.find(FunctionalEnumerations::Global::Month::dec)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_JAN_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::jan)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_FEB_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::feb)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_MAR_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::mar)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_APR_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::apr)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_MAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::may)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_JUN_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::jun)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_JUL_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::jul)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_AUG_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::aug)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_SEP_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::sep)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_OCT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::oct)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_NOV_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::nov)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_DEC_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.find(FunctionalEnumerations::Global::Month::dec)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_JAN_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::jan)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_FEB_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::feb)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_MAR_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::mar)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_APR_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::apr)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_MAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::may)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_JUN_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::jun)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_JUL_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::jul)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_AUG_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::aug)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_SEP_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::sep)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_OCT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::oct)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_NOV_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::nov)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_DEC_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.find(FunctionalEnumerations::Global::Month::dec)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_JAN_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::jan)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_FEB_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::feb)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_MAR_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::mar)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_APR_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::apr)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_MAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::may)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_JUN_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::jun)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_JUL_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::jul)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_AUG_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::aug)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_SEP_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::sep)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_OCT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::oct)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_NOV_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::nov)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_DEC_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.find(FunctionalEnumerations::Global::Month::dec)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_JAN_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::jan)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_FEB_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::feb)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_MAR_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::mar)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_APR_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::apr)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_MAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::may)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_JUN_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::jun)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_JUL_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::jul)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_AUG_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::aug)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_SEP_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::sep)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_OCT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::oct)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_NOV_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::nov)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_DEC_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.find(FunctionalEnumerations::Global::Month::dec)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_BACTERIUM_INCIDENCE_PART_STAPHA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_BACTERIUM_MASTITIS_INCIDENCE_PART.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_BACTERIUM_INCIDENCE_PART_STREPTU_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_BACTERIUM_MASTITIS_INCIDENCE_PART.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_BACTERIUM_INCIDENCE_PART_GN_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_BACTERIUM_MASTITIS_INCIDENCE_PART.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_BACTERIUM_INCIDENCE_PART_CNS_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_BACTERIUM_MASTITIS_INCIDENCE_PART.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_BACTERIUM_INCIDENCE_PART_CB_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_BACTERIUM_MASTITIS_INCIDENCE_PART.find(FunctionalEnumerations::Health::BacteriumType::CB)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        MastitisTreatmentPlanParameters::getDefaultValues(vLines); // Mastitis treatment plan
        line = INDIVIDUAL_MASTITIS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_COW_MASTITIS_INCIDENCE_PREVENTION_FACTOR); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = HERD_MASTITIS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_HERD_MASTITIS_INCIDENCE_PREVENTION_FACTOR); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = VET_CARE_CONTRACT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_VET_CARE_CONTRACT_OPTION); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = TechnicalConstants::COMMENT + "Ketosis (2.2.3.2.2) :"; vLines.push_back(line);
        line = KETOSIS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_KETOSIS_INCIDENCE_PREVENTION_FACTOR); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MONENSIN_BOLUS_USAGE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MONENSIN_BOLUS_USE); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = CETODETECT_USAGE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_CETODETECT_USAGE); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = HERD_NAVIGATOR_OPTION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_CETO_HERD_NAVIGATOR_OPTION); vLines.push_back(Tools::changeChar(line,'.', ','));
        KetosisTreatmentPlanParameters::getDefaultValues(vLines); // Ketosis treatment plan
        line = TechnicalConstants::COMMENT + "Lameness (2.2.3.3.2) :"; vLines.push_back(line);
        
        line = LAMENESS_NON_INFECTIOUS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_LAMENESS_INCIDENCE_PREVENTION_FACTOR.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_INFECTIOUS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_LAMENESS_INCIDENCE_PREVENTION_FACTOR.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = FOOT_BATH_OPTION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_FOOT_BATH_OPTION); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = FOOT_BATH_EFFECT_DURATION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_FOOT_BATH_EFFECT_DURATION); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = FOOT_BATH_STABULATION_FREQUENCY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_FOOT_BATH_FREQUENCY.find(FunctionalEnumerations::Population::Location::stabulation)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = FOOT_BATH_FULL_PASTURE_FREQUENCY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_FOOT_BATH_FREQUENCY.find(FunctionalEnumerations::Population::Location::fullPasture)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = FOOT_BATH_HALF_STABULATION_PASTURE_FREQUENCY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_FOOT_BATH_FREQUENCY.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = PREVENTIVE_TRIMMING_OPTION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_PREVENTIVE_TRIMMING_OPTION); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_DETECTION_MODE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_LAMENESS_DETECTION_MODE); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = COW_COUNT_FOR_SMALL_TRIMMING_OR_CARE_GROUP_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_COW_COUNT_FOR_SMALL_TRIMMING_OR_CARE_GROUP); vLines.push_back(Tools::changeChar(line,'.', ','));
        LamenessTreatmentPlanParameters::getDefaultValues(vLines); // Lameness treatment plan

        line = ""; vLines.push_back(line);
        line = TechnicalConstants::COMMENT + "Genetic decisions (2.2.4.2) :"; vLines.push_back(line);
        //line = MALE_DATA_FILE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(TechnicalConstants::BULL_CATALOG_DEFAULT_VALUES_FILE_NAME); vLines.push_back(line);
        line = GENETIC_STRATEGY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Genetic::DEFAULT_GENETIC_STRATEGY); vLines.push_back(Tools::changeChar(line,'.', ','));

        line = ""; vLines.push_back(line);
        line = TechnicalConstants::COMMENT + "Population decisions (2.2.5.2) :"; vLines.push_back(line);
        line = INITIAL_BREED_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_INITIAL_COW_BREED); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MEAN_ADULT_NUMBER_TARGET_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_MEAN_ADULT_NUMBER_TARGET); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = ANNUAL_HERD_RENEWABLE_PART_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_ANNUAL_HERD_RENEWAL_PART); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = ANNUAL_HERD_RENEWABLE_DELTA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_ANNUAL_HERD_RENEWAL_DELTA); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = FEMALE_CALVE_MANAGEMENT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_FEMALE_CALVE_MANAGEMENT); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MAX_LACTATION_RANK_FOR_END_INSEMINATION_DECISION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_MAX_LACTATION_RANK_FOR_END_INSEMINATION_DECISION); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MIN_MILK_PRODUCTION_HERD_RATIO_FOR_END_INSEMINATION_DECISION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_MIN_MILK_PRODUCTION_HERD_RATIO_FOR_END_INSEMINATION_DECISION); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MAX_SCC_LEVEL_FOR_END_INSEMINATION_DECISION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_MAX_SCC_LEVEL_FOR_END_INSEMINATION_DECISION); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MINIMUM_MILK_QUANTITY_FACTOR_FOR_RENTABILITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_MODULATION_FACTOR_FOR_LOW_QUANTITY_FOR_RENTABILITY); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_HEIFER_BEGIN_PASTURE_DATE_DAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_YOUNG_HEIFER_BEGIN_PASTURE_DATE.first); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_HEIFER_BEGIN_PASTURE_DATE_MONTH_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_YOUNG_HEIFER_BEGIN_PASTURE_DATE.second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_HEIFER_END_PASTURE_DATE_DAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_YOUNG_HEIFER_END_PASTURE_DATE.first); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_HEIFER_END_PASTURE_DATE_MONTH_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_YOUNG_HEIFER_END_PASTURE_DATE.second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OTHER_HEIFER_AND_DRIED_COW_BEGIN_PASTURE_DATE_DAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_OTHER_HEIFER_AND_DRIED_COW_BEGIN_PASTURE_DATE.first); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OTHER_HEIFER_AND_DRIED_COW_BEGIN_PASTURE_DATE_MONTH_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_OTHER_HEIFER_AND_DRIED_COW_BEGIN_PASTURE_DATE.second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OTHER_HEIFER_AND_DRIED_COW_END_PASTURE_DATE_DAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_OTHER_HEIFER_AND_DRIED_COW_END_PASTURE_DATE.first); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OTHER_HEIFER_AND_DRIED_COW_END_PASTURE_DATE_MONTH_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_OTHER_HEIFER_AND_DRIED_COW_END_PASTURE_DATE.second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATING_COW_BEGIN_FULL_PASTURE_DATE_DAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_LACTATING_COW_BEGIN_FULL_PASTURE_DATE.first); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATING_COW_BEGIN_FULL_PASTURE_DATE_MONTH_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_LACTATING_COW_BEGIN_FULL_PASTURE_DATE.second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATING_COW_END_FULL_PASTURE_DATE_DAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_LACTATING_COW_END_FULL_PASTURE_DATE.first); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATING_COW_END_FULL_PASTURE_DATE_MONTH_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_LACTATING_COW_END_FULL_PASTURE_DATE.second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATING_COW_BEGIN_STABULATION_DATE_DAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_LACTATING_COW_BEGIN_STABULATION_DATE.first); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATING_COW_BEGIN_STABULATION_DATE_MONTH_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_LACTATING_COW_BEGIN_STABULATION_DATE.second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATING_COW_END_STABULATION_DATE_DAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_LACTATING_COW_END_STABULATION_DATE.first); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATING_COW_END_STABULATION_DATE_MONTH_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_LACTATING_COW_END_STABULATION_DATE.second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = CALF_STRAW_CONSUMPTION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_CALF_STRAW_CONSUMPTION); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = HEIFER_STRAW_CONSUMPTION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_HEIFER_STRAW_CONSUMPTION); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = ADULT_STRAW_CONSUMPTION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Population::DEFAULT_COW_STRAW_CONSUMPTION); vLines.push_back(Tools::changeChar(line,'.', ','));

        line = ""; vLines.push_back(line);
        line = TechnicalConstants::COMMENT + "Feeding decisions (2.2.6.2) :"; vLines.push_back(line);
        FeedingPlanParameters::getDefaultValues(vLines); // Feeding plan
        
        // File generation
        std::string fileName = path + TechnicalConstants::DEFAULT_DAIRY_FARM_PARAMETER_VALUES_FILE_NAME;
        Tools::writeFile(vLines, fileName, true);        
    }
#endif // _LOG


} /* End of namespace Parameters */
}