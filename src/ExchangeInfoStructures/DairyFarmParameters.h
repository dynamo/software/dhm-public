////////////////////////////////////////////
//                                        //
// File : DairyFarmParameters.h           //
//                                        //
// Author : Philippe Gontier              //
//                                        //
// Date : 01/10/2015                      //
//                                        //
// Farm and herd settings                 //
//                                        //
////////////////////////////////////////////

#ifndef Parameters_DairyFarmParameters_h
#define Parameters_DairyFarmParameters_h

// Standard
#include <string>
#include <fstream>
#include <map>

// Boost
#include <boost/date_time/gregorian/gregorian.hpp>

// Project
#include "SystemToSimulateParameters.h"
#include "MatingPlanParameters.h"
#include "FeedingPlanParameters.h"
#include "MastitisTreatmentPlanParameters.h"
#include "KetosisTreatmentPlanParameters.h"
#include "LamenessTreatmentPlanParameters.h"
#include "ReproductionTreatmentPlanParameters.h"
#include "DairyFarmParametersConstants.h"


#include "MilkProductCharacteristic.h"

namespace ExchangeInfoStructures
{
/// Parameters
namespace Parameters
{             
    /// This structure make it possible to define in a specific way (i.e. excluding default values) the herd to be created and the management practices of the farm. 
    /// See the § 3.2.1.1 "Farm creating" of the "Functional description and terms of use" documentation.
    struct DairyFarmParameters : public SystemToSimulateParameters
    {
    private:
        // serialization
        DECLARE_SERIALIZE_STD_METHODS;

    public:
    // Attributes
    // ----------
        
    // Reproduction decisions (§ 2.2.1.2) :
    // ------------------------------------
        
        /// Age of breeding decision (see § 2.2.1.2.1 of the "Functional description and terms of use" documentation)
        unsigned int ageToBreedingDecision;
        
        /// Detection mode (see § 2.2.1.2.2 of the "Functional description and terms of use" documentation)
        FunctionalEnumerations::Reproduction::DetectionMode detectionMode;
        
        /// Slippery floor option (see § 2.2.1.2.3 of the "Functional description and terms of use" documentation)
        bool slipperyFloor;
        
        /// Mating plan (see § 2.2.1.2.4 of the "Functional description and terms of use" documentation)
        MatingPlanParameters matingPlan;

        /// Factor to increase/reduce the herd fertility (see § 2.2.1.2.5 of the "Functional description and terms of use" documentation)
        float fertilityFactor;
        
        /// Breeding delay after calving decision (see § 2.2.1.2.6 of the "Functional description and terms of use" documentation)       
        unsigned int breedingDelayAfterCalvingDecision;
        
        /// Number of days after heifer breeding decision to not do the first insemination decision (never see the oestrus, see § 2.2.1.2.7 of the "Functional description and terms of use" documentation)       
        unsigned int numberOfDaysAfterHeiferBreedingDecisionForStopFirstInseminationDecision;
        
        /// Number of days after adult breeding decision to not do the first insemination decision (never see the oestrus, see § 2.2.1.2.7 of the "Functional description and terms of use" documentation) 
        unsigned int numberOfDaysAfterAdultBreedingDecisionForStopFirstInseminationDecision;
        
        /// Number of days after heifer breeding decision for stop IA decision (IAs never fertilized, see § 2.2.1.2.7 of the "Functional description and terms of use" documentation) 
        unsigned int numberOfDaysAfterHeiferBreedingDecisionForStopInseminationDecision;
        
        /// Number of days after last adult breeding decision for stop IA decision (IAs never fertilized, see § 2.2.1.2.7 of the "Functional description and terms of use" documentation) 
        unsigned int numberOfDaysAfterAdultBreedingDecisionForStopInseminationDecision; 
        
        /// Veterinary reproduction contract (see § 2.2.1.2.9 of the "Functional description and terms of use" documentation)
        bool vetReproductionContract;

        /// Reproduction treatment plan (see § 2.2.1.2.9 of the "Functional description and terms of use" documentation)
        ReproductionTreatmentPlanParameters reproductionTreatmentPlan;

        
    // Lactation decisions (§ 2.2.2.2) :
    // ---------------------------------
                
        /// Dried period duration (see § 2.2.2.2.1 of the "Functional description and terms of use" documentation)
        unsigned int driedPeriodDuration;

        /// Daily milk frequency (see § 2.2.2.2.2 of the "Functional description and terms of use" documentation)
        FunctionalEnumerations::Lactation::MilkingFrequency milkingFrequency;
        
        /// Legal minimum delay to deliver milk (see § 2.2.2.2.3 of the "Functional description and terms of use" documentation)
        unsigned int dayBeforeDeliverMilk;
        
        /// Herd production delta (see § 2.2.2.2.4 of the "Functional description and terms of use" documentation)
        ExchangeInfoStructures::MilkProductCharacteristic herdProductionDelta;
        
    // Health decisions (§ 2.2.3.2) :
    // ------------------------------
        
        /// Veterinary health contract
        FunctionalEnumerations::Health::VeterinaryCareContractType vetCareContract;
        
        /// G1 Mastitis detection sensibility (see § 2.2.3.1.2.1 of the "Functional description and terms of use" documentation)
        float G1MastitisDetectionSensibility;
        
        /// Mastitis saisonality probability factor (see § 2.2.3.1.2.2 of the "Functional description and terms of use" documentation)
        std::map<FunctionalEnumerations::Health::BacteriumType,std::map<FunctionalEnumerations::Global::Month, float>> mastitisSaisonalityProbabilityFactor;
        
        /// Mastitis bacterium incidence part (see § 2.2.3.1.2.3 of the "Functional description and terms of use" documentation)
        std::map<FunctionalEnumerations::Health::BacteriumType, float> bacteriumIncidencePart;
        
        /// Mastitis treatment plan (see § 2.2.3.1.2.4 of the "Functional description and terms of use" documentation)
        MastitisTreatmentPlanParameters mastitisTreatmentPlan;
        
        /// Individual mastitis incidence prevention factor (see § 2.2.3.1.2.5 of the "Functional description and terms of use" documentation)
        float individualMastitisIncidencePreventionFactor;
        
        /// Herd mastitis incidence prevention factor (see § 2.2.3.1.2.6 of the "Functional description and terms of use" documentation)
        float herdMastitisIncidencePreventionFactor;
        
        /// Ketosis incidence prevention factor (see § 2.2.3.2.2.1.1 of the "Functional description and terms of use" documentation)
        float ketosisIncidencePreventionFactor;
        
        /// Monensin bolus usage to prevent ketosis cases (see § 2.2.3.2.2.1.2 of the "Functional description and terms of use" documentation)
        bool monensinBolusUsage;
        
        /// CetoDetect usage for subclinical ketosis (see § 2.2.3.2.2.2.1 of the "Functional description and terms of use" documentation)
        bool cetoDetectUsage;
        
        /// Herd Navigator option for subclinical ketosis (see § 2.2.3.2.2.2.2 of the "Functional description and terms of use" documentation)
        bool herdNavigatorOption;
        
        /// Ketosis treatment plan (see § 2.2.3.2.2.3 of the "Functional description and terms of use" documentation)
        KetosisTreatmentPlanParameters ketosisTreatmentPlan;
        
        /// Lameness incidence prevention factor (see § 2.2.3.3.2.1 of the "Functional description and terms of use" documentation)
        std::map<FunctionalEnumerations::Health::LamenessInfectiousStateType, float> lamenessIncidencePreventionFactor;
        
        /// Foot bath option (see § 2.2.3.3.2.1.2 of the "Functional description and terms of use" documentation)
        bool footBath;
        
        /// Foot bath efficiency durations (see § 2.2.3.3.2.1.2 of the "Functional description and terms of use" documentation)
        unsigned int footBathProtectionDurationForLameness;
        
        /// Foot bath frequency (see § 2.2.3.3.2.1.3 of the "Functional description and terms of use" documentation)
        std::map<FunctionalEnumerations::Population::Location, unsigned int> footBathFrequency;
               
        /// Preventive trimming options (see § 2.2.3.3.2.1.4 of the "Functional description and terms of use" documentation)
        FunctionalEnumerations::Health::LamenessPreventiveTrimmingOption preventiveTrimmingOption;
        
         /// Detection mode (see § 2.2.3.3.2.2 of the "Functional description and terms of use" documentation)
        FunctionalEnumerations::Health::LamenessDetectionMode lamenessDetectionMode;
        
       /// Cow count for small trimming or lameness group (see § 2.2.3.3.2.3 of the "Functional description and terms of use" documentation)
        unsigned int cowCountForSmallTrimmingOrCareGroup;
        
        /// Lameness treatment plan (see § 2.2.3.3.2.4 of the "Functional description and terms of use" documentation)
        LamenessTreatmentPlanParameters lamenessTreatmentPlan;

    // Genetic decisions (§ 2.2.4.2) :
    // -------------------------------
        
        /// Optional file name for male genetic data (see § 2.2.4.2.1 of the "Functional description and terms of use" documentation)
        std::string maleDataFile;
        
        /// Genetic strategy (see § 2.2.4.2.2 of the "Functional description and terms of use" documentation)
        FunctionalEnumerations::Genetic::GeneticStrategy geneticStrategy;
        
    // Population decisions (§ 2.2.5.2) :
    // ----------------------------------
        
        /// Initial breed of the herd (see § 2.2.5.2.1.1 of the "Functional description and terms of use" documentation)
        FunctionalEnumerations::Genetic::Breed initialBreed;
        
        /// Mean adult dairy cows number (see § 2.2.5.2.1.2 of the "Functional description and terms of use" documentation)
        unsigned int meanAdultNumberTarget;
        
        /// Annual herd renewable part (see § 2.2.5.2.1.3 of the "Functional description and terms of use" documentation)
        float initialAnnualHerdRenewablePart;
        float annualHerdRenewableDelta;
              
        /// Female calve management (see § 2.2.5.2.1.4 of the "Functional description and terms of use" documentation)
        FunctionalEnumerations::Population::FemaleCalveManagement femaleCalveManagement;
        
        /// Max lactation rank for stop insemination decision (see § 2.2.5.2.1.5 of the "Functional description and terms of use" documentation)
        unsigned int maxLactationRankForEndInseminationDecision;
        
        /// Minimal milk production herd ratio for stop insemination decision (see § 2.2.5.2.1.6 of the "Functional description and terms of use" documentation)
        float minMilkProductionHerdRatioForEndInseminationDecision;
        
        /// Maximal SCC level in the two last milk controls for stop insemination decision (see § 2.2.5.2.1.7 of the "Functional description and terms of use" documentation)
        unsigned int maxSCCLevelForEndInseminationDecision;

        /// Minimum milk quantity for dried period (see § 2.2.5.2.1.8 of the "Functional description and terms of use" documentation)
        float minimumMilkQuantityFactorForRentability;

        /// Young heifer begin pasture dates decision (see § 2.2.5.2.2.1 of the "Functional description and terms of use" documentation)
        std::pair<unsigned int, FunctionalEnumerations::Global::Month> youngHeiferBeginPastureDate;

        /// Young heifer end pasture dates decision (see § 2.2.5.2.2.1 of the "Functional description and terms of use" documentation)
        std::pair<unsigned int, FunctionalEnumerations::Global::Month> youngHeiferEndPastureDate;
        
        /// Other heifers and dried cows begin pasture dates decision (see § 2.2.5.2.2.1 of the "Functional description and terms of use" documentation)
        std::pair<unsigned int, FunctionalEnumerations::Global::Month> otherHeiferAndDriedCowBeginPastureDate;

        /// Other heifers and dried cows end pasture dates decision (see § 2.2.5.2.2.1 of the "Functional description and terms of use" documentation)
        std::pair<unsigned int, FunctionalEnumerations::Global::Month> otherHeiferAndDriedCowEndPastureDate;
        
        /// Lactation cows begin full pasture dates decision (see § 2.2.5.2.2.1 of the "Functional description and terms of use" documentation)
        std::pair<unsigned int, FunctionalEnumerations::Global::Month> lactatingCowBeginFullPastureDate;

        /// Lactation cows end full pasture dates decision (see § 2.2.5.2.2.1 of the "Functional description and terms of use" documentation)
        std::pair<unsigned int, FunctionalEnumerations::Global::Month> lactatingCowEndFullPastureDate;
        
        /// Lactation cows begin full stabulation dates decision (see § 2.2.5.2.2.1 of the "Functional description and terms of use" documentation)
        std::pair<unsigned int, FunctionalEnumerations::Global::Month> lactatingCowBeginStabulationDate;

        /// Lactation cows end full stabulation dates decision (see § 2.2.5.2.2.1 of the "Functional description and terms of use" documentation)
        std::pair<unsigned int, FunctionalEnumerations::Global::Month> lactatingCowEndStabulationDate;
        
        /// Calf straw consumption (see § 2.2.5.2.2.2 of the "Functional description and terms of use" documentation)
        float calfStrawConsumption;
        
        /// Heifer straw consumption (see § 2.2.5.2.2.2 of the "Functional description and terms of use" documentation)
        float heiferStrawConsumption;
        
        /// Cow straw consumption (see § 2.2.5.2.2.2 of the "Functional description and terms of use" documentation)
        float adultStrawConsumption;
        

    // Feeding decisions (§ 2.2.6.2) :
    // -------------------------------

        /// Calves, heifers and cows feeding plan (see § 2.2.6.2 of the "Functional description and terms of use" documentation)
        FeedingPlanParameters feedingPlan;
        
#ifdef _SENSIBILITY_ANALYSIS
        
    // Sensitivity analysis :
    // ----------------------     
        float SA_mastitisPercentPreventionProgress; // FunctionalConstants::Health::PERCENT_PREVENTION_PROGRESS
        float SA_ketosisPercentPreventionProgress; // FunctionalConstants::Health::PERCENT_PREVENTION_PROGRESS
        float SA_additionalMastitisHealingIfCareVetContract; // FunctionalConstants::Health::ADDITIONAL_MASTITIS_HEALING_PROBABILITY_IF_CARE_VET_CONTRACT
        float SA_additionalKetosisHealingIfCareVetContract; // FunctionalConstants::Health::ADDITIONAL_KETOSIS_HEALING_PROBABILITY_IF_CARE_VET_CONTRACT
        float SA_nonManagedMortalityFactorWithVetCareContract; // FunctionalConstants::Population::NON_MANAGED_MORTALITY_FACTOR_WITH_VET_CARE_CONTRACT
        
#endif // _SENSIBILITY_ANALYSIS
        
    protected:
        void concrete(){}; // To allow instanciation

    public:
        // constructors / destructors
        DairyFarmParameters(); // For serialization
        DairyFarmParameters(std::string &dairyFarmName);
        DairyFarmParameters(std::string &dairyFarmName, std::string &fileName, std::string &geneticCataloguePath);
        DairyFarmParameters (const DairyFarmParameters &obj);        // copy constructor
        virtual ~DairyFarmParameters(){}; // destructor
           
        // Methods
        
        /// Value checking, errors in the std::string parameter reference
        virtual bool autoControl (
                                    /// Reference std::string to put the error message (empty value if no error)
                                    std::string &message
                                    ) override;
        bool setImportedValue(const std::string &key, std::string &content);
        bool getValueToExport(const std::string &key, std::string &content);
#ifdef _LOG
        static void saveDefaultDairyFarmValues(const std::string &path);
#endif // _LOG
    }; /* End of struct DairyFarmParameters */
      
    
} /* End of namespace Parameters */
}
#endif // Parameters_DairyFarmParameters_h
