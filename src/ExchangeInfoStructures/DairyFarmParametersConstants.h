////////////////////////////////////////////
//                                        //
// File : DairyFarmParametersConstants.h  //
//                                        //
// Author : Philippe Gontier              //
//                                        //
// Date : 01/10/2015                      //
//                                        //
// Farm and herd settings                 //
//                                        //
////////////////////////////////////////////

#ifndef Parameters_DairyFarmParametersConstants_h
#define Parameters_DairyFarmParametersConstants_h

// Standard
#include <string>

// Boost

// Project

namespace ExchangeInfoStructures
{
/// Parameters
namespace Parameters
{             
    // Import/export key values   
    
    static const std::string COMMENT_PARAM_KEY = "comment";

    static const std::string VET_REPRODUCTION_CONTRACT_PARAM_KEY = "vetReproductionContract";
    static const std::string AGE_TO_BREEDING_DECISION_PARAM_KEY = "ageToBreedingDecision";
    static const std::string DETECTION_MODE_PARAM_KEY = "detectionMode";
    static const std::string SLIPPERY_FLOOR_PARAM_KEY = "slipperyFloor";
    // (mating plan)...
    static const std::string FERTILITY_FACTOR_PARAM_KEY = "fertilityFactor";
    static const std::string BREEDING_DELAY_AFTER_CALVING_DECISION_PARAM_KEY = "breedingDelayAfterCalvingDecision";
    static const std::string NUMBER_OF_DAYS_AFTER_HEIFER_BREEDING_DECISION_FOR_STOP_FIRST_INSEMINATION_DECISION_PARAM_KEY = "numberOfDaysAfterHeiferBreedingDecisionForStopFirstInseminationDecision";
    static const std::string NUMBER_OF_DAYS_AFTER_HEIFER_BREEDING_DECISION_FOR_STOP_INSEMINATION_DECISION_PARAM_KEY = "numberOfDaysAfterHeiferBreedingDecisionForStopInseminationDecision";
    static const std::string NUMBER_OF_DAYS_AFTER_ADULT_BREEDING_DECISION_FOR_STOP_FIRST_INSEMINATION_DECISION_PARAM_KEY = "numberOfDaysAfterAdultBreedingDecisionForStopFirstInseminationDecision";
    static const std::string NUMBER_OF_DAYS_AFTER_ADULT_BREEDING_DECISION_FOR_STOP_INSEMINATION_DECISION_PARAM_KEY = "numberOfDaysAfterAdultBreedingDecisionForStopInseminationDecision";

    static const std::string DRIED_PERIOD_DURATION_PARAM_KEY = "driedPeriodDuration";
    static const std::string MINIMUM_MILK_QUANTITY_FACTOR_FOR_RENTABILITY_PARAM_KEY = "minimumMilkQuantityFactorForRentability";
    static const std::string MILKING_FREQUENCY_PARAM_KEY = "milkingFrequency";
    static const std::string DAY_BEFORE_DELIVER_MILK_PARAM_KEY = "dayBeforeDeliverMilk";
    static const std::string HERD_PRODUCTION_DELTA_LAIT_PARAM_KEY = "herdProductionDelta_Lait";
    static const std::string HERD_PRODUCTION_DELTA_TB_PARAM_KEY = "herdProductionDelta_TB";
    static const std::string HERD_PRODUCTION_DELTA_TP_PARAM_KEY = "herdProductionDelta_TP";

    static const std::string VET_CARE_CONTRACT_PARAM_KEY = "vetCareContract";
    static const std::string G1_MASTITIS_DETECTION_SENSIBILITY_PARAM_KEY = "G1MastitisDetectionSensibility";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_JAN_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StaphA_jan";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_FEB_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StaphA_feb";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_MAR_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StaphA_mar";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_APR_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StaphA_apr";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_MAY_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StaphA_may";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_JUN_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StaphA_jun";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_JUL_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StaphA_jul";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_AUG_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StaphA_aug";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_SEP_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StaphA_sep";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_OCT_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StaphA_oct";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_NOV_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StaphA_nov";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STAPHA_DEC_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StaphA_dec";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_JAN_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StreptU_jan";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_FEB_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StreptU_feb";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_MAR_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StreptU_mar";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_APR_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StreptU_apr";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_MAY_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StreptU_may";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_JUN_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StreptU_jun";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_JUL_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StreptU_jul";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_AUG_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StreptU_aug";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_SEP_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StreptU_sep";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_OCT_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StreptU_oct";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_NOV_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StreptU_nov";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_STREPTU_DEC_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_StreptU_dec";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_JAN_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_Gn_jan";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_FEB_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_Gn_feb";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_MAR_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_Gn_mar";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_APR_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_Gn_apr";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_MAY_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_Gn_may";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_JUN_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_Gn_jun";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_JUL_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_Gn_jul";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_AUG_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_Gn_aug";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_SEP_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_Gn_sep";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_OCT_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_Gn_oct";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_NOV_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_Gn_nov";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_GN_DEC_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_Gn_dec";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_JAN_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CNS_jan";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_FEB_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CNS_feb";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_MAR_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CNS_mar";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_APR_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CNS_apr";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_MAY_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CNS_may";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_JUN_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CNS_jun";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_JUL_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CNS_jul";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_AUG_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CNS_aug";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_SEP_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CNS_sep";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_OCT_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CNS_oct";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_NOV_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CNS_nov";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CNS_DEC_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CNS_dec";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_JAN_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CB_jan";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_FEB_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CB_feb";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_MAR_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CB_mar";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_APR_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CB_apr";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_MAY_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CB_may";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_JUN_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CB_jun";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_JUL_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CB_jul";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_AUG_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CB_aug";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_SEP_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CB_sep";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_OCT_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CB_oct";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_NOV_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CB_nov";
    static const std::string MASTITIS_SAISONALITY_PROBABILITY_FACTOR_CB_DEC_PARAM_KEY = "mastitisSaisonalityProbabilityFactor_CB_dec";
    static const std::string MASTITIS_BACTERIUM_INCIDENCE_PART_STAPHA_PARAM_KEY = "mastitisBacteriumIncidencePartStaphA";
    static const std::string MASTITIS_BACTERIUM_INCIDENCE_PART_STREPTU_PARAM_KEY = "mastitisBacteriumIncidencePartStreptU";
    static const std::string MASTITIS_BACTERIUM_INCIDENCE_PART_GN_PARAM_KEY = "mastitisBacteriumIncidencePartGn";
    static const std::string MASTITIS_BACTERIUM_INCIDENCE_PART_CNS_PARAM_KEY = "mastitisBacteriumIncidencePartCNS";
    static const std::string MASTITIS_BACTERIUM_INCIDENCE_PART_CB_PARAM_KEY = "mastitisBacteriumIncidencePartCB";

    // (mastitis treatment plan) ...
    static const std::string INDIVIDUAL_MASTITIS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY = "individualMastitisIncidencePreventionFactor";
    static const std::string HERD_MASTITIS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY = "herdMastitisIncidencePreventionFactor";
    static const std::string KETOSIS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY = "ketosisIncidencePreventionFactor";
    static const std::string MONENSIN_BOLUS_USAGE_PARAM_KEY = "monensinBolusUsage";
    static const std::string CETODETECT_USAGE_PARAM_KEY = "cetoDetectUsage";
    static const std::string HERD_NAVIGATOR_OPTION_PARAM_KEY = "herdNavigatorOption";
    // (ketosis treatment plan)...
    static const std::string LAMENESS_NON_INFECTIOUS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY = "lamenessNonInfectiousIncidencePreventionFactor";
    static const std::string LAMENESS_INFECTIOUS_INCIDENCE_PREVENTION_FACTOR_PARAM_KEY = "lamenessInfectiousIncidencePreventionFactor";
    static const std::string FOOT_BATH_OPTION_PARAM_KEY = "footBathOption";
    static const std::string FOOT_BATH_EFFECT_DURATION_PARAM_KEY = "footBathEffectDuration";
    static const std::string FOOT_BATH_STABULATION_FREQUENCY_PARAM_KEY = "footBathStabulationFrequency";
    static const std::string FOOT_BATH_FULL_PASTURE_FREQUENCY_PARAM_KEY = "footBathFullPastureFrequency";
    static const std::string FOOT_BATH_HALF_STABULATION_PASTURE_FREQUENCY_PARAM_KEY = "footBathHalfStabulationPastureFrequency";
    static const std::string PREVENTIVE_TRIMMING_OPTION_PARAM_KEY = "preventiveTrimmingOption";
    static const std::string LAMENESS_DETECTION_MODE_PARAM_KEY = "lamenessDetectionMode";
    static const std::string COW_COUNT_FOR_SMALL_TRIMMING_OR_CARE_GROUP_PARAM_KEY = "cowNumberForSmallTrimmingOrCareGroup";
    // (lameness treatment plan)...
    
    static const std::string MALE_DATA_FILE_PARAM_KEY = "maleDataFile";
    static const std::string GENETIC_STRATEGY_PARAM_KEY = "geneticStrategy";

    static const std::string INITIAL_BREED_PARAM_KEY = "initialBreed";
    static const std::string MEAN_ADULT_NUMBER_TARGET_PARAM_KEY = "meanAdultNumberTarget";
    static const std::string ANNUAL_HERD_RENEWABLE_PART_PARAM_KEY = "initialAnnualHerdRenewablePart";
    static const std::string ANNUAL_HERD_RENEWABLE_DELTA_PARAM_KEY = "annualHerdRenewableDelta";
    static const std::string FEMALE_CALVE_MANAGEMENT_PARAM_KEY = "femaleCalveManagement";
    static const std::string MAX_LACTATION_RANK_FOR_END_INSEMINATION_DECISION_PARAM_KEY = "maxLactationRankForEndInseminationDecision";
    static const std::string MIN_MILK_PRODUCTION_HERD_RATIO_FOR_END_INSEMINATION_DECISION_PARAM_KEY = "minMilkProductionHerdRatioForEndInseminationDecision";
    static const std::string MAX_SCC_LEVEL_FOR_END_INSEMINATION_DECISION_PARAM_KEY = "maxSCCLevelForEndInseminationDecision";
    
    static const std::string YOUNG_HEIFER_BEGIN_PASTURE_DATE_DAY_PARAM_KEY = "youngHeiferBeginPastureDate_day";
    static const std::string YOUNG_HEIFER_BEGIN_PASTURE_DATE_MONTH_PARAM_KEY = "youngHeiferBeginPastureDate_month";
    static const std::string YOUNG_HEIFER_END_PASTURE_DATE_DAY_PARAM_KEY = "youngHeiferEndPastureDate_day";
    static const std::string YOUNG_HEIFER_END_PASTURE_DATE_MONTH_PARAM_KEY = "youngHeiferEndPastureDate_month";
    static const std::string OTHER_HEIFER_AND_DRIED_COW_BEGIN_PASTURE_DATE_DAY_PARAM_KEY = "otherHeiferAndDriedCowBeginPastureDate_day";
    static const std::string OTHER_HEIFER_AND_DRIED_COW_BEGIN_PASTURE_DATE_MONTH_PARAM_KEY = "otherHeiferAndDriedCowBeginPastureDate_month";
    static const std::string OTHER_HEIFER_AND_DRIED_COW_END_PASTURE_DATE_DAY_PARAM_KEY = "otherHeiferAndDriedCowEndPastureDate_day";
    static const std::string OTHER_HEIFER_AND_DRIED_COW_END_PASTURE_DATE_MONTH_PARAM_KEY = "otherHeiferAndDriedCowEndPastureDate_month";
    static const std::string LACTATING_COW_BEGIN_FULL_PASTURE_DATE_DAY_PARAM_KEY = "lactatingCowBeginFullPastureDate_day";
    static const std::string LACTATING_COW_BEGIN_FULL_PASTURE_DATE_MONTH_PARAM_KEY = "lactatingCowBeginFullPastureDate_month";
    static const std::string LACTATING_COW_END_FULL_PASTURE_DATE_DAY_PARAM_KEY = "lactatingCowEndFullPastureDate_day";
    static const std::string LACTATING_COW_END_FULL_PASTURE_DATE_MONTH_PARAM_KEY = "lactatingCowEndFullPasture_month";
    static const std::string LACTATING_COW_BEGIN_STABULATION_DATE_DAY_PARAM_KEY = "lactatingCowBeginStabulationDate_day";
    static const std::string LACTATING_COW_BEGIN_STABULATION_DATE_MONTH_PARAM_KEY = "lactatingCowBeginStabulationDate_month";
    static const std::string LACTATING_COW_END_STABULATION_DATE_DAY_PARAM_KEY = "lactatingCowEndStabulationDate_day";
    static const std::string LACTATING_COW_END_STABULATION_DATE_MONTH_PARAM_KEY = "lactatingCowEndStabulation_month";    
    
    static const std::string CALF_STRAW_CONSUMPTION_PARAM_KEY = "calfStrawConsumption";
    static const std::string HEIFER_STRAW_CONSUMPTION_PARAM_KEY = "heiferStrawConsumption";
    static const std::string ADULT_STRAW_CONSUMPTION_PARAM_KEY = "adultStrawConsumption";
    
#ifdef _SENSIBILITY_ANALYSIS
    static const std::string MASTITIS_PERCENT_PREVENTION_PROGRESS_SA_KEY = "SA_mastitisPercentPreventionProgress";
    static const std::string KETOSIS_PERCENT_PREVENTION_PROGRESS_SA_KEY = "SA_ketosisPercentPreventionProgress";
    static const std::string ADDITIONAL_MASTITIS_HEALING_PROBABILITY_IF_CARE_VET_CONTRACT_SA_KEY = "SA_additionalMastitisHealingIfCareVetContract";
    static const std::string ADDITIONAL_KETOSIS_HEALING_PROBABILITY_IF_CARE_VET_CONTRACT_SA_KEY = "SA_additionalKetosisHealingIfCareVetContract";
    static const std::string NON_MANAGED_MORTALITY_FACTOR_WITH_VET_CARE_CONTRACT_SA_KEY = "SA_nonManagedMortalityFactorWithVetCareContract";
#endif // _SENSIBILITY_ANALYSIS
    
} /* End of namespace Parameters */
}
#endif // Parameters_DairyFarmParametersConstants_h
