#ifndef DetectionConditions_h
#define DetectionConditions_h

// standard

// project
#include "../ExchangeInfoStructures/FunctionalEnumerations.h"

namespace ExchangeInfoStructures
{
    struct DetectionConditions
    {
    public:
        FunctionalEnumerations::Reproduction::DetectionMode detectionMode;
        FunctionalEnumerations::Reproduction::DairyCowParity parity;
        unsigned int lastUnfecundedOvulationNumber;
        unsigned int returnInHeatNumber;
        bool slipperyFloor;
        unsigned int simultaneousOvulation; // Number of simulateous ovulation in the batch
        float dayMilkProduction; // PLJ
        float lamenessDetectionFactor; // Lameness oestrus detection factor
    };
    
} /* End of namespace Reproduction */
#endif // DetectionConditions_h
