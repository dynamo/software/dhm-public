#include "EnDairyFarmDictionary.h"

// project
#include "LanguageDictionary.h"

namespace ExchangeInfoStructures
{
    EnDairyFarmDictionary::EnDairyFarmDictionary() : EnDictionary()
    {
        // Setting the used messages labels 
        _mpMessageDictionary[KEY_THE_ANIMAL_NUMBER_IS_NOT_VALID] = "At least 1 animal and less than 10000 by cathegory is required";
        _mpMessageDictionary[KEY_THE_RENEWAL_RATIO_IS_NOT_VALID] = "The renewal ratio of the herd is not valid";
        _mpMessageDictionary[KEY_THE_COW_MASTITIS_INCIDENCE_PREVENTION_FACTOR_IS_NOT_VALID] = "The cow mastitis incidence prevention factor is not valid";
        _mpMessageDictionary[KEY_THE_HERD_MASTITIS_INCIDENCE_PREVENTION_FACTOR_IS_NOT_VALID] = "The herd mastitis incidence prevention factor is not valid";
        _mpMessageDictionary[KEY_THE_KETOSIS_INCIDENCE_PREVENTION_FACTOR_IS_NOT_VALID] = "The ketosis incidence prevention factor is not valid";
        _mpMessageDictionary[KEY_THE_NI_LAMENESS_INCIDENCE_PREVENTION_FACTOR_IS_NOT_VALID] = "The non infectious lameness incidence prevention factor is not valid";
        _mpMessageDictionary[KEY_THE_I_LAMENESS_INCIDENCE_PREVENTION_FACTOR_IS_NOT_VALID] = "The infectious lameness incidence prevention factor is not valid";
        _mpMessageDictionary[KEY_A_TYPE_RATIO_IS_NEGATIVE] = "A type ratio is negative";
        _mpMessageDictionary[KEY_THE_TYPE_RATIO_SUM_NOT_EQUAL_TO_0_OR_1] = "The type ratio sum is not equal to 0 (empty mixture) or 1 (correct mixture)";
        _mpMessageDictionary[KEY_THE_QUANTITY_IS_NEGATIVE] = "The quantity is negative";
        _mpMessageDictionary[KEY_THE_RATIO_SUM_NOT_EQUAL_TO_1] = "The ratio sum is not equal to 1";
    }
        
} /* End of namespace ExchangeInfoStructures */