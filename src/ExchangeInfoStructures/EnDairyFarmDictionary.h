#ifndef ExchangeInfoStructures_EnDairyFarmDictionary_h
#define ExchangeInfoStructures_EnDairyFarmDictionary_h

// project
#include "EnDictionary.h"

namespace ExchangeInfoStructures
{
    // class for english language of the labels and messages
    class EnDairyFarmDictionary : public EnDictionary
    {
    public:
        EnDairyFarmDictionary(); // default constructor
    }; // end of the EnDairyHerdDictionary class
} /* End of namespace ExchangeInfoStructures */

#endif // ExchangeInfoStructures_EnDairyFarmDictionary_h
