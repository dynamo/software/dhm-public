////////////////////////////////////////
//                                    //
// File : EnDictionary.cpp            //
//                                    //
// Author : Philippe Gontier          //
//                                    //
// Version 1.0                        //
//                                    //
// Date : 01/10/2015                  //
//                                    //
////////////////////////////////////////

#include "EnDictionary.h"

namespace ExchangeInfoStructures
{
    EnDictionary::EnDictionary() : LanguageDictionary(LanguageDictionary::en)
    {
        // Setting the used messages labels   
        _mpMessageDictionary[KEY_THE_SYSTEM_TO_SIMULATE] = "The system to simulate \"";
        _mpMessageDictionary[KEY_ALREADY_EXISTS] = "\" already exists";
        _mpMessageDictionary[KEY_THE_ACCOUNTING_MODEL] = "The accounting model \"";
        _mpMessageDictionary[KEY_THE_PROTOCOL] = "The protocol \"";
        _mpMessageDictionary[KEY_IS_UNKNOWN] = "\" is unknown";
        _mpMessageDictionary[KEY_THE_NAME_IS_EMPTY] = "The name is empty";
        _mpMessageDictionary[KEY_THE_RUN_NUMBER_IS_NOT_VALID] = "The run number is not valid";
        _mpMessageDictionary[KEY_THE_RUN_DURATION_IS_NOT_VALID] = "The run duration is not valid";
        _mpMessageDictionary[KEY_THE_DATE_OF_THE_SIMULATION_BEGIN_IS_NOT_VALID] = "The date of the simulation begin is not valid";
    }
        
} /* End of namespace ExchangeInfoStructures */