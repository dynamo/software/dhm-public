////////////////////////////////////////
//                                    //
// File : EnDictionary.h              //
//                                    //
// Author : Philippe Gontier          //
//                                    //
// Version 1.0                        //
//                                    //
// Date : 01/10/2015                  //
//                                    //
////////////////////////////////////////

#ifndef ExchangeInfoStructures_EnDictionary_h
#define ExchangeInfoStructures_EnDictionary_h

// project
#include "LanguageDictionary.h"

namespace ExchangeInfoStructures
{
    // class for english language of the labels and messages
    class EnDictionary : public LanguageDictionary
    {
    public:
        EnDictionary(); // default constructor
    }; // end of the EnDictionary class
} /* End of namespace ExchangeInfoStructures */

#endif // ExchangeInfoStructures_EnDictionary_h
