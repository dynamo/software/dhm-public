#include "ExcludedCalvingPeriodParameters.h"

// Project
#include "../Tools/Tools.h"

BOOST_CLASS_EXPORT(ExchangeInfoStructures::Parameters::ExcludedCalvingPeriodParameters) // Recommended (mandatory if virtual serialization)

namespace ExchangeInfoStructures
{
namespace Parameters
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(firstMonthForCalvingExculsion); \
    ar & BOOST_SERIALIZATION_NVP(lastMonthForCalvingExculsion); \
    
    IMPLEMENT_SERIALIZE_STD_METHODS(ExcludedCalvingPeriodParameters);
    
    ExcludedCalvingPeriodParameters::ExcludedCalvingPeriodParameters()
    {
    }

    ExcludedCalvingPeriodParameters::ExcludedCalvingPeriodParameters(FunctionalEnumerations::Global::Month theFirstMonth, FunctionalEnumerations::Global::Month theLastMonth)
    {
        firstMonthForCalvingExculsion = theFirstMonth;
        lastMonthForCalvingExculsion = theLastMonth;
    }
        
    ExcludedCalvingPeriodParameters::ExcludedCalvingPeriodParameters(ExcludedCalvingPeriodParameters const& other)
    {
        firstMonthForCalvingExculsion = other.firstMonthForCalvingExculsion;
        lastMonthForCalvingExculsion = other.lastMonthForCalvingExculsion;
    }
    
    bool ExcludedCalvingPeriodParameters::operator==(ExcludedCalvingPeriodParameters const& other)
    {
        return firstMonthForCalvingExculsion == other.firstMonthForCalvingExculsion and lastMonthForCalvingExculsion == other.lastMonthForCalvingExculsion;
    }
}
}