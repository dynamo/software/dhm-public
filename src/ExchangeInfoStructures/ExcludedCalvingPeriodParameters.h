#ifndef ExcludedCalvingPeriodParameters_h
#define ExcludedCalvingPeriodParameters_h

// Project
#include "FunctionalEnumerations.h"

#include "../Tools/Serialization.h"

namespace ExchangeInfoStructures
{
namespace Parameters
{
    /// This structure make it possible to define a simulation protocol 
    /// See the § 2.2.1.2.5 "Grouped calving" of the "Functional description and terms of use" documentation.
    struct ExcludedCalvingPeriodParameters
    {
    private:
        DECLARE_SERIALIZE_STD_METHODS;

    public:
        
        /// First month for calving exclusion
        FunctionalEnumerations::Global::Month firstMonthForCalvingExculsion = (FunctionalEnumerations::Global::Month)0;
        
        /// Last month for calving exclusion
        FunctionalEnumerations::Global::Month lastMonthForCalvingExculsion = (FunctionalEnumerations::Global::Month)0;
        
        ExcludedCalvingPeriodParameters();
        ExcludedCalvingPeriodParameters(FunctionalEnumerations::Global::Month theFirstMonth, FunctionalEnumerations::Global::Month theLastMonth);
        virtual ~ExcludedCalvingPeriodParameters(){};
        ExcludedCalvingPeriodParameters(ExcludedCalvingPeriodParameters const& other);
        bool operator==(ExcludedCalvingPeriodParameters const& other);

    };
}
}
#endif // ExcludedCalvingPeriodParameters
