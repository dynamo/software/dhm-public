#include "FeedingPlanParameters.h"

// project
#include "../Tools/Tools.h"
#include "LanguageDictionary.h"
#include "FunctionalConstants.h"
#include "TechnicalConstants.h"
#include "FeedingPlanParametersConstants.h"
#include "HeadParameters.h"

BOOST_CLASS_EXPORT(ExchangeInfoStructures::Parameters::FeedingPlanParameters) // Recommended (mandatory if virtual serialization)

namespace ExchangeInfoStructures
{
namespace Parameters
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
            ar & BOOST_SERIALIZATION_NVP(calfMilkQuantity); \
            ar & BOOST_SERIALIZATION_NVP(calfMilkType); \
            ar & BOOST_SERIALIZATION_NVP(weaningAge); \
            ar & BOOST_SERIALIZATION_NVP(unweanedFemaleCalfConcentrateMixture); \
            ar & BOOST_SERIALIZATION_NVP(weanedFemaleCalfForageMixture); \
            ar & BOOST_SERIALIZATION_NVP(weanedFemaleCalfConcentrateMixture); \
            ar & BOOST_SERIALIZATION_NVP(weanedFemaleCalfMineralVitamins); \
            ar & BOOST_SERIALIZATION_NVP(youngUnbredHeiferForageMixture); \
            ar & BOOST_SERIALIZATION_NVP(youngUnbredHeiferConcentrateMixture); \
            ar & BOOST_SERIALIZATION_NVP(youngUnbredHeiferMineralVitamins); \
            ar & BOOST_SERIALIZATION_NVP(oldUnbredHeiferForageMixture); \
            ar & BOOST_SERIALIZATION_NVP(oldUnbredHeiferConcentrateMixture); \
            ar & BOOST_SERIALIZATION_NVP(oldUnbredHeiferMineralVitamins); \
            ar & BOOST_SERIALIZATION_NVP(bredHeiferForageMixture); \
            ar & BOOST_SERIALIZATION_NVP(bredHeiferConcentrateMixture); \
            ar & BOOST_SERIALIZATION_NVP(bredHeiferMineralVitamins); \
            ar & BOOST_SERIALIZATION_NVP(RBE_MilkProduction); \
            ar & BOOST_SERIALIZATION_NVP(lactationForageMixture); \
            ar & BOOST_SERIALIZATION_NVP(lactationConcentrateMixture); \
            ar & BOOST_SERIALIZATION_NVP(lactationPeriodMineralVitamins); \
            ar & BOOST_SERIALIZATION_NVP(gainForOneConcentrateKilo); \
            ar & BOOST_SERIALIZATION_NVP(productConcentrateMixture); \
    
    IMPLEMENT_SERIALIZE_STD_METHODS(FeedingPlanParameters);


    FeedingPlanParameters::FeedingPlanParameters()
    {
        // Calves
        calfMilkQuantity = FunctionalConstants::Feeding::DEFAULT_DAY_MILK_CALF_QUANTITY;
        calfMilkType = FunctionalConstants::Feeding::DEFAULT_CALF_MILK_FEEDING_TYPE;
        weaningAge = FunctionalConstants::Feeding::DEFAULT_WEANING_AGE;
        unweanedFemaleCalfConcentrateMixture.quantity = FunctionalConstants::Feeding::DEFAULT_UNWEANED_FEMALE_CALF_CONCENTRATE_FEEDING_QUANTITY;
        unweanedFemaleCalfConcentrateMixture.typeRatios = FunctionalConstants::Feeding::DEFAULT_UNWEANED_FEMALE_CALF_CONCENTRATE_FEEDING_TYPE;
        
        // Weaned calves
        weanedFemaleCalfForageMixture.quantity = FunctionalConstants::Feeding::DEFAULT_WEANED_FEMALE_CALF_FORAGE_FEEDING_QUANTITY;
        weanedFemaleCalfForageMixture.typeRatios = FunctionalConstants::Feeding::DEFAULT_WEANED_FEMALE_CALF_FORAGE_FEEDING_TYPE;
        weanedFemaleCalfConcentrateMixture.quantity = FunctionalConstants::Feeding::DEFAULT_WEANED_FEMALE_CALF_CONCENTRATE_FEDDING_QUANTITY;
        weanedFemaleCalfConcentrateMixture.typeRatios = FunctionalConstants::Feeding::DEFAULT_WEANED_FEMALE_CALF_CONCENTRATE_FEEDING_TYPE;
        weanedFemaleCalfMineralVitamins = FunctionalConstants::Feeding::DEFAULT_WEANED_FEMALE_CALF_MINERAL_VITAMIN_FEDDING_QUANTITY;
       
        // Heifers :
        
        // Location depending
        for (unsigned int location = FunctionalEnumerations::Population::Location::stabulation; location <= FunctionalEnumerations::Population::Location::fullPasture; location++) // no halfStabulationPasture for heifers
        {
            // Young heifers
            ForageMixtureCharacteristicParameters fmc;
            fmc.quantity = FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_FORAGE_FEDDING_QUANTITY.find((FunctionalEnumerations::Population::Location)location)->second;
            fmc.typeRatios = FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_FORAGE_FEDDING_TYPE.find((FunctionalEnumerations::Population::Location)location)->second;
            youngUnbredHeiferForageMixture[(FunctionalEnumerations::Population::Location)location] = fmc;
            
            ConcentrateMixtureCharacteristicParameters cmc;
            cmc.quantity = FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_CONCENTRATE_FEDDING_QUANTITY.find((FunctionalEnumerations::Population::Location)location)->second;
            cmc.typeRatios = FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_CONCENTRATE_FEDDING_TYPE.find((FunctionalEnumerations::Population::Location)location)->second;
            youngUnbredHeiferConcentrateMixture[(FunctionalEnumerations::Population::Location)location] = cmc;

            // Old heifers
            fmc.quantity = FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_FORAGE_FEDDING_FEDDING_QUANTITY.find((FunctionalEnumerations::Population::Location)location)->second;
            fmc.typeRatios = FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_FORAGE_FEDDING_TYPE.find((FunctionalEnumerations::Population::Location)location)->second;
            oldUnbredHeiferForageMixture[(FunctionalEnumerations::Population::Location)location] = fmc;
            
            cmc.quantity = FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_CONCENTRATE_FEDDING_QUANTITY.find((FunctionalEnumerations::Population::Location)location)->second;
            cmc.typeRatios = FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_CONCENTRATE_FEDDING_TYPE.find((FunctionalEnumerations::Population::Location)location)->second;
            oldUnbredHeiferConcentrateMixture[(FunctionalEnumerations::Population::Location)location] = cmc;
            
            // Bred heifers
            fmc.quantity = FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_FORAGE_FEDDING_QUANTITY.find((FunctionalEnumerations::Population::Location)location)->second;
            fmc.typeRatios = FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_FORAGE_FEDDING_TYPE.find((FunctionalEnumerations::Population::Location)location)->second;
            bredHeiferForageMixture[(FunctionalEnumerations::Population::Location)location] = fmc;
            
            cmc.quantity = FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_CONCENTRATE_FEDDING_QUANTITY.find((FunctionalEnumerations::Population::Location)location)->second;
            cmc.typeRatios = FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_CONCENTRATE_FEDDING_TYPE.find((FunctionalEnumerations::Population::Location)location)->second;
            bredHeiferConcentrateMixture[(FunctionalEnumerations::Population::Location)location] = cmc;
        }
        youngUnbredHeiferMineralVitamins = FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_MINERAL_VITAMIN_FEDDING_QUANTITY;  
        oldUnbredHeiferMineralVitamins = FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_MINERAL_VITAMIN_FEDDING_QUANTITY;
        bredHeiferMineralVitamins = FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_MINERAL_VITAMIN_FEDDING_QUANTITY;
        
        // Milking cows :
        
        RBE_MilkProduction = FunctionalConstants::Feeding::DEFAULT_RBE_MILK_PRODUCTION;

        // location depending
        for (unsigned int location = FunctionalEnumerations::Population::Location::stabulation; location <= FunctionalEnumerations::Population::Location::halfStabulationPasture; location++)
        {
            // Young heifers
            ForageMixtureCharacteristicParameters fmc;
            fmc.quantity = FunctionalConstants::Feeding::DEFAULT_MILKING_COW_FORAGE_FEDDING_QUANTITY.find((FunctionalEnumerations::Population::Location)location)->second;
            fmc.typeRatios = FunctionalConstants::Feeding::DEFAULT_MILKING_COW_FORAGE_FEDDING_TYPE.find((FunctionalEnumerations::Population::Location)location)->second;
            lactationForageMixture[(FunctionalEnumerations::Population::Location)location] = fmc;
            
            ConcentrateMixtureCharacteristicParameters cmc;
            cmc.quantity = FunctionalConstants::Feeding::DEFAULT_MILKING_COW_CONCENTRATE_FEDDING_QUANTITY.find((FunctionalEnumerations::Population::Location)location)->second;
            cmc.typeRatios = FunctionalConstants::Feeding::DEFAULT_MILKING_COW_CONCENTRATE_FEDDING_TYPE.find((FunctionalEnumerations::Population::Location)location)->second;
            lactationConcentrateMixture[(FunctionalEnumerations::Population::Location)location] = cmc;
        }
        lactationPeriodMineralVitamins = FunctionalConstants::Feeding::DEFAULT_MILKING_COW_MINERAL_VITAMIN_FEDDING_QUANTITY;
        
        // Product concentrate mixture
        gainForOneConcentrateKilo = FunctionalConstants::Feeding::DEFAULT_GAIN_FOR_ONE_KILO_CONCENTRATE;
        productConcentrateMixture.quantity = 0.0f; // Depending on the milk product
        productConcentrateMixture.typeRatios = FunctionalConstants::Feeding::DEFAULT_PRODUCT_CONCENTRATE_TYPE;
    }
    
    FeedingPlanParameters::~FeedingPlanParameters()
    {
        
    }

    // Constructeur de copie   
    FeedingPlanParameters::FeedingPlanParameters(const FeedingPlanParameters &obj)
    {
        calfMilkQuantity = obj.calfMilkQuantity;
        calfMilkType = obj.calfMilkType;
        weaningAge = obj.weaningAge;
        unweanedFemaleCalfConcentrateMixture = obj.unweanedFemaleCalfConcentrateMixture;
        weanedFemaleCalfForageMixture = obj.weanedFemaleCalfForageMixture;
        weanedFemaleCalfConcentrateMixture = obj.weanedFemaleCalfConcentrateMixture;
        weanedFemaleCalfMineralVitamins = obj.weanedFemaleCalfMineralVitamins;
        youngUnbredHeiferForageMixture = obj.youngUnbredHeiferForageMixture;
        youngUnbredHeiferConcentrateMixture = obj.youngUnbredHeiferConcentrateMixture;
        youngUnbredHeiferMineralVitamins = obj.youngUnbredHeiferMineralVitamins;
        oldUnbredHeiferForageMixture = obj.oldUnbredHeiferForageMixture;
        oldUnbredHeiferConcentrateMixture = obj.oldUnbredHeiferConcentrateMixture;
        oldUnbredHeiferMineralVitamins = obj.oldUnbredHeiferMineralVitamins;
        bredHeiferForageMixture = obj.bredHeiferForageMixture;
        bredHeiferConcentrateMixture = obj.bredHeiferConcentrateMixture;
        bredHeiferMineralVitamins = obj.bredHeiferMineralVitamins;
        RBE_MilkProduction = obj.RBE_MilkProduction;
        lactationForageMixture = obj.lactationForageMixture;
        lactationConcentrateMixture = obj.lactationConcentrateMixture;
        lactationPeriodMineralVitamins = obj.lactationPeriodMineralVitamins;
        gainForOneConcentrateKilo = obj.gainForOneConcentrateKilo;
        productConcentrateMixture = obj.productConcentrateMixture;
    }
    
    bool FeedingPlanParameters::autoControl(std::string &message)
    {
        message = "";
        bool res = true;
        if (res)
        {
            res = unweanedFemaleCalfConcentrateMixture.autoControl(message);
            if (not res) message += " : unweanedFemaleCalfConcentrateMixture";
        }
        if (res)
        {
            res = weanedFemaleCalfForageMixture.autoControl(message);
            if (not res) message += " : weanedFemaleCalfForageMixture";
        }
        if (res)
        {
            res = weanedFemaleCalfConcentrateMixture.autoControl(message);
            if (not res) message += " : weanedFemaleCalfConcentrateMixture";
        }
        if (res) for (auto mix : youngUnbredHeiferForageMixture)
        {
            res = (mix.second).autoControl(message);
            if (not res)
            {
                message += " : youngUnbredHeiferForageMixture";
                break;
            }
        }
        if (res) for (auto mix : youngUnbredHeiferConcentrateMixture)
        {
            res = (mix.second).autoControl(message);
            if (not res)
            {
                message += " : youngUnbredHeiferConcentrateMixture";
                break;
            }
        }
        if (res) for (auto mix : oldUnbredHeiferForageMixture)
        {
            res = (mix.second).autoControl(message);
            if (not res)
            {
                message += " : oldUnbredHeiferForageMixture";
                break;
            }
        }
        if (res) for (auto mix : oldUnbredHeiferConcentrateMixture)
        {
            res = (mix.second).autoControl(message);
            if (not res)
            {
                message += " : oldUnbredHeiferConcentrateMixture";
                break;
            }
        }
        if (res) for (auto mix : bredHeiferForageMixture)
        {
            res = (mix.second).autoControl(message);
            if (not res)
            {
                message += " : bredHeiferForageMixture";
                break;
            }
        }
        if (res) for (auto mix : bredHeiferConcentrateMixture)
        {
            res = (mix.second).autoControl(message);
            if (not res)
            {
                message += " : bredHeiferConcentrateMixture";
                break;
            }
        }
        if (res) for (auto mix : lactationForageMixture)
        {
            res = (mix.second).autoControl(message);
            if (not res)
            {
                message += " : lactationForageMixture";
                break;
            }
        }
        if (res) for (auto mix : lactationConcentrateMixture)
        {
            res = (mix.second).autoControl(message);
            if (not res)
            {
                message += " : lactationConcentrateMixture";
                break;
            }
        }
        if (res)
        {
            res = productConcentrateMixture.autoControl(message);
            if (not res) message += " : productConcentrateMixture";
        }
        return res;
    } /* End of method AutoControl */    
        
    bool FeedingPlanParameters::setImportedValue(const std::string &key, std::string &content)
    {
        
        if (key == CALF_MILK_QUANTITY_PARAM_KEY) {calfMilkQuantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == CALF_MILK_TYPE_PARAM_KEY) {calfMilkType = (FunctionalEnumerations::Feeding::CalfMilkType)std::atoi(content.c_str()); return true;} 
        if (key == WEANING_AGE_PARAM_KEY) {weaningAge = std::atoi(content.c_str()); return true;} 
        if (key == UNWEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_QUANTITY_PARAM_KEY) {unweanedFemaleCalfConcentrateMixture.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == UNWEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_RAPESEED_PARAM_KEY) {unweanedFemaleCalfConcentrateMixture.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == UNWEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_SOJA_PARAM_KEY) {unweanedFemaleCalfConcentrateMixture.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == UNWEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_BARLEY_PARAM_KEY) {unweanedFemaleCalfConcentrateMixture.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == WEANED_FEMALE_CALF_FORAGE_MIXTURE_QUANTITY_PARAM_KEY) {weanedFemaleCalfForageMixture.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == WEANED_FEMALE_CALF_FORAGE_MIXTURE_TYPE_MAIZEPLANT_PARAM_KEY) {weanedFemaleCalfForageMixture.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == WEANED_FEMALE_CALF_FORAGE_MIXTURE_TYPE_GRASS_PARAM_KEY) {weanedFemaleCalfForageMixture.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == WEANED_FEMALE_CALF_FORAGE_MIXTURE_TYPE_HAY_PARAM_KEY) {weanedFemaleCalfForageMixture.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == WEANED_FEMALE_CALF_FORAGE_MIXTURE_TYPE_GRASS_SILAGE_PARAM_KEY) {weanedFemaleCalfForageMixture.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == WEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_QUANTITY_PARAM_KEY) {weanedFemaleCalfConcentrateMixture.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == WEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_RAPESEED_PARAM_KEY) {weanedFemaleCalfConcentrateMixture.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == WEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_SOJA_PARAM_KEY) {weanedFemaleCalfConcentrateMixture.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == WEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_BARLEY_PARAM_KEY) {weanedFemaleCalfConcentrateMixture.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == WEANED_FEMALE_CALF_MINERAL_VITAMINS_PARAM_KEY) {weanedFemaleCalfMineralVitamins = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY) {youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_MAIZEPLANT_PARAM_KEY) {youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_PARAM_KEY) {youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_HAY_PARAM_KEY) {youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_SILAGE_PARAM_KEY) {youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY) {youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY) {youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_PARAM_KEY) {youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_HAY_PARAM_KEY) {youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY) {youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY) {youngUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_RAPESEED_PARAM_KEY) {youngUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_SOJA_PARAM_KEY) {youngUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_BARLEY_PARAM_KEY) {youngUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY) {youngUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_RAPESEED_PARAM_KEY) {youngUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_SOJA_PARAM_KEY) {youngUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_BARLEY_PARAM_KEY) {youngUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_MINERAL_VITAMINS_STABULATION_PARAM_KEY) {youngUnbredHeiferMineralVitamins.find(FunctionalEnumerations::Population::Location::stabulation)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == YOUNG_UNBRED_HEIFER_MINERAL_VITAMINS_PASTURE_PARAM_KEY) {youngUnbredHeiferMineralVitamins.find(FunctionalEnumerations::Population::Location::fullPasture)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY) {oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_MAIZEPLANT_PARAM_KEY) {oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_PARAM_KEY) {oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_HAY_PARAM_KEY) {oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_SILAGE_PARAM_KEY) {oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY) {oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY) {oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_PARAM_KEY) {oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_HAY_PARAM_KEY) {oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY) {oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY) {oldUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_RAPESEED_PARAM_KEY) {oldUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_SOJA_PARAM_KEY) {oldUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_BARLEY_PARAM_KEY) {oldUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY) {oldUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_RAPESEED_PARAM_KEY) {oldUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_SOJA_PARAM_KEY) {oldUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_BARLEY_PARAM_KEY) {oldUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == OLD_UNBRED_HEIFER_MINERAL_VITAMINS_STABULATION_PARAM_KEY) {oldUnbredHeiferMineralVitamins.find(FunctionalEnumerations::Population::Location::stabulation)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == OLD_UNBRED_HEIFER_MINERAL_VITAMINS_PASTURE_PARAM_KEY) {oldUnbredHeiferMineralVitamins.find(FunctionalEnumerations::Population::Location::fullPasture)->second = atof(Tools::changeChar(content, ',', '.').c_str());  return true;} 
        if (key == BRED_HEIFER_FORAGE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY) {bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_MAIZEPLANT_PARAM_KEY) {bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_PARAM_KEY) {bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_HAY_PARAM_KEY) {bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_SILAGE_PARAM_KEY) {bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_FORAGE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY) {bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY) {bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_PARAM_KEY) {bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_HAY_PARAM_KEY) {bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY) {bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY) {bredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_RAPESEED_PARAM_KEY) {bredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_SOJA_PARAM_KEY) {bredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_BARLEY_PARAM_KEY) {bredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY) {bredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_RAPESEED_PARAM_KEY) {bredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_SOJA_PARAM_KEY) {bredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_BARLEY_PARAM_KEY) {bredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_MINERAL_VITAMINS_STABULATION_PARAM_KEY) {bredHeiferMineralVitamins.find(FunctionalEnumerations::Population::Location::stabulation)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == BRED_HEIFER_MINERAL_VITAMINS_PASTURE_PARAM_KEY) {bredHeiferMineralVitamins.find(FunctionalEnumerations::Population::Location::fullPasture)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == RBE_MILK_PRODUCTION_PARAM_KEY) {RBE_MilkProduction = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_FORAGE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY) {lactationForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_FORAGE_MIXTURE_STABULATION_TYPE_MAIZEPLANT_PARAM_KEY) {lactationForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_PARAM_KEY) {lactationForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == LACTATION_FORAGE_MIXTURE_STABULATION_TYPE_HAY_PARAM_KEY) {lactationForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_SILAGE_PARAM_KEY) {lactationForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_FORAGE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY) {lactationForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_FORAGE_MIXTURE_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY) {lactationForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_PARAM_KEY) {lactationForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == LACTATION_FORAGE_MIXTURE_PASTURE_TYPE_HAY_PARAM_KEY) {lactationForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == LACTATION_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY) {lactationForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_QUANTITY_PARAM_KEY) {lactationForageMixture.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY) {lactationForageMixture.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_GRASS_PARAM_KEY) {lactationForageMixture.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_HAY_PARAM_KEY) {lactationForageMixture.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY) {lactationForageMixture.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_CONCENTRATE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY) {lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_CONCENTRATE_MIXTURE_STABULATION_TYPE_RAPESEED_PARAM_KEY) {lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_CONCENTRATE_MIXTURE_STABULATION_TYPE_SOJA_PARAM_KEY) {lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == LACTATION_CONCENTRATE_MIXTURE_STABULATION_TYPE_BARLEY_PARAM_KEY) {lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == LACTATION_CONCENTRATE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY) {lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == LACTATION_CONCENTRATE_MIXTURE_PASTURE_TYPE_RAPESEED_PARAM_KEY) {lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_CONCENTRATE_MIXTURE_PASTURE_TYPE_BARLEY_PARAM_KEY) {lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_CONCENTRATE_MIXTURE_PASTURE_TYPE_SOJA_PARAM_KEY) {lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_CONCENTRATE_MIXTURE_HALF_STABULATION_PASTURE_QUANTITY_PARAM_KEY) {lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.quantity = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_CONCENTRATE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_RAPESEED_PARAM_KEY) {lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_CONCENTRATE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_SOJA_PARAM_KEY) {lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_CONCENTRATE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_BARLEY_PARAM_KEY) {lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_PERIOD_MINERAL_VITAMINS_STABULATION_PARAM_KEY) {lactationPeriodMineralVitamins.find(FunctionalEnumerations::Population::Location::stabulation)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == LACTATION_PERIOD_MINERAL_VITAMINS_PASTURE_PARAM_KEY) {lactationPeriodMineralVitamins.find(FunctionalEnumerations::Population::Location::fullPasture)->second = atof(Tools::changeChar(content, ',', '.').c_str());  return true;} 
        if (key == LACTATION_PERIOD_MINERAL_VITAMINS_HALF_STABULATION_PASTURE_PARAM_KEY) {lactationPeriodMineralVitamins.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == GAIN_FOR_ONE_CONCENTRATE_KILO_PARAM_KEY) {gainForOneConcentrateKilo = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == PRODUCT_CONCENTRATE_MIXTURE_TYPE_RAPESEED_PARAM_KEY) {productConcentrateMixture.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == PRODUCT_CONCENTRATE_MIXTURE_TYPE_SOJA_PARAM_KEY) {productConcentrateMixture.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == PRODUCT_CONCENTRATE_MIXTURE_TYPE_BARLEY_PARAM_KEY) {productConcentrateMixture.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        
        return false; // Not here
    }

    bool FeedingPlanParameters::getValueToExport(const std::string &key, std::string &content)
    {
        GET_FLOAT_VALUE_TO_IMPORT(CALF_MILK_QUANTITY_PARAM_KEY, calfMilkQuantity) 
        GET_INTEGER_VALUE_TO_IMPORT(CALF_MILK_TYPE_PARAM_KEY, calfMilkType)
        GET_INTEGER_VALUE_TO_IMPORT(WEANING_AGE_PARAM_KEY, weaningAge)
        GET_FLOAT_VALUE_TO_IMPORT(UNWEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_QUANTITY_PARAM_KEY, unweanedFemaleCalfConcentrateMixture.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(UNWEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_RAPESEED_PARAM_KEY, unweanedFemaleCalfConcentrateMixture.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second)
        GET_FLOAT_VALUE_TO_IMPORT(UNWEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_SOJA_PARAM_KEY, unweanedFemaleCalfConcentrateMixture.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second)
        GET_FLOAT_VALUE_TO_IMPORT(UNWEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_BARLEY_PARAM_KEY, unweanedFemaleCalfConcentrateMixture.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second)
        GET_FLOAT_VALUE_TO_IMPORT(WEANED_FEMALE_CALF_FORAGE_MIXTURE_QUANTITY_PARAM_KEY, weanedFemaleCalfForageMixture.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(WEANED_FEMALE_CALF_FORAGE_MIXTURE_TYPE_MAIZEPLANT_PARAM_KEY, weanedFemaleCalfForageMixture.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second)
        GET_FLOAT_VALUE_TO_IMPORT(WEANED_FEMALE_CALF_FORAGE_MIXTURE_TYPE_GRASS_PARAM_KEY, weanedFemaleCalfForageMixture.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second)
        GET_FLOAT_VALUE_TO_IMPORT(WEANED_FEMALE_CALF_FORAGE_MIXTURE_TYPE_HAY_PARAM_KEY, weanedFemaleCalfForageMixture.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second)
        GET_FLOAT_VALUE_TO_IMPORT(WEANED_FEMALE_CALF_FORAGE_MIXTURE_TYPE_GRASS_SILAGE_PARAM_KEY, weanedFemaleCalfForageMixture.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second)
        GET_FLOAT_VALUE_TO_IMPORT(WEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_QUANTITY_PARAM_KEY, weanedFemaleCalfConcentrateMixture.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(WEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_RAPESEED_PARAM_KEY, weanedFemaleCalfConcentrateMixture.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second)
        GET_FLOAT_VALUE_TO_IMPORT(WEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_SOJA_PARAM_KEY, weanedFemaleCalfConcentrateMixture.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second)
        GET_FLOAT_VALUE_TO_IMPORT(WEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_BARLEY_PARAM_KEY, weanedFemaleCalfConcentrateMixture.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second)
        GET_FLOAT_VALUE_TO_IMPORT(WEANED_FEMALE_CALF_MINERAL_VITAMINS_PARAM_KEY, weanedFemaleCalfMineralVitamins)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY, youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_MAIZEPLANT_PARAM_KEY, youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_PARAM_KEY, youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_HAY_PARAM_KEY, youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_SILAGE_PARAM_KEY, youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY, youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY, youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_PARAM_KEY, youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_HAY_PARAM_KEY, youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY, youngUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY, youngUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_RAPESEED_PARAM_KEY, youngUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_SOJA_PARAM_KEY, youngUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_BARLEY_PARAM_KEY, youngUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY, youngUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_RAPESEED_PARAM_KEY, youngUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_SOJA_PARAM_KEY, youngUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_BARLEY_PARAM_KEY, youngUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_MINERAL_VITAMINS_STABULATION_PARAM_KEY, youngUnbredHeiferMineralVitamins.find(FunctionalEnumerations::Population::Location::stabulation)->second)
        GET_FLOAT_VALUE_TO_IMPORT(YOUNG_UNBRED_HEIFER_MINERAL_VITAMINS_PASTURE_PARAM_KEY, youngUnbredHeiferMineralVitamins.find(FunctionalEnumerations::Population::Location::fullPasture)->second)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY, oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_MAIZEPLANT_PARAM_KEY, oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_PARAM_KEY, oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_HAY_PARAM_KEY, oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_SILAGE_PARAM_KEY, oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY, oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY, oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_PARAM_KEY, oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_HAY_PARAM_KEY, oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second) 
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY, oldUnbredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY, oldUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_RAPESEED_PARAM_KEY, oldUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_SOJA_PARAM_KEY, oldUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_BARLEY_PARAM_KEY, oldUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY, oldUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_RAPESEED_PARAM_KEY, oldUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_SOJA_PARAM_KEY, oldUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_BARLEY_PARAM_KEY, oldUnbredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_MINERAL_VITAMINS_STABULATION_PARAM_KEY, oldUnbredHeiferMineralVitamins.find(FunctionalEnumerations::Population::Location::stabulation)->second)
        GET_FLOAT_VALUE_TO_IMPORT(OLD_UNBRED_HEIFER_MINERAL_VITAMINS_PASTURE_PARAM_KEY, oldUnbredHeiferMineralVitamins.find(FunctionalEnumerations::Population::Location::fullPasture)->second)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_FORAGE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY, bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_MAIZEPLANT_PARAM_KEY, bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_PARAM_KEY, bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_HAY_PARAM_KEY, bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_SILAGE_PARAM_KEY, bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_FORAGE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY, bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY, bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_PARAM_KEY, bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_HAY_PARAM_KEY, bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY, bredHeiferForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY, bredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_RAPESEED_PARAM_KEY, bredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_SOJA_PARAM_KEY, bredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_BARLEY_PARAM_KEY, bredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY, bredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_RAPESEED_PARAM_KEY, bredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_SOJA_PARAM_KEY, bredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second) 
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_BARLEY_PARAM_KEY, bredHeiferConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_MINERAL_VITAMINS_STABULATION_PARAM_KEY, bredHeiferMineralVitamins.find(FunctionalEnumerations::Population::Location::stabulation)->second)
        GET_FLOAT_VALUE_TO_IMPORT(BRED_HEIFER_MINERAL_VITAMINS_PASTURE_PARAM_KEY, bredHeiferMineralVitamins.find(FunctionalEnumerations::Population::Location::fullPasture)->second)
        GET_FLOAT_VALUE_TO_IMPORT(RBE_MILK_PRODUCTION_PARAM_KEY, RBE_MilkProduction) 
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_FORAGE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY, lactationForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_FORAGE_MIXTURE_STABULATION_TYPE_MAIZEPLANT_PARAM_KEY, lactationForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_PARAM_KEY, lactationForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_FORAGE_MIXTURE_STABULATION_TYPE_HAY_PARAM_KEY, lactationForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_SILAGE_PARAM_KEY, lactationForageMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_FORAGE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY, lactationForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_FORAGE_MIXTURE_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY, lactationForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_PARAM_KEY, lactationForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_FORAGE_MIXTURE_PASTURE_TYPE_HAY_PARAM_KEY, lactationForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY, lactationForageMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_QUANTITY_PARAM_KEY, lactationForageMixture.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY, lactationForageMixture.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_GRASS_PARAM_KEY, lactationForageMixture.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grass)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_HAY_PARAM_KEY, lactationForageMixture.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::hay)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY, lactationForageMixture.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_CONCENTRATE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY, lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_CONCENTRATE_MIXTURE_STABULATION_TYPE_RAPESEED_PARAM_KEY, lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_CONCENTRATE_MIXTURE_STABULATION_TYPE_SOJA_PARAM_KEY, lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_CONCENTRATE_MIXTURE_STABULATION_TYPE_BARLEY_PARAM_KEY, lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::stabulation)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_CONCENTRATE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY, lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_CONCENTRATE_MIXTURE_PASTURE_TYPE_RAPESEED_PARAM_KEY, lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_CONCENTRATE_MIXTURE_PASTURE_TYPE_BARLEY_PARAM_KEY, lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_CONCENTRATE_MIXTURE_PASTURE_TYPE_SOJA_PARAM_KEY, lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::fullPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_CONCENTRATE_MIXTURE_HALF_STABULATION_PASTURE_QUANTITY_PARAM_KEY, lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.quantity)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_CONCENTRATE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_RAPESEED_PARAM_KEY, lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_CONCENTRATE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_SOJA_PARAM_KEY, lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_CONCENTRATE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_BARLEY_PARAM_KEY, lactationConcentrateMixture.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_PERIOD_MINERAL_VITAMINS_STABULATION_PARAM_KEY, lactationPeriodMineralVitamins.find(FunctionalEnumerations::Population::Location::stabulation)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_PERIOD_MINERAL_VITAMINS_PASTURE_PARAM_KEY, lactationPeriodMineralVitamins.find(FunctionalEnumerations::Population::Location::fullPasture)->second)
        GET_FLOAT_VALUE_TO_IMPORT(LACTATION_PERIOD_MINERAL_VITAMINS_HALF_STABULATION_PASTURE_PARAM_KEY, lactationPeriodMineralVitamins.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second)
        GET_FLOAT_VALUE_TO_IMPORT(GAIN_FOR_ONE_CONCENTRATE_KILO_PARAM_KEY, gainForOneConcentrateKilo)
        GET_FLOAT_VALUE_TO_IMPORT(PRODUCT_CONCENTRATE_MIXTURE_TYPE_RAPESEED_PARAM_KEY, productConcentrateMixture.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second)
        GET_FLOAT_VALUE_TO_IMPORT(PRODUCT_CONCENTRATE_MIXTURE_TYPE_SOJA_PARAM_KEY, productConcentrateMixture.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second)
        GET_FLOAT_VALUE_TO_IMPORT(PRODUCT_CONCENTRATE_MIXTURE_TYPE_BARLEY_PARAM_KEY, productConcentrateMixture.typeRatios.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second)

        return false; // Not here
    }    
    
#ifdef _LOG
    void FeedingPlanParameters::getDefaultValues(std::vector<std::string> &vLines)
    {
        std::string line = "";   
        
        line = CALF_MILK_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_DAY_MILK_CALF_QUANTITY); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = CALF_MILK_TYPE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_CALF_MILK_FEEDING_TYPE); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = WEANING_AGE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_WEANING_AGE); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = UNWEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_UNWEANED_FEMALE_CALF_CONCENTRATE_FEEDING_QUANTITY); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = UNWEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_RAPESEED_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_UNWEANED_FEMALE_CALF_CONCENTRATE_FEEDING_TYPE.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = UNWEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_SOJA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_UNWEANED_FEMALE_CALF_CONCENTRATE_FEEDING_TYPE.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = UNWEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_BARLEY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_UNWEANED_FEMALE_CALF_CONCENTRATE_FEEDING_TYPE.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = WEANED_FEMALE_CALF_FORAGE_MIXTURE_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_WEANED_FEMALE_CALF_FORAGE_FEEDING_QUANTITY); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = WEANED_FEMALE_CALF_FORAGE_MIXTURE_TYPE_MAIZEPLANT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_WEANED_FEMALE_CALF_FORAGE_FEEDING_TYPE.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = WEANED_FEMALE_CALF_FORAGE_MIXTURE_TYPE_GRASS_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_WEANED_FEMALE_CALF_FORAGE_FEEDING_TYPE.find(FunctionalEnumerations::Feeding::ForageType::grass)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = WEANED_FEMALE_CALF_FORAGE_MIXTURE_TYPE_HAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_WEANED_FEMALE_CALF_FORAGE_FEEDING_TYPE.find(FunctionalEnumerations::Feeding::ForageType::hay)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = WEANED_FEMALE_CALF_FORAGE_MIXTURE_TYPE_GRASS_SILAGE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_WEANED_FEMALE_CALF_FORAGE_FEEDING_TYPE.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = WEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_WEANED_FEMALE_CALF_CONCENTRATE_FEDDING_QUANTITY); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = WEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_RAPESEED_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_WEANED_FEMALE_CALF_CONCENTRATE_FEEDING_TYPE.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = WEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_SOJA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_WEANED_FEMALE_CALF_CONCENTRATE_FEEDING_TYPE.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = WEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_BARLEY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_WEANED_FEMALE_CALF_CONCENTRATE_FEEDING_TYPE.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = WEANED_FEMALE_CALF_MINERAL_VITAMINS_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_WEANED_FEMALE_CALF_MINERAL_VITAMIN_FEDDING_QUANTITY); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_FORAGE_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::stabulation)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_MAIZEPLANT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ForageType::grass)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_HAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ForageType::hay)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_SILAGE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_FORAGE_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::fullPasture)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::grass)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_HAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::hay)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_CONCENTRATE_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::stabulation)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_RAPESEED_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_SOJA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_BARLEY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_CONCENTRATE_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::fullPasture)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_RAPESEED_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_SOJA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_BARLEY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_MINERAL_VITAMINS_STABULATION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_MINERAL_VITAMIN_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::stabulation)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = YOUNG_UNBRED_HEIFER_MINERAL_VITAMINS_PASTURE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_YOUNG_HEIFER_MINERAL_VITAMIN_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::fullPasture)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_FORAGE_FEDDING_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::stabulation)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_MAIZEPLANT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ForageType::grass)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_HAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ForageType::hay)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_SILAGE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_FORAGE_FEDDING_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::fullPasture)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::grass)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_HAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::hay)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_CONCENTRATE_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::stabulation)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_RAPESEED_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_SOJA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_BARLEY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_CONCENTRATE_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::fullPasture)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_RAPESEED_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_SOJA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_BARLEY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_MINERAL_VITAMINS_STABULATION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_MINERAL_VITAMIN_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::stabulation)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = OLD_UNBRED_HEIFER_MINERAL_VITAMINS_PASTURE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_OLD_HEIFER_MINERAL_VITAMIN_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::fullPasture)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_FORAGE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_FORAGE_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::stabulation)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_MAIZEPLANT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ForageType::grass)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_HAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ForageType::hay)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_SILAGE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_FORAGE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_FORAGE_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::fullPasture)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::grass)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_HAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::hay)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_CONCENTRATE_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::stabulation)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_RAPESEED_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_SOJA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_BARLEY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_CONCENTRATE_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::fullPasture)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_RAPESEED_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_SOJA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_BARLEY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_MINERAL_VITAMINS_STABULATION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_MINERAL_VITAMIN_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::stabulation)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = BRED_HEIFER_MINERAL_VITAMINS_PASTURE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_BRED_HEIFER_MINERAL_VITAMIN_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::fullPasture)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = RBE_MILK_PRODUCTION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_RBE_MILK_PRODUCTION); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_FORAGE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_FORAGE_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::stabulation)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_FORAGE_MIXTURE_STABULATION_TYPE_MAIZEPLANT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ForageType::grass)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_FORAGE_MIXTURE_STABULATION_TYPE_HAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ForageType::hay)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_SILAGE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_FORAGE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_FORAGE_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::fullPasture)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_FORAGE_MIXTURE_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::grass)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_FORAGE_MIXTURE_PASTURE_TYPE_HAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::hay)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_FORAGE_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::maizePlant)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_GRASS_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::grass)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_HAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::hay)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_FORAGE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.find(FunctionalEnumerations::Feeding::ForageType::grassSilage)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_CONCENTRATE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_CONCENTRATE_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::stabulation)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_CONCENTRATE_MIXTURE_STABULATION_TYPE_RAPESEED_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_CONCENTRATE_MIXTURE_STABULATION_TYPE_SOJA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_CONCENTRATE_MIXTURE_STABULATION_TYPE_BARLEY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::stabulation)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_CONCENTRATE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_CONCENTRATE_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::fullPasture)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_CONCENTRATE_MIXTURE_PASTURE_TYPE_RAPESEED_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_CONCENTRATE_MIXTURE_PASTURE_TYPE_BARLEY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_CONCENTRATE_MIXTURE_PASTURE_TYPE_SOJA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::fullPasture)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_CONCENTRATE_MIXTURE_HALF_STABULATION_PASTURE_QUANTITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_CONCENTRATE_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_CONCENTRATE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_RAPESEED_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_CONCENTRATE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_SOJA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_CONCENTRATE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_BARLEY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_CONCENTRATE_FEDDING_TYPE.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_PERIOD_MINERAL_VITAMINS_STABULATION_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_MINERAL_VITAMIN_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::stabulation)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_PERIOD_MINERAL_VITAMINS_PASTURE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_MINERAL_VITAMIN_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::fullPasture)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LACTATION_PERIOD_MINERAL_VITAMINS_HALF_STABULATION_PASTURE_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_MILKING_COW_MINERAL_VITAMIN_FEDDING_QUANTITY.find(FunctionalEnumerations::Population::Location::halfStabulationPasture)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = GAIN_FOR_ONE_CONCENTRATE_KILO_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_GAIN_FOR_ONE_KILO_CONCENTRATE); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = PRODUCT_CONCENTRATE_MIXTURE_TYPE_RAPESEED_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_PRODUCT_CONCENTRATE_TYPE.find(FunctionalEnumerations::Feeding::ConcentrateType::rapeseed)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = PRODUCT_CONCENTRATE_MIXTURE_TYPE_SOJA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_PRODUCT_CONCENTRATE_TYPE.find(FunctionalEnumerations::Feeding::ConcentrateType::soja)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = PRODUCT_CONCENTRATE_MIXTURE_TYPE_BARLEY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Feeding::DEFAULT_PRODUCT_CONCENTRATE_TYPE.find(FunctionalEnumerations::Feeding::ConcentrateType::barley)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
    }
#endif // _LOG
    
} /* End of namespace Parameters */
}