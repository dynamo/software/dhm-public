///////////////////////////////////////
//                                   //
// File : FeedingPlanParameters.h    //
//                                   //
// Author : Philippe Gontier         //
//                                   //
// Date : 01/10/2015                 //
//                                   //
// Feeding settings                  //
//                                   //
///////////////////////////////////////

#ifndef Parameters_FeedingPlanParameters_h
#define Parameters_FeedingPlanParameters_h

// Standard
#include <map>

// Project
#include "../Tools/Serialization.h"

#include "FunctionalEnumerations.h"
#include "MixtureCharacteristicParameters.h"

namespace ExchangeInfoStructures
{
namespace Parameters
{
    /// This structure make it possible to configure the diets according to the situation and the age of the animals
    /// See the § 3.2.1.2 "Feeding plan" of the "Functional description and terms of use" documentation.
    struct  FeedingPlanParameters
    {        
        
    public :

    // Calves (§ 2.2.6.2.1.1) :
    // ------------------------
        
        /// Calf milk quantity (see § 2.2.6.2.1.1 of the "Functional description and terms of use" documentation)
        float calfMilkQuantity; // Quantity of milk to fed unweaned calves
        
        /// Calf milk type (see § 2.2.6.2.1.1 of the "Functional description and terms of use" documentation)
        FunctionalEnumerations::Feeding::CalfMilkType calfMilkType; // Type of milk to fed unweaned calves
        
        /// Weaning age (see § 2.2.6.2.1.1 of the "Functional description and terms of use" documentation)
        unsigned int weaningAge;
        
        /// Unweaned female calves concentrated (see § 2.2.6.2.1.1 of the "Functional description and terms of use" documentation)
        ConcentrateMixtureCharacteristicParameters unweanedFemaleCalfConcentrateMixture;
        
    // Weaned female calves (§ 2.2.6.2.1.2) :
    // --------------------------------------
        
        /// Forage mixture for weaned female calves (see § 2.2.6.2.1.2 of the "Functional description and terms of use" documentation)
        ForageMixtureCharacteristicParameters weanedFemaleCalfForageMixture;
        
        /// Concentrate mixture for weaned female calves (see § 2.2.6.2.1.2 of the "Functional description and terms of use" documentation)
        ConcentrateMixtureCharacteristicParameters weanedFemaleCalfConcentrateMixture;
        
        /// Calf mineral and vitamin quantities (see § 2.2.6.2.1.2 of the "Functional description and terms of use" documentation)        
        float weanedFemaleCalfMineralVitamins;
        
    // Heifers (§ 2.2.6.2.1.3) :
    // -------------------------
        
        /// Forage mixture for young unbred heifers (see § 2.2.6.2.1.3 of the "Functional description and terms of use" documentation)
        std::map<FunctionalEnumerations::Population::Location, ForageMixtureCharacteristicParameters> youngUnbredHeiferForageMixture;
        
        /// Concentrate mixture for young unbred heifers (see § 2.2.6.2.1.3 of the "Functional description and terms of use" documentation)
        std::map<FunctionalEnumerations::Population::Location, ConcentrateMixtureCharacteristicParameters> youngUnbredHeiferConcentrateMixture;
        
        /// Young unbred heifer mineral and vitamin quantities (see § 2.2.6.2.1.3 of the "Functional description and terms of use" documentation)        
        std::map<FunctionalEnumerations::Population::Location, float> youngUnbredHeiferMineralVitamins;
        
        
        /// Forage mixture for old unbred heifers (see § 2.2.6.2.1.3 of the "Functional description and terms of use" documentation)
        std::map<FunctionalEnumerations::Population::Location, ForageMixtureCharacteristicParameters> oldUnbredHeiferForageMixture;
        
        /// Concentrate mixture for old unbred heifers (see § 2.2.6.2.1.3 of the "Functional description and terms of use" documentation)
        std::map<FunctionalEnumerations::Population::Location, ConcentrateMixtureCharacteristicParameters> oldUnbredHeiferConcentrateMixture;
        
        /// Old unbred heifer mineral and vitamin quantities (see § 2.2.6.2.1.3 of the "Functional description and terms of use" documentation)        
        std::map<FunctionalEnumerations::Population::Location, float> oldUnbredHeiferMineralVitamins;
        
        
        /// Forage mixture for bred heifers (see § 2.2.6.2.1.3 of the "Functional description and terms of use" documentation)
        std::map<FunctionalEnumerations::Population::Location, ForageMixtureCharacteristicParameters> bredHeiferForageMixture;
        
        /// Concentrate mixture for bred heifers (see § 2.2.6.2.1.3 of the "Functional description and terms of use" documentation)
        std::map<FunctionalEnumerations::Population::Location, ConcentrateMixtureCharacteristicParameters> bredHeiferConcentrateMixture;
        
        /// Bred heifer mineral and vitamin quantities (see § 2.2.6.2.1.3 of the "Functional description and terms of use" documentation)        
        std::map<FunctionalEnumerations::Population::Location, float> bredHeiferMineralVitamins;
        
    // Milking cows (§ 2.2.6.2.1.4) :
    // ------------------------------
        
        /// Balance basic feed during lactation period (see § 2.2.6.2.1.4 of the "Functional description and terms of use" documentation)
        float RBE_MilkProduction;
        
        /// Forage mixture during lactation period (see § 2.2.6.2.1.4 of the "Functional description and terms of use" documentation)
        std::map<FunctionalEnumerations::Population::Location, ForageMixtureCharacteristicParameters> lactationForageMixture;
        
        /// Concentrate mixture during lactation period (see § 2.2.6.2.1.4 of the "Functional description and terms of use" documentation)
        std::map<FunctionalEnumerations::Population::Location, ConcentrateMixtureCharacteristicParameters> lactationConcentrateMixture;
        
        /// Lactation period mineral and vitamin quantities (see § 2.2.6.2.1.4 of the "Functional description and terms of use" documentation)        
        std::map<FunctionalEnumerations::Population::Location, float> lactationPeriodMineralVitamins;
        
    // Product concentrate mixture (§ 2.2.6.2.2) :
    // -------------------------------------------
        
        /// Product gain for 1 kg of concentrate feed (see § 2.2.6.2.2 of the "Functional description and terms of use" documentation)
        float gainForOneConcentrateKilo;
        
        /// Product concentrate mixture (see § 2.2.6.2.2 of the "Functional description and terms of use" documentation)
        ConcentrateMixtureCharacteristicParameters productConcentrateMixture; // Product

    private:
        // serialization
        DECLARE_SERIALIZE_STD_METHODS;
        
    public:
        
        /// Value checking, errors in the std::string parameter reference
        bool autoControl (
                                    /// Reference std::string to put the error message (empty value if no error)
                                    std::string &message
                                    );
        FeedingPlanParameters();
        virtual ~FeedingPlanParameters();
        FeedingPlanParameters (const FeedingPlanParameters &obj);        //constructeur de copie   
#ifdef _LOG
        static void getDefaultValues(std::vector<std::string> &vLines);
#endif // _LOG
        bool setImportedValue(const std::string &key, std::string &content);
        bool getValueToExport(const std::string &key, std::string &content);
    };
} /* End of namespace ExchangeInfoStructures */
}
#endif // Parameters_FeedingPlanParameters_h
