/////////////////////////////////////////////
//                                         //
// File : FeedingPlanParametersConstants.h //
//                                         //
// Author : Philippe Gontier               //
//                                         //
// Date : 01/10/2015                       //
//                                         //
// Feeding settings                        //
//                                         //
/////////////////////////////////////////////

#ifndef Parameters_FeedingPlanParametersConstants_h
#define Parameters_FeedingPlanParametersConstants_h

// Standard
#include <string>

// Project

namespace ExchangeInfoStructures
{
namespace Parameters
{

    // Import/export key values
    static const std::string CALF_MILK_QUANTITY_PARAM_KEY = "calfMilkQuantity";
    static const std::string CALF_MILK_TYPE_PARAM_KEY = "calfMilkType";
    static const std::string WEANING_AGE_PARAM_KEY = "weaningAge";
    static const std::string UNWEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_QUANTITY_PARAM_KEY = "unweanedFemaleCalfConcentrateMixture_quantity";
    static const std::string UNWEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_RAPESEED_PARAM_KEY = "unweanedFemaleCalfConcentrateMixture_type_rapeseed";
    static const std::string UNWEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_SOJA_PARAM_KEY = "unweanedFemaleCalfConcentrateMixture_type_soja";
    static const std::string UNWEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_BARLEY_PARAM_KEY = "unweanedFemaleCalfConcentrateMixture_type_barley";
    static const std::string WEANED_FEMALE_CALF_FORAGE_MIXTURE_QUANTITY_PARAM_KEY = "weanedFemaleCalfForageMixture_quantity";
    static const std::string WEANED_FEMALE_CALF_FORAGE_MIXTURE_TYPE_MAIZEPLANT_PARAM_KEY = "weanedFemaleCalfForageMixture_type_maizePlant";
    static const std::string WEANED_FEMALE_CALF_FORAGE_MIXTURE_TYPE_GRASS_PARAM_KEY = "weanedFemaleCalfForageMixture_type_grass";
    static const std::string WEANED_FEMALE_CALF_FORAGE_MIXTURE_TYPE_HAY_PARAM_KEY = "weanedFemaleCalfForageMixture_type_hay";
    static const std::string WEANED_FEMALE_CALF_FORAGE_MIXTURE_TYPE_GRASS_SILAGE_PARAM_KEY = "weanedFemaleCalfForageMixture_type_grass_silage";
    static const std::string WEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_QUANTITY_PARAM_KEY = "weanedFemaleCalfConcentrateMixture_quantity";
    static const std::string WEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_RAPESEED_PARAM_KEY = "weanedFemaleCalfConcentrateMixture_type_rapeseed";
    static const std::string WEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_SOJA_PARAM_KEY = "weanedFemaleCalfConcentrateMixture_type_soja";
    static const std::string WEANED_FEMALE_CALF_CONCENTRATE_MIXTURE_TYPE_BARLEY_PARAM_KEY = "weanedFemaleCalfConcentrateMixture_type_barley";
    static const std::string WEANED_FEMALE_CALF_MINERAL_VITAMINS_PARAM_KEY = "weanedFemaleCalfMineralVitamins";
    static const std::string YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY = "youngUnbredHeiferForageMixture_stabulation_quantity";
    static const std::string YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_MAIZEPLANT_PARAM_KEY = "youngUnbredHeiferForageMixture_stabulation_type_maizePlant";
    static const std::string YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_PARAM_KEY = "youngUnbredHeiferForageMixture_stabulation_type_grass";
    static const std::string YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_HAY_PARAM_KEY = "youngUnbredHeiferForageMixture_stabulation_type_hay";
    static const std::string YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_SILAGE_PARAM_KEY = "youngUnbredHeiferForageMixture_stabulation_type_grass_silage";
    static const std::string YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY = "youngUnbredHeiferForageMixture_pasture_quantity";
    static const std::string YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY = "youngUnbredHeiferForageMixture_pasture_type_maizePlant";
    static const std::string YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_PARAM_KEY = "youngUnbredHeiferForageMixture_pasture_type_grass";
    static const std::string YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_HAY_PARAM_KEY = "youngUnbredHeiferForageMixture_pasture_type_hay";
    static const std::string YOUNG_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY = "youngUnbredHeiferForageMixture_pasture_type_grass_silage";
    static const std::string YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY = "youngUnbredHeiferConcentrateMixture_stabulation_quantity";
    static const std::string YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_RAPESEED_PARAM_KEY = "youngUnbredHeiferConcentrateMixture_stabulation_type_rapeseed";
    static const std::string YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_SOJA_PARAM_KEY = "youngUnbredHeiferConcentrateMixture_stabulation_type_soja";
    static const std::string YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_BARLEY_PARAM_KEY = "youngUnbredHeiferConcentrateMixture_stabulation_type_barley";
    static const std::string YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY = "youngUnbredHeiferConcentrateMixture_pasture_quantity";
    static const std::string YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_RAPESEED_PARAM_KEY = "youngUnbredHeiferConcentrateMixture_pasture_type_rapeseed";
    static const std::string YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_SOJA_PARAM_KEY = "youngUnbredHeiferConcentrateMixture_pasture_type_soja";
    static const std::string YOUNG_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_BARLEY_PARAM_KEY = "youngUnbredHeiferConcentrateMixture_pasture_type_barley";
    static const std::string YOUNG_UNBRED_HEIFER_MINERAL_VITAMINS_STABULATION_PARAM_KEY = "youngUnbredHeiferMineralVitamins_stabulation";
    static const std::string YOUNG_UNBRED_HEIFER_MINERAL_VITAMINS_PASTURE_PARAM_KEY = "youngUnbredHeiferMineralVitamins_pasture";
    static const std::string OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY = "oldUnbredHeiferForageMixture_stabulation_quantity";
    static const std::string OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_MAIZEPLANT_PARAM_KEY = "oldUnbredHeiferForageMixture_stabulation_type_maizePlant";
    static const std::string OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_PARAM_KEY = "oldUnbredHeiferForageMixture_stabulation_type_grass";
    static const std::string OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_HAY_PARAM_KEY = "oldUnbredHeiferForageMixture_stabulation_type_hay";
    static const std::string OLD_UNBRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_SILAGE_PARAM_KEY = "oldUnbredHeiferForageMixture_stabulation_type_grass_silage";
    static const std::string OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY = "oldUnbredHeiferForageMixture_pasture_quantity";
    static const std::string OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY = "oldUnbredHeiferForageMixture_pasture_type_maizePlant";
    static const std::string OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_PARAM_KEY = "oldUnbredHeiferForageMixture_pasture_type_grass";
    static const std::string OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_HAY_PARAM_KEY = "oldUnbredHeiferForageMixture_pasture_type_hay";
    static const std::string OLD_UNBRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY = "oldUnbredHeiferForageMixture_pasture_type_grass_silage";
    static const std::string OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY = "oldUnbredHeiferConcentrateMixture_stabulation_quantity";
    static const std::string OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_RAPESEED_PARAM_KEY = "oldUnbredHeiferConcentrateMixture_stabulation_type_rapeseed";
    static const std::string OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_SOJA_PARAM_KEY = "oldUnbredHeiferConcentrateMixture_stabulation_type_soja";
    static const std::string OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_BARLEY_PARAM_KEY = "oldUnbredHeiferConcentrateMixture_stabulation_type_barley";
    static const std::string OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY = "oldUnbredHeiferConcentrateMixture_pasture_quantity";
    static const std::string OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_RAPESEED_PARAM_KEY = "oldUnbredHeiferConcentrateMixture_pasture_type_rapeseed";
    static const std::string OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_SOJA_PARAM_KEY = "oldUnbredHeiferConcentrateMixture_pasture_type_soja";
    static const std::string OLD_UNBRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_BARLEY_PARAM_KEY = "oldUnbredHeiferConcentrateMixture_pasture_type_barley";
    static const std::string OLD_UNBRED_HEIFER_MINERAL_VITAMINS_STABULATION_PARAM_KEY = "oldUnbredHeiferMineralVitamins_stabulation";
    static const std::string OLD_UNBRED_HEIFER_MINERAL_VITAMINS_PASTURE_PARAM_KEY = "oldUnbredHeiferMineralVitamins_pasture";
    static const std::string BRED_HEIFER_FORAGE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY = "bredHeiferForageMixture_stabulation_quantity";
    static const std::string BRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_MAIZEPLANT_PARAM_KEY = "bredHeiferForageMixture_stabulation_type_maizePlant";
    static const std::string BRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_PARAM_KEY = "bredHeiferForageMixture_stabulation_type_grass";
    static const std::string BRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_HAY_PARAM_KEY = "bredHeiferForageMixture_stabulation_type_hay";
    static const std::string BRED_HEIFER_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_SILAGE_PARAM_KEY = "bredHeiferForageMixture_stabulation_type_grass_silage";
    static const std::string BRED_HEIFER_FORAGE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY = "bredHeiferForageMixture_pasture_quantity";
    static const std::string BRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY = "bredHeiferForageMixture_pasture_type_maizePlant";
    static const std::string BRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_PARAM_KEY = "bredHeiferForageMixture_pasture_type_grass";
    static const std::string BRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_HAY_PARAM_KEY = "bredHeiferForageMixture_pasture_type_hay";
    static const std::string BRED_HEIFER_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY = "bredHeiferForageMixture_pasture_type_grass_silage";
    static const std::string BRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY = "bredHeiferConcentrateMixture_stabulation_quantity";
    static const std::string BRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_RAPESEED_PARAM_KEY = "bredHeiferConcentrateMixture_stabulation_type_rapeseed";
    static const std::string BRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_SOJA_PARAM_KEY = "bredHeiferConcentrateMixture_stabulation_type_soja";
    static const std::string BRED_HEIFER_CONCENTRATE_MIXTURE_STABULATION_TYPE_BARLEY_PARAM_KEY = "bredHeiferConcentrateMixture_stabulation_type_barley";
    static const std::string BRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY = "bredHeiferConcentrateMixture_pasture_quantity";
    static const std::string BRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_RAPESEED_PARAM_KEY = "bredHeiferConcentrateMixture_pasture_type_rapeseed";
    static const std::string BRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_SOJA_PARAM_KEY = "bredHeiferConcentrateMixture_pasture_type_soja";
    static const std::string BRED_HEIFER_CONCENTRATE_MIXTURE_PASTURE_TYPE_BARLEY_PARAM_KEY = "bredHeiferConcentrateMixture_pasture_type_barley";
    static const std::string BRED_HEIFER_MINERAL_VITAMINS_STABULATION_PARAM_KEY = "bredHeiferMineralVitamins_stabulation";
    static const std::string BRED_HEIFER_MINERAL_VITAMINS_PASTURE_PARAM_KEY = "bredHeiferMineralVitamins_pasture";
    static const std::string RBE_MILK_PRODUCTION_PARAM_KEY = "RBE_MilkProduction"; 
    static const std::string LACTATION_FORAGE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY = "lactationForageMixture_stabulation_quantity";
    static const std::string LACTATION_FORAGE_MIXTURE_STABULATION_TYPE_MAIZEPLANT_PARAM_KEY = "lactationForageMixture_stabulation_type_maizePlant";
    static const std::string LACTATION_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_PARAM_KEY = "lactationForageMixture_stabulation_type_grass";
    static const std::string LACTATION_FORAGE_MIXTURE_STABULATION_TYPE_HAY_PARAM_KEY = "lactationForageMixture_stabulation_type_hay";
    static const std::string LACTATION_FORAGE_MIXTURE_STABULATION_TYPE_GRASS_SILAGE_PARAM_KEY = "lactationForageMixture_stabulation_type_grass_silage";
    static const std::string LACTATION_FORAGE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY = "lactationForageMixture_pasture_quantity";
    static const std::string LACTATION_FORAGE_MIXTURE_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY = "lactationForageMixture_pasture_type_maizePlant";
    static const std::string LACTATION_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_PARAM_KEY = "lactationForageMixture_pasture_type_grass";
    static const std::string LACTATION_FORAGE_MIXTURE_PASTURE_TYPE_HAY_PARAM_KEY = "lactationForageMixture_pasture_type_hay";
    static const std::string LACTATION_FORAGE_MIXTURE_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY = "lactationForageMixture_pasture_type_grass_silage";
    static const std::string LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_QUANTITY_PARAM_KEY = "lactationForageMixture_halfStabulationPasture_quantity";
    static const std::string LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_MAIZEPLANT_PARAM_KEY = "lactationForageMixture_halfStabulationPasture_type_maizePlant";
    static const std::string LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_GRASS_PARAM_KEY = "lactationForageMixture_halfStabulationPasture_type_grass";
    static const std::string LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_HAY_PARAM_KEY = "lactationForageMixture_halfStabulationPasture_type_hay";
    static const std::string LACTATION_FORAGE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_GRASS_SILAGE_PARAM_KEY = "lactationForageMixture_mixStabulationPasture_type_grass_silage";
    static const std::string LACTATION_CONCENTRATE_MIXTURE_STABULATION_QUANTITY_PARAM_KEY = "lactationConcentrateMixture_stabulation_quantity";
    static const std::string LACTATION_CONCENTRATE_MIXTURE_STABULATION_TYPE_RAPESEED_PARAM_KEY = "lactationConcentrateMixture_stabulation_type_rapeseed";
    static const std::string LACTATION_CONCENTRATE_MIXTURE_STABULATION_TYPE_SOJA_PARAM_KEY = "lactationConcentrateMixture_stabulation_type_soja";
    static const std::string LACTATION_CONCENTRATE_MIXTURE_STABULATION_TYPE_BARLEY_PARAM_KEY = "lactationConcentrateMixture_stabulation_type_barley";
    static const std::string LACTATION_CONCENTRATE_MIXTURE_PASTURE_QUANTITY_PARAM_KEY = "lactationConcentrateMixture_pasture_quantity";
    static const std::string LACTATION_CONCENTRATE_MIXTURE_PASTURE_TYPE_RAPESEED_PARAM_KEY = "lactationConcentrateMixture_pasture_type_rapeseed";
    static const std::string LACTATION_CONCENTRATE_MIXTURE_PASTURE_TYPE_SOJA_PARAM_KEY = "lactationConcentrateMixture_pasture_type_soja";
    static const std::string LACTATION_CONCENTRATE_MIXTURE_PASTURE_TYPE_BARLEY_PARAM_KEY = "lactationConcentrateMixture_pasture_type_barley";
    static const std::string LACTATION_CONCENTRATE_MIXTURE_HALF_STABULATION_PASTURE_QUANTITY_PARAM_KEY = "lactationConcentrateMixture_halfStabulationPasture_quantity";
    static const std::string LACTATION_CONCENTRATE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_RAPESEED_PARAM_KEY = "lactationConcentrateMixture_halfStabulationPasture_type_rapeseed";
    static const std::string LACTATION_CONCENTRATE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_SOJA_PARAM_KEY = "lactationConcentrateMixture_halfStabulationPasture_type_soja";
    static const std::string LACTATION_CONCENTRATE_MIXTURE_HALF_STABULATION_PASTURE_TYPE_BARLEY_PARAM_KEY = "lactationConcentrateMixture_halfStabulationPasture_type_barley";
    static const std::string LACTATION_PERIOD_MINERAL_VITAMINS_STABULATION_PARAM_KEY = "lactationPeriodMineralVitamins_stabulation";
    static const std::string LACTATION_PERIOD_MINERAL_VITAMINS_PASTURE_PARAM_KEY = "lactationPeriodMineralVitamins_pasture";
    static const std::string LACTATION_PERIOD_MINERAL_VITAMINS_HALF_STABULATION_PASTURE_PARAM_KEY = "lactationPeriodMineralVitamins_halfStabulationPasture";
    static const std::string GAIN_FOR_ONE_CONCENTRATE_KILO_PARAM_KEY = "gainForOneConcentrateKilo";
    static const std::string PRODUCT_CONCENTRATE_MIXTURE_TYPE_RAPESEED_PARAM_KEY = "productConcentrateMixture_type_rapeseed";
    static const std::string PRODUCT_CONCENTRATE_MIXTURE_TYPE_SOJA_PARAM_KEY = "productConcentrateMixture_type_soja";
    static const std::string PRODUCT_CONCENTRATE_MIXTURE_TYPE_BARLEY_PARAM_KEY = "productConcentrateMixture_type_barley";
} /* End of namespace ExchangeInfoStructures */
}
#endif // Parameters_FeedingPlanParametersConstants_h
