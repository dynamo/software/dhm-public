#ifndef FunctionalConstants_h
#define FunctionalConstants_h

// standard
#include <string>
#include <map>
#include <vector>

// project
#include "FunctionalEnumerations.h"
#include "TechnicalConstants.h"
#include "../Tools/Tools.h"

// ------------------------------------------------------------------------------------------------------------------------------------------------------------
// Here are all the "constant" and default values, some of them could be calibrated by a sensibility analysis, based on an external (imported) value collection
// use CONST_SA macro in this case
// ------------------------------------------------------------------------------------------------------------------------------------------------------------
namespace FunctionalConstants
{
    
#ifdef _SENSIBILITY_ANALYSIS
    
class Modules
{
public:
    static void modulateValue(std::string &specificParameterKey, std::string &specificParameterValue);
};

//#define CONST_SA // In sensibility analysis case, some "constants" are not
//#else // _SENSIBILITY_ANALYSIS
//#define CONST_SA static inline const
#endif // _SENSIBILITY_ANALYSIS

/*   
   _____ _       _           _                       _              _       
 / ____| |     | |         | |                     | |            | |      
| |  __| | ___ | |__   __ _| |   ___ ___  _ __  ___| |_ __ _ _ __ | |_ ___ 
| | |_ | |/ _ \| '_ \ / _` | |  / __/ _ \| '_ \/ __| __/ _` | '_ \| __/ __|
| |__| | | (_) | |_) | (_| | | | (_| (_) | | | \__ \ || (_| | | | | |_\__ \
 \_____|_|\___/|_.__/ \__,_|_|  \___\___/|_| |_|___/\__\__,_|_| |_|\__|___/                                                                        

 */                                                                            
class Global
{
public:
    
    static inline const unsigned int COW_QUARTER_NB = 4;  // Quareter nombrer
    static inline const std::string DAY_OF_THE_WEEK_FOR_OPERATIONS = "Sun";
    static inline const std::string DEFAULT_FARM_COMMENT = "Default farm parameters";
}; // namespace Global

/*
 _____                          _            _   _                                   _              _       
|  __ \                        | |          | | (_)                                 | |            | |      
| |__) |___ _ __  _ __ ___   __| |_   _  ___| |_ _  ___  _ __     ___ ___  _ __  ___| |_ __ _ _ __ | |_ ___ 
|  _  // _ \ '_ \| '__/ _ \ / _` | | | |/ __| __| |/ _ \| '_ \   / __/ _ \| '_ \/ __| __/ _` | '_ \| __/ __|
| | \ \  __/ |_) | | | (_) | (_| | |_| | (__| |_| | (_) | | | | | (_| (_) | | | \__ \ || (_| | | | | |_\__ \
|_|  \_\___| .__/|_|  \___/ \__,_|\__,_|\___|\__|_|\___/|_| |_|  \___\___/|_| |_|___/\__\__,_|_| |_|\__|___/
           | |                                                                                              
           |_|                                                                                              
*/
class Reproduction
{
public:
    // Puberty age (2.2.1.1.1.1)
    static inline const unsigned int PUBERTY_MEAN_AGE = 300;
    // Ovulation duration (2.2.1.1.1.2)
    static inline const unsigned int OVULATION_DURATION = 1; 

    // Oeastrus interval duration (2.2.1.1.1.3)
    static inline const std::map<FunctionalEnumerations::Reproduction::DairyCowParity, unsigned int> MEAN_BETWEEN_OVULATION_DURATION = 
                                        {{FunctionalEnumerations::Reproduction::DairyCowParity::heifer,     20},
                                         {FunctionalEnumerations::Reproduction::DairyCowParity::primipare,  21},
                                         {FunctionalEnumerations::Reproduction::DairyCowParity::multipare,  21}};

    static inline const unsigned int STANDARD_DEVIATION_INTERVAL_BETWEEN_OVULATION = 1;                      

    // Detectability/sensitivite (2.2.1.1.2.1 and 2.2.1.1.2.2)
    static inline const std::map<FunctionalEnumerations::Reproduction::DetectionMode, std::map<FunctionalEnumerations::Reproduction::DairyCowParity, std::map<FunctionalEnumerations::Reproduction::OvulationClass, float>>> MONTBELIARDE_OVULATION_DETECTION_PROBA
       {{FunctionalEnumerations::Reproduction::DetectionMode::detector,
                                     {{FunctionalEnumerations::Reproduction::DairyCowParity::heifer,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.90f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.90f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.90f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.90f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.90f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::primipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.80f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.82f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.84f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.84f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.84f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::multipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.82f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.85f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.88f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.88f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.88f}}}}},
        {FunctionalEnumerations::Reproduction::DetectionMode::farmer,
                                     {{FunctionalEnumerations::Reproduction::DairyCowParity::heifer,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.60f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.60f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.60f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.60f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.60f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::primipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.50f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.52f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.54f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.64f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.59f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::multipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.52f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.54f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.58f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.68f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.63f}}}}},
        {FunctionalEnumerations::Reproduction::DetectionMode::robot,
                                     {{FunctionalEnumerations::Reproduction::DairyCowParity::heifer,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.80f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.80f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.80f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.80f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.80f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::primipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.70f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.72f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.74f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.74f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.74f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::multipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.72f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.75f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.78f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.78f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.78f}}}}},
        {FunctionalEnumerations::Reproduction::DetectionMode::male,
                                     {{FunctionalEnumerations::Reproduction::DairyCowParity::heifer,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    1.00f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::primipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    1.00f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::multipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    1.00f}}}}}}; 

    static inline const std::map<FunctionalEnumerations::Reproduction::DetectionMode, std::map<FunctionalEnumerations::Reproduction::DairyCowParity,std::map<FunctionalEnumerations::Reproduction::OvulationClass, float>>> NORMANDE_OVULATION_DETECTION_PROBA
       {{FunctionalEnumerations::Reproduction::DetectionMode::detector,
                                     {{FunctionalEnumerations::Reproduction::DairyCowParity::heifer,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.90f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.90f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.90f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.90f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.90f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::primipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.80f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.82f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.84f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.84f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.84f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::multipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.82f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.85f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.88f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.88f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.88f}}}}},
        {FunctionalEnumerations::Reproduction::DetectionMode::farmer,
                                     {{FunctionalEnumerations::Reproduction::DairyCowParity::heifer,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.60f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.60f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.60f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.60f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.60f}}},  
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::primipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.50f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.52f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.54f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.64f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.59f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::multipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.52f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.54f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.58f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.68f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.63f}}}}},
        {FunctionalEnumerations::Reproduction::DetectionMode::robot,
                                     {{FunctionalEnumerations::Reproduction::DairyCowParity::heifer,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.80f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.80f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.80f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.80f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.80f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::primipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.70f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.72f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.74f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.74f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.74f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::multipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.72f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.75f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.78f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.78f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.78f}}}}},
        {FunctionalEnumerations::Reproduction::DetectionMode::male,
                                      {{FunctionalEnumerations::Reproduction::DairyCowParity::heifer,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    1.00f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::primipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    1.00f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::multipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    1.00f}}}}}};

    static inline const std::map<FunctionalEnumerations::Reproduction::DetectionMode, std::map<FunctionalEnumerations::Reproduction::DairyCowParity,std::map<FunctionalEnumerations::Reproduction::OvulationClass, float>>> PRIM_HOLSTEIN_OVULATION_DETECTION_PROBA
       {{FunctionalEnumerations::Reproduction::DetectionMode::detector,
                                     {{FunctionalEnumerations::Reproduction::DairyCowParity::heifer,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.85f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.85f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.85f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.85f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.85f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::primipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.75f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.77f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.79f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.79f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.79f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::multipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.77f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.79f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.81f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.81f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.81f}}}}},
        {FunctionalEnumerations::Reproduction::DetectionMode::farmer,
                                      {{FunctionalEnumerations::Reproduction::DairyCowParity::heifer,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.55f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.55f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.55f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.55f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.55f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::primipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.45f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.47f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.49f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.59f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.54f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::multipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.47f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.49f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.51f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.61f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.56f}}}}},
        {FunctionalEnumerations::Reproduction::DetectionMode::robot,
                                     {{FunctionalEnumerations::Reproduction::DairyCowParity::heifer,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.75f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.75f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.75f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.75f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.75}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::primipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.65f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.67},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.69f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.69f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.69f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::multipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  0.67f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 0.69f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   0.71f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     0.71f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    0.71f}}}}},
        {FunctionalEnumerations::Reproduction::DetectionMode::male,
                                     {{FunctionalEnumerations::Reproduction::DairyCowParity::heifer,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    1.00f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::primipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    1.00f}}},
                                      {FunctionalEnumerations::Reproduction::DairyCowParity::multipare,
                                                                   {{FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation,  1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation, 1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation,   1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::firstReturn,     1.00f},
                                                                    {FunctionalEnumerations::Reproduction::OvulationClass::secundReturn,    1.00f}}}}}}; 
    static inline const unsigned int DAY_NUMBER_FOR_NO_OBSERVED_OESTRUS_NO_VET_CONTRACT = 100;
                                                                    
    // Complementaires criterium (2.2.1.1.2.3)
    // Slippery floor (2.2.1.1.2.3.1)
    static inline const float SLIPPERY_FLOOR_OVULATION_DETECTION_RATIO = 0.5f; // Pour les sols glissants, un ratio (abattement) de 0,5 est systematiquement applique pour la detection des ovulations
    
    // Concomitant estrus (2.2.1.1.2.3.2)
    static inline const float FACTOR_2_CONCOMITANT_OESTRUS = 1.3f; // Ratio augmentant la sensibilite est applique pour 2 chaleurs concomitantes
    static inline const float FACTOR_3_AND_MORE_CONCOMITANT_OESTRUS = 1.4f; // Ratio augmentant la sensibilite est applique pour 3 et plus de chaleurs concomitantes

    // Specificity (2.2.1.1.3)

    // Failure risk after caving or breding (2.2.1.1.3.1) 
    // Heifers
    static inline const unsigned int FIRST_DAY_FIRST_HEIFER_SPECIFICITY_PERIOD = 1;
    static inline const unsigned int FIRST_DAY_LAST_HEIFER_SPECIFICITY_PERIOD = 47;
    static inline const float PROBA_HEIFER_SPECIFICITY_ERROR_DETECTOR [2] =   {     0.01f ,     0.02f };
    static inline const float PROBA_HEIFER_SPECIFICITY_ERROR_FARMER [2] =     {     0.04f ,     0.05f };
    static inline const float PROBA_HEIFER_SPECIFICITY_ERROR_ROBOT [2] =      {     0.02f ,     0.03f };
    // Cow 
    static inline const unsigned int FIRST_DAY_FIRST_COW_SPECIFICITY_PERIOD = 40;
    static inline const unsigned int FIRST_DAY_SECUND_COW_SPECIFICITY_PERIOD = 70;
    static inline const unsigned int FIRST_DAY_LAST_COW_SPECIFICITY_PERIOD = 101;
    static inline const float PROBA_COW_SPECIFICITY_ERROR_DETECTOR [6] =  {     0.01f ,     0.02f ,   0.05f ,   0.01f ,     0.02f ,   0.05f };
    static inline const float PROBA_COW_SPECIFICITY_ERROR_FARMER [6] =    {     0.04f ,     0.05f ,   0.13f ,   0.04f ,     0.05f ,   0.13f };
    static inline const float PROBA_COW_SPECIFICITY_ERROR_ROBOT [6] =     {     0.02f ,     0.03f ,   0.06f ,   0.02f ,     0.03f ,   0.06f };

    // Failure risk after insemination (2.2.1.1.3.2) 
    static inline const unsigned int STANDARD_DEVIATION_FOR_SPECIFICITY_ERROR_IA_RETURN = 1;
    static inline const float PROBA_SPECIFICITY_ERROR_IA_RETURN = 0.07f;

    // Inseminations and fertility  (2.2.1.1.4)

    // Fertility  (2.2.1.1.4.2)
    // Montbeliarde
    static inline const std::map<FunctionalEnumerations::Reproduction::TypeInseminationSemen,std::map<FunctionalEnumerations::Reproduction::DairyCowParity, float>> MONTBELIARDE_INSEMINATION_SUCCESS_PROBA
            {{{ FunctionalEnumerations::Reproduction::TypeInseminationSemen::conventionalIA,
                                                       {{ FunctionalEnumerations::Reproduction::DairyCowParity::heifer,     0.85f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::primipare,  0.80f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::multipare,  0.80f}}},
            {   FunctionalEnumerations::Reproduction::TypeInseminationSemen::femaleSexedIA,
                                                       {{ FunctionalEnumerations::Reproduction::DairyCowParity::heifer,     0.73f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::primipare,  0.67f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::multipare,  0.67f}}},
            {   FunctionalEnumerations::Reproduction::TypeInseminationSemen::maleSexedIA,
                                                       {{ FunctionalEnumerations::Reproduction::DairyCowParity::heifer,     0.73f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::primipare,  0.67f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::multipare,  0.67f}}},
            {   FunctionalEnumerations::Reproduction::TypeInseminationSemen::natural,
                                                       {{ FunctionalEnumerations::Reproduction::DairyCowParity::heifer,     0.95f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::primipare,  0.90f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::multipare,  0.90f}}}}}; 

    // Normande
    static inline const std::map<FunctionalEnumerations::Reproduction::TypeInseminationSemen,std::map<FunctionalEnumerations::Reproduction::DairyCowParity, float>> NORMANDE_INSEMINATION_SUCCESS_PROBA
            {{{ FunctionalEnumerations::Reproduction::TypeInseminationSemen::conventionalIA,
                                                       {{ FunctionalEnumerations::Reproduction::DairyCowParity::heifer,     0.85f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::primipare,  0.78f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::multipare,  0.78f}}},
            {   FunctionalEnumerations::Reproduction::TypeInseminationSemen::femaleSexedIA,
                                                       {{ FunctionalEnumerations::Reproduction::DairyCowParity::heifer,     0.79f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::primipare,  0.62f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::multipare,  0.62f}}},
            {   FunctionalEnumerations::Reproduction::TypeInseminationSemen::maleSexedIA,
                                                       {{ FunctionalEnumerations::Reproduction::DairyCowParity::heifer,     0.79f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::primipare,  0.62f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::multipare,  0.62f}}},
            {   FunctionalEnumerations::Reproduction::TypeInseminationSemen::natural,
                                                       {{ FunctionalEnumerations::Reproduction::DairyCowParity::heifer,     0.95f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::primipare,  0.90f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::multipare,  0.90f}}}}}; 

    // Prim'Holstein
    static inline const std::map<FunctionalEnumerations::Reproduction::TypeInseminationSemen,std::map<FunctionalEnumerations::Reproduction::DairyCowParity, float>> PRIM_HOLSTEIN_INSEMINATION_SUCCESS_PROBA
            {{{ FunctionalEnumerations::Reproduction::TypeInseminationSemen::conventionalIA,
                                                       {{ FunctionalEnumerations::Reproduction::DairyCowParity::heifer,     0.83f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::primipare,  0.74f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::multipare,  0.74f}}},
            {   FunctionalEnumerations::Reproduction::TypeInseminationSemen::femaleSexedIA,
                                                       {{ FunctionalEnumerations::Reproduction::DairyCowParity::heifer,     0.69f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::primipare,  0.62f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::multipare,  0.62f}}},
            {   FunctionalEnumerations::Reproduction::TypeInseminationSemen::maleSexedIA,
                                                       {{ FunctionalEnumerations::Reproduction::DairyCowParity::heifer,     0.69f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::primipare,  0.62f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::multipare,  0.62f}}},
            {   FunctionalEnumerations::Reproduction::TypeInseminationSemen::natural,
                                                       {{ FunctionalEnumerations::Reproduction::DairyCowParity::heifer,     0.95f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::primipare,  0.90f},
                                                        { FunctionalEnumerations::Reproduction::DairyCowParity::multipare,  0.90f}}}}}; 

    // Milk influence (2.2.1.1.4.4.1)
    static inline const float LOW_PEAK_MILK_VALUE_FOR_FERTILITY_FACTOR = 27.2f;
    static inline const float FERTILITY_FACTOR_FOR_LOW_PEAK_MILK_VALUE = 1.0f/0.95f;
    static inline const float MEDIUM_PEAK_MILK_VALUE_FOR_FERTILITY_FACTOR = 36.6f;
    static inline const float HIGH_PEAK_MILK_VALUE_FOR_FERTILITY_FACTOR = 47.2f;
    static inline const float FERTILITY_FACTOR_FOR_HIGH_PEAK_MILK_VALUE = 1.0f/1.07f;
    
    // Vet check  (2.2.1.1.5)
    static inline const unsigned int DAY_OF_THE_MONTH_FOR_VET_REPRO_CONTROL = 15;
    static inline const unsigned int DAY_NUMBER_FOR_NO_OBSERVED_OESTRUS_VET_CONTRACT = 60;
    static inline const unsigned int NON_FECUNDANT_INSEMINATION_NUMBER = 3;
    static inline const unsigned int DAY_NUMBER_FOR_NEGATIVE_PREGNANCY_DIAGNOSTIC = 35;
    static inline const float SENSIBILITE_ERRORS_FOR_PREGNANCY_DIAGNOSTIC = 0.037f;
    static inline const float VET_CONTRACT_FERTILITY_DELTA = 0.03f;
    
    // Gestation  (2.2.1.1.6) :
    // Late embrio mortality and abortion (2.2.1.1.6.1)
    static inline const unsigned int FIRST_DAY_FOR_LATE_EMBRYONIC_PERIOD = 16;
    static inline const unsigned int PEAK_DAY_IN_LATE_EMBRYONIC_PERIOD = 21;
    static inline const unsigned int FIRST_DAY_FOR_ABORTION_PERIOD = 90;
    static inline const unsigned int DAY_BEFORE_STANDARD_PREGNANCY_DURATION_FOR_FIRST_CALVINGS = 22;

    static inline const std::map<FunctionalEnumerations::Genetic::Breed, float> LATE_EMBRYONIC_MORTALITY_RISK {   {FunctionalEnumerations::Genetic::Breed::normande,          0.23f},
                                                                                                    {FunctionalEnumerations::Genetic::Breed::primHolstein,      0.21f},
                                                                                                    {FunctionalEnumerations::Genetic::Breed::montbeliarde,      0.21f},
                                                                                                    {FunctionalEnumerations::Genetic::Breed::blancBleueBelge,   0.15f}, //tmp
                                                                                                    {FunctionalEnumerations::Genetic::Breed::charolaise,        0.15f}, //tmp
                                                                                                    {FunctionalEnumerations::Genetic::Breed::limousine,         0.15f}}; //tmp
    static inline const std::map<FunctionalEnumerations::Genetic::Breed, float> ABORTION_RISK {   {FunctionalEnumerations::Genetic::Breed::normande,          0.003f},
                                                                                    {FunctionalEnumerations::Genetic::Breed::primHolstein,      0.025f},
                                                                                    {FunctionalEnumerations::Genetic::Breed::montbeliarde,      0.016f},
                                                                                    {FunctionalEnumerations::Genetic::Breed::blancBleueBelge,   0.016f}, //tmp
                                                                                    {FunctionalEnumerations::Genetic::Breed::charolaise,        0.010f}, //tmp
                                                                                    {FunctionalEnumerations::Genetic::Breed::limousine,         0.010f}}; //tmp


    // Durations (2.2.1.1.6.2) :
    // Breed and parity (2.2.1.1.6.2.1)
                                                                                    
    static inline const std::map<FunctionalEnumerations::Reproduction::DairyCowParity, std::map<FunctionalEnumerations::Genetic::Breed, float>> MEAN_PREGNANCY_DURATION
           {{  FunctionalEnumerations::Reproduction::DairyCowParity::heifer,
                                    {{ FunctionalEnumerations::Genetic::Breed::montbeliarde, 286.0f},
                                    {  FunctionalEnumerations::Genetic::Breed::normande, 285.0f},
                                    {  FunctionalEnumerations::Genetic::Breed::primHolstein, 280.0f},
                                    {  FunctionalEnumerations::Genetic::Breed::blancBleueBelge, 270.0f}, // tmp
                                    {  FunctionalEnumerations::Genetic::Breed::charolaise, 270.0f}, // tmp
                                    {  FunctionalEnumerations::Genetic::Breed::limousine, 270.0f}}}, // tmp
           {  FunctionalEnumerations::Reproduction::DairyCowParity::primipare,
                                    {{ FunctionalEnumerations::Genetic::Breed::montbeliarde, 287.0f},
                                    {  FunctionalEnumerations::Genetic::Breed::normande, 286.0f},
                                    {  FunctionalEnumerations::Genetic::Breed::primHolstein, 282.0f},
                                    {  FunctionalEnumerations::Genetic::Breed::blancBleueBelge, 270.0f}, // tmp
                                    {  FunctionalEnumerations::Genetic::Breed::charolaise, 270.0f}, // tmp
                                    {  FunctionalEnumerations::Genetic::Breed::limousine, 270.0f}}}, // tmp
           {   FunctionalEnumerations::Reproduction::DairyCowParity::multipare,
                                    {{ FunctionalEnumerations::Genetic::Breed::montbeliarde, 287.0f},     
                                    {  FunctionalEnumerations::Genetic::Breed::normande, 286.0f},
                                    {  FunctionalEnumerations::Genetic::Breed::primHolstein, 282.0f},
                                    {  FunctionalEnumerations::Genetic::Breed::blancBleueBelge, 270.0f}, // tmp
                                    {  FunctionalEnumerations::Genetic::Breed::charolaise, 270.0f}, // tmp
                                    {  FunctionalEnumerations::Genetic::Breed::limousine, 270.0f}}}};   // tmp

    // Standard deviation
    static inline const unsigned int STANDARD_DEVIATION_PREGNANCY_DURATION = 5;


    // Sex (2.2.1.1.6.2.2)
    static inline const std::map<FunctionalEnumerations::Reproduction::TypeInseminationSemen, float> SEMEN_FEMALE_BIRTH_PROBA =
                                                                            {{FunctionalEnumerations::Reproduction::TypeInseminationSemen::natural,           0.50f},
                                                                             {FunctionalEnumerations::Reproduction::TypeInseminationSemen::conventionalIA,   0.49f},
                                                                             {FunctionalEnumerations::Reproduction::TypeInseminationSemen::maleSexedIA,       0.07f},
                                                                             {FunctionalEnumerations::Reproduction::TypeInseminationSemen::femaleSexedIA,     0.90f}};
    static inline const unsigned int PREGNANCY_DURATION_DECREASE_FOR_MALE = 1;


    // Gemelity (2.2.1.1.6.2.3)
    static inline const std::map<FunctionalEnumerations::Reproduction::DairyCowParity, float> TWINNING_PROBABILITY =
                                                                 {{   FunctionalEnumerations::Reproduction::DairyCowParity::heifer,    0.01f},
                                                                  {   FunctionalEnumerations::Reproduction::DairyCowParity::primipare, 0.06f},
                                                                  {   FunctionalEnumerations::Reproduction::DairyCowParity::multipare, 0.07f}};
    static inline const unsigned int PREGNANCY_DURATION_DECREASE_FOR_TWINS = 5;


    // Post-partum cycle (2.2.1.1.7)

    // Increase because of a pregnancy interrupt (2.2.1.1.6.1)
    static inline const unsigned int MAX_DAY_INCREASE_FOR_INTERRUPTED_PREGNANCY = 60; // based on Lactation::FIRST_DAY_FOR_ABORTION_COUNTING_LACTATION

    // After Calving post partum (2.2.1.1.6.2)
    static inline const unsigned int INTERRUPTED_CYCLE_DURATION = 35;
    static inline const unsigned int NORMAL_CYCLE_FIRST_DAY = 10;
    static inline const unsigned int NORMAL_CYCLE_LAST_DAY = 50;
    static inline const unsigned int DELAYED_CYCLE_LAST_DAY = 100;

    // Montbeliarde
    static inline const std::map<FunctionalEnumerations::Reproduction::DairyCowParity,std::vector<float>> MONTBELIARDE_CYCLE_PROBA
                                                {{  FunctionalEnumerations::Reproduction::DairyCowParity::primipare,{{0.84f, 0.04f, 0.11f,  0.01f}}},
                                                {   FunctionalEnumerations::Reproduction::DairyCowParity::multipare,{{0.87f, 0.11f, 0.014f, 0.006f}}}}; 
    // Normande
    static inline const std::map<FunctionalEnumerations::Reproduction::DairyCowParity,std::vector<float>> NORMANDE_CYCLE_PROBA
                                                {{  FunctionalEnumerations::Reproduction::DairyCowParity::primipare,{{0.78f, 0.06f, 0.15f, 0.01f}}},
                                                {   FunctionalEnumerations::Reproduction::DairyCowParity::multipare,{{0.83f, 0.15f, 0.014f, 0.006f}}}}; 
    // Prim'Holstein
    static inline const std::map<FunctionalEnumerations::Reproduction::DairyCowParity,std::vector<float>> PRIM_HOLSTEIN_CYCLE_PROBA
                                                {{  FunctionalEnumerations::Reproduction::DairyCowParity::primipare,{{0.51f, 0.19f, 0.17f, 0.01f}}},
                                                {   FunctionalEnumerations::Reproduction::DairyCowParity::multipare,{{0.56f, 0.30f, 0.054f, 0.026f}}}}; 

    // Default Values (2.2.1.2)
    static inline const unsigned int DEFAULT_AGE_TO_BREEDING_DECISION_VALUE = 500; // 2.2.1.2.1
    static inline const FunctionalEnumerations::Reproduction::DetectionMode DEFAULT_DETECTION_MODE_VALUE = FunctionalEnumerations::Reproduction::DetectionMode::detector; // 2.2.1.2.2
    static inline const bool DEFAULT_SLIPPERY_FLOOR_VALUE = false; // 2.2.1.2.3
    
    // Mating plan (2.2.1.2.4)
    static inline const FunctionalEnumerations::Reproduction::TypeInseminationSemen COMPLEMENT_INSEMINATION_TYPE = FunctionalEnumerations::Reproduction::TypeInseminationSemen::conventionalIA;
    static inline const FunctionalEnumerations::Reproduction::TypeInseminationSemen DEFAULT_HEIFER_1_INSEMINATION_TYPE_SEMEN = FunctionalEnumerations::Reproduction::TypeInseminationSemen::femaleSexedIA;
    static inline const float DEFAULT_HEIFER_1_INSEMINATION_TYPE_RATIO = 1.0f;
    static inline const FunctionalEnumerations::Reproduction::TypeInseminationSemen DEFAULT_COW_1_INSEMINATION_TYPE_SEMEN = FunctionalEnumerations::Reproduction::TypeInseminationSemen::conventionalIA;
    static inline const float DEFAULT_COW_1_INSEMINATION_TYPE_RATIO = 1.0f;
    static inline const FunctionalEnumerations::Reproduction::TypeInseminationSemen DEFAULT_HEIFER_2_3_INSEMINATION_TYPE_SEMEN = FunctionalEnumerations::Reproduction::TypeInseminationSemen::conventionalIA;
    static inline const float DEFAULT_HEIFER_2_3_INSEMINATION_TYPE_RATIO = 1.0f;
    static inline const FunctionalEnumerations::Reproduction::TypeInseminationSemen DEFAULT_COW_2_3_INSEMINATION_TYPE_SEMEN = FunctionalEnumerations::Reproduction::TypeInseminationSemen::conventionalIA;
    static inline const float DEFAULT_COW_2_3_INSEMINATION_TYPE_RATIO = 1.0f;
    static inline const FunctionalEnumerations::Reproduction::TypeInseminationSemen DEFAULT_HEIFER_4_OVER_INSEMINATION_TYPE_SEMEN = FunctionalEnumerations::Reproduction::TypeInseminationSemen::conventionalIA;
    static inline const float DEFAULT_HEIFER_4_OVER_INSEMINATION_TYPE_RATIO = 1.0f;
    static inline const FunctionalEnumerations::Reproduction::TypeInseminationSemen DEFAULT_COW_4_OVER_INSEMINATION_TYPE_SEMEN = FunctionalEnumerations::Reproduction::TypeInseminationSemen::conventionalIA;
    static inline const float DEFAULT_COW_4_OVER_INSEMINATION_TYPE_RATIO = 1.0f;
    static inline const FunctionalEnumerations::Genetic::Breed DEFAULT_HEIFER_1_INSEMINATION_BREED_SEMEN = FunctionalEnumerations::Genetic::Breed::primHolstein;
    static inline const float DEFAULT_HEIFER_1_INSEMINATION_BREED_RATIO = 1.0f;
    static inline const FunctionalEnumerations::Genetic::Breed DEFAULT_COW_1_INSEMINATION_BREED_SEMEN = FunctionalEnumerations::Genetic::Breed::primHolstein;
    static inline const float DEFAULT_COW_1_INSEMINATION_BREED_RATIO = 1.0f;
    static inline const FunctionalEnumerations::Genetic::Breed DEFAULT_HEIFER_2_3_INSEMINATION_BREED_SEMEN = FunctionalEnumerations::Genetic::Breed::primHolstein;
    static inline const float DEFAULT_HEIFER_2_3_INSEMINATION_BREED_RATIO = 1.0f;
    static inline const FunctionalEnumerations::Genetic::Breed DEFAULT_COW_2_3_INSEMINATION_BREED_SEMEN = FunctionalEnumerations::Genetic::Breed::primHolstein;
    static inline const float DEFAULT_COW_2_3_INSEMINATION_BREED_RATIO = 1.0f;
    static inline const FunctionalEnumerations::Genetic::Breed DEFAULT_HEIFER_4_OVER_INSEMINATION_BREED_SEMEN = FunctionalEnumerations::Genetic::Breed::limousine;
    static inline const float DEFAULT_HEIFER_4_OVER_INSEMINATION_BREED_RATIO = 1.0f;
    static inline const FunctionalEnumerations::Genetic::Breed DEFAULT_COW_4_OVER_INSEMINATION_BREED_SEMEN = FunctionalEnumerations::Genetic::Breed::charolaise;
    static inline const float DEFAULT_COW_4_OVER_INSEMINATION_BREED_RATIO = 1.0f;
    
    // 2.2.1.2.5

    // 2.2.1.2.6
    static inline const float DEFAULT_FERTILITY_FACTOR_VALUE = 1.0f;
    
    // 2.2.1.2.7
    static inline const unsigned int DEFAULT_BREEDING_DELAY_AFTER_CALVING_DECISION_VALUE = 55; 

    // 2.2.1.2.8
    static inline const unsigned int DEFAULT_NUMBER_OF_DAYS_AFTER_HEIFER_BREEDING_DECISION_FOR_STOP_FIRST_INSEMINATION_DECISION_VALUE = 150;
    static inline const unsigned int DEFAULT_NUMBER_OF_DAYS_AFTER_HEIFER_BREEDING_DECISION_FOR_STOP_INSEMINATION_DECISION_VALUE = 200;
    static inline const unsigned int DEFAULT_NUMBER_OF_DAYS_AFTER_ADULT_BREEDING_DECISION_FOR_STOP_FIRST_INSEMINATION_DECISION_VALUE = 190;
    static inline const unsigned int DEFAULT_NUMBER_OF_DAYS_AFTER_ADULT_BREEDING_DECISION_FOR_STOP_INSEMINATION_DECISION_VALUE = 260;
    
    // 2.2.1.2.9
    static inline const bool DEFAULT_VET_REPRODUCTION_CONTRACT_OPTION = false;
 
    static inline const std::string DEFAULT_REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_TREATMENT_NAME = "Case a : ENZAPROST(R) T ";
    static inline const unsigned int DEFAULT_REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_EFFECT_DELAY = 3;
    static inline const float DEFAULT_REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_SUCCESS_PROBABILITY = 1.0f;                                                                                 
    
    static inline const std::string DEFAULT_REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_NAME = "Case b : RECEPTAL(R) (2 mL)";
    static inline const unsigned int DEFAULT_REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_EFFECT_DELAY = 1; // Normally 0, but test is done after the reproduction actions so treatement really work the day after
    static inline const float DEFAULT_REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_SUCCESS_PROBABILITY = 1.0f;          
    static inline const float DEFAULT_REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_FERTILITY_DELTA = 0.06f;          
    
    static inline const std::string DEFAULT_REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_NAME = "Case c : ENZAPROST(R) T";
    static inline const unsigned int DEFAULT_REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_EFFECT_DELAY = 3;
    static inline const float DEFAULT_REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_SUCCESS_PROBABILITY = 1.0f;                                                                                                                                                             
};
                                            
/*
 _                _        _   _                                   _              _       
| |              | |      | | (_)                                 | |            | |      
| |     __ _  ___| |_ __ _| |_ _  ___  _ __     ___ ___  _ __  ___| |_ __ _ _ __ | |_ ___ 
| |    / _` |/ __| __/ _` | __| |/ _ \| '_ \   / __/ _ \| '_ \/ __| __/ _` | '_ \| __/ __|
| |___| (_| | (__| || (_| | |_| | (_) | | | | | (_| (_) | | | \__ \ || (_| | | | | |_\__ \
|______\__,_|\___|\__\__,_|\__|_|\___/|_| |_|  \___\___/|_| |_|___/\__\__,_|_| |_|\__|___/
                                                                                          
*/
// (2.2.2) 
// --------------------
class Lactation
{
public:

    // (2.2.2.1)
    static inline const unsigned int FIRST_DAY_FOR_ABORTION_COUNTING_LACTATION = 150;
    static inline const float MILK_KILO_TO_LITER_FACTOR = 1.0f/1.033f;

    // Daily milk product depending breed, parity and lactation stage (2.2.2.1.1.2)
    static inline const unsigned int REFERENCE_LACTATION_DURATION = 305;
    static inline const unsigned int REFERENCE_DRY_DURATION = 60;
    
    // SCC without infection (2.2.2.1.1.3)
    static inline const std::vector<float> MILK_SCC_WITHOUT_INFECTION_LEVEL = {43.3f, 67.7f, 84.7f};
  
    // Milk penalties (2.2.2.1.1.5)
    static inline const unsigned int MILK_DELIVERY_FREQUENCY = 3;
    static inline const unsigned int FIRST_DAY_FOR_COLLECTED_MILK_TEST = 5;
    static inline const unsigned int SECOND_DAY_FOR_COLLECTED_MILK_TEST = 15;
    static inline const unsigned int THIRD_DAY_FOR_COLLECTED_MILK_TEST = 25;
    static inline const unsigned int LEVEL_1_FOR_SCC_PENALTY = 250;
    static inline const unsigned int LEVEL_2_FOR_SCC_PENALTY = 300;
    static inline const unsigned int LEVEL_3_FOR_SCC_PENALTY = 450;

    // Parity (2.2.2.1.2.1)                                                    1        2        3      4+                                                                   
    static inline const std::vector<float> MILK_PRODUCT_FACTOR_DEPENDING_LACTATION_RANK = {1/1.3f ,1/1.12f, 1/1.03f, 1.0f};
    static inline const unsigned int PARITY_FOR_TP_DEGRADATION = 4;
    static inline const float TP_LOSS_WITH_PARITY = 0.2f;

    // Dried period effect (2.2.2.1.2.2)
    static inline const unsigned int MINIMUM_DRIED_PERIOD_DURATION_WITHOUT_EFFECT_VALUE = 60;
    static inline const unsigned int MILK_LOSS_PERCENT_BECAUSE_OF_NO_DRIED_PERIOD = 15;
    static inline const float MILK_LOSS_A_RATIO_BECAUSE_OF_NO_DRIED_PERIOD = - (float)MILK_LOSS_PERCENT_BECAUSE_OF_NO_DRIED_PERIOD/(float)MINIMUM_DRIED_PERIOD_DURATION_WITHOUT_EFFECT_VALUE;
    //                                                               L2    L3+
    static inline const std::vector<float> TB_GAIN_BECAUSE_OF_NO_DRIED_PERIOD = { 2.9f, 0.4f};
    static inline const std::vector<float> TP_GAIN_BECAUSE_OF_NO_DRIED_PERIOD = { 3.0f, 1.1f};
    static inline const std::vector<float> SCC_GAIN_BECAUSE_OF_NO_DRIED_PERIOD = { 0.0f, 0.0f};

    // Pregnancy effect (2.2.2.1.2.3)
    static inline const unsigned int LACTATION_STAGE_1_FOR_PREGNANCY_EFFECT = 242;
    static inline const float LACTATION_VALUE_1_FOR_PREGNANCY_EFFECT = 1.0f;
    static inline const unsigned int LACTATION_STAGE_2_FOR_PREGNANCY_EFFECT = 272;
    static inline const float LACTATION_VALUE_2_FOR_PREGNANCY_EFFECT = 2.0f;
    static inline const unsigned int LACTATION_STAGE_3_FOR_PREGNANCY_EFFECT = 303;
    static inline const float LACTATION_VALUE_3_FOR_PREGNANCY_EFFECT = 2.5f;

    // Milk production reduced when oestrus (2.2.2.1.2.5)
    static inline const float OESTRUS_MILK_PRODUCTION_FACTOR = 0.925f;

    // Sunny impact (2.2.2.1.2.6)
    static inline const float SUMMER_MAX_QUANTITY_VALUE = -1.5f;
    static inline const float SUMMER_MAX_TB_VALUE = 1.5f;
    static inline const float SUMMER_MAX_TP_VALUE = 0.75f;
    static inline const float SUMMER_MAX_SCC_VALUE = 0.0f;

    // Milking Frequency (2.2.2.1.2.7)
    static inline const float MILKING_ONCE_A_DAY_QUANTITY_FACTOR = 0.75f; // -25%
    static inline const float MILKING_ONCE_A_DAY_TB = 2.7f; // g/kg
    static inline const float MILKING_ONCE_A_DAY_TP = 2.2f; // g/kg
    static inline const float MILKING_ONCE_A_DAY_SCC_FACTOR = 1.0f; // No effect
    static inline const float MILKING_OVER_TWICE_A_DAY_QUANTITY_FACTOR = 1.15f; // +15%
    
    // SCC (2.2.2.1.2.8)
    static inline const float REFERENCE_BASE_SCC_FOR_MILK_QUANTITY_REDUCTION = 50;
    static inline const std::vector<std::tuple<float, float, float>> SCC_FOR_MILK_QUANTITY_REDUCTION_EQUATION_PARITY_PARAMETER
                                {std::make_tuple(0.0f,      0.0f,    0.44f),
                                 std::make_tuple(0.00002f, -0.0019f, 0.5006f),
                                 std::make_tuple(0.00001f,  0.0015f, 0.3341f)};
 
    // Product after lost quarter (2.2.2.1.2.11)
    static inline const float MILK_PRODUCT_INCREASE_PERCENT_FOR_OTHER_QUARTERS_AFTER_QUARTER_LOSS = ((1.0f / Global::COW_QUARTER_NB) - 0.20f) / (Global::COW_QUARTER_NB-1);

    // Random aspect (2.2.2.1.2.13)
    static inline const float RANDOM_DAY_MILK_QUANTITY_ADJUSTMENT = 2.0f;
    static inline const float RANDOM_DAY_MILK_TB_SD_ADJUSTMENT = 1.25f;
    static inline const float RANDOM_DAY_MILK_TP_SD_ADJUSTMENT = 0.5f;
    static inline const float RANDOM_DAY_MILK_SCC_SD_ADJUSTMENT = 0.1f;
    
    // Default Values (2.2.2.2)
    static inline const unsigned int DEFAULT_DRIED_PERIOD_DURATION_VALUE = 60; // 2.2.2.2.1
//    static inline const unsigned int DEFAULT_MINIMUM_MILK_QUANTITY_FOR_DRIED_PERIOD = 5;
    static inline const FunctionalEnumerations::Lactation::MilkingFrequency DEFAULT_MILKING_FREQUENCY_VALUE = FunctionalEnumerations::Lactation::MilkingFrequency::twiceADay; // 2.2.2.2.2
    static inline const unsigned int DEFAULT_DELAY_BEFORE_DELIVER_MILK = 6;// 2.2.2.2.3
    // Herd production level delta (2.2.2.2.4)
    static inline const float DEFAULT_HERD_PRODUCTION_DELTA_LAIT = 0.0f;
    static inline const float DEFAULT_HERD_PRODUCTION_DELTA_TB = 0.0f;
    static inline const float DEFAULT_HERD_PRODUCTION_DELTA_TP = 0.0f;
 
    // Day for the milk control (2.2.2.3.3)
    static inline const unsigned int DAY_OF_THE_MONTH_FOR_MILK_CONTROL = 15;
    static inline const unsigned int SCC_REFERENCE_FOR_MILK_CONTROL = 300;
    static inline const float MILK_QUANTITY_FOR_CONTROL = 0.1f;
    
};

/*
 _    _            _ _   _                           _              _       
| |  | |          | | | | |                         | |            | |      
| |__| | ___  __ _| | |_| |__     ___ ___  _ __  ___| |_ __ _ _ __ | |_ ___ 
|  __  |/ _ \/ _` | | __| '_ \   / __/ _ \| '_ \/ __| __/ _` | '_ \| __/ __|
| |  | |  __/ (_| | | |_| | | | | (_| (_) | | | \__ \ || (_| | | | | |_\__ \
|_|  |_|\___|\__,_|_|\__|_| |_|  \___\___/|_| |_|___/\__\__,_|_| |_|\__|___/                                                                           
                                                         
*/
// (2.2.3) 
// ------------------------------
class Health
{
public:
    
    // IIM (2.2.3.1)
    // -------------

    // Mastitis severity distribution (2.2.3.1.1.1.2.2)

    // Severity degree
    static inline const std::map<FunctionalEnumerations::Health::MastitisSeverityLactationRank, std::map<FunctionalEnumerations::Health::MastitisRiskPeriodType, FunctionalEnumerations::Health::MastitisSeverityDegree>> BASIS_MASTITIS_SEVERITY_DISTRIBUTION
               {{  FunctionalEnumerations::Health::MastitisSeverityLactationRank::primipareLactationRank,
                                             {{ FunctionalEnumerations::Health::MastitisRiskPeriodType::periPartumAndLactationBegin,  FunctionalEnumerations::Health::MastitisSeverityDegree::majorated},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::lactation,                    FunctionalEnumerations::Health::MastitisSeverityDegree::basis},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingOff,                    FunctionalEnumerations::Health::MastitisSeverityDegree::minorated},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingInvoluted,              FunctionalEnumerations::Health::MastitisSeverityDegree::minorated}}},
               {   FunctionalEnumerations::Health::MastitisSeverityLactationRank::twoLactationsLactationRank,
                                             {{ FunctionalEnumerations::Health::MastitisRiskPeriodType::periPartumAndLactationBegin,  FunctionalEnumerations::Health::MastitisSeverityDegree::majorated},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::lactation,                    FunctionalEnumerations::Health::MastitisSeverityDegree::basis},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingOff,                    FunctionalEnumerations::Health::MastitisSeverityDegree::majorated},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingInvoluted,              FunctionalEnumerations::Health::MastitisSeverityDegree::majorated}}},
               {   FunctionalEnumerations::Health::MastitisSeverityLactationRank::threeLactationsAndMoreLactationRank,
                                             {{ FunctionalEnumerations::Health::MastitisRiskPeriodType::periPartumAndLactationBegin,  FunctionalEnumerations::Health::MastitisSeverityDegree::majorated},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::lactation,                    FunctionalEnumerations::Health::MastitisSeverityDegree::majorated},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingOff,                    FunctionalEnumerations::Health::MastitisSeverityDegree::majorated},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingInvoluted,              FunctionalEnumerations::Health::MastitisSeverityDegree::majorated}}}}; 

    // Severity distribution
    static inline const std::map<FunctionalEnumerations::Health::MastitisSeverityDegree,std::map<FunctionalEnumerations::Health::BacteriumType,std::map<FunctionalEnumerations::Health::MastitisSeverity, float>>> MASTITIS_SEVERITY_DISTRIBUTION
        {{FunctionalEnumerations::Health::MastitisSeverityDegree::basis,
               {{ FunctionalEnumerations::Health::BacteriumType::StaphA,
                                             {{ FunctionalEnumerations::Health::MastitisSeverity::G0,  0.85f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G1,  0.09f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G2,  0.05f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G3,  0.01f}}},
               {   FunctionalEnumerations::Health::BacteriumType::StreptU,
                                             {{ FunctionalEnumerations::Health::MastitisSeverity::G0,  0.45f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G1,  0.25f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G2,  0.23f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G3,  0.07f}}},
               {   FunctionalEnumerations::Health::BacteriumType::Gn,
                                             {{ FunctionalEnumerations::Health::MastitisSeverity::G0,  0.15f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G1,  0.33f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G2,  0.36f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G3,  0.16f}}},
               {   FunctionalEnumerations::Health::BacteriumType::CNS,
                                             {{ FunctionalEnumerations::Health::MastitisSeverity::G0,  0.90f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G1,  0.05f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G2,  0.05f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G3,  0.00f}}},
               {   FunctionalEnumerations::Health::BacteriumType::CB,
                                             {{ FunctionalEnumerations::Health::MastitisSeverity::G0,  0.98f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G1,  0.015f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G2,  0.003f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G3,  0.002f}}}}},
         {FunctionalEnumerations::Health::MastitisSeverityDegree::minorated, 
               {{ FunctionalEnumerations::Health::BacteriumType::StaphA,
                                             {{ FunctionalEnumerations::Health::MastitisSeverity::G0,  0.87f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G1,  0.07f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G2,  0.04f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G3,  0.02f}}},
               {   FunctionalEnumerations::Health::BacteriumType::StreptU,
                                             {{ FunctionalEnumerations::Health::MastitisSeverity::G0,  0.45f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G1,  0.30f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G2,  0.23f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G3,  0.02f}}},
               {   FunctionalEnumerations::Health::BacteriumType::Gn,
                                             {{ FunctionalEnumerations::Health::MastitisSeverity::G0,  0.15f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G1,  0.38f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G2,  0.41f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G3,  0.06f}}},
               {   FunctionalEnumerations::Health::BacteriumType::CNS,
                                             {{ FunctionalEnumerations::Health::MastitisSeverity::G0,  0.90f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G1,  0.07f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G2,  0.03f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G3,  0.00f}}},
               {   FunctionalEnumerations::Health::BacteriumType::CB,
                                             {{ FunctionalEnumerations::Health::MastitisSeverity::G0,  0.98f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G1,  0.018f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G2,  0.002f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G3,  0.00f}}}}},
         {FunctionalEnumerations::Health::MastitisSeverityDegree::majorated, 
               {{ FunctionalEnumerations::Health::BacteriumType::StaphA,
                                             {{ FunctionalEnumerations::Health::MastitisSeverity::G0,  0.86f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G1,  0.03f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G2,  0.07f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G3,  0.04f}}},
               {   FunctionalEnumerations::Health::BacteriumType::StreptU,
                                             {{ FunctionalEnumerations::Health::MastitisSeverity::G0,  0.25f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G1,  0.13f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G2,  0.49f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G3,  0.13f}}},
               {   FunctionalEnumerations::Health::BacteriumType::Gn,
                                             {{ FunctionalEnumerations::Health::MastitisSeverity::G0,  0.15f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G1,  0.205f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G2,  0.235f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G3,  0.41f}}},
               {   FunctionalEnumerations::Health::BacteriumType::CNS,
                                             {{ FunctionalEnumerations::Health::MastitisSeverity::G0,  0.90f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G1,  0.04f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G2,  0.055f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G3,  0.005}}},
               {   FunctionalEnumerations::Health::BacteriumType::CB,
                                             {{ FunctionalEnumerations::Health::MastitisSeverity::G0,  0.98f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G1,  0.013f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G2,  0.004f},
                                             {  FunctionalEnumerations::Health::MastitisSeverity::G3,  0.003f}}}}}};

    // SCC level (2.2.3.1.1.1.3.1)
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType,std::map<FunctionalEnumerations::Health::MastitisSeverity, float>> MIN_MASTITIS_SCC_LEVEL
           {{  FunctionalEnumerations::Health::BacteriumType::StaphA,
                                         {{ FunctionalEnumerations::Health::MastitisSeverity::G0,  1320.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G1,  1000.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G2,  7700.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G3, 27000.0f}}},
           {   FunctionalEnumerations::Health::BacteriumType::StreptU,
                                         {{ FunctionalEnumerations::Health::MastitisSeverity::G0,  2760.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G1,  2000.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G2,  7500.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G3, 26000.0f}}},
           {   FunctionalEnumerations::Health::BacteriumType::Gn,
                                         {{ FunctionalEnumerations::Health::MastitisSeverity::G0,  2430.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G1,  2500.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G2,  7500.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G3, 26000.0f}}},
           {   FunctionalEnumerations::Health::BacteriumType::CNS,
                                         {{ FunctionalEnumerations::Health::MastitisSeverity::G0,   530.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G1,   700.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G2,  7500.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G3, 27000.0f}}},
           {   FunctionalEnumerations::Health::BacteriumType::CB,
                                         {{ FunctionalEnumerations::Health::MastitisSeverity::G0,   310.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G1,   500.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G2,  6000.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G3, 20000.0f}}}};                                         
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType,std::map<FunctionalEnumerations::Health::MastitisSeverity, float>> MAX_MASTITIS_SCC_LEVEL
           {{  FunctionalEnumerations::Health::BacteriumType::StaphA,
                                         {{ FunctionalEnumerations::Health::MastitisSeverity::G0, 3800.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G1, 27000.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G2, 47500.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G3, 55000.0f}}},
           {   FunctionalEnumerations::Health::BacteriumType::StreptU,
                                         {{ FunctionalEnumerations::Health::MastitisSeverity::G0, 6160.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G1, 26000.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G2, 47500.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G3, 55000.0f}}},
           {   FunctionalEnumerations::Health::BacteriumType::Gn,
                                         {{ FunctionalEnumerations::Health::MastitisSeverity::G0, 6700.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G1, 26000.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G2, 47500.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G3, 55000.0f}}},
           {   FunctionalEnumerations::Health::BacteriumType::CNS,
                                         {{ FunctionalEnumerations::Health::MastitisSeverity::G0, 2080.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G1, 27000.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G2, 47500.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G3, 55000.0f}}},
           {   FunctionalEnumerations::Health::BacteriumType::CB,
                                         {{ FunctionalEnumerations::Health::MastitisSeverity::G0, 1980.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G1, 20000.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G2, 34000.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G3, 40000.0f}}}};
    static inline const float ADDITIONAL_SCC_RATIO_MOST_LIKELY  = 3.0f;


    // Milk loss (2.2.3.1.1.1.3.2)
    // loss
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType,std::map<FunctionalEnumerations::Health::MastitisSeverity, float>> MAX_MASTITIS_MILK_PRODUCT_REDUCTION_PERCENT_DUE_TO_CLINICAL_HIGHT_EFFECT
           {{  FunctionalEnumerations::Health::BacteriumType::StaphA,
                                         {{ FunctionalEnumerations::Health::MastitisSeverity::G0, 0.00f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G1, 0.20f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G2, 0.40f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G3, 0.75f}}},
           {   FunctionalEnumerations::Health::BacteriumType::StreptU,
                                         {{ FunctionalEnumerations::Health::MastitisSeverity::G0, 0.00f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G1, 0.25f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G2, 0.40f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G3, 0.75f}}},
           {   FunctionalEnumerations::Health::BacteriumType::Gn,
                                         {{ FunctionalEnumerations::Health::MastitisSeverity::G0, 0.0f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G1, 0.01f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G2, 0.04f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G3, 1.10f}}},
           {   FunctionalEnumerations::Health::BacteriumType::CNS,
                                         {{ FunctionalEnumerations::Health::MastitisSeverity::G0, 0.00f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G1, 0.00f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G2, 0.40f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G3, 0.60f}}},
           {   FunctionalEnumerations::Health::BacteriumType::CB,
                                         {{ FunctionalEnumerations::Health::MastitisSeverity::G0, 0.00f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G1, 0.20f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G2, 0.40f},
                                         {  FunctionalEnumerations::Health::MastitisSeverity::G3, 0.60f}}}};
    // Duration
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType, unsigned int> MIN_MASTITIS_MILK_LOSS_DURATION
                                                {{FunctionalEnumerations::Health::BacteriumType::StaphA , 8},
                                                { FunctionalEnumerations::Health::BacteriumType::StreptU, 5},
                                                { FunctionalEnumerations::Health::BacteriumType::Gn     , 5},
                                                { FunctionalEnumerations::Health::BacteriumType::CNS    , 2},
                                                { FunctionalEnumerations::Health::BacteriumType::CB     , 2}};
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType, unsigned int> MAX_MASTITIS_MILK_LOSS_DURATION
                                                {{FunctionalEnumerations::Health::BacteriumType::StaphA , 10},
                                                { FunctionalEnumerations::Health::BacteriumType::StreptU, 10},
                                                { FunctionalEnumerations::Health::BacteriumType::Gn     , 15},
                                                { FunctionalEnumerations::Health::BacteriumType::CNS    , 10},
                                                { FunctionalEnumerations::Health::BacteriumType::CB     , 10}};
    // G3 Quarter loss probability
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType, float> G3_MASTITIS_QUARTER_LOSS_PROBABILITY       {{FunctionalEnumerations::Health::BacteriumType::StaphA,    0.02f},
                                                                                    { FunctionalEnumerations::Health::BacteriumType::StreptU,   0.06f},
                                                                                    { FunctionalEnumerations::Health::BacteriumType::Gn,        0.02f},
                                                                                    { FunctionalEnumerations::Health::BacteriumType::CNS,       0.02f},
                                                                                    { FunctionalEnumerations::Health::BacteriumType::CB,        0.01f}};
                                                                                    
    // Feed impact (2.2.3.1.1.1.3.3)
    static inline const float MASTITIS_MILK_LOSS_QUANTITY_FOR_ONE_KG_FEED = 8.0f;
    static inline const float MASTITIS_MILK_LOSS_FACTOR_DUE_QUARTER_LOSS = 1.0f/2.0f;

    // G3 Mortality and culling probability (2.2.3.1.1.1.3.4)
    // Mortality
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType, float> G3_MASTITIS_MORTALITY_PROBABILITY          {{FunctionalEnumerations::Health::BacteriumType::StaphA,    0.02f},
                                                                                    { FunctionalEnumerations::Health::BacteriumType::StreptU,   0.01f},
                                                                                    { FunctionalEnumerations::Health::BacteriumType::Gn,        0.05f},
                                                                                    { FunctionalEnumerations::Health::BacteriumType::CNS,       0.0f},
                                                                                    { FunctionalEnumerations::Health::BacteriumType::CB,        0.0f}};

    // Culling
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType, float> G3_MASTITIS_CULLING_PROBABILITY            {{FunctionalEnumerations::Health::BacteriumType::StaphA,    0.02f},
                                                                                    { FunctionalEnumerations::Health::BacteriumType::StreptU,   0.01f},
                                                                                    { FunctionalEnumerations::Health::BacteriumType::Gn,        0.08f},
                                                                                    { FunctionalEnumerations::Health::BacteriumType::CNS,       0.0f},
                                                                                    { FunctionalEnumerations::Health::BacteriumType::CB,        0.0f}};

    // Refractory period duration (2.2.3.1.1.2)
    static inline const unsigned int MASTITIS_REFRACTORY_PERIOD_DURATION = 7;

    // Mastitis risk (2.2.3.1.1.2.1)                                                                                
    // Period durations in day 
    static inline const unsigned int MAX_DRY_DURATION_FOR_POST_DRY_MASTITIS_RISK = 21;
    static inline const unsigned int MAX_CALVING_PROXIMITY_FOR_INVOLUED_DRY_MASTITIS_RISK = 7;
    static inline const unsigned int MAX_CALVING_DELAY_FOR_PERI_PARTUM_MASTITIS_RISK = 45;
    
    // For calculation of the basis incidence probability of mastitis in risk periods
    static inline const unsigned int REFERENCE_MASTITIS_RISK_LACTATION_DURATION = 305;
    static inline const unsigned int REFERENCE_MASTITIS_RISK_DRY_OFF_DURATION = 60;
    static inline const unsigned int REFERENCE_MASTITIS_RISK_CYCLE_DURATION = REFERENCE_MASTITIS_RISK_LACTATION_DURATION + REFERENCE_MASTITIS_RISK_DRY_OFF_DURATION;
    static inline const unsigned int REFERENCE_PERIPARTUM_MASTITIS_RISK_PERIOD_DURATION = MAX_CALVING_PROXIMITY_FOR_INVOLUED_DRY_MASTITIS_RISK + MAX_CALVING_DELAY_FOR_PERI_PARTUM_MASTITIS_RISK;// ( = 52)
    static inline const unsigned int REFERENCE_PERIPARTUM_MASTITIS_RISK_PERIOD_PEAK = MAX_CALVING_PROXIMITY_FOR_INVOLUED_DRY_MASTITIS_RISK + 1;// ( = 8)
    static inline const unsigned int REFERENCE_DRY_MASTITIS_RISK_PERIOD_PEAK = 5;
    static inline const unsigned int REFERENCE_DRYINGINVOLUTED_MASTITIS_RISK_PERIOD_DURATION = REFERENCE_MASTITIS_RISK_DRY_OFF_DURATION - MAX_DRY_DURATION_FOR_POST_DRY_MASTITIS_RISK - MAX_CALVING_PROXIMITY_FOR_INVOLUED_DRY_MASTITIS_RISK; // (=32)
    static inline const unsigned int REFERENCE_LACTATION_MASTITIS_RISK_PERIOD_DURATION = REFERENCE_MASTITIS_RISK_LACTATION_DURATION - MAX_CALVING_DELAY_FOR_PERI_PARTUM_MASTITIS_RISK; // (=260)
    static inline const unsigned int REFERENCE_PERIPARTUM_MASTITIS_RISK_PERIOD_LAMBDA = 10;
    static inline const unsigned int REFERENCE_DRY_MASTITIS_RISK_PERIOD_LAMBDA = 4;

    // Basis Risk period ratio (Ecomast)
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType,std::map<FunctionalEnumerations::Health::MastitisRiskPeriodType, float>> BASIS_MASTITIS_RISK_PERIOD_RATIO
               {{ FunctionalEnumerations::Health::BacteriumType::StaphA,
                                             {{ FunctionalEnumerations::Health::MastitisRiskPeriodType::periPartumAndLactationBegin,             0.42},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::lactation,                               0.31f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingOff,                               0.2f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingInvoluted,                         0.07f}}},
               {   FunctionalEnumerations::Health::BacteriumType::StreptU,
                                             {{ FunctionalEnumerations::Health::MastitisRiskPeriodType::periPartumAndLactationBegin,             0.51f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::lactation,                               0.22f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingOff,                               0.2f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingInvoluted,                         0.07f}}},
               {   FunctionalEnumerations::Health::BacteriumType::Gn,
                                             {{ FunctionalEnumerations::Health::MastitisRiskPeriodType::periPartumAndLactationBegin,             0.615f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::lactation,                               0.235f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingOff,                               0.1f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingInvoluted,                         0.05f}}},
               {   FunctionalEnumerations::Health::BacteriumType::CNS,
                                             {{ FunctionalEnumerations::Health::MastitisRiskPeriodType::periPartumAndLactationBegin,             0.825f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::lactation,                               0.025f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingOff,                               0.1f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingInvoluted,                         0.05f}}},
               {   FunctionalEnumerations::Health::BacteriumType::CB,
                                             {{ FunctionalEnumerations::Health::MastitisRiskPeriodType::periPartumAndLactationBegin,             0.30f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::lactation,                               0.55f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingOff,                               0.08f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingInvoluted,                         0.07f}}}}; 
                                                                
    // Risk modulation (2.2.3.1.1.2.2)
                                                                
    // Milk production (2.2.3.1.1.2.2.1.1)
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType, std::tuple<float, float, float, float>> MASTITIS_MILK_PRODUCT_MODULATION
                                                {{FunctionalEnumerations::Health::BacteriumType::StaphA,        std::make_tuple(25.0f, 0.50f, 50.0f, 1.5f)},
                                                { FunctionalEnumerations::Health::BacteriumType::StreptU,       std::make_tuple(25.0f, 0.45f, 50.0f, 2.0f)},
                                                { FunctionalEnumerations::Health::BacteriumType::Gn,            std::make_tuple(25.0f, 0.45f, 50.0f, 1.8f)},
                                                { FunctionalEnumerations::Health::BacteriumType::CNS,           std::make_tuple(25.0f, 0.50f, 50.0f, 1.5f)},
                                                { FunctionalEnumerations::Health::BacteriumType::CB,            std::make_tuple(25.0f, 0.50f, 50.0f, 1.5f)}};

    // Lactation rank (2.2.3.1.1.2.2.1.2)
    static inline const std::map<FunctionalEnumerations::Health::MastitisLactationRankModulation, float> MASTITIS_LACTATION_RANK_MODULATION
                                    {{FunctionalEnumerations::Health::MastitisLactationRankModulation::heiferRankModulation,                   0.00f},
                                    { FunctionalEnumerations::Health::MastitisLactationRankModulation::primipareRankModulation,                0.75f},
                                    { FunctionalEnumerations::Health::MastitisLactationRankModulation::twoLactationsRankModulation,            1.00f},
                                    { FunctionalEnumerations::Health::MastitisLactationRankModulation::threeLactationsRankModulation,          1.00f},
                                    { FunctionalEnumerations::Health::MastitisLactationRankModulation::fourLactationsAndMoreRankModulation,    1.50f}};
                                    
    // Milk potential (2.2.3.1.1.2.2.1.3)
    static inline const std::tuple<float, float, float, float> MASTITIS_MILK_POTENTIAL_MODULATION {std::make_tuple(4000.0f, 0.8f, 12000.0f, 1.5f)};

    // Pre-existing SCC level (2.2.3.1.1.2.2.1.5)
    static inline const float SCC_LOW_LEVEL_MASTITIS_MODULATION = 75.0f;
    static inline const float SCC_HI_LEVEL_MASTITIS_MODULATION = 750.0f;
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType, std::tuple<float, float, float>> PRE_EXISTING_SCC_MASTITIS_MODULATION
                                                {{FunctionalEnumerations::Health::BacteriumType::StaphA,        std::make_tuple(1.0f, 1.0f, 0.8f)},
                                                { FunctionalEnumerations::Health::BacteriumType::StreptU,       std::make_tuple(1.0f, 1.0f, 0.8f)},
                                                { FunctionalEnumerations::Health::BacteriumType::Gn,            std::make_tuple(1.1f, 1.0f, 0.8f)},
                                                { FunctionalEnumerations::Health::BacteriumType::CNS,           std::make_tuple(1.0f, 1.0f, 0.8f)},
                                                { FunctionalEnumerations::Health::BacteriumType::CB,            std::make_tuple(1.0f, 1.0f, 0.8f)}};

    // Quarter dependancies (2.2.3.1.1.2.2.1.6)
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType, float> QUARTER_MASTITIS_PROBA_DEPENDANCIES   
                                                                                {{FunctionalEnumerations::Health::BacteriumType::StaphA,    3.2f},
                                                                                { FunctionalEnumerations::Health::BacteriumType::StreptU,   3.2f},
                                                                                { FunctionalEnumerations::Health::BacteriumType::Gn,        3.2f},
                                                                                { FunctionalEnumerations::Health::BacteriumType::CNS,       3.2f},
                                                                                { FunctionalEnumerations::Health::BacteriumType::CB,        3.2f}};
                                                                                    
    // Other diseases effect on mastitis (2.2.3.1.1.2.2.1.7)
    static inline const float KETOSIS_MASTITIS_RISK = 1.61f;
    static inline const float LAMENESS_MASTITIS_RISK = 1.44f;

               
    // Batch contagion (2.2.3.1.1.2.2.2.2)
    static inline const unsigned int PREVIOUS_DAY_FOR_MASTITIS_BATCH_CONTAGION = 7;
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType, float> MASTITIS_BATCH_CONTAGION_EFFECT        {{FunctionalEnumerations::Health::BacteriumType::StaphA,    0.15f},
                                                                                                                { FunctionalEnumerations::Health::BacteriumType::StreptU,   0.08f},
                                                                                                                { FunctionalEnumerations::Health::BacteriumType::Gn,        0.05f},
                                                                                                                { FunctionalEnumerations::Health::BacteriumType::CNS,       0.10f},
                                                                                                                { FunctionalEnumerations::Health::BacteriumType::CB,        0.15f}};

    // Basis spontaneous healing (2.2.3.1.1.4.1.1)
    static inline const unsigned int G0_MASTITIS_DURATION_BEFORE_SPONTANEOUS_HEALING = 50;
    static inline const unsigned int G0_MASTITIS_PERSISTANCE = 200;
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType,std::map<FunctionalEnumerations::Health::MastitisRiskPeriodType, float>> MASTITIS_SPONTANEOUS_HEALING_PROBABILITY
               {{  FunctionalEnumerations::Health::BacteriumType::StaphA,
                                             {{ FunctionalEnumerations::Health::MastitisRiskPeriodType::periPartumAndLactationBegin,    0.2f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::lactation,                      0.2f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingOff,                      0.35f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingInvoluted,                0.2f}}},
               {   FunctionalEnumerations::Health::BacteriumType::StreptU,
                                             {{ FunctionalEnumerations::Health::MastitisRiskPeriodType::periPartumAndLactationBegin,    0.3f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::lactation,                      0.3f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingOff,                      0.45f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingInvoluted,                0.3f}}},
               {   FunctionalEnumerations::Health::BacteriumType::Gn,
                                             {{ FunctionalEnumerations::Health::MastitisRiskPeriodType::periPartumAndLactationBegin,    0.9f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::lactation,                      0.9f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingOff,                      0.9f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingInvoluted,                0.9f}}},
               {   FunctionalEnumerations::Health::BacteriumType::CNS,
                                             {{ FunctionalEnumerations::Health::MastitisRiskPeriodType::periPartumAndLactationBegin,    0.2f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::lactation,                      0.2f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingOff,                      0.7f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingInvoluted,                0.65f}}},
               {   FunctionalEnumerations::Health::BacteriumType::CB,
                                             {{ FunctionalEnumerations::Health::MastitisRiskPeriodType::periPartumAndLactationBegin,    0.2f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::lactation,                      0.2f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingOff,                      0.6f},
                                             {  FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingInvoluted,                0.5f}}}};
                                             
    // Preexisting mastitis healing with new mastitis occurrence healing (2.2.3.1.1.4.1.2)
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType,std::map<FunctionalEnumerations::Health::BacteriumType, float>> SUBSISTING_MASTITIS_WITH_OCCURRENCY_MASTITIS_HEALING_PROBABILITY
               {{  FunctionalEnumerations::Health::BacteriumType::StaphA,
                                             {{ FunctionalEnumerations::Health::BacteriumType::StreptU,   1.0f},
                                             {  FunctionalEnumerations::Health::BacteriumType::Gn,        0.5f},
                                             {  FunctionalEnumerations::Health::BacteriumType::CNS,       0.5f},
                                             {  FunctionalEnumerations::Health::BacteriumType::CB,        0.5f}}},
               {   FunctionalEnumerations::Health::BacteriumType::StreptU,
                                             {{ FunctionalEnumerations::Health::BacteriumType::StaphA,    1.0f},
                                             {  FunctionalEnumerations::Health::BacteriumType::Gn,        0.5f},
                                             {  FunctionalEnumerations::Health::BacteriumType::CNS,       0.1f},
                                             {  FunctionalEnumerations::Health::BacteriumType::CB,        0.3f}}},
               {   FunctionalEnumerations::Health::BacteriumType::Gn,
                                             {{ FunctionalEnumerations::Health::BacteriumType::StaphA,    1.0f},
                                             {  FunctionalEnumerations::Health::BacteriumType::StreptU,   1.0f},
                                             {  FunctionalEnumerations::Health::BacteriumType::CNS,       0.1f},
                                             {  FunctionalEnumerations::Health::BacteriumType::CB,        0.3f}}},
               {   FunctionalEnumerations::Health::BacteriumType::CNS,
                                             {{ FunctionalEnumerations::Health::BacteriumType::StaphA,    1.0f},
                                             {  FunctionalEnumerations::Health::BacteriumType::StreptU,   1.0f},
                                             {  FunctionalEnumerations::Health::BacteriumType::Gn,        0.5f},
                                             {  FunctionalEnumerations::Health::BacteriumType::CB,        0.7f}}},
               {   FunctionalEnumerations::Health::BacteriumType::CB,
                                             {{ FunctionalEnumerations::Health::BacteriumType::StaphA,    1.0f},
                                             {  FunctionalEnumerations::Health::BacteriumType::StreptU,   1.0f},
                                             {  FunctionalEnumerations::Health::BacteriumType::Gn,        0.5f},
                                             {  FunctionalEnumerations::Health::BacteriumType::CNS,       0.7f}}}};
                                          
    // G0 Flare-up (2.2.3.1.1.4.2.1)                                                                                
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType, float> G0_FLARE_UP_PROBABILITY    {{FunctionalEnumerations::Health::BacteriumType::StaphA,    0.3f},
                                                                    { FunctionalEnumerations::Health::BacteriumType::StreptU,   0.6f},
                                                                    { FunctionalEnumerations::Health::BacteriumType::Gn,        0.9f},
                                                                    { FunctionalEnumerations::Health::BacteriumType::CNS,       0.1f},
                                                                    { FunctionalEnumerations::Health::BacteriumType::CB,        0.0f}};

    // G1 Flare-up (2.2.3.1.1.4.2.2)                                                                                
    static inline const float G1_FLARE_UP_PROBABILITY = 0.1f;
    static inline const unsigned int G1_FLARE_UP_DELAY = 2;
    static inline const unsigned int MASTITIS_G1_TO_G0_DELAY = 5;    
                       
   // Relapse probability (2.2.3.1.1.4.4)                                   
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType, float> MASTITIS_RELAPSE_PROBABILITY
                                             {{ FunctionalEnumerations::Health::BacteriumType::StaphA,      0.5f},
                                             {  FunctionalEnumerations::Health::BacteriumType::StreptU,     0.5f},
                                             {  FunctionalEnumerations::Health::BacteriumType::Gn,          0.1f},
                                             {  FunctionalEnumerations::Health::BacteriumType::CNS,         0.1f},
                                             {  FunctionalEnumerations::Health::BacteriumType::CB,          0.1f}};
    static inline const unsigned int MAX_MASTITIS_DELAY_FOR_RELAPSE = 245;
    static inline const unsigned int ALPHA_FOR_RELAPSE = 2;
    static inline const unsigned int BETA_FOR_RELAPSE = 4;
           
    // G1 detection probability (2.2.3.1.2.1)
    static inline const float G1_MASTITIS_DEFAULT_DETECTION_SENSIBILITY = 0.50f;
    static inline const unsigned int VET_CARE_CONTRACT_DURATION_FOR_FARMER_G1_MASTITIS_DETECTION_PROBABILITY = 6; // months
    static inline const float G1_MASTITIS_DETECTION_PROBABILITY_IF_CARE_VET_CONTRACT = 1.0f;                                                                                 

    // Saisonality (2.2.3.1.2.2)
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType,std::map<FunctionalEnumerations::Global::Month, float>> DEFAULT_MASTITIS_SAISONALITY_PROBABILITY_FACTOR
               {{  FunctionalEnumerations::Health::BacteriumType::StaphA,    {{ FunctionalEnumerations::Global::Month::jan, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::feb, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::mar, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::apr, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::may, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::jun, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::jul, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::aug, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::sep, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::oct, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::nov, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::dec, 1.0f}}},
               {   FunctionalEnumerations::Health::BacteriumType::StreptU,
                                             {{ FunctionalEnumerations::Global::Month::jan, 1.2f},
                                             {  FunctionalEnumerations::Global::Month::feb, 1.2f},
                                             {  FunctionalEnumerations::Global::Month::mar, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::apr, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::may, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::jun, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::jul, 1.2f},
                                             {  FunctionalEnumerations::Global::Month::aug, 1.2f},
                                             {  FunctionalEnumerations::Global::Month::sep, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::oct, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::nov, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::dec, 1.2f}}},
               {   FunctionalEnumerations::Health::BacteriumType::Gn,
                                             {{ FunctionalEnumerations::Global::Month::jan, 1.5f},
                                             {  FunctionalEnumerations::Global::Month::feb, 1.5f},
                                             {  FunctionalEnumerations::Global::Month::mar, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::apr, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::may, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::jun, 1.5f},
                                             {  FunctionalEnumerations::Global::Month::jul, 1.5f},
                                             {  FunctionalEnumerations::Global::Month::aug, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::sep, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::oct, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::nov, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::dec, 1.5f}}},
               {   FunctionalEnumerations::Health::BacteriumType::CNS,
                                             {{ FunctionalEnumerations::Global::Month::jan, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::feb, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::mar, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::apr, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::may, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::jun, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::jul, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::aug, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::sep, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::oct, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::nov, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::dec, 1.0f}}},
               {   FunctionalEnumerations::Health::BacteriumType::CB,
                                             {{ FunctionalEnumerations::Global::Month::jan, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::feb, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::mar, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::apr, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::may, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::jun, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::jul, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::aug, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::sep, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::oct, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::nov, 1.0f},
                                             {  FunctionalEnumerations::Global::Month::dec, 1.0f}}}};    
                                             
    // Bacterium Mastitis incidence part (2.2.3.1.2.3)                                       
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType, float> DEFAULT_BACTERIUM_MASTITIS_INCIDENCE_PART
                                                                                                    {{FunctionalEnumerations::Health::BacteriumType::StaphA,    0.283f},
                                                                                                    { FunctionalEnumerations::Health::BacteriumType::StreptU,   0.351f},
                                                                                                    { FunctionalEnumerations::Health::BacteriumType::Gn,        0.027f},
                                                                                                    { FunctionalEnumerations::Health::BacteriumType::CNS,       0.226f},
                                                                                                    { FunctionalEnumerations::Health::BacteriumType::CB,        0.113f}};

    // Mastitis treatment plan (2.2.3.1.2.4)
    // SCC level for preventive dryoff mastitis treatment                              
    static inline const float SCC_LEVEL_FOR_PREVENTIVE_DRYOFF_TREATMENT = 200.0f;
    
    // Default lactation mastitis treatment
    static inline const std::string DEFAULT_MASTITIS_LACTATION_TREATMENT_NAME = "Default Mastitis wide range treatment";
    static inline const unsigned int DEFAULT_MASTITIS_LACTATION_TREATMENT_EFFECT_DELAY = 2;
    static inline const unsigned int DEFAULT_MASTITIS_LACTATION_TREATMENT_MILK_WAITING_TIME = 4;
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType, float> DEFAULT_MASTITIS_LACTATION_TREATMENT_HEALING_PROBABILITY
                                                                                                    {{FunctionalEnumerations::Health::BacteriumType::StaphA,    0.45f},
                                                                                                    { FunctionalEnumerations::Health::BacteriumType::StreptU,   0.70f},
                                                                                                    { FunctionalEnumerations::Health::BacteriumType::Gn,        0.95f},
                                                                                                    { FunctionalEnumerations::Health::BacteriumType::CNS,       0.85f},
                                                                                                    { FunctionalEnumerations::Health::BacteriumType::CB,        0.90f}};        
                                                                                                    
    
    static inline const float DEFAULT_MASTITIS_LACTATION_FLARE_UP_RISK = 1.0f;                                                                                 
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType, float> DEFAULT_MASTITIS_LACTATION_TREATMENT_PROTECT_PROBABILITY
                                                                                                    {{FunctionalEnumerations::Health::BacteriumType::StaphA,    0.0f},
                                                                                                    { FunctionalEnumerations::Health::BacteriumType::StreptU,   0.0f},
                                                                                                    { FunctionalEnumerations::Health::BacteriumType::Gn,        0.0f},
                                                                                                    { FunctionalEnumerations::Health::BacteriumType::CNS,       0.0f},
                                                                                                    { FunctionalEnumerations::Health::BacteriumType::CB,        0.0f}};     
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType, unsigned int> DEFAULT_MASTITIS_LACTATION_TREATMENT_PROTECT_DURATION
                                                                                                    {{FunctionalEnumerations::Health::BacteriumType::StaphA,    0},
                                                                                                    { FunctionalEnumerations::Health::BacteriumType::StreptU,   0},
                                                                                                    { FunctionalEnumerations::Health::BacteriumType::Gn,        0},
                                                                                                    { FunctionalEnumerations::Health::BacteriumType::CNS,       0},
                                                                                                    { FunctionalEnumerations::Health::BacteriumType::CB,        0}};
    // Default dry mastitis treatment
    static inline const std::string DEFAULT_MASTITIS_DRY_TREATMENT_NAME = "Default Mastitis AB BP treatment";
    static inline const unsigned int DEFAULT_MASTITIS_DRY_TREATMENT_EFFECT_DELAY = 1;
    static inline const unsigned int DEFAULT_MASTITIS_DRY_TREATMENT_MILK_WAITING_TIME = 14;
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType, float> DEFAULT_MASTITIS_DRY_TREATMENT_HEALING_PROBABILITY
                                                                                            {{FunctionalEnumerations::Health::BacteriumType::StaphA,    0.65f},
                                                                                            { FunctionalEnumerations::Health::BacteriumType::StreptU,   0.92f},
                                                                                            { FunctionalEnumerations::Health::BacteriumType::Gn,        1.00f},
                                                                                            { FunctionalEnumerations::Health::BacteriumType::CNS,       0.95f},
                                                                                            { FunctionalEnumerations::Health::BacteriumType::CB,        1.00f}};                                                                                           
    static inline const float DEFAULT_MASTITIS_DRY_FLARE_UP_RISK = 1.0f;                                                                                 
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType, float> DEFAULT_MASTITIS_DRY_TREATMENT_PROTECT_PROBABILITY
                                                                                            {{FunctionalEnumerations::Health::BacteriumType::StaphA,    0.598f},
                                                                                            { FunctionalEnumerations::Health::BacteriumType::StreptU,   0.586f},
                                                                                            { FunctionalEnumerations::Health::BacteriumType::Gn,        0.895f},
                                                                                            { FunctionalEnumerations::Health::BacteriumType::CNS,       0.943f},
                                                                                            { FunctionalEnumerations::Health::BacteriumType::CB,        0.781f}};                                                                                           
    static inline const std::map<FunctionalEnumerations::Health::BacteriumType, unsigned int> DEFAULT_MASTITIS_DRY_TREATMENT_PROTECT_DURATION
                                                                                            {{FunctionalEnumerations::Health::BacteriumType::StaphA,    72},
                                                                                            { FunctionalEnumerations::Health::BacteriumType::StreptU,   72},
                                                                                            { FunctionalEnumerations::Health::BacteriumType::Gn,        72},
                                                                                            { FunctionalEnumerations::Health::BacteriumType::CNS,       72},
                                                                                            { FunctionalEnumerations::Health::BacteriumType::CB,        72}};
                                                                                                    
    static inline const float ADDITIONAL_MASTITIS_HEALING_PROBABILITY_IF_CARE_VET_CONTRACT = 0.05f;                                                                                 
                                         
     // Default prevention modalities
    // Cow (2.2.3.1.2.5)    
    static inline const float DEFAULT_COW_MASTITIS_INCIDENCE_PREVENTION_FACTOR = 1.0f;
    static inline const float MIN_COW_MASTITIS_INCIDENCE_PREVENTION_FACTOR = 0.2f;
    static inline const float MAX_COW_MASTITIS_INCIDENCE_PREVENTION_FACTOR = 2.0f;
   // Herd (2.2.3.1.2.6)  
    static inline const float DEFAULT_HERD_MASTITIS_INCIDENCE_PREVENTION_FACTOR = 1.0f;
    static inline const float MIN_HERD_MASTITIS_INCIDENCE_PREVENTION_FACTOR = 0.2f;
    static inline const float MAX_HERD_MASTITIS_INCIDENCE_PREVENTION_FACTOR = 2.0f;
    // Effects on mastitis prevention of the care vet contract
    // See "DefaultFarmMastitisCalibration.xlsx" file to know how to set value
//    static inline const float PREVENTION_FACTOR_VALUES[3] = {0.4f, 1.0f, 1.7f};
    static inline const unsigned int MONTHS_FOR_PREVENTION_EVOLUTION[3] = {0, 24, 42};
    static inline const float PERCENT_PREVENTION_PROGRESS = 0.75f;
//    static inline const float MASTITIS_PREVENTION_MODULATION_FACTOR_VALUES[3][3] = {{1.0f,  1.3f,  1.75f},  // x 0
//                                                                                    {1.0f,  1.7f,  2.6f },  // x 1
//                                                                                    {1.0f,  1.4f,  1.8f }}; // x 2
//                                                                                 //   y0     y1     y2
   // Vet care contract (2.2.3.1.2.7)  
    static inline unsigned int MONTHLY_SCHEDULED_VET_ACTIVITY_FIRST_YEAR_COUNT = 2;
    static inline unsigned int MONTHLY_SCHEDULED_VET_ACTIVITY_NEXT_YEAR_COUNT = 1;
    static inline const FunctionalEnumerations::Health::VeterinaryCareContractType DEFAULT_VET_CARE_CONTRACT_OPTION = FunctionalEnumerations::Health::VeterinaryCareContractType::noCareContract;   
    
    // Ketosis (2.2.3.2)
    // -----------------
    
    // Ketosis effect on fertility (2.2.3.2.1.1.2.1.2)
    static inline const float KETOSIS_FERTILITY_FAILURE_RISK = 1.66f;
    static inline const unsigned int KETOSIS_DAYS_FOR_DECREASE_FERTILITY_FAILURE_RISK = 30;
    static inline const float KETOSIS_FERTILITY_FAILURE_RISK_IF_G1_SUCCESSFUL_TREATMENT = 1.3f;
    
    // Ketosis effet on ovarian activity (2.2.3.2.1.1.2.1.3), see KetosisEffects.xlsx file for more informations
    static inline const float FIRST_OESTRUS_DELAY_WHEN_KETOSIS = 9.0f * TechnicalConstants::FIRST_OESTRUS_DELAY_WHEN_KETOSIS_CALIBRATION_VALUE;
    static inline const float STANDARD_POSTPARTUM_CYCLE_KETOSIS_RESUMPTION_PROBABILITY = ((1.0f/0.67f) - 1.0f) * TechnicalConstants::KETOSIS_CYCLICITY_CALIBRATION_VALUE; // 8 weeks postpartum cycle resumption when Ketosis probability
    
    // Ketosis effect on milk yield (2.2.3.2.1.1.2.2)
    static inline const float KETOSIS_G1_MILK_PERCENT_QUANTITY_LOSS = -1.12f;
    static inline const unsigned int KETOSIS_G1_MILK_QUANTITY_LOSS_DURATION = 55;
    static inline const float KETOSIS_G1_MILK_TB_DELTA = 1.0f;
    static inline const float KETOSIS_G1_MILK_TP_DELTA = -1.0f;    
    static inline const float KETOSIS_G2_MILK_PERCENT_QUANTITY_LOSS = -2.01f;
    static inline const unsigned int KETOSIS_G2_MILK_QUANTITY_LOSS_DURATION = 55;
    static inline const float KETOSIS_G2_MILK_TB_DELTA = 3.1f;
    static inline const float KETOSIS_G2_MILK_TP_DELTA = -2.2f;    
        
    // Feed impact (2.2.3.2.1.1.2.3)
    static inline const float KETOSIS_MILK_LOSS_QUANTITY_FOR_ONE_KG_FEED = 2.0f;

    // Ketosis effect on mortality (2.2.3.2.1.1.2.4), episode based
    static inline const std::map<unsigned int, float> G1_KETOSIS_MORTALITY_RISK_DEPENDING_LACTATION_RANK   { {0, 0.0f  /100.0f},
                                                                                               {1, 0.0f  /100.0f},
                                                                                               {2, 0.63f /100.0f},
                                                                                               {3, 0.63f /100.0f},
                                                                                               {4, 0.63f /100.0f}};
    static inline const std::map<unsigned int, float> G2_KETOSIS_MORTALITY_RISK_DEPENDING_LACTATION_RANK   { {0, 0.0f  /100.0f},
                                                                                               {1, 0.0f  /100.0f},
                                                                                               {2, 0.7f  /100.0f},
                                                                                               {3, 1.7f  /100.0f},
                                                                                               {4, 2.5f  /100.0f}};
                                                                                               
    // Basis incidence (2.2.3.2.1.2.1)
    static inline const std::map<FunctionalEnumerations::Genetic::Breed, float> BASIS_KETOSIS_INCIDENCE { {FunctionalEnumerations::Genetic::Breed::montbeliarde,  0.35f},
                                                                                            {FunctionalEnumerations::Genetic::Breed::normande,      0.25f},
                                                                                            {FunctionalEnumerations::Genetic::Breed::primHolstein,  0.45f}};
    // Ketosis normalized risk curve spline points, calculated and saved for DHM use in file KETOSIS_RISK_CURVE_FILE_NAME with "UtilityForDHM" application
    static inline const std::vector<double> PERIOD_X_POINTS {  1.0f - 1.0f, 15.0f - 1.0f, 30.0f - 1.0f, 60.0f - 1.0f, 90.0f - 1.0f}; // -1.0f because of the 0 base
    static inline const std::vector<double> PERIOD_Y_POINTS {  22.0f,       20.0f,         8.0f,         0.3f,         0.0f};
    static inline const unsigned int KETOSIS_RISK_PERIOD_DURATION_SINCE_BEGIN_LACTATION = 90; // 3 month
                                                                                               
    // Lactation rank effect on incidence (2.2.3.2.1.2.2.2)
    static inline const std::vector<float> LACTATION_RANK_KETOSIS_INCIDENCE_FACTOR {0.8f, 1.0f, 1.6f};
    static inline const std::map<unsigned int, std::map<FunctionalEnumerations::Genetic::Breed, float>> LACTATION_RANK_KETOSIS_INCIDENCE
           {{  1,
                                    {{ FunctionalEnumerations::Genetic::Breed::montbeliarde,    BASIS_KETOSIS_INCIDENCE.find(FunctionalEnumerations::Genetic::Breed::montbeliarde)->second *    LACTATION_RANK_KETOSIS_INCIDENCE_FACTOR[0]},
                                    {  FunctionalEnumerations::Genetic::Breed::normande,        BASIS_KETOSIS_INCIDENCE.find(FunctionalEnumerations::Genetic::Breed::normande)->second *        LACTATION_RANK_KETOSIS_INCIDENCE_FACTOR[0]},
                                    {  FunctionalEnumerations::Genetic::Breed::primHolstein,    BASIS_KETOSIS_INCIDENCE.find(FunctionalEnumerations::Genetic::Breed::primHolstein)->second *    LACTATION_RANK_KETOSIS_INCIDENCE_FACTOR[0]}}},
           {   2,
                                    {{ FunctionalEnumerations::Genetic::Breed::montbeliarde,    BASIS_KETOSIS_INCIDENCE.find(FunctionalEnumerations::Genetic::Breed::montbeliarde)->second *    LACTATION_RANK_KETOSIS_INCIDENCE_FACTOR[1]},
                                    {  FunctionalEnumerations::Genetic::Breed::normande,        BASIS_KETOSIS_INCIDENCE.find(FunctionalEnumerations::Genetic::Breed::normande)->second *        LACTATION_RANK_KETOSIS_INCIDENCE_FACTOR[1]},
                                    {  FunctionalEnumerations::Genetic::Breed::primHolstein,    BASIS_KETOSIS_INCIDENCE.find(FunctionalEnumerations::Genetic::Breed::primHolstein)->second *    LACTATION_RANK_KETOSIS_INCIDENCE_FACTOR[1]}}},
           {   3,
                                    {{ FunctionalEnumerations::Genetic::Breed::montbeliarde,    BASIS_KETOSIS_INCIDENCE.find(FunctionalEnumerations::Genetic::Breed::montbeliarde)->second *    LACTATION_RANK_KETOSIS_INCIDENCE_FACTOR[2]},     
                                    {  FunctionalEnumerations::Genetic::Breed::normande,        BASIS_KETOSIS_INCIDENCE.find(FunctionalEnumerations::Genetic::Breed::normande)->second *        LACTATION_RANK_KETOSIS_INCIDENCE_FACTOR[2]},
                                    {  FunctionalEnumerations::Genetic::Breed::primHolstein,    BASIS_KETOSIS_INCIDENCE.find(FunctionalEnumerations::Genetic::Breed::primHolstein)->second *    LACTATION_RANK_KETOSIS_INCIDENCE_FACTOR[2]}}}};  

    // Other diseases effect on incidence (2.2.3.2.1.2.2.5)
    static inline const float KETOSIS_RISK_PROBA_FACTOR_WHEN_CLINICAL_MASTITIS_DURING_LACTATION = 1.9f;
    static inline const float KETOSIS_RISK_PROBA_FACTOR_WHEN_LAMENESS = 1.7f;
                                    
    // IVV effect on incidence (2.2.3.2.1.2.2.6)
    static inline const std::vector<unsigned int> IVV_KETOSIS_INCIDENCE_DURATIONS {13, 15};
    static inline const std::vector<float> IVV_KETOSIS_INCIDENCE_PROBA_EFFECT {0.86f, 1.0f, 1.4f};
    
    // First calving age effect on incidence (2.2.3.2.1.2.2.7)
    // May be to come...     
    
    // Prevention factor by using monensin bolus (2.2.3.2.1.2.2.8)
    static inline const float INCIDENCE_FACTOR_DUE_TO_MONENSIN_BOLUS_USE = 1.0f - 0.75f;
    static inline const unsigned int DELAY_BEFORE_CALVING_FOR_MONENSIN_BOLUS_USE = 30;
    static inline const unsigned int PROTECT_DURATION_OF_MONENSIN_BOLUS_TRAITMENT = 95;
    // Effects on ketosis prevention of the care vet contract 
    // See "DefaultFarmMastitisCalibration.xlsx" file to know how to set value
//    static inline const float KETOSIS_PREVENTION_MODULATION_FACTOR_VALUES[3][3] = {{1.0f,  1.4f,  2.7f },  // x 0
//                                                                                   {1.0f,  1.2f,  1.55f},  // x 1
//                                                                                   {1.0f,  1.2f,  1.5f }}; // x 2
//                                                                                //   y0     y1     y2
//                                    
    // Cetodetect (2.2.3.2.1.3.1.1)
    static inline const double CETODETECT_SENSITIVITY = 0.91f;
    static inline const double CETODETECT_SPECIFICITY = 0.88f;
#ifdef _LOG     
    static inline const std::vector<unsigned int> DAY_RANGES_FOR_KETOSIS_CALIBRATION {14, 30, 45, 60, 75, 100};
    static inline const FunctionalEnumerations::Global::Month MONTH_CALVING_BEGIN_FOR_KETOSIS_CALIBRATION = FunctionalEnumerations::Global::Month::sep;
    static inline const FunctionalEnumerations::Global::Month MONTH_CALVING_END_FOR_KETOSIS_CALIBRATION = FunctionalEnumerations::Global::Month::mar;
#endif // _LOG

    // Herd navigator (2.2.3.2.1.3.1.2)
    static inline const unsigned int HERD_NAVIGATOR_TEST_DAY_CYCLE = 3;
    static inline const double HERD_NAVIGATOR_SENSITIVITY = CETODETECT_SENSITIVITY;
    static inline const double HERD_NAVIGATOR_SPECIFICITY = CETODETECT_SPECIFICITY;
    
    // Ketosis blood test (2.2.3.2.1.3.1.3)
    static inline const unsigned int DAY_OF_MONTH_FOR_KETOSIS_BLOOD_TEST = 15;
    static inline const double KETOSIS_BLOOD_TEST_SENSITIVITY = 0.98f;
    static inline const double KETOSIS_BLOOD_TEST_SPECIFICITY = 0.95f;
    
    // Evolution and healing (2.2.3.2.1.4) :
    
    // Duration (2.2.3.2.1.4.1)
    static inline const unsigned int MIN_SUBCLINICAL_KETOSIS_DURATION = 2;
    static inline const unsigned int MAX_SUBCLINICAL_KETOSIS_DURATION = 20;
    static inline const unsigned int MEAN_SUBCLINICAL_KETOSIS_DURATION = 5;
    static inline const unsigned int TECHNICAL_SUBCLINICAL_KETOSIS_DURATION_BASE = 16; // To be sure to have integer values of days when multinomial application (see KetosisEffects.xlsx, tab "Duree")
    static inline const unsigned int CLINICAL_KETOSIS_DURATION = 5;
    
    // FlareUp (2.2.3.2.1.4.2)
    static inline const std::map<FunctionalEnumerations::Reproduction::DairyCowParity, float> SUBCLINICAL_G1_KETOSIS_FLARE_UP_PROBABILITY
                       {{FunctionalEnumerations::Reproduction::DairyCowParity::primipare,   0.05f},
                        {FunctionalEnumerations::Reproduction::DairyCowParity::multipare,   0.07f}};
    static inline const float KETOSIS_FLAREUP_RISK_FACTOR_WHEN_PREVIOUS_KETOSIS_INFECTION = 3.0f;
                        
    // Care (2.2.3.2.1.4.3)
    static inline const unsigned int KETOSIS_DURATION_REDUCTION_WITH_TREATMENT = 1;
    
    // Recurrency (2.2.3.2.1.4.4)
    static inline const unsigned int DELAY_AFTER_KETOSIS_END_FOR_SENSITIVITY = 2;
    static inline const float KETOSIS_RECURRENCY_PROBABILITY = 0.4f;
    static inline const float KETOSIS_MEAN_DAY_FOR_RECURRENCY = 15.0f;
    static inline const float KETOSIS_STANDARD_DEVIATION_FOR_RECURRENCY = 5.0f;
        
    // Ketosis incidence default prevention factor (2.2.3.2.2.1.1)
    static inline const float DEFAULT_KETOSIS_INCIDENCE_PREVENTION_FACTOR = 1.0f;
    static inline const float MIN_KETOSIS_INCIDENCE_PREVENTION_FACTOR = 0.2f;
    static inline const float MAX_KETOSIS_INCIDENCE_PREVENTION_FACTOR = 2.0f;
    
    // Prevention factor by using monensin bolus (2.2.3.2.2.1.2)
    static inline const bool DEFAULT_MONENSIN_BOLUS_USE = false;
    
    // CetoDetect usage default value (2.2.3.2.2.2.1)
    static inline const bool DEFAULT_CETODETECT_USAGE = false;

    // Ketosis for Herd Navigator option default value (2.2.3.2.2.2.2)
    static inline const bool DEFAULT_CETO_HERD_NAVIGATOR_OPTION = false;
    
    // Ketosis treatment plan (2.2.3.2.2.3)
    static inline const std::string DEFAULT_G1_KETOSIS_TREATMENT_NAME = "Default G1 ketosis treatment";
    static inline const unsigned int DEFAULT_G1_KETOSIS_TREATMENT_EFFECT_DELAY = 5;
    static inline const unsigned int DEFAULT_G1_KETOSIS_TREATMENT_MILK_WAITING_TIME = 0;
    static inline const float DEFAULT_G1_KETOSIS_TREATMENT_HEALING_PROBABILITY = 0.5f;                                                                                 
    static inline const float DEFAULT_G1_KETOSIS_TREATMENT_FLARE_UP_RISK = 0.54f;                                                                                 
    static inline const std::string DEFAULT_G2_KETOSIS_TREATMENT_NAME = "Default G2 ketosis treatment";
    static inline const unsigned int DEFAULT_G2_KETOSIS_TREATMENT_EFFECT_DELAY = 0;
    static inline const unsigned int DEFAULT_G2_KETOSIS_TREATMENT_MILK_WAITING_TIME = 3;
    static inline const float DEFAULT_G2_KETOSIS_TREATMENT_HEALING_PROBABILITY = 0.42f;                                                                                 
    static inline const float DEFAULT_G2_KETOSIS_TREATMENT_FLARE_UP_RISK = 1.0f;                                                                                 
    static inline const float ADDITIONAL_KETOSIS_HEALING_PROBABILITY_IF_CARE_VET_CONTRACT = 0.05f;                                                                                 
    
    // Lameness (2.2.3.3)
    // ------------------
    // Lameness effect on ovulation detection (2.2.3.3.1.1.3.1.1)
    static inline const std::map<bool, std::map<FunctionalEnumerations::Reproduction::DetectionMode, float>> G1_G2_LAMENESS_OESTRUS_DETECTION_FACTOR
           {{  true,  {{ FunctionalEnumerations::Reproduction::DetectionMode::detector, 1.0f},
                      {  FunctionalEnumerations::Reproduction::DetectionMode::robot,    1.0f},
                      {  FunctionalEnumerations::Reproduction::DetectionMode::farmer,   0.45f},
                      {  FunctionalEnumerations::Reproduction::DetectionMode::male,     1.0f}}},
           {   false, {{ FunctionalEnumerations::Reproduction::DetectionMode::detector, 0.98f},     
                      {  FunctionalEnumerations::Reproduction::DetectionMode::robot,    1.0f},
                      {  FunctionalEnumerations::Reproduction::DetectionMode::farmer,   0.70f},
                      {  FunctionalEnumerations::Reproduction::DetectionMode::male,     1.0f}}}};  
                        
    // Lameness effect on milk yield (2.2.3.3.1.1.3.2)
    static inline const float LAMENESS_PASTURE_I_G1_MILK_PERCENT_QUANTITY_LOSS = 0.0f;
    static inline const float LAMENESS_STABULATION_I_G1_MILK_PERCENT_QUANTITY_LOSS = -0.7f;
    static inline const float LAMENESS_I_G1_MILK_TB_DELTA = 0.3f;
    static inline const float LAMENESS_I_G1_MILK_TP_DELTA = 0.0f;    
    static inline const float LAMENESS_PASTURE_I_G2_MILK_PERCENT_QUANTITY_LOSS = 0.0f;
    static inline const float LAMENESS_STABULATION_I_G2_MILK_PERCENT_QUANTITY_LOSS = -0.7f;
    static inline const float LAMENESS_I_G2_MILK_TB_DELTA = 1.5f;
    static inline const float LAMENESS_I_G2_MILK_TP_DELTA = 0.0f;    
    static inline const float LAMENESS_NI_G1_MILK_PERCENT_QUANTITY_LOSS = -1.8f;
    static inline const float LAMENESS_NI_G1_MILK_TB_DELTA = 0.03f;
    static inline const float LAMENESS_NI_G1_MILK_TP_DELTA = 0.0f;    
    static inline const float LAMENESS_NI_G2_MILK_PERCENT_QUANTITY_LOSS = -2.5f;
    static inline const float LAMENESS_NI_G2_MILK_TB_DELTA = 1.5f;
    static inline const float LAMENESS_NI_G2_MILK_TP_DELTA = 0.0f;    
                        
    // Lameness impact on feeding (2.2.3.3.1.1.3.3)
    static inline const float LAMENESS_MILK_LOSS_QUANTITY_FOR_ONE_KG_FEED = 2.0f;
    
    // Lameness impact on culling and death (2.2.3.3.1.1.3.5)
    static inline const std::map<FunctionalEnumerations::Health::LamenessInfectiousStateType, std::map<FunctionalEnumerations::Health::LamenessSeverity, float>> LAMENESS_MORTALITY_PROBA
       {{  FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType,
            {{ FunctionalEnumerations::Health::LamenessSeverity::G1Lameness,    0.036f},
             {  FunctionalEnumerations::Health::LamenessSeverity::G2Lameness,   0.042f}}},
       {   FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType,
            {{ FunctionalEnumerations::Health::LamenessSeverity::G1Lameness,    0.0016f},
             {  FunctionalEnumerations::Health::LamenessSeverity::G2Lameness,   0.0016f}}}};  
    static inline const std::map<FunctionalEnumerations::Health::LamenessInfectiousStateType, std::map<FunctionalEnumerations::Health::LamenessSeverity, float>> LAMENESS_CULLING_PROBA
       {{  FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType,
            {{ FunctionalEnumerations::Health::LamenessSeverity::G1Lameness,    0.025f},
             {  FunctionalEnumerations::Health::LamenessSeverity::G2Lameness,   0.045f}}},
       {   FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType,
            {{ FunctionalEnumerations::Health::LamenessSeverity::G1Lameness,    0.0014f},
             {  FunctionalEnumerations::Health::LamenessSeverity::G2Lameness,   0.0014f}}}};  
    static inline const unsigned int LAMENESS_DELAY_FOR_CULLING_OR_DEATH = 15;
    static inline const float LAMENESS_CULLING_PRICE_FACTOR = 0.5f;
    static inline const unsigned int LAMENESS_DURATION_WHEN_DRYING_TO_NOT_ENGAGE_INSEMINATION = 150;
    
    // Basis incidence (2.2.3.3.1.2.1)
    // All periods, delta to take into account for all periods
    static inline const std::map<FunctionalEnumerations::Health::LamenessInfectiousStateType, float> LAMENESS_BASIS_INCIDENCE_ALL_PERIODS 
            {{ FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType,         0.04f},
             { FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType,      0.04f}};
             
    static inline const std::map<FunctionalEnumerations::Health::LamenessInfectiousStateType, float> LAMENESS_BASIS_INCIDENCE_LACTATION_PERIOD
            {{ FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType,         0.20f - LAMENESS_BASIS_INCIDENCE_ALL_PERIODS.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second},
             { FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType,      0.15f - LAMENESS_BASIS_INCIDENCE_ALL_PERIODS.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second}};
                      
    // Year lameness risk = mean proportionnal of lactation and dry period
    static inline const std::map<FunctionalEnumerations::Health::LamenessInfectiousStateType, float> MEAN_YEAR_LAMENESS_BASIS_INCIDENCE 
            {{ FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType,        ((LAMENESS_BASIS_INCIDENCE_LACTATION_PERIOD.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second + LAMENESS_BASIS_INCIDENCE_ALL_PERIODS.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second) * Lactation::REFERENCE_LACTATION_DURATION + LAMENESS_BASIS_INCIDENCE_ALL_PERIODS.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second * Lactation::REFERENCE_DRY_DURATION) / (Lactation::REFERENCE_LACTATION_DURATION + Lactation::REFERENCE_DRY_DURATION)},
             { FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType,     ((LAMENESS_BASIS_INCIDENCE_LACTATION_PERIOD.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second + LAMENESS_BASIS_INCIDENCE_ALL_PERIODS.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second) * Lactation::REFERENCE_LACTATION_DURATION + LAMENESS_BASIS_INCIDENCE_ALL_PERIODS.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second * Lactation::REFERENCE_DRY_DURATION) / (Lactation::REFERENCE_LACTATION_DURATION + Lactation::REFERENCE_DRY_DURATION)}};
             
    // Lameness normalized risk curve settting, for lactation period, calculated and saved for DHM use in file LAMENESS_RISK_CURVE_FILE_NAME with "UtilityForDHM" application
    static inline const unsigned int REFERENCE_LAMENESS_RISK_PERIOD_PEAK = 65;
    static inline const float REFERENCE_LAMENESS_RISK_PERIOD_LAMBDA = 4.5f;
    static inline const unsigned int REFERENCE_LAMENESS_RISK_PERIOD_SMOOTH_SAMPLE = 10;

    // Lactation rank effect on incidence (2.2.3.3.1.2.2.1.2)
    static inline const std::map<FunctionalEnumerations::Health::LamenessInfectiousStateType, std::vector<float>> LACTATION_RANK_LAMENESS_INCIDENCE_FACTOR
    {{  FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType,
        {{  1.0f,    // primiparous
            1.0f,    // P2
            1.8f,    // P3
            2.7f}}}, // P4+
    {   FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType,
        {{  1.0f,     // primiparous
            1.0f,     // P2
            1.1f,     // P3
            1.3f}}}}; // P4+
                        
    // Other diseases effect on incidence (2.2.3.3.1.2.2.1.4)
    static inline const float LAMENESS_RISK_PROBA_FACTOR_WHEN_G1_TO_G3_MASTITIS_CASE = 2.0f;
    static inline const float LAMENESS_RISK_PROBA_FACTOR_WHEN_G1_TO_G2_KETOSIS_CASE = 2.0f;
    
    // Batch prevalence effect on incidence (2.2.3.3.1.2.2.2.1)
    static inline const float BATCH_INFECTIOUS_LAMENESS_PREVALENCE_THRESHOLD = 0.1f;
    static inline const float HALF_BATCH_INFECTIOUS_LAMENESS_PREVALENCE_THRESHOLD = BATCH_INFECTIOUS_LAMENESS_PREVALENCE_THRESHOLD / 2.0f;
    static inline const unsigned int BATCH_INFECTIOUS_LAMENESS_PREVALENCE_FACTOR = 2.0f;
    
    // Season effect on incidence (2.2.3.3.1.2.2.2.2)
    static inline const unsigned int LAMENESS_SEASON_RISK_FACTOR_DAY_1 = 25;
    static inline const unsigned int LAMENESS_SEASON_RISK_FACTOR_DAY_2 = 75;
    static inline const unsigned int LAMENESS_SEASON_RISK_FACTOR_MAX_EFFECT = 2.0f;
    
    // Prevention actions (2.2.3.3.1.2.2.2.3)
    // - trimming
    static inline const float TRIMMING_PREVENTION_NON_INFECTIOUS_LAMENESS_INCIDENCE_FACTOR = 0.7f;
    static inline const unsigned int TRIMMING_PREVENTION_NON_INFECTIOUS_LAMENESS_INCIDENCE_DURATION_IN_DAYS = 2 * 30; // 2 months
    static inline const float TRIMMING_PREVENTION_INFECTIOUS_LAMENESS_INCIDENCE_FACTOR = 0.7f;
    static inline const unsigned int TRIMMING_PREVENTION_INFECTIOUS_LAMENESS_INCIDENCE_DURATION_IN_DAYS = 21; // 3 weeks
    // - foot bath
    static inline const float FOOT_BATH_PREVENTION_INFECTIOUS_LAMENESS_INCIDENCE_FACTOR = 0.89f;
    static inline const float INFECTIOUS_LAMENESS_G1_FOOT_BATH_CURATIVE_HEALING_RISK = 1.30f;
    static inline const unsigned int INFECTIOUS_LAMENESS_G1_FOOT_BATH_DURATION_BEFORE_CURATIVE_EFFECT = 5;
    static inline const float INFECTIOUS_LAMENESS_G2_FOOT_BATH_CURATIVE_HEALING_RISK = 1.30f;
    static inline const unsigned int INFECTIOUS_LAMENESS_G2_FOOT_BATH_DURATION_BEFORE_CURATIVE_EFFECT = 9;
    
    // Lameness detection (2.2.3.3.1.3)
    static inline const std::map<FunctionalEnumerations::Health::LamenessDetectionMode, std::map<FunctionalEnumerations::Health::LamenessSeverity, float>> LAMENESS_DETECTION_PROBA
       {{  FunctionalEnumerations::Health::LamenessDetectionMode::hiSensitiveFarmer,
            {{ FunctionalEnumerations::Health::LamenessSeverity::G1Lameness,    0.40f},
             { FunctionalEnumerations::Health::LamenessSeverity::G2Lameness,    0.95f}}},
       {  FunctionalEnumerations::Health::LamenessDetectionMode::lowSensitiveFarmer,
            {{ FunctionalEnumerations::Health::LamenessSeverity::G1Lameness,    0.20f},
             { FunctionalEnumerations::Health::LamenessSeverity::G2Lameness,    0.70f}}},
       {   FunctionalEnumerations::Health::LamenessDetectionMode::detector,
            {{ FunctionalEnumerations::Health::LamenessSeverity::G1Lameness,    0.90f},
             { FunctionalEnumerations::Health::LamenessSeverity::G2Lameness,    1.00f}}}};  
    static inline const unsigned int DELAY_MIN_FOR_LAMENESS_DETECTION = 5;
    static inline const unsigned int DELAY_MAX_FOR_LAMENESS_DETECTION = 10;
    
    // Evolution and healing (2.2.3.3.1.4)
    static inline const std::map<FunctionalEnumerations::Health::LamenessInfectiousStateType, std::map<FunctionalEnumerations::Health::LamenessSeverity, float>> LAMENESS_SPONTANEOUS_HEALING_PROBABILITY
       {{  FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType,
            {{ FunctionalEnumerations::Health::LamenessSeverity::G1Lameness,    0.50f},
             {  FunctionalEnumerations::Health::LamenessSeverity::G2Lameness,   0.00f}}},
       {   FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType,
            {{ FunctionalEnumerations::Health::LamenessSeverity::G1Lameness,    0.05f},
             {  FunctionalEnumerations::Health::LamenessSeverity::G2Lameness,   0.00f}}}};  
    static inline const std::map<FunctionalEnumerations::Health::LamenessInfectiousStateType, std::map<FunctionalEnumerations::Health::LamenessSeverity, unsigned int>> LAMENESS_SPONTANEOUS_HEALING_DELAY
       {{  FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType,
            {{ FunctionalEnumerations::Health::LamenessSeverity::G1Lameness,    15},
             {  FunctionalEnumerations::Health::LamenessSeverity::G2Lameness,   0}}},
       {   FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType,
            {{ FunctionalEnumerations::Health::LamenessSeverity::G1Lameness,    20},
             {  FunctionalEnumerations::Health::LamenessSeverity::G2Lameness,   0}}}};  
    static inline const float INFECTIOUS_LAMENESS_G1_FOOT_BATH_CURATIVE_HEALTH_PROBABILITY = LAMENESS_SPONTANEOUS_HEALING_PROBABILITY.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second.find(FunctionalEnumerations::Health::LamenessSeverity::G1Lameness)->second * INFECTIOUS_LAMENESS_G1_FOOT_BATH_CURATIVE_HEALING_RISK;
    static inline const float INFECTIOUS_LAMENESS_G2_FOOT_BATH_CURATIVE_HEALTH_PROBABILITY = LAMENESS_SPONTANEOUS_HEALING_PROBABILITY.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second.find(FunctionalEnumerations::Health::LamenessSeverity::G2Lameness)->second * INFECTIOUS_LAMENESS_G2_FOOT_BATH_CURATIVE_HEALING_RISK;
             
    // Flareup (2.2.3.3.1.4.2)
    static inline const std::map<FunctionalEnumerations::Health::LamenessInfectiousStateType, unsigned int> LAMENESS_FLAREUP_MINIMUM_DELAY
                       {{FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType, 70},
                        {FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType,    30}};
    static inline const std::map<FunctionalEnumerations::Health::LamenessInfectiousStateType, unsigned int> LAMENESS_FLAREUP_MAXIMUM_DELAY
                       {{FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType, 170},
                        {FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType,     90}};

    // Delay after lameness healing for new sensibility (2.2.3.3.1.4.3)
    static inline const unsigned int DELAY_AFTER_INFECTIOUS_HEALING_FOR_SENSIBILITY = 21;
    static inline const unsigned int DELAY_AFTER_NON_INFECTIOUS_HEALING_FOR_SENSIBILITY = 60;
    
    // Lameness incidence default prevention factor (2.2.3.3.2.1.1)
    static inline const std::map<FunctionalEnumerations::Health::LamenessInfectiousStateType, float> DEFAULT_LAMENESS_INCIDENCE_PREVENTION_FACTOR
                       {{FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType,   1.0f},
                        {FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType,   1.0f}};
    static inline const std::map<FunctionalEnumerations::Health::LamenessInfectiousStateType, float> MIN_LAMENESS_INCIDENCE_PREVENTION_FACTOR
                       {{FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType,   0.2f},
                        {FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType,   0.2f}};
    static inline const std::map<FunctionalEnumerations::Health::LamenessInfectiousStateType, float> MAX_LAMENESS_INCIDENCE_PREVENTION_FACTOR
                       {{FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType,   2.0f},
                        {FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType,   2.0f}};
                            
    // Foot bath option (2.2.3.3.2.1.2)
    static inline const bool DEFAULT_FOOT_BATH_OPTION = false;
    static inline const unsigned int DEFAULT_FOOT_BATH_EFFECT_DURATION = 21;
 
    // Foot bath frequency (2.2.3.3.2.1.3)
    static inline const std::map<FunctionalEnumerations::Population::Location, unsigned int> DEFAULT_FOOT_BATH_FREQUENCY
                           {{FunctionalEnumerations::Population::Location::stabulation ,            21},
                            {FunctionalEnumerations::Population::Location::fullPasture,             21},
                            {FunctionalEnumerations::Population::Location::halfStabulationPasture,  21}};
                            
    // Trimming option (2.2.3.3.2.1.4)
    static inline const FunctionalEnumerations::Health::LamenessPreventiveTrimmingOption DEFAULT_PREVENTIVE_TRIMMING_OPTION = FunctionalEnumerations::Health::LamenessPreventiveTrimmingOption::preventiveTrimmingOnceAYear;
    static inline const unsigned int DELAY_AFTER_CALVING_FOR_PREVENTIVE_TRIMMING = 61;
    static inline const unsigned int VET_CARE_CONTRACT_DURATION_FOR_LAMENESS_PREVENTION_ACTION = 1; // year
    
    // Detection mode (2.2.3.3.2.2)
    static inline const FunctionalEnumerations::Health::LamenessDetectionMode DEFAULT_LAMENESS_DETECTION_MODE = FunctionalEnumerations::Health::LamenessDetectionMode::lowSensitiveFarmer;
    static inline const unsigned int VET_CARE_CONTRACT_DURATION_FOR_FARMER_LAMENESS_DETECTION_SENSITIVITY = 6; // months
         
    // Grouping option (2.2.3.3.2.3)
    static inline const unsigned int DEFAULT_COW_COUNT_FOR_SMALL_TRIMMING_OR_CARE_GROUP = 10;    
    static inline const unsigned int VET_CARE_CONTRACT_TRIMMING_GROUP_SIZE = 5;
    static inline const unsigned int VET_CARE_CONTRACT_DURATION_FOR_CURATIVE_TRIMMING_GROUP_RESIZING = 6; // months
    
    // Lameness treatment plan (2.2.3.3.2.4)
    static inline const std::string DEFAULT_G1_NON_INFECTIOUS_LAMENESS_TREATMENT_NAME = "Pose d'une talonnette + AINS (G1)";
    static inline const unsigned int DEFAULT_G1_NON_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MIN = 3;
    static inline const unsigned int DEFAULT_G1_NON_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MAX = 7;
    static inline const unsigned int DEFAULT_G1_NON_INFECTIOUS_LAMENESS_TREATMENT_MILK_WAITING_TIME = 0;
    static inline const float DEFAULT_G1_NON_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_1 = 0.55f;                                                                                 
    static inline const float DEFAULT_G1_NON_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_2 = 0.45f;                                                                                 
    
    static inline const std::string DEFAULT_G2_NON_INFECTIOUS_LAMENESS_TREATMENT_NAME = "Pose d'une talonnette + AINS (G2)";
    static inline const unsigned int DEFAULT_G2_NON_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MIN = 7;
    static inline const unsigned int DEFAULT_G2_NON_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MAX = 14;
    static inline const unsigned int DEFAULT_G2_NON_INFECTIOUS_LAMENESS_TREATMENT_MILK_WAITING_TIME = 0;
    static inline const float DEFAULT_G2_NON_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_1 = 0.55f;                                                                                 
    static inline const float DEFAULT_G2_NON_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_2 = 0.25f;                                                                             
    
    static inline const std::string DEFAULT_G1_INFECTIOUS_LAMENESS_TREATMENT_NAME = "Pansement avec desinfectant (G1)";
    static inline const unsigned int DEFAULT_G1_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MIN = 3;
    static inline const unsigned int DEFAULT_G1_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MAX = 3;
    static inline const unsigned int DEFAULT_G1_INFECTIOUS_LAMENESS_TREATMENT_MILK_WAITING_TIME = 0;
    static inline const float DEFAULT_G1_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_1 = 0.90f;                                                                                 
    static inline const float DEFAULT_G1_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_2 = 0.90f;                                                                                 
    
    static inline const std::string DEFAULT_G2_INFECTIOUS_LAMENESS_TREATMENT_NAME = "Pansement avec desinfectant (G2)";
    static inline const unsigned int DEFAULT_G2_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MIN = 7;
    static inline const unsigned int DEFAULT_G2_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MAX = 7;
    static inline const unsigned int DEFAULT_G2_INFECTIOUS_LAMENESS_TREATMENT_MILK_WAITING_TIME = 0;
    static inline const float DEFAULT_G2_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_1 = 0.85f;                                                                                 
    static inline const float DEFAULT_G2_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_2 = 0.85f;    
    
    static inline const unsigned int DELAY_FOR_NEW_LAMENESS_TRAITMENT = 40;
};

/*
  _____                 _   _                            _              _       
 / ____|               | | (_)                          | |            | |      
| |  __  ___ _ __   ___| |_ _  ___    ___ ___  _ __  ___| |_ __ _ _ __ | |_ ___ 
| | |_ |/ _ \ '_ \ / _ \ __| |/ __|  / __/ _ \| '_ \/ __| __/ _` | '_ \| __/ __|
| |__| |  __/ | | |  __/ |_| | (__  | (_| (_) | | | \__ \ || (_| | | | | |_\__ \
 \_____|\___|_| |_|\___|\__|_|\___|  \___\___/|_| |_|___/\__\__,_|_| |_|\__|___/
                                                                                                                  
*/
// (2.2.4) 
// ------------------------------
class Genetic
{
public:
    // Common parameters for genetic value of phenotypic characters (2.2.4.1.1.1)
    // Heritability
    static inline const std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> H2 =
                                                    {{FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,   0.30f},
                                                    { FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,     0.50f},
                                                    { FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,     0.50f},
                                                    { FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,    0.02f},
                                                    { FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,   0.02f},
                                                    { FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,0.15f},
                                                    { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,    0.04f},
                                                    { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,   0.03f}};    
    // Heterosis
    static inline const std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> HETEROSIS =
                                                            {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,510.0f},
                                                            { FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,     0.15f},
                                                            { FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,     0.0f},
                                                            { FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,    0.07f},
                                                            { FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,   0.0f},
                                                            { FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,0.0f},
                                                            { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,    0.0f},
                                                            { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,   0.0f}};
    // Correlation (rho)
    static inline const std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter,std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float>> GENETIC_CORRELATION =
               {{{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,
                                                {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,       1.00f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,        -0.45f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,        -0.40f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,       -0.23f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,       0.26f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,    0.03f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,        0.26f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,       0.26f}}},
               {   FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,
                                                {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,      -0.45f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,         1.00f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,         0.60f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,        0.10f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,      -0.10f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,    0.035f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,       -0.07f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,      -0.07f}}},
               {   FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,
                                                {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,      -0.40f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,         0.60f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,         1.00f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,        0.10f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,      -0.10f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,   -0.29f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,       -0.02f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,      -0.02f}}},
               {   FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,
                                                {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,      -0.23f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,         0.10f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,         0.10f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,        1.00f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,      -0.24f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,    0.00f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,        0.23f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,       0.23f}}},
               {   FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,
                                                {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,       0.26f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,        -0.10f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,        -0.10f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,       -0.24f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,       1.00f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,    0.23f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,        0.37f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,       0.37f}}},
               {   FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,
                                                {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,       0.03f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,         0.035f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,        -0.29f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,        0.00f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,       0.23f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,    1.00f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,        0.09f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,       0.09f}}},
               {   FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,
                                                {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,       0.26f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,        -0.07f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,        -0.02f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,        0.23f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,       0.37f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,    0.09f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,        1.00f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,      -0.01f}}},
               {   FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,
                                                {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,       0.26f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,        -0.07f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,        -0.02f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,        0.23f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,       0.37f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,    0.09f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,       -0.01f},
                                                {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,       1.00f}}}}}; 

    // Breed parameters for genetic value of phenotypic characters (2.2.4.1.1.2)
    
    // Breed effect        
    static inline const std::map<FunctionalEnumerations::Genetic::Breed,std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float>> MEAN_PERFORMANCE_REFERENCE =
               {{  FunctionalEnumerations::Genetic::Breed::montbeliarde,
                                        {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,  7832.0f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,      38.9f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,      33.1f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,      1.0f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,     1.0f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,  1.0f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,      1.0f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,     1.0f}}},
               {   FunctionalEnumerations::Genetic::Breed::normande,
                                        {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,  7028.0f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,      42.1f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,      34.5f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,      1.0f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,     1.0f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,  1.0f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,      1.0f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,     1.0f}}},
               {   FunctionalEnumerations::Genetic::Breed::primHolstein,
                                        {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,  9518.0f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,      40.1f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,      31.9f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,      1.0f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,     1.0f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,  1.0f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,      1.0f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,     1.0f}}}};

    
    // standard deviation
    static inline const std::map<FunctionalEnumerations::Genetic::Breed,std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float>> SD =
               {{  FunctionalEnumerations::Genetic::Breed::montbeliarde,
                                        {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,  553.0f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,      2.03f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,      1.16f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,     0.059f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,    0.037f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait, 0.048f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,     0.00f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,    0.00f}}},
               {   FunctionalEnumerations::Genetic::Breed::normande,
                                        {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,  553.0f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,      2.03f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,      1.16f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,     0.059f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,    0.037f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait, 0.048f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,     0.00f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,    0.00f}}},
               {   FunctionalEnumerations::Genetic::Breed::primHolstein,
                                        {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,  599.0f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,      2.48f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,      1.24f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,     0.059f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,    0.034f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait, 0.046f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,     0.1f},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,    0.06f}}}};

    // Individual potential value (2.2.4.1.2.2)
    
    // Standard deviation factor
    // Used to calculate a factor based on a standard deviation. Rules :
    // - 0.0f = Not a standard deviation, but a quantity to manage
    // - > 0.0f = standard deviation to manage, increasing (factor from 1/X to X)
    // - < 0.0f = standard deviation to manage, decreasing (factor from X to 1/X)
    static inline const float MAX_STANDARD_DEVIATION = 3.0f;
    static inline const std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> SD_FACTOR =
                                                            {{FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,      0.0f},
                                                            { FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,        0.0f},
                                                            { FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,        0.0f},
                                                            { FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,       2.0f},
                                                            { FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,     -5.0f},
                                                            { FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,   1.5f},
                                                            { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,      -1.6f},
                                                            { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,     -1.6f}};

    
    // A constant value to 0.0f
    static inline const std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> NULL_PHENOTYPIC_VALUES =
                                                                                {{FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,   0.0f},
                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,     0.0f},
                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,     0.0f},
                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,    0.0f},
                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,   0.0f},
                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,0.0f},
                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,    0.0f},
                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,   0.0f}};

    // Male catalog (2.2.4.1.3.1)
    // Beef breed performances     
    static inline const std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> DEFAULT_PRODUCTION_FOR_BEEF_COW =
                                                                                 {{FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait, 2000.0f},
                                                                                 { FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,     50.0f},
                                                                                 { FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,     50.0f},
                                                                                 { FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,     1.0f},
                                                                                 { FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,    1.0f},
                                                                                 { FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait, 1.0f},
                                                                                 { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,     1.0f},
                                                                                 { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,    1.0f}};
    // Annual genetic progress (2.2.4.1.3.2)
    static inline const std::map<FunctionalEnumerations::Genetic::Breed,std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float>> ANNUAL_GENETIC_PROGRESS =
               {{  FunctionalEnumerations::Genetic::Breed::montbeliarde,
                                        {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,     52.0f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,        0.03f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,        0.08f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,       0.05f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,      0.04f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,   0.00f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,       0.00f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,      0.00f}}},
               {   FunctionalEnumerations::Genetic::Breed::normande,
                                        {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,     62.0f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,        0.0f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,        0.09f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,       0.02f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,      0.02f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,   0.00f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,       0.00f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,      0.00f}}},
               {   FunctionalEnumerations::Genetic::Breed::primHolstein,
                                        {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,     80.0f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,        0.0f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,        0.04f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,       0.05f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,      0.04f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,   0.00f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,       0.00f},
                                         { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,      0.00f}}}}; 

    // Day for the genetic progress
    static inline const unsigned int DAY_OF_THE_YEAR_FOR_GENETIC_PROGRESS = 1; // 01 jan

    // Meiose random (2.2.4.1.4.2)
    static inline const float SQRT_2 = sqrt(2);
    static inline const std::map<FunctionalEnumerations::Genetic::Breed,std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float>> SD_FOR_MEIOSE_RANDOM = 
               {{  FunctionalEnumerations::Genetic::Breed::montbeliarde,
                                        {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,      (SD.find(FunctionalEnumerations::Genetic::Breed::montbeliarde)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,        (SD.find(FunctionalEnumerations::Genetic::Breed::montbeliarde)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::TB)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,        (SD.find(FunctionalEnumerations::Genetic::Breed::montbeliarde)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::TP)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,       (SD.find(FunctionalEnumerations::Genetic::Breed::montbeliarde)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,      (SD.find(FunctionalEnumerations::Genetic::Breed::montbeliarde)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,   (SD.find(FunctionalEnumerations::Genetic::Breed::montbeliarde)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,        (SD.find(FunctionalEnumerations::Genetic::Breed::montbeliarde)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,        (SD.find(FunctionalEnumerations::Genetic::Breed::montbeliarde)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni)->second / SQRT_2}}},
               {   FunctionalEnumerations::Genetic::Breed::normande,
                                        {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,      (SD.find(FunctionalEnumerations::Genetic::Breed::normande)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,        (SD.find(FunctionalEnumerations::Genetic::Breed::normande)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::TB)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,        (SD.find(FunctionalEnumerations::Genetic::Breed::normande)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::TP)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,       (SD.find(FunctionalEnumerations::Genetic::Breed::normande)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,      (SD.find(FunctionalEnumerations::Genetic::Breed::normande)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,   (SD.find(FunctionalEnumerations::Genetic::Breed::normande)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,        (SD.find(FunctionalEnumerations::Genetic::Breed::normande)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,        (SD.find(FunctionalEnumerations::Genetic::Breed::normande)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni)->second / SQRT_2}}},
               {   FunctionalEnumerations::Genetic::Breed::primHolstein,
                                        {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,      (SD.find(FunctionalEnumerations::Genetic::Breed::primHolstein)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,        (SD.find(FunctionalEnumerations::Genetic::Breed::primHolstein)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::TB)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,        (SD.find(FunctionalEnumerations::Genetic::Breed::primHolstein)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::TP)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,       (SD.find(FunctionalEnumerations::Genetic::Breed::primHolstein)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,      (SD.find(FunctionalEnumerations::Genetic::Breed::primHolstein)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait,   (SD.find(FunctionalEnumerations::Genetic::Breed::primHolstein)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,        (SD.find(FunctionalEnumerations::Genetic::Breed::primHolstein)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi)->second / SQRT_2},
                                        {  FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,        (SD.find(FunctionalEnumerations::Genetic::Breed::primHolstein)->second).find(FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni)->second / SQRT_2}}}};

    // Breed dam/sire ratio when cross-breeding (2.2.4.1.4.3)
    static inline const float BREED_RATIO_WHEN_CROSS_BREEDING = 0.5f;

    // Male catalog setting (2.2.4.2.1)
    
    static inline const std::map<FunctionalEnumerations::Genetic::Breed,std::map<FunctionalEnumerations::Genetic::GeneticStrategy, float>> DEFAULT_GENETIC_LGF_MALE_INDEX_CATALOG 
                        {{ FunctionalEnumerations::Genetic::Breed::montbeliarde,
                                                 {{ FunctionalEnumerations::Genetic::GeneticStrategy::wellBalanced,                    -0.1f},
                                                  { FunctionalEnumerations::Genetic::GeneticStrategy::milkQuantityPriority,            -0.1f},
                                                  { FunctionalEnumerations::Genetic::GeneticStrategy::functionalLongevityPriority,      2.8f}}},
                        {   FunctionalEnumerations::Genetic::Breed::normande,
                                                 {{ FunctionalEnumerations::Genetic::GeneticStrategy::wellBalanced,                     0.5f},
                                                  { FunctionalEnumerations::Genetic::GeneticStrategy::milkQuantityPriority,             0.5f},
                                                  { FunctionalEnumerations::Genetic::GeneticStrategy::functionalLongevityPriority,      3.0f}}},
                        {   FunctionalEnumerations::Genetic::Breed::primHolstein,
                                                 {{ FunctionalEnumerations::Genetic::GeneticStrategy::wellBalanced,                     0.9f},
                                                  { FunctionalEnumerations::Genetic::GeneticStrategy::milkQuantityPriority,             2.1f},
                                                  { FunctionalEnumerations::Genetic::GeneticStrategy::functionalLongevityPriority,      3.6f}}}};

    
    static inline const std::map<FunctionalEnumerations::Genetic::Breed,std::map<FunctionalEnumerations::Genetic::GeneticStrategy, std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float>>> DEFAULT_MALE_GENETIC_CATALOG 
                        {{ FunctionalEnumerations::Genetic::Breed::montbeliarde,
                                                 {{ FunctionalEnumerations::Genetic::GeneticStrategy::wellBalanced,
                                                                                               {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait, 1643.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,     -1.1f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,      0.4f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,     0.5f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,   -0.7f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait, 0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,     0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,    0.0f}}},
                                                  { FunctionalEnumerations::Genetic::GeneticStrategy::milkQuantityPriority,
                                                                                               {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait, 1643.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,     -1.1f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,      0.4f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,     0.5f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,   -0.7f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait, 0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,     0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,    0.0f}}},
                                                  { FunctionalEnumerations::Genetic::GeneticStrategy::functionalLongevityPriority,
                                                                                               {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait, 1007.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,      0.1f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,     -0.1f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,     1.6f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,    0.4f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait, 0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,     0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,    0.0f}}}}},
                        {   FunctionalEnumerations::Genetic::Breed::normande,
                                                 {{ FunctionalEnumerations::Genetic::GeneticStrategy::wellBalanced,
                                                                                               {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait, 1552.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,      3.5f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,      1.9f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,     0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,    0.4f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait, 0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,     0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,    0.0f}}},
                                                  { FunctionalEnumerations::Genetic::GeneticStrategy::milkQuantityPriority,
                                                                                               {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait, 1552.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,      3.5f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,      1.9f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,     0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,    0.4f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait, 0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,     0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,    0.0f}}},
                                                  { FunctionalEnumerations::Genetic::GeneticStrategy::functionalLongevityPriority,
                                                                                               {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait, 1148.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,     -1.2f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,     -0.9f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,     1.7f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,    2.1f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait, 0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,     0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,    0.0f}}}}},
                        {   FunctionalEnumerations::Genetic::Breed::primHolstein,
                                                 {{ FunctionalEnumerations::Genetic::GeneticStrategy::wellBalanced,
                                                                                               {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait, 1173.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,      3.7f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,      3.3f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,     0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,   -0.1f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait, 0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,     0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,    0.0f}}},
                                                  { FunctionalEnumerations::Genetic::GeneticStrategy::milkQuantityPriority,
                                                                                               {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait, 1266.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,      1.6f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,      0.1f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,     2.2f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,    1.3f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait, 0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,     0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,    0.0f}}},
                                                  { FunctionalEnumerations::Genetic::GeneticStrategy::functionalLongevityPriority,
                                                                                               {{ FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait,  835.00f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TB,      1.7f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::TP,      2.2f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer,     2.7f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL,    0.9f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait, 0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi,     0.0f},
                                                                                                { FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni,    0.0f}}}}}};

    // Genetic strategy (2.2.4.2.2)
    static inline const FunctionalEnumerations::Genetic::GeneticStrategy DEFAULT_GENETIC_STRATEGY = FunctionalEnumerations::Genetic::GeneticStrategy::wellBalanced;

 };

/*
 _____                  _       _   _                                   _              _       
|  __ \                | |     | | (_)                                 | |            | |      
| |__) |__  _ __  _   _| | __ _| |_ _  ___  _ __     ___ ___  _ __  ___| |_ __ _ _ __ | |_ ___ 
|  ___/ _ \| '_ \| | | | |/ _` | __| |/ _ \| '_ \   / __/ _ \| '_ \/ __| __/ _` | '_ \| __/ __|
| |  | (_) | |_) | |_| | | (_| | |_| | (_) | | | | | (_| (_) | | | \__ \ || (_| | | | | |_\__ \
|_|   \___/| .__/ \__,_|_|\__,_|\__|_|\___/|_| |_|  \___\___/|_| |_|___/\__\__,_|_| |_|\__|___/
           | |                                                                                 
           |_|                                                                                 
*/
// (2.2.5) 
// ---------------------------------------------------
class Population
{
public:
    // Waiting delay for coming pregnant heifer before purchase new one (2.2.5.1.1.2)
    static inline const unsigned int DAY_OF_THE_MONTH_FOR_PREGNANT_HEIFER_NEED_CALCULATION = 15;
    static inline const unsigned int WAITING_DELAY_FOR_COMING_PREGNANT_HEIFER_BEFORE_PURCHASE_NEW_ONE = 120;

    // Culling (2.2.5.1.1.2.2.2)
    static inline const unsigned int LAST_PERIOD_DURATION_FOR_SCORE = 7;
    static inline const unsigned int LACTATION_STAGE_PROTECTION_DURATION_BEFORE_CULLING = 250;
//    static inline const float HIGH_LEVEL_MILK_PRODUCTION_CULLING = 9500.0f;
//    static inline const float HIGH_LEVEL_MILK_PRODUCTION_RENTABILITY = 13.0f;
//    static inline const float LOW_LEVEL_MILK_PRODUCTION_CULLING = 6000.0f;
//    static inline const float LOW_LEVEL_MILK_PRODUCTION_RENTABILITY = 9.0f;
    static inline const float COEFFICIENT_FOR_DETERMINING_LOW_QUANTITY_FOR_RENTABILITY = 1.1f/640.0f;
    static inline const float DEFAULT_MODULATION_FACTOR_FOR_LOW_QUANTITY_FOR_RENTABILITY = 1.0f;
    static inline const unsigned int DURATION_FOR_MINIMUM_MILK_QUANTITY_FOR_DECISION = 7;

    
    // Pregnant heifer count period (2.2.5.1.1.2.2.3)
    static inline const unsigned int PREGNANT_HEIFER_COUNT_PERIOD = WAITING_DELAY_FOR_COMING_PREGNANT_HEIFER_BEFORE_PURCHASE_NEW_ONE;
    
    // Heifer purchase (2.2.5.1.1.2.3)
    static inline const unsigned int DELAY_BEFORE_CALVING_FOR_BOUGHT_PREGNANT_HEIFERS = 14; // 2 weeks

    // Mortalite  (2.2.5.1.1.3)
    static inline const std::map<FunctionalEnumerations::Genetic::Sex, std::map<FunctionalEnumerations::Genetic::BreedType,std::map<FunctionalEnumerations::Population::MortalityRiskAge, float>>> GLOBAL_MORTALITY_PROBA // Day risk by age
        {{  FunctionalEnumerations::Genetic::Sex::female,
                                    {{ FunctionalEnumerations::Genetic::BreedType::dairy,                                                          // Some with calibration value, see "DeathCalculation.xlsx" file
                                                               {{   FunctionalEnumerations::Population::MortalityRiskAge::underSevenDays,          0.06f  /  7.0f}, // moins d'1 semaine
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::sevenDaysToOneMonth,     0.0296f/ (30.4f - 7.0f)}, // entre 1 semaine et 1 mois
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::oneToTwoMonths,          0.0126f/ (60.8f - 30.4f)}, // enre 1 et 2 mois
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::twoToSixMonths,          0.0231f/(182.5f - 60.8f)}, // entre 2 et 6 mois
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::sixMonthsToOneYear,      0.0152f/(365.0f - 182.5f)}, // entre 6 mois et 1 an
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::oneToTwoYears,           (0.0153f * TechnicalConstants::ONE_TO_TWO_YEARS_OLD_UNMANAGED_DEATH_CALIBRATION_RATIO)/365.0f}, // entre 1 ans et 2 ans
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::twoToThreeAndHalfYears,  (0.026f  * TechnicalConstants::TWO_TO_THREE_AND_HALF_YEARS_OLD_UNMANAGED_DEATH_CALIBRATION_RATIO) / (365.0f * 1.5f)}, // entre 2 et 3.5 ans
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::threeAndHalfToFiveYears, (0.0301f * TechnicalConstants::THREE_AND_HALF_TO_FIVE_YEARS_OLD_UNMANAGED_DEATH_CALIBRATION_RATIO) / (365.0f * 1.5f)}, // entre 3.5 et 5 ans
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::fiveToTenYears,          (0.0442f * TechnicalConstants::FIVE_TO_TEN_YEARS_OLD_UNMANAGED_DEATH_CALIBRATION_RATIO)/(365.0f * 5.0f)}, // entre 5 et 10 ans
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::OverTenYears,            (0.0754f * TechnicalConstants::OVER_TEN_YEARS_OLD_UNMANAGED_DEATH_CALIBRATION_RATIO)/365.0f}}}, // apres 10 ans
                                    {  FunctionalEnumerations::Genetic::BreedType::beef,
                                                               {{   FunctionalEnumerations::Population::MortalityRiskAge::underSevenDays,          0.0436f/  7.0f}, // moins d'1 semaine
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::sevenDaysToOneMonth,     0.0163f/ 30.4f}, // entre 1 semaine et 1 mois
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::oneToTwoMonths,          0.0088f/ 60.8f}, // enre 1 et 2 mois
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::twoToSixMonths,          0.0169f/182.5f}, // entre 2 et 6 mois
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::sixMonthsToOneYear,      0.0135f/365.0f}, // entre 6 mois et 1 an
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::oneToTwoYears,           0.0156f/365.0f}, // entre 1 ans et 2 ans
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::twoToThreeAndHalfYears,  0.015f /365.0f}, // entre 2 et 3.5 ans
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::threeAndHalfToFiveYears, 0.0167f/365.0f}, // entre 3.5 et 5 ans
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::fiveToTenYears,          0.0184f/365.0f}, // entre 5 et 10 ans
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::OverTenYears,            0.0428f/365.0f}}}}}, // apres 10 ans
        {   FunctionalEnumerations::Genetic::Sex::male,
                                   {{  FunctionalEnumerations::Genetic::BreedType::dairy,
                                                               {{   FunctionalEnumerations::Population::MortalityRiskAge::underSevenDays,          0.079f /  7.0f}, // moins d'1 semaine
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::sevenDaysToOneMonth,     0.0296f/ 30.4f}, // entre 1 semaine et 1 mois
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::oneToTwoMonths,          0.0126f/ 60.8f}, // enre 1 et 2 mois
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::twoToSixMonths,          0.0231f/182.5f}, // entre 2 et 6 mois
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::sixMonthsToOneYear,      0.0152f/365.0f}, // entre 6 mois et 1 an
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::oneToTwoYears,           0.0153f/365.0f}, // entre 1 ans et 2 ans
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::twoToThreeAndHalfYears,  0.026f /365.0f}, // entre 2 et 3.5 ans
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::threeAndHalfToFiveYears, 0.0301f/365.0f}, // entre 3.5 et 5 ans
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::fiveToTenYears,          0.0442f/365.0f}, // entre 5 et 10 ans
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::OverTenYears,            0.0754f/365.0f}}}, // apres 10 ans
                                  {  FunctionalEnumerations::Genetic::BreedType::beef,
                                                               {{   FunctionalEnumerations::Population::MortalityRiskAge::underSevenDays,          0.0436f/  7.0f}, // moins d'1 semaine
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::sevenDaysToOneMonth,     0.0163f/ 30.4f}, // entre 1 semaine et 1 mois
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::oneToTwoMonths,          0.0088f/ 60.8f}, // enre 1 et 2 mois
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::twoToSixMonths,          0.0169f/182.5f}, // entre 2 et 6 mois
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::sixMonthsToOneYear,      0.0135f/365.0f}, // entre 6 mois et 1 an
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::oneToTwoYears,           0.0156f/365.0f}, // entre 1 ans et 2 ans
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::twoToThreeAndHalfYears,  0.015f /365.0f}, // entre 2 et 3.5 ans
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::threeAndHalfToFiveYears, 0.0167f/365.0f}, // entre 3.5 et 5 ans
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::fiveToTenYears,          0.0184f/365.0f}, // entre 5 et 10 ans
                                                                {   FunctionalEnumerations::Population::MortalityRiskAge::OverTenYears,            0.0428f/365.0f}}}}}};  // apres 10 ans
                                                                
    static inline const float NON_MANAGED_MORTALITY_FACTOR_WITH_VET_CARE_CONTRACT = 0.9f;
    static inline unsigned int VET_CARE_CONTRACT_DURATION_FOR_FACTOR_APPLICATION = 1; // 1 year                                                           
                   
     // Pasture mimimum age (2.2.5.1.2.1)
    static inline const unsigned int MINIMUM_AGE_FOR_SALE = 14; // 2 weeks
    static inline const unsigned int AGE_TO_GO_IN_YOUNG_UNBRED_HEIFERS_BATCH = 245; // 8 months
    static inline const unsigned int AGE_TO_GO_IN_OLD_UNBRED_HEIFERS_BATCH = 610; // 20 months
    static inline const unsigned int DELAY_BEFORE_CALVING_TO_GO_IN_DRIED_COWS_BATCH_FOR_PREGNANT_HEIFERS = 30; // 1 month
    static inline const unsigned int DELAY_BEFORE_CALVING_TO_GO_IN_PERIPARTURIENT_BATCH = 7; // 1 week
    
    // Initial breed (2.2.5.2.1.1)
    static inline const FunctionalEnumerations::Genetic::Breed DEFAULT_INITIAL_COW_BREED = FunctionalEnumerations::Genetic::Breed::primHolstein;

    // Mean adult number (2.2.5.2.1.2)
    static inline const unsigned int DEFAULT_MEAN_ADULT_NUMBER_TARGET = 70;

    // Renewal ratio (2.2.5.2.1.3)
    static inline const float MIN_ANNUAL_HERD_RENEWAL_PART = 0.25f;
    static inline const float MAX_ANNUAL_HERD_RENEWAL_PART = 0.50f;
    static inline const float DEFAULT_ANNUAL_HERD_RENEWAL_PART = 0.33f;
    static inline const float DEFAULT_ANNUAL_HERD_RENEWAL_DELTA = 0.0f;
    
    // Female calves management (2.2.5.2.1.4)
    static inline const FunctionalEnumerations::Population::FemaleCalveManagement DEFAULT_FEMALE_CALVE_MANAGEMENT = FunctionalEnumerations::Population::FemaleCalveManagement::balanceManagement;
    static inline const float LOW_FEMALE_CALVES_DAM_MILK_SCORE_PERCENT = 0.1f; // 10 percent 
    static inline const float HIGHT_FEMALE_CALVES_DAM_MILK_SCORE_PERCENT = 0.2f; // 20 percent
    static inline const float FEMALE_CALVES_SAFETY_MARGIN = 0.05f; // 5 percent
    
    // Maximum lactation rank (2.2.5.2.1.5)
    static inline const unsigned int DEFAULT_MAX_LACTATION_RANK_FOR_END_INSEMINATION_DECISION = 7;
    
    // Cow production performance (2.2.5.2.1.6)
    static inline const float DEFAULT_MIN_MILK_PRODUCTION_HERD_RATIO_FOR_END_INSEMINATION_DECISION = 0.86f; // 86 percent of a the herd production
    
    // SCC level (2.2.5.2.1.7)
    static inline const unsigned int DEFAULT_MAX_SCC_LEVEL_FOR_END_INSEMINATION_DECISION = 1000; // 1 000 000 / ml

    // Stabulation and pasture calendar (2.2.5.2.2.1)
    static inline const std::pair<unsigned int, FunctionalEnumerations::Global::Month> DEFAULT_YOUNG_HEIFER_BEGIN_PASTURE_DATE {15, FunctionalEnumerations::Global::Month::apr};
    static inline const std::pair<unsigned int, FunctionalEnumerations::Global::Month> DEFAULT_YOUNG_HEIFER_END_PASTURE_DATE {10, FunctionalEnumerations::Global::Month::nov};
    static inline const std::pair<unsigned int, FunctionalEnumerations::Global::Month> DEFAULT_OTHER_HEIFER_AND_DRIED_COW_BEGIN_PASTURE_DATE {15, FunctionalEnumerations::Global::Month::mar};
    static inline const std::pair<unsigned int, FunctionalEnumerations::Global::Month> DEFAULT_OTHER_HEIFER_AND_DRIED_COW_END_PASTURE_DATE {10, FunctionalEnumerations::Global::Month::nov};  
    static inline const std::pair<unsigned int, FunctionalEnumerations::Global::Month> DEFAULT_LACTATING_COW_BEGIN_FULL_PASTURE_DATE {20, FunctionalEnumerations::Global::Month::mar};
    static inline const std::pair<unsigned int, FunctionalEnumerations::Global::Month> DEFAULT_LACTATING_COW_END_FULL_PASTURE_DATE {30, FunctionalEnumerations::Global::Month::jun};
    static inline const std::pair<unsigned int, FunctionalEnumerations::Global::Month> DEFAULT_LACTATING_COW_BEGIN_STABULATION_DATE {11, FunctionalEnumerations::Global::Month::nov};
    static inline const std::pair<unsigned int, FunctionalEnumerations::Global::Month> DEFAULT_LACTATING_COW_END_STABULATION_DATE {10, FunctionalEnumerations::Global::Month::mar};
       
    // Straw consumption (2.2.5.2.2.2)
    static inline const float DEFAULT_CALF_STRAW_CONSUMPTION = 2.0f;
    static inline const float DEFAULT_HEIFER_STRAW_CONSUMPTION = 3.5f;
    static inline const float DEFAULT_COW_STRAW_CONSUMPTION = 7.0f;
    
};

/*
 ______            _ _                                   _              _       
|  ____|          | (_)                                 | |            | |      
| |__ ___  ___  __| |_ _ __   __ _    ___ ___  _ __  ___| |_ __ _ _ __ | |_ ___ 
|  __/ _ \/ _ \/ _` | | '_ \ / _` |  / __/ _ \| '_ \/ __| __/ _` | '_ \| __/ __|
| | |  __/  __/ (_| | | | | | (_| | | (_| (_) | | | \__ \ || (_| | | | | |_\__ \
|_|  \___|\___|\__,_|_|_| |_|\__, |  \___\___/|_| |_|___/\__\__,_|_| |_|\__|___/
                              __/ |                                             
                             |___/    
*/                                          
// (2.2.6) 
// ------------------------------
class Feeding
{
public:
    // (2.2.6.1)
    static inline const unsigned int COLOSTRUM_CONSUMPTION_DURATION = 1; // Colostrum duration
    static inline const float PRIMIPARE_RBE_RATIO = 0.95f; // Ratio for the primipare rations
    static inline const float DRIED_COW_RBE_RATION_RATIO = 1.0f/3.0f;
    static inline const float DRIED_COW_FORAGE_INDOOR_COMPLEMENT_QUANTITY = 1.0f;
    static inline const std::map<unsigned int, float> DRIED_COW_FORAGE_INDOOR_COMPLEMENT_TYPE 
                                                                          {{ FunctionalEnumerations::Feeding::ForageType::maizePlant,   0.0f},
                                                                           { FunctionalEnumerations::Feeding::ForageType::grass,        0.0f},
                                                                           { FunctionalEnumerations::Feeding::ForageType::hay,          1.0f},
                                                                           { FunctionalEnumerations::Feeding::ForageType::grassSilage,  0.0f}};
    static inline const float PERIPARTUM_CONCENTRATE_COMPLEMENT_FEDDING_QUANTITY = 1.0f;
    static inline const std::map<unsigned int, float> PERIPARTUM_CONCENTRATE_COMPLEMENT_FEDDING_TYPE
                                                                          {{ FunctionalEnumerations::Feeding::ConcentrateType::rapeseed,    0.0f},
                                                                           { FunctionalEnumerations::Feeding::ConcentrateType::soja,        0.5f},
                                                                           { FunctionalEnumerations::Feeding::ConcentrateType::barley,      0.5f}};
    static inline const float PERI_PARTUM_NULLIPARE_FEEDING_RATIO = 0.90f;
                                                                          
    // Calf (2.2.6.2.1.1)
    static inline const float DEFAULT_DAY_MILK_CALF_QUANTITY = 6.0f;
    static inline const FunctionalEnumerations::Feeding::CalfMilkType DEFAULT_CALF_MILK_FEEDING_TYPE = FunctionalEnumerations::Feeding::CalfMilkType::naturalMilk;
    static inline const unsigned int DEFAULT_WEANING_AGE = 70;
   
    static inline const float DEFAULT_UNWEANED_FEMALE_CALF_CONCENTRATE_FEEDING_QUANTITY = 0.35f;
    static inline const std::map<unsigned int, float> DEFAULT_UNWEANED_FEMALE_CALF_CONCENTRATE_FEEDING_TYPE 
                                                                          {{ FunctionalEnumerations::Feeding::ConcentrateType::rapeseed,    0.0f},
                                                                           { FunctionalEnumerations::Feeding::ConcentrateType::soja,        0.2f},
                                                                           { FunctionalEnumerations::Feeding::ConcentrateType::barley,      0.8f}};
    
    // Weaned female heifer (2.2.6.2.1.2)    
    static inline const float DEFAULT_WEANED_FEMALE_CALF_FORAGE_FEEDING_QUANTITY = 3.0f;
    static inline const std::map<unsigned int, float> DEFAULT_WEANED_FEMALE_CALF_FORAGE_FEEDING_TYPE 
                                                                          {{ FunctionalEnumerations::Feeding::ForageType::maizePlant,   0.67f},
                                                                           { FunctionalEnumerations::Feeding::ForageType::grass,        0.0f},
                                                                           { FunctionalEnumerations::Feeding::ForageType::hay,          0.33f},
                                                                           { FunctionalEnumerations::Feeding::ForageType::grassSilage,  0.0f}};
    static inline const float DEFAULT_WEANED_FEMALE_CALF_CONCENTRATE_FEDDING_QUANTITY = 1.0f;
    static inline const std::map<unsigned int, float> DEFAULT_WEANED_FEMALE_CALF_CONCENTRATE_FEEDING_TYPE 
                                                                          {{ FunctionalEnumerations::Feeding::ConcentrateType::rapeseed,    0.0f},
                                                                           { FunctionalEnumerations::Feeding::ConcentrateType::soja,        0.5f},
                                                                           { FunctionalEnumerations::Feeding::ConcentrateType::barley,      0.5f}};
    static inline const float DEFAULT_WEANED_FEMALE_CALF_MINERAL_VITAMIN_FEDDING_QUANTITY = 0.02f;
                                                                           
    // Heifer (2.2.6.2.1.3)

    // Young heifers
    static inline const std::map<FunctionalEnumerations::Population::Location, float> DEFAULT_YOUNG_HEIFER_FORAGE_FEDDING_QUANTITY 
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,         5.5f},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,         6.0f}};
    static inline const std::map<FunctionalEnumerations::Population::Location, std::map<unsigned int, float>> DEFAULT_YOUNG_HEIFER_FORAGE_FEDDING_TYPE 
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,
                                                                               {{ FunctionalEnumerations::Feeding::ForageType::maizePlant,      1.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::grass,           0.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::hay,             0.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::grassSilage,     0.0f}}},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,
                                                                               {{ FunctionalEnumerations::Feeding::ForageType::maizePlant,      0.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::grass,           1.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::hay,             0.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::grassSilage,     0.0f}}}};                                                                       
    static inline const std::map<FunctionalEnumerations::Population::Location, float> DEFAULT_YOUNG_HEIFER_CONCENTRATE_FEDDING_QUANTITY
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,         0.5f},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,         0.0f}};
    static inline const std::map<FunctionalEnumerations::Population::Location, std::map<unsigned int, float>> DEFAULT_YOUNG_HEIFER_CONCENTRATE_FEDDING_TYPE
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,
                                                                               {{ FunctionalEnumerations::Feeding::ConcentrateType::rapeseed,   0.0f},
                                                                                { FunctionalEnumerations::Feeding::ConcentrateType::soja,       1.0f},
                                                                                { FunctionalEnumerations::Feeding::ConcentrateType::barley,     0.0f}}},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,
                                                                               {{ FunctionalEnumerations::Feeding::ConcentrateType::rapeseed,   0.0f},
                                                                                { FunctionalEnumerations::Feeding::ConcentrateType::soja,       1.0f},
                                                                                { FunctionalEnumerations::Feeding::ConcentrateType::barley,     0.0f}}}};
    static inline const std::map<FunctionalEnumerations::Population::Location, float> DEFAULT_YOUNG_HEIFER_MINERAL_VITAMIN_FEDDING_QUANTITY
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,         0.06f},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,         0.0f}};
    // Old heifers
    static inline const std::map<FunctionalEnumerations::Population::Location, float> DEFAULT_OLD_HEIFER_FORAGE_FEDDING_FEDDING_QUANTITY 
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,         6.5f},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,         6.5f}};
    static inline const std::map<FunctionalEnumerations::Population::Location, std::map<unsigned int, float>> DEFAULT_OLD_HEIFER_FORAGE_FEDDING_TYPE 
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,
                                                                               {{ FunctionalEnumerations::Feeding::ForageType::maizePlant,      1.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::grass,           0.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::hay,             0.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::grassSilage,     0.0f}}},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,
                                                                               {{ FunctionalEnumerations::Feeding::ForageType::maizePlant,      0.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::grass,           1.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::hay,             0.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::grassSilage,     0.0f}}}};                                                                       
    static inline const std::map<FunctionalEnumerations::Population::Location, float> DEFAULT_OLD_HEIFER_CONCENTRATE_FEDDING_QUANTITY
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,         0.75f},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,         0.0f}};
    static inline const std::map<FunctionalEnumerations::Population::Location, std::map<unsigned int, float>> DEFAULT_OLD_HEIFER_CONCENTRATE_FEDDING_TYPE
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,
                                                                               {{ FunctionalEnumerations::Feeding::ConcentrateType::rapeseed,   0.0f},
                                                                                { FunctionalEnumerations::Feeding::ConcentrateType::soja,       1.0f},
                                                                                { FunctionalEnumerations::Feeding::ConcentrateType::barley,     0.0f}}},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,
                                                                               {{ FunctionalEnumerations::Feeding::ConcentrateType::rapeseed,   0.0f},
                                                                                { FunctionalEnumerations::Feeding::ConcentrateType::soja,       1.0f},
                                                                                { FunctionalEnumerations::Feeding::ConcentrateType::barley,     0.0f}}}};
    static inline const std::map<FunctionalEnumerations::Population::Location, float> DEFAULT_OLD_HEIFER_MINERAL_VITAMIN_FEDDING_QUANTITY
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,         0.06f},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,         0.0f}};
    // Bred heifers
    static inline const std::map<FunctionalEnumerations::Population::Location, float> DEFAULT_BRED_HEIFER_FORAGE_FEDDING_QUANTITY 
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,         8.0f},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,         9.0f}};
    static inline const std::map<FunctionalEnumerations::Population::Location, std::map<unsigned int, float>> DEFAULT_BRED_HEIFER_FORAGE_FEDDING_TYPE 
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,
                                                                               {{ FunctionalEnumerations::Feeding::ForageType::maizePlant,      1.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::grass,           0.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::hay,             0.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::grassSilage,     0.0f}}},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,
                                                                               {{ FunctionalEnumerations::Feeding::ForageType::maizePlant,      0.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::grass,           1.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::hay,             0.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::grassSilage,     0.0f}}}};                                                                       
    static inline const std::map<FunctionalEnumerations::Population::Location, float> DEFAULT_BRED_HEIFER_CONCENTRATE_FEDDING_QUANTITY
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,         1.0f},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,         0.0f}};
    static inline const std::map<FunctionalEnumerations::Population::Location, std::map<unsigned int, float>> DEFAULT_BRED_HEIFER_CONCENTRATE_FEDDING_TYPE
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,
                                                                               {{ FunctionalEnumerations::Feeding::ConcentrateType::rapeseed,   0.0f},
                                                                                { FunctionalEnumerations::Feeding::ConcentrateType::soja,       1.0f},
                                                                                { FunctionalEnumerations::Feeding::ConcentrateType::barley,     0.0f}}},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,
                                                                               {{ FunctionalEnumerations::Feeding::ConcentrateType::rapeseed,   0.0f},
                                                                                { FunctionalEnumerations::Feeding::ConcentrateType::soja,       1.0f},
                                                                                { FunctionalEnumerations::Feeding::ConcentrateType::barley,     0.0f}}}};
    static inline const std::map<FunctionalEnumerations::Population::Location, float> DEFAULT_BRED_HEIFER_MINERAL_VITAMIN_FEDDING_QUANTITY
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,         0.08f},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,         0.0f}};
    
    // Milking cows (2.2.6.2.1.4)
    static inline const float DEFAULT_RBE_MILK_PRODUCTION = 26.0f;
    static inline const FunctionalEnumerations::Global::Month LAST_MONTH_FOR_SPRING = FunctionalEnumerations::Global::Month::jun;
 
    static inline const std::map<FunctionalEnumerations::Population::Location, float> DEFAULT_MILKING_COW_FORAGE_FEDDING_QUANTITY 
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,            16.0f},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,            20.0f},
                                                                           { FunctionalEnumerations::Population::Location::halfStabulationPasture,  16.0f}};
    static inline const std::map<FunctionalEnumerations::Population::Location, std::map<unsigned int, float>> DEFAULT_MILKING_COW_FORAGE_FEDDING_TYPE 
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,
                                                                               {{ FunctionalEnumerations::Feeding::ForageType::maizePlant,          0.95f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::grass,               0.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::hay,                 0.05f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::grassSilage,         0.00f}}},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,
                                                                               {{ FunctionalEnumerations::Feeding::ForageType::maizePlant,          0.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::grass,               1.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::hay,                 0.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::grassSilage,         0.0f}}},
                                                                           { FunctionalEnumerations::Population::Location::halfStabulationPasture,
                                                                               {{ FunctionalEnumerations::Feeding::ForageType::maizePlant,          0.38f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::grass,               0.62f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::hay,                 0.0f},
                                                                                { FunctionalEnumerations::Feeding::ForageType::grassSilage,         0.0f}}}};                                                                       
    static inline const std::map<FunctionalEnumerations::Population::Location, float> DEFAULT_MILKING_COW_CONCENTRATE_FEDDING_QUANTITY
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,             3.1f},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,             0.0f},
                                                                           { FunctionalEnumerations::Population::Location::halfStabulationPasture,   1.5f}};
    static inline const std::map<FunctionalEnumerations::Population::Location, std::map<unsigned int, float>> DEFAULT_MILKING_COW_CONCENTRATE_FEDDING_TYPE
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,
                                                                               {{ FunctionalEnumerations::Feeding::ConcentrateType::rapeseed,       0.2f},
                                                                                { FunctionalEnumerations::Feeding::ConcentrateType::soja,           0.0f},
                                                                                { FunctionalEnumerations::Feeding::ConcentrateType::barley,         0.8f}}},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,
                                                                               {{ FunctionalEnumerations::Feeding::ConcentrateType::rapeseed,       0.0f},
                                                                                { FunctionalEnumerations::Feeding::ConcentrateType::soja,           1.0f},
                                                                                { FunctionalEnumerations::Feeding::ConcentrateType::barley,         0.0f}}},
                                                                           { FunctionalEnumerations::Population::Location::halfStabulationPasture,
                                                                               {{ FunctionalEnumerations::Feeding::ConcentrateType::rapeseed,       0.2f},
                                                                                { FunctionalEnumerations::Feeding::ConcentrateType::soja,           0.0f},
                                                                                { FunctionalEnumerations::Feeding::ConcentrateType::barley,         0.8f}}}};
    static inline const std::map<FunctionalEnumerations::Population::Location, float> DEFAULT_MILKING_COW_MINERAL_VITAMIN_FEDDING_QUANTITY
                                                                          {{ FunctionalEnumerations::Population::Location::stabulation,             0.25f},
                                                                           { FunctionalEnumerations::Population::Location::fullPasture,             0.0f},
                                                                           { FunctionalEnumerations::Population::Location::halfStabulationPasture,   0.1f}};
    
    // Product concentrate Ration (2.2.6.2.2)
    static inline const float DEFAULT_GAIN_FOR_ONE_KILO_CONCENTRATE = 2.5f;
    static inline const std::map<unsigned int, float> DEFAULT_PRODUCT_CONCENTRATE_TYPE  {{ FunctionalEnumerations::Feeding::ConcentrateType::rapeseed, 0.0f},
                                                                           { FunctionalEnumerations::Feeding::ConcentrateType::soja, 0.3f},
                                                                           { FunctionalEnumerations::Feeding::ConcentrateType::barley, 0.7f}};
    // Dehydrated milk ratio (2.2.6.3.1)
    static inline const float DEHYDRATED_MILK_RATIO = 0.125f;
    
};
 
/*
                                   _   _                                   _              _       
    /\                            | | (_)                                 | |            | |      
   /  \   ___ ___ ___  _   _ _ __ | |_ _ _ __   __ _    ___ ___  _ __  ___| |_ __ _ _ __ | |_ ___ 
  / /\ \ / __/ __/ _ \| | | | '_ \| __| | '_ \ / _` |  / __/ _ \| '_ \/ __| __/ _` | '_ \| __/ __|
 / ____ \ (_| (_| (_) | |_| | | | | |_| | | | | (_| | | (_| (_) | | | \__ \ || (_| | | | | |_\__ \
/_/    \_\___\___\___/ \__,_|_| |_|\__|_|_| |_|\__, |  \___\___/|_| |_|___/\__\__,_|_| |_|\__|___/
                                                __/ |                                             
                                               |___/                                              
*/
// (2.2.7) 
// ------------------------------
class AccountingModel
{
public:
    // Reproductin default values (2.2.7.1.1)
    static inline const float DEFAULT_CALF_BIRTH_PRICE = -3.00f; // cout de la naissance (veto))
    static inline const std::map<FunctionalEnumerations::Reproduction::TypeInseminationSemen, float> DEFAULT_INSEMINATION_PRICE =
                                                                                            {{FunctionalEnumerations::Reproduction::TypeInseminationSemen::natural,           0.00f},
                                                                                             {FunctionalEnumerations::Reproduction::TypeInseminationSemen::conventionalIA,  -40.00f},
                                                                                             {FunctionalEnumerations::Reproduction::TypeInseminationSemen::maleSexedIA,      -60.00f},
                                                                                             {FunctionalEnumerations::Reproduction::TypeInseminationSemen::femaleSexedIA,    -60.00f}};
    static inline const float DEFAULT_VET_REPRODUCTION_CONTRACT_COST = -30.00f;
    static inline const float DEFAULT_VET_NO_OESTRUS_SEEN_TREATMENT_COST = -9.00f;
    static inline const float DEFAULT_VET_UNSUCESSFUL_IA_TREATMENT_COST = -30.00f;
    static inline const float DEFAULT_VET_NEGATIVE_PREGNANCY_DIAGNOSTIC_TREATMENT_COST = -9.00f;
    
    // Production default values (2.2.7.1.2)
    static inline const float DEFAULT_MILK_TON_PRICE = 330.00f;
    static inline const float DEFAULT_LEVEL_1_SCC_PENALTY = -3.049f; // 250 <= SCC < 300
    static inline const float DEFAULT_LEVEL_2_SCC_PENALTY = -9.147f; // 300 <= SCC < 400
    static inline const float DEFAULT_LEVEL_3_SCC_PENALTY = -15.245f; // 400 <= SCC
    static inline const float DEFAULT_TB_QUALITY_BONUS = 2.60f;
    static inline const float DEFAULT_TP_QUALITY_BONUS = 6.60f;
    static inline const float TB_BASIS_QUALITY_BONUS = 38.0f;
    static inline const float TP_BASIS_QUALITY_BONUS = 32.0f;

    // Health default values (2.2.7.1.3)
    static inline const float DEFAULT_VET_CARE_CONTRACT_BASED_ON_COW_COST = -30.00f;
    static inline const float DEFAULT_VET_CARE_CONTRACT_BASED_ON_CALVING_COST = -31.50f;
    static inline const float DEFAULT_VET_CARE_CONTRACT_BASED_ON_KILOLITER_COST = -3.52f;
    static inline const float DEFAULT_G1_MASTITIS_LACTATION_WITHOUT_CONTRACT_TREATMENT_COST = -20.00f;
    static inline const float DEFAULT_G1_MASTITIS_LACTATION_WITH_CONTRACT_TREATMENT_COST = -20.00f;
    static inline const float DEFAULT_G2_MASTITIS_LACTATION_WITHOUT_CONTRACT_TREATMENT_COST = -20.00f;
    static inline const float DEFAULT_G2_MASTITIS_LACTATION_WITH_CONTRACT_TREATMENT_COST = -18.00f;
    static inline const float DEFAULT_G3_MASTITIS_LACTATION_WITHOUT_CONTRACT_TREATMENT_COST = -250.00f;
    static inline const float DEFAULT_G3_MASTITIS_LACTATION_WITH_CONTRACT_TREATMENT_COST = -50.00f;
    static inline const float DEFAULT_MASTITIS_DRY_WITHOUT_CONTRACT_TREATMENT_COST = -9.00f;
    static inline const float DEFAULT_MASTITIS_DRY_WITH_CONTRACT_TREATMENT_COST = -9.00f;
    static inline const float DEFAULT_MASTITIS_PREVENTIVE_ACTION_COST = -9.00f;
    static inline const float DEFAULT_MONENSIN_BOLUS_COST = -40.00f;
    static inline const float DEFAULT_G1_KETOSIS_WITHOUT_CONTRACT_TREATMENT_COST = -6.00f;
    static inline const float DEFAULT_G1_KETOSIS_WITH_CONTRACT_TREATMENT_COST = -5.00f;
    static inline const float DEFAULT_G2_KETOSIS_WITHOUT_CONTRACT_TREATMENT_COST = -100.00f;
    static inline const float DEFAULT_G2_KETOSIS_WITH_CONTRACT_TREATMENT_COST = -60.00f;
    static inline const float DEFAULT_KETOSIS_PREVENTIVE_ACTION_COST = -5.00f;
    static inline const float DEFAULT_LAMENESS_ANNUAL_FOOT_BATH_COST = -67.00f;
    static inline const unsigned int COW_NUMBER_FOR_MINIMUM_FOOT_BATH_COST = 75;
    static inline const float DEFAULT_LAMENESS_FOOT_BATH_USAGE_MINIMUM_COST = -324.00f;
    static inline const float DEFAULT_LAMENESS_FOOT_BATH_USAGE_MAXIMUM_COST = -648.00f;
    static inline const float DEFAULT_LAMENESS_FOOT_TRIMMING_WORKSHOP_COST = -75.00f;
    static inline const float DEFAULT_LAMENESS_COW_FOOT_TRIMMING_COST = -20.00f;
    static inline const float DEFAULT_NON_INFECTIOUS_LAMENESS_COW_FOOT_TREATMENT_COST = -17.00f;
    static inline const float DEFAULT_INFECTIOUS_LAMENESS_COW_FOOT_TREATMENT_COST = -5.00f;
    
    // Genetic default values (2.2.7.1.4)
    // none
    
    // Population default values (2.2.7.1.5)
    static inline const float DEFAULT_FEMALE_DAIRY_CALF_PRICE = 60.00f; // credit euro (avis)
    static inline const float DEFAULT_MALE_DAIRY_CALF_PRICE = 80.00f; // credit euro (avis)
    static inline const float DEFAULT_FEMALE_BEEF_BRED_CALF_PRICE = 300.00f; // credit euro (avis)
    static inline const float DEFAULT_MALE_BEEF_BRED_CALF_PRICE = 320.00f; // credit euro (avis)
    static inline const float DEFAULT_UNDER_ONE_YEAR_CALF_PRICE = 250.00f;
    static inline const float DEFAULT_UNDER_TWO_YEAR_CALF_PRICE = 600.00f;
    static inline const float DEFAULT_PREGNANT_HEIFER_PRICE = 800.00f;
    static inline const float DEFAULT_CULLED_COW_PRICE = 700.00f; // credit euro (avis)
    
    // Feeding default values (2.2.7.1.6)
    static inline const float DEFAULT_DEHYDRATED_MILK_TON_PRICE = -2000.00f;
    static inline const std::map<FunctionalEnumerations::Feeding::ForageType, float> DEFAULT_FORAGE_TON_PRICE
                                                                            {{ FunctionalEnumerations::Feeding::ForageType::maizePlant,     -200.00f},
                                                                             { FunctionalEnumerations::Feeding::ForageType::grass,           -45.00f},
                                                                             { FunctionalEnumerations::Feeding::ForageType::hay,            -140.00f},
                                                                             { FunctionalEnumerations::Feeding::ForageType::grassSilage,    -130.00f}};
    static inline const std::map<FunctionalEnumerations::Feeding::ConcentrateType, float> DEFAULT_CONCENTRATE_TON_PRICE
                                                                            {{ FunctionalEnumerations::Feeding::ConcentrateType::rapeseed,  -300.00f},
                                                                             { FunctionalEnumerations::Feeding::ConcentrateType::soja,      -330.00f},
                                                                             { FunctionalEnumerations::Feeding::ConcentrateType::barley,    -160.00f}};
    static inline const float DEFAULT_MINERAL_VITAMIN_TON_PRICE = -660.00f;
    
    // Other cost default values (2.2.7.1.7)
    static inline const float DEFAULT_UNDER_ONE_YEAR_DEATH_CALF_PRICE = -20.00f; // debit euro (avis)
    static inline const float DEFAULT_UNDER_TWO_YEAR_DEATH_CALF_PRICE = -30.00f; // debit euro (avis)
    static inline const float DEFAULT_DEATH_COW_PRICE = -40.00f; // debit euro (avis)
    static inline const float DEFAULT_MILK_CONTROL_PRICE = -45.00f;
    static inline const float DEFAULT_CETO_DETECT_PRICE = -3.76f;
    static inline const float DEFAULT_HERD_NAVIGATOR_PRICE = -35.0f;
    static inline const float DEFAULT_STRAW_TON_PRICE = -20.00f;
    
    static inline const float DEFAULT_CALF_BEDDING_PRICE = -20.0f;
    static inline const float DEFAULT_HEIFER_MISCELLANEOUS_VET_COST = -50.0f;
    static inline const float DEFAULT_OTHER_CALF_BREEDING_COST = -10.0f;
    static inline const float DEFAULT_ANNUAL_COW_BEDDING_PRICE = -20.0f;
    static inline const float DEFAULT_ANNUAL_COW_MISCELLANEOUS_OTHER_VET_COST = -40.0f;
    static inline const float DEFAULT_OTHER_ANNUAL_COW_BREEDING_COST = -80.0f;
        
    // Other costs
    
    // key values   
    static inline const std::string KEY_FEMALE_DAIRY_CALF_PRICE = "femaleDairyCalfPrice";
    static inline const std::string KEY_MALE_DAIRY_CALF_PRICE = "maleDairyCalfPrice";
    static inline const std::string KEY_FEMALE_BEEF_BRED_CALF_PRICE = "femaleBeefBredCalfPrice";
    static inline const std::string KEY_MALE_BEEF_BRED_CALF_PRICE = "maleBeefBredCalfPrice";
    static inline const std::string KEY_UNDER_ONE_YEAR_CALF_PRICE = "underOneYearCalfPrice";
    static inline const std::string KEY_UNDER_TWO_YEAR_CALF_PRICE = "underTwoYearCalfPrice";
    static inline const std::string KEY_PREGNANT_HEIFER_PRICE = "pregnantHeiferPrice";
    static inline const std::string KEY_CULLED_COW_PRICE = "culledCowPrice";
    static inline const std::string KEY_UNDER_ONE_YEAR_DEATH_CALF_PRICE = "underOneYearDeathCalfPrice";
    static inline const std::string KEY_UNDER_TWO_YEAR_DEATH_CALF_PRICE = "underTwoYearDeathCalfPrice";
    static inline const std::string KEY_DEATH_COW_PRICE = "deathCowPrice";
    static inline const std::string KEY_CALF_BIRTH_PRICE = "calfBirthPrice";
    static inline const std::string KEY_NATURAL_INSEMINATION_PRICE = "naturalInseminationPrice";
    static inline const std::string KEY_CONVENTIONAL_ARTIFICIAL_INSEMINATION_PRICE = "conventionalArtificialInseminationPrice";
    static inline const std::string KEY_MALE_SEXED_ARTIFICIAL_INSEMINATIONINSEMINATION_PRICE = "maleSexedArtificialInseminationPrice";
    static inline const std::string KEY_FEMALE_SEXED_ARTIFICIAL_INSEMINATIONINSEMINATION_PRICE = "femaleSexedArtificialInseminationPrice";
    static inline const std::map<FunctionalEnumerations::Reproduction::TypeInseminationSemen,  const std::string &> INSEMINATION_PRICES =
                                                                                            {{FunctionalEnumerations::Reproduction::TypeInseminationSemen::natural,         KEY_NATURAL_INSEMINATION_PRICE},
                                                                                             {FunctionalEnumerations::Reproduction::TypeInseminationSemen::conventionalIA,  KEY_CONVENTIONAL_ARTIFICIAL_INSEMINATION_PRICE},
                                                                                             {FunctionalEnumerations::Reproduction::TypeInseminationSemen::maleSexedIA,     KEY_MALE_SEXED_ARTIFICIAL_INSEMINATIONINSEMINATION_PRICE},
                                                                                             {FunctionalEnumerations::Reproduction::TypeInseminationSemen::femaleSexedIA,   KEY_FEMALE_SEXED_ARTIFICIAL_INSEMINATIONINSEMINATION_PRICE}};

    static inline const std::string KEY_VET_REPRODUCTION_CONTRACT_COST = "vetReproductionContractPrice";
    static inline const std::string KEY_VET_NO_OESTRUS_SEEN_TREATMENT_COST = "vetNoOestrusSeenTreatmentPrice";
    static inline const std::string KEY_VET_UNSUCESSFUL_IA_TREATMENT_COST = "vetUnsuccessfulIATreatmentPrice";
    static inline const std::string KEY_VET_NEGATIVE_PREGNANCY_DIAGNOSTIC_TREATMENT_COST = "vetNegativePregnancyTreatmentPrice";
    static inline const std::string KEY_MILK_TON_PRICE = "milkTonPrice";
    static inline const std::string KEY_MILK_CONTROL_PRICE = "milkControlPrice";
    static inline const std::string KEY_LEVEL_1_SCC_PENALTY = "level1SCCPenaltyPrice";
    static inline const std::string KEY_LEVEL_2_SCC_PENALTY = "level2SCCPenaltyPrice";
    static inline const std::string KEY_LEVEL_3_SCC_PENALTY = "level3SCCPenaltyPrice";
    static inline const std::string KEY_TB_QUALITY_BONUS = "TBQualityBonusPrice";
    static inline const std::string KEY_TP_QUALITY_BONUS = "TPQualityBonusPrice"; 
    static inline const std::string KEY_VET_CARE_CONTRACT_BASED_ON_COW_COST = "vetCareContractBasedOnCowPrice";
    static inline const std::string KEY_VET_CARE_CONTRACT_BASED_ON_CALVING_COST = "vetCareContractBasedOnCavingPrice";
    static inline const std::string KEY_VET_CARE_CONTRACT_BASED_ON_KILOLITER_COST = "vetCareContractBasedOnKiloliterPrice";
    static inline const std::string KEY_G1_MASTITIS_LACTATION_WITHOUT_CONTRACT_TREATMENT_COST = "G1mastitisLactationWithoutContractTreatmentPrice";
    static inline const std::string KEY_G1_MASTITIS_LACTATION_WITH_CONTRACT_TREATMENT_COST = "G1mastitisLactationWithContractTreatmentPrice";
    static inline const std::string KEY_G2_MASTITIS_LACTATION_WITHOUT_CONTRACT_TREATMENT_COST = "G2mastitisLactationWithoutContractTreatmentPrice";
    static inline const std::string KEY_G2_MASTITIS_LACTATION_WITH_CONTRACT_TREATMENT_COST = "G2mastitisLactationWithContractTreatmentPrice";
    static inline const std::string KEY_G3_MASTITIS_LACTATION_WITHOUT_CONTRACT_TREATMENT_COST = "G3mastitisLactationWithoutContractTreatmentPrice";
    static inline const std::string KEY_G3_MASTITIS_LACTATION_WITH_CONTRACT_TREATMENT_COST = "G3mastitisLactationWithContractTreatmentPrice";
    static inline const std::string KEY_MASTITIS_DRY_WITHOUT_CONTRACT_TREATMENT_COST = "mastitisDryWithoutContractTreatmentPrice";
    static inline const std::string KEY_MASTITIS_DRY_WITH_CONTRACT_TREATMENT_COST = "mastitisDryWithContractTreatmentPrice";
    static inline const std::string KEY_MASTITIS_PREVENTIVE_ACTION_COST = "mastitisPreventiveActionsPrice";
    static inline const std::string KEY_MONENSIN_BOLUS_COST = "monensinBolusPrice";
    static inline const std::string KEY_CETO_DETECT_PRICE = "cetoDetectPrice";
    static inline const std::string KEY_HERD_NAVIGATOR_PRICE = "herdNavigatorPrice";
    static inline const std::string KEY_G1_KETOSIS_WITHOUT_CONTRACT_TREATMENT_COST = "G1ketosisWithoutContractTreatmentPrice";
    static inline const std::string KEY_G1_KETOSIS_WITH_CONTRACT_TREATMENT_COST = "G1ketosisWithContractTreatmentPrice";
    static inline const std::string KEY_G2_KETOSIS_WITHOUT_CONTRACT_TREATMENT_COST = "G2ketosisWithoutContractTreatmentPrice";
    static inline const std::string KEY_G2_KETOSIS_WITH_CONTRACT_TREATMENT_COST = "G2ketosisWithContractTreatmentPrice";
    static inline const std::string KEY_KETOSIS_PREVENTIVE_ACTION_COST = "ketosisPreventiveActionsPrice";
    static inline const std::string KEY_LAMENESS_ANNUAL_FOOT_BATH_COST = "lamenessAnnualFootBathPrice";
    static inline const std::string KEY_LAMENESS_FOOT_BATH_USAGE_MINIMUM_COST = "lamenessMinFootBathUsagePrice";
    static inline const std::string KEY_LAMENESS_FOOT_BATH_USAGE_MAXIMUM_COST = "lamenessMaxFootBathUsagePrice";
    static inline const std::string KEY_LAMENESS_FOOT_TRIMMING_WORKSHOP_COST = "lamenessFootTrimmingWorkshopPrice";
    static inline const std::string KEY_LAMENESS_COW_FOOT_TRIMMING_COST = "lamenessCowFootTrimmingPrice";
    static inline const std::string KEY_NON_INFECTIOUS_LAMENESS_COW_FOOT_TREATMENT_COST = "nonInfectiousLamenessCowFootTreatmentPrice";
    static inline const std::string KEY_INFECTIOUS_LAMENESS_COW_FOOT_TREATMENT_COST = "infectiousLamenessCowFootTreatmentPrice";
    static inline const std::string KEY_DEHYDRATED_MILK_TON_PRICE = "dehydratedMilkTonPrice";
    static inline const std::string KEY_STRAW_TON_PRICE = "strawTonPrice";
    static inline const std::string KEY_MAIZE_PLANT_FORAGE_TON_PRICE = "maizePlantTonPrice";
    static inline const std::string KEY_GRASS_FORAGE_TON_PRICE = "grassTonPrice";
    static inline const std::string KEY_HAY_FORAGE_TON_PRICE = "hayTonPrice";
    static inline const std::string KEY_GRASS_SILAGE_FORAGE_TON_PRICE = "grassSilageTonPrice";
    static inline const std::map<FunctionalEnumerations::Feeding::ForageType, const std::string &> KEYS_FORAGE
                                                                            {{ FunctionalEnumerations::Feeding::ForageType::maizePlant,     KEY_MAIZE_PLANT_FORAGE_TON_PRICE},
                                                                             { FunctionalEnumerations::Feeding::ForageType::grass,          KEY_GRASS_FORAGE_TON_PRICE},
                                                                             { FunctionalEnumerations::Feeding::ForageType::hay,            KEY_HAY_FORAGE_TON_PRICE},
                                                                             { FunctionalEnumerations::Feeding::ForageType::grassSilage,    KEY_GRASS_SILAGE_FORAGE_TON_PRICE}};
    static inline const std::string KEY_RAPESEED_CONCENTRATE_TON_PRICE = "rapeseedTonPrice";
    static inline const std::string KEY_SOJA_CONCENTRATE_TON_PRICE = "sojaTonPrice";
    static inline const std::string KEY_BARLEY_CONCENTRATE_TON_PRICE = "barleyTonPrice";
    static inline const std::map<FunctionalEnumerations::Feeding::ConcentrateType, const std::string &> KEYS_CONCENTRATE
                                                                            {{ FunctionalEnumerations::Feeding::ConcentrateType::rapeseed,  KEY_RAPESEED_CONCENTRATE_TON_PRICE},
                                                                             { FunctionalEnumerations::Feeding::ConcentrateType::soja,      KEY_SOJA_CONCENTRATE_TON_PRICE},
                                                                             { FunctionalEnumerations::Feeding::ConcentrateType::barley,    KEY_BARLEY_CONCENTRATE_TON_PRICE}};
    static inline const std::string KEY_MINERAL_VITAMIN_TON_PRICE = "mineralVitaminTonPrice";
    static inline const std::string KEY_CALF_BEDDING_PRICE = "calfBeddingPrice";
    static inline const std::string KEY_HEIFER_MISCELLANEOUS_VET_COST = "heiferMiscellaneousVetCost";        
    static inline const std::string KEY_OTHER_CALF_BREEDING_COST = "otherCalfBreedingCost";       
    static inline const std::string KEY_ANNUAL_COW_BEDDING_PRICE = "annualCowBeddingPrice";
    static inline const std::string KEY_ANNUAL_COW_MISCELLANEOUS_VET_COST = "annualCowMiscellaneousVetCost";                                
    static inline const std::string KEY_OTHER_ANNUAL_COW_BREEDING_COST = "otherAnnualCowBreedingCost";

#ifdef _LOG                
    // Transaction types (debug)
    static inline const std::string NATURAL_INSEMINATION_TRANSACTION_TYPE = "Natural_insemination";
    static inline const std::string CONVENTIONAL_IA_TRANSACTION_TYPE = "Conventional_IA";
    static inline const std::string MALE_SEXED_IA_TRANSACTION_TYPE = "Male_sexed_IA";
    static inline const std::string FEMALE_SEXED_IA_TRANSACTION_TYPE = "Female_sexed_IA";
    static inline const std::string DELIVERED_MILK_TRANSACTION_TYPE = "Delivered_milk";
    static inline const std::string MILK_CONTROL_TRANSACTION_TYPE = "Milk_control";
    static inline const std::string VET_CARE_CONTRACT_BASED_ON_COW_TRANSACTION_TYPE = "Vet_care_contract_based_on_cow";
    static inline const std::string VET_CARE_CONTRACT_BASED_ON_CALVING_TRANSACTION_TYPE = "Vet_care_contract_based_on_caving";
    static inline const std::string VET_CARE_CONTRACT_BASED_ON_KILOLITER_TRANSACTION_TYPE = "Vet_care_contract_based_on_kiloliter";
    static inline const std::string VET_REPRODUCTION_CONTRACT_TRANSACTION_TYPE = "Vet_reproduction_contract";
    static inline const std::string MASTITIS_PREVENTIVE_ACTION_TRANSACTION_TYPE = "mastitis_preventive_actions";
    static inline const std::string KETOSIS_PREVENTIVE_ACTION_TRANSACTION_TYPE = "ketosis_preventive_actions";
    static inline const std::string CETODETECT_SUB_TRANSACTION_TYPE = "_cetodetect";
    static inline const std::string HERD_NAVIGATOR_TRANSACTION_TYPE = "Herd_navigator";
    static inline const std::string MONENSIN_BOLUS_TRANSACTION_TYPE = "Monensin_bolus";
    static inline const std::string ANNUAL_FOOT_BATH_COST_TRANSACTION_TYPE = "Annual_foot_bath_cost";
    static inline const std::string MINIMUM_FOOT_BATH_USAGE_TRANSACTION_TYPE = "Minimum_foot_bath_usage";
    static inline const std::string MAXIMUM_FOOT_BATH_USAGE_TRANSACTION_TYPE = "Maximum_foot_bath_usage";
    static inline const std::string FOOT_TRIMMING_WORKSHOP_TRANSACTION_TYPE = "Foot_trimming_workshop";
    static inline const std::string LAMENESS_COW_FOOT_INDIVIDUAL_CURATIVE_TRIMMING_TRANSACTION_TYPE = "Lameness_cow_foot_individual_curative_trimming";
    static inline const std::string LAMENESS_COW_FOOT_COLLECTIVE_PREVENTIVE_TRIMMING_TRANSACTION_TYPE = "Lameness_cow_foot_collective_preventive_trimming";
    static inline const std::string NON_INFECTIOUS_LAMENESS_COW_FOOT_COLLECTIVE_TREATMENT_TRANSACTION_TYPE = "Non_infectious_lameness_cow_foot_collective_treament";
    static inline const std::string INFECTIOUS_LAMENESS_COW_FOOT_COLLECTIVE_TREATMENT_TRANSACTION_TYPE = "Infectious_lameness_cow_foot_collective_treament";
    static inline const std::string NON_INFECTIOUS_LAMENESS_COW_FOOT_INDIVIDUAL_TREATMENT_TRANSACTION_TYPE = "Non_infectious_lameness_cow_foot_individual_treament";
    static inline const std::string INFECTIOUS_LAMENESS_COW_FOOT_INDIVIDUAL_TREATMENT_TRANSACTION_TYPE = "Infectious_lameness_cow_foot_individual_treament"; 
    static inline const std::string BIRTH_TRANSACTION_TYPE = "Birth";
    static inline const std::string CULLING_TRANSACTION_TYPE = "Culling";
    static inline const std::string DEATH_TRANSACTION_TYPE = "Death";
    static inline const std::string PURCHASE_TRANSACTION_TYPE = "Purchase";
    static inline const std::string SALE_TRANSACTION_TYPE = "Sale";
    static inline const std::string STRAW_CONSUMPTION_TYPE = "Straw_consumption";
    static inline const std::string DEHYDRATED_MILK_TRANSACTION_TYPE = "Dehydrated_milk";
    static inline const std::string CALF_BEDDING_TRANSACTION_TYPE = "Calf_bedding";
    static inline const std::string HEIFER_MISCELLANEOUS_VET_TRANSACTION_TYPE = "Heifer_miscellaneous_Vet";
    static inline const std::string OTHER_CALF_BREEDING_TRANSACTION_TYPE = "Other_calf_breeding";
    static inline const std::string COW_BEDDING_TRANSACTION_TYPE = "Cow_bedding";
    static inline const std::string COW_MISCELLANEOUS_VET_TRANSACTION_TYPE = "Cow_miscellaneous_vet";
    static inline const std::string OTHER_COW_BREEDING_TRANSACTION_TYPE = "Other_cow_breeding";
#endif // _LOG                
};
} /* End of namespace FunctionalConstants */
#endif // FunctionalConstants_h
