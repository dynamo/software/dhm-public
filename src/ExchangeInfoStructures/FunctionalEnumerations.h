#ifndef FunctionalEnumerations_h
#define FunctionalEnumerations_h

/// Functional enumerations
namespace FunctionalEnumerations
{
 
/*  
 ______                                      _   _
|  ____|                                    | | (_)                
| |__   _ __  _   _ _ __ ___   ___ _ __ __ _| |_ _  ___  _ __  ___ 
|  __| | '_ \| | | | '_ ` _ \ / _ \ '__/ _` | __| |/ _ \| '_ \/ __|
| |____| | | | |_| | | | | | |  __/ | | (_| | |_| | (_) | | | \__ \
|______|_| |_|\__,_|_| |_| |_|\___|_|  \__,_|\__|_|\___/|_| |_|___/
 
*/                                                                  
    
/// Global enumerations
namespace Global
{
    /// Months (parameter)
    enum Month 
    {
        /// January, value = 1
        jan = 1,
        /// February, value = 2
        feb = 2,
        /// March, value = 3
        mar = 3,
        /// April, value = 4
        apr = 4,
        /// May, value = 5
        may = 5,
        /// June, value = 6
        jun = 6,
        /// July, value = 7
        jul = 7,
        /// August, value = 8
        aug = 8,
        /// September, value = 9
        sep = 9,
        /// October, value = 10
        oct = 10,
        /// November, value = 11
        nov = 11,
        /// December, value = 12
        dec = 12
    };
}
   
/// Reproduction management enumerations
namespace Reproduction
{
    // Dairy cow parity types (technical)
    enum DairyCowParity
    {
        heifer,
        primipare,
        multipare
    };
    
    /// Detection modes (parameter)
    enum DetectionMode
    {
        /// Detector (sensor), value = 0
        detector,
        /// Robot (with progesterone determination in milk), value = 1
        robot,
        /// Farmer himself, value = 2
        farmer,
        /// Male, value = 3
        male
    };
    
    // End pregnancy reasons (technical)
    enum EndPregnancyReason
    {
        notYetFinished,
        embryoLoose,
        abortion,
        calving
    };

    /// Insemination ranks (parameter)
    enum InseminationRank
    {
        /// First insemination, value = 0
        firstInsemination,
        /// second and third insemination, value = 1
        inseminations2And3,
        /// Fourth and over insemination, value = 2
        inseminations4AndOver
    };
    
    /// Lactation ranks for insemination (parameter)
    enum LactationRankForInsemination
    {
        /// Heifer, value = 0
        heiferForInsemination,
        /// Adult, value = 1
        adultForInsemination
    };

     // Ovulation classes (technical)
    enum OvulationClass
    {
        firstOvulation,
        secundOvulation,
        moreOvulation,
        firstReturn,
        secundReturn
    };
    
    /// Insemination semen types (parameter and result)
    enum TypeInseminationSemen
    {
        /// Natural (male), value = 0
        natural,
        /// Conventional artificial insemination, value = 1
        conventionalIA,
        /// Male sexed artificial insemination, value = 2
        maleSexedIA,
        /// Female sexed artificial insemination, value = 3
        femaleSexedIA,
        // Technical (for enumerations)
        lastTypeInseminationSemen = femaleSexedIA 
    };
}

/// Lactation management enumerations
namespace Lactation
{    
    /// Daily milking frequencies (parameter)
    enum MilkingFrequency
    {
        /// Only once a day, value = 0
        onceADay,
        /// Twice a day, value = 1
        twiceADay,
        /// Over twice a day, value = 2
        overTwiceADay
    };
}

/// Health management enumerations
namespace Health
{
    // Veterinary care contract
    /// Bacterium types (parameter)
    enum VeterinaryCareContractType
    {
        /// No care contract, value = 0
        noCareContract,
         /// Care contract based on cow number, value = 1
        careContractBasedOnCowNumber,
         /// Care contract based on calving number, value = 2
        careContractBasedOnCalvingNumber,
         /// Care contract based on kiloliter number, value = 3
        careContractBasedOnKiloliterNumber,
    };
    
    
    
    // Mastitis :
    // ----------
    // IMM Types (2.2.3.1.1.1.1)
    // Careful: Sorted by rareness (rare to frequent)
    /// Bacterium types (parameter)
    enum BacteriumType
    {
        /// Gram Negative, value = 0
        Gn,
        /// Streptococcus Uberis , value = 1
        StreptU,
        /// Staphylococcus Aureus, value = 2
        StaphA,
        /// Coagulase Negative Staphylococcus, value = 3
        CNS,
        /// Corynebacterium Bovis, value = 4
        CB,
        // Technical (for enumerations)
        lastBacteriumType = CB 
    };
    
    // Mastitis severity lactation degrees (technical 2.2.3.1.1.2.2.1.2)
    enum MastitisLactationRankModulation
    {
        heiferRankModulation,
        primipareRankModulation,
        twoLactationsRankModulation,
        threeLactationsRankModulation,
        fourLactationsAndMoreRankModulation        
    };
      
    // Mastitis risk periods (technical)
    enum MastitisRiskPeriodType
    {
        outOfRiskPeriod, // 0
        periPartumAndLactationBegin, // 1
        lactation, // 2
        dryingOff, // 3
        dryingInvoluted, // 4
        lastMastitisRiskPeriodType = dryingInvoluted
    };
    
    // Mastitis severities (technical 2.2.3.1.1.1.2.1)
    enum MastitisSeverity
    {
        G0 = 0, 
        G1 = 1,
        G2 = 2,
        G3 = 3  
    };
    
    // Mastitis severity degrees (technical 2.2.3.1.1.1.2.2)
    enum MastitisSeverityDegree
    {
        basis,
        minorated,
        majorated
    };
    
    // Mastitis severity lactation ranks (technical 2.2.3.1.1.1.2.2)
    enum MastitisSeverityLactationRank
    {
        heiferLactationRank,
        primipareLactationRank,
        twoLactationsLactationRank,
        threeLactationsAndMoreLactationRank       
    };
    
    // Ketosis :
    // ---------
    
    // Ketosis severities (technical 2.2.3.2.1.1.1)
    enum KetosisSeverity
    {
        G0Ketosis = 0, 
        G1Ketosis = 1,
        G2Ketosis = 2  
    };

    // Lameness :
    // ----------
    
    // Lameness severities (technical 2.2.3.3.1.1.2)
    enum LamenessSeverity
    {
        G0Lameness = 0, 
        G1Lameness = 1,
        G2Lameness = 2  
    };
    
    /// Lameness detection mode (parameter)
    enum LamenessDetectionMode
    {
        /// Detector (sensor), value = 0
        detector,
        /// Low sensitive farmer, value = 1
        lowSensitiveFarmer,
        /// Hi sensitive farmer, value = 2
        hiSensitiveFarmer
    };
    
    /// Preventive trimming option for lameness (parameter and input data)
    enum LamenessPreventiveTrimmingOption
    {
        /// No trimming, value = 0
        noPreventiveTrimming,
        /// Triming once a year, value = 1
        preventiveTrimmingOnceAYear,
        /// Triming twice a year, value = 2
        preventiveTrimmingTwiceAYear,
        /// Small group, value = 3
        preventiveTrimmingSmallGroup,
    };

    
    // Lameness Infectious state type (technical x.x.x.x.x)
    enum LamenessInfectiousStateType
    {
        nonInfectiousLamenessType = 0, 
        infectiousLamenessType = 1,
    };

}

/// Genetic management enumerations
namespace Genetic
{
    
    /// Genetic breeds (parameter and result)
    enum Breed
    {
        /// Montbeliarde, value = 0
        montbeliarde,
        /// Normande, value = 1
        normande,
        /// Prim'Holstein, value = 2
        primHolstein,
        /// Blanc bleue belge, value = 3
        blancBleueBelge,
        /// Charolaise, value = 4
        charolaise,
        /// Limousine, value = 5
        limousine
    };
    
    // Breed types (technical)
    enum BreedType
    {
        dairy,
        beef
    };

    /// Genetic strategies (parameter and input data)
    enum GeneticStrategy
    {
        /// Well balanced strategy, value = 0
        wellBalanced,
        /// Milk quantity priority strategy, value = 1
        milkQuantityPriority,
        /// Functional longevity priority strategy, value = 2
        functionalLongevityPriority
    };
    
    /// Genetic phenotypic characters (parameter)
    enum PhenotypicCharacter
    { 
        // Production indexes :
        // --------------------
        
        /// milk quantity, value = 0
        Lait,
        /// Taux butyreux ou taux de matieres grasses, value = 1
        TB, 
        /// Taux proteique vrai, value = 2
        TP,
        
        // Functional indexes: 
        // -------------------
        
        /// Fertility, value = 3
        Fer,
        
        // Health indexes :
        // ----------------
        
        /// Resistance aux mammites cliniques, value = 4
        MACL, 
                
        /// Taux de BHB dans le lait (maladie metabolique), value = 5
        BHBlait, 

        /// Resistance aux boiteries infectieuses, value = 6
        RBi,
 
        /// Resistance aux boiteries non infectieuses, value = 7
        RBni,

        // Technical (for enumerations)
        lastPhenotypicCharacter = RBni
    };
    
    // Sexes of the animal (technical)
    enum Sex
    {
        male,
        female
    };
}

/// Population management enumerations
namespace Population
{
    // Batch types (technical)
    enum BatchType
    {
        /// To sale calves batch, value = 0
        toSaleCalves,
        /// female calves batch, value = 1
        femaleCalves,
        /// Weaned calves batch, value = 2
        weanedCalves,
        /// Young unbred heifers batch, value = 3
        youngUnbredHeifers,
        /// older unbred heifers batch, value = 4
        oldUnbredHeifers,
        /// bred heifers batch, value = 5
        bredHeifers,
        /// Dried cows batch, value = 6
        driedCowsAndPregnantHeifers,
        /// Periparturient females batch, value = 7
        periParturientFemales,
        /// Lactating cows batch, value = 8
        lactatingCows
    };
    
    // Death reason (technical)
    enum DeathReason
    {
        mastitisDeathReason,
        ketosisDeathReason,
        nonInfectiousLamenessDeathReason,
        infectiousLamenessDeathReason,
        otherDeathReason
    };

    // Culling status (technical)
    enum CullingStatus
    {
        notToCull,
        toCullDueToUnfertility,
        toCullDueToMastitis,
        toCullDueToKetosis,
        toCullDueToNonInfectiousLameness,
        toCullDueToInfectiousLameness,
        toCullDueToLowMilkProduct,
        toCullDueToLactationRank,
        toCullDueToPopulationRegulation
    };
    
    /// Female calve management option (parameter)
    enum FemaleCalveManagement
    {
        /// Keep all female calves , value = 0
        keepAllFemaleCalves,
        /// Balance management, value = 1
        balanceManagement,
        /// Sale all female calves, value = 2
        saleAllFemaleCalves
    };
    
    /// Locations of the batch (parameter)
    enum Location
    {
        /// Stabulation, value = 0
        stabulation,
        /// Full pasture, value = 1
        fullPasture,
        /// Stabulation the night and pasture the day, value = 2
        halfStabulationPasture
    };
    
    // Age mortality risks (technical)
    enum MortalityRiskAge
    {
        underSevenDays = 7,
        sevenDaysToOneMonth = 30,
        oneToTwoMonths = 61,
        twoToSixMonths = 182,
        sixMonthsToOneYear = 365,
        oneToTwoYears = 730,
        twoToThreeAndHalfYears = 1277,
        threeAndHalfToFiveYears = 1826,
        fiveToTenYears = 3652,
        OverTenYears = 3653
    };

    // Sale reasons (technical)
    enum SaleStatus
    {
        notToSale,
        male,
        beefCrossBred,
        sterileFemale,
        extraFemale,
        extraPregnantHeifer
    };
} 

/// Feeding management enumerations
namespace Feeding
{
    /// Calf milk Types (parameter)
    enum CalfMilkType
    {
        /// Natural milk only, value = 0
        naturalMilk,
        /// Discarded milk, and natural milk if not enougth, value = 1
        discardedMilkAndNaturalMilk,
        /// Dehydrated milk, and dehydrated milk if not enougth, value = 2
        discardedMilkAndDehydratedMilk,
        /// Dehydrated milk only, value = 3
        dehydratedMilk
    };
    
    /// Concentrate types (parameter)
    enum ConcentrateType
    {
        /// Rape seed concentrate, value = 0
        rapeseed,
        /// Soja concentrate, value = 1
        soja,
        /// Barley concentrate, value = 2
        barley, // current final value
        // Technical (for enumerations)
        lastConcentrateType = barley 
    };  
    
    /// Forage types (parameter)
    enum ForageType
    {
        /// Maize plant forage, value = 0
        maizePlant,
        /// grass forage, value = 1
        grass,
        /// hay forage, value = 2
        hay,
        /// grass silage, value = 3
        grassSilage, // current final       
        // Technical (for enumerations)
        lastForageType = grassSilage 
    };
}
} /* End of namespace FunctionalEnumerations */
#endif // FunctionalEnumerations_h
