////////////////////////////////////////
//                                    //
// File : HeadParameters.cpp          //
//                                    //
// Author : Philippe Gontier          //
//                                    //
// Version 1.0                        //
//                                    //
// Date : 01/10/2015                  //
//                                    //
////////////////////////////////////////

#include "HeadParameters.h"

// project
#include "LanguageDictionary.h"

BOOST_CLASS_EXPORT(ExchangeInfoStructures::Parameters::HeadParameters) // Recommended (mandatory if virtual serialization)

namespace ExchangeInfoStructures
{
namespace Parameters
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_NVP(name); \
        ar & BOOST_SERIALIZATION_NVP(comment);
    
    IMPLEMENT_SERIALIZE_STD_METHODS(HeadParameters);

    bool HeadParameters::operator==(HeadParameters const& other)
    {
        return name == other.name &&
            comment == other.comment;
    }

    // Copy constructor   
    HeadParameters::HeadParameters (const HeadParameters &obj)
    {
        name = obj.name;
        comment = obj.comment;
    }

    HeadParameters::HeadParameters()
    {
    }
    
    HeadParameters::~HeadParameters()
    {
    }

    bool HeadParameters::autoControl (std::string &message)
    {
        message = "";
        bool res = true;
        // Is there a name ?
        if (name == "")
        {
            res = false;
            message = LanguageDictionary::getMessage(KEY_THE_NAME_IS_EMPTY);
        }
        return res;
    }
} /* End of namespace Parameters */
}