////////////////////////////////////////
//                                    //
// File : HeadParameters.h            //
//                                    //
// Author : Philippe Gontier          //
//                                    //
// Version 1.0                        //
//                                    //
// Date : 01/10/2015                  //
//                                    //
////////////////////////////////////////

#ifndef HeadParameters_h
#define HeadParameters_h

// boost

// project
#include "../Tools/Serialization.h"

#define GET_STRING_VALUE_TO_IMPORT(theKey, theValue) \
    if (key == theKey) \
    { \
        content = theValue; \
        return true; \
    }

#define GET_INTEGER_VALUE_TO_IMPORT(theKey, theValue) \
    if (key == theKey) \
    { \
        content = Tools::toString(theValue); \
        return true; \
    }

#define GET_FLOAT_VALUE_TO_IMPORT(theKey, theValue) \
    if (key == theKey) \
    { \
        std::string tmp = Tools::toString(theValue); \
        content = Tools::changeChar(tmp, '.', ','); \
        return true; \
    }

namespace ExchangeInfoStructures
{
namespace Parameters
{
    /// This structure is the generic part of each parameter structure
    /// See the § 3.2 "Simulation setting" of the "Functional description and terms of use" documentation.
    struct HeadParameters
    {
    private:
        // serialization
        DECLARE_SERIALIZE_STD_METHODS;

    protected:
    virtual void concrete() = 0; // To ensure that it will not be instantiated

    public:
        
        /// Name of the parameters
        std::string name = "";
        
        /// Comment
        std::string comment = "";
                
       // constructors / destructors
        HeadParameters();
        HeadParameters (const HeadParameters &obj);        // copy constructor
        virtual ~HeadParameters(); // destructor
        
        bool operator==(HeadParameters const& other);
        
        // Methods
        
        /// Value checking, errors in the std::string parameter reference
        virtual bool autoControl (
                                    /// Reference std::string to put the error message (empty value if no error)
                                    std::string &message
                                    );
    };
} /* End of namespace Parameters */
}
#endif // HeadParameters_h
