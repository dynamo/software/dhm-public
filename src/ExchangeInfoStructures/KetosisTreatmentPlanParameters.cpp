#include "KetosisTreatmentPlanParameters.h"

// project
#include "../Tools/Tools.h"
#include "FunctionalConstants.h"
#include "TechnicalConstants.h"
#include "HeadParameters.h"

BOOST_CLASS_EXPORT(ExchangeInfoStructures::Parameters::KetosisTreatmentPlanParameters) // Recommended (mandatory if virtual serialization)

namespace ExchangeInfoStructures
{
namespace Parameters
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(G1ketosisTreatmentType); \
    ar & BOOST_SERIALIZATION_NVP(G2ketosisTreatmentType); \
   
    IMPLEMENT_SERIALIZE_STD_METHODS(KetosisTreatmentPlanParameters);

    KetosisTreatmentPlanParameters::KetosisTreatmentPlanParameters()
    {
        // Default values
        G1ketosisTreatmentType.name = FunctionalConstants::Health::DEFAULT_G1_KETOSIS_TREATMENT_NAME;
        G1ketosisTreatmentType.effectDelayMeanOrMin = FunctionalConstants::Health::DEFAULT_G1_KETOSIS_TREATMENT_EFFECT_DELAY;
	G1ketosisTreatmentType.milkWaitingTime = FunctionalConstants::Health::DEFAULT_G1_KETOSIS_TREATMENT_MILK_WAITING_TIME;
        G1ketosisTreatmentType.successProbabilityAllCasesOrFirst = FunctionalConstants::Health::DEFAULT_G1_KETOSIS_TREATMENT_HEALING_PROBABILITY;
        G1ketosisTreatmentType.flareupRisk = FunctionalConstants::Health::DEFAULT_G1_KETOSIS_TREATMENT_FLARE_UP_RISK;

        G2ketosisTreatmentType.name = FunctionalConstants::Health::DEFAULT_G2_KETOSIS_TREATMENT_NAME;
        G2ketosisTreatmentType.effectDelayMeanOrMin = FunctionalConstants::Health::DEFAULT_G2_KETOSIS_TREATMENT_EFFECT_DELAY;
	G2ketosisTreatmentType.milkWaitingTime = FunctionalConstants::Health::DEFAULT_G2_KETOSIS_TREATMENT_MILK_WAITING_TIME;
        G2ketosisTreatmentType.successProbabilityAllCasesOrFirst = FunctionalConstants::Health::DEFAULT_G2_KETOSIS_TREATMENT_HEALING_PROBABILITY;
        G2ketosisTreatmentType.flareupRisk = FunctionalConstants::Health::DEFAULT_G2_KETOSIS_TREATMENT_FLARE_UP_RISK;
    }

    KetosisTreatmentPlanParameters::~KetosisTreatmentPlanParameters()
    {
    }
    
    bool KetosisTreatmentPlanParameters::operator==(KetosisTreatmentPlanParameters const& other)
    {
        return G1ketosisTreatmentType == other.G1ketosisTreatmentType and
               G2ketosisTreatmentType == other.G2ketosisTreatmentType;
    }

    // Constructeur de copie   
    KetosisTreatmentPlanParameters::KetosisTreatmentPlanParameters (const KetosisTreatmentPlanParameters &obj)
    {
	G1ketosisTreatmentType = obj.G1ketosisTreatmentType;
	G2ketosisTreatmentType = obj.G2ketosisTreatmentType;
    }

    bool KetosisTreatmentPlanParameters::autoControl(std::string &message)
    {
        return G1ketosisTreatmentType.autoControl(message) and G2ketosisTreatmentType.autoControl(message);
    } /* End of method AutoControl */  
        
    bool KetosisTreatmentPlanParameters::setImportedValue(const std::string &key, std::string &content)
    {
        if (key == KETOSIS_TREATMENT_PLAN_G1_NAME_PARAM_KEY) {G1ketosisTreatmentType.name = content; return true;}
        if (key == KETOSIS_TREATMENT_PLAN_G1_EFFECT_DELAY_PARAM_KEY) {G1ketosisTreatmentType.effectDelayMeanOrMin = std::atoi(content.c_str()); return true;}
        if (key == KETOSIS_TREATMENT_PLAN_G1_MILK_WAIT_TIME_PARAM_KEY) {G1ketosisTreatmentType.milkWaitingTime = std::atoi(content.c_str()); return true;}
        if (key == KETOSIS_TREATMENT_PLAN_G1_HEALING_PROBABILITY_PARAM_KEY) {G1ketosisTreatmentType.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == KETOSIS_TREATMENT_PLAN_G1_FLAREUP_RISK_PARAM_KEY) {G1ketosisTreatmentType.flareupRisk = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == KETOSIS_TREATMENT_PLAN_G2_NAME_PARAM_KEY) {G2ketosisTreatmentType.name = content; return true;}
        if (key == KETOSIS_TREATMENT_PLAN_G2_EFFECT_DELAY_PARAM_KEY) {G2ketosisTreatmentType.effectDelayMeanOrMin = std::atoi(content.c_str()); return true;}
        if (key == KETOSIS_TREATMENT_PLAN_G2_MILK_WAIT_TIME_PARAM_KEY) {G2ketosisTreatmentType.milkWaitingTime = std::atoi(content.c_str()); return true;}
        if (key == KETOSIS_TREATMENT_PLAN_G2_HEALING_PROBABILITY_PARAM_KEY) {G2ketosisTreatmentType.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == KETOSIS_TREATMENT_PLAN_G2_FLAREUP_RISK_PARAM_KEY) {G2ketosisTreatmentType.flareupRisk = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        
        return false; // Not here
    }
    
    bool KetosisTreatmentPlanParameters::getValueToExport(const std::string &key, std::string &content)
    {
        GET_STRING_VALUE_TO_IMPORT(KETOSIS_TREATMENT_PLAN_G1_NAME_PARAM_KEY, G1ketosisTreatmentType.name)
        GET_INTEGER_VALUE_TO_IMPORT(KETOSIS_TREATMENT_PLAN_G1_EFFECT_DELAY_PARAM_KEY, G1ketosisTreatmentType.effectDelayMeanOrMin)
        GET_INTEGER_VALUE_TO_IMPORT(KETOSIS_TREATMENT_PLAN_G1_MILK_WAIT_TIME_PARAM_KEY, G1ketosisTreatmentType.milkWaitingTime)
        GET_FLOAT_VALUE_TO_IMPORT(KETOSIS_TREATMENT_PLAN_G1_HEALING_PROBABILITY_PARAM_KEY, G1ketosisTreatmentType.successProbabilityAllCasesOrFirst)
        GET_FLOAT_VALUE_TO_IMPORT(KETOSIS_TREATMENT_PLAN_G1_FLAREUP_RISK_PARAM_KEY, G1ketosisTreatmentType.flareupRisk)
        GET_STRING_VALUE_TO_IMPORT(KETOSIS_TREATMENT_PLAN_G2_NAME_PARAM_KEY, G2ketosisTreatmentType.name)
        GET_INTEGER_VALUE_TO_IMPORT(KETOSIS_TREATMENT_PLAN_G2_EFFECT_DELAY_PARAM_KEY, G2ketosisTreatmentType.effectDelayMeanOrMin)
        GET_INTEGER_VALUE_TO_IMPORT(KETOSIS_TREATMENT_PLAN_G2_MILK_WAIT_TIME_PARAM_KEY, G2ketosisTreatmentType.milkWaitingTime)
        GET_FLOAT_VALUE_TO_IMPORT(KETOSIS_TREATMENT_PLAN_G2_HEALING_PROBABILITY_PARAM_KEY, G2ketosisTreatmentType.successProbabilityAllCasesOrFirst)
        GET_FLOAT_VALUE_TO_IMPORT(KETOSIS_TREATMENT_PLAN_G2_FLAREUP_RISK_PARAM_KEY, G2ketosisTreatmentType.flareupRisk)

        return false; // Not here
    }    
    
#ifdef _LOG
    void KetosisTreatmentPlanParameters::getDefaultValues(std::vector<std::string> &vLines)
    {
        std::string line = "";     
        line = KETOSIS_TREATMENT_PLAN_G1_NAME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G1_KETOSIS_TREATMENT_NAME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = KETOSIS_TREATMENT_PLAN_G1_EFFECT_DELAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G1_KETOSIS_TREATMENT_EFFECT_DELAY); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = KETOSIS_TREATMENT_PLAN_G1_MILK_WAIT_TIME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G1_KETOSIS_TREATMENT_MILK_WAITING_TIME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = KETOSIS_TREATMENT_PLAN_G1_HEALING_PROBABILITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G1_KETOSIS_TREATMENT_HEALING_PROBABILITY); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = KETOSIS_TREATMENT_PLAN_G1_FLAREUP_RISK_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G1_KETOSIS_TREATMENT_FLARE_UP_RISK); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = KETOSIS_TREATMENT_PLAN_G2_NAME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G2_KETOSIS_TREATMENT_NAME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = KETOSIS_TREATMENT_PLAN_G2_EFFECT_DELAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G2_KETOSIS_TREATMENT_EFFECT_DELAY); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = KETOSIS_TREATMENT_PLAN_G2_MILK_WAIT_TIME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G2_KETOSIS_TREATMENT_MILK_WAITING_TIME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = KETOSIS_TREATMENT_PLAN_G2_HEALING_PROBABILITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G2_KETOSIS_TREATMENT_HEALING_PROBABILITY); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = KETOSIS_TREATMENT_PLAN_G2_FLAREUP_RISK_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G2_KETOSIS_TREATMENT_FLARE_UP_RISK); vLines.push_back(Tools::changeChar(line,'.', ','));
    }
#endif // _LOG
    
} /* End of namespace Parameters */
}