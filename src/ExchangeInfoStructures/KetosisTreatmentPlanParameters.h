/////////////////////////////////////////////////
//                                             //
// File : KetosisTreatmentPlanParameters.h    //
//                                             //
// Author : Philippe Gontier                   //
//                                             //
// Date : 10/06/2020                           //
//                                             //
// Ketosis treatement settings                //
//                                             //
/////////////////////////////////////////////////
#ifndef Parameters_KetosisTreatmentPlanParameters_h
#define Parameters_KetosisTreatmentPlanParameters_h

// Standard

// Project
#include "../Tools/Serialization.h"
#include "TreatmentTypeParameters.h"

namespace ExchangeInfoStructures
{
namespace Parameters
{
    /// This structure make it possible to makes it possible to define the modalities to be implemented for curative ketosis treatments, differentiated by ketosis severity
    /// See the § 2.2.3.2.2.3 "Treatment plan" of the "Functional description and terms of use" documentation.
    struct KetosisTreatmentPlanParameters
    {        
    private:

    public :
        
        ///  Systemic G1 Ketosis treatment plan (see § 2.2.3.2.2.3 of the "Functional description and terms of use" documentation)
        TreatmentTypeParameters G1ketosisTreatmentType;
        
        ///  Systemic G2 Ketosis treatment plan (see § 2.2.3.2.2.3 of the "Functional description and terms of use" documentation)
        TreatmentTypeParameters G2ketosisTreatmentType;
        
    private:
        DECLARE_SERIALIZE_STD_METHODS;

    public:
        
        /// Value checking, errors in the std::string parameter reference
        bool autoControl (
                                    /// Reference std::string to put the error message (empty value if no error)
                                    std::string &message
                                    );
        KetosisTreatmentPlanParameters();
        virtual ~KetosisTreatmentPlanParameters();
        bool operator==(KetosisTreatmentPlanParameters const& other);
        KetosisTreatmentPlanParameters (const KetosisTreatmentPlanParameters &obj);        //constructeur de copie   
#ifdef _LOG
        static void getDefaultValues(std::vector<std::string> &vLines);
#endif // _LOG
        bool setImportedValue(const std::string &key, std::string &content);
        bool getValueToExport(const std::string &key, std::string &content);

    }; /* End of struct MastitisTreatmentPlanParameters */
    
    // Import/export key values
    static const std::string KETOSIS_TREATMENT_PLAN_G1_NAME_PARAM_KEY = "ketosisTreatmentPlan_G1_name";
    static const std::string KETOSIS_TREATMENT_PLAN_G1_EFFECT_DELAY_PARAM_KEY = "ketosisTreatmentPlan_G1_effectDelay";
    static const std::string KETOSIS_TREATMENT_PLAN_G1_MILK_WAIT_TIME_PARAM_KEY = "ketosisTreatmentPlan_G1_milkWaitTime";
    static const std::string KETOSIS_TREATMENT_PLAN_G1_HEALING_PROBABILITY_PARAM_KEY = "ketosisTreatmentPlan_G1_healingProbability";
    static const std::string KETOSIS_TREATMENT_PLAN_G1_FLAREUP_RISK_PARAM_KEY = "ketosisTreatmentPlan_G1_flareupRisk";
    static const std::string KETOSIS_TREATMENT_PLAN_G1_FIRSTIA_SUCCESS_RISK_PARAM_KEY = "ketosisTreatmentPlan_G1_firstIASuccessRisk";
    static const std::string KETOSIS_TREATMENT_PLAN_G2_NAME_PARAM_KEY = "ketosisTreatmentPlan_G2_name";
    static const std::string KETOSIS_TREATMENT_PLAN_G2_EFFECT_DELAY_PARAM_KEY = "ketosisTreatmentPlan_G2_effectDelay";
    static const std::string KETOSIS_TREATMENT_PLAN_G2_MILK_WAIT_TIME_PARAM_KEY = "ketosisTreatmentPlan_G2_milkWaitTime";
    static const std::string KETOSIS_TREATMENT_PLAN_G2_HEALING_PROBABILITY_PARAM_KEY = "ketosisTreatmentPlan_G2_healingProbability";
    static const std::string KETOSIS_TREATMENT_PLAN_G2_FLAREUP_RISK_PARAM_KEY = "ketosisTreatmentPlan_G2_flareupRisk";
    static const std::string KETOSIS_TREATMENT_PLAN_G2_FIRSTIA_SUCCESS_RISK_PARAM_KEY = "ketosisTreatmentPlan_G2_firstIASuccessRisk";
} /* End of namespace Parameters */
}
#endif // Parameters_KetosisTreatmentPlanParameters_h
