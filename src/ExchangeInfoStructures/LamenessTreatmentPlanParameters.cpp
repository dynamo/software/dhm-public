#include "LamenessTreatmentPlanParameters.h"

// project
#include "../Tools/Tools.h"
#include "FunctionalConstants.h"
#include "TechnicalConstants.h"
#include "HeadParameters.h"

BOOST_CLASS_EXPORT(ExchangeInfoStructures::Parameters::LamenessTreatmentPlanParameters) // Recommended (mandatory if virtual serialization)

namespace ExchangeInfoStructures
{
namespace Parameters
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(G1nonInfectiousLamenessTreatmentType); \
    ar & BOOST_SERIALIZATION_NVP(G2nonInfectiousLamenessTreatmentType); \
    ar & BOOST_SERIALIZATION_NVP(G1infectiousLamenessTreatmentType); \
    ar & BOOST_SERIALIZATION_NVP(G2infectiousLamenessTreatmentType); \
    
    IMPLEMENT_SERIALIZE_STD_METHODS(LamenessTreatmentPlanParameters);

    LamenessTreatmentPlanParameters::LamenessTreatmentPlanParameters()
    {
        // Default values
        G1nonInfectiousLamenessTreatmentType.name = FunctionalConstants::Health::DEFAULT_G1_NON_INFECTIOUS_LAMENESS_TREATMENT_NAME;
        G1nonInfectiousLamenessTreatmentType.effectDelayMeanOrMin = FunctionalConstants::Health::DEFAULT_G1_NON_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MIN;
        G1nonInfectiousLamenessTreatmentType.effectDelayMax = FunctionalConstants::Health::DEFAULT_G1_NON_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MAX;
	G1nonInfectiousLamenessTreatmentType.milkWaitingTime = FunctionalConstants::Health::DEFAULT_G1_NON_INFECTIOUS_LAMENESS_TREATMENT_MILK_WAITING_TIME;
        G1nonInfectiousLamenessTreatmentType.successProbabilityAllCasesOrFirst = FunctionalConstants::Health::DEFAULT_G1_NON_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_1;
        G1nonInfectiousLamenessTreatmentType.successProbabilitySecondAndOtherCases = FunctionalConstants::Health::DEFAULT_G1_NON_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_2;

        G2nonInfectiousLamenessTreatmentType.name = FunctionalConstants::Health::DEFAULT_G2_NON_INFECTIOUS_LAMENESS_TREATMENT_NAME;
        G2nonInfectiousLamenessTreatmentType.effectDelayMeanOrMin = FunctionalConstants::Health::DEFAULT_G2_NON_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MIN;
        G2nonInfectiousLamenessTreatmentType.effectDelayMax = FunctionalConstants::Health::DEFAULT_G2_NON_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MAX;
	G2nonInfectiousLamenessTreatmentType.milkWaitingTime = FunctionalConstants::Health::DEFAULT_G2_NON_INFECTIOUS_LAMENESS_TREATMENT_MILK_WAITING_TIME;
        G2nonInfectiousLamenessTreatmentType.successProbabilityAllCasesOrFirst = FunctionalConstants::Health::DEFAULT_G2_NON_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_1;
        G2nonInfectiousLamenessTreatmentType.successProbabilitySecondAndOtherCases = FunctionalConstants::Health::DEFAULT_G2_NON_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_2;

        G1infectiousLamenessTreatmentType.name = FunctionalConstants::Health::DEFAULT_G1_INFECTIOUS_LAMENESS_TREATMENT_NAME;
        G1infectiousLamenessTreatmentType.effectDelayMeanOrMin = FunctionalConstants::Health::DEFAULT_G1_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MIN;
        G1infectiousLamenessTreatmentType.effectDelayMax = FunctionalConstants::Health::DEFAULT_G1_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MAX;
	G1infectiousLamenessTreatmentType.milkWaitingTime = FunctionalConstants::Health::DEFAULT_G1_INFECTIOUS_LAMENESS_TREATMENT_MILK_WAITING_TIME;
        G1infectiousLamenessTreatmentType.successProbabilityAllCasesOrFirst = FunctionalConstants::Health::DEFAULT_G1_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_1;
        G1infectiousLamenessTreatmentType.successProbabilitySecondAndOtherCases = FunctionalConstants::Health::DEFAULT_G1_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_2;

        G2infectiousLamenessTreatmentType.name = FunctionalConstants::Health::DEFAULT_G2_INFECTIOUS_LAMENESS_TREATMENT_NAME;
        G2infectiousLamenessTreatmentType.effectDelayMeanOrMin = FunctionalConstants::Health::DEFAULT_G2_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MIN;
        G2infectiousLamenessTreatmentType.effectDelayMax = FunctionalConstants::Health::DEFAULT_G2_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MAX;
	G2infectiousLamenessTreatmentType.milkWaitingTime = FunctionalConstants::Health::DEFAULT_G2_INFECTIOUS_LAMENESS_TREATMENT_MILK_WAITING_TIME;
        G2infectiousLamenessTreatmentType.successProbabilityAllCasesOrFirst = FunctionalConstants::Health::DEFAULT_G2_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_1;
        G2infectiousLamenessTreatmentType.successProbabilitySecondAndOtherCases = FunctionalConstants::Health::DEFAULT_G2_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_2;
    }

    LamenessTreatmentPlanParameters::~LamenessTreatmentPlanParameters()
    {
    }
    
    bool LamenessTreatmentPlanParameters::operator==(LamenessTreatmentPlanParameters const& other)
    {
        return G1nonInfectiousLamenessTreatmentType == other.G1nonInfectiousLamenessTreatmentType and
               G1infectiousLamenessTreatmentType == other.G1infectiousLamenessTreatmentType and
               G2nonInfectiousLamenessTreatmentType == other.G2nonInfectiousLamenessTreatmentType and
               G2infectiousLamenessTreatmentType == other.G2infectiousLamenessTreatmentType;
    }

    // Constructeur de copie   
    LamenessTreatmentPlanParameters::LamenessTreatmentPlanParameters (const LamenessTreatmentPlanParameters &obj)
    {
        G1nonInfectiousLamenessTreatmentType = obj.G1nonInfectiousLamenessTreatmentType;
        G1infectiousLamenessTreatmentType = obj.G1infectiousLamenessTreatmentType;
        G2nonInfectiousLamenessTreatmentType = obj.G2nonInfectiousLamenessTreatmentType;
        G2infectiousLamenessTreatmentType = obj.G2infectiousLamenessTreatmentType;
    }

    bool LamenessTreatmentPlanParameters::autoControl(std::string &message)
    {
        return G1nonInfectiousLamenessTreatmentType.autoControl(message) and G1infectiousLamenessTreatmentType.autoControl(message) and G2nonInfectiousLamenessTreatmentType.autoControl(message) and G2infectiousLamenessTreatmentType.autoControl(message);
    } /* End of method AutoControl */  
        
    bool LamenessTreatmentPlanParameters::setImportedValue(const std::string &key, std::string &content)
    {
        if (key == LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_NAME_PARAM_KEY) {G1nonInfectiousLamenessTreatmentType.name = content; return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_MIN_EFFECT_DELAY_PARAM_KEY) {G1nonInfectiousLamenessTreatmentType.effectDelayMeanOrMin = std::atoi(content.c_str()); return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_MAX_EFFECT_DELAY_PARAM_KEY) {G1nonInfectiousLamenessTreatmentType.effectDelayMax = std::atoi(content.c_str()); return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_MILK_WAIT_TIME_PARAM_KEY) {G1nonInfectiousLamenessTreatmentType.milkWaitingTime = std::atoi(content.c_str()); return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_HEALING_PROBABILITY_1_PARAM_KEY) {G1nonInfectiousLamenessTreatmentType.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_HEALING_PROBABILITY_2_PARAM_KEY) {G1nonInfectiousLamenessTreatmentType.successProbabilitySecondAndOtherCases = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        
        if (key == LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_NAME_PARAM_KEY) {G2nonInfectiousLamenessTreatmentType.name = content; return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_MIN_EFFECT_DELAY_PARAM_KEY) {G2nonInfectiousLamenessTreatmentType.effectDelayMeanOrMin = std::atoi(content.c_str()); return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_MAX_EFFECT_DELAY_PARAM_KEY) {G2nonInfectiousLamenessTreatmentType.effectDelayMax = std::atoi(content.c_str()); return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_MILK_WAIT_TIME_PARAM_KEY) {G2nonInfectiousLamenessTreatmentType.milkWaitingTime = std::atoi(content.c_str()); return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_HEALING_PROBABILITY_1_PARAM_KEY) {G2nonInfectiousLamenessTreatmentType.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_HEALING_PROBABILITY_2_PARAM_KEY) {G2nonInfectiousLamenessTreatmentType.successProbabilitySecondAndOtherCases = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}

        if (key == LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_NAME_PARAM_KEY) {G1infectiousLamenessTreatmentType.name = content; return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_MIN_EFFECT_DELAY_PARAM_KEY) {G1infectiousLamenessTreatmentType.effectDelayMeanOrMin = std::atoi(content.c_str()); return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_MAX_EFFECT_DELAY_PARAM_KEY) {G1infectiousLamenessTreatmentType.effectDelayMax = std::atoi(content.c_str()); return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_MILK_WAIT_TIME_PARAM_KEY) {G1infectiousLamenessTreatmentType.milkWaitingTime = std::atoi(content.c_str()); return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_HEALING_PROBABILITY_1_PARAM_KEY) {G1infectiousLamenessTreatmentType.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_HEALING_PROBABILITY_2_PARAM_KEY) {G1infectiousLamenessTreatmentType.successProbabilitySecondAndOtherCases = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}

        if (key == LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_NAME_PARAM_KEY) {G2infectiousLamenessTreatmentType.name = content; return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_MIN_EFFECT_DELAY_PARAM_KEY) {G2infectiousLamenessTreatmentType.effectDelayMeanOrMin = std::atoi(content.c_str()); return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_MAX_EFFECT_DELAY_PARAM_KEY) {G2infectiousLamenessTreatmentType.effectDelayMax = std::atoi(content.c_str()); return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_MILK_WAIT_TIME_PARAM_KEY) {G2infectiousLamenessTreatmentType.milkWaitingTime = std::atoi(content.c_str()); return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_HEALING_PROBABILITY_1_PARAM_KEY) {G2infectiousLamenessTreatmentType.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_HEALING_PROBABILITY_2_PARAM_KEY) {G2infectiousLamenessTreatmentType.successProbabilitySecondAndOtherCases = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        
        return false; // Not here
    }
    
    bool LamenessTreatmentPlanParameters::getValueToExport(const std::string &key, std::string &content)
    {
        GET_STRING_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_NAME_PARAM_KEY, G1nonInfectiousLamenessTreatmentType.name)
        GET_INTEGER_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_MIN_EFFECT_DELAY_PARAM_KEY, G1nonInfectiousLamenessTreatmentType.effectDelayMeanOrMin)
        GET_INTEGER_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_MAX_EFFECT_DELAY_PARAM_KEY, G1nonInfectiousLamenessTreatmentType.effectDelayMax)
        GET_INTEGER_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_MILK_WAIT_TIME_PARAM_KEY, G1nonInfectiousLamenessTreatmentType.milkWaitingTime)
        GET_FLOAT_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_HEALING_PROBABILITY_1_PARAM_KEY, G1nonInfectiousLamenessTreatmentType.successProbabilityAllCasesOrFirst)
        GET_FLOAT_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_HEALING_PROBABILITY_2_PARAM_KEY, G1nonInfectiousLamenessTreatmentType.successProbabilitySecondAndOtherCases)
        
        GET_STRING_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_NAME_PARAM_KEY, G2nonInfectiousLamenessTreatmentType.name)
        GET_INTEGER_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_MIN_EFFECT_DELAY_PARAM_KEY, G2nonInfectiousLamenessTreatmentType.effectDelayMeanOrMin)
        GET_INTEGER_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_MAX_EFFECT_DELAY_PARAM_KEY, G2nonInfectiousLamenessTreatmentType.effectDelayMax)
        GET_INTEGER_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_MILK_WAIT_TIME_PARAM_KEY, G2nonInfectiousLamenessTreatmentType.milkWaitingTime)
        GET_FLOAT_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_HEALING_PROBABILITY_1_PARAM_KEY, G2nonInfectiousLamenessTreatmentType.successProbabilityAllCasesOrFirst)
        GET_FLOAT_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_HEALING_PROBABILITY_2_PARAM_KEY, G2nonInfectiousLamenessTreatmentType.successProbabilitySecondAndOtherCases)

        GET_STRING_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_NAME_PARAM_KEY, G1infectiousLamenessTreatmentType.name)
        GET_INTEGER_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_MIN_EFFECT_DELAY_PARAM_KEY, G1infectiousLamenessTreatmentType.effectDelayMeanOrMin)
        GET_INTEGER_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_MAX_EFFECT_DELAY_PARAM_KEY, G1infectiousLamenessTreatmentType.effectDelayMax)
        GET_INTEGER_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_MILK_WAIT_TIME_PARAM_KEY, G1infectiousLamenessTreatmentType.milkWaitingTime)
        GET_FLOAT_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_HEALING_PROBABILITY_1_PARAM_KEY, G1infectiousLamenessTreatmentType.successProbabilityAllCasesOrFirst)
        GET_FLOAT_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_HEALING_PROBABILITY_2_PARAM_KEY, G1infectiousLamenessTreatmentType.successProbabilitySecondAndOtherCases)

        GET_STRING_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_NAME_PARAM_KEY, G2infectiousLamenessTreatmentType.name)
        GET_INTEGER_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_MIN_EFFECT_DELAY_PARAM_KEY, G2infectiousLamenessTreatmentType.effectDelayMeanOrMin)
        GET_INTEGER_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_MAX_EFFECT_DELAY_PARAM_KEY, G2infectiousLamenessTreatmentType.effectDelayMax)
        GET_INTEGER_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_MILK_WAIT_TIME_PARAM_KEY, G2infectiousLamenessTreatmentType.milkWaitingTime)
        GET_FLOAT_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_HEALING_PROBABILITY_1_PARAM_KEY, G2infectiousLamenessTreatmentType.successProbabilityAllCasesOrFirst)
        GET_FLOAT_VALUE_TO_IMPORT(LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_HEALING_PROBABILITY_2_PARAM_KEY, G2infectiousLamenessTreatmentType.successProbabilitySecondAndOtherCases)

        return false; // Not here
    }    
    
#ifdef _LOG
    void LamenessTreatmentPlanParameters::getDefaultValues(std::vector<std::string> &vLines)
    {
        std::string line = "";   
        line = LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_NAME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G1_NON_INFECTIOUS_LAMENESS_TREATMENT_NAME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_MIN_EFFECT_DELAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G1_NON_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MIN); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_MAX_EFFECT_DELAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G1_NON_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MAX); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_MILK_WAIT_TIME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G1_NON_INFECTIOUS_LAMENESS_TREATMENT_MILK_WAITING_TIME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_HEALING_PROBABILITY_1_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G1_NON_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_1); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_HEALING_PROBABILITY_2_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G1_NON_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_2); vLines.push_back(Tools::changeChar(line,'.', ','));
        
        line = LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_NAME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G2_NON_INFECTIOUS_LAMENESS_TREATMENT_NAME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_MIN_EFFECT_DELAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G2_NON_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MIN); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_MAX_EFFECT_DELAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G2_NON_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MAX); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_MILK_WAIT_TIME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G2_NON_INFECTIOUS_LAMENESS_TREATMENT_MILK_WAITING_TIME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_HEALING_PROBABILITY_1_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G2_NON_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_1); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_HEALING_PROBABILITY_2_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G2_NON_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_2); vLines.push_back(Tools::changeChar(line,'.', ','));

        line = LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_NAME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G1_INFECTIOUS_LAMENESS_TREATMENT_NAME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_MIN_EFFECT_DELAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G1_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MIN); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_MAX_EFFECT_DELAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G1_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MAX); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_MILK_WAIT_TIME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G1_INFECTIOUS_LAMENESS_TREATMENT_MILK_WAITING_TIME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_HEALING_PROBABILITY_1_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G1_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_1); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_HEALING_PROBABILITY_2_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G1_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_2); vLines.push_back(Tools::changeChar(line,'.', ','));

        line = LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_NAME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G2_INFECTIOUS_LAMENESS_TREATMENT_NAME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_MIN_EFFECT_DELAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G2_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MIN); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_MAX_EFFECT_DELAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G2_INFECTIOUS_LAMENESS_TREATMENT_EFFECT_DELAY_MAX); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_MILK_WAIT_TIME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G2_INFECTIOUS_LAMENESS_TREATMENT_MILK_WAITING_TIME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_HEALING_PROBABILITY_1_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G2_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_1); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_HEALING_PROBABILITY_2_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_G2_INFECTIOUS_LAMENESS_TREATMENT_HEALING_PROBABILITY_2); vLines.push_back(Tools::changeChar(line,'.', ','));

    }
#endif // _LOG
    
} /* End of namespace Parameters */
}