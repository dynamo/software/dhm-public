/////////////////////////////////////////////////
//                                             //
// File : LamenessTreatmentPlanParameters.h    //
//                                             //
// Author : Philippe Gontier                   //
//                                             //
// Date : 10/06/2020                           //
//                                             //
// Ketosis treatement settings                 //
//                                             //
/////////////////////////////////////////////////
#ifndef Parameters_LamenessTreatmentPlanParameters_h
#define Parameters_LamenessTreatmentPlanParameters_h

// Standard

// Project
#include "../Tools/Serialization.h"
#include "TreatmentTypeParameters.h"

namespace ExchangeInfoStructures
{
namespace Parameters
{
    /// This structure make it possible to makes it possible to define the modalities to be implemented for curative ketosis treatments, differentiated by ketosis severity
    /// See the § 2.2.3.3.2.3 "Treatment plan" of the "Functional description and terms of use" documentation.
    struct LamenessTreatmentPlanParameters
    {        
    private:

    public :
        
        ///  Systemic G1 non infectious lameness treatment plan (see § 2.2.3.3.2.3 of the "Functional description and terms of use" documentation)
        TreatmentTypeParameters G1nonInfectiousLamenessTreatmentType;
        
        ///  Systemic G2 non infectious  lameness treatment plan (see § 2.2.3.3.2.3 of the "Functional description and terms of use" documentation)
        TreatmentTypeParameters G2nonInfectiousLamenessTreatmentType;
        
        ///  Systemic G1 infectious  lameness treatment plan (see § 2.2.3.3.2.3 of the "Functional description and terms of use" documentation)
        TreatmentTypeParameters G1infectiousLamenessTreatmentType;
        
        ///  Systemic G2 infectious  lameness treatment plan (see § 2.2.3.3.2.3 of the "Functional description and terms of use" documentation)
        TreatmentTypeParameters G2infectiousLamenessTreatmentType;
        
    private:
        DECLARE_SERIALIZE_STD_METHODS;

    public:
        
        /// Value checking, errors in the std::string parameter reference
        bool autoControl (
                                    /// Reference std::string to put the error message (empty value if no error)
                                    std::string &message
                                    );
        LamenessTreatmentPlanParameters();
        virtual ~LamenessTreatmentPlanParameters();
        bool operator==(LamenessTreatmentPlanParameters const& other);
        LamenessTreatmentPlanParameters (const LamenessTreatmentPlanParameters &obj);        //constructeur de copie   
#ifdef _LOG
        static void getDefaultValues(std::vector<std::string> &vLines);
#endif // _LOG
        bool setImportedValue(const std::string &key, std::string &content);
        bool getValueToExport(const std::string &key, std::string &content);

    }; /* End of struct LamenessTreatmentPlanParameters */
    
    // Import/export key values
    static const std::string LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_NAME_PARAM_KEY = "lamenessTreatmentPlan_G1_non_infectious_name";
    static const std::string LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_MIN_EFFECT_DELAY_PARAM_KEY = "lamenessTreatmentPlan_G1_non_infectious_min_effectDelay";
    static const std::string LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_MAX_EFFECT_DELAY_PARAM_KEY = "lamenessTreatmentPlan_G1_non_infectious_max_effectDelay";
    static const std::string LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_MILK_WAIT_TIME_PARAM_KEY = "lamenessTreatmentPlan_G1_non_infectious_milkWaitTime";
    static const std::string LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_HEALING_PROBABILITY_1_PARAM_KEY = "lamenessTreatmentPlan_G1_non_infectious_healingProbability_1";
    static const std::string LAMENESS_TREATMENT_PLAN_G1_NON_INFECTIOUS_HEALING_PROBABILITY_2_PARAM_KEY = "lamenessTreatmentPlan_G1_non_infectious_healingProbability_2";

    static const std::string LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_NAME_PARAM_KEY = "lamenessTreatmentPlan_G2_non_infectious_name";
    static const std::string LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_MIN_EFFECT_DELAY_PARAM_KEY = "lamenessTreatmentPlan_G2_non_infectious_min_effectDelay";
    static const std::string LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_MAX_EFFECT_DELAY_PARAM_KEY = "lamenessTreatmentPlan_G2_non_infectious_max_effectDelay";
    static const std::string LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_MILK_WAIT_TIME_PARAM_KEY = "lamenessTreatmentPlan_G2_non_infectious_milkWaitTime";
    static const std::string LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_HEALING_PROBABILITY_1_PARAM_KEY = "lamenessTreatmentPlan_G2_non_infectious_healingProbability_1";
    static const std::string LAMENESS_TREATMENT_PLAN_G2_NON_INFECTIOUS_HEALING_PROBABILITY_2_PARAM_KEY = "lamenessTreatmentPlan_G2_non_infectious_healingProbability_2";

    static const std::string LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_NAME_PARAM_KEY = "lamenessTreatmentPlan_G1_infectious_name";
    static const std::string LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_MIN_EFFECT_DELAY_PARAM_KEY = "lamenessTreatmentPlan_G1_infectious_min_effectDelay";
    static const std::string LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_MAX_EFFECT_DELAY_PARAM_KEY = "lamenessTreatmentPlan_G1_infectious_max_effectDelay";
    static const std::string LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_MILK_WAIT_TIME_PARAM_KEY = "lamenessTreatmentPlan_G1_infectious_milkWaitTime";
    static const std::string LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_HEALING_PROBABILITY_1_PARAM_KEY = "lamenessTreatmentPlan_G1_infectious_healingProbability_1";
    static const std::string LAMENESS_TREATMENT_PLAN_G1_INFECTIOUS_HEALING_PROBABILITY_2_PARAM_KEY = "lamenessTreatmentPlan_G1_infectious_healingProbability_2";

    static const std::string LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_NAME_PARAM_KEY = "lamenessTreatmentPlan_G2_infectious_name";
    static const std::string LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_MIN_EFFECT_DELAY_PARAM_KEY = "lamenessTreatmentPlan_G2_infectious_min_effectDelay";
    static const std::string LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_MAX_EFFECT_DELAY_PARAM_KEY = "lamenessTreatmentPlan_G2_infectious_max_effectDelay";
    static const std::string LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_MILK_WAIT_TIME_PARAM_KEY = "lamenessTreatmentPlan_G2_infectious_milkWaitTime";
    static const std::string LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_HEALING_PROBABILITY_1_PARAM_KEY = "lamenessTreatmentPlan_G2_infectious_healingProbability_1";
    static const std::string LAMENESS_TREATMENT_PLAN_G2_INFECTIOUS_HEALING_PROBABILITY_2_PARAM_KEY = "lamenessTreatmentPlan_G2_infectious_healingProbability_2";
} /* End of namespace Parameters */
}
#endif // Parameters_LamenessTreatmentPlanParameters_h
