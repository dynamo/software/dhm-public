////////////////////////////////////
//                                //
// File : LanguageDictionary.cpp  //
//                                //
// Author : Philippe Gontier      //
//                                //
// Version 1.0                    //
//                                //
// Date : 01/10/2015              //
//                                //
////////////////////////////////////

#include "LanguageDictionary.h"

// project

namespace ExchangeInfoStructures
{
    // constructor
    LanguageDictionary::LanguageDictionary(LanguageDictionary::language lang)
    {
        _language = lang;
        s_mpAllLanguageDictionary[lang] = this; // Insert to the collection
        s_pCurrentLanguageDictionary = this;
    }
    
    // destructor
    LanguageDictionary::~LanguageDictionary()
    {
        s_mpAllLanguageDictionary.erase(_language);        
    }
    
    LanguageDictionary::language LanguageDictionary::getLanguage()
    {
        return _language;
    }
        
    void LanguageDictionary::clear()
    {
         for (std::map<language,LanguageDictionary*>::iterator it = s_mpAllLanguageDictionary.begin(); it != s_mpAllLanguageDictionary.end(); it = s_mpAllLanguageDictionary.begin())
        {
            delete it->second;
        }
        s_pCurrentLanguageDictionary = nullptr;
    }
        
    // Give statically the message 
    std::string LanguageDictionary::getMessage(unsigned int  messageKey)
    {
        return s_pCurrentLanguageDictionary->getInstanceMessage(messageKey); // ask to the current language
    }
    
    // Give the message by the current language 
    std::string LanguageDictionary::getInstanceMessage(unsigned int  messageKey)
    {
        std::map<unsigned int , std::string>::iterator it = _mpMessageDictionary.find(messageKey);
        std::string message = "!!No result for " + messageKey; // if no response
        
        if(it != _mpMessageDictionary.end())
        {
            // we have a response
            message = it->second;
        }
        return message;
    }
 
    LanguageDictionary* LanguageDictionary::s_pCurrentLanguageDictionary = nullptr;
    std::map<LanguageDictionary::language,LanguageDictionary*> LanguageDictionary::s_mpAllLanguageDictionary;

} /* End of namespace ExchangeInfoStructures */
