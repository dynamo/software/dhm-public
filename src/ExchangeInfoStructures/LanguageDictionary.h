////////////////////////////////////
//                                //
// File : LanguageDictionary.h    //
//                                //
// Author : Philippe Gontier      //
//                                //
// Version 1.0                    //
//                                //
// Date : 01/10/2015              //
//                                //
////////////////////////////////////

#ifndef ExchangeInfoStructures_LanguageDictionary_h
#define ExchangeInfoStructures_LanguageDictionary_h

// standard
#include <string>
#include <map>

namespace ExchangeInfoStructures
{
   // All the keys known for Simulator - Value 1 to 100 reserved
    const unsigned int KEY_THE_NAME_IS_EMPTY = 4;
    const unsigned int KEY_THE_SYSTEM_TO_SIMULATE = 7;
    const unsigned int KEY_ALREADY_EXISTS = 8;
    const unsigned int KEY_THE_ACCOUNTING_MODEL = 17;
    const unsigned int KEY_THE_PROTOCOL = 18;
    const unsigned int KEY_IS_UNKNOWN = 19;
    const unsigned int KEY_THE_RUN_NUMBER_IS_NOT_VALID = 20;
    const unsigned int KEY_THE_RUN_DURATION_IS_NOT_VALID = 21;
    const unsigned int KEY_THE_DATE_OF_THE_SIMULATION_BEGIN_IS_NOT_VALID = 26;
    
    // All the keys known for DHM - over 100
    const unsigned int KEY_THE_ANIMAL_NUMBER_IS_NOT_VALID = 105;
    const unsigned int KEY_THE_RENEWAL_RATIO_IS_NOT_VALID = 114;
    const unsigned int KEY_THE_COW_MASTITIS_INCIDENCE_PREVENTION_FACTOR_IS_NOT_VALID = 115;
    const unsigned int KEY_THE_HERD_MASTITIS_INCIDENCE_PREVENTION_FACTOR_IS_NOT_VALID = 116;
    const unsigned int KEY_THE_KETOSIS_INCIDENCE_PREVENTION_FACTOR_IS_NOT_VALID = 117;
    const unsigned int KEY_THE_NI_LAMENESS_INCIDENCE_PREVENTION_FACTOR_IS_NOT_VALID = 118;
    const unsigned int KEY_THE_I_LAMENESS_INCIDENCE_PREVENTION_FACTOR_IS_NOT_VALID = 119;
    const unsigned int KEY_A_TYPE_RATIO_IS_NEGATIVE = 122;
    const unsigned int KEY_THE_TYPE_RATIO_SUM_NOT_EQUAL_TO_0_OR_1 = 123;
    const unsigned int KEY_THE_QUANTITY_IS_NEGATIVE = 124;
    const unsigned int KEY_THE_RATIO_SUM_NOT_EQUAL_TO_1 = 125;

    class LanguageDictionary
    {
    public:
        enum language {en, fr};
        
    protected:
        language _language; // associated language
        static std::map<language,LanguageDictionary*> s_mpAllLanguageDictionary; // here are all the language dictionary objects 
        static LanguageDictionary* s_pCurrentLanguageDictionary; // here is the label dictionary
        std::map<unsigned int,std::string> _mpMessageDictionary; // Here are all the labels, accessibles by their key. Filled by the child classes
        LanguageDictionary(LanguageDictionary::language lang); // knowing language constructor
        std::string getInstanceMessage(unsigned int  messageKey); // Give the message in the appropriate language
      
    public:
        virtual ~LanguageDictionary(); // destructor
        static void clear();
        LanguageDictionary::language getLanguage();
        static std::string getMessage(unsigned int messageKey);
    };
} /* End of namespace ExchangeInfoStructures */
#endif // ExchangeInfoStructures_LanguageDictionary_h
