#include "MastitisTreatmentPlanParameters.h"

// project
#include "../Tools/Tools.h"
#include "FunctionalConstants.h"
#include "TechnicalConstants.h"
#include "HeadParameters.h"

BOOST_CLASS_EXPORT(ExchangeInfoStructures::Parameters::MastitisTreatmentPlanParameters) // Recommended (mandatory if virtual serialization)

namespace ExchangeInfoStructures
{
namespace Parameters
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(mastitisLactationTreatmentTypes); \
    ar & BOOST_SERIALIZATION_NVP(mastitisDryTreatmentTypes); \
   
    IMPLEMENT_SERIALIZE_STD_METHODS(MastitisTreatmentPlanParameters);

    MastitisTreatmentPlanParameters::MastitisTreatmentPlanParameters()
    {
        // Default values
        TreatmentTypeParameters mtt = TreatmentTypeParameters();
        mtt.name = FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_NAME;
        mtt.effectDelayMeanOrMin = FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_EFFECT_DELAY;
	mtt.milkWaitingTime = FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_MILK_WAITING_TIME;
	mtt.flareupRisk = FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_FLARE_UP_RISK;
        for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++)
        {
            mtt.successProbabilityAllCasesOrFirst = FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_HEALING_PROBABILITY.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second;
            mtt.protectProbability = FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_PROTECT_PROBABILITY.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second;
            mtt.protectDuration = FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_PROTECT_DURATION.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second;
            mastitisLactationTreatmentTypes[(FunctionalEnumerations::Health::BacteriumType)iBact] = mtt;
        }
        
        mtt.name = FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_NAME;
        mtt.effectDelayMeanOrMin = FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_EFFECT_DELAY;
	mtt.milkWaitingTime = FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_MILK_WAITING_TIME;
	mtt.flareupRisk = FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_FLARE_UP_RISK;
        for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++)
        {
            mtt.successProbabilityAllCasesOrFirst = FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_HEALING_PROBABILITY.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second;
            mtt.protectProbability = FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_PROTECT_PROBABILITY.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second;
            mtt.protectDuration = FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_PROTECT_DURATION.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second;
            mastitisDryTreatmentTypes[(FunctionalEnumerations::Health::BacteriumType)iBact] = mtt;
        }
    }
    
    MastitisTreatmentPlanParameters::~MastitisTreatmentPlanParameters()
    {
    }
    
    bool MastitisTreatmentPlanParameters::operator==(MastitisTreatmentPlanParameters const& other)
    {
        bool res = true;
        for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType and res; iBact++)
        {
            res = mastitisLactationTreatmentTypes.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second == other.mastitisLactationTreatmentTypes.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second;
        }
        for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType and res; iBact++)
        {
            res = mastitisDryTreatmentTypes.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second == other.mastitisDryTreatmentTypes.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second;
        }
        return res;
    }

    // Constructeur de copie   
    MastitisTreatmentPlanParameters::MastitisTreatmentPlanParameters (const MastitisTreatmentPlanParameters &obj)
    {
	mastitisLactationTreatmentTypes = obj.mastitisLactationTreatmentTypes;
	mastitisDryTreatmentTypes = obj.mastitisDryTreatmentTypes;
    }

    bool MastitisTreatmentPlanParameters::autoControl(std::string &message)
    {
        bool res = true;
        for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType and res; iBact++)
        {
            res = mastitisLactationTreatmentTypes.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second.autoControl(message);
        }
        for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType and res; iBact++)
        {
            res = mastitisDryTreatmentTypes.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second.autoControl(message);
        }
        return res;
    } /* End of method AutoControl */    
    
    bool MastitisTreatmentPlanParameters::setImportedValue(const std::string &key, std::string &content)
    {
        if (key == MASTITIS_TREATMENT_PLAN_LACTATION_NAME_PARAM_KEY) {for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++) mastitisLactationTreatmentTypes.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second.name = content; return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_LACTATION_EFFECT_DELAY_PARAM_KEY) {for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++) mastitisLactationTreatmentTypes.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second.effectDelayMeanOrMin = std::atoi(content.c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_LACTATION_MILK_WAIT_TIME_PARAM_KEY) {for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++) mastitisLactationTreatmentTypes.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second.milkWaitingTime = std::atoi(content.c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_STAPHA_PARAM_KEY) {mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_STAPHA_PARAM_KEY) {mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.protectProbability = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_STAPHA_PARAM_KEY) {mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.protectDuration = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_STREPTU_PARAM_KEY) {mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_STREPTU_PARAM_KEY) {mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.protectProbability = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_STREPTU_PARAM_KEY) {mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.protectDuration = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_GN_PARAM_KEY) {mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_GN_PARAM_KEY) {mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.protectProbability = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_GN_PARAM_KEY) {mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.protectDuration = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_CNS_PARAM_KEY) {mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_CNS_PARAM_KEY) {mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.protectProbability = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_CNS_PARAM_KEY) {mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.protectDuration = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_CB_PARAM_KEY) {mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_CB_PARAM_KEY) {mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.protectProbability = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_CB_PARAM_KEY) {mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.protectDuration = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_DRY_NAME_PARAM_KEY) {for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++) mastitisDryTreatmentTypes.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second.name = content; return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_DRY_EFFECT_DELAY_PARAM_KEY) {for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++) mastitisDryTreatmentTypes.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second.effectDelayMeanOrMin = std::atoi(content.c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_DRY_MILK_WAIT_TIME_PARAM_KEY) {for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++) mastitisDryTreatmentTypes.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second.milkWaitingTime = std::atoi(content.c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_STAPHA_PARAM_KEY) {mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_STAPHA_PARAM_KEY) {mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.protectProbability = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_STAPHA_PARAM_KEY) {mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.protectDuration = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_STREPTU_PARAM_KEY) {mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_STREPTU_PARAM_KEY) {mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.protectProbability = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_STREPTU_PARAM_KEY) {mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.protectDuration = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_GN_PARAM_KEY) {mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_GN_PARAM_KEY) {mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.protectProbability = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_GN_PARAM_KEY) {mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.protectDuration = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_CNS_PARAM_KEY) {mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_CNS_PARAM_KEY) {mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.protectProbability = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_CNS_PARAM_KEY) {mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.protectDuration = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_CB_PARAM_KEY) {mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_CB_PARAM_KEY) {mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.protectProbability = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 
        if (key == MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_CB_PARAM_KEY) {mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.protectDuration = atof(Tools::changeChar(content, ',', '.').c_str()); return true;} 

        return false; // Not here
    }
    
    bool MastitisTreatmentPlanParameters::getValueToExport(const std::string &key, std::string &content)
    {
        GET_STRING_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_LACTATION_NAME_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::lastBacteriumType)->second.name)
        GET_INTEGER_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_LACTATION_EFFECT_DELAY_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::lastBacteriumType)->second.effectDelayMeanOrMin)
        GET_INTEGER_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_LACTATION_MILK_WAIT_TIME_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::lastBacteriumType)->second.milkWaitingTime)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_STAPHA_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.successProbabilityAllCasesOrFirst)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_STAPHA_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.protectProbability)
        GET_INTEGER_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_STAPHA_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.protectDuration)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_STREPTU_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.successProbabilityAllCasesOrFirst) 
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_STREPTU_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.protectProbability)
        GET_INTEGER_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_STREPTU_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.protectDuration)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_GN_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.successProbabilityAllCasesOrFirst)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_GN_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.protectProbability)
        GET_INTEGER_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_GN_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.protectDuration)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_CNS_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.successProbabilityAllCasesOrFirst)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_CNS_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.protectProbability)
        GET_INTEGER_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_CNS_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.protectDuration)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_CB_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.successProbabilityAllCasesOrFirst)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_CB_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.protectProbability)
        GET_INTEGER_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_CB_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.protectDuration)
        GET_STRING_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_DRY_NAME_PARAM_KEY, mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::lastBacteriumType)->second.name)
        GET_INTEGER_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_DRY_EFFECT_DELAY_PARAM_KEY, mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::lastBacteriumType)->second.effectDelayMeanOrMin)
        GET_INTEGER_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_DRY_MILK_WAIT_TIME_PARAM_KEY, mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::lastBacteriumType)->second.milkWaitingTime)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_STAPHA_PARAM_KEY, mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.successProbabilityAllCasesOrFirst)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_STAPHA_PARAM_KEY, mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.protectProbability)
        GET_INTEGER_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_STAPHA_PARAM_KEY, mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second.protectDuration)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_STREPTU_PARAM_KEY, mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.successProbabilityAllCasesOrFirst)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_STREPTU_PARAM_KEY, mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.protectProbability)
        GET_INTEGER_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_STREPTU_PARAM_KEY, mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second.protectDuration)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_GN_PARAM_KEY, mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.successProbabilityAllCasesOrFirst)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_GN_PARAM_KEY, mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.protectProbability)
        GET_INTEGER_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_GN_PARAM_KEY, mastitisLactationTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second.protectDuration) 
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_CNS_PARAM_KEY, mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.successProbabilityAllCasesOrFirst)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_CNS_PARAM_KEY, mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.protectProbability)
        GET_INTEGER_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_CNS_PARAM_KEY, mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second.protectDuration)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_CB_PARAM_KEY, mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.successProbabilityAllCasesOrFirst)
        GET_FLOAT_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_CB_PARAM_KEY, mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.protectProbability)
        GET_INTEGER_VALUE_TO_IMPORT(MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_CB_PARAM_KEY, mastitisDryTreatmentTypes.find(FunctionalEnumerations::Health::BacteriumType::CB)->second.protectDuration)

        return false; // Not here
    }    
    
#ifdef _LOG
    void MastitisTreatmentPlanParameters::getDefaultValues(std::vector<std::string> &vLines)
    {
        std::string line = "";
        line = MASTITIS_TREATMENT_PLAN_LACTATION_NAME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_NAME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_LACTATION_EFFECT_DELAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_EFFECT_DELAY); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_LACTATION_MILK_WAIT_TIME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_MILK_WAITING_TIME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_STAPHA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_HEALING_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_STAPHA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_PROTECT_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_STAPHA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_PROTECT_DURATION.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_STREPTU_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_HEALING_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_STREPTU_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_PROTECT_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_STREPTU_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_PROTECT_DURATION.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_GN_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_HEALING_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_GN_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_PROTECT_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_GN_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_PROTECT_DURATION.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_CNS_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_HEALING_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_CNS_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_PROTECT_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_CNS_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_PROTECT_DURATION.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_CB_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_HEALING_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::CB)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_CB_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_PROTECT_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::CB)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_CB_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_LACTATION_TREATMENT_PROTECT_DURATION.find(FunctionalEnumerations::Health::BacteriumType::CB)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_DRY_NAME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_NAME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_DRY_EFFECT_DELAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_EFFECT_DELAY); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_DRY_MILK_WAIT_TIME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_MILK_WAITING_TIME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_STAPHA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_HEALING_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_STAPHA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_PROTECT_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_STAPHA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_PROTECT_DURATION.find(FunctionalEnumerations::Health::BacteriumType::StaphA)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_STREPTU_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_HEALING_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_STREPTU_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_PROTECT_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_STREPTU_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_PROTECT_DURATION.find(FunctionalEnumerations::Health::BacteriumType::StreptU)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_GN_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_HEALING_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_GN_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_PROTECT_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_GN_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_PROTECT_DURATION.find(FunctionalEnumerations::Health::BacteriumType::Gn)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_CNS_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_HEALING_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_CNS_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_PROTECT_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_CNS_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_PROTECT_DURATION.find(FunctionalEnumerations::Health::BacteriumType::CNS)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_CB_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_HEALING_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::CB)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_CB_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_PROTECT_PROBABILITY.find(FunctionalEnumerations::Health::BacteriumType::CB)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_CB_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Health::DEFAULT_MASTITIS_DRY_TREATMENT_PROTECT_DURATION.find(FunctionalEnumerations::Health::BacteriumType::CB)->second); vLines.push_back(Tools::changeChar(line,'.', ','));
    }
#endif // _LOG

    
} /* End of namespace Parameters */
}