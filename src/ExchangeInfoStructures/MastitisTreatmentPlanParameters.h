/////////////////////////////////////////////////
//                                             //
// File : MastitisTreatmentPlanParameters.h    //
//                                             //
// Author : Philippe Gontier                   //
//                                             //
// Date : 01/10/2015                           //
//                                             //
// Mastitis treatement settings                //
//                                             //
/////////////////////////////////////////////////
#ifndef Parameters_MastitisTreatmentPlanParameters_h
#define Parameters_MastitisTreatmentPlanParameters_h

// Standard

// Project
#include "../Tools/Serialization.h"

// project
#include "FunctionalEnumerations.h"
#include "TreatmentTypeParameters.h"

namespace ExchangeInfoStructures
{
namespace Parameters
{
    /// This structure make it possible to makes it possible to define the modalities to be implemented for curative mastitis treatments, differentiated for lactation and drying periods
    /// See the § 2.2.3.1.2.4 "Treatment plan" of the "Functional description and terms of use" documentation.
    struct MastitisTreatmentPlanParameters
    {        
    private:

    public :
        
        ///  Mastitis treatment plan during lactation period (see § 2.2.3.1.2.4 of the "Functional description and terms of use" documentation)
        std::map<FunctionalEnumerations::Health::BacteriumType, TreatmentTypeParameters> mastitisLactationTreatmentTypes;

        ///  Mastitis treatment plan during dry period (see § 2.2.3.1.2.4 of the "Functional description and terms of use" documentation)
        std::map<FunctionalEnumerations::Health::BacteriumType, TreatmentTypeParameters> mastitisDryTreatmentTypes;
        
    private:
        DECLARE_SERIALIZE_STD_METHODS;

    public:
        
        /// Value checking, errors in the std::string parameter reference
        bool autoControl (
                                    /// Reference std::string to put the error message (empty value if no error)
                                    std::string &message
                                    );
        MastitisTreatmentPlanParameters();
        virtual ~MastitisTreatmentPlanParameters();
        bool operator==(MastitisTreatmentPlanParameters const& other);
        MastitisTreatmentPlanParameters (const MastitisTreatmentPlanParameters &obj);        //constructeur de copie   
#ifdef _LOG
        static void getDefaultValues(std::vector<std::string> &vLines);
#endif // _LOG
        bool setImportedValue(const std::string &key, std::string &content);
        bool getValueToExport(const std::string &key, std::string &content);

    }; /* End of struct MastitisTreatmentPlanParameters */
        
    // Import/export key values
    static const std::string MASTITIS_TREATMENT_PLAN_LACTATION_NAME_PARAM_KEY = "mastitisTreatmentPlan_Lactation_name";
    static const std::string MASTITIS_TREATMENT_PLAN_LACTATION_EFFECT_DELAY_PARAM_KEY = "mastitisTreatmentPlan_Lactation_effectDelay";
    static const std::string MASTITIS_TREATMENT_PLAN_LACTATION_MILK_WAIT_TIME_PARAM_KEY = "mastitisTreatmentPlan_Lactation_milkWaitTime";
    static const std::string MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_STAPHA_PARAM_KEY = "mastitisTreatmentPlan_Lactation_healingProbability_StaphA";
    static const std::string MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_STAPHA_PARAM_KEY = "mastitisTreatmentPlan_Lactation_protectProbability_StaphA";
    static const std::string MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_STAPHA_PARAM_KEY = "mastitisTreatmentPlan_Lactation_protectDuration_StaphA";
    static const std::string MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_STREPTU_PARAM_KEY = "mastitisTreatmentPlan_Lactation_healingProbability_StreptU";
    static const std::string MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_STREPTU_PARAM_KEY = "mastitisTreatmentPlan_Lactation_protectProbability_StreptU";
    static const std::string MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_STREPTU_PARAM_KEY = "mastitisTreatmentPlan_Lactation_protectDuration_StreptU";
    static const std::string MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_GN_PARAM_KEY = "mastitisTreatmentPlan_Lactation_healingProbability_Gn";
    static const std::string MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_GN_PARAM_KEY = "mastitisTreatmentPlan_Lactation_protectProbability_Gn";
    static const std::string MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_GN_PARAM_KEY = "mastitisTreatmentPlan_Lactation_protectDuration_Gn";
    static const std::string MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_CNS_PARAM_KEY = "mastitisTreatmentPlan_Lactation_healingProbability_CNS";
    static const std::string MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_CNS_PARAM_KEY = "mastitisTreatmentPlan_Lactation_protectProbability_CNS";
    static const std::string MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_CNS_PARAM_KEY = "mastitisTreatmentPlan_Lactation_protectDuration_CNS";
    static const std::string MASTITIS_TREATMENT_PLAN_LACTATION_HEALING_PROBABILITY_CB_PARAM_KEY = "mastitisTreatmentPlan_Lactation_healingProbability_CB";
    static const std::string MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_PROBABILITY_CB_PARAM_KEY = "mastitisTreatmentPlan_Lactation_protectProbability_CB";
    static const std::string MASTITIS_TREATMENT_PLAN_LACTATION_PROTECT_DURATION_CB_PARAM_KEY = "mastitisTreatmentPlan_Lactation_protectDuration_CB";
    static const std::string MASTITIS_TREATMENT_PLAN_DRY_NAME_PARAM_KEY = "mastitisTreatmentPlan_Dry_name";
    static const std::string MASTITIS_TREATMENT_PLAN_DRY_EFFECT_DELAY_PARAM_KEY = "mastitisTreatmentPlan_Dry_effectDelay";
    static const std::string MASTITIS_TREATMENT_PLAN_DRY_MILK_WAIT_TIME_PARAM_KEY = "mastitisTreatmentPlan_Dry_milkWaitTime";
    static const std::string MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_STAPHA_PARAM_KEY = "mastitisTreatmentPlan_Dry_healingProbability_StaphA";
    static const std::string MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_STAPHA_PARAM_KEY = "mastitisTreatmentPlan_Dry_protectProbability_StaphA";
    static const std::string MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_STAPHA_PARAM_KEY = "mastitisTreatmentPlan_Dry_protectDuration_StaphA";
    static const std::string MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_STREPTU_PARAM_KEY = "mastitisTreatmentPlan_Dry_healingProbability_StreptU";
    static const std::string MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_STREPTU_PARAM_KEY = "mastitisTreatmentPlan_Dry_protectProbability_StreptU";
    static const std::string MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_STREPTU_PARAM_KEY = "mastitisTreatmentPlan_Dry_protectDuration_StreptU";
    static const std::string MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_GN_PARAM_KEY = "mastitisTreatmentPlan_Dry_healingProbability_Gn";
    static const std::string MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_GN_PARAM_KEY = "mastitisTreatmentPlan_Dry_protectProbability_Gn";
    static const std::string MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_GN_PARAM_KEY = "mastitisTreatmentPlan_Dry_protectDuration_Gn";
    static const std::string MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_CNS_PARAM_KEY = "mastitisTreatmentPlan_Dry_healingProbability_CNS";
    static const std::string MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_CNS_PARAM_KEY = "mastitisTreatmentPlan_Dry_protectProbability_CNS";
    static const std::string MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_CNS_PARAM_KEY = "mastitisTreatmentPlan_Dry_protectDuration_CNS";
    static const std::string MASTITIS_TREATMENT_PLAN_DRY_HEALING_PROBABILITY_CB_PARAM_KEY = "mastitisTreatmentPlan_Dry_healingProbability_CB";
    static const std::string MASTITIS_TREATMENT_PLAN_DRY_PROTECT_PROBABILITY_CB_PARAM_KEY = "mastitisTreatmentPlan_Dry_protectProbability_CB";
    static const std::string MASTITIS_TREATMENT_PLAN_DRY_PROTECT_DURATION_CB_PARAM_KEY = "mastitisTreatmentPlan_Dry_protectDuration_CB";
} /* End of namespace Parameters */
}
#endif // Parameters_MastitisTreatmentPlanParameters_h
