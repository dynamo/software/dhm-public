#include "MatingPlanParameters.h"

// project
#include "../Tools/Tools.h"
#include "LanguageDictionary.h"
#include "FunctionalConstants.h"
#include "TechnicalConstants.h"
#include "HeadParameters.h"

BOOST_CLASS_EXPORT(ExchangeInfoStructures::Parameters::MatingPlanParameters) // Recommended (mandatory if virtual serialization)

namespace ExchangeInfoStructures
{
namespace Parameters
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_NVP(typeInseminationSemen); \
        ar & BOOST_SERIALIZATION_NVP(typeInseminationRatio); \
        ar & BOOST_SERIALIZATION_NVP(breedInseminationSemen); \
        ar & BOOST_SERIALIZATION_NVP(breedInseminationRatio); \
        ar & BOOST_SERIALIZATION_NVP(excludedHeiferCalvingPeriod); \
    
    IMPLEMENT_SERIALIZE_STD_METHODS(MatingPlanParameters);

    MatingPlanParameters::MatingPlanParameters()
    {

        typeInseminationSemen =
            {{FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination,
                    {{FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination, FunctionalConstants::Reproduction::DEFAULT_HEIFER_1_INSEMINATION_TYPE_SEMEN},
                    { FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination, FunctionalConstants::Reproduction::DEFAULT_COW_1_INSEMINATION_TYPE_SEMEN}}},
             {FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3,
                    {{FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination, FunctionalConstants::Reproduction::DEFAULT_HEIFER_2_3_INSEMINATION_TYPE_SEMEN},
                    { FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination, FunctionalConstants::Reproduction::DEFAULT_COW_2_3_INSEMINATION_TYPE_SEMEN}}},
             {FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver,
                    {{FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination, FunctionalConstants::Reproduction::DEFAULT_HEIFER_4_OVER_INSEMINATION_TYPE_SEMEN},
                    { FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination, FunctionalConstants::Reproduction::DEFAULT_COW_4_OVER_INSEMINATION_TYPE_SEMEN}}}};                               
        typeInseminationRatio =
            {{FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination,
                    {{FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination, FunctionalConstants::Reproduction::DEFAULT_HEIFER_1_INSEMINATION_TYPE_RATIO},
                    { FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination, FunctionalConstants::Reproduction::DEFAULT_COW_1_INSEMINATION_TYPE_RATIO}}},
             {FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3,
                    {{FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination, FunctionalConstants::Reproduction::DEFAULT_HEIFER_2_3_INSEMINATION_TYPE_RATIO},
                    { FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination, FunctionalConstants::Reproduction::DEFAULT_COW_2_3_INSEMINATION_TYPE_RATIO}}},
             {FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver,
                    {{FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination, FunctionalConstants::Reproduction::DEFAULT_HEIFER_4_OVER_INSEMINATION_TYPE_RATIO},
                    { FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination, FunctionalConstants::Reproduction::DEFAULT_COW_4_OVER_INSEMINATION_TYPE_RATIO}}}}; 
                                                    
        breedInseminationSemen =
            {{FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination,
                    {{FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination, FunctionalConstants::Reproduction::DEFAULT_HEIFER_1_INSEMINATION_BREED_SEMEN},
                    { FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination, FunctionalConstants::Reproduction::DEFAULT_COW_1_INSEMINATION_BREED_SEMEN}}},
             {FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3,
                    {{FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination, FunctionalConstants::Reproduction::DEFAULT_HEIFER_2_3_INSEMINATION_BREED_SEMEN},
                    { FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination, FunctionalConstants::Reproduction::DEFAULT_COW_2_3_INSEMINATION_BREED_SEMEN}}},
             {FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver,
                    {{FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination, FunctionalConstants::Reproduction::DEFAULT_HEIFER_4_OVER_INSEMINATION_BREED_SEMEN},
                    { FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination, FunctionalConstants::Reproduction::DEFAULT_COW_4_OVER_INSEMINATION_BREED_SEMEN}}}}; 
        breedInseminationRatio =
            {{FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination,
                    {{FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination, FunctionalConstants::Reproduction::DEFAULT_HEIFER_1_INSEMINATION_BREED_RATIO},
                    { FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination, FunctionalConstants::Reproduction::DEFAULT_COW_1_INSEMINATION_BREED_RATIO}}},
             {FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3,
                    {{FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination, FunctionalConstants::Reproduction::DEFAULT_HEIFER_2_3_INSEMINATION_BREED_RATIO},
                    { FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination, FunctionalConstants::Reproduction::DEFAULT_COW_2_3_INSEMINATION_BREED_RATIO}}},
             {FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver,
                    {{FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination, FunctionalConstants::Reproduction::DEFAULT_HEIFER_4_OVER_INSEMINATION_BREED_RATIO},
                    { FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination, FunctionalConstants::Reproduction::DEFAULT_COW_4_OVER_INSEMINATION_BREED_RATIO}}}}; 
    }
    
    MatingPlanParameters::~MatingPlanParameters()
    {
    }
    
    bool MatingPlanParameters::operator==(MatingPlanParameters const& other)
    {
        return  typeInseminationSemen == other.typeInseminationSemen and
                typeInseminationRatio == other.typeInseminationRatio and
                breedInseminationSemen == other.breedInseminationSemen and
                breedInseminationRatio == other.breedInseminationRatio and
                excludedHeiferCalvingPeriod == other.excludedHeiferCalvingPeriod;
    }

    // Constructeur de copie   
    MatingPlanParameters::MatingPlanParameters (const MatingPlanParameters &obj)
    {
        typeInseminationSemen = obj.typeInseminationSemen;
        typeInseminationRatio = obj.typeInseminationRatio;
        breedInseminationSemen = obj.breedInseminationSemen;
        breedInseminationRatio = obj.breedInseminationRatio;
        excludedHeiferCalvingPeriod = obj.excludedHeiferCalvingPeriod;
    }

    bool MatingPlanParameters::autoControl(std::string &message)
    {
        message = "";

        bool res = true;
        if (!res)
        {
            message = "Mating plan parameters not correct";
        }
        return res;
    } /* End of method AutoControl */    
        
    void MatingPlanParameters::setTypeInseminationSemen(FunctionalEnumerations::Reproduction::InseminationRank ir, FunctionalEnumerations::Reproduction::LactationRankForInsemination la, FunctionalEnumerations::Reproduction::TypeInseminationSemen tis, float ratio)
    {
        (typeInseminationSemen.find(ir)->second).find(la)->second = tis;
        (typeInseminationRatio.find(ir)->second).find(la)->second = ratio;
    }
    
    void MatingPlanParameters::getTypeInseminationSemen(FunctionalEnumerations::Reproduction::InseminationRank ir, FunctionalEnumerations::Reproduction::LactationRankForInsemination la, FunctionalEnumerations::Reproduction::TypeInseminationSemen &tis, float &ratio)
    {
        tis = (typeInseminationSemen.find(ir)->second).find(la)->second;     
        ratio = (typeInseminationRatio.find(ir)->second).find(la)->second;     
    }
    
    void MatingPlanParameters::setBreedInseminationSemen(FunctionalEnumerations::Reproduction::InseminationRank ir, FunctionalEnumerations::Reproduction::LactationRankForInsemination la, FunctionalEnumerations::Genetic::Breed b, float ratio)
    {
        (breedInseminationSemen.find(ir)->second).find(la)->second = b;       
        (breedInseminationRatio.find(ir)->second).find(la)->second = ratio;       
    }
    
    void MatingPlanParameters::getBreedInseminationSemen(FunctionalEnumerations::Reproduction::InseminationRank ir, FunctionalEnumerations::Reproduction::LactationRankForInsemination la, FunctionalEnumerations::Genetic::Breed &b, float &ratio)
    {
        b = (breedInseminationSemen.find(ir)->second).find(la)->second;       
        ratio = (breedInseminationRatio.find(ir)->second).find(la)->second;       
    }
    
    bool MatingPlanParameters::setImportedValue(const std::string &key, std::string &content)
    {
        if (key == MATING_PLAN_TYPE_INSEMINATION_SEMEN_FIRSTIA_HEIFER_PARAM_KEY) {typeInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination] = (FunctionalEnumerations::Reproduction::TypeInseminationSemen)std::atoi(content.c_str()); return true;}
        if (key == MATING_PLAN_TYPE_INSEMINATION_RATIO_FIRSTIA_HEIFER_PARAM_KEY) {typeInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination] = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MATING_PLAN_TYPE_INSEMINATION_SEMEN_FIRSTIA_ADULT_PARAM_KEY) {typeInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination] = (FunctionalEnumerations::Reproduction::TypeInseminationSemen)std::atoi(content.c_str()); return true;}
        if (key == MATING_PLAN_TYPE_INSEMINATION_RATIO_FIRSTIA_ADULT_PARAM_KEY) {typeInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination] = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MATING_PLAN_TYPE_INSEMINATION_SEMEN_IA2AND3_HEIFER_PARAM_KEY) {typeInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination] = (FunctionalEnumerations::Reproduction::TypeInseminationSemen)std::atoi(content.c_str()); return true;}
        if (key == MATING_PLAN_TYPE_INSEMINATION_RATIO_IA2AND3_HEIFER_PARAM_KEY) {typeInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination] = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MATING_PLAN_TYPE_INSEMINATION_SEMEN_IA2AND3_ADULT_PARAM_KEY) {typeInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination] = (FunctionalEnumerations::Reproduction::TypeInseminationSemen)std::atoi(content.c_str()); return true;}
        if (key == MATING_PLAN_TYPE_INSEMINATION_RATIO_IA2AND3_ADULT_PARAM_KEY) {typeInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination] = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MATING_PLAN_TYPE_INSEMINATION_SEMEN_IA4ANDOVER_HEIFER_PARAM_KEY) {typeInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination] = (FunctionalEnumerations::Reproduction::TypeInseminationSemen)std::atoi(content.c_str()); return true;}
        if (key == MATING_PLAN_TYPE_INSEMINATION_RATIO_IA4ANDOVER_HEIFER_PARAM_KEY) {typeInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination] = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MATING_PLAN_TYPE_INSEMINATION_SEMEN_IA4ANDOVER_ADULT_PARAM_KEY) {typeInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination] = (FunctionalEnumerations::Reproduction::TypeInseminationSemen)std::atoi(content.c_str()); return true;}
        if (key == MATING_PLAN_TYPE_INSEMINATION_RATIO_IA4ANDOVER_ADULT_PARAM_KEY) {typeInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination] = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MATING_PLAN_BREED_INSEMINATION_SEMEN_FIRSTIA_HEIFER_PARAM_KEY) {breedInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination] = (FunctionalEnumerations::Genetic::Breed)std::atoi(content.c_str()); return true;}
        if (key == MATING_PLAN_BREED_INSEMINATION_RATIO_FIRSTIA_HEIFER_PARAM_KEY) {breedInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination] = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MATING_PLAN_BREED_INSEMINATION_SEMEN_FIRSTIA_ADULT_PARAM_KEY) {breedInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination] = (FunctionalEnumerations::Genetic::Breed)std::atoi(content.c_str()); return true;}
        if (key == MATING_PLAN_BREED_INSEMINATION_RATIO_FIRSTIA_ADULT_PARAM_KEY) {breedInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination] = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MATING_PLAN_BREED_INSEMINATION_SEMEN_IA2AND3_HEIFER_PARAM_KEY) {breedInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination] = (FunctionalEnumerations::Genetic::Breed)std::atoi(content.c_str()); return true;}
        if (key == MATING_PLAN_BREED_INSEMINATION_RATIO_IA2AND3_HEIFER_PARAM_KEY) {breedInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination] = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MATING_PLAN_BREED_INSEMINATION_SEMEN_IA2AND3_ADULT_PARAM_KEY) {breedInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination] = (FunctionalEnumerations::Genetic::Breed)std::atoi(content.c_str()); return true;}
        if (key == MATING_PLAN_BREED_INSEMINATION_RATIO_IA2AND3_ADULT_PARAM_KEY) {breedInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination] = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MATING_PLAN_BREED_INSEMINATION_SEMEN_IA4ANDOVER_HEIFER_PARAM_KEY) {breedInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination] = (FunctionalEnumerations::Genetic::Breed)std::atoi(content.c_str()); return true;}
        if (key == MATING_PLAN_BREED_INSEMINATION_RATIO_IA4ANDOVER_HEIFER_PARAM_KEY) {breedInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination] = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MATING_PLAN_BREED_INSEMINATION_SEMEN_IA4ANDOVER_ADULT_PARAM_KEY) {breedInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination] = (FunctionalEnumerations::Genetic::Breed)std::atoi(content.c_str()); return true;}
        if (key == MATING_PLAN_BREED_INSEMINATION_RATIO_IA4ANDOVER_ADULT_PARAM_KEY) {breedInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination] = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == MATING_PLAN_FIRST_EXCLUDED_MONTH_FOR_HEIFER_CALVING_PARAM_KEY) {excludedHeiferCalvingPeriod.firstMonthForCalvingExculsion = (FunctionalEnumerations::Global::Month)std::atoi(content.c_str()); return true;}
        if (key == MATING_PLAN_LAST_EXCLUDED_MONTH_FOR_HEIFER_CALVING_PARAM_KEY) {excludedHeiferCalvingPeriod.lastMonthForCalvingExculsion = (FunctionalEnumerations::Global::Month)std::atoi(content.c_str()); return true;}

        return false; // Not here
    }

    bool MatingPlanParameters::getValueToExport(const std::string &key, std::string &content)
    {
        GET_INTEGER_VALUE_TO_IMPORT(MATING_PLAN_TYPE_INSEMINATION_SEMEN_FIRSTIA_HEIFER_PARAM_KEY, typeInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination])
        GET_FLOAT_VALUE_TO_IMPORT(MATING_PLAN_TYPE_INSEMINATION_RATIO_FIRSTIA_HEIFER_PARAM_KEY, typeInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination])
        GET_INTEGER_VALUE_TO_IMPORT(MATING_PLAN_TYPE_INSEMINATION_SEMEN_FIRSTIA_ADULT_PARAM_KEY, typeInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination])
        GET_FLOAT_VALUE_TO_IMPORT(MATING_PLAN_TYPE_INSEMINATION_RATIO_FIRSTIA_ADULT_PARAM_KEY, typeInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination])
        GET_INTEGER_VALUE_TO_IMPORT(MATING_PLAN_TYPE_INSEMINATION_SEMEN_IA2AND3_HEIFER_PARAM_KEY, typeInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination])
        GET_FLOAT_VALUE_TO_IMPORT(MATING_PLAN_TYPE_INSEMINATION_RATIO_IA2AND3_HEIFER_PARAM_KEY, typeInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination])
        GET_INTEGER_VALUE_TO_IMPORT(MATING_PLAN_TYPE_INSEMINATION_SEMEN_IA2AND3_ADULT_PARAM_KEY, typeInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination])
        GET_FLOAT_VALUE_TO_IMPORT(MATING_PLAN_TYPE_INSEMINATION_RATIO_IA2AND3_ADULT_PARAM_KEY, typeInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination])
        GET_INTEGER_VALUE_TO_IMPORT(MATING_PLAN_TYPE_INSEMINATION_SEMEN_IA4ANDOVER_HEIFER_PARAM_KEY, typeInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination])
        GET_FLOAT_VALUE_TO_IMPORT(MATING_PLAN_TYPE_INSEMINATION_RATIO_IA4ANDOVER_HEIFER_PARAM_KEY, typeInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination])
        GET_INTEGER_VALUE_TO_IMPORT(MATING_PLAN_TYPE_INSEMINATION_SEMEN_IA4ANDOVER_ADULT_PARAM_KEY, typeInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination])
        GET_FLOAT_VALUE_TO_IMPORT(MATING_PLAN_TYPE_INSEMINATION_RATIO_IA4ANDOVER_ADULT_PARAM_KEY, typeInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination])
        GET_INTEGER_VALUE_TO_IMPORT(MATING_PLAN_BREED_INSEMINATION_SEMEN_FIRSTIA_HEIFER_PARAM_KEY, breedInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination])
        GET_FLOAT_VALUE_TO_IMPORT(MATING_PLAN_BREED_INSEMINATION_RATIO_FIRSTIA_HEIFER_PARAM_KEY, breedInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination])
        GET_INTEGER_VALUE_TO_IMPORT(MATING_PLAN_BREED_INSEMINATION_SEMEN_FIRSTIA_ADULT_PARAM_KEY, breedInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination])
        GET_FLOAT_VALUE_TO_IMPORT(MATING_PLAN_BREED_INSEMINATION_RATIO_FIRSTIA_ADULT_PARAM_KEY, breedInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::firstInsemination)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination])
        GET_INTEGER_VALUE_TO_IMPORT(MATING_PLAN_BREED_INSEMINATION_SEMEN_IA2AND3_HEIFER_PARAM_KEY, breedInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination])
        GET_FLOAT_VALUE_TO_IMPORT(MATING_PLAN_BREED_INSEMINATION_RATIO_IA2AND3_HEIFER_PARAM_KEY, breedInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination])
        GET_INTEGER_VALUE_TO_IMPORT(MATING_PLAN_BREED_INSEMINATION_SEMEN_IA2AND3_ADULT_PARAM_KEY, breedInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination])
        GET_FLOAT_VALUE_TO_IMPORT(MATING_PLAN_BREED_INSEMINATION_RATIO_IA2AND3_ADULT_PARAM_KEY, breedInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations2And3)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination])
        GET_INTEGER_VALUE_TO_IMPORT(MATING_PLAN_BREED_INSEMINATION_SEMEN_IA4ANDOVER_HEIFER_PARAM_KEY, breedInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination])
        GET_FLOAT_VALUE_TO_IMPORT(MATING_PLAN_BREED_INSEMINATION_RATIO_IA4ANDOVER_HEIFER_PARAM_KEY, breedInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::heiferForInsemination])
        GET_INTEGER_VALUE_TO_IMPORT(MATING_PLAN_BREED_INSEMINATION_SEMEN_IA4ANDOVER_ADULT_PARAM_KEY, breedInseminationSemen.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination])
        GET_FLOAT_VALUE_TO_IMPORT(MATING_PLAN_BREED_INSEMINATION_RATIO_IA4ANDOVER_ADULT_PARAM_KEY, breedInseminationRatio.find(FunctionalEnumerations::Reproduction::InseminationRank::inseminations4AndOver)->second[FunctionalEnumerations::Reproduction::LactationRankForInsemination::adultForInsemination])
        GET_INTEGER_VALUE_TO_IMPORT(MATING_PLAN_FIRST_EXCLUDED_MONTH_FOR_HEIFER_CALVING_PARAM_KEY, excludedHeiferCalvingPeriod.firstMonthForCalvingExculsion)
        GET_INTEGER_VALUE_TO_IMPORT(MATING_PLAN_LAST_EXCLUDED_MONTH_FOR_HEIFER_CALVING_PARAM_KEY, excludedHeiferCalvingPeriod.lastMonthForCalvingExculsion)

        return false; // Not here
    }    
    
#ifdef _LOG
    void MatingPlanParameters::getDefaultValues(std::vector<std::string> &vLines)
    {
        std::string line = "";        
        line = MATING_PLAN_TYPE_INSEMINATION_SEMEN_FIRSTIA_HEIFER_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_HEIFER_1_INSEMINATION_TYPE_SEMEN); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_TYPE_INSEMINATION_RATIO_FIRSTIA_HEIFER_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_HEIFER_1_INSEMINATION_TYPE_RATIO); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_TYPE_INSEMINATION_SEMEN_FIRSTIA_ADULT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_COW_1_INSEMINATION_TYPE_SEMEN); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_TYPE_INSEMINATION_RATIO_FIRSTIA_ADULT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_COW_1_INSEMINATION_TYPE_RATIO); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_TYPE_INSEMINATION_SEMEN_IA2AND3_HEIFER_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_HEIFER_2_3_INSEMINATION_TYPE_SEMEN); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_TYPE_INSEMINATION_RATIO_IA2AND3_HEIFER_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_HEIFER_2_3_INSEMINATION_TYPE_RATIO); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_TYPE_INSEMINATION_SEMEN_IA2AND3_ADULT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_COW_2_3_INSEMINATION_TYPE_SEMEN); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_TYPE_INSEMINATION_RATIO_IA2AND3_ADULT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_COW_2_3_INSEMINATION_TYPE_RATIO); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_TYPE_INSEMINATION_SEMEN_IA4ANDOVER_HEIFER_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_HEIFER_4_OVER_INSEMINATION_TYPE_SEMEN); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_TYPE_INSEMINATION_RATIO_IA4ANDOVER_HEIFER_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_HEIFER_4_OVER_INSEMINATION_TYPE_RATIO); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_TYPE_INSEMINATION_SEMEN_IA4ANDOVER_ADULT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_COW_4_OVER_INSEMINATION_TYPE_SEMEN); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_TYPE_INSEMINATION_RATIO_IA4ANDOVER_ADULT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_COW_4_OVER_INSEMINATION_TYPE_RATIO); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_BREED_INSEMINATION_SEMEN_FIRSTIA_HEIFER_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_HEIFER_1_INSEMINATION_BREED_SEMEN); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_BREED_INSEMINATION_RATIO_FIRSTIA_HEIFER_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_HEIFER_1_INSEMINATION_BREED_RATIO); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_BREED_INSEMINATION_SEMEN_FIRSTIA_ADULT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_COW_1_INSEMINATION_BREED_SEMEN); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_BREED_INSEMINATION_RATIO_FIRSTIA_ADULT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_HEIFER_1_INSEMINATION_BREED_RATIO); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_BREED_INSEMINATION_SEMEN_IA2AND3_HEIFER_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_HEIFER_2_3_INSEMINATION_BREED_SEMEN); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_BREED_INSEMINATION_RATIO_IA2AND3_HEIFER_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_HEIFER_1_INSEMINATION_BREED_RATIO); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_BREED_INSEMINATION_SEMEN_IA2AND3_ADULT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_COW_2_3_INSEMINATION_BREED_SEMEN); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_BREED_INSEMINATION_RATIO_IA2AND3_ADULT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_HEIFER_1_INSEMINATION_BREED_RATIO); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_BREED_INSEMINATION_SEMEN_IA4ANDOVER_HEIFER_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_HEIFER_4_OVER_INSEMINATION_BREED_SEMEN); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_BREED_INSEMINATION_RATIO_IA4ANDOVER_HEIFER_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_HEIFER_1_INSEMINATION_BREED_RATIO); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_BREED_INSEMINATION_SEMEN_IA4ANDOVER_ADULT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_COW_4_OVER_INSEMINATION_BREED_SEMEN); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_BREED_INSEMINATION_RATIO_IA4ANDOVER_ADULT_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_HEIFER_1_INSEMINATION_BREED_RATIO); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_FIRST_EXCLUDED_MONTH_FOR_HEIFER_CALVING_PARAM_KEY + TechnicalConstants::EXPORT_SEP + "0"; vLines.push_back(Tools::changeChar(line,'.', ','));
        line = MATING_PLAN_LAST_EXCLUDED_MONTH_FOR_HEIFER_CALVING_PARAM_KEY + TechnicalConstants::EXPORT_SEP + "0"; vLines.push_back(Tools::changeChar(line,'.', ','));
    }
#endif // _LOG

} /* End of namespace Parameters */
}