/////////////////////////////////////////////
//                                         //
// File : MatingPlanParameters.h           //
//                                         //
// Author : Philippe Gontier               //
//                                         //
// Date : 01/10/2015                       //
//                                         //
// Insemination type and breed assignement //
//                                         //
/////////////////////////////////////////////

#ifndef Parameters_MatingPlanParameters_h
#define Parameters_MatingPlanParameters_h

// Standard
#include <map>

// Project
#include "../Tools/Serialization.h"
#include "FunctionalEnumerations.h"
#include "ExcludedCalvingPeriodParameters.h"
#include "MatingPlanParametersConstants.h"

namespace ExchangeInfoStructures
{
namespace Parameters
{
    /// This structure make it possible to define the insemination type and the breed according to insemination rank and parity of the animals. 
    /// See the § 3.2.1.1 "Mating plan" of the "Functional description and terms of use" documentation.
    struct MatingPlanParameters
    {                
        /// Semen type assigned according to insemination rank and parity (see § 2.2.1.2.4 of the "Functional description and terms of use" documentation)
        std::map<FunctionalEnumerations::Reproduction::InseminationRank, std::map<FunctionalEnumerations::Reproduction::LactationRankForInsemination, FunctionalEnumerations::Reproduction::TypeInseminationSemen>> typeInseminationSemen; 
                                                    
        /// Ratio assigned to the semen type according to insemination rank and parity (see § 2.2.1.2.4 of the "Functional description and terms of use" documentation)
        std::map<FunctionalEnumerations::Reproduction::InseminationRank, std::map<FunctionalEnumerations::Reproduction::LactationRankForInsemination, float>> typeInseminationRatio; 
                                                    
        /// Breed of the insemination assigned according to insemination rank and parity (see § 2.2.1.2.4 of the "Functional description and terms of use" documentation)
        std::map<FunctionalEnumerations::Reproduction::InseminationRank, std::map<FunctionalEnumerations::Reproduction::LactationRankForInsemination, FunctionalEnumerations::Genetic::Breed>> breedInseminationSemen; 

        /// Ratio assigned for the inseminated breed according to insemination rank and parity (see § 2.2.1.2.4 of the "Functional description and terms of use" documentation)
        std::map<FunctionalEnumerations::Reproduction::InseminationRank, std::map<FunctionalEnumerations::Reproduction::LactationRankForInsemination, float>> breedInseminationRatio; 

        /// Grouping calving parameter (see § 2.2.1.2.5 of the "Functional description and terms of use" documentation)
        ExcludedCalvingPeriodParameters excludedHeiferCalvingPeriod;

    private:
        DECLARE_SERIALIZE_STD_METHODS;
    public:
        
        /// Value checking, errors in the std::string parameter reference
        bool autoControl (
                                    /// Reference std::string to put the error message (empty value if no error)
                                    std::string &message
                                    );
        MatingPlanParameters();
        virtual ~MatingPlanParameters();
        bool operator==(MatingPlanParameters const& other);
        MatingPlanParameters (const MatingPlanParameters &obj);        //constructeur de copie 
        
        /// Set the type of insemination for a given insemination rank and parity (see § 2.2.1.2.4 of the "Functional description and terms of use" documentation)
        void setTypeInseminationSemen(FunctionalEnumerations::Reproduction::InseminationRank ir, FunctionalEnumerations::Reproduction::LactationRankForInsemination la, FunctionalEnumerations::Reproduction::TypeInseminationSemen tis, float ratio);
        
        /// Get the type of insemination regarding a given insemination rank and parity (see § 2.2.1.2.4 of the "Functional description and terms of use" documentation)
        void getTypeInseminationSemen(FunctionalEnumerations::Reproduction::InseminationRank ir, FunctionalEnumerations::Reproduction::LactationRankForInsemination la, FunctionalEnumerations::Reproduction::TypeInseminationSemen &tis, float &ratio);
        
        /// Set the breed of insemination for a given insemination rank and parity (see § 2.2.1.2.4 of the "Functional description and terms of use" documentation)
        void setBreedInseminationSemen(FunctionalEnumerations::Reproduction::InseminationRank ir, FunctionalEnumerations::Reproduction::LactationRankForInsemination la, FunctionalEnumerations::Genetic::Breed b, float ratio);
        
        /// Get the breed of insemination regarding a given insemination rank and parity (see § 2.2.1.2.4 of the "Functional description and terms of use" documentation)
        void getBreedInseminationSemen(FunctionalEnumerations::Reproduction::InseminationRank ir, FunctionalEnumerations::Reproduction::LactationRankForInsemination la, FunctionalEnumerations::Genetic::Breed &b, float &ratio);
        
#ifdef _LOG
        static void getDefaultValues(std::vector<std::string> &vLines);
#endif // _LOG
        bool setImportedValue(const std::string &key, std::string &content);
        bool getValueToExport(const std::string &key, std::string &content);

    }; /* End of class MatingPlanParameters */
        
} /* End of namespace Parameters */
}
#endif // Parameters_MatingPlanParameters_h
