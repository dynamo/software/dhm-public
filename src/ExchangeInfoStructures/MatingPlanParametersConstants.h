/////////////////////////////////////////////
//                                         //
// File : MatingPlanParametersConstant.h   //
//                                         //
// Author : Philippe Gontier               //
//                                         //
// Date : 01/10/2015                       //
//                                         //
// Insemination type and breed assignement //
//                                         //
/////////////////////////////////////////////

#ifndef Parameters_MatingPlanParameters_constants_h
#define Parameters_MatingPlanParameters_constants_h

// Standard
#include <string>

// Project

namespace ExchangeInfoStructures
{
namespace Parameters
{
       
    // Import/export key values
    static const std::string MATING_PLAN_TYPE_INSEMINATION_SEMEN_FIRSTIA_HEIFER_PARAM_KEY = "matingPlan_TypeInseminationSemen_firstIA_heifer";
    static const std::string MATING_PLAN_TYPE_INSEMINATION_RATIO_FIRSTIA_HEIFER_PARAM_KEY = "matingPlan_TypeInseminationRatio_firstIA_heifer";
    static const std::string MATING_PLAN_TYPE_INSEMINATION_SEMEN_FIRSTIA_ADULT_PARAM_KEY = "matingPlan_TypeInseminationSemen_firstIA_adult";
    static const std::string MATING_PLAN_TYPE_INSEMINATION_RATIO_FIRSTIA_ADULT_PARAM_KEY = "matingPlan_TypeInseminationRatio_firstIA_adult";
    static const std::string MATING_PLAN_TYPE_INSEMINATION_SEMEN_IA2AND3_HEIFER_PARAM_KEY = "matingPlan_TypeInseminationSemen_IA2and3_heifer";
    static const std::string MATING_PLAN_TYPE_INSEMINATION_RATIO_IA2AND3_HEIFER_PARAM_KEY = "matingPlan_TypeInseminationRatio_IA2and3_heifer";
    static const std::string MATING_PLAN_TYPE_INSEMINATION_SEMEN_IA2AND3_ADULT_PARAM_KEY = "matingPlan_TypeInseminationSemen_IA2and3_adult";
    static const std::string MATING_PLAN_TYPE_INSEMINATION_RATIO_IA2AND3_ADULT_PARAM_KEY = "matingPlan_TypeInseminationRatio_IA2and3_adult";
    static const std::string MATING_PLAN_TYPE_INSEMINATION_SEMEN_IA4ANDOVER_HEIFER_PARAM_KEY = "matingPlan_TypeInseminationSemen_IA4andOver_heifer";
    static const std::string MATING_PLAN_TYPE_INSEMINATION_RATIO_IA4ANDOVER_HEIFER_PARAM_KEY = "matingPlan_TypeInseminationRatio_IA4andOver_heifer";
    static const std::string MATING_PLAN_TYPE_INSEMINATION_SEMEN_IA4ANDOVER_ADULT_PARAM_KEY = "matingPlan_TypeInseminationSemen_IA4andOver_adult";
    static const std::string MATING_PLAN_TYPE_INSEMINATION_RATIO_IA4ANDOVER_ADULT_PARAM_KEY = "matingPlan_TypeInseminationRatio_IA4andOver_adult";
    static const std::string MATING_PLAN_BREED_INSEMINATION_SEMEN_FIRSTIA_HEIFER_PARAM_KEY = "matingPlan_BreedInseminationSemen_firstIA_heifer";
    static const std::string MATING_PLAN_BREED_INSEMINATION_RATIO_FIRSTIA_HEIFER_PARAM_KEY = "matingPlan_BreedInseminationRatio_firstIA_heifer";
    static const std::string MATING_PLAN_BREED_INSEMINATION_SEMEN_FIRSTIA_ADULT_PARAM_KEY = "matingPlan_BreedInseminationSemen_firstIA_adult";
    static const std::string MATING_PLAN_BREED_INSEMINATION_RATIO_FIRSTIA_ADULT_PARAM_KEY = "matingPlan_BreedInseminationRatio_firstIA_adult";
    static const std::string MATING_PLAN_BREED_INSEMINATION_SEMEN_IA2AND3_HEIFER_PARAM_KEY = "matingPlan_BreedInseminationSemen_IA2and3_heifer";
    static const std::string MATING_PLAN_BREED_INSEMINATION_RATIO_IA2AND3_HEIFER_PARAM_KEY = "matingPlan_BreedInseminationRatio_IA2and3_heifer";
    static const std::string MATING_PLAN_BREED_INSEMINATION_SEMEN_IA2AND3_ADULT_PARAM_KEY = "matingPlan_BreedInseminationSemen_IA2and3_adult";
    static const std::string MATING_PLAN_BREED_INSEMINATION_RATIO_IA2AND3_ADULT_PARAM_KEY = "matingPlan_BreedInseminationRatio_IA2and3_adult";
    static const std::string MATING_PLAN_BREED_INSEMINATION_SEMEN_IA4ANDOVER_HEIFER_PARAM_KEY = "matingPlan_BreedInseminationSemen_IA4andOver_heifer";
    static const std::string MATING_PLAN_BREED_INSEMINATION_RATIO_IA4ANDOVER_HEIFER_PARAM_KEY = "matingPlan_BreedInseminationRatio_IA4andOver_heifer";
    static const std::string MATING_PLAN_BREED_INSEMINATION_SEMEN_IA4ANDOVER_ADULT_PARAM_KEY = "matingPlan_BreedInseminationSemen_IA4andOver_adult";   
    static const std::string MATING_PLAN_BREED_INSEMINATION_RATIO_IA4ANDOVER_ADULT_PARAM_KEY = "matingPlan_BreedInseminationRatio_IA4andOver_adult";   
    static const std::string MATING_PLAN_FIRST_EXCLUDED_MONTH_FOR_HEIFER_CALVING_PARAM_KEY = "matingPlan_FirstExcludedMonthForHeiferCalving";
    static const std::string MATING_PLAN_LAST_EXCLUDED_MONTH_FOR_HEIFER_CALVING_PARAM_KEY = "matingPlan_LastExcludedMonthForHeiferCalving";
     
} /* End of namespace Parameters */
}
#endif // Parameters_MatingPlanParameters_constants_h
