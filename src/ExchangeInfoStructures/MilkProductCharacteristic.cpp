#include "MilkProductCharacteristic.h"

// Project
#include "../Tools/Tools.h"

BOOST_CLASS_EXPORT(ExchangeInfoStructures::MilkProductCharacteristic) // Recommended (mandatory if virtual serialization)

namespace ExchangeInfoStructures
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(quantity); \
    ar & BOOST_SERIALIZATION_NVP(TB); \
    ar & BOOST_SERIALIZATION_NVP(TP); \
    ar & BOOST_SERIALIZATION_NVP(SCC);
    
    IMPLEMENT_SERIALIZE_STD_METHODS(MilkProductCharacteristic);

    MilkProductCharacteristic::MilkProductCharacteristic()
    {
        init();
    }
        
    MilkProductCharacteristic::MilkProductCharacteristic(float theQuantity, float theTB, float theTP, float theSCC)
    {
        quantity = theQuantity;
        TB = theTB;
        TP = theTP;
        SCC = theSCC;
    }

    MilkProductCharacteristic::MilkProductCharacteristic(MilkProductCharacteristic const& other)
    {
        quantity = other.quantity;
        TB = other.TB;
        TP = other.TP;
        SCC = other.SCC;
    }
             
    void MilkProductCharacteristic::init()
    {
        quantity = 0.0f;
        TB = 0.0f;
        TP = 0.0f;
        SCC = 0.0f;
    }

    MilkProductCharacteristic MilkProductCharacteristic::operator / (float const& div)
    {
        MilkProductCharacteristic res(*this);
        res /= div;
        return res;
    }

    MilkProductCharacteristic MilkProductCharacteristic::operator * (float const& prod)
    {
        MilkProductCharacteristic res(*this);
        res *= prod;
        return res;
    }
    
    MilkProductCharacteristic MilkProductCharacteristic::operator * (MilkProductCharacteristic const& other)
    {
        MilkProductCharacteristic res(*this);
        res *= other;
        return res;
    }
    
    MilkProductCharacteristic MilkProductCharacteristic::operator + (MilkProductCharacteristic const& other)
    {
        MilkProductCharacteristic res(*this);
        res += other;
        return res;
    }

    void MilkProductCharacteristic::operator /= (float const& div)
    {
        quantity = quantity / div;
    }

    void MilkProductCharacteristic::operator *= (float const& fact)
    {
        quantity = quantity * fact;
    }

    void MilkProductCharacteristic::operator *= (MilkProductCharacteristic const& other)
    {
        quantity *= other.quantity;
        TB *= other.TB;
        TP *= other.TP;
        SCC *= other.SCC;
    }

    void MilkProductCharacteristic::operator += (MilkProductCharacteristic const& other)
    {
        if (quantity > 0)
        {
            float quantitySum = quantity + other.quantity;
            float pcOriginQuantity = quantity/quantitySum;
            float pcOtherQuantity = other.quantity/quantitySum;
            quantity = quantitySum;
            TB = TB * pcOriginQuantity + other.TB * pcOtherQuantity;
            TP = TP * pcOriginQuantity + other.TP * pcOtherQuantity;
            SCC = SCC * pcOriginQuantity + other.SCC * pcOtherQuantity;
        }
        else
        {
            quantity = other.quantity;
            TB = other.TB;
            TP = other.TP;
            SCC = other.SCC;
        }
    }

    void MilkProductCharacteristic::operator += (float const& qty)
    {
        quantity += qty;
    }

    void MilkProductCharacteristic::operator -= (float const& qty)
    {
        quantity -= qty;
    }
    
    bool MilkProductCharacteristic::operator==(MilkProductCharacteristic const& other)
    {
        return  quantity == other.quantity and
                TB == other.TB and
                TP == other.TP and
                SCC == other.SCC;
    }
            
    bool MilkProductCharacteristic::operator!=(MilkProductCharacteristic const& other)
    {
        return not (*this == other);
    }

}