#ifndef MilkProductCharacteristic_h
#define MilkProductCharacteristic_h

// standard
#include <string>

// Project
#include "../Tools/Serialization.h"

namespace ExchangeInfoStructures
{
    struct MilkProductCharacteristic
    {
    private:
        // serialization
        DECLARE_SERIALIZE_STD_METHODS;

    public:
        
        float quantity = 0.0f;
        float TB = 0.0f;
        float TP = 0.0f;
        float SCC = 0.0f;
        
        MilkProductCharacteristic();
        MilkProductCharacteristic(float theQuantity, float theTB, float theTP, float theSCC);
        virtual ~MilkProductCharacteristic(){};
        MilkProductCharacteristic(MilkProductCharacteristic const& other);
        MilkProductCharacteristic operator + (MilkProductCharacteristic const& other);
        MilkProductCharacteristic operator * (MilkProductCharacteristic const& other);
        void operator += (MilkProductCharacteristic const& other);
        void operator *= (MilkProductCharacteristic const& other);

        MilkProductCharacteristic operator / (float const& div);
        MilkProductCharacteristic operator * (float const& prod);
        void operator += (float const& qty);
        void operator -= (float const& qty);
        void operator *= (float const& fact);
        void operator /= (float const& div);
        bool operator==(MilkProductCharacteristic const& other);
        bool operator!=(MilkProductCharacteristic const& other);

        void init(); 
    };
} /* End of namespace Lactation */
#endif // MilkProductCharacteristic_h
