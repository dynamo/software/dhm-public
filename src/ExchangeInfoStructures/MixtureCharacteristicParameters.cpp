#include "MixtureCharacteristicParameters.h"

// Project
#include "../Tools/Tools.h"
#include "LanguageDictionary.h"

BOOST_CLASS_EXPORT(ExchangeInfoStructures::Parameters::MixtureCharacteristicParameters) // Recommended (mandatory if virtual serialization)
BOOST_CLASS_EXPORT(ExchangeInfoStructures::Parameters::ForageMixtureCharacteristicParameters) // Recommended (mandatory if virtual serialization)
BOOST_CLASS_EXPORT(ExchangeInfoStructures::Parameters::ConcentrateMixtureCharacteristicParameters) // Recommended (mandatory if virtual serialization)

namespace ExchangeInfoStructures
{
namespace Parameters
{
    // Macro to code statement for serialization methods 
#define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(quantity); \
    ar & BOOST_SERIALIZATION_NVP(typeRatios);

    IMPLEMENT_SERIALIZE_STD_METHODS(MixtureCharacteristicParameters);

#undef STATEMENTS_SERIALIZE_STD_METHODS
#define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(MixtureCharacteristicParameters);

    IMPLEMENT_SERIALIZE_STD_METHODS(ForageMixtureCharacteristicParameters);
    IMPLEMENT_SERIALIZE_STD_METHODS(ConcentrateMixtureCharacteristicParameters);

    MixtureCharacteristicParameters::MixtureCharacteristicParameters()
    {
    }

    MixtureCharacteristicParameters::MixtureCharacteristicParameters(MixtureCharacteristicParameters const& other)
    {
        quantity = other.quantity;
        typeRatios = other.typeRatios;
    }

    void MixtureCharacteristicParameters::init(unsigned int typeNumber)
    {
        quantity = 0.0f;
        for (unsigned int i = 0; i < typeNumber; i++)
        {
            setRatio(i, 0.0f);
        }
    }

#if defined _LOG || defined _FULL_RESULTS
    std::string MixtureCharacteristicParameters::getStrForageType(FunctionalEnumerations::Feeding::ForageType ft)
    {
        return ft == FunctionalEnumerations::Feeding::ForageType::maizePlant ? "Maize_silage" :
               ft == FunctionalEnumerations::Feeding::ForageType::grass ? "Grass" :
               ft == FunctionalEnumerations::Feeding::ForageType::hay ? "Hay" :
               ft == FunctionalEnumerations::Feeding::ForageType::grassSilage ? "Grass_silage" :
               "Unwnown Forage Type"; // hope it never appears;
    }

    std::string MixtureCharacteristicParameters::getStrConcentrateType(FunctionalEnumerations::Feeding::ConcentrateType ct)
    {
        return ct == FunctionalEnumerations::Feeding::ConcentrateType::rapeseed ? "Rapeseed_meal" :
               ct == FunctionalEnumerations::Feeding::ConcentrateType::soja ? "Soja_meal" :
               ct == FunctionalEnumerations::Feeding::ConcentrateType::barley ? "Barley" :
               "Unwnown Concentrate Type"; // hope it never appears;
    }
#endif // _LOG || _FULL_RESULTS           

    bool MixtureCharacteristicParameters::autoControl(std::string &message)
    {
        bool res = quantity >= 0.0f;
        if (res)
        {
            float sum = 0.0f;
            for (std::map<unsigned int, float>::iterator it = typeRatios.begin();  it != typeRatios.end() and res; it++)
            {
                if (it->second < 0.0f)
                {
                    res = false;
                    message = LanguageDictionary::getMessage(KEY_A_TYPE_RATIO_IS_NEGATIVE);
                }
                else
                {
                   sum += it->second;
                }
            }
            if (res and sum != 0.0f and sum != 1.0f)
            {
                res = false;
                message = LanguageDictionary::getMessage(KEY_THE_TYPE_RATIO_SUM_NOT_EQUAL_TO_0_OR_1);
            }
        }
        else
        {
            message = LanguageDictionary::getMessage(KEY_THE_QUANTITY_IS_NEGATIVE);
        }
        return res;
    }

    bool MixtureCharacteristicParameters::operator==(MixtureCharacteristicParameters const& other)
    {
        return (quantity == other.quantity && 
                typeRatios == other.typeRatios);
    }

    MixtureCharacteristicParameters MixtureCharacteristicParameters::operator / (float const& div)
    {
        ForageMixtureCharacteristicParameters res; 
        res.quantity = quantity / div;
        return res;
    }

    MixtureCharacteristicParameters MixtureCharacteristicParameters::operator * (float const& prod)
    {
        ForageMixtureCharacteristicParameters res; 
        res.quantity = quantity * prod;
        return res;
    }

    void MixtureCharacteristicParameters::operator /= (float const& div)
    {
        quantity = quantity / div;
    }

    void MixtureCharacteristicParameters::operator *= (float const& fact)
    {
        quantity = quantity * fact;
    }

    void MixtureCharacteristicParameters::operator += (MixtureCharacteristicParameters &other)
    {
        if (quantity == 0.0f)
        {
            quantity = other.quantity;
            typeRatios = other.typeRatios;
        }
        else if (other.quantity != 0.0f)
        {
            float quantitySum = quantity + other.quantity;
            if (quantitySum > 0.0f)
            {
                float pcOriginQuantity = quantity/quantitySum;
                float pcOtherQuantity = other.quantity/quantitySum;
                quantity = quantitySum;
                for (unsigned int i = 0; i < typeRatios.size(); i++)
                {
                    float newRatio = getRatio(i) * pcOriginQuantity + other.getRatio(i) * pcOtherQuantity;
                    setRatio((FunctionalEnumerations::Feeding::ForageType)i, newRatio);
                }
            }
        }
    }

    void MixtureCharacteristicParameters::operator += (float const& operatorQuantity)
    {
        quantity += operatorQuantity;
    }

    void MixtureCharacteristicParameters::operator -= (MixtureCharacteristicParameters const& other)
    {
        MixtureCharacteristicParameters tmp(other);
        tmp.quantity = -tmp.quantity;
        *this += tmp;
    }

    void MixtureCharacteristicParameters::operator -= (float const& operatorQuantity)
    {
        quantity -= operatorQuantity;
    }

    float MixtureCharacteristicParameters::getQuantity(unsigned int typeRatio)
    {
        return quantity == 0.0f ? 0.0f : getRatio(typeRatio) * quantity;

    }

    void MixtureCharacteristicParameters::setRatio(unsigned int typeRatio, float r)
    {
        typeRatios[typeRatio] = r;            
    }

    float MixtureCharacteristicParameters::getRatio(unsigned int typeRatio)
    {
        return typeRatios.find(typeRatio)->second;            
    }
}
}