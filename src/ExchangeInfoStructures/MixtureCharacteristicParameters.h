//////////////////////////////////////////////////
//                                              //
// File : MixtureCharacteristicParameters.h     //
//                                              //
// Author : Philippe Gontier                    //
//                                              //
// Date : 01/10/2015                            //
//                                              //
// Mixture characteristic settings, Including : //
// - ForageMixtureCharacteristicParameters      //
// - ConcentrateMixtureCharacteristicParameters //
//                                              //
//////////////////////////////////////////////////

#ifndef Parameters_MixtureCharacteristicParameters_h
#define Parameters_MixtureCharacteristicParameters_h

// standard

// Project
#include "../Tools/Serialization.h"
#include "FunctionalEnumerations.h"

namespace ExchangeInfoStructures
{
namespace Parameters
{
    /// This structure is the mixture characteristic parameter (generic, Forage and Concentrate) to set the feeding quantities and ratios associated to aliment types.
    /// See the § 3.2.1.2 "Feeding plan" of the "Functional description and terms of use" documentation.
    struct MixtureCharacteristicParameters
    {
    private:
        
        // serialization
        DECLARE_SERIALIZE_STD_METHODS;

    public:
        
        /// Global quantity of feed
        float quantity = 0.0f;
        
        /// Generic feed distribution by ratios depending on types
        std::map<unsigned int, float> typeRatios;
        
        
        MixtureCharacteristicParameters();
        MixtureCharacteristicParameters(MixtureCharacteristicParameters const& other);
        virtual ~MixtureCharacteristicParameters(){};

        bool operator==(MixtureCharacteristicParameters const& other);
        MixtureCharacteristicParameters operator / (float const& div);
        MixtureCharacteristicParameters operator * (float const& prod);
        void operator += (MixtureCharacteristicParameters &other);
        void operator += (float const& operatorQuantity);
        void operator -= (MixtureCharacteristicParameters const& other);
        void operator -= (float const& operatorQuantity);
        void operator *= (float const& fact);
        void operator /= (float const& div);
                      
#if defined _LOG || defined _FULL_RESULTS
        static std::string getStrForageType(FunctionalEnumerations::Feeding::ForageType ft);
        static std::string getStrConcentrateType(FunctionalEnumerations::Feeding::ConcentrateType ct);
#endif // _LOG || _FULL_RESULTS                

    protected :
        
        /// To get the quantity of feed, regarding it's type
        float getQuantity(unsigned int typeRatio);
            
        /// To set the ratio of a type of feed
        void setRatio(unsigned int typeRatio, float r);
            
        /// To get the ratio, regarding it's type
        float getRatio(unsigned int typeRatio);
        
        /// To initialize with all the aliment type, with a ration to 0
        void init(unsigned int typeNumber);
        
    public :
        /// Value checking, errors in the std::string parameter reference
        bool autoControl (
                                    /// Reference std::string to put the error message (empty value if no error)
                                    std::string &message
                                    );
    };
#define CONDITIONAL_DECLARE_SERIALIZE_STD_METHODS DECLARE_SERIALIZE_STD_METHODS

#define SPECIFIC_MIXTURE_CHARACTERISTIC_DECLARATION(className,typeR,maxRatioTypeValue) \
    struct className : public MixtureCharacteristicParameters \
    { \
        CONDITIONAL_DECLARE_SERIALIZE_STD_METHODS; \
    public: \
        inline float getQuantity(typeR typeRatio){ return MixtureCharacteristicParameters::getQuantity(((unsigned int)typeRatio));} \
        inline void setRatio(typeR typeRatio, float r){ MixtureCharacteristicParameters::setRatio(((unsigned int)typeRatio), r);} \
        inline float getRatio(typeR typeRatio){ return MixtureCharacteristicParameters::getRatio(((unsigned int)typeRatio));} \
        inline void init() { MixtureCharacteristicParameters::init (((unsigned int)typeR::maxRatioTypeValue)+1);} \
        virtual ~className(){}; \
    }; \

    // Specific mixture characteristic declarations, with max ratio value
    SPECIFIC_MIXTURE_CHARACTERISTIC_DECLARATION(ForageMixtureCharacteristicParameters,FunctionalEnumerations::Feeding::ForageType,lastForageType)
    SPECIFIC_MIXTURE_CHARACTERISTIC_DECLARATION(ConcentrateMixtureCharacteristicParameters,FunctionalEnumerations::Feeding::ConcentrateType,lastConcentrateType)
    
} /* End of namespace Lactation */
}
#endif // Parameters_MixtureCharacteristicParameters_h
