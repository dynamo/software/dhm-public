////////////////////////////////////////
//                                    //
// File : PriceTendencyParameters.cpp //
//                                    //
// Author : Philippe Gontier          //
//                                    //
// Version 1.0                        //
//                                    //
// Date : 01/10/2015                  //
//                                    //
////////////////////////////////////////

#include "PriceTendencyParameters.h"

// Project

BOOST_CLASS_EXPORT(ExchangeInfoStructures::Parameters::PriceTendencyParameters) // Recommended (mandatory if virtual serialization)

namespace ExchangeInfoStructures
{
namespace Parameters
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(currentPrice); \
    ar & BOOST_SERIALIZATION_NVP(annualTendency); \
    
    IMPLEMENT_SERIALIZE_STD_METHODS(PriceTendencyParameters);
    
    PriceTendencyParameters::PriceTendencyParameters()
    {
    }

    PriceTendencyParameters::PriceTendencyParameters(float theBasePrice, float theAnnualTendency)
    {
        currentPrice = theBasePrice;
        annualTendency = theAnnualTendency;
    }
        
    PriceTendencyParameters::PriceTendencyParameters(PriceTendencyParameters const& other)
    {
        currentPrice = other.currentPrice;
        annualTendency = other.annualTendency;
    }
    
    bool PriceTendencyParameters::operator==(PriceTendencyParameters const& other)
    {
        return currentPrice == other.currentPrice and annualTendency == other.annualTendency;
    }
    
    void PriceTendencyParameters::actualizePricesForNewMonth()
    {
        currentPrice *= 1.0f + (annualTendency - 1.0f)/12.0f;
    }
}
}