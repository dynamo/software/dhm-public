////////////////////////////////////////
//                                    //
// File : PriceTendencyParameters.h   //
//                                    //
// Author : Philippe Gontier          //
//                                    //
// Version 1.0                        //
//                                    //
// Date : 01/10/2015                  //
//                                    //
////////////////////////////////////////

#ifndef PriceTendency_h
#define PriceTendency_h

// standard

// Project
#include "../Tools/Serialization.h"

namespace ExchangeInfoStructures
{
namespace Parameters
{
    /// This structure make it possible to define price tendencies 
    /// See the § 2.2.7.1 "Accounting parameters" of the "Functional description and terms of use" documentation.
    struct PriceTendencyParameters
    {
    private:
        DECLARE_SERIALIZE_STD_METHODS;

    public:
        PriceTendencyParameters();
        
        /// Base price
        float currentPrice;

        /// Annual tendency
        float annualTendency;
        
        PriceTendencyParameters(float theBasePrice, float theAnnualTendency = 1.0f);
        virtual ~PriceTendencyParameters(){};
        PriceTendencyParameters(PriceTendencyParameters const& other);
        bool operator==(PriceTendencyParameters const& other);
        
        /// Month price update
        void actualizePricesForNewMonth();
    };
}    
}
#endif // PriceTendency
