////////////////////////////////////
//                                //
// File : ProtocolParameters.cpp  //
//                                //
// Author : Philippe Gontier      //
//                                //
// Version 1.0                    //
//                                //
// Date : 01/10/2015              //
//                                //
////////////////////////////////////

#include "ProtocolParameters.h"

// project
#include "LanguageDictionary.h"
#include "TechnicalConstants.h"

BOOST_CLASS_EXPORT(ExchangeInfoStructures::Parameters::ProtocolParameters) // Recommended (mandatory if virtual serialization)

namespace ExchangeInfoStructures
{
namespace Parameters
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(HeadParameters); \
        ar & BOOST_SERIALIZATION_NVP(simulationDuration); \
        ar & BOOST_SERIALIZATION_NVP(warmupDuration); \
        ar & BOOST_SERIALIZATION_NVP(simMonth); \
        ar & BOOST_SERIALIZATION_NVP(simYear); \
        ar & BOOST_SERIALIZATION_NVP(runNumber); \
        ar & BOOST_SERIALIZATION_NVP(runableName); \
        ar & BOOST_SERIALIZATION_NVP(accountingModelName); \
        ar & BOOST_SERIALIZATION_NVP(discriminant1); \
        ar & BOOST_SERIALIZATION_NVP(discriminant2); \
        ar & BOOST_SERIALIZATION_NVP(discriminant3); \
        ar & BOOST_SERIALIZATION_NVP(discriminant4); \
    
    IMPLEMENT_SERIALIZE_STD_METHODS(ProtocolParameters);

    bool ProtocolParameters::operator==(ProtocolParameters const& other)
    {
        bool res = HeadParameters::operator==(other) && 
                simulationDuration == other.simulationDuration &&
                warmupDuration == other.warmupDuration &&
                simMonth == other.simMonth &&
                simYear == other.simYear &&
                runNumber == other.runNumber &&
                runableName == other.runableName &&
                accountingModelName == other.accountingModelName &&
                discriminant1 == other.discriminant1 &&
                discriminant2 == other.discriminant2 &&
                discriminant3 == other.discriminant3 &&
                discriminant4 == other.discriminant4;
        return res;
    }
    
    bool ProtocolParameters::operator!=(ProtocolParameters const& other)
    {
        return !(*this == other);
    }
    
    ProtocolParameters::ProtocolParameters()
    {
        simulationDuration = TechnicalConstants::DEFAULT_SIMULATION_DURATION;
        warmupDuration = TechnicalConstants::DEFAULT_WARMUP_DURATION;
        simMonth = TechnicalConstants::DEFAULT_SIMULATION_MONTH;
        simYear = TechnicalConstants::DEFAULT_SIMULATION_YEAR;
        runNumber = TechnicalConstants::DEFAULT_RUN_NUMBER;
        runableName = "";
        accountingModelName = "";
        discriminant1 = "";
        discriminant2 = "";
        discriminant3 = "";
        discriminant4 = "";
    }

    ProtocolParameters::~ProtocolParameters()
    {
        
    }
    
    // Constructeur de copie   
    ProtocolParameters::ProtocolParameters (const ProtocolParameters &obj) : HeadParameters(obj)
    {
        simulationDuration = obj.simulationDuration;
        warmupDuration = obj.warmupDuration;
        simMonth = obj.simMonth;
        simYear = obj.simYear;
        runNumber = obj.runNumber;
        runableName = obj.runableName;
        accountingModelName = obj.accountingModelName;
        discriminant1 = obj.discriminant1;
        discriminant2 = obj.discriminant2;
        discriminant3 = obj.discriminant3;
        discriminant4 = obj.discriminant4;
    }
    
    bool ProtocolParameters::autoControl (std::string &message)
    {
        bool res = HeadParameters::autoControl(message);
        
        // Test of the number
        if (res and runNumber == 0)
        {
            res = false;
            message = LanguageDictionary::getMessage(KEY_THE_RUN_NUMBER_IS_NOT_VALID);
        }
        if (res and simulationDuration == 0)
        {
            res = false;
            message = LanguageDictionary::getMessage(KEY_THE_RUN_DURATION_IS_NOT_VALID);
        }
//        if (res and runableName == "")
//        {
//            res = false;
//            message = LanguageDictionary::getMessage(KEY_THE_SYSTEM_TO_SIMULATE) + runableName + LanguageDictionary::getMessage(KEY_IS_UNKNOWN);
//        }
        if (res and (simMonth < 1 or simMonth > 12))
        {
            res = false;
            message = LanguageDictionary::getMessage(KEY_THE_DATE_OF_THE_SIMULATION_BEGIN_IS_NOT_VALID);
        }
        
        if (res and simYear < 1)
        {
            res = false;
            message = LanguageDictionary::getMessage(KEY_THE_DATE_OF_THE_SIMULATION_BEGIN_IS_NOT_VALID);
        }
        
        return res;
    } /* End of method AutoControl */    
    
} /* End of namespace Parameters */
}
