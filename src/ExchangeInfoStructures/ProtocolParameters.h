////////////////////////////////////
//                                //
// File : ProtocolParameters.h    //
//                                //
// Author : Philippe Gontier      //
//                                //
// Version 1.0                    //
//                                //
// Date : 01/10/2015              //
//                                //
////////////////////////////////////

#ifndef Parameters_ProtocolParameters_h
#define Parameters_ProtocolParameters_h

// standard
#include <string>

// project
#include "HeadParameters.h"

namespace ExchangeInfoStructures
{
namespace Parameters
{
    /// This structure make it possible to define a simulation protocol 
    /// See the § 3.2.3 "Simulation protocol creating" of the "Functional description and terms of use" documentation.
    struct ProtocolParameters : public HeadParameters
    {
    private:

        // serialization
        DECLARE_SERIALIZE_STD_METHODS;

    protected:
        void concrete(){}; // To allow instanciation

    public:
        // attributes
        
        /// Duration of the run (in year)
        unsigned int simulationDuration; // duration of the run (in year, including warmup)
        
        /// Duration of the warmup (in year)
        unsigned int warmupDuration; // duration of the run (in year, included in simulation duration)
        
        /// Month of the begin of the simulation
        unsigned int simMonth;
         
        /// Year of the begin of the simulation
        unsigned int simYear; 
        
        /// Number of runs to simulate
        unsigned int runNumber; // number of run to simulate
         
        /// Name of the farm exploitation to run in the protocol
        std::string runableName;
         
        /// Name of the accounting model to use for the protocol
        std::string accountingModelName;
         
        /// Discriminant 1
        std::string discriminant1 = "";
        
        /// Discriminant 2
        std::string discriminant2 = "";
        
        /// Discriminant 3
        std::string discriminant3 = "";
        
        /// Discriminant 4
        std::string discriminant4 = "";

    public:
       // constructors / destructors
        ProtocolParameters(); // default constructor
        ProtocolParameters (const ProtocolParameters &obj);        // copy constructor
        virtual ~ProtocolParameters(); // destructor
           
        // Methods
        
        /// Value checking, errors in the std::string parameter reference
        virtual bool autoControl (
                                    /// Reference std::string to put the error message (empty value if no error)
                                    std::string &message
                                    ) override;
        bool operator==(ProtocolParameters const& other);
        bool operator!=(ProtocolParameters const& other);

    }; /* End of struct ProtocolParameters */
} /* End of namespace Parameters */
}
#endif // Parameters_ProtocolParameters_h
