#include "ReproductionTreatmentPlanParameters.h"

// project
#include "../Tools/Tools.h"
#include "FunctionalConstants.h"
#include "TechnicalConstants.h"
#include "HeadParameters.h"

BOOST_CLASS_EXPORT(ExchangeInfoStructures::Parameters::ReproductionTreatmentPlanParameters) // Recommended (mandatory if virtual serialization)

namespace ExchangeInfoStructures
{
namespace Parameters
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(noObservedOestrusReproductionTreatmentType); \
    ar & BOOST_SERIALIZATION_NVP(tooMuchUnsucessfulInseminationReproductionTreatmentType); \
    ar & BOOST_SERIALIZATION_NVP(negativePregnancyReproductionTreatmentType);

    
    IMPLEMENT_SERIALIZE_STD_METHODS(ReproductionTreatmentPlanParameters);

    ReproductionTreatmentPlanParameters::ReproductionTreatmentPlanParameters()
    {
        // Default values
        noObservedOestrusReproductionTreatmentType.name = FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_TREATMENT_NAME;
        noObservedOestrusReproductionTreatmentType.effectDelayMeanOrMin = FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_EFFECT_DELAY;
        noObservedOestrusReproductionTreatmentType.successProbabilityAllCasesOrFirst = FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_SUCCESS_PROBABILITY;

        tooMuchUnsucessfulInseminationReproductionTreatmentType.name = FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_NAME;
        tooMuchUnsucessfulInseminationReproductionTreatmentType.effectDelayMeanOrMin = FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_EFFECT_DELAY;
        tooMuchUnsucessfulInseminationReproductionTreatmentType.successProbabilityAllCasesOrFirst = FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_SUCCESS_PROBABILITY;
        tooMuchUnsucessfulInseminationReproductionTreatmentType.fertilityDelta = FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_FERTILITY_DELTA;

        
        negativePregnancyReproductionTreatmentType.name = FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_NAME;
        negativePregnancyReproductionTreatmentType.effectDelayMeanOrMin = FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_EFFECT_DELAY;
        negativePregnancyReproductionTreatmentType.successProbabilityAllCasesOrFirst = FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_SUCCESS_PROBABILITY;
    }

    ReproductionTreatmentPlanParameters::~ReproductionTreatmentPlanParameters()
    {
    }
    
    bool ReproductionTreatmentPlanParameters::operator==(ReproductionTreatmentPlanParameters const& other)
    {
        return noObservedOestrusReproductionTreatmentType == other.noObservedOestrusReproductionTreatmentType and
               tooMuchUnsucessfulInseminationReproductionTreatmentType == other.tooMuchUnsucessfulInseminationReproductionTreatmentType and
               negativePregnancyReproductionTreatmentType == other.negativePregnancyReproductionTreatmentType;
    }

    // Constructeur de copie   
    ReproductionTreatmentPlanParameters::ReproductionTreatmentPlanParameters (const ReproductionTreatmentPlanParameters &obj)
    {
        noObservedOestrusReproductionTreatmentType = obj.noObservedOestrusReproductionTreatmentType;
        tooMuchUnsucessfulInseminationReproductionTreatmentType = obj.tooMuchUnsucessfulInseminationReproductionTreatmentType;
        negativePregnancyReproductionTreatmentType = obj.negativePregnancyReproductionTreatmentType;
    }

    bool ReproductionTreatmentPlanParameters::autoControl(std::string &message)
    {
        return noObservedOestrusReproductionTreatmentType.autoControl(message) and tooMuchUnsucessfulInseminationReproductionTreatmentType.autoControl(message) and negativePregnancyReproductionTreatmentType.autoControl(message);
    } /* End of method AutoControl */  
        
    bool ReproductionTreatmentPlanParameters::setImportedValue(const std::string &key, std::string &content)
    {
        if (key == REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_NAME_PARAM_KEY) {noObservedOestrusReproductionTreatmentType.name = content; return true;}
        if (key == REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_EFFECT_DELAY_PARAM_KEY) {noObservedOestrusReproductionTreatmentType.effectDelayMeanOrMin = std::atoi(content.c_str()); return true;}
        if (key == REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_SUCCESS_PROBABILITY_PARAM_KEY) {noObservedOestrusReproductionTreatmentType.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        
        if (key == REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_NAME_PARAM_KEY) {tooMuchUnsucessfulInseminationReproductionTreatmentType.name = content; return true;}
        if (key == REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_EFFECT_DELAY_PARAM_KEY) {tooMuchUnsucessfulInseminationReproductionTreatmentType.effectDelayMeanOrMin = std::atoi(content.c_str()); return true;}
        if (key == REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_SUCCESS_PROBABILITY_PARAM_KEY) {tooMuchUnsucessfulInseminationReproductionTreatmentType.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        if (key == REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_FERTILITY_DELTA_PARAM_KEY) {tooMuchUnsucessfulInseminationReproductionTreatmentType.fertilityDelta = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}

        if (key == REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_NAME_PARAM_KEY) {negativePregnancyReproductionTreatmentType.name = content; return true;}
        if (key == REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_EFFECT_DELAY_PARAM_KEY) {negativePregnancyReproductionTreatmentType.effectDelayMeanOrMin = std::atoi(content.c_str()); return true;}
        if (key == REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_SUCCESS_PROBABILITY_PARAM_KEY) {negativePregnancyReproductionTreatmentType.successProbabilityAllCasesOrFirst = atof(Tools::changeChar(content, ',', '.').c_str()); return true;}
        
        return false; // Not here
    }
    
    bool ReproductionTreatmentPlanParameters::getValueToExport(const std::string &key, std::string &content)
    {
        GET_STRING_VALUE_TO_IMPORT(REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_NAME_PARAM_KEY, noObservedOestrusReproductionTreatmentType.name)
        GET_INTEGER_VALUE_TO_IMPORT(REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_EFFECT_DELAY_PARAM_KEY, noObservedOestrusReproductionTreatmentType.effectDelayMeanOrMin)
        GET_FLOAT_VALUE_TO_IMPORT(REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_SUCCESS_PROBABILITY_PARAM_KEY, noObservedOestrusReproductionTreatmentType.successProbabilityAllCasesOrFirst)
        
        GET_STRING_VALUE_TO_IMPORT(REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_NAME_PARAM_KEY, tooMuchUnsucessfulInseminationReproductionTreatmentType.name)
        GET_INTEGER_VALUE_TO_IMPORT(REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_EFFECT_DELAY_PARAM_KEY, tooMuchUnsucessfulInseminationReproductionTreatmentType.effectDelayMeanOrMin)
        GET_FLOAT_VALUE_TO_IMPORT(REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_SUCCESS_PROBABILITY_PARAM_KEY, tooMuchUnsucessfulInseminationReproductionTreatmentType.successProbabilityAllCasesOrFirst)
        GET_FLOAT_VALUE_TO_IMPORT(REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_FERTILITY_DELTA_PARAM_KEY, tooMuchUnsucessfulInseminationReproductionTreatmentType.fertilityDelta)

        GET_STRING_VALUE_TO_IMPORT(REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_NAME_PARAM_KEY, negativePregnancyReproductionTreatmentType.name)
        GET_INTEGER_VALUE_TO_IMPORT(REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_EFFECT_DELAY_PARAM_KEY, negativePregnancyReproductionTreatmentType.effectDelayMeanOrMin)
        GET_FLOAT_VALUE_TO_IMPORT(REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_SUCCESS_PROBABILITY_PARAM_KEY, negativePregnancyReproductionTreatmentType.successProbabilityAllCasesOrFirst)

        return false; // Not here
    }    
    
#ifdef _LOG
    void ReproductionTreatmentPlanParameters::getDefaultValues(std::vector<std::string> &vLines)
    {
        std::string line = "";   
        line = REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_NAME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_TREATMENT_NAME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_EFFECT_DELAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_EFFECT_DELAY); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_SUCCESS_PROBABILITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_SUCCESS_PROBABILITY); vLines.push_back(Tools::changeChar(line,'.', ','));
        
        line = REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_NAME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_NAME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_EFFECT_DELAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_EFFECT_DELAY); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_SUCCESS_PROBABILITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_SUCCESS_PROBABILITY); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_FERTILITY_DELTA_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_FERTILITY_DELTA); vLines.push_back(Tools::changeChar(line,'.', ','));

        line = REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_NAME_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_NAME); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_EFFECT_DELAY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_EFFECT_DELAY); vLines.push_back(Tools::changeChar(line,'.', ','));
        line = REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_SUCCESS_PROBABILITY_PARAM_KEY + TechnicalConstants::EXPORT_SEP + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_SUCCESS_PROBABILITY); vLines.push_back(Tools::changeChar(line,'.', ','));
    }
#endif // _LOG
    
} /* End of namespace Parameters */
}