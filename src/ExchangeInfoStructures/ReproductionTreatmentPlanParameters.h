/////////////////////////////////////////////////
//                                             //
// File : ReproductionTreatmentPlanParameters.h    //
//                                             //
// Author : Philippe Gontier                   //
//                                             //
// Date : 27/09/2022                           //
//                                             //
// Ketosis treatement settings                 //
//                                             //
/////////////////////////////////////////////////
#ifndef Parameters_ReproductionTreatmentPlanParameters_h
#define Parameters_ReproductionTreatmentPlanParameters_h

// Standard

// Project
#include "../Tools/Serialization.h"
#include "TreatmentTypeParameters.h"

namespace ExchangeInfoStructures
{
namespace Parameters
{
    /// This structure make it possible to makes it possible to define the modalities to be implemented for curative ketosis treatments, differentiated by ketosis severity
    /// See the § 2.2.3.3.2.3 "Treatment plan" of the "Functional description and terms of use" documentation.
    struct ReproductionTreatmentPlanParameters
    {        
    private:

    public :
        
        ///  Systemic G1 non infectious lameness treatment plan (see § 2.2.1.2.9 of the "Functional description and terms of use" documentation)
        TreatmentTypeParameters noObservedOestrusReproductionTreatmentType;
        
        ///  Systemic G2 non infectious  lameness treatment plan (see § 2.2.1.2.9 of the "Functional description and terms of use" documentation)
        TreatmentTypeParameters tooMuchUnsucessfulInseminationReproductionTreatmentType;
        
        ///  Systemic G1 infectious  lameness treatment plan (see § 2.2.1.2.9 of the "Functional description and terms of use" documentation)
        TreatmentTypeParameters negativePregnancyReproductionTreatmentType;
                
    private:
        DECLARE_SERIALIZE_STD_METHODS;

    public:
        
        /// Value checking, errors in the std::string parameter reference
        bool autoControl (
                                    /// Reference std::string to put the error message (empty value if no error)
                                    std::string &message
                                    );
        ReproductionTreatmentPlanParameters();
        virtual ~ReproductionTreatmentPlanParameters();
        bool operator==(ReproductionTreatmentPlanParameters const& other);
        ReproductionTreatmentPlanParameters (const ReproductionTreatmentPlanParameters &obj);        //constructeur de copie   
#ifdef _LOG
        static void getDefaultValues(std::vector<std::string> &vLines);
#endif // _LOG
        bool setImportedValue(const std::string &key, std::string &content);
        bool getValueToExport(const std::string &key, std::string &content);

    }; /* End of struct ReproductionTreatmentPlanParameters */
    
    // Import/export key values
    static const std::string REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_NAME_PARAM_KEY = "reproductionTreatmentPlan_No_oestrus_detected_after_calving_name";
    static const std::string REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_EFFECT_DELAY_PARAM_KEY = "reproductionTreatmentPlan_No_oestrus_detected_after_calving_effectDelay";
    static const std::string REPRODUCTION_TREATMENT_PLAN_NO_OBSERVED_OESTRUS_SUCCESS_PROBABILITY_PARAM_KEY = "reproductionTreatmentPlan_No_oestrus_detected_after_calving_successProbability";
    
    static const std::string REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_NAME_PARAM_KEY = "reproductionTreatmentPlan_Too_much_unsuccessful_inseminations_name";
    static const std::string REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_EFFECT_DELAY_PARAM_KEY = "reproductionTreatmentPlan_Too_much_unsuccessful_inseminations_effectDelay";
    static const std::string REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_SUCCESS_PROBABILITY_PARAM_KEY = "reproductionTreatmentPlan_Too_much_unsuccessful_inseminations_successProbability";
    static const std::string REPRODUCTION_TREATMENT_PLAN_TOO_MUCH_UNSUCCESSFUL_INSEMINATIONS_FERTILITY_DELTA_PARAM_KEY = "reproductionTreatmentPlan_Too_much_unsuccessful_inseminations_fertilityDelta";

    static const std::string REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_NAME_PARAM_KEY = "reproductionTreatmentPlan_Negative_pregnancy_diagnostic_name";
    static const std::string REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_EFFECT_DELAY_PARAM_KEY = "reproductionTreatmentPlan_Negative_pregnancy_diagnostic_effectDelay";
    static const std::string REPRODUCTION_TREATMENT_PLAN_NEGATIVE_PREGNANCY_DIAGNOSTIC_SUCCESS_PROBABILITY_PARAM_KEY = "reproductionTreatmentPlan_Negative_pregnancy_diagnostic_successProbability";

} /* End of namespace Parameters */
}
#endif // Parameters_ReproductionTreatmentPlanParameters_h
