///////////////////////////////////////////
//                                       //
// File : SystemToSimulateParameters.cpp //
//                                       //
// Author : Philippe Gontier             //
//                                       //
// Version 1.0                           //
//                                       //
// Date : 01/10/2015                     //
//                                       //
///////////////////////////////////////////

#include "SystemToSimulateParameters.h"

BOOST_CLASS_EXPORT(ExchangeInfoStructures::Parameters::SystemToSimulateParameters) // Recommended (mandatory if virtual serialization)

namespace ExchangeInfoStructures
{
namespace Parameters
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(HeadParameters); \
    
    IMPLEMENT_SERIALIZE_STD_METHODS(SystemToSimulateParameters);
    
    SystemToSimulateParameters::SystemToSimulateParameters() : HeadParameters()
    {
    }
    
    // Constructeur de copie   
    SystemToSimulateParameters::SystemToSimulateParameters (const SystemToSimulateParameters &obj) : HeadParameters(obj)
    {
    }

    bool SystemToSimulateParameters::autoControl(std::string &message)
    {
        return HeadParameters::autoControl(message);
    } /* End of method AutoControl */        
} /* End of namespace SystemToSimulateParameters */
}