//////////////////////////////////////////
//                                      //
// File : SystemToSimulateParameters.h  //
//                                      //
// Author : Philippe Gontier            //
//                                      //
// Version 1.0                          //
//                                      //
// Date : 01/10/2015                    //
//                                      //
//////////////////////////////////////////

#ifndef Parameters_SystemToSimulateParameters_h
#define Parameters_SystemToSimulateParameters_h

// Standard

// Project
#include "HeadParameters.h"

namespace ExchangeInfoStructures
{
/// Parameters
namespace Parameters
{             
    struct SystemToSimulateParameters : public HeadParameters
    {
    private:
        // serialization
        DECLARE_SERIALIZE_STD_METHODS;

    public:
        // constructors / destructors
        SystemToSimulateParameters();
        SystemToSimulateParameters (const SystemToSimulateParameters &obj);        // copy constructor
        virtual ~SystemToSimulateParameters(){}; // destructor
 
        /// Value checking, errors in the std::string parameter reference
        virtual bool autoControl (
                                    /// Reference std::string to put the error message (empty value if no error)
                                    std::string &message
                                    ) override;
    }; /* End of struct SystemToSimulateParameters */
} /* End of namespace Parameters */
}
#endif // Parameters_SystemToSimulateParameters_h
