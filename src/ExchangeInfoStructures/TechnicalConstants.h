#ifndef TechnicalConstants_h
#define TechnicalConstants_h

namespace TechnicalConstants
{ 
// Constantes de management simulation
// -----------------------------------
const std::string STR_VERSION = "Version V1.2.2 - 11/2023";

const char EXPORT_SEP = ';'; // Field separator in export files

const std::string DAY_OF_THE_WEEK_FOR_OPERATIONS = "Sun";

// folders and files
const std::string DATA_FILE_EXTENSION = ".csv";
const std::string LOG_FILE_NAME = "log";
const std::string LOG_FILE_EXTENSION = ".txt";
const std::string COMMENT = "//";

const std::string DATA_FOLDER = "data/";
const std::string TECHNICAL_DATA_FOLDER = DATA_FOLDER + "Technical/";
const std::string RELATIVE_TECHNICAL_DATA_FOLDER = "../" + TECHNICAL_DATA_FOLDER;
const std::string RELATIVE_DAIRY_FARM_DEFAULT_FOLDER = "../" + DATA_FOLDER + "DairyFarms/";
const std::string RELATIVE_ACCOUNTING_DATA_DEFAULT_FOLDER = "../" + DATA_FOLDER + "AccountingData/";
const std::string RELATIVE_GENETIC_CATALOGUE_DATA_DEFAULT_FOLDER = "../" + DATA_FOLDER + "GeneticCatalogues/";
const std::string MASTITIS_RISK_CURVE_FILE_NAME = "MastitisRiskCurves" + DATA_FILE_EXTENSION;
const std::string KETOSIS_RISK_CURVE_FILE_NAME = "KetosisRiskCurve" + DATA_FILE_EXTENSION;
const std::string LAMENESS_RISK_CURVE_FILE_NAME = "LamenessRiskCurve" + DATA_FILE_EXTENSION;
const std::string LACTATION_CURVE_FILE_NAME = "LactationCurves" + DATA_FILE_EXTENSION;
const std::string DEFAULT_RELATIVE_RESULT_FOLDER = "../results/";
#ifdef _LOG
const std::string ACCOUNTING_DATA_FOLDER = DATA_FOLDER + "AccountingData/";
const std::string DAIRY_FARM_DATA_FOLDER = DATA_FOLDER + "DairyFarms/";
const std::string GENETIC_CATALOGUES_DATA_FOLDER = DATA_FOLDER + "GeneticCatalogues/";
const std::string SCC_CURVE_FILE_NAME = "SccCurves" + DATA_FILE_EXTENSION;
#endif
 // Result files
#if defined _DHM_SERVER || defined _LOG
const std::string XML_ARCHIVE_EXTENSION = ".xml"; // Archive Extension
#endif // _DHM_SERVER || _LOG

const std::string BINARY_ARCHIVE_EXTENSION = ".dat"; // Archive Extension


// Simulation constants
const unsigned int TIME_LAPS_DURATION = 1;

// Protocol constants
const std::string DEFAULT_DAIRY_FARM_NAME = "Default dairy farm";
const std::string DEFAULT_ACCOUNTING_MODEL_NAME = "Default accounting model";
const std::string ALL_FARM_OPTION = "_all";
const unsigned int DEFAULT_RUN_NUMBER = 8;
const unsigned int DEFAULT_SIMULATION_MONTH = 1;
const unsigned int DEFAULT_SIMULATION_YEAR = 2022;
const unsigned int DEFAULT_SIMULATION_DURATION = 10;
const unsigned int DEFAULT_WARMUP_DURATION = 0;
const std::string SIMULATION_DURATION_KEY = "simulationDuration";
const std::string WARMUP_DURATION_KEY = "warmupDuration";
const std::string SIMULATION_MONTH_KEY= "simMonth";
const std::string SIMULATION_YEAR_KEY = "simYear";
const std::string RUN_NUMBER_KEY = "runNumber";
const std::string FARM_NAME_KEY = "farmName";
const std::string BASIS_KEY = "_basis";
 

// Result file names 
const std::string LOG_DIRECTORY = "log/"; // Archive Extension
// Calibration 
// -----------

// Presimulation duration
const int PRESIMULATION_DURATION = 12; // Years

// Milk production : when the pre-simulation finishes (after farmer selection of the cows in his herd, the production must respects the breed genetic value
const float MILK_PRODUCTION_QUANTITY_FACTOR_CALIBRATION = 7600.0f / 10081.9f;
const float MILK_PRODUCTION_TB_FACTOR_CALIBRATION = 40.1 / 51.28f;
const float MILK_PRODUCTION_TP_FACTOR_CALIBRATION = 31.9f / 41.34f;
const float MILK_PRODUCTION_SCC_FACTOR_CALIBRATION = 200.0f / 206.0f;
                            
// Default Mastitis objectives :
// Clinical_Mastitis_occurency_by_cow in TechnicalHealthResults.csv = 35%
//      -> BASE_CALIBRATION_MASTITIS_INCIDENCE
// Average_SCC in MilkControlResults.csv = 200 000/ml
// SCC_300_prevalence in MilkControlResults.csv between 83% and 85%)
//       -> FunctionalConstants::Health::ADDITIONAL_SCC_RATIO_MOST_LIKELY
const float BASE_CALIBRATION_MASTITIS_INCIDENCE = 6.52f;

// Ketosis calibration values :
const float BASE_CALIBRATION_KETOSIS_INCIDENCE = 1.79f;
const float FIRST_OESTRUS_DELAY_WHEN_KETOSIS_CALIBRATION_VALUE = 1.0f;
const float KETOSIS_CYCLICITY_CALIBRATION_VALUE = 1.0f;
const float KETOSIS_FERTILITY_CALIBRATION_VALUE = 2.0f;

// Mortality calibration values :
// File "DeathCalculation.xlsx" with data in "ExitResults.csv" and "DeathResults.csv"
// Set following values to 1.0f before calibration simulation (see technical report)
const float ONE_TO_TWO_YEARS_OLD_UNMANAGED_DEATH_CALIBRATION_RATIO = 1.0f; // 1 to 2 years old
const float TWO_TO_THREE_AND_HALF_YEARS_OLD_UNMANAGED_DEATH_CALIBRATION_RATIO = 0.655949f; // 2 to 3.5 years old
const float THREE_AND_HALF_TO_FIVE_YEARS_OLD_UNMANAGED_DEATH_CALIBRATION_RATIO = 0.709251f; // 3.5 to 5 years old
const float FIVE_TO_TEN_YEARS_OLD_UNMANAGED_DEATH_CALIBRATION_RATIO = 0.347518f; // 5 to 10 years old
const float OVER_TEN_YEARS_OLD_UNMANAGED_DEATH_CALIBRATION_RATIO = 0.6666667f; // over 10 years old

// Population regulation :
// We have anticipate the culling decision regarding the population situation, then to define here a value to ponderate the futur culling need
// This value depend on female calve management strategy and annual herd renewable part parameters, it have been observed during calibration :
const float MIN_RENEWAL_PART_CALIBRATION = 0.25f;
const float MIN_RENEWAL_PART_CALIBRATION_RES = 4.6f;
const float MEDIUM_RENEWAL_PART_CALIBRATION = 0.35f;
const float MEDIUM_RENEWAL_PART_CALIBRATION_RES = 2.6f;
const float MAX_RENEWAL_PART_CALIBRATION = 0.50f;
const float MAX_RENEWAL_PART_CALIBRATION_RES = 1.3f;

const float FACTOR_POPULATION_NUMBER_CALIBRATION = 1.04f;
const float ANNUAL_HERD_RENEWABLE_PART_CALIBRATION = 0.89f;

// Managed costs calibration
//const float BASE_CALIBRATION_MANAGED_HEALTH_ANNUAL_COST_PER_COW = -18.0f;


#ifdef _LOG
const std::string ANIMAL_LOG_DIRECTORY = LOG_DIRECTORY + "animals/"; // Archive Extension
const std::string BULL_CATALOG_DEFAULT_VALUES_FILE_NAME = "BaseBullGeneticValues" + DATA_FILE_EXTENSION;
const std::string DEFAULT_DAIRY_FARM_PARAMETER_VALUES_FILE_NAME = "DefaultDairyFarmParameterValues" + DATA_FILE_EXTENSION;
const std::string DEFAULT_ACCOUNTING_PARAMETER_VALUES_FILE_NAME = "DefaultAccountingParameterValues" + DATA_FILE_EXTENSION;
const std::string MANAGED_HEALTH_COSTS_FOR_CALIBRATION_VALUES_FILE_NAME = "ManagedHealthCostsForCalibration" + DATA_FILE_EXTENSION;
const std::string DEATH_RESULT_FILE_NAME = "DeathResults" + DATA_FILE_EXTENSION;
const std::string EXIT_RESULT_FILE_NAME = "ExitResults" + DATA_FILE_EXTENSION;
#endif

#if defined _LOG || defined _FULL_RESULTS
const std::string PRIMIPARE_REPRODUCTION_RESULTS_BASED_ON_CALVING_FILE_NAME = "PrimipareReproductionResultsBasedOnCalving" + DATA_FILE_EXTENSION; // 2.2.1.3.1.1
const std::string MULTIPARE_REPRODUCTION_RESULTS_BASED_ON_CALVING_FILE_NAME = "MultipareReproductionResultsBasedOnCalving" + DATA_FILE_EXTENSION; // 2.2.1.3.1.2
const std::string NULLIPARE_REPRODUCTION_RESULTS_BASED_ON_INSEMINATION_FILE_NAME = "NullipareReproductionResultsBasedOnInsemination" + DATA_FILE_EXTENSION; // 2.2.1.3.2.1
const std::string PRIMIMULTIPARE_REPRODUCTION_RESULTS_BASED_ON_INSEMINATION_FILE_NAME = "PrimiMultipareReproductionResultsBasedOnInsemination" + DATA_FILE_EXTENSION; // 2.2.1.3.2.2
const std::string DELIVERED_MILK_RESULTS_FILE_NAME = "DeliveredMilk" + DATA_FILE_EXTENSION; // 2.2.2.3.1
const std::string DISCARDED_MILK_RESULTS_FILE_NAME = "DiscardedMilk" + DATA_FILE_EXTENSION; // 2.2.2.3.2
const std::string ANNUAL_MASTITIS_RESULTS_FILE_NAME = "AnnualMastitisResults" + DATA_FILE_EXTENSION; // 2.2.3.1.3
const std::string ANNUAL_KETOSIS_RESULTS_FILE_NAME = "AnnualKetosisResults" + DATA_FILE_EXTENSION; // 2.2.3.2.3
const std::string ANNUAL_NON_INFECTIOUS_LAMENESS_RESULTS_FILE_NAME = "AnnualNonInfectiousLamenessResults" + DATA_FILE_EXTENSION; // 2.2.3.3.3
const std::string ANNUAL_INFECTIOUS_LAMENESS_RESULTS_FILE_NAME = "AnnualInfectiousLamenessResults" + DATA_FILE_EXTENSION; // 2.2.3.3.3
const std::string CALF_FEEDING_RESULTS_FILE_NAME = "CalfFeedingResults" + DATA_FILE_EXTENSION; // 2.2.6.3.1
const std::string HEIFER_FEEDING_RESULTS_FILE_NAME = "HeiferFeedingResults" + DATA_FILE_EXTENSION; // 2.2.6.3.2
const std::string COW_FEEDING_RESULTS_FILE_NAME = "CowFeedingResults" + DATA_FILE_EXTENSION; // 2.2.6.3.3
#endif // _LOG || _FULL_RESULTS

const std::string TECHNICAL_AND_ECONOMICAL_RESULT_FILE_NAME = "TechnicalAndEconomicalResults" + DATA_FILE_EXTENSION; // 3.4
const std::string MILK_CONTROL_RESULT_FILE_NAME = "MilkControlResults" + DATA_FILE_EXTENSION; // 2.2.2.3.3


// milk persistance
const int MAXIMUM_DURATION_FOR_MILK_CURVE = 800;

}

#endif // TechnicalConstants_h
