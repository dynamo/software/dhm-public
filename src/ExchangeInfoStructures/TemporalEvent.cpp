#include "TemporalEvent.h"

// project

BOOST_CLASS_EXPORT(ExchangeInfoStructures::TemporalEvent) // Recommended (mandatory if virtual serialization)

namespace ExchangeInfoStructures
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(type); \
    ar & BOOST_SERIALIZATION_NVP(beginEventPeriod); \
    ar & BOOST_SERIALIZATION_NVP(eventDate); \
    ar & BOOST_SERIALIZATION_NVP(endEventPeriod);
    
    IMPLEMENT_SERIALIZE_STD_METHODS(TemporalEvent);

    // Copy constructor   
    TemporalEvent::TemporalEvent (const TemporalEvent &obj)
    {
        type = obj.type;
        beginEventPeriod = obj.beginEventPeriod;
        eventDate = obj.eventDate;
        endEventPeriod = obj.endEventPeriod;       
    }
    
    TemporalEvent::TemporalEvent(TemporalEventType theType, const boost::gregorian::date theBeginPeriodDate, const boost::gregorian::date theEventDate, const boost::gregorian::date theEndPeriodDate)
    {
        type = theType;
        beginEventPeriod = theBeginPeriodDate;
        eventDate = theEventDate;
        endEventPeriod = theEndPeriodDate;        
    }
    
    TemporalEvent::TemporalEvent()
    {
    }
    
    TemporalEvent::~TemporalEvent()
    {
    }

    bool TemporalEvent::isCurrent(const boost::gregorian::date theDate)
    {
        return (theDate >= beginEventPeriod && theDate <= endEventPeriod);
    }
    
    bool TemporalEvent::isOutOfTime(const boost::gregorian::date theDate)
    {
        return (theDate > endEventPeriod);
    }
    
    bool TemporalEvent::operator==(TemporalEvent const& other)
    {
        return type == other.type && beginEventPeriod == other.beginEventPeriod && eventDate == other.eventDate && endEventPeriod == other.endEventPeriod;
    }

    bool TemporalEvent::operator!=(TemporalEvent const& other)
    {
        return !(*this == other); // Already coded
    }


} /* End of namespace ExchangeInfoStructures */