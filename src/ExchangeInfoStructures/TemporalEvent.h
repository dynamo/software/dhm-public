#ifndef ExchangeInfoStructures_TemporalEvent_h
#define ExchangeInfoStructures_TemporalEvent_h

// boost

// project
#include "../Tools/Serialization.h"

namespace ExchangeInfoStructures
{
    struct TemporalEvent
    {
    private:
        
        // serialization
        DECLARE_SERIALIZE_STD_METHODS;
        
        TemporalEvent(); // default constructor for serialization

    public:
        enum TemporalEventType
        { 
            IAMM
        };

        TemporalEventType type;
        boost::gregorian::date beginEventPeriod;
        boost::gregorian::date eventDate;
        boost::gregorian::date endEventPeriod;       
                
        // constructors / destructors
        TemporalEvent(TemporalEventType theType, const boost::gregorian::date theEventDate) : TemporalEvent(theType, theEventDate, theEventDate, theEventDate) {};
        TemporalEvent(TemporalEventType theType, const boost::gregorian::date theBeginPeriodDate, const boost::gregorian::date theEventDate, const boost::gregorian::date theEndPeriodDate);
        TemporalEvent (const TemporalEvent &obj);        // copy constructor
        virtual ~TemporalEvent(); // destructor
        
        // Operators
        bool operator==(TemporalEvent const& other);
        bool operator!=(TemporalEvent const& other);
        
        // Methods
        bool isCurrent(const boost::gregorian::date theDate);
        bool isOutOfTime(const boost::gregorian::date theDate);
    };
} /* End of namespace ExchangeInfoStructures */
#endif // ExchangeInfoStructures_TemporalEvent_h
