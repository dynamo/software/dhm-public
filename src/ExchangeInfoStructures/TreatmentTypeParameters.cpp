#include "TreatmentTypeParameters.h"

// project

BOOST_CLASS_EXPORT(ExchangeInfoStructures::Parameters::TreatmentTypeParameters) // Recommended (mandatory if virtual serialization)

namespace ExchangeInfoStructures
{
namespace Parameters
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
	ar & BOOST_SERIALIZATION_NVP(name); \
        ar & BOOST_SERIALIZATION_NVP(effectDelayMeanOrMin); \
        ar & BOOST_SERIALIZATION_NVP(effectDelayMax); \
        ar & BOOST_SERIALIZATION_NVP(milkWaitingTime); \
        ar & BOOST_SERIALIZATION_NVP(successProbabilityAllCasesOrFirst); \
        ar & BOOST_SERIALIZATION_NVP(successProbabilitySecondAndOtherCases); \
        ar & BOOST_SERIALIZATION_NVP(flareupRisk); \
        ar & BOOST_SERIALIZATION_NVP(protectProbability); \
        ar & BOOST_SERIALIZATION_NVP(protectDuration); \
        ar & BOOST_SERIALIZATION_NVP(fertilityDelta); \

        IMPLEMENT_SERIALIZE_STD_METHODS(TreatmentTypeParameters);

    TreatmentTypeParameters::TreatmentTypeParameters()
    {
    }
    
    TreatmentTypeParameters::~TreatmentTypeParameters()
    {
        
    }
    
    bool TreatmentTypeParameters::operator==(TreatmentTypeParameters const& other)
    {
        return name == other.name and
               effectDelayMeanOrMin == other.effectDelayMeanOrMin and
               effectDelayMax == other.effectDelayMax and
               milkWaitingTime == other.milkWaitingTime and
               successProbabilityAllCasesOrFirst == other.successProbabilityAllCasesOrFirst and
               successProbabilitySecondAndOtherCases == other.successProbabilitySecondAndOtherCases and
               flareupRisk == other.flareupRisk and
               protectProbability == other.protectProbability and
               protectDuration == other.protectDuration and
               fertilityDelta == other.fertilityDelta;      
    }

    // Constructeur de copie   
    TreatmentTypeParameters::TreatmentTypeParameters(const TreatmentTypeParameters &obj)
    {
        name = obj.name;
        effectDelayMeanOrMin = obj.effectDelayMeanOrMin;
        effectDelayMax = obj.effectDelayMax;
        milkWaitingTime = obj.milkWaitingTime;
        successProbabilityAllCasesOrFirst = obj.successProbabilityAllCasesOrFirst;
        successProbabilitySecondAndOtherCases = obj.successProbabilitySecondAndOtherCases;
        flareupRisk = obj.flareupRisk;
        protectProbability = obj.protectProbability;
        protectDuration = obj.protectDuration;
        fertilityDelta = obj.fertilityDelta;
    }

    bool TreatmentTypeParameters::autoControl(std::string &message)
    {
        bool res = true;
        
        return res;
    } /* End of method AutoControl */    

} /* End of namespace Parameters */
}