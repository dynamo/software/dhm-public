/////////////////////////////////////////
//                                     //
// File : TreatmentTypeParameters.h    //
//                                     //
// Author : Philippe Gontier           //
//                                     //
// Date : 01/10/2015                   //
//                                     //
// Treatement type settings            //
//                                     //
/////////////////////////////////////////
#ifndef Parameters_TreatmentTypeParameters_h
#define Parameters_TreatmentTypeParameters_h

// standard

// project
#include "../Tools/Serialization.h"

namespace ExchangeInfoStructures
{
namespace Parameters
{
    /// This structure is to define generic treatement type settings
    struct TreatmentTypeParameters
    {        
    public:
        /// Name of the treatment
        std::string name;

        /// Mean or min treatment effect delay
        unsigned int effectDelayMeanOrMin = 0;
        
        /// Max treatment effect delay
        unsigned int effectDelayMax = 0;
        
        /// Milk waiting duration for delivery
        unsigned int milkWaitingTime = 0;
        
        /// Success probability for all case or for the first one
        float successProbabilityAllCasesOrFirst = 0.0f;

        /// Healing probability for the second and other cases
        float successProbabilitySecondAndOtherCases = 0.0f;

        /// Flare up risk
        float flareupRisk = 0.0f;

        /// Protect probability
        float protectProbability = 0.0f;

        /// Protection duration
        unsigned int protectDuration = 0;

        /// Fertility factor
        float fertilityDelta = 0.0f;

    private:
        DECLARE_SERIALIZE_STD_METHODS;

    public:
        
        /// Value checking, errors in the std::string parameter reference
        bool autoControl (
                                    /// Reference std::string to put the error message (empty value if no error)
                                    std::string &message
                                    );
        TreatmentTypeParameters();
        virtual ~TreatmentTypeParameters();
        bool operator==(TreatmentTypeParameters const& other);
        TreatmentTypeParameters (const TreatmentTypeParameters &obj);        //constructeur de copie   
    };
} /* End of namespace Parameters */
}
#endif // Parameters_TreatmentTypeParameters_h
