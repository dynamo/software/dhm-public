#include "BeefBreed.h"

// boost

// standard

// project
#include "../GeneticManagement.h"
#include "../../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(Genetic::Breed::BeefBreed) // Recommended (mandatory if virtual serialization)

namespace Genetic
{
namespace Breed
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Breed); \
    
    IMPLEMENT_SERIALIZE_STD_METHODS(BeefBreed);
    
    BeefBreed::BeefBreed(FunctionalEnumerations::Genetic::Breed theBreed) : Breed(theBreed)
    {
        _mBreedMeanPerformances = FunctionalConstants::Genetic::DEFAULT_PRODUCTION_FOR_BEEF_COW;
        _pmOvulationDetectionProbability = &FunctionalConstants::Reproduction::NORMANDE_OVULATION_DETECTION_PROBA; // tmp, beef breeds have Normande values until real specification
    }
       
    void BeefBreed::getMaleGeneticValuesForCatalog(std::map<FunctionalEnumerations::Genetic::Breed, std::vector<Genetic::GeneticValue>> &maleGeneticValueCatalog, const std::string &maleDataFile, FunctionalEnumerations::Genetic::GeneticStrategy geneticStrategy)
    {
        // Only one generic value for the beef breeds
        std::vector<Genetic::GeneticValue> cat;
         
        Genetic::GeneticValue ge;
        //ge.setGeneticStrategy(geneticStrategy);
            
        for (std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float>::iterator it =  _mBreedMeanPerformances.begin(); it != _mBreedMeanPerformances.end(); it++)
        {
            FunctionalEnumerations::Genetic::PhenotypicCharacter currentChar = it->first;
            ge.setCharacterGeneticValue(currentChar, CharacterGeneticValue(0.0f, 0.0f, it->second, currentChar));
        }
        ge.finalizeCalculatedGeneticValues(geneticStrategy);
        cat.push_back(ge);
        maleGeneticValueCatalog[getEnumerationBreed()] = cat;
    }

    FunctionalEnumerations::Genetic::BreedType BeefBreed::getBreedType()
    {
        return FunctionalEnumerations::Genetic::BreedType::beef;
    }
    
    float BeefBreed::getInseminationSuccessProba(FunctionalEnumerations::Reproduction::DairyCowParity parity, FunctionalEnumerations::Reproduction::TypeInseminationSemen ts)
    {
        return (FunctionalConstants::Reproduction::NORMANDE_INSEMINATION_SUCCESS_PROBA.find(ts)->second).find(parity)->second;  // tmp, beef breeds have Normande values until real specification
    }
}
}