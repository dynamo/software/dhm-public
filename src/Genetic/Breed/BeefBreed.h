#ifndef BeefBreed_h
#define BeefBreed_h

// standard
#include <map>
#include <vector>

// project
#include "Breed.h"
#include "../GeneticValue.h"

namespace Genetic
{
namespace Breed
{
    class BeefBreed : public Breed
    {

    DECLARE_SERIALIZE_STD_METHODS;

    protected:
        BeefBreed(FunctionalEnumerations::Genetic::Breed theBreed);
        void getMaleGeneticValuesForCatalog(std::map<FunctionalEnumerations::Genetic::Breed, std::vector<Genetic::GeneticValue>> &MaleGeneticValueCatalog, const std::string &MaleDataFile, FunctionalEnumerations::Genetic::GeneticStrategy geneticStrategy) override;
   
    public:
        BeefBreed(){} // serialization constructor
        virtual ~BeefBreed(){}
        
        FunctionalEnumerations::Genetic::BreedType getBreedType();
        float getInseminationSuccessProba(FunctionalEnumerations::Reproduction::DairyCowParity parity, FunctionalEnumerations::Reproduction::TypeInseminationSemen ts);
        void appendAnnualProgress(){};
    };
    }
} /* End of namespace Genetic */
#endif // BeefBreed_h
