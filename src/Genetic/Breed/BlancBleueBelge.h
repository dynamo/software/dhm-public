#ifndef DataStructures_BlancBleueBelgeBreed_h
#define DataStructures_BlancBleueBelgeBreed_h

// standard

// project
#include "BeefBreed.h"

namespace Genetic
{
namespace Breed
{
    class BlancBleueBelge : public BeefBreed
    {
        
    DECLARE_SERIALIZE_STD_METHODS;
    
    protected:
        void concrete(){}; // To allow instanciation

    public:
        BlancBleueBelge() : BeefBreed(FunctionalEnumerations::Genetic::Breed::blancBleueBelge){}
        virtual ~BlancBleueBelge(){}
#ifdef _LOG
        std::string getName() {return "BlancBleueBelge";}
#endif // _LOG
    };
}
} /* End of namespace Genetic */
#endif // DataStructures_BlancBleueBelgeBreed_h
