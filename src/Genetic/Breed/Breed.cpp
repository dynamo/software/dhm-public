#include "Breed.h"

// boost

// standard
#include <assert.h> // assert

// project
#include "../../Tools/Random.h"
#include "../GeneticManagement.h"
#include "../../ExchangeInfoStructures/FunctionalConstants.h"
#include "../../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(Genetic::Breed::Breed) // Recommended (mandatory if virtual serialization)

namespace Genetic
{
namespace Breed
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
            ar & BOOST_SERIALIZATION_NVP(_breed); \
            ar & BOOST_SERIALIZATION_NVP(_mBreedMeanPerformances); \
            ar & BOOST_SERIALIZATION_NVP(_pmOvulationDetectionProbability); \
    
    IMPLEMENT_SERIALIZE_STD_METHODS(Breed);
    
    FunctionalEnumerations::Genetic::Breed Breed::getEnumerationBreed()
    {
        return _breed;
    }
        
    Breed::Breed(
                FunctionalEnumerations::Genetic::Breed theBreed
                )
    {
        _breed = theBreed;
    }
          
    unsigned int Breed::getBetweenOvulationDuration(FunctionalEnumerations::Reproduction::DairyCowParity parity)
    {
        return FunctionalConstants::Reproduction::MEAN_BETWEEN_OVULATION_DURATION.find(parity)->second;
    }

    float Breed::getOvulationDetectionProbability(FunctionalEnumerations::Reproduction::DetectionMode detectionMode, FunctionalEnumerations::Reproduction::DairyCowParity parity, FunctionalEnumerations::Reproduction::OvulationClass oc)
    {
        return (((_pmOvulationDetectionProbability->find(detectionMode)->second).find(parity)->second).find(oc))->second;       
    }
  
    float Breed::getSensibilityProba(ExchangeInfoStructures::DetectionConditions &dc)
    {
#ifdef _LOG
        assert (dc.lastUnfecundedOvulationNumber>0); // If we are here, the animal has at least 1 Ovulation
#endif // _LOG
        FunctionalEnumerations::Reproduction::OvulationClass oc;
        if (dc.returnInHeatNumber > 0)
        {
            // We need to take into account of the return in heat conditions
            if      (dc.returnInHeatNumber == 1) oc = FunctionalEnumerations::Reproduction::OvulationClass::firstReturn;
            else if (dc.returnInHeatNumber == 2) oc = FunctionalEnumerations::Reproduction::OvulationClass::secundReturn;
            else                                 oc = FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation;
        }
        else
        {
            if      (dc.lastUnfecundedOvulationNumber == 1) oc = FunctionalEnumerations::Reproduction::OvulationClass::firstOvulation;
            else if (dc.lastUnfecundedOvulationNumber == 2) oc = FunctionalEnumerations::Reproduction::OvulationClass::secundOvulation;
            else                                            oc = FunctionalEnumerations::Reproduction::OvulationClass::moreOvulation;
        }
        // Get the good detection percent
        float probaToReturn = getOvulationDetectionProbability(dc.detectionMode, dc.parity, oc);
         
        // slippery floors
        if (dc.slipperyFloor && dc.detectionMode != FunctionalEnumerations::Reproduction::DetectionMode::male)
        {
            probaToReturn *= FunctionalConstants::Reproduction::SLIPPERY_FLOOR_OVULATION_DETECTION_RATIO;
        }
        
        // concomitance ?
        if (dc.simultaneousOvulation == 2)
        {
            probaToReturn *= FunctionalConstants::Reproduction::FACTOR_2_CONCOMITANT_OESTRUS;
        }
        else if (dc.simultaneousOvulation > 2)
        {
            probaToReturn *= FunctionalConstants::Reproduction::FACTOR_3_AND_MORE_CONCOMITANT_OESTRUS;
        }
        
        if (probaToReturn > 1.0f)
        {
            probaToReturn = 1.0f;
        }
        return probaToReturn;
    }

    std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> &Breed::getBreedMeanProductions()
    {
        return _mBreedMeanPerformances;
    }
    
    unsigned int Breed::getPregnancyDuration(FunctionalEnumerations::Reproduction::DairyCowParity parity)
    {
        return FunctionalConstants::Reproduction::MEAN_PREGNANCY_DURATION.find(parity)->second.find(_breed)->second;
    }

    float Breed::getBreedMeanProduction(FunctionalEnumerations::Genetic::PhenotypicCharacter car)
    {
        return getBreedMeanProductions().find(car)->second;
    }
    
    // Give a new Male Genetic value Catalog
    void Breed::getNewMaleGeneticValueCatalog(std::map<FunctionalEnumerations::Genetic::Breed, std::vector<Genetic::GeneticValue>> &maleGeneticValueCatalog, const std::string &maleDataFile, FunctionalEnumerations::Genetic::GeneticStrategy geneticStrategy, std::map<FunctionalEnumerations::Genetic::Breed, Genetic::Breed::Breed*> &mBreeds)
    {
        for (std::map<FunctionalEnumerations::Genetic::Breed,Breed*>::iterator it = mBreeds.begin(); it != mBreeds.end(); it++)
        {
            (it->second)->getMaleGeneticValuesForCatalog(maleGeneticValueCatalog, maleDataFile, geneticStrategy); // Ask to each breed to give it's data
        }
    }

    Genetic::GeneticValue Breed::getACowGeneticValue(FunctionalEnumerations::Genetic::GeneticStrategy geneticStrategy, Tools::Random &random)
    {
        return Genetic::GeneticManagement::generateAVariateAdultGeneticValue(FunctionalConstants::Genetic::NULL_PHENOTYPIC_VALUES, _mBreedMeanPerformances, _breed, geneticStrategy, random);
    }
    
}
}