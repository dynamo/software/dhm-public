#ifndef DataStructures_Breed_h
#define DataStructures_Breed_h

// standard
#include <map>
#include <vector>

// project
#include "../GeneticValue.h"
#include "../../Tools/Random.h"
#include "../../ExchangeInfoStructures/DetectionConditions.h"

namespace Genetic
{
namespace Breed
{
    class Breed
    {
        FunctionalEnumerations::Genetic::Breed _breed;
        
    protected:
       // Typical values of the performance of the breed
        std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> _mBreedMeanPerformances;

    private:
        
    DECLARE_SERIALIZE_STD_METHODS;
    

    protected:
        const std::map<FunctionalEnumerations::Reproduction::DetectionMode, std::map<FunctionalEnumerations::Reproduction::DairyCowParity,std::map<FunctionalEnumerations::Reproduction::OvulationClass, float>>> *_pmOvulationDetectionProbability;         

        virtual void getMaleGeneticValuesForCatalog(std::map<FunctionalEnumerations::Genetic::Breed, std::vector<Genetic::GeneticValue>> &MaleGeneticValueCatalog, const std::string &MaleDataFile, FunctionalEnumerations::Genetic::GeneticStrategy geneticStrategy) = 0;
        virtual void concrete() = 0; // To ensure that it will not be instantiated
    private:
        float getOvulationDetectionProbability(FunctionalEnumerations::Reproduction::DetectionMode detectionMode, FunctionalEnumerations::Reproduction::DairyCowParity parity, FunctionalEnumerations::Reproduction::OvulationClass oc);

    public:
        Breed(){} // serialization constructor
        Breed(FunctionalEnumerations::Genetic::Breed theBreed);
        virtual ~Breed(){}
        FunctionalEnumerations::Genetic::Breed getEnumerationBreed();
        virtual FunctionalEnumerations::Genetic::BreedType getBreedType() = 0;
#ifdef _LOG
        virtual std::string getName() = 0;
#endif // _LOG
        float getSensibilityProba(ExchangeInfoStructures::DetectionConditions &dc);
        virtual float getInseminationSuccessProba(FunctionalEnumerations::Reproduction::DairyCowParity parity, FunctionalEnumerations::Reproduction::TypeInseminationSemen ts)=0;
        unsigned int getPregnancyDuration(FunctionalEnumerations::Reproduction::DairyCowParity parity);
        unsigned int getBetweenOvulationDuration(FunctionalEnumerations::Reproduction::DairyCowParity parity);
        inline virtual bool isDairy() {return false;}
        std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> &getBreedMeanProductions();      
        float getBreedMeanProduction(FunctionalEnumerations::Genetic::PhenotypicCharacter car);
        Genetic::GeneticValue getACowGeneticValue(FunctionalEnumerations::Genetic::GeneticStrategy geneticStrategy, Tools::Random &random);
        virtual void appendAnnualProgress() = 0;       
        static void getNewMaleGeneticValueCatalog(std::map<FunctionalEnumerations::Genetic::Breed, std::vector<Genetic::GeneticValue>> &MaleGeneticValueCatalog, const std::string &MaleDataFile, FunctionalEnumerations::Genetic::GeneticStrategy geneticStrategy, std::map<FunctionalEnumerations::Genetic::Breed, Genetic::Breed::Breed*> &mBreeds);
    };
}
} /* End of namespace Genetic */

#endif // DataStructures_Breed_h
