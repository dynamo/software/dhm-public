#ifndef DataStructures_CharolaiseBreed_h
#define DataStructures_CharolaiseBreed_h

// standard

// project
#include "BeefBreed.h"

namespace Genetic
{
namespace Breed
{
    class Charolaise : public BeefBreed
    {
        
    DECLARE_SERIALIZE_STD_METHODS;
    
    protected:
        void concrete(){}; // To allow instanciation

    public:
        Charolaise() : BeefBreed(FunctionalEnumerations::Genetic::Breed::charolaise){} 
        virtual ~Charolaise(){}
#ifdef _LOG
        std::string getName() {return "Charolaise";}
#endif // _LOG
    };
}
} /* End of namespace Genetic */

#endif // DataStructures_CharolaiseBreed_h
