#include "DairyBreed.h"

// boost

// standard
#include <assert.h> // assert

// project
#include "../../ExchangeInfoStructures/FunctionalConstants.h"
#include "../../ExchangeInfoStructures/TechnicalConstants.h"
#include "../../ExchangeInfoStructures/MilkProductCharacteristic.h"
#include "../../Tools/Random.h"
#include "../../Tools/Tools.h"
#include "../GeneticManagement.h"

BOOST_CLASS_EXPORT(Genetic::Breed::DairyBreed) // Recommended (mandatory if virtual serialization)

namespace Genetic
{
namespace Breed
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Breed); \
            ar & BOOST_SERIALIZATION_NVP(_mRefMilkCurves); \
            ar & BOOST_SERIALIZATION_NVP(_mNormalizedPeakMilkYield); \
            ar & BOOST_SERIALIZATION_NVP(_mPeakMilkDay); \
    
    IMPLEMENT_SERIALIZE_STD_METHODS(DairyBreed);

    DairyBreed::DairyBreed(FunctionalEnumerations::Genetic::Breed theBreed, std::string &technicalDataFolder) : Breed(theBreed)
    {
        // creating the theoric base curves
        initReferenceCurves(technicalDataFolder);
        // ...
    }
    
    
    void DairyBreed::initReferenceCurves(std::string &technicalDataFolder)
    {
        // Ref performances setting
        _mBreedMeanPerformances = FunctionalConstants::Genetic::MEAN_PERFORMANCE_REFERENCE.find(getEnumerationBreed())->second; // Milk performances
        
        // Data file reading
        std::string lactationCurveFileNameForBreed = technicalDataFolder + TechnicalConstants::LACTATION_CURVE_FILE_NAME;
        std::vector<std::vector<std::string>> vLines;
        Tools::readFile(vLines, lactationCurveFileNameForBreed, TechnicalConstants::EXPORT_SEP);

        for (unsigned int currentLactationNumber = 0; currentLactationNumber < 3;  currentLactationNumber++)
        {
        
            std::vector<ExchangeInfoStructures::MilkProductCharacteristic> currentMilkCurve;

            // Only forms, quantities depending on the genetic value of each individual
            float milkPeak = 0.0f;
            unsigned int milkPeakDay = 0;
            for (unsigned int milkStage = 1; milkStage <= TechnicalConstants::MAXIMUM_DURATION_FOR_MILK_CURVE; milkStage++)
            {
                std::vector<std::string> fileLine = vLines[milkStage];
                ExchangeInfoStructures::MilkProductCharacteristic normalizedMpc;             
                normalizedMpc.quantity = atof(Tools::changeChar(fileLine[currentLactationNumber], ',', '.').c_str());
                normalizedMpc.TB = atof(Tools::changeChar(fileLine[3], ',', '.').c_str());
                normalizedMpc.TP = atof(Tools::changeChar(fileLine[4], ',', '.').c_str());
                normalizedMpc.SCC = atof(Tools::changeChar(fileLine[5], ',', '.').c_str());

                currentMilkCurve.push_back(normalizedMpc);
                if (milkPeak < normalizedMpc.quantity)
                {
                    milkPeak = normalizedMpc.quantity;
                    milkPeakDay = milkStage;
                }
            }
                         
            // Store the curve of the lactation rank
            _mRefMilkCurves[currentLactationNumber+1] = currentMilkCurve;
            _mNormalizedPeakMilkYield[currentLactationNumber+1] = milkPeak;
            _mPeakMilkDay[currentLactationNumber+1] = milkPeakDay;
         }
    }
    
    FunctionalEnumerations::Genetic::BreedType DairyBreed::getBreedType()
    {
        return FunctionalEnumerations::Genetic::BreedType::dairy;
    }
    
    void DairyBreed::getMaleGeneticValuesForCatalog(std::map<FunctionalEnumerations::Genetic::Breed, std::vector<Genetic::GeneticValue>> &maleGeneticValueCatalog, const std::string &maleDataFile, FunctionalEnumerations::Genetic::GeneticStrategy geneticStrategy)
    {
        std::vector<Genetic::GeneticValue> cat;
        
        if (maleDataFile != "")
        {
            // We have a file to download, so may be several males/breed

            // Read the file
            std::vector<std::vector<std::string>> vLines;
            Tools::readFile(vLines, maleDataFile.c_str(), TechnicalConstants::EXPORT_SEP);  
            for (std::vector<std::vector<std::string>>::iterator itLine = vLines.begin(); itLine != vLines.end(); itLine++)
            {
                std::vector<std::string> &line = *itLine;
                assert (line.size() == (unsigned int) FunctionalEnumerations::Genetic::PhenotypicCharacter::lastPhenotypicCharacter + 4);
                if (line[0] == Tools::toString(getEnumerationBreed()) and line[1] == Tools::toString(geneticStrategy))
                {
                    Genetic::GeneticValue gv; // Male genetic value
                    //gv.setGeneticStrategy(geneticStrategy);
                    gv.setLGF(atof(Tools::changeChar(line[2], ',', '.').c_str()));
                    unsigned phenoInd = 0;
                    for (unsigned int iCell = 3; iCell < line.size(); iCell++)
                    {

                        FunctionalEnumerations::Genetic::PhenotypicCharacter currentChar = (FunctionalEnumerations::Genetic::PhenotypicCharacter) (phenoInd++); // Carreful, must be in the same order than FunctionalEnumerations::Genetic::PhenotypicCharacter definition
                        float trueVG = atof(Tools::changeChar(line[iCell], ',', '.').c_str());
                        gv.setCharacterGeneticValue(currentChar, CharacterGeneticValue(trueVG, 0.0f, _mBreedMeanPerformances.find(currentChar)->second, currentChar));    

                    }
                    // Now we can calculate the other calculated indexes
                    gv.finalizeCalculatedGeneticValues(geneticStrategy);
                    cat.push_back(gv); // Add in the catalog of the breed
                }
            }
        }
        else
        {
            // We use default values, so only one Male/breed
            Genetic::GeneticValue gv; // Male genetic value
            //gv.setGeneticStrategy(geneticStrategy);
            gv.setLGF(((FunctionalConstants::Genetic::DEFAULT_GENETIC_LGF_MALE_INDEX_CATALOG.find(getEnumerationBreed())->second).find(geneticStrategy))->second);
            const std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> &defaultMaleData = ((FunctionalConstants::Genetic::DEFAULT_MALE_GENETIC_CATALOG.find(getEnumerationBreed())->second).find(geneticStrategy))->second;
            
            for (std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float>::const_iterator it = defaultMaleData.begin(); it != defaultMaleData.end(); it++)
            {
                FunctionalEnumerations::Genetic::PhenotypicCharacter currentChar = it->first;
                gv.setCharacterGeneticValue(currentChar, CharacterGeneticValue(it->second, 0.0f, _mBreedMeanPerformances.find(currentChar)->second, currentChar));    
            }
            // Now we can calculate the other calculated indexes
            gv.finalizeCalculatedGeneticValues(geneticStrategy);
            cat.push_back(gv); // Add in the catalog of the breed
        }
        // Add the catalog of the breed in the global catalog
        maleGeneticValueCatalog[getEnumerationBreed()] = cat;
    }
    
#ifdef _LOG
    void DairyBreed::saveMilkCurvesInFile(const std::string &theResultPath)
    {
        // Save Milk curves informations
        std::string logPath = theResultPath + TechnicalConstants::LOG_DIRECTORY;
        mkdir(logPath.c_str() DIR_RIGHT);  
        std::vector<std::string> linesForTheSaveFile;
        std::string fileName = logPath + getName() + TechnicalConstants::LACTATION_CURVE_FILE_NAME;
        
        // Header
        std::string line =  "milk lactation rank 1 (kg)" + Tools::toString(TechnicalConstants::EXPORT_SEP) + 
                            "milk lactation rank 2 (kg)" + Tools::toString(TechnicalConstants::EXPORT_SEP) + 
                            "milk lactation rank 3 and more (kg)" + Tools::toString(TechnicalConstants::EXPORT_SEP) + 
                            "fat containt (g/kg)" + Tools::toString(TechnicalConstants::EXPORT_SEP) +
                            "protein content (g/kg)";
        
        linesForTheSaveFile.push_back(line);
        for (unsigned int milkStage = 0; milkStage < FunctionalConstants::Lactation::REFERENCE_LACTATION_DURATION; milkStage++)
        {
            line = "";
            for (unsigned int currentLactationNumber = 1; currentLactationNumber <= 3;  currentLactationNumber++)
            {
                float parityFactor = 1.0f;
                if (currentLactationNumber < 3) parityFactor = FunctionalConstants::Lactation::MILK_PRODUCT_FACTOR_DEPENDING_LACTATION_RANK[currentLactationNumber-1];
                ExchangeInfoStructures::MilkProductCharacteristic mpc = getNormalizedStandardMilkProductionOfTheDay(milkStage, currentLactationNumber);
                mpc.quantity *= (getBreedMeanProduction(FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait)) * parityFactor;
                std::string tmp = Tools::toString(mpc.quantity);
                line += Tools::changeChar(tmp,'.', ',') + Tools::toString(TechnicalConstants::EXPORT_SEP);
                if (currentLactationNumber  == 3)
                {
                    mpc.TB *=  getBreedMeanProduction(FunctionalEnumerations::Genetic::PhenotypicCharacter::TB);
                    tmp = Tools::toString(mpc.TB);
                    line += Tools::changeChar(tmp,'.', ',') + Tools::toString(TechnicalConstants::EXPORT_SEP);

                    mpc.TP *=  getBreedMeanProduction(FunctionalEnumerations::Genetic::PhenotypicCharacter::TP);
                    tmp = Tools::toString(mpc.TP);
                    line += Tools::changeChar(tmp,'.', ',');
                }
            }
            linesForTheSaveFile.push_back(line);
        }
        Tools::writeFile(linesForTheSaveFile, fileName, true);        
    }
    
    void DairyBreed::saveSCCCurvesInFile(const std::string &theResultPath)
    {
        // Save Milk curves informations
        std::string logPath = theResultPath +TechnicalConstants::LOG_DIRECTORY;
        mkdir(logPath.c_str() DIR_RIGHT);  
        std::vector<std::string> linesForTheSaveFile;
        std::string fileName = logPath + TechnicalConstants::SCC_CURVE_FILE_NAME;
        
        // Header
        std::string line =  "lactation rank 1" + Tools::toString(TechnicalConstants::EXPORT_SEP) + 
                            "lactation rank 2" + Tools::toString(TechnicalConstants::EXPORT_SEP) + 
                            "lactation rank 3 and more";
        
        linesForTheSaveFile.push_back(line);
        for (unsigned int milkStage = 0; milkStage < FunctionalConstants::Lactation::REFERENCE_LACTATION_DURATION; milkStage++)
        {
            line = "";
            for (unsigned int currentLactationNumber = 1; currentLactationNumber <= 3;  currentLactationNumber++)
            {
                if (currentLactationNumber > 1)
                {
                    line += Tools::toString(TechnicalConstants::EXPORT_SEP);
                }
                ExchangeInfoStructures::MilkProductCharacteristic mpc = getNormalizedStandardMilkProductionOfTheDay(milkStage, currentLactationNumber);
                mpc.SCC *=  FunctionalConstants::Lactation::MILK_SCC_WITHOUT_INFECTION_LEVEL[currentLactationNumber-1];
                std::string strSCC = Tools::toString(mpc.SCC);
                line += Tools::changeChar(strSCC,'.', ',');
            }
            linesForTheSaveFile.push_back(line);
        }
        Tools::writeFile(linesForTheSaveFile, fileName, true);        
    }
#endif // _LOG 

    ExchangeInfoStructures::MilkProductCharacteristic DairyBreed::getNormalizedStandardMilkProductionOfTheDay(unsigned int day, unsigned int lactationNumber)
    {
        ExchangeInfoStructures::MilkProductCharacteristic res;
#ifdef _LOG
        assert (lactationNumber > 0 and day < TechnicalConstants::MAXIMUM_DURATION_FOR_MILK_CURVE); // At least 1 lactation
#endif // _LOG
        if (lactationNumber > 3) lactationNumber = 3; // at the most 3
//        if (day < TechnicalConstants::MAXIMUM_DURATION_FOR_MILK_CURVE) 
//        {
            res = _mRefMilkCurves[lactationNumber].at(day);
//        }
//        else
//        {
//            res = _mRefMilkCurves[lactationNumber].at(TechnicalConstants::MAXIMUM_DURATION_FOR_MILK_CURVE-1); // Last of the curve
//            // Maintaining the gradient
//            float beforeLastQuantity = _mRefMilkCurves[lactationNumber].at(TechnicalConstants::MAXIMUM_DURATION_FOR_MILK_CURVE-2).quantity;
//            std::cout << "Before : " << res.quantity;
//            res.quantity -= beforeLastQuantity * (day - TechnicalConstants::MAXIMUM_DURATION_FOR_MILK_CURVE + 1);
//            std::cout << "after =  " << res.quantity << std::endl;
//        }
        return res;
    }

    std::map<unsigned int, std::vector<ExchangeInfoStructures::MilkProductCharacteristic>> &DairyBreed::getStandardLactationCurves(std::map<unsigned int, float> &normalizedPeakMilkYield, std::map<unsigned int, unsigned int> &peakMilkDay)
    {
        normalizedPeakMilkYield = _mNormalizedPeakMilkYield;
        peakMilkDay = _mPeakMilkDay;
        return _mRefMilkCurves;
    }
        
    void DairyBreed::appendAnnualProgress()
    {  
        const std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> &values = FunctionalConstants::Genetic::ANNUAL_GENETIC_PROGRESS.find(getEnumerationBreed())->second;
        for (auto itPC : values)
        {
            float &value = _mBreedMeanPerformances.find(itPC.first)->second;
            value += itPC.second;
        }
    }
}
}