#ifndef DataStructures_DairyBreed_h
#define DataStructures_DairyBreed_h

// standard
#include <map>
#include <vector>

// project
#include "../../Tools/Random.h"

#include "Breed.h"
#include "../../ExchangeInfoStructures/MilkProductCharacteristic.h"

namespace Genetic
{
namespace Breed
{
    class DairyBreed : public Breed
    {
        std::map<unsigned int, std::vector<ExchangeInfoStructures::MilkProductCharacteristic>> _mRefMilkCurves; // Q, TB, TP, SCC
        std::map<unsigned int, float> _mNormalizedPeakMilkYield;
        std::map<unsigned int, unsigned int> _mPeakMilkDay;

    DECLARE_SERIALIZE_STD_METHODS;
    
    protected:
        void getMaleGeneticValuesForCatalog(std::map<FunctionalEnumerations::Genetic::Breed, std::vector<Genetic::GeneticValue>> &MaleGeneticValueCatalog, const std::string &MaleDataFile, FunctionalEnumerations::Genetic::GeneticStrategy geneticStrategy) override;
    
    private:
        void initReferenceCurves(std::string &technicalDataFolder);
        
    public:
        DairyBreed(){} // serialization constructor
        DairyBreed(FunctionalEnumerations::Genetic::Breed theBreed, std::string &executableFolder);
        virtual ~DairyBreed(){}

        // reproduction
        float getSensibilityProba(ExchangeInfoStructures::DetectionConditions &dc);
        virtual const std::map<FunctionalEnumerations::Reproduction::DairyCowParity,std::vector<float>> &getPostPartumOvulationDurationMatrix()=0;
        
        bool isDairy() override {return true;}

        FunctionalEnumerations::Genetic::BreedType getBreedType();
        
        // Lactation
        ExchangeInfoStructures::MilkProductCharacteristic getNormalizedStandardMilkProductionOfTheDay(unsigned int day, unsigned int lactationNumber);
        std::map<unsigned int, std::vector<ExchangeInfoStructures::MilkProductCharacteristic>> &getStandardLactationCurves(std::map<unsigned int, float> &normalizedPeakMilkYield, std::map<unsigned int, unsigned int> &peakMilkDay);
        
#ifdef _LOG
        void saveMilkCurvesInFile(const std::string &theResultPath);
        void saveSCCCurvesInFile(const std::string &theResultPath);
#endif // _LOG 
        
        void appendAnnualProgress();

    };
}
} /* End of namespace Genetic */
#endif // DataStructures_DairyBreed_h
