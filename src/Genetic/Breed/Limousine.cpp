#include "Limousine.h"

// project

BOOST_CLASS_EXPORT(Genetic::Breed::Limousine) // Recommended (mandatory if virtual serialization)

namespace Genetic
{    
namespace Breed
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(BeefBreed); \
    
    IMPLEMENT_SERIALIZE_STD_METHODS(Limousine);
    
}    
} /* End of namespace DataStructures */
