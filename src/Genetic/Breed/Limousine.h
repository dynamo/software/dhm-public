#ifndef DataStructures_LimousineBreed_h
#define DataStructures_LimousineBreed_h

// standard

// project
#include "BeefBreed.h"

namespace Genetic
{
namespace Breed
{
    class Limousine : public BeefBreed
    {
        
    DECLARE_SERIALIZE_STD_METHODS;
    
    protected:
        void concrete(){}; // To allow instanciation

    public:
        Limousine() : BeefBreed(FunctionalEnumerations::Genetic::Breed::limousine){}
        virtual ~Limousine(){}
#ifdef _LOG
        std::string getName() {return "Limousine";}
#endif // _LOG
    };
}
} /* End of namespace Genetic */
#endif // DataStructures_LimousineBreed_h
