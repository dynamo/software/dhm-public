#include "Montbeliarde.h"

// standard

// project
#include "../../Tools/Random.h"
#include "../../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(Genetic::Breed::Montbeliarde) // Recommended (mandatory if virtual serialization)

namespace Genetic
{
namespace Breed
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyBreed); \
    
    IMPLEMENT_SERIALIZE_STD_METHODS(Montbeliarde);

    Montbeliarde::Montbeliarde(std::string &technicalDataFolder) : DairyBreed   (
                                                   FunctionalEnumerations::Genetic::Breed::montbeliarde, technicalDataFolder
                                                )
    {
        _pmOvulationDetectionProbability = &FunctionalConstants::Reproduction::MONTBELIARDE_OVULATION_DETECTION_PROBA;
    }
  
    float Montbeliarde::getInseminationSuccessProba(FunctionalEnumerations::Reproduction::DairyCowParity parity, FunctionalEnumerations::Reproduction::TypeInseminationSemen ts)
    {
        return (FunctionalConstants::Reproduction::MONTBELIARDE_INSEMINATION_SUCCESS_PROBA.find(ts)->second).find(parity)->second;
    }
    
    const std::map<FunctionalEnumerations::Reproduction::DairyCowParity,std::vector<float>> &Montbeliarde::getPostPartumOvulationDurationMatrix()
    {
        return FunctionalConstants::Reproduction::MONTBELIARDE_CYCLE_PROBA;
    }   
}
}
