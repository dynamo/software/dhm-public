#ifndef DataStructures_MontbeliardeBreed_h
#define DataStructures_MontbeliardeBreed_h

// Project
#include "DairyBreed.h"

namespace Genetic
{
namespace Breed
{
    class Montbeliarde : public DairyBreed
    {

    DECLARE_SERIALIZE_STD_METHODS;
    
    private:
        Montbeliarde(){}
    protected:
        void concrete(){}; // To allow instanciation

    public:
        Montbeliarde(std::string &technicalDataFolder);
        virtual ~Montbeliarde(){}
#ifdef _LOG
        std::string getName() {return "Montbeliarde";}
#endif // _LOG
        
        float getInseminationSuccessProba(FunctionalEnumerations::Reproduction::DairyCowParity parity, FunctionalEnumerations::Reproduction::TypeInseminationSemen ts);
        const std::map<FunctionalEnumerations::Reproduction::DairyCowParity,std::vector<float>> &getPostPartumOvulationDurationMatrix();
    };
}
} /* End of namespace DataStructures */
#endif // DataStructures_MontbeliardeBreed_h
