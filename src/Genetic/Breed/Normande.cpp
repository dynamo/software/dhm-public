#include "Normande.h"

// standard

// project
#include "../../Tools/Random.h"
#include "../../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(Genetic::Breed::Normande) // Recommended (mandatory if virtual serialization)

namespace Genetic
{
namespace Breed
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyBreed); \
    
    IMPLEMENT_SERIALIZE_STD_METHODS(Normande);

    Normande::Normande(std::string &technicalDataFolder) : DairyBreed   (
                                            FunctionalEnumerations::Genetic::Breed::normande , technicalDataFolder
                                        )
    {
        _pmOvulationDetectionProbability = &FunctionalConstants::Reproduction::NORMANDE_OVULATION_DETECTION_PROBA;
        
    }
        
    float Normande::getInseminationSuccessProba(FunctionalEnumerations::Reproduction::DairyCowParity parity, FunctionalEnumerations::Reproduction::TypeInseminationSemen ts)
    {
        return (FunctionalConstants::Reproduction::NORMANDE_INSEMINATION_SUCCESS_PROBA.find(ts)->second).find(parity)->second;
    }
    
    const std::map<FunctionalEnumerations::Reproduction::DairyCowParity,std::vector<float>> &Normande::getPostPartumOvulationDurationMatrix() //, bool &interrupted)
    {
        return FunctionalConstants::Reproduction::NORMANDE_CYCLE_PROBA;
    }   
}
}