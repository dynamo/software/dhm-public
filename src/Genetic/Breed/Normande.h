#ifndef DataStructures_NormandeBreed_h
#define DataStructures_NormandeBreed_h

// Project
#include "DairyBreed.h"

namespace Genetic
{
namespace Breed
{
    class Normande : public DairyBreed
    {

    DECLARE_SERIALIZE_STD_METHODS;
    
    private:
        Normande(){}
    protected:
        void concrete(){}; // To allow instanciation

    public:
        Normande(std::string &technicalDataFolder);
        virtual ~Normande(){}
#ifdef _LOG
        std::string getName() {return "Normande";}
#endif // _LOG

        float getInseminationSuccessProba(FunctionalEnumerations::Reproduction::DairyCowParity parity, FunctionalEnumerations::Reproduction::TypeInseminationSemen ts);
        const std::map<FunctionalEnumerations::Reproduction::DairyCowParity,std::vector<float>> &getPostPartumOvulationDurationMatrix();
    };
}
} /* End of namespace DataStructures */
#endif // DataStructures_NormandeBreed_h
