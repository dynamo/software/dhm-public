#include "PrimHolstein.h"

// standard

// project
#include "../../Tools/Random.h"
#include "../../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(Genetic::Breed::PrimHolstein) // Recommended (mandatory if virtual serialization)

namespace Genetic
{
namespace Breed
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyBreed); \
    
    IMPLEMENT_SERIALIZE_STD_METHODS(PrimHolstein);

    PrimHolstein::PrimHolstein(std::string &technicalDataFolder) : DairyBreed   (
                                                    FunctionalEnumerations::Genetic::Breed::primHolstein, technicalDataFolder
                                                )
    {
        _pmOvulationDetectionProbability = &FunctionalConstants::Reproduction::PRIM_HOLSTEIN_OVULATION_DETECTION_PROBA;
    }
      
    float PrimHolstein::getInseminationSuccessProba(FunctionalEnumerations::Reproduction::DairyCowParity parity, FunctionalEnumerations::Reproduction::TypeInseminationSemen ts)
    {
        return (FunctionalConstants::Reproduction::PRIM_HOLSTEIN_INSEMINATION_SUCCESS_PROBA.find(ts)->second).find(parity)->second;
    }
    
    const std::map<FunctionalEnumerations::Reproduction::DairyCowParity,std::vector<float>> &PrimHolstein::getPostPartumOvulationDurationMatrix() //, bool &interrupted)
    {
        return FunctionalConstants::Reproduction::PRIM_HOLSTEIN_CYCLE_PROBA;
    }   
}
}
