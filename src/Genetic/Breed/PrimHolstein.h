#ifndef DataStructures_PrimHolsteinBreed_h
#define DataStructures_PrimHolsteinBreed_h

// project
#include "DairyBreed.h"

namespace Genetic
{
namespace Breed
{
    class PrimHolstein : public DairyBreed
    {

    DECLARE_SERIALIZE_STD_METHODS;
    
    private:
        PrimHolstein(){}
    protected:
        void concrete(){}; // To allow instanciation
    public:
        PrimHolstein(std::string &technicalDataFolder);
        virtual ~PrimHolstein(){}
#ifdef _LOG
        std::string getName() {return "PrimHolstein";}
#endif // _LOG
        
        float getInseminationSuccessProba(FunctionalEnumerations::Reproduction::DairyCowParity parity, FunctionalEnumerations::Reproduction::TypeInseminationSemen ts);
        const std::map<FunctionalEnumerations::Reproduction::DairyCowParity,std::vector<float>> &getPostPartumOvulationDurationMatrix();
    };
}
} /* End of namespace DataStructures */
#endif // DataStructures_PrimHolsteinBreed_h
