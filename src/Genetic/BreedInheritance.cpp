#include "BreedInheritance.h"
#include "GeneticManagement.h"
#include "./Breed/Breed.h"

BOOST_CLASS_EXPORT(Genetic::BreedInheritance) // Recommended (mandatory if virtual serialization)

namespace Genetic
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(_mInheritsBreeds); \
    ar & BOOST_SERIALIZATION_NVP(_mBreedMeanProductions); \
    ar & BOOST_SERIALIZATION_NVP(_heterosisFactor); \

    IMPLEMENT_SERIALIZE_STD_METHODS(BreedInheritance);
 
    BreedInheritance::BreedInheritance (Genetic::Breed::Breed* pBreed)
    {
        _mInheritsBreeds[pBreed] = 1.0f; // 100%
        _mBreedMeanProductions = pBreed->getBreedMeanProductions(); // use the breed performances
    }
    
    std::map<Genetic::Breed::Breed*, float> &BreedInheritance::getBreeds()
    {
        return _mInheritsBreeds;
    }
    
    bool BreedInheritance::isPureBreed()
    {
        return _mInheritsBreeds.size() == 1;
    }
    
    bool BreedInheritance::isPureBreed(Genetic::Breed::Breed* &pBreed)
    {
        bool res = isPureBreed();
        if (res) pBreed = (_mInheritsBreeds.begin()->first);
        return res;
    }
    
    bool BreedInheritance::isPureDairy()
    {
        bool res = true;
        for (std::map<Genetic::Breed::Breed*, float>::iterator it = _mInheritsBreeds.begin(); it != _mInheritsBreeds.end() and res; it++)
        {
            res = (it->first)->isDairy();
        }
        return res;
    }
    
    bool BreedInheritance::isBeefCrossed()
    {
        return !isPureDairy();
    }
    
    bool BreedInheritance::isMilkCrossed()
    {
        return isPureDairy() and not isPureBreed();
    }

    void BreedInheritance::setHeterosisFactor(float heterosisFactor)
    {
        _heterosisFactor = heterosisFactor;
    }
    
    float BreedInheritance::getHeterosisFactor()
    {
        return _heterosisFactor;
    }

    void BreedInheritance::setSpecificBreedMeanProduction(FunctionalEnumerations::Genetic::PhenotypicCharacter pc, float value)
    {
        _mBreedMeanProductions[pc] = value;
    }

    void BreedInheritance::setBreedMeanProductions(std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> &pProd)
    {
        _mBreedMeanProductions = pProd;
    }

    float BreedInheritance::getBreedMeanProduction(FunctionalEnumerations::Genetic::PhenotypicCharacter pc)
    {
        return _mBreedMeanProductions.find(pc)->second;
    }
    
    // Return a float value regarding the breed inheritance
    float BreedInheritance::getCrossedValue(const std::map<FunctionalEnumerations::Genetic::Breed, float> &originBreedMatrix)
    {
        float res = 0.0f;
        for (std::map<Genetic::Breed::Breed*, float>::iterator itInheritance = _mInheritsBreeds.begin(); itInheritance != _mInheritsBreeds.end(); itInheritance++)
        {
            std::map<FunctionalEnumerations::Genetic::Breed, float>::const_iterator itOrigin = originBreedMatrix.find((itInheritance->first)->getEnumerationBreed());
            if (itOrigin != originBreedMatrix.end())
            {
                // This breed is to consider from the origin matrix
                res += itOrigin->second * itInheritance->second;
            }
        }
        return res;
    }
}