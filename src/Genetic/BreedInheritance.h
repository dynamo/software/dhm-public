#ifndef DataStructures_BreedInheritance_h
#define DataStructures_BreedInheritance_h

// boost

// project
#include "../Tools/Serialization.h"
#include "../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "./Breed/Breed.h"

namespace Genetic
{
    class BreedInheritance
    {
        std::map<Genetic::Breed::Breed*, float> _mInheritsBreeds; // The ratio by breed
        std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> _mBreedMeanProductions; // own performances
        float _heterosisFactor = 0.0f;
        
    private:

        DECLARE_SERIALIZE_STD_METHODS;
        
    public: 
        BreedInheritance(){} // For serialization, but not only
        BreedInheritance (Genetic::Breed::Breed* pBreed);
        virtual ~BreedInheritance(){} // For serialization
        std::map<Genetic::Breed::Breed*, float> &getBreeds();
        bool isPureBreed();
        bool isPureBreed(Genetic::Breed::Breed* &pBreed);
        bool isPureDairy();
        bool isBeefCrossed();
        bool isMilkCrossed();
        void setHeterosisFactor(float heterosisFactor);
        float getHeterosisFactor();
        void setSpecificBreedMeanProduction(FunctionalEnumerations::Genetic::PhenotypicCharacter pc, float value);
        void setBreedMeanProductions(std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> &pProd);  
        float getBreedMeanProduction(FunctionalEnumerations::Genetic::PhenotypicCharacter pc);
        float getCrossedValue(const std::map<FunctionalEnumerations::Genetic::Breed, float> &originBreedMatrix);

    };
} /* End of namespace DataStructures */
#endif // DataStructures_BreedInheritancee_h
