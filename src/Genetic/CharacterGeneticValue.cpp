#include "CharacterGeneticValue.h"

// Project
#include "../Tools/Tools.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(Genetic::CharacterGeneticValue) // Recommended (mandatory if virtual serialization)

namespace Genetic
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
            ar & BOOST_SERIALIZATION_NVP(_IsAboutPhenotypicCharacter); \
            ar & BOOST_SERIALIZATION_NVP(_herdTrueVG); \
            ar & BOOST_SERIALIZATION_NVP(_herdPhenotypeReference); \
            ar & BOOST_SERIALIZATION_NVP(_trueVG); \
            ar & BOOST_SERIALIZATION_NVP(_correctedPerformance); \
    
    IMPLEMENT_SERIALIZE_STD_METHODS(CharacterGeneticValue);
   
    float CharacterGeneticValue::getTrueVG()
    {
        return _trueVG;
    }
    
    float CharacterGeneticValue::getCorrectedPerformance()
    {
        return _correctedPerformance;
    }
            
    CharacterGeneticValue::CharacterGeneticValue(CharacterGeneticValue const& other) // copy constructor 
    {
        _IsAboutPhenotypicCharacter = other._IsAboutPhenotypicCharacter;
        _herdTrueVG = other._herdTrueVG;
        _herdPhenotypeReference = other._herdPhenotypeReference;
        _trueVG = other._trueVG;
        _correctedPerformance = other._correctedPerformance;
    }    

    CharacterGeneticValue::CharacterGeneticValue(float trueVG, float herdTrueVG, float herdReference, FunctionalEnumerations::Genetic::PhenotypicCharacter currentChar)
    {
        _IsAboutPhenotypicCharacter = currentChar;
        _herdTrueVG = herdTrueVG;
        _herdPhenotypeReference = herdReference;
        _trueVG = trueVG;
        float sdFactor = FunctionalConstants::Genetic::SD_FACTOR.find(_IsAboutPhenotypicCharacter)->second;
        if (sdFactor == 0.0f)
        {
            // Value to manage
            _correctedPerformance = herdReference + (trueVG - herdTrueVG)/FunctionalConstants::Genetic::H2.find(_IsAboutPhenotypicCharacter)->second;
        }
        else if (sdFactor > 0.0f)
        {
            // Increasing
            if (trueVG < 0.0f)
            {
                _correctedPerformance =  1 / (1 + (sdFactor - 1) * trueVG / (-FunctionalConstants::Genetic::MAX_STANDARD_DEVIATION));
            }
            else // (trueVG >= 0.0f)
            {
                _correctedPerformance = 1 + (sdFactor - 1) * trueVG / FunctionalConstants::Genetic::MAX_STANDARD_DEVIATION;
            }
        }
        else // (sdFactor <= 0.0f)
        {
            // Decreasing
            sdFactor = -sdFactor;
            if (trueVG < 0.0f)
            {
                _correctedPerformance = 1 + (sdFactor - 1) * trueVG / (-FunctionalConstants::Genetic::MAX_STANDARD_DEVIATION);
            }
            else // (trueVG >= 0.0f)
            {
                _correctedPerformance =  1 / (1 + (sdFactor - 1) * trueVG / FunctionalConstants::Genetic::MAX_STANDARD_DEVIATION);
            }
        }
    }
       
    void CharacterGeneticValue::progress(float correctedPerformanceProgress)
    {
        // Set the adult level
        _correctedPerformance += correctedPerformanceProgress;
        
        // Set the true VG
        _trueVG = ((_correctedPerformance - _herdPhenotypeReference) * FunctionalConstants::Genetic::H2.find(_IsAboutPhenotypicCharacter)->second) + _herdTrueVG;
    }      
}
