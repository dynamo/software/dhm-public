#ifndef CharacterGeneticValue_h
#define CharacterGeneticValue_h

// standard
#include <string>

// project
#include "../Tools/Serialization.h"
#include "../ExchangeInfoStructures/FunctionalEnumerations.h"

namespace Genetic
{
    struct CharacterGeneticValue
    {
    private:
        FunctionalEnumerations::Genetic::PhenotypicCharacter _IsAboutPhenotypicCharacter;
        float _herdTrueVG;
        float _herdPhenotypeReference;
        float _trueVG;
        float _correctedPerformance;
    private:

        DECLARE_SERIALIZE_STD_METHODS;
    protected:
    public:
        CharacterGeneticValue(){}; // serialization constructor     
        CharacterGeneticValue(CharacterGeneticValue const& other); // copy constructor     
        CharacterGeneticValue(float trueVG, float herdTrueVG, float herdReference, FunctionalEnumerations::Genetic::PhenotypicCharacter currentChar);         
        virtual ~CharacterGeneticValue(){};
        float getTrueVG();
        float getCorrectedPerformance();
        void progress(float correctedPerformanceProgress);
    };
} /* End of namespace Lactation */
#endif // CharacterGeneticValue_h
