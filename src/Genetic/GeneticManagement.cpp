#include "GeneticManagement.h"

// Standard
#include <assert.h>

// Project
#include "../ExchangeInfoStructures/TechnicalConstants.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"
#include "BreedInheritance.h"
#include "../Tools/Tools.h"
#include "./GeneticValue.h"
#include "./Breed/PrimHolstein.h"
#include "./Breed/Montbeliarde.h"
#include "./Breed/Normande.h"
#include "./Breed/BlancBleueBelge.h"
#include "./Breed/Charolaise.h"
#include "./Breed/Limousine.h"

namespace Genetic
{
    void GeneticManagement::init()
    {
        if (s_geneticVarianceContravarianceMatrix.size() == 0)
        {
            // Matrix
            std::vector<FunctionalEnumerations::Genetic::Breed> breeds = {  FunctionalEnumerations::Genetic::Breed::montbeliarde,
                                                                         FunctionalEnumerations::Genetic::Breed::normande,
                                                                         FunctionalEnumerations::Genetic::Breed::primHolstein};
            for (std::vector<FunctionalEnumerations::Genetic::Breed>::iterator it = breeds.begin(); it != breeds.end(); it++)
            {
                s_geneticVarianceContravarianceMatrix[*it] = Tools::getVarianceCovarianceMatrix<>(FunctionalConstants::Genetic::SD.find(*it)->second, FunctionalConstants::Genetic::GENETIC_CORRELATION);
                s_geneticVarianceCovarianceMatrixForMeioseRandom[*it] = Tools::getVarianceCovarianceMatrix<>(FunctionalConstants::Genetic::SD_FOR_MEIOSE_RANDOM.find(*it)->second, FunctionalConstants::Genetic::GENETIC_CORRELATION);
            }
        }
    }
    
    void GeneticManagement::getNewBreeds(std::map<FunctionalEnumerations::Genetic::Breed, Genetic::Breed::Breed*> &pCollection)
    {
        std::string technicalDataFolder = Tools::getExecutableFolder() + TechnicalConstants::RELATIVE_TECHNICAL_DATA_FOLDER;

        Genetic::Breed::Breed* pBreed = new Genetic::Breed::BlancBleueBelge();  pCollection[pBreed->getEnumerationBreed()] = pBreed;
        pBreed = new Genetic::Breed::Charolaise();                              pCollection[pBreed->getEnumerationBreed()] = pBreed;
        pBreed = new Genetic::Breed::Limousine();                               pCollection[pBreed->getEnumerationBreed()] = pBreed;
        pBreed = new Genetic::Breed::Montbeliarde(technicalDataFolder);            pCollection[pBreed->getEnumerationBreed()] = pBreed;
        pBreed = new Genetic::Breed::Normande(technicalDataFolder);                pCollection[pBreed->getEnumerationBreed()] = pBreed;
        pBreed = new Genetic::Breed::PrimHolstein(technicalDataFolder);            pCollection[pBreed->getEnumerationBreed()] = pBreed;
    }
    
#ifdef _LOG
    void GeneticManagement::saveAllMilkCurvesInFiles(const std::string &theResultPath)
    {
        std::string technicalDataFolder = Tools::getExecutableFolder() + TechnicalConstants::RELATIVE_TECHNICAL_DATA_FOLDER;
        Genetic::Breed::Montbeliarde m(technicalDataFolder);     m.saveMilkCurvesInFile(theResultPath);
        Genetic::Breed::Normande n(technicalDataFolder);         n.saveMilkCurvesInFile(theResultPath);
        Genetic::Breed::PrimHolstein p(technicalDataFolder);     p.saveMilkCurvesInFile(theResultPath);
                                                                 p.saveSCCCurvesInFile(theResultPath); // The scc curves are the same for each breed
    }
#endif // _LOG 

    // For bought cows (always dairy)
    Genetic::GeneticValue GeneticManagement::generateAVariateAdultGeneticValue(const std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> &herdTrueGeneticValue, std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> &herdPhenotypeReference, FunctionalEnumerations::Genetic::Breed theBreed, FunctionalEnumerations::Genetic::GeneticStrategy geneticStrategy, Tools::Random &random)
    {
        Genetic::GeneticValue res;
        //res.setGeneticStrategy(geneticStrategy);

        // Phenotypic characters with variability and correlation
        // ------------------------------------------------------
        std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> characterGeneticValues;
        Tools::getMultivariateNormalRandomValue<>(herdTrueGeneticValue, s_geneticVarianceContravarianceMatrix.find(theBreed)->second, characterGeneticValues, random);
        for (std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float>::iterator itGV =  characterGeneticValues.begin(); itGV != characterGeneticValues.end(); itGV++)
        {
            FunctionalEnumerations::Genetic::PhenotypicCharacter currentChar = itGV->first;
            float trueVG = itGV->second;
            res.setCharacterGeneticValue(currentChar, CharacterGeneticValue(trueVG, herdTrueGeneticValue.find(currentChar)->second, herdPhenotypeReference.find(currentChar)->second, currentChar));
        }

        // Now we can calculate the other indexes
        res.finalizeCalculatedGeneticValues(geneticStrategy);
       
        return res;    
    }

    void GeneticManagement::setGeneticValueToCalf(I_GeneticDairyCow* pCalf, I_GeneticDairyCow* pDam, Genetic::Breed::Breed* pSireBreed, FunctionalEnumerations::Genetic::GeneticStrategy geneticStrategy, Tools::Random &random)
    {
        Genetic::GeneticValue &calfGE = pCalf->getGeneticValue();
        Genetic::GeneticValue &damGE = pDam->getGeneticValue();
        Genetic::GeneticValue &sireGE = pDam->getSirGeneticValueOfTheLastFertilyInsemination();

        std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, Genetic::CharacterGeneticValue> &damValues = damGE.getCharacterGeneticValues();
        std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, Genetic::CharacterGeneticValue> &sireValues = sireGE.getCharacterGeneticValues();

        // Get the meiose random with specific variance-covariance matrix (only for dairy breed sir)
        std::map<::FunctionalEnumerations::Genetic::PhenotypicCharacter, float> meioseRandom;
        if (pSireBreed->isDairy())            
        {
            Tools::getMultivariateNormalRandomValue<>(FunctionalConstants::Genetic::NULL_PHENOTYPIC_VALUES, s_geneticVarianceCovarianceMatrixForMeioseRandom.find(pSireBreed->getEnumerationBreed())->second, meioseRandom, random); // with mean to 0
        }
        else
        {
            // Beef male
            meioseRandom = FunctionalConstants::Genetic::NULL_PHENOTYPIC_VALUES;
        }

        //calfGE.setGeneticStrategy(geneticStrategy);
        calfGE.setLGF(damGE.getLGF(), sireGE.getLGF());
        for (std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, Genetic::CharacterGeneticValue>::iterator itDam = damValues.begin(); itDam != damValues.end(); itDam++)
        {
            FunctionalEnumerations::Genetic::PhenotypicCharacter currentChar = itDam->first;
            std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, Genetic::CharacterGeneticValue>::iterator itSire = sireValues.find(currentChar);

            float calfTrueVG = (itDam->second.getTrueVG() + itSire->second.getTrueVG())/2 // base
                                + meioseRandom.find(currentChar)->second
                                + FunctionalConstants::Genetic::HETEROSIS.find(currentChar)->second * pCalf->getCowBreedInheritance()->getHeterosisFactor(); // Heterosis


            calfGE.setCharacterGeneticValue(currentChar, Genetic::CharacterGeneticValue(calfTrueVG, 0.0f, pCalf->getCowBreedInheritance()->getBreedMeanProduction(currentChar), currentChar));
        }
        calfGE.finalizeCalculatedGeneticValues(geneticStrategy); // For calculated values
    }

    void GeneticManagement::calculateTheoricPregnancyDurations(Genetic::BreedInheritance* pInherit, std::map<FunctionalEnumerations::Reproduction::DairyCowParity, unsigned int> &data)
    { 
        // We give the mean of all the genetic breed pregnancy theoric durations
        for (unsigned int parity = FunctionalEnumerations::Reproduction::DairyCowParity::heifer; parity <= FunctionalEnumerations::Reproduction::DairyCowParity::multipare; parity++)
        {
            float meanPregnancyDuration = pInherit->getCrossedValue(FunctionalConstants::Reproduction::MEAN_PREGNANCY_DURATION.find((FunctionalEnumerations::Reproduction::DairyCowParity)parity)->second);
            data[(FunctionalEnumerations::Reproduction::DairyCowParity)parity] = meanPregnancyDuration;
        }
    }

    void GeneticManagement::calculateTheoricBetweenOvulationDurations(Genetic::BreedInheritance* pInherit, std::map<FunctionalEnumerations::Reproduction::DairyCowParity, unsigned int> &data, Tools::Random &random)
    {
        // We give the mean of all the breed between ovulation durations
        for (unsigned int parity = FunctionalEnumerations::Reproduction::DairyCowParity::heifer; parity <= FunctionalEnumerations::Reproduction::DairyCowParity::multipare; parity++)
        {
            float meanBetweenOvulationDuration = 0;
            for (auto gi : pInherit->getBreeds())
            {
                meanBetweenOvulationDuration += (gi.first)->getBetweenOvulationDuration((FunctionalEnumerations::Reproduction::DairyCowParity)parity) * gi.second;
            }
            data[(FunctionalEnumerations::Reproduction::DairyCowParity)parity] = meanBetweenOvulationDuration + random.ran_gaussian(0.0f, FunctionalConstants::Reproduction::STANDARD_DEVIATION_INTERVAL_BETWEEN_OVULATION); // Loi normale (moyenne, ecart type);
        }
    }
    
    bool GeneticManagement::isOvulationDetected(Genetic::BreedInheritance* pInherit, ExchangeInfoStructures::DetectionConditions &dc, Tools::Random &random)
    {
        return (random.ran_bernoulli(getOvulationDetectionProba(pInherit, dc)));
    }
   
    float GeneticManagement::getOvulationDetectionProba(Genetic::BreedInheritance* pInherit, ExchangeInfoStructures::DetectionConditions &dc)
    {
        float meanProba = 0.0f;
        for (auto gi : pInherit->getBreeds())
        {
            meanProba += (gi.first)->getSensibilityProba(dc) * gi.second;
        }
        
        // milk production (2.2.1.1.2.3.3)
        float milkFactor = 1-(dc.dayMilkProduction / 100.0f);
                
        if (milkFactor < 0) milkFactor = 0.0f;
        return meanProba * milkFactor * dc.lamenessDetectionFactor;
    }
   
    float GeneticManagement::getBasisInseminationSuccessProbability(Genetic::BreedInheritance* pInherit, FunctionalEnumerations::Reproduction::DairyCowParity parity, FunctionalEnumerations::Reproduction::TypeInseminationSemen ts, float farmFertilityFactor, float geneticFertilityPerformance, float medicFertilityDelta)
    {
        float successProba = 0.0f;
        for (auto gi : pInherit->getBreeds())
        {
            successProba += (gi.first)->getInseminationSuccessProba(parity, ts) * gi.second;
        }
        float unsuccessProba = (1.0f - successProba)/(farmFertilityFactor * geneticFertilityPerformance);
        if (unsuccessProba < medicFertilityDelta)
        {
            successProba = 1.0f;
        }
        else
        {
            successProba = (1.0f - (unsuccessProba - medicFertilityDelta));    
        }
        return successProba;
    }
    
    void GeneticManagement::setFarrowedBreeds(Genetic::BreedInheritance *pTheNewEmptyBreeds, std::map<Genetic::Breed::Breed*, float> &damBreeds, Genetic::Breed::Breed* pSireBreed)
    {
        // Sir genetics
        (pTheNewEmptyBreeds->getBreeds())[pSireBreed] = FunctionalConstants::Genetic::BREED_RATIO_WHEN_CROSS_BREEDING;

        // Dam genetics
        float ratioDam = 1 - FunctionalConstants::Genetic::BREED_RATIO_WHEN_CROSS_BREEDING;
        for (auto db : damBreeds)
        {
            std::map<Genetic::Breed::Breed*, float>::iterator it = (pTheNewEmptyBreeds->getBreeds()).find(db.first);
            if (it == (pTheNewEmptyBreeds->getBreeds()).end())
            {
                // not this breed yet
                (pTheNewEmptyBreeds->getBreeds())[db.first] = ratioDam * db.second;
            }
            else
            {
                // has already this breed
                (it->second)+= ratioDam * db.second;               
            }
        }
        
        // Breed performances
        if (!pTheNewEmptyBreeds->isPureBreed())
        {
            // Heterosis factor based on the sire and dam common breed
            float sireBreedInDam = 0.0f;
            std::map<Genetic::Breed::Breed*, float>::iterator it = damBreeds.find(pSireBreed);
            if (it != damBreeds.end())
            {
                sireBreedInDam = it->second;
            }
            pTheNewEmptyBreeds->setHeterosisFactor(1.0f - sireBreedInDam);
            
            // we have to calculate specific performances for the mix, based on each breeds
            std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> &base = pSireBreed->getBreedMeanProductions(); // Just to enumerate list of valorizable characteristics
            for (std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float>::iterator itChar = base.begin(); itChar != base.end(); itChar++)
            {
                FunctionalEnumerations::Genetic::PhenotypicCharacter phenoChar = itChar->first;
                float valForPhenotypicCharacter = 0.0f;
                for (it = pTheNewEmptyBreeds->getBreeds().begin(); it != pTheNewEmptyBreeds->getBreeds().end(); it++)
                {
                    valForPhenotypicCharacter += (it->first)->getBreedMeanProduction(phenoChar) * it->second;
                }     
                
                // Add heterosis to specific performances
                valForPhenotypicCharacter += FunctionalConstants::Genetic::HETEROSIS.find(phenoChar)->second * pTheNewEmptyBreeds->getHeterosisFactor();
                
                // set the value
                pTheNewEmptyBreeds->setSpecificBreedMeanProduction(phenoChar, valForPhenotypicCharacter);
            }
        }
        else
        {
            // We don't have to calculate a specific performance, just to give the one of the unic breed
            pTheNewEmptyBreeds->setBreedMeanProductions(pSireBreed->getBreedMeanProductions());
        }
        
#ifdef _LOG
        // Verifying the global ratio : must be = 1.0f
        float res = 0.0f;
        for (auto neg : (pTheNewEmptyBreeds->getBreeds()))
        {
            res += neg.second;
        }
        assert (res == 1.0f);
#endif // _LOG        
    }
    
    // Setting the eventually individual breed dependent mortality risk
    //const std::map<FunctionalEnumerations::Population::MortalityRiskAge, float>* GeneticManagement::generateAndGetMortalityRiskMatrix(Genetic::BreedInheritance* pBi, FunctionalEnumerations::Genetic::Sex sex, std::map<FunctionalEnumerations::Population::MortalityRiskAge, float> &specificMatrix,
    const std::map<FunctionalEnumerations::Population::MortalityRiskAge, float> GeneticManagement::generateAndGetMortalityRiskMatrix(Genetic::BreedInheritance* pBi, FunctionalEnumerations::Genetic::Sex sex,
                    std::map<FunctionalEnumerations::Genetic::Sex, std::map<FunctionalEnumerations::Genetic::BreedType,std::map<FunctionalEnumerations::Population::MortalityRiskAge, float>>> &globalMortalityProba)
    {
        Genetic::Breed::Breed* pTheBreed;
        if (pBi->isPureBreed(pTheBreed))
        {
            // global value
            return globalMortalityProba.find(sex)->second.find(pTheBreed->getBreedType())->second;
        }
        else
        {
            // specific individual value
            std::map<FunctionalEnumerations::Genetic::BreedType, float> btMap; // tmp mat to knows the proportion of the several breed types
            for (auto b : pBi->getBreeds())
            {
                FunctionalEnumerations::Genetic::BreedType theBreebType = (b.first)->getBreedType();
                std::map<FunctionalEnumerations::Genetic::BreedType, float>::iterator itBtMap = btMap.find(theBreebType);
                if (itBtMap == btMap.end())
                {
                    btMap[theBreebType] = b.second;
                }
                else
                {
                    btMap[theBreebType] += b.second;
                }
            }
            
            if (btMap.size() == 1)
            {
                // we have only one breed type, so we can use the global value 
                return globalMortalityProba.find(sex)->second.find(btMap.begin()->first)->second;
            }
            else
            {
                std::map<FunctionalEnumerations::Population::MortalityRiskAge, float> specificMatrix;
                // We have several type of breed, we have to calculate a specific mortality risk table, with the mean of the types of breed, depending on the ratio
                for (auto bt : btMap)
                {
                    FunctionalEnumerations::Genetic::BreedType theBreebType = bt.first;
                    float theRatio = bt.second;
                    const std::map<FunctionalEnumerations::Population::MortalityRiskAge, float>*  pTheBreedTypeGeneralMatrix = &globalMortalityProba.find(sex)->second.find(theBreebType)->second;
                    if (specificMatrix.size() == 0)
                    {
                        // the first type of breed
                        for (std::map<FunctionalEnumerations::Population::MortalityRiskAge, float>::const_iterator it = pTheBreedTypeGeneralMatrix->begin(); it != pTheBreedTypeGeneralMatrix->end(); it++)
                        {
                            specificMatrix[it->first] = it->second * theRatio;
                        }
                    }
                    else
                    {
                        for (std::map<FunctionalEnumerations::Population::MortalityRiskAge, float>::const_iterator it = pTheBreedTypeGeneralMatrix->begin(); it != pTheBreedTypeGeneralMatrix->end(); it++)
                        {
                            specificMatrix[it->first] += it->second * theRatio;
                        }
                    }
                }      
                return specificMatrix;
            }
        }
    }  
    
    // Setting the eventually individual breed dependent Pots partum duration 
    const std::map<FunctionalEnumerations::Reproduction::DairyCowParity, std::vector<float>>* GeneticManagement::generateAndGetPostPartumOvulationDurationMatrix(Genetic::BreedInheritance* pBi, std::map<FunctionalEnumerations::Reproduction::DairyCowParity, std::vector<float>> &specificMatrix)
    {
        Genetic::Breed::Breed* pTheBreed = nullptr;
        if (pBi->isPureBreed(pTheBreed))
        {
            Breed::DairyBreed* pFb = dynamic_cast<Breed::DairyBreed*>(pTheBreed);
            
#ifdef _LOG
            assert (pFb != nullptr); // it must be a farmable breeds
#endif // _LOG
            
             // global value
            return &(pFb->getPostPartumOvulationDurationMatrix());
        }
        else
        {
            // specific individual value
            for (unsigned int i = FunctionalEnumerations::Reproduction::DairyCowParity::primipare; i <= FunctionalEnumerations::Reproduction::DairyCowParity::multipare; i++)
            {
                std::vector<float> tmpV {0.0f, 0.0f, 0.0f, 0.0f};
                unsigned int breedCount = 0;
                Breed::DairyBreed* pPotentialUniqueBreed = nullptr;
                
                float ratio = 0.0f;
                for (auto b : pBi->getBreeds())
                {
                    if (Breed::DairyBreed* pFb = dynamic_cast<Breed::DairyBreed*>(b.first))
                    {
                        breedCount +=1;
                        pPotentialUniqueBreed = pFb;
                        ratio += b.second;
                        const std::vector<float> &data = pFb->getPostPartumOvulationDurationMatrix().find((FunctionalEnumerations::Reproduction::DairyCowParity)i)->second;
                        for (unsigned int j = 0; j < tmpV.size(); j++)
                        {
                            tmpV[j] += data[j]*b.second;
                        }
                    }
                }
                if (breedCount == 1)
                {
                    // we have only one useful breed, it is the value of the breed
                    return &(pPotentialUniqueBreed->getPostPartumOvulationDurationMatrix());
                }
                else if (ratio > 0.0f)
                {
                    // we have several useful breeds, it is a specific matrix
                    if (ratio < 1.0f) 
                    {
                        // complete the ration
                        for (unsigned int j = 0; j < tmpV.size(); j++)
                        {
                            tmpV[j] /= (ratio);
                        }
                    }
                    specificMatrix[(FunctionalEnumerations::Reproduction::DairyCowParity)i] = tmpV;
                }      
            }
            return &specificMatrix;
        }
    }
    
    // Setting the eventually individual milk production curve 
    const std::map<unsigned int, std::vector<ExchangeInfoStructures::MilkProductCharacteristic>>* GeneticManagement::generateAndGetNormalizedLactationCurves(Genetic::BreedInheritance* pBi, std::map<unsigned int, std::vector<ExchangeInfoStructures::MilkProductCharacteristic>> &specificCurves, std::map<unsigned int, float> &normalizedPeakMilkYield, std::map<unsigned int, unsigned int> &peakMilkDay)
    {
        Genetic::Breed::Breed* pTheBreed = nullptr;
        if (pBi->isPureBreed(pTheBreed))
        {
            // No generation, just to use the standard breed lactation curve
            Breed::DairyBreed* pFb = dynamic_cast<Breed::DairyBreed*>(pTheBreed);
            
#ifdef _LOG
            assert (pFb != nullptr); // it must be a farmable breeds
#endif // _LOG
            
             // global value
            return &(pFb->getStandardLactationCurves(normalizedPeakMilkYield, peakMilkDay));
        }
        else
        {
            // specific individual value
            for (unsigned int currentLactationNumber = 1; currentLactationNumber <=3;  currentLactationNumber++)
            {
                std::vector<ExchangeInfoStructures::MilkProductCharacteristic> currentCurve;
                float milkPeakQuantity = 0.0f;
                unsigned int milkPeakDay = 0;
                for (unsigned int dayNumber = 0; dayNumber < TechnicalConstants::MAXIMUM_DURATION_FOR_MILK_CURVE; dayNumber++)
                {
                    ExchangeInfoStructures::MilkProductCharacteristic normalizedMilkProductionOfTheDay;
                    float usedRatio = 0.0f;
                    for (auto gi : pBi->getBreeds())
                    {
                        if (Breed::DairyBreed* fb = dynamic_cast<Breed::DairyBreed*>(gi.first))
                        {
                            normalizedMilkProductionOfTheDay += fb->getNormalizedStandardMilkProductionOfTheDay(dayNumber, currentLactationNumber) * (float)gi.second;
                            usedRatio += gi.second;
                        }
                    }
                    normalizedMilkProductionOfTheDay /= usedRatio;
                    
                    // It's not the end of the lactation period
                    currentCurve.push_back(normalizedMilkProductionOfTheDay);
                    if (milkPeakQuantity < normalizedMilkProductionOfTheDay.quantity)
                    {
                        milkPeakQuantity = normalizedMilkProductionOfTheDay.quantity;
                        milkPeakDay = dayNumber;
                    }
                }
                specificCurves[currentLactationNumber] = currentCurve;
                normalizedPeakMilkYield[currentLactationNumber] = milkPeakQuantity;
                peakMilkDay[currentLactationNumber] = milkPeakDay;
            }
            return &specificCurves;
        }
    }
    
    void GeneticManagement::getMilkGeneticProductionOfTheDay(const std::vector<ExchangeInfoStructures::MilkProductCharacteristic> &normalizedMilkProduction, Genetic::GeneticValue &GeneticValue, ExchangeInfoStructures::MilkProductCharacteristic &milkProduction, unsigned int day, unsigned int lactationRank)
    {
        unsigned int dayToConsider = day;

        if (dayToConsider >= TechnicalConstants::MAXIMUM_DURATION_FOR_MILK_CURVE) dayToConsider = TechnicalConstants::MAXIMUM_DURATION_FOR_MILK_CURVE - 1;   
        
        // Genetic value
        float normalizedLevelToConsider = normalizedMilkProduction.at(dayToConsider).quantity;
        
        // Quantity gradient to maintain ?
        unsigned int dayDelta = day - dayToConsider;
        if (dayDelta > 0)
        {
            float previousNormalizedLevel = normalizedMilkProduction.at(TechnicalConstants::MAXIMUM_DURATION_FOR_MILK_CURVE - 2).quantity;
            normalizedLevelToConsider -= (previousNormalizedLevel - normalizedLevelToConsider) * dayDelta;
        }
        milkProduction.quantity = GeneticValue.getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait).getCorrectedPerformance() * normalizedLevelToConsider;
        milkProduction.TB = GeneticValue.getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::TB).getCorrectedPerformance() * normalizedMilkProduction.at(dayToConsider).TB;
        milkProduction.TP = GeneticValue.getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::TP).getCorrectedPerformance() * normalizedMilkProduction.at(dayToConsider).TP;
        milkProduction.SCC = FunctionalConstants::Lactation::MILK_SCC_WITHOUT_INFECTION_LEVEL[lactationRank-1] * normalizedMilkProduction.at(dayToConsider).SCC;
    }
        
    std::map<FunctionalEnumerations::Genetic::Breed,std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter,std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float>>> GeneticManagement::s_geneticVarianceCovarianceMatrixForMeioseRandom;
    std::map<FunctionalEnumerations::Genetic::Breed,std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter,std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float>>> GeneticManagement::s_geneticVarianceContravarianceMatrix;

}