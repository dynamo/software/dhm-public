#ifndef GENETICMANAGEMENT_H
#define GENETICMANAGEMENT_H

// Standard 
#include <map>

// project
#include "../Tools/Random.h"
#include "../ExchangeInfoStructures/DetectionConditions.h"
#include "../ExchangeInfoStructures/MilkProductCharacteristic.h"
#include "./GeneticValue.h"
#include "./Breed/DairyBreed.h"
#include "I_GeneticDairyCow.h"

namespace Genetic
{
    class BreedInheritance;
    
    class GeneticManagement
    {
        static std::map<FunctionalEnumerations::Genetic::Breed,std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter,std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float>>> s_geneticVarianceCovarianceMatrixForMeioseRandom;
        static std::map<FunctionalEnumerations::Genetic::Breed,std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter,std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float>>> s_geneticVarianceContravarianceMatrix;

    protected:
        virtual void concrete() = 0; // To ensure that it will not be instantiated

        public:
            
            // General
            static void init();
            static void getNewBreeds(std::map<FunctionalEnumerations::Genetic::Breed, Genetic::Breed::Breed*> &pCollection);
            static void setGeneticValueToCalf(I_GeneticDairyCow* pCalf, I_GeneticDairyCow* pDam, Genetic::Breed::Breed* pSireBreed, FunctionalEnumerations::Genetic::GeneticStrategy geneticStrategy, Tools::Random &random);
            static Genetic::GeneticValue generateAVariateAdultGeneticValue(const std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> &herdTrueGeneticValue, std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> &herdPhenotypeReference, FunctionalEnumerations::Genetic::Breed theBreed, FunctionalEnumerations::Genetic::GeneticStrategy geneticStrategy, Tools::Random &random);
      
            // Reproduction
            static void calculateTheoricPregnancyDurations(Genetic::BreedInheritance* pInherit, std::map<FunctionalEnumerations::Reproduction::DairyCowParity, unsigned int> &data);
            static void calculateTheoricBetweenOvulationDurations (Genetic::BreedInheritance* pInherit, std::map<FunctionalEnumerations::Reproduction::DairyCowParity, unsigned int> &data, Tools::Random &random);

            static bool isOvulationDetected(Genetic::BreedInheritance* pInherit, ExchangeInfoStructures::DetectionConditions &dc, Tools::Random &random);
            static float getOvulationDetectionProba(Genetic::BreedInheritance* pInherit, ExchangeInfoStructures::DetectionConditions &dc);
            static float getBasisInseminationSuccessProbability(Genetic::BreedInheritance* pInherit, FunctionalEnumerations::Reproduction::DairyCowParity parity, FunctionalEnumerations::Reproduction::TypeInseminationSemen ts, float farmFertilityFactor, float geneticFertilityPerformance, float medicFertilityDelta);            
            static void setFarrowedBreeds(Genetic::BreedInheritance *pTheNewEmptyBreeds, std::map<Genetic::Breed::Breed*, float> &damBreeds, Genetic::Breed::Breed* pSireBreed);
            //static const std::map<FunctionalEnumerations::Population::MortalityRiskAge, float> *generateAndGetMortalityRiskMatrix(Genetic::BreedInheritance* pBi, FunctionalEnumerations::Genetic::Sex sex, std::map<FunctionalEnumerations::Population::MortalityRiskAge, float> &specificMatrix,
            static const std::map<FunctionalEnumerations::Population::MortalityRiskAge, float> generateAndGetMortalityRiskMatrix(Genetic::BreedInheritance* pBi, FunctionalEnumerations::Genetic::Sex sex,
                            std::map<FunctionalEnumerations::Genetic::Sex, std::map<FunctionalEnumerations::Genetic::BreedType,std::map<FunctionalEnumerations::Population::MortalityRiskAge, float>>> &globalMortalityProba);       // Setting the eventually individual breed dependent mortality risk
            static const std::map<FunctionalEnumerations::Reproduction::DairyCowParity, std::vector<float>>* generateAndGetPostPartumOvulationDurationMatrix(Genetic::BreedInheritance* pBi, std::map<FunctionalEnumerations::Reproduction::DairyCowParity, std::vector<float>> &specificMatrix);

            // Lactation
            static const std::map<unsigned int, std::vector<ExchangeInfoStructures::MilkProductCharacteristic>>* generateAndGetNormalizedLactationCurves(Genetic::BreedInheritance* pBi, std::map<unsigned int, std::vector<ExchangeInfoStructures::MilkProductCharacteristic>> &specificCurves, std::map<unsigned int, float> &normalizedPeakMilkYield, std::map<unsigned int, unsigned int> &peakMilkDay);
            static void getMilkGeneticProductionOfTheDay( const std::vector<ExchangeInfoStructures::MilkProductCharacteristic> &normalizedMilkProduction, Genetic::GeneticValue &GeneticValue, ExchangeInfoStructures::MilkProductCharacteristic &milkProduction, unsigned int day, unsigned int lactationRank);
#ifdef _LOG
            static void saveAllMilkCurvesInFiles(const std::string &theResultPath);
#endif // _LOG 

    };
}
#endif /* GENETICMANAGEMENT_H */
