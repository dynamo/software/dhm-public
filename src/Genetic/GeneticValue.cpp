#include "GeneticValue.h"

// Project
#include "../Tools/Tools.h"

BOOST_CLASS_EXPORT(Genetic::GeneticValue) // Recommended (mandatory if virtual serialization)

namespace Genetic
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
            ar & BOOST_SERIALIZATION_NVP(_note); \
            ar & BOOST_SERIALIZATION_NVP(_LGF); \
            ar & BOOST_SERIALIZATION_NVP(_mHasCharacterGeneticValues); \
            ar & BOOST_SERIALIZATION_NVP(_MG); \
            ar & BOOST_SERIALIZATION_NVP(_MP); \
            ar & BOOST_SERIALIZATION_NVP(_INEL); \
    
    IMPLEMENT_SERIALIZE_STD_METHODS(GeneticValue);
    
    GeneticValue::GeneticValue()
    {
    }

    GeneticValue::GeneticValue(GeneticValue const& other) // copy constructor 
    {
        _note = other._note;
        _LGF = other._LGF;
        _mHasCharacterGeneticValues = other._mHasCharacterGeneticValues;
        _MG = other._MG;
        _MP = other._MP;
        _INEL = other._INEL;
    }    
    
    void GeneticValue::setLGF(float lgfDam, float lgfSir)
    {
        _LGF = (lgfDam + lgfSir)/2.0f;
    }
        
     void GeneticValue::setLGF(float lgf)
    {
        _LGF = lgf;
    }
        
    std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, Genetic::CharacterGeneticValue> &GeneticValue::getCharacterGeneticValues()
    {
        return _mHasCharacterGeneticValues;
    }
    
    Genetic::CharacterGeneticValue &GeneticValue::getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter phenotypicCharacter)
    {
        return _mHasCharacterGeneticValues.find(phenotypicCharacter)->second;
    }
     
    void GeneticValue::setCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter phenotypicCharacter, CharacterGeneticValue value)
    {
        _mHasCharacterGeneticValues[phenotypicCharacter] = value;
    }
        
    void GeneticValue::finalizeCalculatedGeneticValues(FunctionalEnumerations::Genetic::GeneticStrategy strategy)
    {
        // MG = LAIT * TB/1000
        _MG = getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait).getCorrectedPerformance() * getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::TB).getTrueVG() / 1000;
        
        // MP = LAIT * TP/1000
        _MP = getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait).getCorrectedPerformance() * getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::TP).getTrueVG() / 1000;
    
        // INEL = 0.98 (MP + 0.2 MG + TP + 0.5 TB)
        _INEL = 0.98f * (  _MP
                           + (0.2f * _MG)
                           + getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::TP).getTrueVG()
                           + 0.5f * getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::TB).getTrueVG()
                        );
        
        // Note depending on the genetic strategy
        if (strategy == FunctionalEnumerations::Genetic::GeneticStrategy::wellBalanced)
        {
            _note = getINEL();
        }
        else if (strategy == FunctionalEnumerations::Genetic::GeneticStrategy::milkQuantityPriority)
        {
            _note = getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait).getCorrectedPerformance();
        }
        else if (strategy == FunctionalEnumerations::Genetic::GeneticStrategy::functionalLongevityPriority) 
        {
            _note = getLGF();
        }
        else
        {
            // No one ?
            std::string errorMessage = "Unknown genetic strategy : " + Tools::toString(strategy);
            throw std::runtime_error(errorMessage);
        }
    }
    
    float GeneticValue::getMG()
    {
        return _MG;
    }
    
    float GeneticValue::getMP()
    {
        return _MP;
    }

    float GeneticValue::getINEL()
    {
        return _INEL;
    }
    
    float GeneticValue::getLGF()
    {
        return _LGF;
    }
    
    bool GeneticValue::operator < (GeneticValue &other)
    {
        return getGeneticNote() < other.getGeneticNote();
    }
    
    float GeneticValue::getGeneticNote()
    {
        return _note;
    }
    
    void GeneticValue::progress(const std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> &progressValues, FunctionalEnumerations::Genetic::GeneticStrategy strategy)
    {
        for (std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float>::const_iterator it = progressValues.begin(); it != progressValues.end(); it++)
        {
            _mHasCharacterGeneticValues.find(it->first)->second.progress((it->second)); // The character genetic value has to progress of the progress value
        }
        finalizeCalculatedGeneticValues(strategy);
    }
}