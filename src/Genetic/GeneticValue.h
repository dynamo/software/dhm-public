#ifndef GeneticValue_h
#define GeneticValue_h

// standard
#include <string>
#include <map>

// project
#include "../Tools/Serialization.h"
#include "../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "../Genetic/CharacterGeneticValue.h"

namespace Genetic
{
    struct GeneticValue
    {
    private:
        // http://fr.france-genetique-elevage.org/Les-index-des-races-bovines.html
        // http://idele.fr/fileadmin/medias/Documents/Nomenclatures/Nomenclature_BL_fr.pdf
        //FunctionalEnumerations::Genetic::GeneticStrategy _strategy;
        float _note = 0.0f;
        float _LGF = 0.0f;
        std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, Genetic::CharacterGeneticValue> _mHasCharacterGeneticValues;
        float _MG = 0.0f;
        float _MP = 0.0f;
        float _INEL = 0.0f;
        
        DECLARE_SERIALIZE_STD_METHODS;

    public:
        GeneticValue();
        GeneticValue(GeneticValue const& other); // copy constructor           
        virtual ~GeneticValue(){};

        float getGeneticNote();
        void setLGF(float lgfDam, float lgfSir);
        void setLGF(float lgf);
        float getLGF();
        std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, Genetic::CharacterGeneticValue> &getCharacterGeneticValues();
        Genetic::CharacterGeneticValue &getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter phenotypicCharacter);
        void setCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter phenotypicCharacter, CharacterGeneticValue value);
        bool operator < (GeneticValue &other);
        void finalizeCalculatedGeneticValues(FunctionalEnumerations::Genetic::GeneticStrategy strategy);
        float getMG();
        float getMP();
        float getINEL();
        
        void progress(const std::map<FunctionalEnumerations::Genetic::PhenotypicCharacter, float> &progressValues, FunctionalEnumerations::Genetic::GeneticStrategy strategy);
    };
} /* End of namespace Lactation */
#endif // GeneticValue_h
