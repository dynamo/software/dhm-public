#ifndef Genetic_I_GeneticDairyCow_h
#define Genetic_I_GeneticDairyCow_h

// boost

// project
#include "GeneticValue.h"
#include "BreedInheritance.h"

namespace Genetic
{
    class I_GeneticDairyCow
    {
    public:
        virtual ~I_GeneticDairyCow() { }
        virtual Genetic::GeneticValue &getGeneticValue() = 0;
        virtual Genetic::GeneticValue &getSirGeneticValueOfTheLastFertilyInsemination() = 0;
        virtual Genetic::BreedInheritance* getCowBreedInheritance() = 0;
        
    };
} /* End of namespace Genetic */

#endif // Genetic_I_GeneticDairyCow_h
