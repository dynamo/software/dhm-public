#include "MaleCatalog.h"

// standard
#include <map>

// Project
#include "./Breed/Breed.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"
#include "../ResultStructures/DairyFarmResult.h"

BOOST_CLASS_EXPORT(Genetic::MaleCatalog) // Recommended (mandatory if virtual serialization)

namespace Genetic
{
     // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
    ar & BOOST_SERIALIZATION_NVP(_mIsComposedMaleGeneticValues); \

    IMPLEMENT_SERIALIZE_STD_METHODS(MaleCatalog);

    MaleCatalog::MaleCatalog(const std::string &maleDataFile, FunctionalEnumerations::Genetic::GeneticStrategy geneticStrategy, std::map<FunctionalEnumerations::Genetic::Breed, Genetic::Breed::Breed*> &mBreeds)
    {
        Breed::Breed::getNewMaleGeneticValueCatalog(_mIsComposedMaleGeneticValues, maleDataFile, geneticStrategy, mBreeds);
    }
    
    Genetic::GeneticValue &MaleCatalog::getASireGeneticValue(FunctionalEnumerations::Genetic::Breed br, const boost::gregorian::date &simDate, Results::DairyFarmResult* pRes, Tools::Random &random)
    {
        // get a random sire in the male catalog, depending on the breed
        std::vector<GeneticValue> &breedCatalog = _mIsComposedMaleGeneticValues.find(br)->second;
        
#ifdef _LOG
        assert (breedCatalog.size() != 0); // We must have at least a male in the catalog
#endif // _LOG
        
        GeneticValue &res = breedCatalog[random.rng_uniform_int(breedCatalog.size())]; // Loi uniforme
#ifdef _LOG
        Results::GeneticResult gr;
        gr.date = simDate;
        gr.idOrBreed = br;
        gr.VGLait = res.getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::Lait).getCorrectedPerformance();
        gr.VGTB = res.getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::TB).getCorrectedPerformance();
        gr.VGTP = res.getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::TP).getCorrectedPerformance();
        gr.VGFer = res.getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::Fer).getCorrectedPerformance();
        gr.VGMACL = res.getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::MACL).getCorrectedPerformance();
        gr.VGBHBlait = res.getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::BHBlait).getCorrectedPerformance();
        gr.VGRBi = res.getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::RBi).getCorrectedPerformance();
        gr.VGRBni = res.getCharacterGeneticValue(FunctionalEnumerations::Genetic::PhenotypicCharacter::RBni).getCorrectedPerformance();
        pRes->getGeneticResults().MaleVGResults.push_back(gr);
#endif // _LOG       
        return res;
    }
    
    void MaleCatalog::appendMaleAnnualProgress(FunctionalEnumerations::Genetic::GeneticStrategy strategy)
    {
        // calculating the male genetic progress
        for (std::map<FunctionalEnumerations::Genetic::Breed, std::vector<GeneticValue>>::iterator it = _mIsComposedMaleGeneticValues.begin(); it != _mIsComposedMaleGeneticValues.end(); it++)
        {
            if (FunctionalConstants::Genetic::ANNUAL_GENETIC_PROGRESS.find(it->first) != FunctionalConstants::Genetic::ANNUAL_GENETIC_PROGRESS.end())
            {
                // We have to set a genetic progress
                std::vector<GeneticValue> &breedCatalog = _mIsComposedMaleGeneticValues.find(it->first)->second;

                // To setup all the male values
                for (std::vector<GeneticValue>::iterator itCat = breedCatalog.begin(); itCat != breedCatalog.end(); itCat++)
                {
                    GeneticValue &gv = *itCat;
                    gv.progress(FunctionalConstants::Genetic::ANNUAL_GENETIC_PROGRESS.find(it->first)->second, strategy);
                }                    
            }
        }
    }
}
