#ifndef Genetic_MaleCatalog_h
#define Genetic_MaleCatalog_h

// standard
#include <string>

// project
#include "./GeneticValue.h"
#include "./Breed/Breed.h"
#include "../Tools/Serialization.h"
#include "../Tools/Random.h"
#include "../ResultStructures/DairyFarmResult.h"

namespace Results
{
    class DairyFarmResult;
}

namespace Genetic
{
    class MaleCatalog
    {
    private:
        DECLARE_SERIALIZE_STD_METHODS;
        
        std::map<FunctionalEnumerations::Genetic::Breed, std::vector<Genetic::GeneticValue>> _mIsComposedMaleGeneticValues;

    public:
        MaleCatalog(){}; // For serialization
        MaleCatalog(const std::string &MaleDataFile, FunctionalEnumerations::Genetic::GeneticStrategy geneticStrategy, std::map<FunctionalEnumerations::Genetic::Breed, Genetic::Breed::Breed*> &mBreeds);
        virtual ~MaleCatalog(){};
        
        GeneticValue &getASireGeneticValue(FunctionalEnumerations::Genetic::Breed br, const boost::gregorian::date &simDate, Results::DairyFarmResult* pRes, Tools::Random &random);
        void appendMaleAnnualProgress(FunctionalEnumerations::Genetic::GeneticStrategy strategy);

    }; /* End of class MaleCatalog */
} /* End of namespace Genetic */
#endif // Genetic_MaleCatalog_h
