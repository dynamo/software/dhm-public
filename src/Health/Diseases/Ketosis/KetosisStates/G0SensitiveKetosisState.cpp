#include "G0SensitiveKetosisState.h"

// project
//#include "I_KetosisProneDairyCow.h"
#include "../../../../DataStructures/DairyMammal.h"
#include "G0UnsensitiveKetosisState.h"
#include "../../../HealthManagement.h"

BOOST_CLASS_EXPORT(Health::Ketosis::KetosisStates::G0SensitiveKetosisState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Ketosis
{
    namespace KetosisStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(KetosisState); \

        IMPLEMENT_SERIALIZE_STD_METHODS(G0SensitiveKetosisState);

        G0SensitiveKetosisState::G0SensitiveKetosisState(const boost::gregorian::date &beginDate, DataStructures::DairyMammal* pCow) : KetosisState(beginDate, pCow)
        {
            unsigned int duration = pCow->getDurationUntilEndKetosisRisk();
            assert (duration != 0);
            
            _predictedEndDate = beginDate + boost::gregorian::date_duration(duration);
        }

        void G0SensitiveKetosisState::progress(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState
#ifdef _LOG
                                                          , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                          )
        {
            KetosisState::progress(simDate, random, pNewState
#ifdef _LOG
                                                          , log
#endif // _LOG            
                                                          );
            
            if (isOutOfTime(simDate))
            {
                assert(pNewState == nullptr);
                pNewState = new G0UnsensitiveKetosisState(simDate, _pAppliesToDairyMammal, 0, random);
            }
            else
            {        
                // Do we have a new Ketosis occurency ?
                HealthManagement::manageNewKetosisOccurency(simDate, _pAppliesToDairyMammal, random, pNewState);
            }
        }

        G0SensitiveKetosisState::~G0SensitiveKetosisState()
        {
        }
                
        void G0SensitiveKetosisState::considerCetodetectTest(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState)
        {
            // No current ketosis case, but may be a specificity error ?
            if (random.ran_resultRegardingSpecificity(FunctionalConstants::Health::CETODETECT_SPECIFICITY))
            {
                // Subclinical (fictive) ketosis case found, manage treatmment
                takeTreatmentIfOutOfEffect(simDate, FunctionalEnumerations::Health::KetosisSeverity::G1Ketosis, pNewState, random
#ifdef _LOG
                                                                                                    , " due to cetodetect test specificity error"
#endif // _LOG            
                                                                                                    );
            }
        }
        void G0SensitiveKetosisState::considerHerdNavigatorTest(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState)
        {
            // No current ketosis case, but may be a specificity error ?
            if (random.ran_resultRegardingSpecificity(FunctionalConstants::Health::HERD_NAVIGATOR_SPECIFICITY))
            {
                // Subclinical (fictive) ketosis case found, manage treatmment
                takeTreatmentIfOutOfEffect(simDate, FunctionalEnumerations::Health::KetosisSeverity::G1Ketosis, pNewState, random
#ifdef _LOG
                                                                                                    , " due to herd navigator test specificity error"
#endif // _LOG            
                                                                                                    );
            }
        }
        
        void G0SensitiveKetosisState::considerKetosisBloodTest(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState)
        {
            // No current ketosis case, but may be a specificity error ?
            if (random.ran_resultRegardingSpecificity(FunctionalConstants::Health::KETOSIS_BLOOD_TEST_SPECIFICITY))
            {
                // Subclinical (fictive) ketosis case found, manage treatmment
                takeTreatmentIfOutOfEffect(simDate, FunctionalEnumerations::Health::KetosisSeverity::G1Ketosis, pNewState, random
#ifdef _LOG
                                                                                                    , " due to blood test specificity error"
#endif // _LOG            
                                                                                                    );
            }
        }
        
        void G0SensitiveKetosisState::takeTreatment(const boost::gregorian::date &simDate, FunctionalEnumerations::Health::KetosisSeverity severity, bool &success, KetosisState* &pNewState, Tools::Random &random
#ifdef _LOG
                                                                                                    , std::string origin
#endif // _LOG            
                                                                                                    )
        {
            // Base
            KetosisState::takeTreatment(simDate, severity, success, pNewState, random
#ifdef _LOG
                                                                                                    , origin
#endif // _LOG            
                                                                                                    );
            
            // May be a protect effect ?
            unsigned int protectionDuration = getProtectionDuration(simDate);
            if (protectionDuration > 0)
            {
                assert(pNewState == nullptr);
                pNewState = new G0UnsensitiveKetosisState(simDate, _pAppliesToDairyMammal, protectionDuration, random);
            }
        }
        
        void G0SensitiveKetosisState::exitFromKetosisRiskPeriod(const boost::gregorian::date &date, KetosisState* &pNewState)
        {
            assert (pNewState == nullptr);
            pNewState = new G0UnsensitiveKetosisState(date, _pAppliesToDairyMammal);
        }

#ifdef _LOG
        std::string G0SensitiveKetosisState::getStrKetosisState()
        {
            return "G0s";
        }
#endif // _LOG            
    }
}
}
