#ifndef Health_DairyCowKetosisStates_G0SensitiveKetosisState_h
#define Health_DairyCowKetosisStates_G0SensitiveKetosisState_h

// project
#include "KetosisState.h"

namespace Health
{
namespace Ketosis
{
    namespace KetosisStates
    {
        class G0SensitiveKetosisState : public KetosisState
        {
            DECLARE_SERIALIZE_STD_METHODS;

        protected:
            G0SensitiveKetosisState(){};
            void concrete(){}; // To allow instanciation
             
        public:
            G0SensitiveKetosisState(const boost::gregorian::date &beginDate, DataStructures::DairyMammal* pCow);
            virtual ~G0SensitiveKetosisState();
            inline FunctionalEnumerations::Health::KetosisSeverity getSeverity(){return FunctionalEnumerations::Health::KetosisSeverity::G0Ketosis;};
            void considerCetodetectTest(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState) override;
            void considerHerdNavigatorTest(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState) override;
            void considerKetosisBloodTest(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState) override;
            void progress(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState
#ifdef _LOG
                                                              , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                              ) final;
            void exitFromKetosisRiskPeriod(const boost::gregorian::date &date, KetosisState* &pNewState) override;
#ifdef _LOG
            std::string getStrKetosisState();
#endif // _LOG            
        protected:
            void takeTreatment(const boost::gregorian::date &simDate, FunctionalEnumerations::Health::KetosisSeverity severity, bool &success, KetosisState* &pNewState, Tools::Random &random
#ifdef _LOG
                                                                                                    , std::string origin
#endif // _LOG            
                                                                                                    ) override; 
        };
        }
    }
}
#endif // Health_DairyCowKetosisStates_G0SensitiveKetosisState_h
