#include "G0UnsensitiveKetosisState.h"

// project
//#include "I_KetosisProneDairyCow.h"
#include "../../../../DataStructures/DairyMammal.h"
#include "G0SensitiveKetosisState.h"
#include "G1SubclinicalKetosisState.h"

BOOST_CLASS_EXPORT(Health::Ketosis::KetosisStates::G0UnsensitiveKetosisState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Ketosis
{
    namespace KetosisStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(KetosisState); \
        ar & BOOST_SERIALIZATION_NVP(_nextStateIsG1Recurrency); \

        IMPLEMENT_SERIALIZE_STD_METHODS(G0UnsensitiveKetosisState);

        G0UnsensitiveKetosisState::G0UnsensitiveKetosisState(const boost::gregorian::date &beginDate, DataStructures::DairyMammal* pCow, unsigned int protectDuration, Tools::Random &random) : KetosisState(beginDate, pCow)
        {
            if (pCow->willHaveKetosisRecurrency())
            {
                _nextStateIsG1Recurrency = true;
                int intDuration = protectDuration + random.ran_normal_distribution(FunctionalConstants::Health::KETOSIS_MEAN_DAY_FOR_RECURRENCY, FunctionalConstants::Health::KETOSIS_STANDARD_DEVIATION_FOR_RECURRENCY);
                if (intDuration <= 0) intDuration = 1;
                boost::gregorian::date_duration duration = boost::gregorian::date_duration(intDuration);
                setDuration(duration);
            }
            else if (protectDuration != 0)
            {
                boost::gregorian::date_duration duration = boost::gregorian::date_duration(protectDuration);
                setDuration(duration);
            }
        }
        
        void G0UnsensitiveKetosisState::progress(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState
#ifdef _LOG
                                                          , Results::DayAnimalLogInformations &log
#endif // _LOG
                                                          )
        {
            KetosisState::progress(simDate, random, pNewState
#ifdef _LOG
                                                          , log
#endif // _LOG            
                                                          );
            // End of unsensitive state ?
            if (isOutOfTime(simDate))
            {
                if (_pAppliesToDairyMammal->isInKetosisRiskPeriod())
                {
                    assert (pNewState == nullptr);
                    if (_nextStateIsG1Recurrency)
                    {
                        // Recurrence
                        pNewState = new G1SubclinicalKetosisState(simDate, _pAppliesToDairyMammal, true, random);
                    }
                    else
                    {
                        pNewState = new G0SensitiveKetosisState(simDate, _pAppliesToDairyMammal);
                    }
                }
                else
                {
                    // May be a end of protection
                    setDurationInfinite();
                }
            }
        }

        G0UnsensitiveKetosisState::~G0UnsensitiveKetosisState()
        {
        }
        
        void G0UnsensitiveKetosisState::enterInKetosisRiskPeriod(const boost::gregorian::date &date, KetosisState* &pNewState)
        {
            if (_predictedEndDate.is_infinity() )
            {
                // Not protected state
                assert (pNewState == nullptr);
                pNewState = new G0SensitiveKetosisState(date, _pAppliesToDairyMammal);
            }
        }

#ifdef _LOG
        std::string G0UnsensitiveKetosisState::getStrKetosisState()
        {
            return "G0u";
        }
#endif // _LOG            
    }
}
}
