#ifndef Health_DairyCowKetosisStates_G0UnsensitiveKetosisState_h
#define Health_DairyCowKetosisStates_G0UnsensitiveKetosisState_h

// project
#include "KetosisState.h"

namespace Health
{
namespace Ketosis
{
    namespace KetosisStates
    {
        class G0UnsensitiveKetosisState : public KetosisState
        {
            DECLARE_SERIALIZE_STD_METHODS;
        private:
            
            bool _nextStateIsG1Recurrency = false;
        
        protected:
            G0UnsensitiveKetosisState(){};
            void concrete(){}; // To allow instanciation

        public:
            G0UnsensitiveKetosisState(const boost::gregorian::date &beginDate, DataStructures::DairyMammal* pCow, unsigned int protectDuration, Tools::Random &random);
            G0UnsensitiveKetosisState(const boost::gregorian::date &beginDate, DataStructures::DairyMammal* pCow) : KetosisState(beginDate, pCow){}
            virtual ~G0UnsensitiveKetosisState();
            inline FunctionalEnumerations::Health::KetosisSeverity getSeverity(){return FunctionalEnumerations::Health::KetosisSeverity::G0Ketosis;};
            void progress(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState
#ifdef _LOG
                                                              , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                              ) final;
            void enterInKetosisRiskPeriod(const boost::gregorian::date &date, KetosisState* &pNewState) override;
#ifdef _LOG
            std::string getStrKetosisState();
#endif // _LOG            
        };
    }
}
}
#endif // Health_DairyCowKetosisStates_G0UnsensitiveKetosisState_h
