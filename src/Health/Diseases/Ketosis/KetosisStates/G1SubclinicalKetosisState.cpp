#include "G1SubclinicalKetosisState.h"

// project
//#include "./I_KetosisProneDairyCow.h"
#include "../../../../DataStructures/DairyMammal.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"
#include "G2ClinicalKetosisState.h"
#include "../../../HealthManagement.h"

BOOST_CLASS_EXPORT(Health::Ketosis::KetosisStates::G1SubclinicalKetosisState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Ketosis
{
    namespace KetosisStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(IllKetosisState); \
        ar & BOOST_SERIALIZATION_NVP(_recurrence); \
        ar & BOOST_SERIALIZATION_NVP(_flareup); \

        IMPLEMENT_SERIALIZE_STD_METHODS(G1SubclinicalKetosisState);

        G1SubclinicalKetosisState::G1SubclinicalKetosisState(const boost::gregorian::date &beginDate, DataStructures::DairyMammal* pCow, bool recurrence, Tools::Random &random)
                : IllKetosisState(beginDate, pCow, FunctionalConstants::Health::KETOSIS_G1_MILK_PERCENT_QUANTITY_LOSS, FunctionalConstants::Health::KETOSIS_G1_MILK_QUANTITY_LOSS_DURATION)
        {
            _recurrence = recurrence;
            
            if (not recurrence) pCow->notifyNewKetosisOccurence(beginDate, FunctionalEnumerations::Health::KetosisSeverity::G1Ketosis);
          
            // Duration
            boost::gregorian::date_duration duration = boost::gregorian::date_duration(pCow->getSubclinicalKetosisDuration());
            setDuration(duration);
            
            // At the end of the duration, will this G1 flare up ?
            FunctionalEnumerations::Reproduction::DairyCowParity parity = _pAppliesToDairyMammal->getParityForKetosis();
            assert (parity != FunctionalEnumerations::Reproduction::DairyCowParity::heifer); // Heifer can't have a ketosis
            float flareupProbability = FunctionalConstants::Health::SUBCLINICAL_G1_KETOSIS_FLARE_UP_PROBABILITY.find(parity)->second;
            if (pCow->hadKetosisCaseDuringPreviousLactation()) flareupProbability *= FunctionalConstants::Health::KETOSIS_FLAREUP_RISK_FACTOR_WHEN_PREVIOUS_KETOSIS_INFECTION;
            _flareup = random.ran_bernoulli(flareupProbability);
        }
        
        void G1SubclinicalKetosisState::considerHerdNavigatorTest(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState)
        {
            // Current subclinical ketosis case, but may be a sensitivity error ?
            if (random.ran_resultRegardingSensitivity(FunctionalConstants::Health::CETODETECT_SENSITIVITY))
            {
                 // subclinical ketosis case detected, manage a treatment
                takeTreatmentIfOutOfEffect(simDate, FunctionalEnumerations::Health::KetosisSeverity::G1Ketosis, pNewState, random
#ifdef _LOG
                                                                                                    , " due to real subclinical ketosis case detected by Herd navigator"
#endif // _LOG            
                                                                                                    );
            }
        }

        void G1SubclinicalKetosisState::considerCetodetectTest(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState)
        {
            // Current subclinical ketosis case, but may be a sensitivity error ?
            if (random.ran_resultRegardingSensitivity(FunctionalConstants::Health::HERD_NAVIGATOR_SENSITIVITY))
            {
                // subclinical ketosis case detected, manage a treatment
                takeTreatmentIfOutOfEffect(simDate, FunctionalEnumerations::Health::KetosisSeverity::G1Ketosis, pNewState, random
#ifdef _LOG
                                                                                                    , " due to real subclinical ketosis case detected by cetodetect"
#endif // _LOG            
                                                                                                    );
            }
        }
        
        void G1SubclinicalKetosisState::considerKetosisBloodTest(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState)
        {
            // Current subclinical ketosis case, but may be a sensitivity error ?
            if (random.ran_resultRegardingSensitivity(FunctionalConstants::Health::KETOSIS_BLOOD_TEST_SENSITIVITY))
            {
                // subclinical ketosis case detected, manage a treatment
                takeTreatmentIfOutOfEffect(simDate, FunctionalEnumerations::Health::KetosisSeverity::G1Ketosis, pNewState, random
#ifdef _LOG
                                                                                                    , " due to real subclinical ketosis case detected by blood test"
#endif // _LOG            
                                                                                                    );
            }
        }

        void G1SubclinicalKetosisState::takeTreatment(const boost::gregorian::date &simDate, FunctionalEnumerations::Health::KetosisSeverity severity, bool &success, KetosisState* &pNewState, Tools::Random &random
#ifdef _LOG
                                                                                                    , std::string origin
#endif // _LOG            
                                                                                                    )
        {
            // Base
            IllKetosisState::takeTreatment(simDate, severity, success, pNewState, random
#ifdef _LOG
                                                                                                    , origin
#endif // _LOG            
                                                                                                    );
            
            // Notify the success
            if (success)
            {
                _flareup = false;
                _pAppliesToDairyMammal->considereG1KetosisSuccessfulTreatmentDuringTheLactation();
            }
            else if (_flareup)
            {
                // We have to reconsider the flare up regarding the treatment
                _flareup = random.ran_bernoulli(_treatmentTypeUsed.flareupRisk);
            }
        }
        
        void G1SubclinicalKetosisState::getKetosisTBTPEffect(float &milkTBDelta, float &milkTPDelta)
        {
            milkTBDelta = FunctionalConstants::Health::KETOSIS_G1_MILK_TB_DELTA;
            milkTPDelta = FunctionalConstants::Health::KETOSIS_G1_MILK_TP_DELTA;
        }
        
        void G1SubclinicalKetosisState::progress(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState
#ifdef _LOG
                                                          , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                          )
        {
            // Base state
            IllKetosisState::progress(simDate, random, pNewState
#ifdef _LOG
                                                          , log
#endif // _LOG            
                                                          );
            
            _pAppliesToDairyMammal->increaseG1ketosisCumulateDurationInTheLactation();
            // Is it the day for potential flareup ?
            if (isOutOfTime(simDate))
            {
                assert(pNewState == nullptr);
                if (_flareup)
                {
                    pNewState = new G2ClinicalKetosisState(simDate, _pAppliesToDairyMammal, random);
                }
                else
                {
                    pNewState = getEndIllKetosisState(simDate, random);   
                }
            }
        }

        G1SubclinicalKetosisState::~G1SubclinicalKetosisState()
        {

        }    
                    
        bool G1SubclinicalKetosisState::recurrencyIsPossible()
        {
            return true;
        }

#ifdef _LOG
            
        std::string G1SubclinicalKetosisState::getStrKetosisState()
        {
            if (_recurrence)
            {
                return "G1r";
            }
            else
            {
                return "G1";
            }
        }
#endif // _LOG                 
    }
}
}