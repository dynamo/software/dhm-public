#ifndef Health_DairyCowKetosisStates_G1SubclinicalKetosisState_h
#define Health_DairyCowKetosisStates_G1SubclinicalKetosisState_h

// project
#include "IllKetosisState.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"

namespace Health
{
namespace Ketosis
{
    namespace KetosisStates
    {
        class G1SubclinicalKetosisState : public IllKetosisState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
            bool _recurrence;
            bool _flareup;
        
        protected:
            bool recurrencyIsPossible() override;
            void concrete(){}; // To allow instanciation

        public:
            G1SubclinicalKetosisState(){};
            G1SubclinicalKetosisState(const boost::gregorian::date &beginDate, DataStructures::DairyMammal* pCow, bool recurrence, Tools::Random &random);
            inline FunctionalEnumerations::Health::KetosisSeverity getSeverity(){return FunctionalEnumerations::Health::KetosisSeverity::G1Ketosis;};
            virtual ~G1SubclinicalKetosisState();
            void considerCetodetectTest(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState) override;
            void considerHerdNavigatorTest(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState) override;
            void considerKetosisBloodTest(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState) override;
            void progress(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState
#ifdef _LOG
                                                              , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                              ) final;
            inline const std::map<unsigned int, float> &getKetosisMortalityRiskDependingLactationRankReferency() override {return FunctionalConstants::Health::G1_KETOSIS_MORTALITY_RISK_DEPENDING_LACTATION_RANK;}
            void getKetosisTBTPEffect(float &milkTBDelta, float &milkTPDelta);
            inline bool isCurrentlyG1ToG2KetosisCase() override {return true;}
        protected:
#ifdef _LOG
            std::string getStrKetosisState();
#endif // _LOG            
            void takeTreatment(const boost::gregorian::date &simDate, FunctionalEnumerations::Health::KetosisSeverity severity, bool &success, KetosisState* &pNewState, Tools::Random &random
#ifdef _LOG
                                                                                                    , std::string origin
#endif // _LOG            
                                                                                                    ) override; 
        };
    }
}
}
#endif // Health_DairyCowKetosisStates_G1SubclinicalKetosisState_h
