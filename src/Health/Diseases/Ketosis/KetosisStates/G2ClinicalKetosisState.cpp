#include "G2ClinicalKetosisState.h"

// project
//#include "./I_KetosisProneDairyCow.h"
#include "../../../../DataStructures/DairyMammal.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"
#include "../../../HealthManagement.h"

BOOST_CLASS_EXPORT(Health::Ketosis::KetosisStates::G2ClinicalKetosisState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Ketosis
{
    namespace KetosisStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(IllKetosisState); \
        ar & BOOST_SERIALIZATION_NVP(_dateForFirstTreatment); \
        ar & BOOST_SERIALIZATION_NVP(_dateForPotentialSecondTreatment); \

        IMPLEMENT_SERIALIZE_STD_METHODS(G2ClinicalKetosisState);

        G2ClinicalKetosisState::G2ClinicalKetosisState(const boost::gregorian::date &beginDate, DataStructures::DairyMammal* pCow, Tools::Random &random)
                : IllKetosisState(beginDate, pCow, FunctionalConstants::Health::KETOSIS_G2_MILK_PERCENT_QUANTITY_LOSS, FunctionalConstants::Health::KETOSIS_G2_MILK_QUANTITY_LOSS_DURATION)
        {
            pCow->notifyNewKetosisOccurence(beginDate, FunctionalEnumerations::Health::KetosisSeverity::G2Ketosis);
          
            // Duration
            boost::gregorian::date_duration duration = boost::gregorian::date_duration(FunctionalConstants::Health::CLINICAL_KETOSIS_DURATION);
            setDuration(duration);
                        
            // Treatment plan
            _dateForFirstTreatment = _beginDate + boost::gregorian::date_duration(1); // First treatment is the first day
            _dateForPotentialSecondTreatment = _predictedEndDate - boost::gregorian::date_duration(1); // Second treatment is the last day
        }

        void G2ClinicalKetosisState::progress(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState
#ifdef _LOG
                                                          , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                          )
        {
            IllKetosisState::progress(simDate, random, pNewState
#ifdef _LOG
                                                          , log
#endif // _LOG            
                                                          );
            
            _pAppliesToDairyMammal->increaseG2ketosisCumulateDurationInTheLactation();
            
 
            if (simDate == _dateForFirstTreatment or simDate == _dateForPotentialSecondTreatment)
            {
                bool success = false;
#ifdef _LOG
                std::string origin = "";
                if (simDate == _dateForFirstTreatment)
                {
                    origin = " first time";
                }
                else
                {
                    origin = " last time";
                }
#endif // _LOG            
                takeTreatment(simDate, FunctionalEnumerations::Health::KetosisSeverity::G2Ketosis, success, pNewState, random
#ifdef _LOG
                                                , origin
#endif // _LOG            
                                                );
                if (simDate == _dateForFirstTreatment and success)
                {
                    _predictedEndDate = simDate;
                }
            }
            if (isOutOfTime(simDate))
            {
                assert(pNewState == nullptr);
                pNewState = getEndIllKetosisState(simDate, random);
            }
        }
        
        G2ClinicalKetosisState::~G2ClinicalKetosisState()
        {

        }        
        
        void G2ClinicalKetosisState::getKetosisTBTPEffect(float &milkTBDelta, float &milkTPDelta)
        {
            milkTBDelta = FunctionalConstants::Health::KETOSIS_G2_MILK_TB_DELTA;
            milkTPDelta = FunctionalConstants::Health::KETOSIS_G2_MILK_TP_DELTA;
        }
        
#ifdef _LOG
        std::string G2ClinicalKetosisState::getStrKetosisState()
        {
            return "G2";
        }
#endif // _LOG            
    }
}
}