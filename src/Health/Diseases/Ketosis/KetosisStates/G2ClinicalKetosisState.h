#ifndef Health_DairyCowKetosisStates_G2ClinicalKetosisState_h
#define Health_DairyCowKetosisStates_G2ClinicalKetosisState_h

// project
#include "IllKetosisState.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"

namespace Health
{
namespace Ketosis
{
    namespace KetosisStates
    {
        class G2ClinicalKetosisState : public IllKetosisState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        private :
            boost::gregorian::date _dateForFirstTreatment;
            boost::gregorian::date _dateForPotentialSecondTreatment;

        protected:
            void concrete(){}; // To allow instanciation

        public:
            G2ClinicalKetosisState(){};
            G2ClinicalKetosisState(const boost::gregorian::date &beginDate, DataStructures::DairyMammal* pCow, Tools::Random &random);
            virtual ~G2ClinicalKetosisState();
            inline FunctionalEnumerations::Health::KetosisSeverity getSeverity(){return FunctionalEnumerations::Health::KetosisSeverity::G2Ketosis;};
            void progress(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState
#ifdef _LOG
                                                              , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                              ) final;
            inline const std::map<unsigned int, float> &getKetosisMortalityRiskDependingLactationRankReferency() override {return FunctionalConstants::Health::G2_KETOSIS_MORTALITY_RISK_DEPENDING_LACTATION_RANK;}
            void getKetosisTBTPEffect(float &milkTBDelta, float &milkTPDelta) override;
            inline bool isCurrentlyG1ToG2KetosisCase() override {return true;}
        protected:
#ifdef _LOG
            std::string getStrKetosisState();
#endif // _LOG            
        };
        }
    }
}
#endif // Health_DairyCowKetosisStates_G2ClinicalKetosisState_h
