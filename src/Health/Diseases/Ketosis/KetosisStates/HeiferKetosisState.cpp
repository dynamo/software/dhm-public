#include "HeiferKetosisState.h"

// project

BOOST_CLASS_EXPORT(Health::Ketosis::KetosisStates::HeiferKetosisState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Ketosis
{
    namespace KetosisStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(G0UnsensitiveKetosisState); \

        IMPLEMENT_SERIALIZE_STD_METHODS(HeiferKetosisState);

        HeiferKetosisState::HeiferKetosisState(const boost::gregorian::date &beginDate, DataStructures::DairyMammal* pCow) : G0UnsensitiveKetosisState(beginDate, pCow)
        {
            _mastitisRiskInvolved = 1.0f;
        }

        HeiferKetosisState::~HeiferKetosisState()
        {
        }
        
#ifdef _LOG
        std::string HeiferKetosisState::getStrKetosisState()
        {
            return "Heifer";
        }
#endif // _LOG            
    }
}
}
