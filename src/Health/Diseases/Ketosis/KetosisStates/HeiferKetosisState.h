#ifndef Health_DairyCowKetosisStates_HeiferKetosisState_h
#define Health_DairyCowKetosisStates_HeiferKetosisState_h

// project
#include "G0UnsensitiveKetosisState.h"

namespace Health
{
namespace Ketosis
{
    namespace KetosisStates
    {
        class HeiferKetosisState : public G0UnsensitiveKetosisState
        {         
            DECLARE_SERIALIZE_STD_METHODS;

        protected:
            HeiferKetosisState(){};
            void concrete(){}; // To allow instanciation
             
        public:
            HeiferKetosisState(const boost::gregorian::date &beginDate, DataStructures::DairyMammal* pCow);
            virtual ~HeiferKetosisState();
            
#ifdef _LOG
            std::string getStrKetosisState() override;
#endif // _LOG            
        };

    }
}
}

#endif // Health_DairyCowKetosisStates_HeiferKetosisState_h
