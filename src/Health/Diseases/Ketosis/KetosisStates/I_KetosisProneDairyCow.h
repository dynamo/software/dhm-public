//#ifndef Health_CowKetosisStates_I_KetosisProneDairyCow_h
//#define Health_CowKetosisStates_I_KetosisProneDairyCow_h
//
//// boost
//
//// project
//#include "../../../../ExchangeInfoStructures/FunctionalEnumerations.h"
//
//namespace Tools
//{
//    class Random;
//}
//namespace ExchangeInfoStructures
//{
//    namespace Parameters
//    {
//        class TreatmentTypeParameters;
//    }
//    class MilkProductCharacteristic;
//}
//namespace Health
//{
//namespace Ketosis
//{
//    namespace KetosisStates
//    {
//        //class KetosisState;
//   
//        class I_KetosisProneDairyCow
//        {
//        public:
//            
//            virtual ~I_KetosisProneDairyCow() { }
//
//            virtual bool isInKetosisRiskPeriod() = 0;
//            virtual unsigned int getDurationUntilEndKetosisRisk() = 0;
//            virtual void notifyNewKetosisOccurence(const boost::gregorian::date &date, FunctionalEnumerations::Health::KetosisSeverity severity) = 0;
//            virtual ExchangeInfoStructures::Parameters::TreatmentTypeParameters &takeKetosisTreatment(const boost::gregorian::date &date, FunctionalEnumerations::Health::KetosisSeverity severity, bool &success
//#ifdef _LOG
//                                                                                                    , std::string origin
//#endif // _LOG            
//                                                                                                    ) = 0;
//            virtual float getKetosisRisk(const boost::gregorian::date &beginDate) = 0;   
//            virtual float getBasisKetosisIncidence() = 0;   
//            virtual unsigned int getSubclinicalKetosisDuration() = 0;   
//            virtual void increaseG1ketosisCumulateDurationInTheLactation() = 0;
//            virtual void increaseG2ketosisCumulateDurationInTheLactation() = 0;
//            virtual void considereG1KetosisSuccessfulTreatmentDuringTheLactation() = 0;
//            virtual boost::gregorian::date &getEndKetosisMilkQuantityEffect() = 0;
//            virtual float &getKetosisMilkQuantityFactorEffect() = 0;
//
//            virtual unsigned int getLactationRankWithLimitForKetosis(unsigned int limit) = 0;
//            virtual FunctionalEnumerations::Reproduction::DairyCowParity getParityForKetosis() = 0;
//            virtual bool hadKetosisCaseDuringPreviousLactation() = 0;
//            virtual bool willHaveKetosisRecurrency() = 0;
//            virtual void diesNowBecauseOfKetosis(
//#ifdef _LOG                   
//            const boost::gregorian::date &date
//#endif // _LOG            
//                                            ) = 0;
//#ifdef _LOG
//            virtual unsigned int getIdForKetosis() = 0;
//#endif // _LOG            
//        };
//    } /* End of namespace KetosisStates */
//} 
//}
//#endif // Health_CowKetosisStates_I_KetosisProneDairyCow_h
