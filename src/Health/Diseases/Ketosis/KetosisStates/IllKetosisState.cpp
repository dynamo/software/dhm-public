#include "IllKetosisState.h"

// project
//#include "I_KetosisProneDairyCow.h"
#include "../../../../DataStructures/DairyMammal.h"
#include "G0SensitiveKetosisState.h"
#include "G0UnsensitiveKetosisState.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(Health::Ketosis::KetosisStates::IllKetosisState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Ketosis
{
    namespace KetosisStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(KetosisState); \
        ar & BOOST_SERIALIZATION_NVP(_endIsMedicalHealing); \

        IMPLEMENT_SERIALIZE_STD_METHODS(IllKetosisState);

        IllKetosisState::IllKetosisState(const boost::gregorian::date &beginDate, DataStructures::DairyMammal* pCow, const float milkPercentQuantityLoss, const unsigned int milkQuantityLossDuration) : KetosisState(beginDate, pCow)
        {
            // Milk quantity effect end
            boost::gregorian::date &endDate = pCow->getEndKetosisMilkQuantityEffect();
            endDate = beginDate + boost::gregorian::date_duration(milkQuantityLossDuration);
            
            // Milk percent quantity factor
            float &quantityFactor = pCow->getKetosisMilkQuantityFactorEffect();
            quantityFactor = (100.0f + milkPercentQuantityLoss) / 100.0f;
            
            // Mastitis risk based on ketosis incidence, Specifically Ketosis case, calculation details in "KetosisEffects.xlsx" file, tab "Autres maladies"
            _mastitisRiskInvolved = FunctionalConstants::Health::KETOSIS_MASTITIS_RISK - (FunctionalConstants::Health::KETOSIS_MASTITIS_RISK - 1) * pCow->getBasisKetosisIncidence();
        }

        void IllKetosisState::progress(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState
#ifdef _LOG
                                                          , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                          )
        {
            KetosisState::progress(simDate, random, pNewState
#ifdef _LOG
                                                          , log
#endif // _LOG            
                                                          );
        }
        
        KetosisState* IllKetosisState::getEndIllKetosisState(const boost::gregorian::date &simDate, Tools::Random &random)
        {
            
            unsigned int protectionDuration = getProtectionDuration(simDate);
            if (protectionDuration < FunctionalConstants::Health::DELAY_AFTER_KETOSIS_END_FOR_SENSITIVITY) protectionDuration = FunctionalConstants::Health::DELAY_AFTER_KETOSIS_END_FOR_SENSITIVITY;
            
            if (not _endIsMedicalHealing)
            {
                // Death ?
                float deathRisk = 0.0f;
                const std::map<unsigned int, float>::const_iterator it = getKetosisMortalityRiskDependingLactationRankReferency().find(_pAppliesToDairyMammal->getLactationRankWithLimitForKetosis(4));
                if (it != getKetosisMortalityRiskDependingLactationRankReferency().end())
                {
                    deathRisk = it->second;
                }
                if (random.ran_bernoulli(deathRisk))
                {
                    // Death
                    _pAppliesToDairyMammal->diesNowBecauseOfKetosis  (           
#ifdef _LOG                   
                                                                    simDate
#endif // _LOG            
                                                                    );
                    return this;
                }
            }
            
            // Spontaneous or medical healing
            return new G0UnsensitiveKetosisState(simDate, _pAppliesToDairyMammal, protectionDuration, random);
        }
        
        void IllKetosisState::takeTreatment(const boost::gregorian::date &simDate, FunctionalEnumerations::Health::KetosisSeverity severity, bool &success, KetosisState* &pNewState, Tools::Random &random
#ifdef _LOG
                                                                                                    , std::string origin
#endif // _LOG            
                                                                                                    )
        {
            // Base
            KetosisState::takeTreatment(simDate, severity, success, pNewState, random
#ifdef _LOG
                                                                                                    , origin
#endif // _LOG            
                                                                                                    );
            _endIsMedicalHealing = success;
            // Effect management
            if (success and _predictedEndTreatmentEffectDate < _predictedEndDate)
            {
               _predictedEndDate = _predictedEndTreatmentEffectDate;
            }
            else
            {
                _predictedEndDate -= boost::gregorian::date_duration(FunctionalConstants::Health::KETOSIS_DURATION_REDUCTION_WITH_TREATMENT);
            }
        }

        IllKetosisState::~IllKetosisState()
        {

        }               
    }
}
}