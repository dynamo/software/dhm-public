#ifndef Health_DairyCowKetosisStates_IllKetosisState_h
#define Health_DairyCowKetosisStates_IllKetosisState_h

// project
#include "KetosisState.h"

namespace Health
{
namespace Ketosis
{
    namespace KetosisStates
    {
        class IllKetosisState : public KetosisState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        protected:
            bool _endIsMedicalHealing = false;
            
            KetosisState* getEndIllKetosisState(const boost::gregorian::date &simDate, Tools::Random &random);
        public:
            IllKetosisState(){};
            IllKetosisState(const boost::gregorian::date &beginDate, DataStructures::DairyMammal* pCow, const float milkPercentQuantityLoss, const unsigned int milkQuantityLossDuration);
            virtual ~IllKetosisState();
            virtual void takeTreatment(const boost::gregorian::date &simDate, FunctionalEnumerations::Health::KetosisSeverity severity, bool &success, KetosisState* &pNewState, Tools::Random &random
#ifdef _LOG
                                                                                                    , std::string origin
#endif // _LOG            
                                                                                                    ) override; 

            virtual void progress(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState
#ifdef _LOG
                                                              , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                              ) override;            
            inline bool isIll() override {return true;}
            virtual const std::map<unsigned int, float> &getKetosisMortalityRiskDependingLactationRankReferency() = 0;
        };
    }
}
}
#endif // Health_DairyCowKetosisStates_IllKetosisState_h
