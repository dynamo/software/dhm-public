#include "KetosisState.h"

// boost

// project
//#include "I_KetosisProneDairyCow.h"
#include "../../../../DataStructures/DairyMammal.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"
#include "../../../../ResultStructures/DayAnimalLogInformations.h"

BOOST_CLASS_EXPORT(Health::Ketosis::KetosisStates::KetosisState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Ketosis
{
    namespace KetosisStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_NVP(_beginDate); \
        ar & BOOST_SERIALIZATION_NVP(_predictedEndDate); \
        ar & BOOST_SERIALIZATION_NVP(_pAppliesToDairyMammal); \
        ar & BOOST_SERIALIZATION_NVP(_treatmentTypeUsed); \
        ar & BOOST_SERIALIZATION_NVP(_predictedEndTreatmentEffectDate); \
        ar & BOOST_SERIALIZATION_NVP(_predictedEndTreatmentProtectionDate); \

        IMPLEMENT_SERIALIZE_STD_METHODS(KetosisState);
        
        KetosisState::KetosisState(const boost::gregorian::date &beginDate, DataStructures::DairyMammal* pCow)
        {
            _beginDate = beginDate;
                        
            setDurationInfinite();
            
            _pAppliesToDairyMammal = pCow;
            
            // Mastitis risk based on ketosis incidence, Generally not Ketosis case, calculation details in "KetosisEffects.xlsx" file, tab "Autres maladies"*
            _mastitisRiskInvolved = 1.0f - (FunctionalConstants::Health::KETOSIS_MASTITIS_RISK - 1.0f) * pCow->getBasisKetosisIncidence();
                        
            // By default, the date of the end of treatment effect and protection are not set
            _predictedEndTreatmentEffectDate = boost::gregorian::date(boost::gregorian::not_a_date_time);
            _predictedEndTreatmentProtectionDate = boost::gregorian::date(boost::gregorian::not_a_date_time);
        }
        
        void KetosisState::progress(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState
#ifdef _LOG
                                                          , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                          )
        {
#ifdef _LOG
            log.ketosisState = getStrKetosisState();
#endif // _LOG            
        }

        bool KetosisState::isOutOfTime(const boost::gregorian::date &baseDate)
        {
            return baseDate >= _predictedEndDate;
        }
        
        KetosisState::~KetosisState()
        {
        }
                
        void KetosisState::getKetosisTBTPEffect(float &milkTBDelta, float &milkTPDelta)
        {
            milkTBDelta = 0.0f;
            milkTPDelta = 0.0f;
        }
        
        boost::gregorian::date &KetosisState::getBeginDate()
        {
            return _beginDate;
        }
            
        void KetosisState::setDurationInfinite()
        {
            // General concept
            boost::gregorian::date_duration duration = boost::gregorian::date_duration(boost::gregorian::pos_infin);
            
            setDuration(duration);
        }

        void KetosisState::setDuration(boost::gregorian::date_duration &duration)
        {
            _predictedEndDate = _beginDate + duration;
        }
               
        void KetosisState::takeTreatment(const boost::gregorian::date &simDate, FunctionalEnumerations::Health::KetosisSeverity severity, bool &success, KetosisState* &pNewState, Tools::Random &random
#ifdef _LOG
                                                                                                    , std::string origin
#endif // _LOG            
                                                                                                    )
        {
            _treatmentTypeUsed = _pAppliesToDairyMammal->takeKetosisTreatment(simDate, severity, success
#ifdef _LOG
                                                                                                    , origin
#endif // _LOG            
                                                                                                    );
            _predictedEndTreatmentEffectDate = simDate + boost::gregorian::days(_treatmentTypeUsed.effectDelayMeanOrMin);
            _predictedEndTreatmentProtectionDate = simDate + boost::gregorian::days(_treatmentTypeUsed.protectDuration);
        }
        
        void KetosisState::takeTreatmentIfOutOfEffect(const boost::gregorian::date &simDate, FunctionalEnumerations::Health::KetosisSeverity severity, KetosisState* &pNewState, Tools::Random &random
#ifdef _LOG
                                                                                                    , std::string origin
#endif // _LOG            
                                                                                                    )
        {
            if (not treatmentAsCurrentlyEffect(simDate))
            {
                bool success = false;
                takeTreatment(simDate, severity, success, pNewState, random
#ifdef _LOG
                                                , origin
#endif // _LOG            
                                                );
            }
        }
        
        unsigned int KetosisState::getProtectionDuration(const boost::gregorian::date &simDate)
        {
            if (not _predictedEndTreatmentProtectionDate.is_not_a_date())
            {
                if (_predictedEndTreatmentProtectionDate > simDate)
                {   
                    return (_predictedEndTreatmentProtectionDate - simDate).days();
                }
            }
            return 0;
        }

        bool KetosisState::treatmentAsCurrentlyEffect(const boost::gregorian::date &simDate)
        {
            return _predictedEndTreatmentEffectDate != boost::gregorian::date(boost::gregorian::not_a_date_time) and (_predictedEndTreatmentEffectDate >= simDate);
        }

        float KetosisState::getMastitisRisk()
        {
            return _mastitisRiskInvolved;
        }       
    } /* End of namespace DataStructures::States */
}
}