#ifndef Health_DairyCowKetosisStates_KetosisState_h
#define Health_DairyCowKetosisStates_KetosisState_h

// boost

// project
#include "../../../../Tools/Serialization.h"
#include "../../../../ExchangeInfoStructures/MilkProductCharacteristic.h"
#include "../../../../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "../../../../Tools/Random.h"
#include "../../../../ExchangeInfoStructures/TreatmentTypeParameters.h"
namespace Results
{
#ifdef _LOG
    class DayAnimalLogInformations;
#endif // _LOG            
}

namespace DataStructures
{
    class DairyMammal;
}

namespace Health
{
namespace Ketosis
{
    namespace KetosisStates
    {
        //class I_KetosisProneDairyCow;
        
        class KetosisState
        {
        DECLARE_SERIALIZE_STD_METHODS;

        protected:
            boost::gregorian::date _beginDate;
            boost::gregorian::date _predictedEndDate;
            
            float _mastitisRiskInvolved = 0.0f;

            DataStructures::DairyMammal* _pAppliesToDairyMammal;

            ExchangeInfoStructures::Parameters::TreatmentTypeParameters _treatmentTypeUsed;
            boost::gregorian::date _predictedEndTreatmentEffectDate;
            boost::gregorian::date _predictedEndTreatmentProtectionDate;
            
            KetosisState(const boost::gregorian::date &beginDate, DataStructures::DairyMammal* pCow);
            KetosisState(){};
            virtual void concrete() = 0; // To ensure that it will not be instantiated
            bool isOutOfTime(const boost::gregorian::date &baseDate);
            bool treatmentAsCurrentlyEffect(const boost::gregorian::date &simDate);
            virtual void takeTreatment(const boost::gregorian::date &simDate, FunctionalEnumerations::Health::KetosisSeverity severity, bool &success, KetosisState* &pNewState, Tools::Random &random
#ifdef _LOG
                                                                                                    , std::string origin
#endif // _LOG            
                                                                                                    );
            void takeTreatmentIfOutOfEffect(const boost::gregorian::date &simDate, FunctionalEnumerations::Health::KetosisSeverity severity, KetosisState* &pNewState, Tools::Random &random
#ifdef _LOG
                                                                                                    , std::string origin
#endif // _LOG            
                                                                                                    );
            unsigned int getProtectionDuration(const boost::gregorian::date &simDate);

        public:
            virtual ~KetosisState();
            boost::gregorian::date &getBeginDate();
            void setDurationInfinite();
            void setDuration(boost::gregorian::date_duration &duration);
            
            virtual void progress(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState
#ifdef _LOG
                                                              , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                              );
            virtual bool recurrencyIsPossible(){return false;}
            virtual bool isIll(){return false;}
            virtual inline bool isCurrentlyG1ToG2KetosisCase() {return false;}
            virtual inline void enterInKetosisRiskPeriod(const boost::gregorian::date &date, KetosisState* &pNewState){};
            virtual inline void exitFromKetosisRiskPeriod(const boost::gregorian::date &date, KetosisState* &pNewState){};
            virtual FunctionalEnumerations::Health::KetosisSeverity getSeverity() = 0;
            virtual inline void considerCetodetectTest(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState){}; // Generally nothing to do
            virtual inline void considerHerdNavigatorTest(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState){}; // Generally nothing to do
            virtual inline void considerKetosisBloodTest(const boost::gregorian::date &simDate, Tools::Random &random, KetosisState* &pNewState){}; // Generally nothing to do
            float getMastitisRisk();
            virtual void getKetosisTBTPEffect(float &milkTBDelta, float &milkTPDelta);
#ifdef _LOG
            virtual std::string getStrKetosisState() = 0;
#endif // _LOG            
        };
    }
} /* End of namespace Health */
}
#endif // Health_DairyCowKetosisStates_KetosisState_h
