#include "G0SensitiveLamenessState.h"

// project
//#include "I_LamenessProneFoots.h"
#include "../../../../DataStructures/Foots.h"
#include "../../../HealthManagement.h"

BOOST_CLASS_EXPORT(Health::Lameness::LamenessStates::G0SensitiveLamenessState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Lameness
{
    namespace LamenessStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(LamenessState); \

        IMPLEMENT_SERIALIZE_STD_METHODS(G0SensitiveLamenessState);

        G0SensitiveLamenessState::G0SensitiveLamenessState(const boost::gregorian::date &beginDate, DataStructures::Foots* pFoots, FunctionalEnumerations::Health::LamenessInfectiousStateType lamenessInfectiousStatus) : LamenessState(beginDate, pFoots, lamenessInfectiousStatus)
        {
        }

        void G0SensitiveLamenessState::progress(const boost::gregorian::date &simDate, Tools::Random &random, LamenessState* &pNewState
#ifdef _LOG
                                                          , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                          )
        {
            LamenessState::progress(simDate, random, pNewState
#ifdef _LOG
                                                          , log
#endif // _LOG            
                                                          );
            // Do we have a new Ketosis occurency ?
            HealthManagement::manageNewLamenessOccurency(simDate, _pAppliesToFoots, _lamenessInfectiousType, random, pNewState);
        }

        G0SensitiveLamenessState::~G0SensitiveLamenessState()
        {
        }
                
#ifdef _LOG
        std::string G0SensitiveLamenessState::getStrLamenessState()
        {
            return "G0s";
        }
#endif // _LOG            
    }
}
}
