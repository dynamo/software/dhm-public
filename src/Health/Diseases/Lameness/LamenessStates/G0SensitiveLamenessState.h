#ifndef Health_DairyCowLamenessStates_G0SensitiveLamenessState_h
#define Health_DairyCowLamenessStates_G0SensitiveLamenessState_h

// project
#include "LamenessState.h"

namespace Health
{
namespace Lameness
{
    namespace LamenessStates
    {
        class G0SensitiveLamenessState : public LamenessState
        {
            DECLARE_SERIALIZE_STD_METHODS;

        protected:
            G0SensitiveLamenessState(){};
            void concrete(){}; // To allow instanciation
             
        public:
            G0SensitiveLamenessState(const boost::gregorian::date &beginDate, DataStructures::Foots* pFoots, FunctionalEnumerations::Health::LamenessInfectiousStateType lamenessInfectiousStatus);
            virtual ~G0SensitiveLamenessState();
            inline FunctionalEnumerations::Health::LamenessSeverity getSeverity(){return FunctionalEnumerations::Health::LamenessSeverity::G0Lameness;};
            void progress(const boost::gregorian::date &simDate, Tools::Random &random, LamenessState* &pNewState
#ifdef _LOG
                                                              , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                              ) final;
#ifdef _LOG
            std::string getStrLamenessState();
#endif // _LOG
        };
        }
    }
}
#endif // Health_DairyCowLamenessStates_G0SensitiveLamenessState_h
