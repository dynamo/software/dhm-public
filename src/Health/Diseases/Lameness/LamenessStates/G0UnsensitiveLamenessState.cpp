#include "G0UnsensitiveLamenessState.h"

// project
//#include "I_LamenessProneFoots.h"
#include "../../../../DataStructures/Foots.h"
#include "G0SensitiveLamenessState.h"

BOOST_CLASS_EXPORT(Health::Lameness::LamenessStates::G0UnsensitiveLamenessState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Lameness
{
    namespace LamenessStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(LamenessState); \

        IMPLEMENT_SERIALIZE_STD_METHODS(G0UnsensitiveLamenessState);
        
        G0UnsensitiveLamenessState::G0UnsensitiveLamenessState(const boost::gregorian::date &beginDate, DataStructures::Foots* pFoots, FunctionalEnumerations::Health::LamenessInfectiousStateType lamenessInfectiousStatus, unsigned int durationBeforeSensibility) : LamenessState(beginDate, pFoots, lamenessInfectiousStatus)
        {
            if (durationBeforeSensibility != INT_MAX)
            {
                _predictedEndDate = beginDate + boost::gregorian::date_duration(durationBeforeSensibility);
            }
        }
        
        G0UnsensitiveLamenessState::~G0UnsensitiveLamenessState()
        {
        }
        
        void G0UnsensitiveLamenessState::progress(const boost::gregorian::date &simDate, Tools::Random &random, LamenessState* &pNewState
#ifdef _LOG
                                                          , Results::DayAnimalLogInformations &log
#endif // _LOG
                                                          )
        {
            LamenessState::progress(simDate, random, pNewState
#ifdef _LOG
                                                          , log
#endif // _LOG            
            
                                                          );
            
            if (isOutOfTime(simDate))
            {
                getNewSensitiveState(simDate, pNewState);        
            }
        }

        void G0UnsensitiveLamenessState::becomeSensitive(const boost::gregorian::date &date, LamenessState* &pNewState)
        {
            if (_predictedEndDate.is_infinity() ) // only one time, after is managed by the dynamics
            {
                // Not protected state
                assert (pNewState == nullptr);
                getNewSensitiveState(date, pNewState);
            }
        }
        
        void G0UnsensitiveLamenessState::getNewSensitiveState(const boost::gregorian::date &date, LamenessState* &pNewState)
        {
            pNewState = new G0SensitiveLamenessState(date, _pAppliesToFoots, _lamenessInfectiousType);    
        }

#ifdef _LOG
        std::string G0UnsensitiveLamenessState::getStrLamenessState()
        {
            return "G0u";
        }
#endif // _LOG            
    }
}
}
