#ifndef Health_DairyCowLamenessStates_G0UnsensitiveLamenessState_h
#define Health_DairyCowLamenessStates_G0UnsensitiveLamenessState_h

// project
#include "LamenessState.h"

namespace Health
{
namespace Lameness
{
    namespace LamenessStates
    {
        class G0UnsensitiveLamenessState : public LamenessState
        {
            DECLARE_SERIALIZE_STD_METHODS;
        private:
        
        protected:
            G0UnsensitiveLamenessState(){};
            void concrete(){}; // To allow instanciation
             
        public:
            G0UnsensitiveLamenessState(const boost::gregorian::date &beginDate, DataStructures::Foots* pFoots, FunctionalEnumerations::Health::LamenessInfectiousStateType lamenessInfectiousStatus, unsigned int durationBeforeSensibility);
            virtual ~G0UnsensitiveLamenessState();
            inline FunctionalEnumerations::Health::LamenessSeverity getSeverity(){return FunctionalEnumerations::Health::LamenessSeverity::G0Lameness;};
            void progress(const boost::gregorian::date &simDate, Tools::Random &random, LamenessState* &pNewState
#ifdef _LOG
                                                              , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                              ) final;
            
            void becomeSensitive(const boost::gregorian::date &date, LamenessState* &pNewState) override;
            void getNewSensitiveState(const boost::gregorian::date &date, LamenessState* &pNewState);
#ifdef _LOG
            std::string getStrLamenessState();
#endif // _LOG            
        };
    }
}
}
#endif // Health_DairyCowLamenessStates_G0UnsensitiveLamenessState_h
