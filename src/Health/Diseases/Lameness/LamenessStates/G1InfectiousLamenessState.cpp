#include "G1InfectiousLamenessState.h"

// project
//#include "./I_LamenessProneFoots.h"
#include "../../../../DataStructures/Foots.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"
#include "../../../../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "G0UnsensitiveLamenessState.h"
#include "G2InfectiousLamenessState.h"

BOOST_CLASS_EXPORT(Health::Lameness::LamenessStates::G1InfectiousLamenessState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Lameness
{
    namespace LamenessStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(G1LamenessState); \

        IMPLEMENT_SERIALIZE_STD_METHODS(G1InfectiousLamenessState);

        G1InfectiousLamenessState::G1InfectiousLamenessState(const boost::gregorian::date &beginDate, DataStructures::Foots* pFoots)
                : G1LamenessState(beginDate, pFoots, FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)
        {
            pFoots->notifyNewG1InfectiousLamenessOccurence(beginDate);         
            setIllLamenessStateFutur(beginDate,
                          FunctionalConstants::Health::LAMENESS_MORTALITY_PROBA.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second.find(FunctionalEnumerations::Health::LamenessSeverity::G1Lameness)->second, // mortalityProba
                          FunctionalConstants::Health::LAMENESS_CULLING_PROBA.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second.find(FunctionalEnumerations::Health::LamenessSeverity::G1Lameness)->second, // cullingProba
                          FunctionalConstants::Health::LAMENESS_DETECTION_PROBA.find(_pAppliesToFoots->getLamenessDetectionMode())->second.find(FunctionalEnumerations::Health::LamenessSeverity::G1Lameness)->second, // detectionProba
                          FunctionalConstants::Health::LAMENESS_SPONTANEOUS_HEALING_PROBABILITY.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second.find(FunctionalEnumerations::Health::LamenessSeverity::G1Lameness)->second, // spontaneousHealingProba
                          FunctionalConstants::Health::LAMENESS_SPONTANEOUS_HEALING_DELAY.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second.find(FunctionalEnumerations::Health::LamenessSeverity::G1Lameness)->second, // spontaneousHealingDelay
                          FunctionalConstants::Health::LAMENESS_FLAREUP_MINIMUM_DELAY.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second, // minDurationBeforeFlareup
                          FunctionalConstants::Health::LAMENESS_FLAREUP_MAXIMUM_DELAY.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second, // maxDurationBeforeFlareup

                    true // flareup is the end
#ifdef _LOG
                         , pFoots->getIdForLameness()
#endif // _LOG 
                        );
        }
        
        LamenessState* G1InfectiousLamenessState::getUnsensitiveStateAfterHealing(const boost::gregorian::date &simDate)
        {
            return new G0UnsensitiveLamenessState(simDate, _pAppliesToFoots, getLamenessInfectiousStatus(), FunctionalConstants::Health::DELAY_AFTER_INFECTIOUS_HEALING_FOR_SENSIBILITY);
        }
        
        LamenessState* G1InfectiousLamenessState::getFlareupState(const boost::gregorian::date &simDate, bool tookFirstTreatment)
        {
            return new G2InfectiousLamenessState(simDate, boost::gregorian::date_duration(simDate - _beginIllDate), _pAppliesToFoots);
        }

        void G1InfectiousLamenessState::getLamenessTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability)
        {
            _pAppliesToFoots->getLamenessG1InfectiousTreatmentInformations(minEffectDelay, maxEffectDelay, firstCaseHealingProbability, otherCaseHealingProbability);
        }
        
        float G1InfectiousLamenessState::getFootBathDayHealingProbability(const boost::gregorian::date &date, unsigned int &durationBeforeEffect)
        {
            float proba = 0.0f;
            float dayRatio = 0.0f;
            if (_pAppliesToFoots->hasCurrentlyEfficientFootbath(date, dayRatio))
            {
                durationBeforeEffect = FunctionalConstants::Health::INFECTIOUS_LAMENESS_G1_FOOT_BATH_DURATION_BEFORE_CURATIVE_EFFECT;
                proba = FunctionalConstants::Health::INFECTIOUS_LAMENESS_G1_FOOT_BATH_CURATIVE_HEALTH_PROBABILITY * dayRatio;
            }
            return proba;
        }

        void G1InfectiousLamenessState::progress(const boost::gregorian::date &simDate, Tools::Random &random, LamenessState* &pNewState
#ifdef _LOG
                                                          , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                          )
        {
            // Base state
            G1LamenessState::progress(simDate, random, pNewState
#ifdef _LOG
                                                          , log
#endif // _LOG            
                                                          );
        }

        G1InfectiousLamenessState::~G1InfectiousLamenessState()
        {

        }    
        
        void G1InfectiousLamenessState::getLamenessBaseMilkEffect(float &baseQuantityLamenessDelta, float &baseTBLamenessDelta, float &baseTPLamenessDelta, FunctionalEnumerations::Population::Location currentLocation)
        {
            baseQuantityLamenessDelta = currentLocation == FunctionalEnumerations::Population::Location::fullPasture ? FunctionalConstants::Health::LAMENESS_PASTURE_I_G1_MILK_PERCENT_QUANTITY_LOSS :
                                                            currentLocation == FunctionalEnumerations::Population::Location::halfStabulationPasture ? FunctionalConstants::Health::LAMENESS_STABULATION_I_G1_MILK_PERCENT_QUANTITY_LOSS/2.0f : FunctionalConstants::Health::LAMENESS_STABULATION_I_G1_MILK_PERCENT_QUANTITY_LOSS;
            baseTBLamenessDelta = FunctionalConstants::Health::LAMENESS_I_G1_MILK_TB_DELTA;
            baseTPLamenessDelta = FunctionalConstants::Health::LAMENESS_I_G1_MILK_TP_DELTA;
        }
        
#ifdef _LOG           
        std::string G1InfectiousLamenessState::getStrLamenessState()
        {
            return "G1i";
        }
#endif // _LOG     
                    
    }
}
}