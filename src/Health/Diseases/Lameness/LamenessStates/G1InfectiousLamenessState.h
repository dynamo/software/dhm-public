#ifndef Health_DairyCowLamenessStates_G1InfectiousLamenessState_h
#define Health_DairyCowLamenessStates_G1InfectiousLamenessState_h

// project
#include "G1LamenessState.h"

namespace Health
{
namespace Lameness
{
    namespace LamenessStates
    {
        class G1InfectiousLamenessState : public G1LamenessState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        protected:
            LamenessState* getUnsensitiveStateAfterHealing(const boost::gregorian::date &simDate);
            LamenessState* getFlareupState(const boost::gregorian::date &simDate, bool tookFirstTreatment) override;
            void concrete(){}; // To allow instanciation

        public:
            G1InfectiousLamenessState(){};
            G1InfectiousLamenessState(const boost::gregorian::date &beginDate, DataStructures::Foots* pFoots);
            virtual ~G1InfectiousLamenessState();
            void progress(const boost::gregorian::date &simDate, Tools::Random &random, LamenessState* &pNewState
#ifdef _LOG
                                                              , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                              ) final;
            inline FunctionalEnumerations::Population::CullingStatus getCullingReason() {return FunctionalEnumerations::Population::CullingStatus::toCullDueToInfectiousLameness;}                     
            inline FunctionalEnumerations::Population::DeathReason getDeathReason() {return FunctionalEnumerations::Population::DeathReason::infectiousLamenessDeathReason;}
            void getLamenessBaseMilkEffect(float &baseQuantityLamenessDelta, float &baseTBLamenessDelta, float &baseTPLamenessDelta, FunctionalEnumerations::Population::Location currentLocation);
        protected:
#ifdef _LOG
            std::string getStrLamenessState();
#endif // _LOG       
            
            void getLamenessTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability);
            float getFootBathDayHealingProbability(const boost::gregorian::date &date, unsigned int &durationBeforeEffect) override;
        };
    }
}
}
#endif // Health_DairyCowLamenessStates_G1InfectiousLamenessState_h
