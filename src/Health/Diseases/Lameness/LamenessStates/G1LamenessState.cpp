#include "G1LamenessState.h"

// project
//#include "./I_LamenessProneFoots.h"
#include "../../../../DataStructures/Foots.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(Health::Lameness::LamenessStates::G1LamenessState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Lameness
{
    namespace LamenessStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(IllLamenessState); \

        IMPLEMENT_SERIALIZE_STD_METHODS(G1LamenessState);

        G1LamenessState::G1LamenessState(const boost::gregorian::date &beginDate, DataStructures::Foots* pFoots, FunctionalEnumerations::Health::LamenessInfectiousStateType lamenessInfectiousStatus)
                : IllLamenessState(beginDate, pFoots, lamenessInfectiousStatus)
        {
            _beginIllDate = beginDate;
        }
        
        void G1LamenessState::progress(const boost::gregorian::date &simDate, Tools::Random &random, LamenessState* &pNewState
#ifdef _LOG
                                                          , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                          )
        {
            // Base state
            IllLamenessState::progress(simDate, random, pNewState
#ifdef _LOG
                                                          , log
#endif // _LOG            
                                                          );   
        }

        G1LamenessState::~G1LamenessState()
        {

        }    
    }
}
}