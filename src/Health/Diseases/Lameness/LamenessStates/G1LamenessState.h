#ifndef Health_DairyCowLamenessStates_G1LamenessState_h
#define Health_DairyCowLamenessStates_G1LamenessState_h

// project
#include "IllLamenessState.h"

namespace Health
{
namespace Lameness
{
    namespace LamenessStates
    {
        class G1LamenessState : public IllLamenessState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        public:
            G1LamenessState(){};
            G1LamenessState(const boost::gregorian::date &beginDate, DataStructures::Foots* pFoots, FunctionalEnumerations::Health::LamenessInfectiousStateType lamenessInfectiousStatus);
            inline FunctionalEnumerations::Health::LamenessSeverity getSeverity(){return FunctionalEnumerations::Health::LamenessSeverity::G1Lameness;};
            virtual ~G1LamenessState();
            void progress(const boost::gregorian::date &simDate, Tools::Random &random, LamenessState* &pNewState
#ifdef _LOG
                                                              , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                              ) override;
            bool isG1Ill() override {return true;}
            inline void isIll(bool &G1Ill, bool &G2Ill, bool &detected) override {G1Ill = true; G2Ill = false; detected = isDetected();}

        protected:
        };
    }
}
}
#endif // Health_DairyCowLamenessStates_G1LamenessState_h
