#include "G1NonInfectiousLamenessState.h"

// project
//#include "./I_LamenessProneFoots.h"
#include "../../../../DataStructures/Foots.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"
#include "../../../../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "G0SensitiveLamenessState.h"
#include "G2NonInfectiousLamenessState.h"

BOOST_CLASS_EXPORT(Health::Lameness::LamenessStates::G1NonInfectiousLamenessState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Lameness
{
    namespace LamenessStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(G1LamenessState); \

        IMPLEMENT_SERIALIZE_STD_METHODS(G1NonInfectiousLamenessState);

        G1NonInfectiousLamenessState::G1NonInfectiousLamenessState(const boost::gregorian::date &beginDate, DataStructures::Foots* pFoots)
                : G1LamenessState(beginDate, pFoots, FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)
        {
            pFoots->notifyNewG1NonInfectiousLamenessOccurence(beginDate); 
            
            setIllLamenessStateFutur(beginDate,
                          FunctionalConstants::Health::LAMENESS_MORTALITY_PROBA.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second.find(FunctionalEnumerations::Health::LamenessSeverity::G1Lameness)->second, // mortalityProba
                          FunctionalConstants::Health::LAMENESS_CULLING_PROBA.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second.find(FunctionalEnumerations::Health::LamenessSeverity::G1Lameness)->second, // cullingProba
                          FunctionalConstants::Health::LAMENESS_DETECTION_PROBA.find(_pAppliesToFoots->getLamenessDetectionMode())->second.find(FunctionalEnumerations::Health::LamenessSeverity::G1Lameness)->second, // detectionProba
                          FunctionalConstants::Health::LAMENESS_SPONTANEOUS_HEALING_PROBABILITY.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second.find(FunctionalEnumerations::Health::LamenessSeverity::G1Lameness)->second, // spontaneousHealingProba
                          FunctionalConstants::Health::LAMENESS_SPONTANEOUS_HEALING_DELAY.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second.find(FunctionalEnumerations::Health::LamenessSeverity::G1Lameness)->second, // spontaneousHealingDelay
                          FunctionalConstants::Health::LAMENESS_FLAREUP_MINIMUM_DELAY.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second, // minDurationBeforeFlareup
                          FunctionalConstants::Health::LAMENESS_FLAREUP_MAXIMUM_DELAY.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second, // maxDurationBeforeFlareup
                          true // flareup is the end
#ifdef _LOG
                         , pFoots->getIdForLameness()
#endif // _LOG            
                        );
        }
                
        void G1NonInfectiousLamenessState::getLamenessTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability)
        {
            _pAppliesToFoots->getLamenessG1NonInfectiousTreatmentInformations(minEffectDelay, maxEffectDelay, firstCaseHealingProbability, otherCaseHealingProbability);
        }
        
        LamenessState* G1NonInfectiousLamenessState::getUnsensitiveStateAfterHealing(const boost::gregorian::date &simDate)
        {
            return new G0UnsensitiveLamenessState(simDate, _pAppliesToFoots, getLamenessInfectiousStatus(), FunctionalConstants::Health::DELAY_AFTER_NON_INFECTIOUS_HEALING_FOR_SENSIBILITY);
        }
        
        LamenessState* G1NonInfectiousLamenessState::getFlareupState(const boost::gregorian::date &simDate, bool tookFirstTreatment)
        {
            return new G2NonInfectiousLamenessState(simDate, boost::gregorian::date_duration(simDate - _beginIllDate), _pAppliesToFoots);
        }

        void G1NonInfectiousLamenessState::getLamenessBaseMilkEffect(float &baseQuantityLamenessDelta, float &baseTBLamenessDelta, float &baseTPLamenessDelta, FunctionalEnumerations::Population::Location currentLocation)
        {
            baseQuantityLamenessDelta = FunctionalConstants::Health::LAMENESS_NI_G1_MILK_PERCENT_QUANTITY_LOSS;
            baseTBLamenessDelta = FunctionalConstants::Health::LAMENESS_NI_G1_MILK_TB_DELTA;
            baseTPLamenessDelta = FunctionalConstants::Health::LAMENESS_NI_G1_MILK_TP_DELTA;
        }
        
        void G1NonInfectiousLamenessState::progress(const boost::gregorian::date &simDate, Tools::Random &random, LamenessState* &pNewState
#ifdef _LOG
                                                          , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                          )
        {
            // Base state
            G1LamenessState::progress(simDate, random, pNewState
#ifdef _LOG
                                                          , log
#endif // _LOG            
                                                          );           
        }

        G1NonInfectiousLamenessState::~G1NonInfectiousLamenessState()
        {

        }    
                            
#ifdef _LOG           
        std::string G1NonInfectiousLamenessState::getStrLamenessState()
        {
            return "G1ni";
        }
#endif // _LOG     
                    
    }
}
}