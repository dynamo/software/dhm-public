#ifndef Health_DairyCowLamenessStates_G1NonInfectiousLamenessState_h
#define Health_DairyCowLamenessStates_G1NonInfectiousLamenessState_h

// project
#include "G1LamenessState.h"

namespace Health
{
namespace Lameness
{
    namespace LamenessStates
    {
        class G1NonInfectiousLamenessState : public G1LamenessState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        protected:
            LamenessState* getUnsensitiveStateAfterHealing(const boost::gregorian::date &simDate);
            LamenessState* getFlareupState(const boost::gregorian::date &simDate, bool tookFirstTreatment) override;
            void concrete(){}; // To allow instanciation

        public:
            G1NonInfectiousLamenessState(){};
            G1NonInfectiousLamenessState(const boost::gregorian::date &beginDate, DataStructures::Foots* pFoots);
            virtual ~G1NonInfectiousLamenessState();
            void progress(const boost::gregorian::date &simDate, Tools::Random &random, LamenessState* &pNewState
#ifdef _LOG
                                                              , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                              ) final;
            inline FunctionalEnumerations::Population::CullingStatus getCullingReason() {return FunctionalEnumerations::Population::CullingStatus::toCullDueToNonInfectiousLameness;}
            inline FunctionalEnumerations::Population::DeathReason getDeathReason() {return FunctionalEnumerations::Population::DeathReason::nonInfectiousLamenessDeathReason;}
            void getLamenessBaseMilkEffect(float &baseQuantityLamenessDelta, float &baseTBLamenessDelta, float &baseTPLamenessDelta, FunctionalEnumerations::Population::Location currentLocation);
        protected:
#ifdef _LOG
            std::string getStrLamenessState();
#endif // _LOG     
            void getLamenessTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability);
        };
    }
}
}
#endif // Health_DairyCowLamenessStates_G1NonInfectiousLamenessState_h
