#include "G2InfectiousLamenessState.h"

// project
//#include "./I_LamenessProneFoots.h"
#include "../../../../DataStructures/Foots.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"
#include "../../../../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "G0UnsensitiveLamenessState.h"
#include "G0SensitiveLamenessState.h"

BOOST_CLASS_EXPORT(Health::Lameness::LamenessStates::G2InfectiousLamenessState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Lameness
{
    namespace LamenessStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(G2LamenessState); \

        IMPLEMENT_SERIALIZE_STD_METHODS(G2InfectiousLamenessState);

        G2InfectiousLamenessState::G2InfectiousLamenessState(const boost::gregorian::date &beginDate, boost::gregorian::date_duration previousG1Duration, DataStructures::Foots* pFoots)
                : G2LamenessState(beginDate, previousG1Duration, pFoots, FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)
        {
            pFoots->notifyNewG2InfectiousLamenessOccurence(beginDate);     
            setIllLamenessStateFutur(beginDate,
                          FunctionalConstants::Health::LAMENESS_MORTALITY_PROBA.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second.find(FunctionalEnumerations::Health::LamenessSeverity::G2Lameness)->second, // mortalityProba
                          FunctionalConstants::Health::LAMENESS_CULLING_PROBA.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second.find(FunctionalEnumerations::Health::LamenessSeverity::G2Lameness)->second, // cullingProba
                          FunctionalConstants::Health::LAMENESS_DETECTION_PROBA.find(_pAppliesToFoots->getLamenessDetectionMode())->second.find(FunctionalEnumerations::Health::LamenessSeverity::G2Lameness)->second, // detectionProba
                          FunctionalConstants::Health::LAMENESS_SPONTANEOUS_HEALING_PROBABILITY.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second.find(FunctionalEnumerations::Health::LamenessSeverity::G2Lameness)->second, // spontaneousHealingProba
                          FunctionalConstants::Health::LAMENESS_SPONTANEOUS_HEALING_DELAY.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second.find(FunctionalEnumerations::Health::LamenessSeverity::G2Lameness)->second, // spontaneousHealingDelay
                          FunctionalConstants::Health::LAMENESS_FLAREUP_MINIMUM_DELAY.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second, // minDurationBeforeFlareup
                          FunctionalConstants::Health::LAMENESS_FLAREUP_MAXIMUM_DELAY.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)->second, // maxDurationBeforeFlareup
                          false // flareup is not the end
#ifdef _LOG
                          , pFoots->getIdForLameness()
#endif // _LOG            
                                    );
        }

        LamenessState* G2InfectiousLamenessState::getUnsensitiveStateAfterHealing(const boost::gregorian::date &simDate)
        {
            return new G0UnsensitiveLamenessState(simDate, _pAppliesToFoots, getLamenessInfectiousStatus(), FunctionalConstants::Health::DELAY_AFTER_INFECTIOUS_HEALING_FOR_SENSIBILITY);
        }
        
        void G2InfectiousLamenessState::getLamenessTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability)
        {
            _pAppliesToFoots->getLamenessG2InfectiousTreatmentInformations(minEffectDelay, maxEffectDelay, firstCaseHealingProbability, otherCaseHealingProbability);
        }

        void G2InfectiousLamenessState::getLamenessBaseMilkEffect(float &baseQuantityLamenessDelta, float &baseTBLamenessDelta, float &baseTPLamenessDelta, FunctionalEnumerations::Population::Location currentLocation)
        {
            baseQuantityLamenessDelta = currentLocation == FunctionalEnumerations::Population::Location::fullPasture ? FunctionalConstants::Health::LAMENESS_PASTURE_I_G2_MILK_PERCENT_QUANTITY_LOSS :
                                                            currentLocation == FunctionalEnumerations::Population::Location::halfStabulationPasture ? FunctionalConstants::Health::LAMENESS_STABULATION_I_G2_MILK_PERCENT_QUANTITY_LOSS/2.0f : FunctionalConstants::Health::LAMENESS_STABULATION_I_G2_MILK_PERCENT_QUANTITY_LOSS;  
            baseTBLamenessDelta = FunctionalConstants::Health::LAMENESS_I_G2_MILK_TB_DELTA;
            baseTPLamenessDelta = FunctionalConstants::Health::LAMENESS_I_G2_MILK_TP_DELTA;
        }
        
        float G2InfectiousLamenessState::getFootBathDayHealingProbability(const boost::gregorian::date &date, unsigned int &durationBeforeEffect)
        {
            float proba = 0.0f;
            float dayRatio = 0.0f;
            if (_pAppliesToFoots->hasCurrentlyEfficientFootbath(date, dayRatio))
            {
                durationBeforeEffect = FunctionalConstants::Health::INFECTIOUS_LAMENESS_G2_FOOT_BATH_DURATION_BEFORE_CURATIVE_EFFECT;
                proba = FunctionalConstants::Health::INFECTIOUS_LAMENESS_G2_FOOT_BATH_CURATIVE_HEALTH_PROBABILITY * dayRatio;
            }
            return proba;
        }

        void G2InfectiousLamenessState::progress(const boost::gregorian::date &simDate, Tools::Random &random, LamenessState* &pNewState
#ifdef _LOG
                                                          , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                          )
        {
            // Base state
            G2LamenessState::progress(simDate, random, pNewState
#ifdef _LOG
                                                          , log
#endif // _LOG            
                                                          );           
        }

        G2InfectiousLamenessState::~G2InfectiousLamenessState()
        {

        }    
                    
#ifdef _LOG  
        std::string G2InfectiousLamenessState::getStrLamenessState()
        {
            return "G2i";
        }
#endif // _LOG                         
    }
}
}