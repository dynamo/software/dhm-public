#ifndef Health_DairyCowLamenessStates_G2LamenessState_h
#define Health_DairyCowLamenessStates_G2LamenessState_h

// project
#include "IllLamenessState.h"

namespace Health
{
namespace Lameness
{
    namespace LamenessStates
    {
        class G2LamenessState : public IllLamenessState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        protected:

        public:
            G2LamenessState(){};
            G2LamenessState(const boost::gregorian::date &beginDate, boost::gregorian::date_duration &previousG1Duration, DataStructures::Foots* pFoots, FunctionalEnumerations::Health::LamenessInfectiousStateType lamenessInfectiousStatus);
            inline FunctionalEnumerations::Health::LamenessSeverity getSeverity(){return FunctionalEnumerations::Health::LamenessSeverity::G2Lameness;};
            virtual ~G2LamenessState();
            void progress(const boost::gregorian::date &simDate, Tools::Random &random, LamenessState* &pNewState
#ifdef _LOG
                                                              , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                              ) override;
            bool isG2Ill() override {return true;}
            inline void isIll(bool &G1Ill, bool &G2Ill, bool &detected) override {G1Ill = false; G2Ill = true; detected = isDetected();}
        protected:
            bool inline flareUpIsPossible(){return false;}
        };
    }
}
}
#endif // Health_DairyCowLamenessStates_G2LamenessState_h
