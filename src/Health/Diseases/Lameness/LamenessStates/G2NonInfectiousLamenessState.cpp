#include "G2NonInfectiousLamenessState.h"

// project
//#include "./I_LamenessProneFoots.h"
#include "../../../../DataStructures/Foots.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"
#include "G0UnsensitiveLamenessState.h"
#include "G0SensitiveLamenessState.h"

BOOST_CLASS_EXPORT(Health::Lameness::LamenessStates::G2NonInfectiousLamenessState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Lameness
{
    namespace LamenessStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(G2LamenessState); \

        IMPLEMENT_SERIALIZE_STD_METHODS(G2NonInfectiousLamenessState);

        G2NonInfectiousLamenessState::G2NonInfectiousLamenessState(const boost::gregorian::date &beginDate, boost::gregorian::date_duration previousG1Duration, DataStructures::Foots* pFoots)
                : G2LamenessState(beginDate, previousG1Duration, pFoots, FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)
        {
            pFoots->notifyNewG2NonInfectiousLamenessOccurence(beginDate);   
            setIllLamenessStateFutur(beginDate,
                          FunctionalConstants::Health::LAMENESS_MORTALITY_PROBA.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second.find(FunctionalEnumerations::Health::LamenessSeverity::G2Lameness)->second, // mortalityProba
                          FunctionalConstants::Health::LAMENESS_CULLING_PROBA.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second.find(FunctionalEnumerations::Health::LamenessSeverity::G2Lameness)->second, // cullingProba
                          FunctionalConstants::Health::LAMENESS_DETECTION_PROBA.find(_pAppliesToFoots->getLamenessDetectionMode())->second.find(FunctionalEnumerations::Health::LamenessSeverity::G2Lameness)->second, // detectionProba
                          FunctionalConstants::Health::LAMENESS_SPONTANEOUS_HEALING_PROBABILITY.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second.find(FunctionalEnumerations::Health::LamenessSeverity::G2Lameness)->second, // spontaneousHealingProba
                          FunctionalConstants::Health::LAMENESS_SPONTANEOUS_HEALING_DELAY.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second.find(FunctionalEnumerations::Health::LamenessSeverity::G2Lameness)->second, // spontaneousHealingDelay
                          FunctionalConstants::Health::LAMENESS_FLAREUP_MINIMUM_DELAY.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second, // minDurationBeforeFlareup
                          FunctionalConstants::Health::LAMENESS_FLAREUP_MAXIMUM_DELAY.find(FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType)->second, // maxDurationBeforeFlareup
                          false // flareup is not the end
#ifdef _LOG
                         , pFoots->getIdForLameness()
#endif // _LOG            
                                    );
        }
        
        void G2NonInfectiousLamenessState::getLamenessTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability)
        {
            _pAppliesToFoots->getLamenessG2NonInfectiousTreatmentInformations(minEffectDelay, maxEffectDelay, firstCaseHealingProbability, otherCaseHealingProbability);
        }
        
        LamenessState* G2NonInfectiousLamenessState::getUnsensitiveStateAfterHealing(const boost::gregorian::date &simDate)
        {
            return new G0UnsensitiveLamenessState(simDate, _pAppliesToFoots, getLamenessInfectiousStatus(), FunctionalConstants::Health::DELAY_AFTER_NON_INFECTIOUS_HEALING_FOR_SENSIBILITY);
        }
        
        void G2NonInfectiousLamenessState::getLamenessBaseMilkEffect(float &baseQuantityLamenessDelta, float &baseTBLamenessDelta, float &baseTPLamenessDelta, FunctionalEnumerations::Population::Location currentLocation)
        {
            baseQuantityLamenessDelta = FunctionalConstants::Health::LAMENESS_NI_G2_MILK_PERCENT_QUANTITY_LOSS;
            baseTBLamenessDelta = FunctionalConstants::Health::LAMENESS_NI_G2_MILK_TB_DELTA;
            baseTPLamenessDelta = FunctionalConstants::Health::LAMENESS_NI_G2_MILK_TP_DELTA;
        }
        
        void G2NonInfectiousLamenessState::progress(const boost::gregorian::date &simDate, Tools::Random &random, LamenessState* &pNewState
#ifdef _LOG
                                                          , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                          )
        {
            // Base state
            G2LamenessState::progress(simDate, random, pNewState
#ifdef _LOG
                                                          , log
#endif // _LOG            
                                                          );
        }

        G2NonInfectiousLamenessState::~G2NonInfectiousLamenessState()
        {

        }    
                    
#ifdef _LOG           
        std::string G2NonInfectiousLamenessState::getStrLamenessState()
        {
            return "G2ni";
        }
#endif // _LOG     
                    
    }
}
}