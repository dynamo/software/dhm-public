#ifndef Health_DairyCowLamenessStates_G2NonInfectiousLamenessState_h
#define Health_DairyCowLamenessStates_G2NonInfectiousLamenessState_h

// project
#include "G2LamenessState.h"
#include "G0UnsensitiveLamenessState.h"

namespace Health
{
namespace Lameness
{
    namespace LamenessStates
    {
        class G2NonInfectiousLamenessState : public G2LamenessState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        protected:
            LamenessState* getUnsensitiveStateAfterHealing(const boost::gregorian::date &simDate);
            void concrete(){}; // To allow instanciation
            
        public:
            G2NonInfectiousLamenessState(){};
            G2NonInfectiousLamenessState(const boost::gregorian::date &beginDate, boost::gregorian::date_duration previousG1Duration, DataStructures::Foots* pFoots);
            virtual ~G2NonInfectiousLamenessState();
            void progress(const boost::gregorian::date &simDate, Tools::Random &random, LamenessState* &pNewState
#ifdef _LOG
                                                              , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                              ) final;
            inline FunctionalEnumerations::Population::CullingStatus getCullingReason() {return FunctionalEnumerations::Population::CullingStatus::toCullDueToNonInfectiousLameness;}
            inline FunctionalEnumerations::Population::DeathReason getDeathReason() {return FunctionalEnumerations::Population::DeathReason::nonInfectiousLamenessDeathReason;}
            void getLamenessBaseMilkEffect(float &baseQuantityLamenessDelta, float &baseTBLamenessDelta, float &baseTPLamenessDelta, FunctionalEnumerations::Population::Location currentLocation);
        protected:
#ifdef _LOG
            std::string getStrLamenessState();
#endif // _LOG  
            void getLamenessTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability);
        };
    }
}
}
#endif // Health_DairyCowLamenessStates_G2NonInfectiousLamenessState_h
