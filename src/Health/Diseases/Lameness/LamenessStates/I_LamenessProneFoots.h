//#ifndef Health_CowLamenessStates_I_LamenessProneFoots_h
//#define Health_CowLamenessStates_I_LamenessProneFoots_h
//
//// Boost
//#include <boost/date_time/gregorian/gregorian.hpp>
//
//// project
//#include "../../../../ExchangeInfoStructures/FunctionalEnumerations.h"
//#include "../../../../ExchangeInfoStructures/MilkProductCharacteristic.h"
//
//namespace Tools
//{
//    class Random;
//}
//
//namespace Health
//{
//namespace Lameness
//{
//    namespace LamenessStates
//    {
//        class I_LamenessProneFoots
//        {
//        public:
//            
//            virtual ~I_LamenessProneFoots() { }
//            
//            virtual Tools::Random &getRandom() = 0;
//            
//#ifdef _LOG
//            virtual unsigned int getUniqueIdForLameness() = 0;
//#endif // _LOG            
//            virtual float getYearInfectiousLamenessIncidence(FunctionalEnumerations::Health::LamenessInfectiousStateType lamenessInfectiousStatus) = 0;
//            virtual void notifyNewG1InfectiousLamenessOccurence(const boost::gregorian::date &date) = 0;
//            virtual void notifyNewG1NonInfectiousLamenessOccurence(const boost::gregorian::date &date) = 0;
//            virtual void notifyNewG2InfectiousLamenessOccurence(const boost::gregorian::date &date) = 0;
//            virtual void notifyNewG2NonInfectiousLamenessOccurence(const boost::gregorian::date &date) = 0;
//            virtual void notifyForLamenessSporadicDetectionOrVerificationToday(
//#ifdef _LOG                
//                                                    const boost::gregorian::date &date
//#endif // _LOG                
//                                                    ) = 0;
//            virtual float getLamenessRisk(const boost::gregorian::date &simDate, FunctionalEnumerations::Health::LamenessInfectiousStateType infectiousType) = 0;   
//            virtual FunctionalEnumerations::Population::Location getCurrentLocation() = 0;
//            virtual void getLamenessEffectOnMilkProduction(const boost::gregorian::date &date, ExchangeInfoStructures::MilkProductCharacteristic &mc) = 0;
//            virtual void diesNowBecauseOfLameness(
//#ifdef _LOG                   
//            const boost::gregorian::date &date, 
//#endif // _LOG            
//                                            FunctionalEnumerations::Population::DeathReason deathReason) = 0;
//            virtual void notifyMammalIsCulledForSevereLameness(const boost::gregorian::date &date, FunctionalEnumerations::Population::CullingStatus cs) = 0;
//            virtual FunctionalEnumerations::Health::LamenessDetectionMode getLamenessDetectionMode() = 0;
//#ifdef _LOG
//            virtual unsigned int getIdForLameness() = 0;
//#endif // _LOG            
//            virtual void getLamenessG1InfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability) = 0;
//            virtual void getLamenessG1NonInfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability) = 0;
//            virtual void getLamenessG2InfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability) = 0;
//            virtual void getLamenessG2NonInfectiousTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability) = 0;
//            virtual bool hasCurrentlyEfficientFootbath(const boost::gregorian::date &date, float &dayRatio) = 0;          
//        };
//    } /* End of namespace LamenessStates */
//} 
//}
//#endif // Health_CowLamenessStates_I_LamenessProneFoots_h
