#include "IllLamenessState.h"

// project
//#include "I_LamenessProneFoots.h"
#include "../../../../DataStructures/Foots.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(Health::Lameness::LamenessStates::IllLamenessState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Lameness
{
    namespace LamenessStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(LamenessState); \
        ar & BOOST_SERIALIZATION_NVP(_detected); \
        ar & BOOST_SERIALIZATION_NVP(_tookFirstTreatment); \
        ar & BOOST_SERIALIZATION_NVP(_previousNextTraitmentDate); \
        ar & BOOST_SERIALIZATION_NVP(_plannedDetectionDay); \
        ar & BOOST_SERIALIZATION_NVP(_willDeath); \
        ar & BOOST_SERIALIZATION_NVP(_willBeCulled); \
        ar & BOOST_SERIALIZATION_NVP(_willHeal); \
        ar & BOOST_SERIALIZATION_NVP(_willFlareup); \

        IMPLEMENT_SERIALIZE_STD_METHODS(IllLamenessState);

        IllLamenessState::IllLamenessState(const boost::gregorian::date &beginDate, DataStructures::Foots* pFoots, FunctionalEnumerations::Health::LamenessInfectiousStateType lamenessInfectiousStatus) : LamenessState(beginDate, pFoots, lamenessInfectiousStatus)
        {
            _mastitisRiskInvolved = FunctionalConstants::Health::LAMENESS_MASTITIS_RISK - (FunctionalConstants::Health::LAMENESS_MASTITIS_RISK - 1) * pFoots->getYearInfectiousLamenessIncidence(lamenessInfectiousStatus);
        }
        
        void IllLamenessState::setIllLamenessStateFutur(const boost::gregorian::date &date, float mortalityProba, float cullingProba, float detectionProba, float spontaneousHealingProba, unsigned int spontaneousHealingDelay, unsigned int minDurationBeforeFlareup, unsigned int maxDurationBeforeFlareup, bool theEndIsFlareup
#ifdef _LOG
                                                                                                                                                            , unsigned int id
#endif // _LOG            
                                                                                                                                                                             )
        {
            Tools::Random &random = _pAppliesToFoots->getRandom();
            if (random.ran_bernoulli(mortalityProba))
            {
                // Death 
                _willDeath = true;          
                unsigned int delay = random.rng_uniform_int(FunctionalConstants::Health::LAMENESS_DELAY_FOR_CULLING_OR_DEATH);
                _predictedEndDate = date + boost::gregorian::date_duration(delay + FunctionalConstants::Health::LAMENESS_DELAY_FOR_CULLING_OR_DEATH);
            }
            else if (random.ran_bernoulli(cullingProba))
            {
                // Culling 
                _willBeCulled = true;          
                unsigned int delay = random.rng_uniform_int(FunctionalConstants::Health::LAMENESS_DELAY_FOR_CULLING_OR_DEATH);
                _predictedEndDate = date + boost::gregorian::date_duration(delay + FunctionalConstants::Health::LAMENESS_DELAY_FOR_CULLING_OR_DEATH);
            }
            else if (random.ran_bernoulli(spontaneousHealingProba))
            {
                // Spontaneous 
                _willHeal = true;          
                _predictedEndDate = date + boost::gregorian::date_duration(spontaneousHealingDelay);
            }
            else if (theEndIsFlareup)
            {
                // Flareup 
                _willFlareup = true;          
                unsigned int delay = minDurationBeforeFlareup + random.rng_uniform_int(maxDurationBeforeFlareup - minDurationBeforeFlareup);
                _predictedEndDate = date + boost::gregorian::date_duration(delay);
            }
            
            // plan detection ?
            if (random.ran_bernoulli(detectionProba))
            {
                // will be detected 
                unsigned int delay = random.rng_uniform_int(FunctionalConstants::Health::DELAY_MAX_FOR_LAMENESS_DETECTION - FunctionalConstants::Health::DELAY_MIN_FOR_LAMENESS_DETECTION) + FunctionalConstants::Health::DELAY_MIN_FOR_LAMENESS_DETECTION;
                _plannedDetectionDay = date + boost::gregorian::date_duration(delay);
            }
            // else no end, infinite duration
        }

        void IllLamenessState::progress(const boost::gregorian::date &simDate, Tools::Random &random, LamenessState* &pNewState
#ifdef _LOG
                                                          , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                          )
        {
            LamenessState::progress(simDate, random, pNewState
#ifdef _LOG
                                                          , log
#endif // _LOG            
                                                          );
            
            if (simDate == _plannedDetectionDay)
            {
                _detected = true;
                _pAppliesToFoots->notifyForLamenessSporadicDetectionOrVerificationToday(
#ifdef _LOG                
                                                    simDate
#endif // _LOG                
                                                    );
            }
            if (isOutOfTime(simDate))
            {
                assert(pNewState == nullptr);
                if (_willDeath)
                {
                    // Death
                    _pAppliesToFoots->diesNowBecauseOfLameness  (           
#ifdef _LOG                   
                                                    simDate, 
#endif // _LOG            
                                                    getDeathReason());
                }
                else if (_willBeCulled)
                {
                    _pAppliesToFoots->notifyMammalIsCulledForSevereLameness(simDate, getCullingReason());
                }
                else if (_willHeal)
                {
                    pNewState = getUnsensitiveStateAfterHealing(simDate);

                }
                else if (_willFlareup)
                {
                    pNewState = getFlareupState(simDate, _tookFirstTreatment);
                }
                else
                {
                    // Time out without reason ?
                    std::string errorMessage = "IllLamenessState::progress : Time out without reason ";
                    throw std::runtime_error(errorMessage);
                }
            }
            else
            {
                // May be a healing by foot bath ?
                unsigned int durationBeforeEffect;
                if (random.ran_bernoulli(getFootBathDayHealingProbability(simDate, durationBeforeEffect)))
                {
                    // healing because of the foot bath
                    boost::gregorian::date footBathHealingDate = simDate + boost::gregorian::date_duration(durationBeforeEffect);
                    if (_willHeal)
                    {
                        // Healing was already planned, is this one before the planned one ?
                        if (_predictedEndDate > footBathHealingDate)
                        {
                            _predictedEndDate = footBathHealingDate;
                        }
                    }
                    else
                    {
                        // No healing was planned before, this one is to plan
                        _willHeal = true;          
                        _predictedEndDate = footBathHealingDate;
                    }
                }
            }
        }
        
        void IllLamenessState::takeTreatment(const boost::gregorian::date &date, Tools::Random &random)
        {
            _detected = true;
            _previousNextTraitmentDate = date + boost::gregorian::date_duration(FunctionalConstants::Health::DELAY_FOR_NEW_LAMENESS_TRAITMENT);
            
            // Curative Effect
            unsigned int minEffectDelay;
            unsigned int maxEffectDelay;
            float firstCaseHealingProbability;
            float otherCaseHealingProbability;
            getLamenessTreatmentInformations(minEffectDelay, maxEffectDelay, firstCaseHealingProbability, otherCaseHealingProbability);
            
            bool heal = false;
            if (not _tookFirstTreatment)
            {
                // First treatment
                heal = random.ran_bernoulli(firstCaseHealingProbability);
                _tookFirstTreatment = true;
            }
            else
            {
                // Second or more treatments
                heal = random.ran_bernoulli(otherCaseHealingProbability);
            }
            if (heal)
            {
                _willHeal = true;
                unsigned int delay = maxEffectDelay - minEffectDelay;
                if (delay == 0)
                {
                    // Min and max are equal
                    delay = minEffectDelay;
                }
                else
                {
                    delay = random.rng_uniform_int(delay) + minEffectDelay;
                }
                _predictedEndDate = date + boost::gregorian::date_duration(delay);
            }
        }
       
        unsigned int IllLamenessState::getCurrentDurationLamenessIll(const boost::gregorian::date &date)
        {
            return boost::gregorian::date_period(_beginIllDate, date).length().days();
        }
        
        float IllLamenessState::getLamenessOestrusDetectionFactor(FunctionalEnumerations::Reproduction::DetectionMode dm, bool first)
        {
            return (FunctionalConstants::Health::G1_G2_LAMENESS_OESTRUS_DETECTION_FACTOR.find(first)->second).find(dm)->second;
        }
        
        void IllLamenessState::getLamenessHighestMilkEffect(float &quantityLamenessDelta, float &TBLamenessDelta, float &TPLamenessDelta, FunctionalEnumerations::Population::Location currentLocation)
        {
            float baseQuantityLamenessDelta = 0.0f;
            float baseTBLamenessDelta = 0.0f;
            float baseTPLamenessDelta = 0.0f;
            getLamenessBaseMilkEffect(baseQuantityLamenessDelta, baseTBLamenessDelta, baseTPLamenessDelta, currentLocation);
            
            if (abs(baseQuantityLamenessDelta) > abs(quantityLamenessDelta)) quantityLamenessDelta = baseQuantityLamenessDelta;
            if (abs(baseTBLamenessDelta) > abs(TBLamenessDelta)) TBLamenessDelta = baseTBLamenessDelta;
            if (abs(baseTPLamenessDelta) > abs(TPLamenessDelta)) TPLamenessDelta = baseTPLamenessDelta;
        }
        
        IllLamenessState::~IllLamenessState()
        {

        }           
    }
}
}