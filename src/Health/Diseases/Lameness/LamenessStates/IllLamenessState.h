#ifndef Health_DairyCowLamenessStates_IllLamenessState_h
#define Health_DairyCowLamenessStates_IllLamenessState_h

// project
#include "LamenessState.h"

namespace Health
{
namespace Lameness
{
    namespace LamenessStates
    {
        class IllLamenessState : public LamenessState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
            
        private :
            bool _detected = false;

            boost::gregorian::date _plannedDetectionDay;
            bool _willDeath= false;
            bool _willBeCulled = false;
            bool _willHeal = false;
            bool _willFlareup = false;

        protected:
            boost::gregorian::date _beginIllDate;
            boost::gregorian::date _lastTraitmentDate;
            bool _tookFirstTreatment = false;          
            boost::gregorian::date _previousNextTraitmentDate;
            
            virtual LamenessState* getUnsensitiveStateAfterHealing(const boost::gregorian::date &simDate) = 0;
            virtual inline LamenessState* getFlareupState(const boost::gregorian::date &simDate, bool tookFirstTreatment){throw std::runtime_error("IllLamenessState::getFlareupState : inapropriate operation");}
            void setIllLamenessStateFutur(const boost::gregorian::date &date, float mortalityProba, float cullingProba, float detectionProba, float spontaneousHealingProba, unsigned int spontaneousHealingDelay, unsigned int minDurationBeforeFlareup, unsigned int maxDurationBeforeFlareup, bool theEndIsFlareup
#ifdef _LOG
                                                                                                                                                            , unsigned int id
#endif // _LOG            
                                                                                                                                                                             );

        public:
            IllLamenessState(){};
            IllLamenessState(const boost::gregorian::date &beginDate, DataStructures::Foots* pFoots, FunctionalEnumerations::Health::LamenessInfectiousStateType lamenessInfectiousStatus);
            virtual ~IllLamenessState();

            virtual void progress(const boost::gregorian::date &simDate, Tools::Random &random, LamenessState* &pNewState
#ifdef _LOG
                                                              , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                              ) override;
            virtual FunctionalEnumerations::Population::CullingStatus getCullingReason() = 0;
            virtual FunctionalEnumerations::Population::DeathReason getDeathReason() = 0;
            virtual void getLamenessBaseMilkEffect(float &baseQuantityLamenessDelta, float &baseTBLamenessDelta, float &baseTPLamenessDelta, FunctionalEnumerations::Population::Location currentLocation) = 0;
            unsigned int getCurrentDurationLamenessIll(const boost::gregorian::date &date) override;
            float getLamenessOestrusDetectionFactor(FunctionalEnumerations::Reproduction::DetectionMode dm, bool first) override;
            virtual void takeTreatment(const boost::gregorian::date &date, Tools::Random &random) override;  
            inline bool isDetected(){return _detected;}
            inline bool isIll() override {return true;}
       protected:
            void getLamenessHighestMilkEffect(float &quantityLamenessDelta, float &TBLamenessDelta, float &TPLamenessDelta, FunctionalEnumerations::Population::Location currentLocation) override;
            virtual void getLamenessTreatmentInformations(unsigned int &minEffectDelay, unsigned int &maxEffectDelay, float &firstCaseHealingProbability, float &otherCaseHealingProbability) = 0;
            virtual inline float getFootBathDayHealingProbability(const boost::gregorian::date &date, unsigned int &durationBeforeEffect){return 0.0f;}
         };
    }
}
}
#endif // Health_DairyCowLamenessStates_IllLamenessState_h
