#include "LamenessState.h"

// boost

// project
//#include "I_LamenessProneFoots.h"
#include "../../../../DataStructures/Foots.h"
#include "../../../../ResultStructures/DayAnimalLogInformations.h"
#include "G0UnsensitiveLamenessState.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(Health::Lameness::LamenessStates::LamenessState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Lameness
{
    namespace LamenessStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_NVP(_beginDate); \
        ar & BOOST_SERIALIZATION_NVP(_predictedEndDate); \
        ar & BOOST_SERIALIZATION_NVP(_pAppliesToFoots); \
        ar & BOOST_SERIALIZATION_NVP(_lamenessInfectiousType); \



        IMPLEMENT_SERIALIZE_STD_METHODS(LamenessState);
        
        LamenessState::LamenessState(const boost::gregorian::date &beginDate, DataStructures::Foots* pFoots, FunctionalEnumerations::Health::LamenessInfectiousStateType lamenessInfectiousStatus)
        {
            _beginDate = beginDate;
                        
            setDurationInfinite();
            
            _pAppliesToFoots = pFoots;
            _lamenessInfectiousType = lamenessInfectiousStatus;
            
            // Mastitis risk based on lameness incidence, Generally not Lameness case
            _mastitisRiskInvolved = 1.0f - (FunctionalConstants::Health::LAMENESS_MASTITIS_RISK - 1.0f) * pFoots->getYearInfectiousLamenessIncidence(lamenessInfectiousStatus);
        }
        
        void LamenessState::progress(const boost::gregorian::date &simDate, Tools::Random &random, LamenessState* &pNewState
#ifdef _LOG
                                                          , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                          )
        {
        }

        bool LamenessState::isOutOfTime(const boost::gregorian::date &baseDate)
        {
            return baseDate >= _predictedEndDate;
        }
        
        LamenessState::~LamenessState()
        {
        }
        
        boost::gregorian::date &LamenessState::getBeginDate()
        {
            return _beginDate;
        }
            
        void LamenessState::setDurationInfinite()
        {
            // General concept
            boost::gregorian::date_duration duration = boost::gregorian::date_duration(boost::gregorian::pos_infin);            
            setDuration(duration);
        }

        void LamenessState::setDuration(boost::gregorian::date_duration &duration)
        {
            _predictedEndDate = _beginDate + duration;
        }
        
        float LamenessState::getLamenessOestrusDetectionFactor(FunctionalEnumerations::Reproduction::DetectionMode dm, bool first)
        {
            return 1.0f;
        }

        FunctionalEnumerations::Health::LamenessInfectiousStateType LamenessState::getLamenessInfectiousStatus()
        {
            return _lamenessInfectiousType;
        }
    } /* End of namespace DataStructures::States */
}
}