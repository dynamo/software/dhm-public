#ifndef Health_LamenessStates_LamenessState_h
#define Health_LamenessStates_LamenessState_h

// boost

// project
#include "../../../../Tools/Serialization.h"
#include "../../../../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "../../../../Tools/Random.h"
#include "../../../../ExchangeInfoStructures/TreatmentTypeParameters.h"
namespace Results
{
//    class DairyFarmResult;
#ifdef _LOG
    class DayAnimalLogInformations;
#endif // _LOG            
}

namespace DataStructures
{
    class Foots;
}

namespace Health
{
namespace Lameness
{
    namespace LamenessStates
    {
//        class I_LamenessProneFoots;
        
        class LamenessState
        {
        DECLARE_SERIALIZE_STD_METHODS;

        protected:
            boost::gregorian::date _beginDate;
            boost::gregorian::date _predictedEndDate;
            
            float _mastitisRiskInvolved = 0.0f;

            DataStructures::Foots* _pAppliesToFoots;
            FunctionalEnumerations::Health::LamenessInfectiousStateType _lamenessInfectiousType;
            LamenessState(const boost::gregorian::date &beginDate, DataStructures::Foots* pFoots, FunctionalEnumerations::Health::LamenessInfectiousStateType lamenessInfectiousStatus);
            LamenessState(){};
            virtual void concrete() = 0; // To ensure that it will not be instantiated
            bool isOutOfTime(const boost::gregorian::date &baseDate);

        public:
            virtual ~LamenessState();
            virtual inline void takeTreatment(const boost::gregorian::date &date, Tools::Random &random){};
            boost::gregorian::date &getBeginDate();
            void setDurationInfinite();
            void setDuration(boost::gregorian::date_duration &duration);    
            virtual void progress(const boost::gregorian::date &simDate, Tools::Random &random, LamenessState* &pNewState
#ifdef _LOG
                                                              , Results::DayAnimalLogInformations &log
#endif // _LOG            
                                                              );
            virtual inline unsigned int getCurrentDurationLamenessIll(const boost::gregorian::date &date){return 0;}
            virtual float getLamenessOestrusDetectionFactor(FunctionalEnumerations::Reproduction::DetectionMode dm, bool first);
            virtual inline void isIll(bool &G1Ill, bool &G2Ill, bool &detected){G1Ill = false; G2Ill = false; detected = false;}
            virtual inline bool isIll(){return false;}
            virtual bool isG1Ill(){return false;}
            virtual bool isG2Ill(){return false;}
            inline float getMastitisRiskFactor(){return _mastitisRiskInvolved;}
            virtual inline void becomeSensitive(const boost::gregorian::date &date, LamenessState* &pNewState){};
            virtual FunctionalEnumerations::Health::LamenessSeverity getSeverity() = 0;
            virtual void getLamenessHighestMilkEffect(float &quantityLamenessDelta, float &TBLamenessDelta, float &TPLamenessDelta, FunctionalEnumerations::Population::Location currentLocation) {}
#ifdef _LOG
            virtual std::string getStrLamenessState() = 0;
#endif // _LOG            
            FunctionalEnumerations::Health::LamenessInfectiousStateType getLamenessInfectiousStatus();
        };
    }
} /* End of namespace Health */
}
#endif // Health_LamenessStates_LamenessState_h
