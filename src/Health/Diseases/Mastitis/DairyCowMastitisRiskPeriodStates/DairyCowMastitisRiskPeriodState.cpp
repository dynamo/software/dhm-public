#include "DairyCowMastitisRiskPeriodState.h"

// boost

// project
//#include "I_MastitisRiskPeriodDairyCow.h"
#include "../../../../DataStructures/DairyMammal.h"
#include "../../../HealthManagement.h"
#include "../../../../Tools/Tools.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(Health::Mastitis::RiskPeriodStates::DairyCowMastitisRiskPeriodState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Mastitis
{
    namespace RiskPeriodStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_NVP(_beginDate); \
        ar & BOOST_SERIALIZATION_NVP(_pAppliesToDairyMammal); \
        ar & BOOST_SERIALIZATION_NVP(_concernedMastitisSeverityDegree); \
        ar & BOOST_SERIALIZATION_NVP(_mastitisLactationRankModulation); \
 
        IMPLEMENT_SERIALIZE_STD_METHODS(DairyCowMastitisRiskPeriodState);
        
        DairyCowMastitisRiskPeriodState::DairyCowMastitisRiskPeriodState(DataStructures::DairyMammal* pDairyMammal, const boost::gregorian::date &beginDate)
        {
            _beginDate = beginDate;
            _pAppliesToDairyMammal = pDairyMammal;
        }
        
        unsigned int DairyCowMastitisRiskPeriodState::getReferenceDuration()
        {
            // Only asked for Mastitis risk period state concerned 
            std::string message = "DairyCowMastitisRiskPeriodState::getReferenceDuration : Not concerned state";
#ifdef _LOG
            message += " : " + getStrMastitisRiskPeriodType();
#endif // _LOG                  
            throw std::runtime_error(message);
        }
        
        void DairyCowMastitisRiskPeriodState::setMastitisSeverityDegree(DataStructures::DairyMammal* pDairyCow, unsigned int correction)
        {
            unsigned int lr = pDairyCow->getLactationRankForMastitisSeverityDegree() + correction;
#ifdef _LOG
            assert (lr != 0); // This question is asked for sensitive cows, so with at least 1 (corrected for comming calving) lactation.
#endif // _LOG
            
            // Determining the mastitis severity lactation rank and the mastitis lactation rank modulation
            FunctionalEnumerations::Health::MastitisSeverityLactationRank mslr;
            if (lr == 1)
            {
                // Primipare
                mslr = FunctionalEnumerations::Health::MastitisSeverityLactationRank::primipareLactationRank;
                _mastitisLactationRankModulation = FunctionalEnumerations::Health::MastitisLactationRankModulation::primipareRankModulation;
            }
            else if (lr == 2)
            {
                mslr = FunctionalEnumerations::Health::MastitisSeverityLactationRank::twoLactationsLactationRank;
                _mastitisLactationRankModulation = FunctionalEnumerations::Health::MastitisLactationRankModulation::twoLactationsRankModulation;
            }
            else
            {
                mslr = FunctionalEnumerations::Health::MastitisSeverityLactationRank::threeLactationsAndMoreLactationRank;
                if (lr == 3)
                {
                    _mastitisLactationRankModulation = FunctionalEnumerations::Health::MastitisLactationRankModulation::threeLactationsRankModulation;
                }
                else
                {
                    _mastitisLactationRankModulation = FunctionalEnumerations::Health::MastitisLactationRankModulation::fourLactationsAndMoreRankModulation;
                }
            }           
#ifdef _LOG
            _concernedMastitisLactationRank = mslr;
#endif // _LOG
            
            // Determining the mastitis severity degree
            _concernedMastitisSeverityDegree = (FunctionalConstants::Health::BASIS_MASTITIS_SEVERITY_DISTRIBUTION.find(mslr)->second).find(getMastitisRiskPeriodType())->second;
        }
                
        boost::gregorian::date &DairyCowMastitisRiskPeriodState::getBeginDate()
        {
            return _beginDate;
        }
            
        double DairyCowMastitisRiskPeriodState::getCurrentMastitisRiskPeriodProbability(const boost::gregorian::date &date, FunctionalEnumerations::Health::BacteriumType bacteriumType)
        {
            return HealthManagement::getMastitisRiskPeriodProbabilities(bacteriumType, getMastitisRiskPeriodType())[getCurrentMastitisRiskPeriodDay(date)];
        }
        
        unsigned int DairyCowMastitisRiskPeriodState::getCurrentMastitisRiskPeriodDay(const boost::gregorian::date &date)
        {
            boost::gregorian::date_duration dd = date - _beginDate;    
            
            // If we are outside the reference duration, we give the last day of the reference duration
            return (unsigned int)dd.days() >= getReferenceDuration() ? getReferenceDuration()-1 : (unsigned int)dd.days();           
        }
        
        FunctionalEnumerations::Health::MastitisSeverityDegree DairyCowMastitisRiskPeriodState::getMastitisSeverityDegree()
        {
            return _concernedMastitisSeverityDegree;
        }
        
        FunctionalEnumerations::Health::MastitisLactationRankModulation DairyCowMastitisRiskPeriodState::getMastitisLactationRankModulation()
        {
            return _mastitisLactationRankModulation;
        }
        
#ifdef _LOG
        unsigned int DairyCowMastitisRiskPeriodState::getDayOfTheMastitisRiskReferenceCycle(const boost::gregorian::date &date)
        {
            // Only asked for Mastitis risk period state concerned 
            std::string message = "DairyCowMastitisRiskPeriodState::getDayOfTheMastitisRiskReferenceCycle : Not concerned state : " + getStrMastitisRiskPeriodType();
            throw std::runtime_error(message);
        }
        
        std::string DairyCowMastitisRiskPeriodState::getStrMastitisRiskPeriodType()
        {
            std::string res = "Lact=" + Tools::toString(_concernedMastitisLactationRank) + " - Period=" + Tools::toString(getMastitisRiskPeriodType()) + " - Degree=" + Tools::toString(_concernedMastitisSeverityDegree);
            return res;
        }      
#endif // _LOG
    }     
}
}