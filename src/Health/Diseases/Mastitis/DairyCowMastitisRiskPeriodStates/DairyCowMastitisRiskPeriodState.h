#ifndef Health_DairyCowMastitisRiskPeriodStates_DairyCowMastitisRiskPeriodState_h
#define Health_DairyCowMastitisRiskPeriodStates_DairyCowMastitisRiskPeriodState_h

// boost

// project
#include "../../../../Tools/Serialization.h"
#include "../../../../ExchangeInfoStructures/FunctionalEnumerations.h"

namespace DataStructures
{
    class DairyMammal;
}

namespace Health
{
namespace Mastitis
{
    namespace RiskPeriodStates
    {
        //class I_MastitisRiskPeriodDairyCow;
        
        class DairyCowMastitisRiskPeriodState
        {

        DECLARE_SERIALIZE_STD_METHODS;

            unsigned int getDayFromTheOrigin(const boost::gregorian::date &date);

        protected:
            boost::gregorian::date _beginDate;
            DataStructures::DairyMammal* _pAppliesToDairyMammal;
            
            FunctionalEnumerations::Health::MastitisSeverityDegree _concernedMastitisSeverityDegree = FunctionalEnumerations::Health::MastitisSeverityDegree::basis;
            FunctionalEnumerations::Health::MastitisLactationRankModulation _mastitisLactationRankModulation = FunctionalEnumerations::Health::MastitisLactationRankModulation::heiferRankModulation;
#ifdef _LOG
            FunctionalEnumerations::Health::MastitisSeverityLactationRank _concernedMastitisLactationRank = FunctionalEnumerations::Health::MastitisSeverityLactationRank::heiferLactationRank;
#endif // _LOG

            DairyCowMastitisRiskPeriodState(DataStructures::DairyMammal* pDairyMammal, const boost::gregorian::date &beginDate);
            DairyCowMastitisRiskPeriodState(){};
            virtual void concrete() = 0; // To ensure that it will not be instantiated
            virtual unsigned int getReferenceDuration();
            void setMastitisSeverityDegree(DataStructures::DairyMammal* pDairyCow, unsigned int correction);

        public:
            virtual ~DairyCowMastitisRiskPeriodState(){};
            boost::gregorian::date &getBeginDate();
            virtual FunctionalEnumerations::Health::MastitisRiskPeriodType getMastitisRiskPeriodType() = 0;
            double getCurrentMastitisRiskPeriodProbability(const boost::gregorian::date &date, FunctionalEnumerations::Health::BacteriumType bacteriumType);
            unsigned int getCurrentMastitisRiskPeriodDay(const boost::gregorian::date &date);          
            FunctionalEnumerations::Health::MastitisSeverityDegree getMastitisSeverityDegree();
            FunctionalEnumerations::Health::MastitisLactationRankModulation getMastitisLactationRankModulation();
            virtual inline bool needToStudyMastitisOccurenceProbability(){return true;}
            virtual void calvingIsDone(){};
#ifdef _LOG
            virtual unsigned int getDayOfTheMastitisRiskReferenceCycle(const boost::gregorian::date &date);
            std::string getStrMastitisRiskPeriodType();
#endif // _LOG
        };
    }
}
}
#endif // Health_DairyCowMastitisRiskPeriodStates_DairyCowMastitisRiskPeriodState_h
