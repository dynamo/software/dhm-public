#ifndef Health_DairyCowMastitisRiskPeriodStates_DryingInvolutedRiskPeriodDairyCowState_h
#define Health_DairyCowMastitisRiskPeriodStates_DryingInvolutedRiskPeriodDairyCowState_h

// project
#include "DairyCowMastitisRiskPeriodState.h"

namespace Health
{
namespace Mastitis
{
    namespace RiskPeriodStates
    {
        class DryingInvolutedRiskPeriodDairyCowState : public DairyCowMastitisRiskPeriodState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        protected:
            void concrete(){}; // To allow instanciation
        public:
            DryingInvolutedRiskPeriodDairyCowState(){};
            DryingInvolutedRiskPeriodDairyCowState(DataStructures::DairyMammal* pDairyCow, const boost::gregorian::date &beginDate);
            virtual ~DryingInvolutedRiskPeriodDairyCowState(){};
            inline FunctionalEnumerations::Health::MastitisRiskPeriodType getMastitisRiskPeriodType(){return FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingInvoluted;}
            unsigned int getReferenceDuration() override;
#ifdef _LOG
            unsigned int getDayOfTheMastitisRiskReferenceCycle(const boost::gregorian::date &date) override;
#endif // _LOG
        };
    }
}
}
#endif // Health_DairyCowMastitisRiskPeriodStates_DryingInvolutedRiskPeriodDairyCowState_h
