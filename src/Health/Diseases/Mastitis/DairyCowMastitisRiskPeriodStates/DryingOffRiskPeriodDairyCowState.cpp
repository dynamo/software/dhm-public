#include "DryingOffRiskPeriodDairyCowState.h"

// project
//#include "I_MastitisRiskPeriodDairyCow.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(Health::Mastitis::RiskPeriodStates::DryingOffRiskPeriodDairyCowState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Mastitis
{
    namespace RiskPeriodStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyCowMastitisRiskPeriodState); \

        IMPLEMENT_SERIALIZE_STD_METHODS(DryingOffRiskPeriodDairyCowState);

        DryingOffRiskPeriodDairyCowState::DryingOffRiskPeriodDairyCowState(DataStructures::DairyMammal* pDairyCow, const boost::gregorian::date &beginDate) : DairyCowMastitisRiskPeriodState(pDairyCow, beginDate)
        {
            // Set the corresponding lactation rank
            setMastitisSeverityDegree(pDairyCow, 0);
        }

        unsigned int DryingOffRiskPeriodDairyCowState::getReferenceDuration()
        {
            return FunctionalConstants::Health::MAX_DRY_DURATION_FOR_POST_DRY_MASTITIS_RISK;
        }

#ifdef _LOG
        unsigned int DryingOffRiskPeriodDairyCowState::getDayOfTheMastitisRiskReferenceCycle(const boost::gregorian::date &date)
        {
            return FunctionalConstants::Health::REFERENCE_PERIPARTUM_MASTITIS_RISK_PERIOD_DURATION + FunctionalConstants::Health::REFERENCE_LACTATION_MASTITIS_RISK_PERIOD_DURATION + getCurrentMastitisRiskPeriodDay(date);
        }
#endif // _LOG
    }
}
}
