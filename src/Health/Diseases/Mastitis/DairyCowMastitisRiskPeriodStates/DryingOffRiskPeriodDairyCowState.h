#ifndef Health_DairyCowMastitisRiskPeriodStates_DryingOffRiskPeriodDairyCowState_h
#define Health_DairyCowMastitisRiskPeriodStates_DryingOffRiskPeriodDairyCowState_h

// project
#include "DairyCowMastitisRiskPeriodState.h"

namespace Health
{
namespace Mastitis
{
    namespace RiskPeriodStates
    {
        class DryingOffRiskPeriodDairyCowState : public DairyCowMastitisRiskPeriodState
        {
            DECLARE_SERIALIZE_STD_METHODS;
            
        protected:
            void concrete(){}; // To allow instanciation
            
        public:
            DryingOffRiskPeriodDairyCowState(){};
            DryingOffRiskPeriodDairyCowState(DataStructures::DairyMammal* pDairyCow, const boost::gregorian::date &beginDate);
            virtual ~DryingOffRiskPeriodDairyCowState(){};
            inline FunctionalEnumerations::Health::MastitisRiskPeriodType getMastitisRiskPeriodType(){return FunctionalEnumerations::Health::MastitisRiskPeriodType::dryingOff;}
            unsigned int getReferenceDuration() override;
#ifdef _LOG
            unsigned int getDayOfTheMastitisRiskReferenceCycle(const boost::gregorian::date &date) override;
#endif // _LOG
        };
    }
}
}
#endif // Health_DairyCowMastitisRiskPeriodStates_DryingOffRiskPeriodDairyCowState_h
