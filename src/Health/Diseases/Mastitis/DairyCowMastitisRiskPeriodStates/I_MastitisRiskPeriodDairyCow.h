//#ifndef Health_DairyCowMastitisRiskPeriodStates_I_MastitisRiskPeriodDairyCow_h
//#define Health_DairyCowMastitisRiskPeriodStates_I_MastitisRiskPeriodDairyCow_h
//
//// boost
//
//// project
//
//namespace Health
//{
//namespace Mastitis
//{
//    namespace RiskPeriodStates
//    {
//        class DairyCowMastitisRiskPeriodState;
//   
//        class I_MastitisRiskPeriodDairyCow
//        {
//        public:
//            
//            virtual ~I_MastitisRiskPeriodDairyCow(){}
//            virtual void ensureUdderIsNowMastitisSensitive(const boost::gregorian::date &date) = 0;
//            virtual unsigned int getLactationRankForMastitisSeverityDegree() = 0;
//        };
//    }
//}
//}
//#endif // Health_DairyCowMastitisRiskPeriodStates_I_MastitisRiskPeriodDairyCow_h
