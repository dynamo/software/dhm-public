#include "LactationRiskPeriodDairyCowState.h"

// project
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(Health::Mastitis::RiskPeriodStates::LactationRiskPeriodDairyCowState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Mastitis
{
    namespace RiskPeriodStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyCowMastitisRiskPeriodState);

        IMPLEMENT_SERIALIZE_STD_METHODS(LactationRiskPeriodDairyCowState);

        LactationRiskPeriodDairyCowState::LactationRiskPeriodDairyCowState(DataStructures::DairyMammal* pDairyCow, const boost::gregorian::date &beginDate) : DairyCowMastitisRiskPeriodState(pDairyCow, beginDate)
        {
            // Set the corresponding lactation rank
            setMastitisSeverityDegree(pDairyCow, 0);
        }

        unsigned int LactationRiskPeriodDairyCowState::getReferenceDuration()
        {
            return FunctionalConstants::Health::REFERENCE_LACTATION_MASTITIS_RISK_PERIOD_DURATION;
        }

#ifdef _LOG
        unsigned int LactationRiskPeriodDairyCowState::getDayOfTheMastitisRiskReferenceCycle(const boost::gregorian::date &date)
        {
            return FunctionalConstants::Health::REFERENCE_PERIPARTUM_MASTITIS_RISK_PERIOD_DURATION + getCurrentMastitisRiskPeriodDay(date);
        }
#endif // _LOG
    }
}
}