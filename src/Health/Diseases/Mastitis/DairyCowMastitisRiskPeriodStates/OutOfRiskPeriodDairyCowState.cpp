#include "OutOfRiskPeriodDairyCowState.h"

// project

BOOST_CLASS_EXPORT(Health::Mastitis::RiskPeriodStates::OutOfRiskPeriodDairyCowState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Mastitis
{
    namespace RiskPeriodStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyCowMastitisRiskPeriodState);

        IMPLEMENT_SERIALIZE_STD_METHODS(OutOfRiskPeriodDairyCowState);

        OutOfRiskPeriodDairyCowState::OutOfRiskPeriodDairyCowState(DataStructures::DairyMammal* pDairyCow, const boost::gregorian::date &beginDate) : DairyCowMastitisRiskPeriodState(pDairyCow, beginDate)
        {

        }
    }
}
}