#ifndef Health_DairyCowMastitisRiskPeriodStates_OutOfRiskPeriodDairyCowState_h
#define Health_DairyCowMastitisRiskPeriodStates_OutOfRiskPeriodDairyCowState_h

// project
#include "DairyCowMastitisRiskPeriodState.h"

namespace Health
{
namespace Mastitis
{
    namespace RiskPeriodStates
    {
        class OutOfRiskPeriodDairyCowState : public DairyCowMastitisRiskPeriodState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        protected:
            void concrete(){}; // To allow instanciation
        public:
            OutOfRiskPeriodDairyCowState(){};
            OutOfRiskPeriodDairyCowState(DataStructures::DairyMammal* pDairyCow, const boost::gregorian::date &beginDate);
            virtual ~OutOfRiskPeriodDairyCowState(){};
            inline FunctionalEnumerations::Health::MastitisRiskPeriodType getMastitisRiskPeriodType(){return FunctionalEnumerations::Health::MastitisRiskPeriodType::outOfRiskPeriod;}
            inline bool needToStudyMastitisOccurenceProbability() override {return false;}
        };
    }
}
}
#endif // Health_DairyCowMastitisRiskPeriodStates_OutOfRiskPeriodDairyCowState_h
