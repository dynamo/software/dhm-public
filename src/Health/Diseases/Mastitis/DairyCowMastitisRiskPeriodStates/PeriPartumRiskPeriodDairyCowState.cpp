#include "PeriPartumRiskPeriodDairyCowState.h"

// project
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"
//#include "I_MastitisRiskPeriodDairyCow.h"
#include "../../../../DataStructures/DairyMammal.h"

BOOST_CLASS_EXPORT(Health::Mastitis::RiskPeriodStates::PeriPartumRiskPeriodDairyCowState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Mastitis
{
    namespace RiskPeriodStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyCowMastitisRiskPeriodState); \
        ar & BOOST_SERIALIZATION_NVP(_calving); \

        IMPLEMENT_SERIALIZE_STD_METHODS(PeriPartumRiskPeriodDairyCowState);

        PeriPartumRiskPeriodDairyCowState::PeriPartumRiskPeriodDairyCowState(DataStructures::DairyMammal* pDairyCow, const boost::gregorian::date &beginDate, bool calving) : DairyCowMastitisRiskPeriodState(pDairyCow, beginDate)
        {
            // Already claving ? (may be abort...)
            _calving = calving;

            // If it is the first time, the cow (udder) is now mastitis sensitive
            pDairyCow->ensureUdderIsNowMastitisSensitive(beginDate);

            // The lactation rank will increase in a few days (comming calving day), so the correction is 1.
            setMastitisSeverityDegree(pDairyCow, 1);
        }

        void PeriPartumRiskPeriodDairyCowState::calvingIsDone()
        {
            _calving = true;
        }

        unsigned int PeriPartumRiskPeriodDairyCowState::getReferenceDuration()
        {
            return FunctionalConstants::Health::REFERENCE_PERIPARTUM_MASTITIS_RISK_PERIOD_DURATION;
        }

#ifdef _LOG
        unsigned int PeriPartumRiskPeriodDairyCowState::getDayOfTheMastitisRiskReferenceCycle(const boost::gregorian::date &date)
        {
            return  getCurrentMastitisRiskPeriodDay(date);
        }
#endif // _LOG
    }
}
}