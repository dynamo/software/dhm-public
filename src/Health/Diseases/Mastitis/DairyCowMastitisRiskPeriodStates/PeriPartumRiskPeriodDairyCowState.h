#ifndef Health_DairyCowMastitisRiskPeriodStates_PeriPartumRiskPeriodDairyCowState_h
#define Health_DairyCowMastitisRiskPeriodStates_PeriPartumRiskPeriodDairyCowState_h

// project
#include "DairyCowMastitisRiskPeriodState.h"

namespace Health
{
namespace Mastitis
{
    namespace RiskPeriodStates
    {
        class PeriPartumRiskPeriodDairyCowState : public DairyCowMastitisRiskPeriodState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
            bool _calving = false;
        protected:
            void concrete(){}; // To allow instanciation
        public:
            PeriPartumRiskPeriodDairyCowState(){};
            PeriPartumRiskPeriodDairyCowState(DataStructures::DairyMammal* pDairyCow, const boost::gregorian::date &beginDate, bool calving);
            void calvingIsDone() override;
            virtual ~PeriPartumRiskPeriodDairyCowState(){};
            inline FunctionalEnumerations::Health::MastitisRiskPeriodType getMastitisRiskPeriodType(){return FunctionalEnumerations::Health::MastitisRiskPeriodType::periPartumAndLactationBegin;}
            unsigned int getReferenceDuration() override;
#ifdef _LOG
            unsigned int getDayOfTheMastitisRiskReferenceCycle(const boost::gregorian::date &date) override;
#endif // _LOG
        };
    }
}
}
#endif // Health_DairyCowMastitisRiskPeriodStates_PeriPartumRiskPeriodDairyCowState_h
