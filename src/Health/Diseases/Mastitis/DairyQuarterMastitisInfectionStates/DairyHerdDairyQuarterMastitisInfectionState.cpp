#include "DairyHerdDairyQuarterMastitisInfectionState.h"

// boost

// project
//#include "I_DairyHerdMastitisProneDairyQuarter.h"
#include "../../../../DataStructures/DairyQuarter.h"

BOOST_CLASS_EXPORT(Health::Mastitis::DairyQuarterInfectionStates::DairyHerdDairyQuarterMastitisInfectionState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_NVP(_beginDate); \
        ar & BOOST_SERIALIZATION_NVP(_predictedEndDate); \
        ar & BOOST_SERIALIZATION_NVP(_concernedBacteriumType); \
        ar & BOOST_SERIALIZATION_NVP(_pAppliesToDairyQuarter); \
 
        IMPLEMENT_SERIALIZE_STD_METHODS(DairyHerdDairyQuarterMastitisInfectionState);
       
        //DairyHerdDairyQuarterMastitisInfectionState::DairyHerdDairyQuarterMastitisInfectionState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType concernedBacterium)
        DairyHerdDairyQuarterMastitisInfectionState::DairyHerdDairyQuarterMastitisInfectionState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType concernedBacterium)
        {
            _beginDate = beginDate;
            
            // General concept
            boost::gregorian::date_duration duration = boost::gregorian::date_duration(boost::gregorian::pos_infin);
            
            setDuration(duration);          
            _concernedBacteriumType = concernedBacterium;         
            _pAppliesToDairyQuarter = pDairyQuarter;
        }
        
        bool DairyHerdDairyQuarterMastitisInfectionState::isOutOfTime(const boost::gregorian::date &baseDate)
        {
            return baseDate >= _predictedEndDate;
        }
        
        DairyHerdDairyQuarterMastitisInfectionState::~DairyHerdDairyQuarterMastitisInfectionState()
        {
        }
        
        boost::gregorian::date &DairyHerdDairyQuarterMastitisInfectionState::getBeginDate()
        {
            return _beginDate;
        }
            
        void DairyHerdDairyQuarterMastitisInfectionState::setDuration(boost::gregorian::date_duration &duration)
        {
            _predictedEndDate = _beginDate + duration;
        }
        
        bool DairyHerdDairyQuarterMastitisInfectionState::isUnsensitive()
        {
            return false;
        }
        
        bool DairyHerdDairyQuarterMastitisInfectionState::isSensitive()
        {
            return false;
        }

        FunctionalEnumerations::Health::BacteriumType DairyHerdDairyQuarterMastitisInfectionState::getConcernedBacteriumType()
        {
            return _concernedBacteriumType;
        }
       
        FunctionalEnumerations::Health::MastitisSeverity DairyHerdDairyQuarterMastitisInfectionState::getMastitisSeverity()
        {
            throw std::runtime_error("DairyHerdDairyQuarterMastitisInfectionState::getMastitisSeverity : Not concerned state");  
        }           
    } /* End of namespace DataStructures::States */
}
}