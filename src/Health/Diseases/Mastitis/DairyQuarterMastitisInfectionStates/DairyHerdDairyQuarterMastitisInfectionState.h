#ifndef Health_DairyQuarterMastitisInfectionStates_DairyHerdDairyQuarterMastitisInfectionState_h
#define Health_DairyQuarterMastitisInfectionStates_DairyHerdDairyQuarterMastitisInfectionState_h

// project
#include "../../../../Tools/Serialization.h"
#include "../../../../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "../../../../Tools/Random.h"

namespace DataStructures
{
    class DairyQuarter;
}
namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        //class I_DairyHerdMastitisProneDairyQuarter;
        
        class DairyHerdDairyQuarterMastitisInfectionState
        {
        DECLARE_SERIALIZE_STD_METHODS;

        protected:
            boost::gregorian::date _beginDate;
            boost::gregorian::date _predictedEndDate;

            FunctionalEnumerations::Health::BacteriumType _concernedBacteriumType;
            //I_DairyHerdMastitisProneDairyQuarter* _pAppliesToDairyQuarter;
            DataStructures::DairyQuarter* _pAppliesToDairyQuarter;

            //DairyHerdDairyQuarterMastitisInfectionState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType concernedBacterium);
            DairyHerdDairyQuarterMastitisInfectionState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType concernedBacterium);
            DairyHerdDairyQuarterMastitisInfectionState(){};
            virtual void concrete() = 0; // To ensure that it will not be instantiated
            bool isOutOfTime(const boost::gregorian::date &baseDate);

        public:
            virtual ~DairyHerdDairyQuarterMastitisInfectionState();
            boost::gregorian::date &getBeginDate();
            void setDuration(boost::gregorian::date_duration &duration);
            FunctionalEnumerations::Health::BacteriumType getConcernedBacteriumType();
            virtual void progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState) = 0;
            virtual bool isUnsensitive();
            virtual bool isSensitive();
            virtual inline bool isInfected() {return false;}
            virtual inline bool isClinicalInfected() {return false;}
            virtual inline bool isG1ToG3MastitisInfected() {return false;}
            virtual float getHightEffectMastitisMilkProductReduction(const boost::gregorian::date &theDate){return 0.0f;}
            virtual unsigned int getMastitisAdditionalSCC(Tools::Random &random){return 0;}
            virtual void setTreatmentInformations(const boost::gregorian::date &simDate, unsigned int effectDelay, float healingProbability, float protectProbability, unsigned int protectDelay, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState) = 0;
            virtual FunctionalEnumerations::Health::MastitisSeverity getMastitisSeverity();
#ifdef _LOG
            virtual std::string getStrCurrentMastitisDiagnostic() = 0;      
#endif // _LOG            
        };
    }
    }
} /* End of namespace Production */
#endif // Health_DairyQuarterMastitisInfectionStates_DairyHerdDairyQuarterMastitisInfectionState_h
