#include "DetectedG1SeverityMastitisInfectedDairyQuarterState.h"

// project
#include "MastitisSensitiveDairyQuarterState.h"
#include "MastitisUnsensitiveDairyQuarterState.h"
#include "../../../HealthManagement.h"

BOOST_CLASS_EXPORT(Health::Mastitis::DairyQuarterInfectionStates::DetectedG1SeverityMastitisInfectedDairyQuarterState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(MastitisClinicalInfectedDairyQuarterState);

        IMPLEMENT_SERIALIZE_STD_METHODS(DetectedG1SeverityMastitisInfectedDairyQuarterState);

        //DetectedG1SeverityMastitisInfectedDairyQuarterState::DetectedG1SeverityMastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, bool newCase, bool newClinical, Tools::Random &random) : MastitisClinicalInfectedDairyQuarterState(beginDate, pDairyQuarter, bacterium, FunctionalEnumerations::Health::MastitisSeverity::G1, newCase, newClinical, random)
        DetectedG1SeverityMastitisInfectedDairyQuarterState::DetectedG1SeverityMastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, bool newCase, bool newClinical, Tools::Random &random) : MastitisClinicalInfectedDairyQuarterState(beginDate, pDairyQuarter, bacterium, FunctionalEnumerations::Health::MastitisSeverity::G1, newCase, newClinical, random)
        {                
        }

        FunctionalEnumerations::Health::MastitisSeverity DetectedG1SeverityMastitisInfectedDairyQuarterState::getMastitisSeverity()
        {
            return FunctionalEnumerations::Health::MastitisSeverity::G1;         
        }

        void DetectedG1SeverityMastitisInfectedDairyQuarterState::progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState)
        {
            MastitisClinicalInfectedDairyQuarterState::progress(simDate, healthToday, random, pNewState);
            if (isOutOfTime(simDate))
            {
                // Spontaneous (or other bacterium or treatment) healing :
                pNewState = getHealUpState(simDate, healthToday, random);
            }
        }
        
        DetectedG1SeverityMastitisInfectedDairyQuarterState::~DetectedG1SeverityMastitisInfectedDairyQuarterState()
        {
        }

#ifdef _LOG
        std::string DetectedG1SeverityMastitisInfectedDairyQuarterState::getStrGravity()
        {
            return "G1d" ;
        }
#endif // _LOG            
    }
}
}