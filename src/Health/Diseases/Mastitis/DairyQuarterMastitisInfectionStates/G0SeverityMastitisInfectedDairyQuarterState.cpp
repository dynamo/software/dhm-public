#include "G0SeverityMastitisInfectedDairyQuarterState.h"

// project
//#include "I_DairyHerdMastitisProneDairyQuarter.h"
#include "../../../../DataStructures/DairyQuarter.h"
#include "MastitisSensitiveDairyQuarterState.h"
#include "UndetectedG1SeverityMastitisInfectedDairyQuarterState.h"
#include "DetectedG1SeverityMastitisInfectedDairyQuarterState.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(Health::Mastitis::DairyQuarterInfectionStates::G0SeverityMastitisInfectedDairyQuarterState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(MastitisSubclinicalInfectedDairyQuarterState); \

        IMPLEMENT_SERIALIZE_STD_METHODS(G0SeverityMastitisInfectedDairyQuarterState);

        FunctionalEnumerations::Health::MastitisSeverity G0SeverityMastitisInfectedDairyQuarterState::getMastitisSeverity()
        {
            return FunctionalEnumerations::Health::MastitisSeverity::G0;
        }

//        G0SeverityMastitisInfectedDairyQuarterState::G0SeverityMastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, bool newCase, Tools::Random &random)
        G0SeverityMastitisInfectedDairyQuarterState::G0SeverityMastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, bool newCase, Tools::Random &random)
                                            : MastitisSubclinicalInfectedDairyQuarterState(beginDate, pDairyQuarter, bacterium, FunctionalEnumerations::Health::MastitisSeverity::G0, newCase, random)
        {                
            // Persistence
            boost::gregorian::date_duration duration = boost::gregorian::date_duration(FunctionalConstants::Health::G0_MASTITIS_DURATION_BEFORE_SPONTANEOUS_HEALING);
            setDuration(duration);
        }

        void G0SeverityMastitisInfectedDairyQuarterState::progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState)
        {
            MastitisInfectedDairyQuarterState::progress(simDate, healthToday, random, pNewState);
            if (isOutOfTime(simDate))
            {
                // We are here in 3 cases
                // - 1: other healing or treatment
                // - 2: if not, possibe hearly spontaneous healing
                // - 3: if not, flareup or new and last spontaneous healing
                
              
                if (_healedByOtherHealingOrTreatment)
                {
                    // Case 1
                    pNewState = getHealUpState(simDate, healthToday, random);
                    
                }
                else if (_possibleEarlySpontaneousHealing)
                {
                    // Case 2
                    if (random.ran_bernoulli(FunctionalConstants::Health::G0_FLARE_UP_PROBABILITY.find(_concernedBacteriumType)->second))
                    {
                        pNewState = getHealUpState(simDate, healthToday, random);
                    }
                    else
                    {
                        // Persistence
                        boost::gregorian::date_duration duration = boost::gregorian::date_duration(FunctionalConstants::Health::G0_MASTITIS_PERSISTANCE); 
                        setDuration(duration);
                        _possibleEarlySpontaneousHealing = false;
                    }   
                }
                else
                {
                    // Case 3
                    if (random.ran_bernoulli(FunctionalConstants::Health::G0_FLARE_UP_PROBABILITY.find(_concernedBacteriumType)->second))
                    {
                        // Flare up, G0 become G1
                        if (_pAppliesToDairyQuarter->G1SeverityMastitisIsDetected())
                        {
                            pNewState = new DetectedG1SeverityMastitisInfectedDairyQuarterState(simDate, _pAppliesToDairyQuarter, _concernedBacteriumType, false, true, random);
                        }
                        else
                        {
                            pNewState = new UndetectedG1SeverityMastitisInfectedDairyQuarterState(simDate, _pAppliesToDairyQuarter, _concernedBacteriumType, false, random);
                        }
                    }
                    else
                    {
                        // Spontaneous healing :
                        pNewState = getHealUpState(simDate, healthToday, random);
                    }
                }
               
                
                
//                if (not _healedByOtherHealingOrTreatment and random.ran_bernoulli(FunctionalConstants::Health::G0_FLARE_UP_PROBABILITY.find(_concernedBacteriumType)->second))
//                {
//                    // Flare up, G0 become G1
//                    if (_pAppliesToDairyQuarter->G1SeverityMastitisIsDetected())
//                    {
//                        pNewState = new DetectedG1SeverityMastitisInfectedDairyQuarterState(simDate, _pAppliesToDairyQuarter, _concernedBacteriumType, false, true, random);
//                    }
//                    else
//                    {
//                        pNewState = new UndetectedG1SeverityMastitisInfectedDairyQuarterState(simDate, _pAppliesToDairyQuarter, _concernedBacteriumType, false, random);
//                    }
//                }
//                else
//                {
//                    if (_healedByOtherHealingOrTreatment or random.ran_bernoulli((FunctionalConstants::Health::MASTITIS_SPONTANEOUS_HEALING_PROBABILITY.find(_concernedBacteriumType)->second).find(_pAppliesToDairyQuarter->getCurrentMastitisRiskPeriodType())->second))
//                    {
//                        // Spontaneous (or other bacterium or treatment) healing :
//                        pNewState = getHealUpState(simDate, healthToday, random);
//                    }
//                    else
//                    {
//                        // Persistence
//                        boost::gregorian::date_duration duration = boost::gregorian::date_duration(FunctionalConstants::Health::G0_MASTITIS_PERSISTANCE); 
//                        setDuration(duration);
//                    }
//                }
            }
        }
        
        G0SeverityMastitisInfectedDairyQuarterState::~G0SeverityMastitisInfectedDairyQuarterState()
        {
        }

#ifdef _LOG
        std::string G0SeverityMastitisInfectedDairyQuarterState::getStrGravity()
        {
            return "G0 " ;
        }
#endif // _LOG            
    }
}
}