#ifndef Health_DairyQuarterMastitisInfectionStates_G0SeverityMastitisInfectedDairyQuarterState_h
#define Health_DairyQuarterMastitisInfectionStates_G0SeverityMastitisInfectedDairyQuarterState_h

// project
#include "MastitisSubclinicalInfectedDairyQuarterState.h"

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        class G0SeverityMastitisInfectedDairyQuarterState : public MastitisSubclinicalInfectedDairyQuarterState 
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        bool _possibleEarlySpontaneousHealing = true;
        
        protected:
            G0SeverityMastitisInfectedDairyQuarterState(){}; // For serialization
            void concrete(){}; // To allow instanciation

        public:
            //G0SeverityMastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, bool newCase, Tools::Random &random);
            G0SeverityMastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, bool newCase, Tools::Random &random);
            virtual ~G0SeverityMastitisInfectedDairyQuarterState();
            void progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState) override;
            FunctionalEnumerations::Health::MastitisSeverity getMastitisSeverity() override;
#ifdef _LOG
            std::string getStrGravity();
#endif // _LOG            
        };
    }
}
}
#endif // Health_DairyQuarterMastitisInfectionStates_G0SeverityMastitisInfectedDairyQuarterState_h
