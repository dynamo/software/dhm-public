#include "G2SeverityMastitisInfectedDairyQuarterState.h"

// project
#include "MastitisSensitiveDairyQuarterState.h"
#include "../../../HealthManagement.h"

BOOST_CLASS_EXPORT(Health::Mastitis::DairyQuarterInfectionStates::G2SeverityMastitisInfectedDairyQuarterState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(MastitisClinicalInfectedDairyQuarterState);

        IMPLEMENT_SERIALIZE_STD_METHODS(G2SeverityMastitisInfectedDairyQuarterState);

//        G2SeverityMastitisInfectedDairyQuarterState::G2SeverityMastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, bool newCase, bool newClinical, Tools::Random &random) : MastitisClinicalInfectedDairyQuarterState(beginDate, pDairyQuarter, bacterium, FunctionalEnumerations::Health::MastitisSeverity::G2, newCase, newClinical, random)
        G2SeverityMastitisInfectedDairyQuarterState::G2SeverityMastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, bool newCase, bool newClinical, Tools::Random &random) : MastitisClinicalInfectedDairyQuarterState(beginDate, pDairyQuarter, bacterium, FunctionalEnumerations::Health::MastitisSeverity::G2, newCase, newClinical, random)
        {                
        }

        FunctionalEnumerations::Health::MastitisSeverity G2SeverityMastitisInfectedDairyQuarterState::getMastitisSeverity()
        {
            return FunctionalEnumerations::Health::MastitisSeverity::G2;         
        }

        void G2SeverityMastitisInfectedDairyQuarterState::progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState)
        {
            MastitisClinicalInfectedDairyQuarterState::progress(simDate, healthToday, random, pNewState);
            if (isOutOfTime(simDate))
            {
                // Treatment (or other bacterium) healing :
               pNewState = getHealUpState(simDate, healthToday, random);
            }
        }
        
        G2SeverityMastitisInfectedDairyQuarterState::~G2SeverityMastitisInfectedDairyQuarterState()
        {
        }

#ifdef _LOG
        std::string G2SeverityMastitisInfectedDairyQuarterState::getStrGravity()
        {
            return "G2 " ;
        }
#endif // _LOG            
    }
}
}