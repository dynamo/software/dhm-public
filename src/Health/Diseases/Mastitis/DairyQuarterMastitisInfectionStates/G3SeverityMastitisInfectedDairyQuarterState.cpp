#include "G3SeverityMastitisInfectedDairyQuarterState.h"

// project
//#include "I_DairyHerdMastitisProneDairyQuarter.h"
#include "MastitisSensitiveDairyQuarterState.h"
#include "../../../HealthManagement.h"
#include "../../../../ResultStructures/DairyFarmResult.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(Health::Mastitis::DairyQuarterInfectionStates::G3SeverityMastitisInfectedDairyQuarterState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(MastitisClinicalInfectedDairyQuarterState); \

        IMPLEMENT_SERIALIZE_STD_METHODS(G3SeverityMastitisInfectedDairyQuarterState);


        //G3SeverityMastitisInfectedDairyQuarterState::G3SeverityMastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, bool newCase, bool newClinical, Tools::Random &random) : MastitisClinicalInfectedDairyQuarterState(beginDate, pDairyQuarter, bacterium, FunctionalEnumerations::Health::MastitisSeverity::G3, newCase, newClinical, random)
        G3SeverityMastitisInfectedDairyQuarterState::G3SeverityMastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, bool newCase, bool newClinical, Tools::Random &random) : MastitisClinicalInfectedDairyQuarterState(beginDate, pDairyQuarter, bacterium, FunctionalEnumerations::Health::MastitisSeverity::G3, newCase, newClinical, random)
        {
        }

        void G3SeverityMastitisInfectedDairyQuarterState::progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState)
        {
            MastitisClinicalInfectedDairyQuarterState::progress(simDate, healthToday, random, pNewState);
            if (isOutOfTime(simDate))
            {
                if (random.ran_bernoulli(FunctionalConstants::Health::G3_MASTITIS_MORTALITY_PROBABILITY.find(_concernedBacteriumType)->second))
                {
                    // Death
                    _pAppliesToDairyQuarter->mammalDiesNowBecauseOfMastitis(
#ifdef _LOG
                                                                    simDate
#endif // _LOG            
                                                                    );
                }
                else if (random.ran_bernoulli(FunctionalConstants::Health::G3_MASTITIS_CULLING_PROBABILITY.find(_concernedBacteriumType)->second))
                {
                    // Culling
                    _pAppliesToDairyQuarter->cowIsCulledForSevereMastitis(simDate);
                    // Culling is once a week, so we stay in this state until that
                    _predictedEndDate = boost::gregorian::date(boost::gregorian::pos_infin);
                }
                else if (random.ran_bernoulli(FunctionalConstants::Health::G3_MASTITIS_QUARTER_LOSS_PROBABILITY.find(_concernedBacteriumType)->second))
                {
                    // Dairy quarter loss
                    _pAppliesToDairyQuarter->isLostNow();
                }
                else
                {
                    // Treatment (or other bacterium) healing :
                    pNewState = getHealUpState(simDate, healthToday, random);
                }
            }
        }

        FunctionalEnumerations::Health::MastitisSeverity G3SeverityMastitisInfectedDairyQuarterState::getMastitisSeverity()
        {
            return FunctionalEnumerations::Health::MastitisSeverity::G3;                
        }
        
        G3SeverityMastitisInfectedDairyQuarterState::~G3SeverityMastitisInfectedDairyQuarterState()
        {
        }

#ifdef _LOG
        std::string G3SeverityMastitisInfectedDairyQuarterState::getStrGravity()
        {
            return "G3 " ;
        }
#endif // _LOG            
    }
}
}