#ifndef Health_DairyQuarterMastitisInfectionStates_G3SeverityMastitisInfectedDairyQuarterState_h
#define Health_DairyQuarterMastitisInfectionStates_G3SeverityMastitisInfectedDairyQuarterState_h

// project
#include "MastitisClinicalInfectedDairyQuarterState.h"

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        class G3SeverityMastitisInfectedDairyQuarterState : public MastitisClinicalInfectedDairyQuarterState
        {
        DECLARE_SERIALIZE_STD_METHODS;

        protected:
            G3SeverityMastitisInfectedDairyQuarterState(){}; // For serialization
            void concrete(){}; // To allow instanciation

        public:
            
            //G3SeverityMastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, bool newCase, bool newClinical, Tools::Random &random);
            G3SeverityMastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, bool newCase, bool newClinical, Tools::Random &random);
            virtual ~G3SeverityMastitisInfectedDairyQuarterState();
            void progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState) override;
            FunctionalEnumerations::Health::MastitisSeverity getMastitisSeverity() override;
            inline bool isG1ToG3MastitisInfected() override {return true;}
#ifdef _LOG
            std::string getStrGravity();
#endif // _LOG            
        };
    }
}
}
#endif // Health_DairyQuarterMastitisInfectionStates_G3SeverityMastitisInfectedDairyQuarterState_h