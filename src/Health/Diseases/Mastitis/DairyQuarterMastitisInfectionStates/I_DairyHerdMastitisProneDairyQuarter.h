//#ifndef Health_DairyQuarterMastitisInfectionStates_I_DairyHerdMastitisProneDairyQuarter_h
//#define Health_DairyQuarterMastitisInfectionStates_I_DairyHerdMastitisProneDairyQuarter_h
//
//// boost
//
//// project
//#include "../../../../ExchangeInfoStructures/FunctionalEnumerations.h"
//
//namespace Tools
//{
//    class Random;
//}
//namespace Health
//{
//namespace Mastitis
//{
//    namespace DairyQuarterInfectionStates
//    {
//        class DairyHerdDairyQuarterMastitisInfectionState;
//   
//        class I_DairyHerdMastitisProneDairyQuarter
//        {
//        public:
//            
//            // virtual destructor for interface 
//            virtual ~I_DairyHerdMastitisProneDairyQuarter() { }
//
//            virtual double getMastitisOccurenceProbability(FunctionalEnumerations::Health::BacteriumType bacterium) = 0;
//            virtual FunctionalEnumerations::Health::MastitisSeverity getRandomMastitisSeverity(FunctionalEnumerations::Health::BacteriumType bacterium) = 0;
//            virtual void appendMastitis(FunctionalEnumerations::Health::BacteriumType bacterium, const boost::gregorian::date &beginDate, bool newCase, bool newSubclinical, bool newClinical) = 0;
//            virtual void mastitisSensitiveIsNowUnsensitive(const boost::gregorian::date &date, boost::gregorian::date_duration &duration) = 0;
//            virtual void isLostNow()=0;
//            virtual bool G1SeverityMastitisIsDetected() = 0;
//            virtual void programMastitisRelapse(boost::gregorian::date beginRelapseDate, FunctionalEnumerations::Health::MastitisSeverity severity, FunctionalEnumerations::Health::BacteriumType bacterium) = 0;
//            virtual bool hasProgramedMastitis(boost::gregorian::date date, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity &severity) = 0;
//            virtual bool hasBeenInfectedDuringTheStep()=0;
//            virtual void mammalDiesNowBecauseOfMastitis(
//#ifdef _LOG
//                                                        const boost::gregorian::date &date
//#endif // _LOG            
//                                                        )=0;
//            virtual FunctionalEnumerations::Health::MastitisRiskPeriodType getCurrentMastitisRiskPeriodType() = 0;
//            virtual void needMastitisTreatment(const boost::gregorian::date &date, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity) = 0;
//            virtual void updateUdderEndMilkWaitingDate(const boost::gregorian::date &date) = 0;
//            virtual void cowIsCulledForSevereMastitis(const boost::gregorian::date &date) = 0;
//            virtual float getCurrentMastitisPreventionFactor() = 0;
//#ifdef _LOG
//            virtual unsigned int getMammalUniqueId()=0;
//#endif // _LOG            
//        };
//    }
//}
//}
//#endif // Health_DairyQuarterMastitisInfectionStates_I_DairyHerdMastitisProneDairyQuarter_h
