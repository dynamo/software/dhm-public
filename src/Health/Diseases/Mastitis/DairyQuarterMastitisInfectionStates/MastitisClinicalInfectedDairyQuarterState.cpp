#include "MastitisClinicalInfectedDairyQuarterState.h"

// project
//#include "I_DairyHerdMastitisProneDairyQuarter.h"
#include "../../../../DataStructures/DairyQuarter.h"
#include "MastitisSensitiveDairyQuarterState.h"
#include "../../../../ResultStructures/DairyFarmResult.h"

BOOST_CLASS_EXPORT(Health::Mastitis::DairyQuarterInfectionStates::MastitisClinicalInfectedDairyQuarterState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(MastitisInfectedDairyQuarterState); \
        ar & BOOST_SERIALIZATION_NVP(_milkProductLossDueToClinicalMastitisHightEffectPercent); \
        ar & BOOST_SERIALIZATION_NVP(_predictedEndHighEffectDate); \

        IMPLEMENT_SERIALIZE_STD_METHODS(MastitisClinicalInfectedDairyQuarterState);

//        MastitisClinicalInfectedDairyQuarterState::MastitisClinicalInfectedDairyQuarterState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity, bool newCase, bool newClinical
        MastitisClinicalInfectedDairyQuarterState::MastitisClinicalInfectedDairyQuarterState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity, bool newCase, bool newClinical
                                                            , Tools::Random &random) : MastitisInfectedDairyQuarterState(beginDate, pDairyQuarter, bacterium, severity, random)
        {
            _pAppliesToDairyQuarter->appendMastitis(bacterium, beginDate, newCase, false, newClinical);

            // Mastitis treatment :
            _pAppliesToDairyQuarter->needMastitisTreatment(beginDate, bacterium, severity);
            
            // Durations depending on the bacterium
            calculateHightEffectDuration(beginDate, bacterium, random);

            // Milk loss percent
            _milkProductLossDueToClinicalMastitisHightEffectPercent = random.ran_flat(0.0f, (FunctionalConstants::Health::MAX_MASTITIS_MILK_PRODUCT_REDUCTION_PERCENT_DUE_TO_CLINICAL_HIGHT_EFFECT.find(bacterium)->second).find(severity)->second);

        }

        void MastitisClinicalInfectedDairyQuarterState::progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState)
        {
            MastitisInfectedDairyQuarterState::progress(simDate, healthToday, random, pNewState);
            // Last day of the treatment effect ?
            if (_predictedTreatmentEffectDate == simDate)
            {
                // Not infection Healing ?
                if (_predictedEndDate != simDate)
                {
                    // Ask for a new traitment
                    _pAppliesToDairyQuarter->needMastitisTreatment(simDate, _concernedBacteriumType, _severity);
                }
            }
        }
                
        void MastitisClinicalInfectedDairyQuarterState::calculateHightEffectDuration(const boost::gregorian::date &beginDate, FunctionalEnumerations::Health::BacteriumType bacterium, Tools::Random &random)
        {
            unsigned int hightEffectDurationMini = FunctionalConstants::Health::MIN_MASTITIS_MILK_LOSS_DURATION.find(bacterium)->second;
            unsigned int hightEffectDurationMaxi = FunctionalConstants::Health::MAX_MASTITIS_MILK_LOSS_DURATION.find(bacterium)->second;
            
            unsigned int hightEffectDuration = random.rng_uniform_int(hightEffectDurationMaxi - hightEffectDurationMini) + hightEffectDurationMini;
            _predictedEndHighEffectDate = beginDate + boost::gregorian::date_duration(hightEffectDuration);
        }

        float MastitisClinicalInfectedDairyQuarterState::getHightEffectMastitisMilkProductReduction(const boost::gregorian::date &theDate)
        {
            if (theDate > _predictedEndHighEffectDate) return 0.0f;
            else return _milkProductLossDueToClinicalMastitisHightEffectPercent;
        }

        MastitisClinicalInfectedDairyQuarterState::~MastitisClinicalInfectedDairyQuarterState()
        {

        }        
    }
}
}