#ifndef Health_DairyQuarterMastitisInfectionStates_MastitisClinicalInfectedDairyQuarterState_h
#define Health_DairyQuarterMastitisInfectionStates_MastitisClinicalInfectedDairyQuarterState_h

// project
#include "MastitisInfectedDairyQuarterState.h"
#include "../../../../Tools/Random.h"
#include "../../../../ExchangeInfoStructures/TreatmentTypeParameters.h"

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        class MastitisClinicalInfectedDairyQuarterState : public MastitisInfectedDairyQuarterState
        {
        private:

        DECLARE_SERIALIZE_STD_METHODS;
        
            float _milkProductLossDueToClinicalMastitisHightEffectPercent;
            boost::gregorian::date _predictedEndHighEffectDate;
        private:
            void calculateHightEffectDuration(const boost::gregorian::date &beginDate, FunctionalEnumerations::Health::BacteriumType bacterium, Tools::Random &random);
        protected:
        public:
            MastitisClinicalInfectedDairyQuarterState(){};
            
            //MastitisClinicalInfectedDairyQuarterState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity, bool newCase, bool newClinical, Tools::Random &random);
            MastitisClinicalInfectedDairyQuarterState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity, bool newCase, bool newClinical, Tools::Random &random);
            float getHightEffectMastitisMilkProductReduction(const boost::gregorian::date &theDate) override;
            virtual ~MastitisClinicalInfectedDairyQuarterState();
            virtual void progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState) override;
            bool inline isClinicalInfected() override {return true;}
        };
    }
}
}
#endif // Health_DairyQuarterMastitisInfectionStates_MastitisClinicalInfectedDairyQuarterState_h
