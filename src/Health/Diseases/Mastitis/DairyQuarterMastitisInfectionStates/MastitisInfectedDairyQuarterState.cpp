#include "MastitisInfectedDairyQuarterState.h"

// project
//#include "I_DairyHerdMastitisProneDairyQuarter.h"
#include "MastitisSensitiveDairyQuarterState.h"
#include "MastitisUnsensitiveDairyQuarterState.h"
#include "../../../HealthManagement.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"
#include "../../../../ExchangeInfoStructures/TechnicalConstants.h"
#include "../../../../ExchangeInfoStructures/FunctionalEnumerations.h"

BOOST_CLASS_EXPORT(Health::Mastitis::DairyQuarterInfectionStates::MastitisInfectedDairyQuarterState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyHerdDairyQuarterMastitisInfectionState); \
        ar & BOOST_SERIALIZATION_NVP(_healedByOtherHealingOrTreatment); \
        ar & BOOST_SERIALIZATION_NVP(_predictedTreatmentEffectDate); \
        ar & BOOST_SERIALIZATION_NVP(_healingProbability); \
        ar & BOOST_SERIALIZATION_NVP(_protectProbability); \
        ar & BOOST_SERIALIZATION_NVP(_protectDuration); \
        ar & BOOST_SERIALIZATION_NVP(_severity); \

        IMPLEMENT_SERIALIZE_STD_METHODS(MastitisInfectedDairyQuarterState);

        //MastitisInfectedDairyQuarterState::MastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity, Tools::Random &random) : DairyHerdDairyQuarterMastitisInfectionState(beginDate, pDairyQuarter, bacterium)
        MastitisInfectedDairyQuarterState::MastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity, Tools::Random &random) : DairyHerdDairyQuarterMastitisInfectionState(beginDate, pDairyQuarter, bacterium)
        {
            // By default, the date of the treatment effect is infinite
            _predictedTreatmentEffectDate = boost::gregorian::date(boost::gregorian::pos_infin);           
            _severity = severity;
        }

        void MastitisInfectedDairyQuarterState::progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState)
        {
            if (_predictedTreatmentEffectDate == simDate)
            {
                // Healing by treatment ?
                if (random.ran_bernoulli(_healingProbability))
                {
                    _predictedEndDate = simDate; // The healig is today
                    _healedByOtherHealingOrTreatment = true; // This healing comes from treatment
                    if (!random.ran_bernoulli(_protectProbability))
                    {
                        _protectDuration = 0;
                    }                        
                }
            }
        }

        void MastitisInfectedDairyQuarterState::healTomorrowBecauseOfOccurencyHealth(const boost::gregorian::date &date)
        {
            _predictedEndDate = date + boost::gregorian::days(1);
            _healedByOtherHealingOrTreatment = true; // This healing comes from another healing
            // No protection
        }

        void MastitisInfectedDairyQuarterState::setTreatmentInformations(const boost::gregorian::date &simDate, unsigned int effectDelay, float healingProbability, float protectProbability, unsigned int protectDuration, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState)
        {
            _predictedTreatmentEffectDate = simDate + boost::gregorian::days(effectDelay);
            _healingProbability = healingProbability;
            _protectProbability = protectProbability;
            _protectDuration = protectDuration;
        }

        DairyHerdDairyQuarterMastitisInfectionState* MastitisInfectedDairyQuarterState::getHealUpState(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random)
        {
            // Other bacterium or treatment healing :
            if (healthToday.find(simDate) == healthToday.end())
            {
                healthToday[simDate] = _concernedBacteriumType; 
            }
            
            if (_protectDuration > 0)
            {
                DairyQuarterInfectionStates::MastitisUnsensitiveDairyQuarterState* pNewState = new DairyQuarterInfectionStates::MastitisUnsensitiveDairyQuarterState(simDate, _pAppliesToDairyQuarter, _concernedBacteriumType);
                boost::gregorian::date_duration duration = boost::gregorian::date_duration(_protectDuration);
                pNewState->setDuration(duration);
                return pNewState;  
            }
            else
            {
                if (not _healedByOtherHealingOrTreatment)
                {
                    // Persisting mastitis and pontaneous healing, so may be a relapse
                    Health::HealthManagement::programPotentialMastitisRelapse(_beginDate, getMastitisSeverity(), _concernedBacteriumType, _pAppliesToDairyQuarter, random);               
                }
                return new MastitisSensitiveDairyQuarterState(simDate, _pAppliesToDairyQuarter, _concernedBacteriumType);
            }
        }

        unsigned int MastitisInfectedDairyQuarterState::getMastitisAdditionalSCC(Tools::Random &random)
        {
            // Original code
            float SCCMini = (FunctionalConstants::Health::MIN_MASTITIS_SCC_LEVEL.find(_concernedBacteriumType)->second).find(getMastitisSeverity())->second;
            float SCCMaxi = (FunctionalConstants::Health::MAX_MASTITIS_SCC_LEVEL.find(_concernedBacteriumType)->second).find(getMastitisSeverity())->second;
            float mostLikely = SCCMini + (SCCMaxi - SCCMini)/FunctionalConstants::Health::ADDITIONAL_SCC_RATIO_MOST_LIKELY ;
            return random.betaPertDistribution(SCCMini, SCCMaxi, mostLikely, 5.0f);
        }

        MastitisInfectedDairyQuarterState::~MastitisInfectedDairyQuarterState()
        {
        }

#ifdef _LOG
        std::string MastitisInfectedDairyQuarterState::getStrCurrentMastitisDiagnostic()
        {
            return "-I-" + getStrGravity() + "]";
        }
#endif // _LOG            
    }
}
}
