#ifndef Health_DairyQuarterMastitisInfectionStates_MastitisInfectedDairyQuarterState_h
#define Health_DairyQuarterMastitisInfectionStates_MastitisInfectedDairyQuarterState_h

// project
#include "DairyHerdDairyQuarterMastitisInfectionState.h"

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        class MastitisSensitiveDairyQuarterState;

        class MastitisInfectedDairyQuarterState : public DairyHerdDairyQuarterMastitisInfectionState
        {
            
            DECLARE_SERIALIZE_STD_METHODS;
        
        protected:
            boost::gregorian::date _predictedTreatmentEffectDate;
            float _healingProbability = 0.0f;
            float _protectProbability = 0.0f;
            unsigned int _protectDuration = 0;   
            bool _healedByOtherHealingOrTreatment = false;
            FunctionalEnumerations::Health::MastitisSeverity _severity;
             
        protected:
            MastitisInfectedDairyQuarterState(){};

            DairyHerdDairyQuarterMastitisInfectionState* getHealUpState(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random);
            
        public:
            //MastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity, Tools::Random &random);
            MastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity, Tools::Random &random);
            virtual ~MastitisInfectedDairyQuarterState();
            virtual void progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState) override;
            virtual inline bool isInfected() override {return true;}
            unsigned int getMastitisAdditionalSCC(Tools::Random &random) override;
            void healTomorrowBecauseOfOccurencyHealth(const boost::gregorian::date &date);
            void setTreatmentInformations(const boost::gregorian::date &simDate, unsigned int effectDelay, float healingProbability, float protectProbability, unsigned int protectionDuration, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState);
#ifdef _LOG
            std::string getStrCurrentMastitisDiagnostic();
            virtual std::string getStrGravity()=0;          
#endif // _LOG            
        };
    }
}
}
#endif // Health_DairyQuarterMastitisInfectionStates_MastitisInfectedDairyQuarterState_h
