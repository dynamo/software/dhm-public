#include "MastitisSensitiveDairyQuarterState.h"

// project
//#include "I_DairyHerdMastitisProneDairyQuarter.h"
#include "../../../../DataStructures/DairyQuarter.h"
#include "../../../../Tools/Random.h"
#include "MastitisUnsensitiveDairyQuarterState.h"
#include "G0SeverityMastitisInfectedDairyQuarterState.h"
#include "DetectedG1SeverityMastitisInfectedDairyQuarterState.h"
#include "UndetectedG1SeverityMastitisInfectedDairyQuarterState.h"
#include "G2SeverityMastitisInfectedDairyQuarterState.h"
#include "G3SeverityMastitisInfectedDairyQuarterState.h"

BOOST_CLASS_EXPORT(Health::Mastitis::DairyQuarterInfectionStates::MastitisSensitiveDairyQuarterState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyHerdDairyQuarterMastitisInfectionState);

        IMPLEMENT_SERIALIZE_STD_METHODS(MastitisSensitiveDairyQuarterState);


        //MastitisSensitiveDairyQuarterState::MastitisSensitiveDairyQuarterState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium) : DairyHerdDairyQuarterMastitisInfectionState(beginDate, pDairyQuarter, bacterium)
        MastitisSensitiveDairyQuarterState::MastitisSensitiveDairyQuarterState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium) : DairyHerdDairyQuarterMastitisInfectionState(beginDate, pDairyQuarter, bacterium)
        {
        }

        void MastitisSensitiveDairyQuarterState::progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState)
        {
            // This state is the origin of new mastitis occurence (or a relapse)
            if (!_pAppliesToDairyQuarter->hasBeenInfectedDuringTheStep()) // Only on occurency by step, the rarest bacterium did just progressed before 
            {

                FunctionalEnumerations::Health::MastitisSeverity severity;
                bool newMastitis = _pAppliesToDairyQuarter->hasProgramedMastitis(simDate, _concernedBacteriumType, severity); // Mastitis relapse ?
                if (not newMastitis)
                {
                    // Not programmed, so may be a random new occurence ?
                    // Basic probability depending on the bacterium type
                    float baseProba = _pAppliesToDairyQuarter->getMastitisOccurenceProbability(_concernedBacteriumType);

                    // Mastitis prevention modality factor
                    // First we have to know the severity occurency
                    severity = _pAppliesToDairyQuarter->getRandomMastitisSeverity(_concernedBacteriumType);                       

                    baseProba /= _pAppliesToDairyQuarter->getCurrentMastitisPreventionFactor();


                    if (random.ran_bernoulli(baseProba)) // Mastitis occurence ?
                    {
                        // This is a new mastitis occurence
                        newMastitis = true;
                    }
                }

                if (newMastitis)
                {
                    // Mastitis creating
                    if (severity == FunctionalEnumerations::Health::MastitisSeverity::G0)
                    {
                        pNewState = new G0SeverityMastitisInfectedDairyQuarterState(simDate, _pAppliesToDairyQuarter, _concernedBacteriumType, true, random);
                    }
                    else if (severity == FunctionalEnumerations::Health::MastitisSeverity::G1)
                    {
                        if (_pAppliesToDairyQuarter->G1SeverityMastitisIsDetected())
                        {
                            pNewState = new DetectedG1SeverityMastitisInfectedDairyQuarterState(simDate, _pAppliesToDairyQuarter, _concernedBacteriumType, true, true, random);
                        }
                        else
                        {
                            pNewState = new UndetectedG1SeverityMastitisInfectedDairyQuarterState(simDate, _pAppliesToDairyQuarter, _concernedBacteriumType, true, random);
                        }
                    }
                    else if (severity == FunctionalEnumerations::Health::MastitisSeverity::G2)
                    {
                        pNewState = new G2SeverityMastitisInfectedDairyQuarterState(simDate, _pAppliesToDairyQuarter, _concernedBacteriumType, true, true, random);
                    }
                    else if (severity == FunctionalEnumerations::Health::MastitisSeverity::G3)
                    {
                        pNewState = new G3SeverityMastitisInfectedDairyQuarterState(simDate, _pAppliesToDairyQuarter, _concernedBacteriumType, true, true, random);
                    }
#ifdef _LOG
                    assert (pNewState != nullptr);
#endif // _LOG
                }  
            }
        }

        bool MastitisSensitiveDairyQuarterState::isSensitive()
        {
            return true;
        }

        MastitisSensitiveDairyQuarterState::~MastitisSensitiveDairyQuarterState()
        {

        }
        void MastitisSensitiveDairyQuarterState::setTreatmentInformations(const boost::gregorian::date &simDate, unsigned int effectDelay, float healingProbability, float protectProbability, unsigned int protectDuration, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState)
        {
            // Protection ?
            if (random.ran_bernoulli(protectProbability))
            {
                pNewState = new MastitisUnsensitiveDairyQuarterState(simDate, _pAppliesToDairyQuarter, _concernedBacteriumType);
                boost::gregorian::date_duration duration = boost::gregorian::date_duration(protectDuration);
                pNewState->setDuration(duration);
            }
        }
        
#ifdef _LOG
        std::string MastitisSensitiveDairyQuarterState::getStrCurrentMastitisDiagnostic()
        {
            return "-S-   ]" ;
        }
#endif // _LOG            

    }
}
}