#ifndef Health_DairyQuarterMastitisInfectionStates_MastitisSensitiveDairyQuarterState_h
#define Health_DairyQuarterMastitisInfectionStates_MastitisSensitiveDairyQuarterState_h

// project
#include "DairyHerdDairyQuarterMastitisInfectionState.h"
#include "../../../../Tools/Random.h"

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        class MastitisSensitiveDairyQuarterState : public DairyHerdDairyQuarterMastitisInfectionState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        protected:
            void concrete(){}; // To allow instanciation
        public:
            MastitisSensitiveDairyQuarterState(){};

            //MastitisSensitiveDairyQuarterState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium);
            MastitisSensitiveDairyQuarterState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium);
            virtual ~MastitisSensitiveDairyQuarterState();
            virtual void progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState) override;
            bool isSensitive() override;
            void setTreatmentInformations(const boost::gregorian::date &simDate, unsigned int effectDelay, float healingProbability, float protectProbability, unsigned int protectionDuration, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState);
#ifdef _LOG
            std::string getStrCurrentMastitisDiagnostic();
#endif // _LOG            
        };
    }
}
}

#endif // Health_DairyQuarterMastitisInfectionStates_MastitisSensitiveDairyQuarterState_h
