#include "MastitisSubclinicalInfectedDairyQuarterState.h"

// project
//#include "I_DairyHerdMastitisProneDairyQuarter.h"
#include "../../../../DataStructures/DairyQuarter.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(Health::Mastitis::DairyQuarterInfectionStates::MastitisSubclinicalInfectedDairyQuarterState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(MastitisInfectedDairyQuarterState);

        IMPLEMENT_SERIALIZE_STD_METHODS(MastitisSubclinicalInfectedDairyQuarterState);


        //MastitisSubclinicalInfectedDairyQuarterState::MastitisSubclinicalInfectedDairyQuarterState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity, bool newCase, Tools::Random &random) : MastitisInfectedDairyQuarterState(beginDate, pDairyQuarter, bacterium, severity, random)
        MastitisSubclinicalInfectedDairyQuarterState::MastitisSubclinicalInfectedDairyQuarterState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity, bool newCase, Tools::Random &random) : MastitisInfectedDairyQuarterState(beginDate, pDairyQuarter, bacterium, severity, random)
        {
            pDairyQuarter->appendMastitis(bacterium, beginDate, newCase, true, false);
        }

        void MastitisSubclinicalInfectedDairyQuarterState::progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState)
        {
            MastitisInfectedDairyQuarterState::progress(simDate, healthToday, random, pNewState);
        }

        MastitisSubclinicalInfectedDairyQuarterState::~MastitisSubclinicalInfectedDairyQuarterState()
        {

        }        
    }
}
}