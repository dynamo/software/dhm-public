#ifndef Health_DairyQuarterMastitisInfectionStates_MastitisSubclinicalInfectedDairyQuarterState_h
#define Health_DairyQuarterMastitisInfectionStates_MastitisSubclinicalInfectedDairyQuarterState_h

// project
#include "MastitisInfectedDairyQuarterState.h"
#include "../../../../Tools/Random.h"

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        class MastitisSubclinicalInfectedDairyQuarterState : public MastitisInfectedDairyQuarterState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        protected:

        public:
            MastitisSubclinicalInfectedDairyQuarterState(){};

            //MastitisSubclinicalInfectedDairyQuarterState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity, bool newCase, Tools::Random &random);
            MastitisSubclinicalInfectedDairyQuarterState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverity severity, bool newCase, Tools::Random &random);
            virtual ~MastitisSubclinicalInfectedDairyQuarterState();
            virtual void progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState) override;
        };
    }
}
}
#endif // Health_DairyQuarterMastitisInfectionStates_MastitisSubclinicalInfectedDairyQuarterState_h
