#include "MastitisUnsensitiveDairyQuarterState.h"

// project
//#include "I_DairyHerdMastitisProneDairyQuarter.h"
#include "MastitisSensitiveDairyQuarterState.h"

BOOST_CLASS_EXPORT(Health::Mastitis::DairyQuarterInfectionStates::MastitisUnsensitiveDairyQuarterState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyHerdDairyQuarterMastitisInfectionState);

        IMPLEMENT_SERIALIZE_STD_METHODS(MastitisUnsensitiveDairyQuarterState);

        //MastitisUnsensitiveDairyQuarterState::MastitisUnsensitiveDairyQuarterState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium) : DairyHerdDairyQuarterMastitisInfectionState(beginDate, pDairyQuarter, bacterium)
        MastitisUnsensitiveDairyQuarterState::MastitisUnsensitiveDairyQuarterState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium) : DairyHerdDairyQuarterMastitisInfectionState(beginDate, pDairyQuarter, bacterium)
        {
        }

        void MastitisUnsensitiveDairyQuarterState::progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState)

        {
            // End of unsensitive state ?
            if (isOutOfTime(simDate))
            {
                pNewState = new MastitisSensitiveDairyQuarterState(simDate, _pAppliesToDairyQuarter, _concernedBacteriumType);
            }
        }

        MastitisUnsensitiveDairyQuarterState::~MastitisUnsensitiveDairyQuarterState()
        {
        }

        bool MastitisUnsensitiveDairyQuarterState::isUnsensitive()
        {
            return true;
        }

        void MastitisUnsensitiveDairyQuarterState::setTreatmentInformations(const boost::gregorian::date &simDate, unsigned int effectDelay, float healingProbability, float protectProbability, unsigned int protectDuration, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState)
        {
            // Protection ?
            if (random.ran_bernoulli(protectProbability))
            {
                boost::gregorian::date endProtectionDate = simDate + boost::gregorian::days(protectDuration);
                if (endProtectionDate > _predictedEndDate)
                {
                    _predictedEndDate = endProtectionDate;
                }
            }
        }
#ifdef _LOG
        std::string MastitisUnsensitiveDairyQuarterState::getStrCurrentMastitisDiagnostic()
        {
            return "-U-   ]" ;
        }
#endif // _LOG            
    }
}
}