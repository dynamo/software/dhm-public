#ifndef Health_DairyQuarterMastitisInfectionStates_MastitisUnsensitiveDairyQuarterState_h
#define Health_DairyQuarterMastitisInfectionStatesMastitisStates_MastitisUnsensitiveDairyQuarterState_h

// project
#include "DairyHerdDairyQuarterMastitisInfectionState.h"

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        class MastitisUnsensitiveDairyQuarterState : public DairyHerdDairyQuarterMastitisInfectionState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        protected:
            void concrete(){}; // To allow instanciation
        public:
            MastitisUnsensitiveDairyQuarterState(){};
            //MastitisUnsensitiveDairyQuarterState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium);
            MastitisUnsensitiveDairyQuarterState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium);
            virtual ~MastitisUnsensitiveDairyQuarterState();
            virtual void progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState) override;
            bool isUnsensitive() override;
            void setTreatmentInformations(const boost::gregorian::date &simDate, unsigned int effectDelay, float healingProbability, float protectProbability, unsigned int protectionDuration, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState);
#ifdef _LOG
            std::string getStrCurrentMastitisDiagnostic();
#endif // _LOG            
        };
    }
}
}
#endif // Health_DairyQuarterMastitisInfectionStates_MastitisUnsensitiveDairyQuarterState_h
