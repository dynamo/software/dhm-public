#include "UndetectedG1SeverityMastitisInfectedDairyQuarterState.h"

// project
#include "MastitisSensitiveDairyQuarterState.h"
#include "G0SeverityMastitisInfectedDairyQuarterState.h"
#include "G2SeverityMastitisInfectedDairyQuarterState.h"
#include "../../../HealthManagement.h"
#include "../../../../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(Health::Mastitis::DairyQuarterInfectionStates::UndetectedG1SeverityMastitisInfectedDairyQuarterState) // Recommended (mandatory if virtual serialization)

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(MastitisSubclinicalInfectedDairyQuarterState); \
        ar & BOOST_SERIALIZATION_NVP(_flareup); \

        IMPLEMENT_SERIALIZE_STD_METHODS(UndetectedG1SeverityMastitisInfectedDairyQuarterState);

        //UndetectedG1SeverityMastitisInfectedDairyQuarterState::UndetectedG1SeverityMastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, bool newCase, Tools::Random &random)
        UndetectedG1SeverityMastitisInfectedDairyQuarterState::UndetectedG1SeverityMastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, bool newCase, Tools::Random &random)
                                                      : MastitisSubclinicalInfectedDairyQuarterState(beginDate, pDairyQuarter, bacterium, FunctionalEnumerations::Health::MastitisSeverity::G1, newCase, random)
        {             

            if (random.ran_bernoulli(FunctionalConstants::Health::G1_FLARE_UP_PROBABILITY))
            {
                // Flare up : G1 become G2
                _predictedEndDate = beginDate + boost::gregorian::days(FunctionalConstants::Health::G1_FLARE_UP_DELAY);
                _flareup = true;
            }
            else
            {
                boost::gregorian::date_duration duration = boost::gregorian::date_duration(FunctionalConstants::Health::MASTITIS_G1_TO_G0_DELAY);
                setDuration(duration);
            }
        }

        FunctionalEnumerations::Health::MastitisSeverity UndetectedG1SeverityMastitisInfectedDairyQuarterState::getMastitisSeverity()
        {
            return FunctionalEnumerations::Health::MastitisSeverity::G1;
        }

        void UndetectedG1SeverityMastitisInfectedDairyQuarterState::progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState)
        {
            MastitisSubclinicalInfectedDairyQuarterState::progress(simDate, healthToday, random, pNewState);                
            if (isOutOfTime(simDate))
            {
                if (not _healedByOtherHealingOrTreatment and _flareup)
                {
                    // Flare up : G1 become G2
                    pNewState = new G2SeverityMastitisInfectedDairyQuarterState(simDate, _pAppliesToDairyQuarter, _concernedBacteriumType, false, true, random);
                }
                else if (_healedByOtherHealingOrTreatment or random.ran_bernoulli((FunctionalConstants::Health::MASTITIS_SPONTANEOUS_HEALING_PROBABILITY.find(_concernedBacteriumType)->second).find(_pAppliesToDairyQuarter->getCurrentMastitisRiskPeriodType())->second))
                {
                    // Spontaneous (or other bacterium) healing
                    pNewState = getHealUpState(simDate, healthToday, random);
                }
                else
                {
                    // G1 become G0
                    pNewState = new G0SeverityMastitisInfectedDairyQuarterState(simDate, _pAppliesToDairyQuarter, _concernedBacteriumType, false, random);
                }
            }
        }
        
        UndetectedG1SeverityMastitisInfectedDairyQuarterState::~UndetectedG1SeverityMastitisInfectedDairyQuarterState()
        {
        }

#ifdef _LOG
        std::string UndetectedG1SeverityMastitisInfectedDairyQuarterState::getStrGravity()
        {
            return "G1u" ;
        }
#endif // _LOG            
    }
}
}