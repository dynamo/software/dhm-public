#ifndef Health_DairyQuarterMastitisInfectionStates_UndetectedG1SeverityMastitisInfectedDairyQuarterState_h
#define Health_DairyQuarterMastitisInfectionStates_UndetectedG1SeverityMastitisInfectedDairyQuarterState_h

// project
#include "MastitisSubclinicalInfectedDairyQuarterState.h"

namespace Health
{
namespace Mastitis
{
    namespace DairyQuarterInfectionStates
    {
        class UndetectedG1SeverityMastitisInfectedDairyQuarterState : public MastitisSubclinicalInfectedDairyQuarterState
        {
            bool _flareup = false; // Next state will be G0 ?
            
        DECLARE_SERIALIZE_STD_METHODS;
        
        protected:
            UndetectedG1SeverityMastitisInfectedDairyQuarterState(){}; // For serialization
            void concrete(){}; // To allow instanciation

        public:

            //UndetectedG1SeverityMastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, bool newCase, Tools::Random &random);
            UndetectedG1SeverityMastitisInfectedDairyQuarterState(const boost::gregorian::date &beginDate, DataStructures::DairyQuarter* pDairyQuarter, FunctionalEnumerations::Health::BacteriumType bacterium, bool newCase, Tools::Random &random);
            virtual ~UndetectedG1SeverityMastitisInfectedDairyQuarterState();
            void progress(const boost::gregorian::date &simDate, std::map<boost::gregorian::date, FunctionalEnumerations::Health::BacteriumType> &healthToday, Tools::Random &random, DairyHerdDairyQuarterMastitisInfectionState* &pNewState);
            FunctionalEnumerations::Health::MastitisSeverity getMastitisSeverity() override;
            inline bool isG1ToG3MastitisInfected() override {return true;}
#ifdef _LOG
            std::string getStrGravity();
#endif // _LOG            
        };
    }
}
}

#endif // Health_DairyQuarterMastitisInfectionStates_UndetectedG1SeverityMastitisInfectedDairyQuarterState_h
