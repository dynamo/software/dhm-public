#include "HealthManagement.h"

// standard

// Boost
#include <boost/date_time/gregorian/gregorian.hpp>

// Project
#include "../Tools/Random.h"
#include "../Tools/Tools.h"
#include "../ExchangeInfoStructures/TechnicalConstants.h"
#include "./Diseases/Mastitis/DairyCowMastitisRiskPeriodStates/DairyCowMastitisRiskPeriodState.h"
#include "./Diseases/Ketosis/KetosisStates/G1SubclinicalKetosisState.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"
#include "../ExchangeInfoStructures/TechnicalConstants.h"
#include "Diseases/Lameness/LamenessStates/G1InfectiousLamenessState.h"
#include "Diseases/Lameness/LamenessStates/G1NonInfectiousLamenessState.h"
#include "Diseases/Lameness/LamenessStates/G2InfectiousLamenessState.h"
#include "Diseases/Lameness/LamenessStates/G2NonInfectiousLamenessState.h"
#include "../DataStructures/DairyMammal.h"

namespace Health
{
    void HealthManagement::initMastitisRiskPeriodMatrix(std::string &technicalDataFolder)
    {
       // Preparing the collection
        for (unsigned int indBacterium = FunctionalEnumerations::Health::BacteriumType::Gn; indBacterium <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; indBacterium++)
        {
            std::map<FunctionalEnumerations::Health::MastitisRiskPeriodType, std::vector<double>> vTmpRiskPeriod;
            for (unsigned int indRiskPeriod = FunctionalEnumerations::Health::MastitisRiskPeriodType::periPartumAndLactationBegin; indRiskPeriod <= FunctionalEnumerations::Health::MastitisRiskPeriodType::lastMastitisRiskPeriodType; indRiskPeriod++)
            {
                vTmpRiskPeriod[(FunctionalEnumerations::Health::MastitisRiskPeriodType)indRiskPeriod] = std::vector<double>();
            }
            s_mastitisRiskPeriodMatrix[(FunctionalEnumerations::Health::BacteriumType)indBacterium] = vTmpRiskPeriod;
        }
                
        // Data file reading
        //std::string mastitisCurveFileName = TechnicalConstants::RELATIVE_TECHNICAL_DATA_FOLDER + TechnicalConstants::MASTITIS_RISK_CURVE_FILE_NAME;
        std::string mastitisCurveFileName = technicalDataFolder + TechnicalConstants::MASTITIS_RISK_CURVE_FILE_NAME;
        std::vector<std::vector<std::string>> vLines;
        Tools::readFile(vLines, mastitisCurveFileName, TechnicalConstants::EXPORT_SEP);
        assert(vLines.size() > 0);
        for (auto itLigne = vLines.begin(); itLigne != vLines.end(); itLigne++)
        {
            std::vector<std::string> &vTempLine = *itLigne;
            FunctionalEnumerations::Health::BacteriumType iBacterium = (FunctionalEnumerations::Health::BacteriumType)std::atoi(vTempLine[0].c_str());
            FunctionalEnumerations::Health::MastitisRiskPeriodType iRiskPeriod = (FunctionalEnumerations::Health::MastitisRiskPeriodType)std::atoi(vTempLine[1].c_str());
            std::vector<double> &pTempValues = (((s_mastitisRiskPeriodMatrix.find(iBacterium)->second).find(iRiskPeriod))->second);
            for (unsigned int indField = 2; indField < vTempLine.size(); indField++)
            {
                pTempValues.push_back(atof(vTempLine[indField].c_str()));
            }
        }
    }

    void HealthManagement::initKetosisRiskPeriodCurve(std::string &technicalDataFolder)
    {
        // Data file reading
        //std::string ketosisCurveFileName = TechnicalConstants::RELATIVE_TECHNICAL_DATA_FOLDER + TechnicalConstants::KETOSIS_RISK_CURVE_FILE_NAME;
        std::string ketosisCurveFileName = technicalDataFolder + TechnicalConstants::KETOSIS_RISK_CURVE_FILE_NAME;
        std::vector<std::vector<std::string>> vLines;
        Tools::readFile(vLines, ketosisCurveFileName, TechnicalConstants::EXPORT_SEP);
        assert(vLines.size() > 0);
        for (auto itLigne = vLines.begin(); itLigne != vLines.end(); itLigne++)
        {
            s_ketosisRiskPeriodCurve.push_back(atof((*itLigne)[1].c_str()));
        }
    }
    
    void HealthManagement::initLamenessRiskLactationPeriodCurve(std::string &technicalDataFolder)
    {
        // Data file reading
        //std::string lamenessCurveFileName = TechnicalConstants::RELATIVE_TECHNICAL_DATA_FOLDER + TechnicalConstants::LAMENESS_RISK_CURVE_FILE_NAME;
        std::string lamenessCurveFileName = technicalDataFolder + TechnicalConstants::LAMENESS_RISK_CURVE_FILE_NAME;
        std::vector<std::vector<std::string>> vLines;
        Tools::readFile(vLines, lamenessCurveFileName, TechnicalConstants::EXPORT_SEP);
        assert(vLines.size() > 0);
        for (auto itLigne = vLines.begin(); itLigne != vLines.end(); itLigne++)
        {
            s_lamenessRiskLactationPeriodCurve.push_back(atof((*itLigne)[0].c_str())); // Getvalue
        }
    }

    void HealthManagement::initLamenessStabulationRiskCurve(std::string &technicalDataFolder)
    {
        for (unsigned int i = 0; i <= FunctionalConstants::Health::LAMENESS_SEASON_RISK_FACTOR_DAY_2; i++)
        {
            if (i <= FunctionalConstants::Health::LAMENESS_SEASON_RISK_FACTOR_DAY_1)
            { 
                // Basis effect
                s_lamenessStabulationRiskCurve.push_back(1.0f);
            }
            else
            { 
                // Effect to be calculated
                float val = 1.0f + ((float)(i - FunctionalConstants::Health::LAMENESS_SEASON_RISK_FACTOR_DAY_1)/(float)(FunctionalConstants::Health::LAMENESS_SEASON_RISK_FACTOR_DAY_2 - FunctionalConstants::Health::LAMENESS_SEASON_RISK_FACTOR_DAY_1)) * (FunctionalConstants::Health::LAMENESS_SEASON_RISK_FACTOR_MAX_EFFECT - 1.0f);
                s_lamenessStabulationRiskCurve.push_back(val);
            }
        }
    }

    std::vector<double> &HealthManagement::getMastitisRiskPeriodProbabilities(FunctionalEnumerations::Health::BacteriumType bacteriumType, FunctionalEnumerations::Health::MastitisRiskPeriodType periodType)
    {
        return (s_mastitisRiskPeriodMatrix.find(bacteriumType)->second).find(periodType)->second;
    }
    
    double HealthManagement::getMastitisOccurenceProbability(   FunctionalEnumerations::Health::BacteriumType bacterium,
                                                                const boost::gregorian::date &date,
                                                                Mastitis::RiskPeriodStates::DairyCowMastitisRiskPeriodState* pMastitisPeriodState,
                                                                float saisonalityFactor,
                                                                float bacteriumIncidencePart,
                                                                unsigned int weekBatchPrevalence,
                                                                bool lactationIsOngoing,
                                                                float milkProductOfTheDay,
                                                                float preexistingSCC,
                                                                float MACL_CorrectedPerformance,
                                                                FunctionalEnumerations::Health::MastitisLactationRankModulation lactationRankForModulation,
                                                                float milkPotentialFactor,
                                                                bool udderIsAlreadyInfectedByThisBacterium)
    {
        // Give the mastitis occurence probability
        // ---------------------------------------
        // Batch contagion factor
        float batchContagionFactor = 1.0f + FunctionalConstants::Health::MASTITIS_BATCH_CONTAGION_EFFECT.find(bacterium)->second * weekBatchPrevalence;

        // Milk product
        float milkProductFactor;
        float preexistingSCCFactor;
        if (not lactationIsOngoing)
        {
            milkProductFactor = 1.0f;
            preexistingSCCFactor = 1.0f;
        }
        else
        {
            // Milk product factor
            milkProductFactor = Tools::getProportionalFactor(milkProductOfTheDay, FunctionalConstants::Health::MASTITIS_MILK_PRODUCT_MODULATION.find(bacterium)->second);
            
            // Preexisting SCC factor
            if (preexistingSCC < FunctionalConstants::Health::SCC_LOW_LEVEL_MASTITIS_MODULATION)
            {
                preexistingSCCFactor = std::get<0>(FunctionalConstants::Health::PRE_EXISTING_SCC_MASTITIS_MODULATION.find(bacterium)->second);
            }
            else if (preexistingSCC > FunctionalConstants::Health::SCC_HI_LEVEL_MASTITIS_MODULATION)
            {
                preexistingSCCFactor = std::get<2>(FunctionalConstants::Health::PRE_EXISTING_SCC_MASTITIS_MODULATION.find(bacterium)->second);
            }
            else
            {
                preexistingSCCFactor = std::get<1>(FunctionalConstants::Health::PRE_EXISTING_SCC_MASTITIS_MODULATION.find(bacterium)->second);
            }
        }

        // Lactation rank factor
        float lactationRankFactor = FunctionalConstants::Health::MASTITIS_LACTATION_RANK_MODULATION.find(lactationRankForModulation)->second;

        // Dairy quarter dependency factor
        float dairyQuarterDependencyFactor = udderIsAlreadyInfectedByThisBacterium ? FunctionalConstants::Health::QUARTER_MASTITIS_PROBA_DEPENDANCIES.find(bacterium)->second : 1.0f;

        // Probability calculation
        return  pMastitisPeriodState->getCurrentMastitisRiskPeriodProbability(date, bacterium) *
                TechnicalConstants::BASE_CALIBRATION_MASTITIS_INCIDENCE * 
//                6.52f * 
                saisonalityFactor *
                bacteriumIncidencePart * 
                batchContagionFactor *
                milkProductFactor *
                preexistingSCCFactor *
                lactationRankFactor *
                milkPotentialFactor *
                MACL_CorrectedPerformance *
                dairyQuarterDependencyFactor / (FunctionalEnumerations::Health::lastBacteriumType+1);
    }
        
    FunctionalEnumerations::Health::MastitisSeverity HealthManagement::getRandomMastitisSeverity(FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverityDegree degree, Tools::Random &random)
    {
        // Give a mastitis severity depending on the bacterium and the severity degree
        // ---------------------------------------------------------------------------
       
        const std::map<FunctionalEnumerations::Health::MastitisSeverity, float> &refTable = (FunctionalConstants::Health::MASTITIS_SEVERITY_DISTRIBUTION.find(degree)->second).find(bacterium)->second;
        double floor = 0.0f;
        double rand = random.ran_flat(floor, 0.99f);
        //double rand = random.ran_flat(floor, 0.99f)/4.0f;
        for (std::map<FunctionalEnumerations::Health::MastitisSeverity, float>::const_iterator it = refTable.begin(); it != refTable.end(); it++)
        {
            if (rand >= floor and rand < floor + it->second)
            {
                return it->first;
            }
            floor += it->second;
        }
        

        // All the proportion have been explored before, so we can't be here
        throw std::runtime_error("HealthManagement::getRandomMastitisSeverity : out of proportion");
    }
    
#ifdef _LOG
    void HealthManagement::saveMastitisRiskPeriodMatrix(const std::string &theResultPath)
    {
        // Save mastitis risk period matrix in one file by bacterium (for documentation)
        // -----------------------------------------------------------------------------
        // Preparing the log folder
        std::string logPath = theResultPath + TechnicalConstants::LOG_DIRECTORY;
        mkdir(logPath.c_str() DIR_RIGHT);  

        // Get the mastitis risk period matrix informations
        for (unsigned int iBact = 0; iBact <= FunctionalEnumerations::Health::BacteriumType::lastBacteriumType; iBact++)
        {
            std::vector<std::string> linesForTheSaveFile;
            std::string fileName = logPath + Tools::toString(iBact) + "-MastitisRiskPeriodMatrix.csv";
            std::map<FunctionalEnumerations::Health::MastitisRiskPeriodType, std::vector<double>> &tmpBacteriumPeriods = s_mastitisRiskPeriodMatrix.find((FunctionalEnumerations::Health::BacteriumType)iBact)->second;
            for (std::map<FunctionalEnumerations::Health::MastitisRiskPeriodType, std::vector<double>>::iterator itPeriod = tmpBacteriumPeriods.begin(); itPeriod != tmpBacteriumPeriods.end(); itPeriod++)
            {
                std::vector<double> &data = itPeriod->second;
                for (std::vector<double>::iterator it = data.begin(); it != data.end(); it++)
                {
                    std::string line = Tools::toString(*it);
                    line = Tools::changeChar(line,'.', ',');
                    linesForTheSaveFile.push_back(line);
                }
            }
            Tools::writeFile(linesForTheSaveFile, fileName, true);        
        }
    }
    
    void HealthManagement::saveLamenessRiskPeriodMatrix(const std::string &theResultPath)
    {
        // Save lameness risk period matrix in one file by Lactation class and lameness type (-I or NI), for documentation
        // ---------------------------------------------------------------------------------------------------------------
        // Preparing the log folder
        std::string logPath = theResultPath + TechnicalConstants::LOG_DIRECTORY;
        mkdir(logPath.c_str() DIR_RIGHT);  
        
        // Get the lameness risk period matrix informations
        for (unsigned int i_ni = FunctionalEnumerations::Health::LamenessInfectiousStateType::nonInfectiousLamenessType; i_ni <= (unsigned int)FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType; i_ni++)
        {
            FunctionalEnumerations::Health::LamenessInfectiousStateType i_ni_enum = (FunctionalEnumerations::Health::LamenessInfectiousStateType)i_ni;
            for (unsigned int lactationRank = 1; lactationRank <= 4; lactationRank++ )
            {
                std::vector<std::string> linesForTheSaveFile;
                std::string fileName = logPath + Tools::toString(i_ni) + "-" + Tools::toString(lactationRank) + "-LamenessRiskPeriodMatrix.csv";
                
                float lactationRankFactor = FunctionalConstants::Health::LACTATION_RANK_LAMENESS_INCIDENCE_FACTOR.find(i_ni_enum)->second[lactationRank-1];
                
                // All periods
                float dryDaysAllPeriodDependingRankLamenessIncidenceByDay =         FunctionalConstants::Health::LAMENESS_BASIS_INCIDENCE_ALL_PERIODS.find(i_ni_enum)->second * lactationRankFactor
                                                                                / ((float)FunctionalConstants::Lactation::REFERENCE_DRY_DURATION);
                
                float lactationDaysAllPeriodDependingRankLamenessIncidenceByDay =   FunctionalConstants::Health::LAMENESS_BASIS_INCIDENCE_ALL_PERIODS.find(i_ni_enum)->second * lactationRankFactor
                                                                                / ((float)FunctionalConstants::Lactation::REFERENCE_LACTATION_DURATION);
                // Lactation incidence   
                float lactationPeriodDependingRankLamenessIncidence =  FunctionalConstants::Health::LAMENESS_BASIS_INCIDENCE_LACTATION_PERIOD.find(i_ni_enum)->second
                                                                                        * lactationRankFactor;
                for (unsigned int day = 0; day < 365; day++)
                {
                    float dayIncidence = 0.0f;
                    if (day < 305)
                    {
                        dayIncidence = lactationDaysAllPeriodDependingRankLamenessIncidenceByDay + lactationPeriodDependingRankLamenessIncidence * Health::HealthManagement::getLactationLamenessCurveFactor(day);
                    }
                    else
                    {
                        dayIncidence = dryDaysAllPeriodDependingRankLamenessIncidenceByDay;
                    }
                    std::string line = Tools::toString(dayIncidence);
                    line = Tools::changeChar(line,'.', ',');
                    linesForTheSaveFile.push_back(line);
                }
                Tools::writeFile(linesForTheSaveFile, fileName, true);        
            }
        }        
    }
    
#endif // _LOG
                
    //void HealthManagement::programPotentialMastitisRelapse(const boost::gregorian::date &beginIIMDate, FunctionalEnumerations::Health::MastitisSeverity severity, FunctionalEnumerations::Health::BacteriumType bacterium, Mastitis::DairyQuarterInfectionStates::I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, Tools::Random &random)
    void HealthManagement::programPotentialMastitisRelapse(const boost::gregorian::date &beginIIMDate, FunctionalEnumerations::Health::MastitisSeverity severity, FunctionalEnumerations::Health::BacteriumType bacterium, DataStructures::DairyQuarter* pDairyQuarter, Tools::Random &random)
    {
        // program a relapse ?
        if (random.ran_bernoulli(FunctionalConstants::Health::MASTITIS_RELAPSE_PROBABILITY.find(bacterium)->second))
        {
            // yes, calculation of the date
            unsigned int daysForRelapse = random.ran_beta(FunctionalConstants::Health::ALPHA_FOR_RELAPSE, FunctionalConstants::Health::BETA_FOR_RELAPSE) * FunctionalConstants::Health::MAX_MASTITIS_DELAY_FOR_RELAPSE;
            boost::gregorian::date beginRelapseDate = beginIIMDate + boost::gregorian::date_duration(daysForRelapse);
            pDairyQuarter->programMastitisRelapse(beginRelapseDate, severity, bacterium);
        }
    }

    void HealthManagement::getketosisDayDurationValues(Tools::Random::ValuesBasedOnMultinomialDistribution &valuesToCalculate, Tools::Random &random)
    {
        // Subclinical duration curve and value distribution
        // -------------------------------------------------
        // initial conditions (See KetosisEffects.xlsx, tab "Duree")
        std::vector<unsigned int> multinomialValues;
        for (unsigned int dayNumber = 0; dayNumber <= FunctionalConstants::Health::MAX_SUBCLINICAL_KETOSIS_DURATION; dayNumber++)
        {
            unsigned int valueForTheDay = 0;
            if (dayNumber < FunctionalConstants::Health::MIN_SUBCLINICAL_KETOSIS_DURATION)
            {
                valueForTheDay = 0;
            }
            else if (dayNumber <= FunctionalConstants::Health::MEAN_SUBCLINICAL_KETOSIS_DURATION)
            {
                valueForTheDay = FunctionalConstants::Health::TECHNICAL_SUBCLINICAL_KETOSIS_DURATION_BASE * (dayNumber-FunctionalConstants::Health::MIN_SUBCLINICAL_KETOSIS_DURATION+1)/(FunctionalConstants::Health::MEAN_SUBCLINICAL_KETOSIS_DURATION-FunctionalConstants::Health::MIN_SUBCLINICAL_KETOSIS_DURATION+1);
            }
            else if (dayNumber == FunctionalConstants::Health::MEAN_SUBCLINICAL_KETOSIS_DURATION)
            {
                valueForTheDay = FunctionalConstants::Health::TECHNICAL_SUBCLINICAL_KETOSIS_DURATION_BASE;
            }
            else
            {
                valueForTheDay = FunctionalConstants::Health::TECHNICAL_SUBCLINICAL_KETOSIS_DURATION_BASE * (FunctionalConstants::Health::MAX_SUBCLINICAL_KETOSIS_DURATION+1-dayNumber)/(FunctionalConstants::Health::MAX_SUBCLINICAL_KETOSIS_DURATION+1-FunctionalConstants::Health::MEAN_SUBCLINICAL_KETOSIS_DURATION);
            }
            multinomialValues.push_back(valueForTheDay);
        }
            
        // Value list for random distribution
        valuesToCalculate = random.getValuesBasedOnMultinomialDistribution(multinomialValues);
    }

    void HealthManagement::manageNewKetosisOccurency(const boost::gregorian::date &beginDate, DataStructures::DairyMammal* pCow, Tools::Random &random, Ketosis::KetosisStates::KetosisState* &pNewState)
    {
        if (random.ran_bernoulli(pCow->getKetosisRisk(beginDate)))
        {
            // We have a new occurency, 
            assert (pNewState == nullptr);
            pNewState = new Ketosis::KetosisStates::G1SubclinicalKetosisState(beginDate, pCow, false, random);
        }
    }
    
    void HealthManagement::getKetosisEffectOnPostPartumDelay(bool standardCycle, float incidence, int &dayToAdd, std::vector<float> &durationProbabilities, bool ketosisDuringTheLactation)
    {
        // For calculation details, see document "KetosisEffects.xlsx", tab "Cyclicity" 
        float initialStandardInterruptedDurationProbability = durationProbabilities[1];
        if (!ketosisDuringTheLactation)
        {
            // First oestrus delay to decrease (negative increase) regarding basis
            dayToAdd = - FunctionalConstants::Health::FIRST_OESTRUS_DELAY_WHEN_KETOSIS * incidence;

            // Decrease interrupt risk due to no ketosis for standard cycle case
            if (standardCycle) // The planned cycle was standard
            {
                durationProbabilities[1] *= 1.0f - FunctionalConstants::Health::STANDARD_POSTPARTUM_CYCLE_KETOSIS_RESUMPTION_PROBABILITY * incidence;
            }
        }
        else
        {
            // First oestrus delay to increase regarding basis
            dayToAdd = FunctionalConstants::Health::FIRST_OESTRUS_DELAY_WHEN_KETOSIS * (1.0f - incidence);

            // Increase interrupt risk due to ketosis for standard cycle case
            if (standardCycle) // The planned cycle was standard
            {
                durationProbabilities[1] *= 1.0f + FunctionalConstants::Health::STANDARD_POSTPARTUM_CYCLE_KETOSIS_RESUMPTION_PROBABILITY * (1.0f - incidence);
            }
        }    
        durationProbabilities[0] += initialStandardInterruptedDurationProbability - durationProbabilities[1]; // Update normal standard probal regarding new standard interrupt risk
    }
    
    void HealthManagement::getPonderedInseminationSuccessProbability(unsigned int ketosisDurationInTheLactation, bool G1ketosisSuccessfulTreatmentDuringTheLactation, float incidence, float &inseminationSuccessProbability)
    {
        float basisFailProbability = 1.0f - inseminationSuccessProbability;
        
        float brutFailProbability = basisFailProbability * 
                                                            ((ketosisDurationInTheLactation < FunctionalConstants::Health::KETOSIS_DAYS_FOR_DECREASE_FERTILITY_FAILURE_RISK and G1ketosisSuccessfulTreatmentDuringTheLactation)
                                                            ?
                                                                    FunctionalConstants::Health::KETOSIS_FERTILITY_FAILURE_RISK_IF_G1_SUCCESSFUL_TREATMENT
                                                                    :
                                                                    FunctionalConstants::Health::KETOSIS_FERTILITY_FAILURE_RISK);
    
        if(ketosisDurationInTheLactation > 0)
        {
            inseminationSuccessProbability = 1.0f - (basisFailProbability + (brutFailProbability - basisFailProbability) * (1.0f - incidence) * TechnicalConstants::KETOSIS_FERTILITY_CALIBRATION_VALUE);          
        }
        else
        {
            inseminationSuccessProbability = 1.0f - (basisFailProbability - (brutFailProbability - basisFailProbability) * (incidence) * TechnicalConstants::KETOSIS_FERTILITY_CALIBRATION_VALUE);                    
        }
    }
    
    float HealthManagement::getLactationRankBasisIncidence(unsigned int lactationRank, std::map<unsigned int, float> &basisIncidence)
    {
        if (lactationRank > basisIncidence.size())
        {
            lactationRank = basisIncidence.size();
        }
        return basisIncidence.find(lactationRank)->second;
    }

    // To calculate the specific and modulated incidence for a cow
    float HealthManagement::getModulatedCowKetosisIncidenceOfTheDay(unsigned int lactationStage, unsigned int lactationRank, std::map<unsigned int, float> &cowBasisKetosisIncidence, float dairyFarmKetosisPreventionFactor, unsigned int lastCalvingIntervalInMonth, bool currentMonensinBolusEffect, bool hadClinicalMastitisSinceLactationBegin, bool hasLamenessCase)
    {         
        
        // Basis knowing breed and lactation rank
        float incidence = getLactationRankBasisIncidence(lactationRank, cowBasisKetosisIncidence);

        // Lactation stage
        assert (lactationStage <= s_ketosisRiskPeriodCurve.size());
        incidence *= s_ketosisRiskPeriodCurve[lactationStage-1];        
        
        // Modulations :
        
        // Genetic BHBlait value 
        // -> Already in the specific cow basis incidence calculation
        
        // Prevention factor
        incidence /= dairyFarmKetosisPreventionFactor;
        
        
        // Other diseases
        // Mastitis
        if (hadClinicalMastitisSinceLactationBegin)
        {
            incidence *= FunctionalConstants::Health::KETOSIS_RISK_PROBA_FACTOR_WHEN_CLINICAL_MASTITIS_DURING_LACTATION;
        }
        // Lameness
        if (hasLamenessCase)
        {
            incidence *= FunctionalConstants::Health::KETOSIS_RISK_PROBA_FACTOR_WHEN_LAMENESS;
        }
        
        // IVV effect
        if (lastCalvingIntervalInMonth > 0)
        {
            if (lastCalvingIntervalInMonth <= FunctionalConstants::Health::IVV_KETOSIS_INCIDENCE_DURATIONS[0])
            {
                incidence *= FunctionalConstants::Health::IVV_KETOSIS_INCIDENCE_PROBA_EFFECT[0];
            }
            else if (lastCalvingIntervalInMonth <= FunctionalConstants::Health::IVV_KETOSIS_INCIDENCE_DURATIONS[1])
            {
                incidence *= FunctionalConstants::Health::IVV_KETOSIS_INCIDENCE_PROBA_EFFECT[1];
            }
            else
            {
               incidence *= FunctionalConstants::Health::IVV_KETOSIS_INCIDENCE_PROBA_EFFECT[2];
            }
        }               
        if (currentMonensinBolusEffect)
        {
            incidence *= FunctionalConstants::Health::INCIDENCE_FACTOR_DUE_TO_MONENSIN_BOLUS_USE;
        }
        return incidence;
    }
    
    void HealthManagement::manageNewLamenessOccurency(const boost::gregorian::date &simDate, DataStructures::Foots* pFoots, FunctionalEnumerations::Health::LamenessInfectiousStateType infectiousType, Tools::Random &random, Lameness::LamenessStates::LamenessState* &pNewState)
    {
        if (random.ran_bernoulli(pFoots->getLamenessRisk(simDate, infectiousType)))
        {
            // We have a new occurency
            assert (pNewState == nullptr);
                         
            if (infectiousType == FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)
            {
                // Infectious
                pNewState = new Lameness::LamenessStates::G1InfectiousLamenessState(simDate, pFoots); 
            }
            else
            {
                // Non infectious
                pNewState = new Lameness::LamenessStates::G1NonInfectiousLamenessState(simDate, pFoots); 
            }
        }
    }
    
    float HealthManagement::getLactationLamenessCurveFactor(int lactationStage)
    {
        unsigned int curveIndex = lactationStage;
        if (curveIndex >= s_lamenessRiskLactationPeriodCurve.size()) curveIndex = s_lamenessRiskLactationPeriodCurve.size()-1;
        return s_lamenessRiskLactationPeriodCurve[curveIndex];
    }
        
    float HealthManagement::getModulatedCowLamenessIncidenceOfTheDay(float baseIncidence, bool hasCurrentlyG1ToG3MastitisCase, bool hasCurrentlyG1ToG2KetosisCase, FunctionalEnumerations::Health::LamenessInfectiousStateType infectiousType, FunctionalEnumerations::Population::Location location, unsigned int dayNumberInLocation, float batchPrevalence)
    {         
        // Modulations :
        
        // Genetic RBi or RBni value 
        // -> Already in the specific cow basis incidence calculation
        
        // Prevention factor
        // -> already set in comonInit of the dairyMammal
        
        float incidence = baseIncidence;
        
        // Other diseases
        // Mastitis
        if (hasCurrentlyG1ToG3MastitisCase)
        {
            incidence *= FunctionalConstants::Health::LAMENESS_RISK_PROBA_FACTOR_WHEN_G1_TO_G3_MASTITIS_CASE;
        }
        // Ketosis
        if (hasCurrentlyG1ToG2KetosisCase)
        {
            incidence *= FunctionalConstants::Health::LAMENESS_RISK_PROBA_FACTOR_WHEN_G1_TO_G2_KETOSIS_CASE;
        }
        
        if (infectiousType == FunctionalEnumerations::Health::LamenessInfectiousStateType::infectiousLamenessType)
        {
            // Batch prevalence effect
            if (batchPrevalence > FunctionalConstants::Health::BATCH_INFECTIOUS_LAMENESS_PREVALENCE_THRESHOLD)
            {
                incidence *= FunctionalConstants::Health::BATCH_INFECTIOUS_LAMENESS_PREVALENCE_FACTOR;
            }
            else if (batchPrevalence > FunctionalConstants::Health::HALF_BATCH_INFECTIOUS_LAMENESS_PREVALENCE_THRESHOLD)
            {
                float val = 1.0f + (batchPrevalence - FunctionalConstants::Health::HALF_BATCH_INFECTIOUS_LAMENESS_PREVALENCE_THRESHOLD)/(FunctionalConstants::Health::BATCH_INFECTIOUS_LAMENESS_PREVALENCE_THRESHOLD - FunctionalConstants::Health::HALF_BATCH_INFECTIOUS_LAMENESS_PREVALENCE_THRESHOLD);
                incidence *= val; 
            }
            
            // Stabulation effect
            if (location != FunctionalEnumerations::Population::Location::fullPasture)
            {
                unsigned int dayToConsider = dayNumberInLocation;
                if (dayNumberInLocation >= s_lamenessStabulationRiskCurve.size())
                {
                    dayToConsider = s_lamenessStabulationRiskCurve.size()-1;
                }
                float pastureFactor = s_lamenessStabulationRiskCurve[dayToConsider];
                
                // Effect divided by 2 if half stabulation
                if (location == FunctionalEnumerations::Population::Location::halfStabulationPasture)
                {
                    pastureFactor = 1.0f + ((pastureFactor - 1.0f) / 2.0f);
                }
                incidence *= pastureFactor;
            } 
        }
        return incidence;
    }

    // Static initializations
    std::map<FunctionalEnumerations::Health::BacteriumType, std::map<FunctionalEnumerations::Health::MastitisRiskPeriodType, std::vector<double>>> HealthManagement::s_mastitisRiskPeriodMatrix;    
    std::vector<double> HealthManagement::s_ketosisRiskPeriodCurve;    
    std::vector<double> HealthManagement::s_lamenessRiskLactationPeriodCurve; 
    std::vector<float> HealthManagement::s_lamenessStabulationRiskCurve;    
}
    
    

