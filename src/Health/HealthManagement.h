#ifndef HealthManagement_h
#define HealthManagement_h

// standard

// boost
#include <boost/date_time/gregorian/gregorian.hpp>

// project
#include "../ExchangeInfoStructures/FunctionalConstants.h"
//#include "./Diseases/Mastitis/DairyQuarterMastitisInfectionStates/I_DairyHerdMastitisProneDairyQuarter.h"
#include "../DataStructures/DairyQuarter.h"
//#include "./Diseases/Ketosis/KetosisStates/I_KetosisProneDairyCow.h"
//#include "./Diseases/Lameness/LamenessStates/I_LamenessProneFoots.h"
#include "../DataStructures/Foots.h"
#include "../Tools/Random.h"

namespace Tools
{
    class Random;
}

namespace DataStructures
{
    class DairyMammal;
}

namespace Health
{
    namespace Mastitis
    {
        namespace RiskPeriodStates
        {
            class DairyCowMastitisRiskPeriodState;
        }
    }
    namespace Ketosis
    {
        namespace KetosisStates
        {
            class KetosisState;
        }
    }
    namespace Lameness
    {
        namespace LamenessStates
        {
            class LamenessState;
        }
    }

    class HealthManagement
    {
        static std::map<FunctionalEnumerations::Health::BacteriumType, std::map<FunctionalEnumerations::Health::MastitisRiskPeriodType, std::vector<double>>> s_mastitisRiskPeriodMatrix;    
        static std::vector<double> s_ketosisRiskPeriodCurve;    
        static std::vector<double> s_lamenessRiskLactationPeriodCurve;    
        static std::vector<float> s_lamenessStabulationRiskCurve;    

    protected:
        virtual void concrete() = 0; // To ensure that it will not be instantiated

    public:
        // Mastitis
        static void initMastitisRiskPeriodMatrix(std::string &technicalDataFolder);
        static std::vector<double> &getMastitisRiskPeriodProbabilities(FunctionalEnumerations::Health::BacteriumType bacteriumType, FunctionalEnumerations::Health::MastitisRiskPeriodType periodType);
        static double getMastitisOccurenceProbability(FunctionalEnumerations::Health::BacteriumType bacterium, const boost::gregorian::date &date, Mastitis::RiskPeriodStates::DairyCowMastitisRiskPeriodState* pMastitisPeriodState, float saisonalityFactor, float bacteriumIncidencePart, unsigned int weekBatchPrevalence, bool lactationIsOngoing, float milkProductOfTheDay, float preexistingSCC, float MACL_CorrectedPerformance, FunctionalEnumerations::Health::MastitisLactationRankModulation lactationRankForModulation, float milkPotentialFactor, bool udderIsAlreadyInfectedByThisBacterium);
        static FunctionalEnumerations::Health::MastitisSeverity getRandomMastitisSeverity(FunctionalEnumerations::Health::BacteriumType bacterium, FunctionalEnumerations::Health::MastitisSeverityDegree degree, Tools::Random &random);
#ifdef _LOG
        static void saveMastitisRiskPeriodMatrix(const std::string &theResultPath);
        static void saveLamenessRiskPeriodMatrix(const std::string &theResultPath);
#endif // _LOG 
        
        
        //static void programPotentialMastitisRelapse(const boost::gregorian::date &beginIIMDate, FunctionalEnumerations::Health::MastitisSeverity severity, FunctionalEnumerations::Health::BacteriumType bacterium, Mastitis::DairyQuarterInfectionStates::I_DairyHerdMastitisProneDairyQuarter* pDairyQuarter, Tools::Random &random);
        static void programPotentialMastitisRelapse(const boost::gregorian::date &beginIIMDate, FunctionalEnumerations::Health::MastitisSeverity severity, FunctionalEnumerations::Health::BacteriumType bacterium, DataStructures::DairyQuarter* pDairyQuarter, Tools::Random &random);

        static float getLactationRankBasisIncidence(unsigned int limitedLactationRank, std::map<unsigned int, float> &basisIncidence);
        // Ketosis
        static void initKetosisRiskPeriodCurve(std::string &technicalDataFolder);
        static void getketosisDayDurationValues(Tools::Random::ValuesBasedOnMultinomialDistribution &valuesToCalculate, Tools::Random &random);
        static void manageNewKetosisOccurency(const boost::gregorian::date &beginDate, DataStructures::DairyMammal* pCow, Tools::Random &random, Ketosis::KetosisStates::KetosisState* &pNewState);
        static void getKetosisEffectOnPostPartumDelay(bool standardCycle, float incidence, int &dayToAdd, std::vector<float> &durationProbabilities, bool ketosisDuringTheLactation);
        static void getPonderedInseminationSuccessProbability(unsigned int ketosisDurationInTheLactation, bool G1ketosisSuccessfulTreatmentDuringTheLactation, float incidence, float &inseminationSuccessProbability);
        static float getModulatedCowKetosisIncidenceOfTheDay(unsigned int lactationStage, unsigned int lactationRank, std::map<unsigned int, float> &cowBasisKetosisIncidence, float dairyFarmKetosisPreventionFactor, unsigned int lastCalvingIntervalInMonth, bool currentMonensinBolusEffect, bool hadClinicalMastitisSinceLactationBegin, bool hasLamenessCase);

        // Lameness
        static void initLamenessRiskLactationPeriodCurve(std::string &technicalDataFolder);
        static void initLamenessStabulationRiskCurve(std::string &technicalDataFolder); 
        static void manageNewLamenessOccurency(const boost::gregorian::date &simDate, DataStructures::Foots* pFoots, FunctionalEnumerations::Health::LamenessInfectiousStateType infectiousType, Tools::Random &random, Lameness::LamenessStates::LamenessState* &pNewState);
        static float getLactationLamenessCurveFactor(int lactationStage);
        static float getModulatedCowLamenessIncidenceOfTheDay(float baseIncidence, bool hasCurrentlyG1ToG3MastitisCase, bool hasCurrentlyG1ToG2KetosisCase, FunctionalEnumerations::Health::LamenessInfectiousStateType infectiousType, FunctionalEnumerations::Population::Location location, unsigned int dayNumberInLocation, float batchPrevalence);

    };
}
#endif // HealthManagement_h
