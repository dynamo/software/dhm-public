#include "DairyHerdLactationState.h"

// boost

// project
//#include "I_DairyHerdLactationAnimal.h"
#include "ExchangeInfoStructures/FunctionalConstants.h"
#include "../../DataStructures/DairyCow.h"

BOOST_CLASS_EXPORT(Lactation::DairyHerdLactationStates::DairyHerdLactationState) // Recommended (mandatory if virtual serialization)

namespace Lactation
{
    namespace DairyHerdLactationStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_NVP(_beginDate); \
        ar & BOOST_SERIALIZATION_NVP(_predictedEndDate); \
        ar & BOOST_SERIALIZATION_NVP(_pAppliesToDairyMammal); \

        IMPLEMENT_SERIALIZE_STD_METHODS(DairyHerdLactationState);
        
        DairyHerdLactationState::DairyHerdLactationState(const boost::gregorian::date &beginDate
#ifdef _LOG
                                                                                                        , const std::string &strState
#endif // _LOG 
                                                                                                        , DataStructures::DairyMammal* pAnim)
        {
            _beginDate = beginDate;
            _pAppliesToDairyMammal = (DataStructures::DairyCow*)pAnim;
            
            // General concept
            _predictedEndDate = boost::gregorian::date(boost::gregorian::pos_infin);
            
            pAnim->setLactationStage(-1);
                    
#ifdef _LOG
            pAnim->addDairyHerdLactationAnimalLog(beginDate, strState);
#endif // _LOG 

        }
        
        bool DairyHerdLactationState::isOutOfTime(const boost::gregorian::date &baseDate)
        {
            return baseDate >= _predictedEndDate;
        }
        
        DairyHerdLactationState::~DairyHerdLactationState()
        {
        }
        
        boost::gregorian::date &DairyHerdLactationState::getBeginDate()
        {
            return _beginDate;
        }

        bool DairyHerdLactationState::isMilking()
        {
            return false;
        }
        
        float DairyHerdLactationState::getFullLactationPonderedMilkQuantity()
        {
            throw std::runtime_error("DairyHerdLactationState::getFullLactationPonderedMilkQuantity : unappropried lactation state");
        }
    } /* End of namespace DataStructures::States */        
} /* End of namespace DataStructures */
