#ifndef Production_DairyHerdLactationStates_DairyHerdLactationState_h
#define Production_DairyHerdLactationStates_DairyHerdLactationState_h

// boost
#include <boost/date_time/gregorian/gregorian.hpp>

// project
#include "../../Tools/Serialization.h"
#include "../../Tools/Random.h"

namespace DataStructures
{
    class DairyCow;
    class DairyMammal;
}

namespace Lactation
{
    namespace DairyHerdLactationStates
    {
        //class I_DairyHerdLactationAnimal;
        
        class DairyHerdLactationState
        {
        DECLARE_SERIALIZE_STD_METHODS;

        protected:
            boost::gregorian::date _beginDate;
            boost::gregorian::date _predictedEndDate;
            DataStructures::DairyCow* _pAppliesToDairyMammal;

            DairyHerdLactationState(const boost::gregorian::date &beginDate
#ifdef _LOG
                                                                                        , const std::string &strState
#endif // _LOG 
                                                                                        , DataStructures::DairyMammal* pAnim);
            DairyHerdLactationState(){};
            virtual void concrete() = 0; // To ensure that it will not be instantiated
            bool isOutOfTime(const boost::gregorian::date &baseDate);

        public:
            virtual ~DairyHerdLactationState();
            boost::gregorian::date &getBeginDate();
            
            virtual void progress(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &simDate, DairyHerdLactationState* &pNewState)=0;
            virtual void planDryOffDate(const boost::gregorian::date &dryOffDate){};
            virtual void cancelPlanifiedDryOffDate(){};
            virtual bool isMilking();
            virtual float getFullLactationPonderedMilkQuantity();
        };
    }
} /* End of namespace Production */
#endif // Production_DairyHerdLactationStates_DairyHerdLactationState_h
