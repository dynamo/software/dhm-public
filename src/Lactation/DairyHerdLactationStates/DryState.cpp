#include "DryState.h"

// project
//#include "I_DairyHerdLactationAnimal.h"
#include "InvoluatedDryState.h"
#include "../../ExchangeInfoStructures/FunctionalConstants.h"
#include "../../DataStructures/DairyMammal.h"

BOOST_CLASS_EXPORT(Lactation::DairyHerdLactationStates::DryState) // Recommended (mandatory if virtual serialization)

namespace Lactation
{
    namespace DairyHerdLactationStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyHerdLactationState);

        IMPLEMENT_SERIALIZE_STD_METHODS(DryState);

        DryState::DryState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate) : DairyHerdLactationState(beginDate
#ifdef _LOG
                                                                                                                                , "Dried "
#endif // _LOG 
                                                                                                                                , pAnim)
        {
            // The next state is Involuate state, so we have to determine a duration value
            boost::gregorian::date_duration dd(FunctionalConstants::Health::MAX_DRY_DURATION_FOR_POST_DRY_MASTITIS_RISK);
            _predictedEndDate = _beginDate + dd;   
            pAnim->dryOff(beginDate);
        }

        void DryState::progress(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &simDate, DairyHerdLactationState* &pNewState)
        {
            if (isOutOfTime(simDate))
            {
                // Farrowing stage
                // ---------------
                pNewState = new InvoluatedDryState(pAnim, simDate);
            }
        }
        
        DryState::~DryState()
        {
            
        }
    }
}