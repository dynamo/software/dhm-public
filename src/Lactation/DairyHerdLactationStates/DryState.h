#ifndef Production_DairyHerdProductionStates_DryState_h
#define Production_DairyHerdProductionStates_DryState_h

// project
#include "DairyHerdLactationState.h"

namespace Lactation
{
    namespace DairyHerdLactationStates
    {
    class DryState : public DairyHerdLactationState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        protected:
            void concrete(){}; // To allow instanciation
        public:
            DryState(){};
            DryState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate);
            virtual ~DryState();
            virtual void progress(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &simDate, DairyHerdLactationState* &pNewState);
        };
    }
}
#endif // Production_DairyHerdProductionStates_DryState_h
