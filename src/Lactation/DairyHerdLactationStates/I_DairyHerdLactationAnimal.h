//#ifndef Production_DairyHerdLactationStates_I_DairyHerdLactationAnimal_h
//#define Production_DairyHerdLactationStates_I_DairyHerdLactationAnimal_h
//
//// boost
//
//// project
//
//namespace ExchangeInfoStructures
//{
//    class MilkProductCharacteristic;
//}
//namespace Lactation
//{
//    namespace DairyHerdLactationStates
//    {
//        class DairyHerdLactationState;
//   
//        class I_DairyHerdLactationAnimal
//        {
//        public:
//            
//            // virtual destructor for interface 
//            virtual ~I_DairyHerdLactationAnimal() { }
//            virtual unsigned int getUniqueIdForLactationState() = 0; // tmp
//            
//#ifdef _LOG
//           virtual void addDairyHerdLactationAnimalLog(const boost::gregorian::date &date, const std::string &log) = 0;
//#endif // _LOG 
//            virtual void setDryingOffMastitisRiskPeriod(const boost::gregorian::date &date) = 0;
//            virtual void setDryingInvolutedMastitisRiskPeriod(const boost::gregorian::date &date) = 0;
//            virtual void setLactationMastitisRiskPeriod(const boost::gregorian::date &date) = 0;
//            virtual float produceMilkForTheDay(unsigned int day, const boost::gregorian::date &date, ExchangeInfoStructures::MilkProductCharacteristic &currentDriedPeriodEffectToMilk, DairyHerdLactationState* &pNewState) = 0;
//            virtual void dryOff(const boost::gregorian::date &date) = 0;
//            virtual void setLactationStage(int lactationStage) = 0;
//            virtual void considerHerdNavigatorTest(const boost::gregorian::date &simDate) = 0;
//            virtual void goInLactatingCowsBatch(const boost::gregorian::date &simDate) = 0;
//            virtual unsigned int getBreedingDelayAfterCalvingDecision() = 0;
//            virtual void setLastBeginLactationPonderedMilkQuantity(float ponderedQuantity, const boost::gregorian::date &date) = 0;
//            virtual bool useHerdNavigator() = 0;
//            virtual void setNotYetControledForThisLactation() = 0;
//            virtual void enterInKetosisRiskPeriod(const boost::gregorian::date &date) = 0;
//            virtual void becomeLamenessSensitive(const boost::gregorian::date &date) = 0;
//            virtual bool needToTrimForLamenessPreventionWhenLactation() = 0;
//            virtual void toTrimForLamenessPrevention(const boost::gregorian::date &date) = 0;
//        };
//    } /* End of namespace Reproduction::DairyHerdReproductionStates */
//} /* End of namespace Reproduction */
//#endif // Production_DairyHerdLactationStates_I_DairyHerdLactationAnimal_h
