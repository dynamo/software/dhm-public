#include "InvoluatedDryState.h"

// project
//#include "I_DairyHerdLactationAnimal.h"
#include "../../DataStructures/DairyMammal.h"

BOOST_CLASS_EXPORT(Lactation::DairyHerdLactationStates::InvoluatedDryState) // Recommended (mandatory if virtual serialization)

namespace Lactation
{
    namespace DairyHerdLactationStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyHerdLactationState);

        IMPLEMENT_SERIALIZE_STD_METHODS(InvoluatedDryState);

        InvoluatedDryState::InvoluatedDryState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate) : DairyHerdLactationState(beginDate
#ifdef _LOG
                                                                                                                                                    , "Involuate dried "
#endif // _LOG 
                                                                                                                                                    , pAnim)
        {
            // No predicted end date to set, duration is infinite until a new calving
            pAnim->setDryingInvolutedMastitisRiskPeriod(beginDate);
        }

        void InvoluatedDryState::progress(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &simDate, DairyHerdLactationState* &pNewState)
        {
            // Nothing to do except let time pass
        }
        
        InvoluatedDryState::~InvoluatedDryState()
        {
            
        }
    }
}