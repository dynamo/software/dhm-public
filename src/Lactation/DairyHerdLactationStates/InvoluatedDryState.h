#ifndef Production_DairyHerdProductionStates_InvoluatedDryState_h
#define Production_DairyHerdProductionStates_InvoluatedDryState_h

// project
#include "DairyHerdLactationState.h"

namespace Lactation
{
    namespace DairyHerdLactationStates
    {
        class InvoluatedDryState : public DairyHerdLactationState
        {
        DECLARE_SERIALIZE_STD_METHODS;

        protected:
            void concrete(){}; // To allow instanciation

        public:
            InvoluatedDryState(){};
            InvoluatedDryState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate);
            virtual ~InvoluatedDryState();
            virtual void progress(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &simDate, DairyHerdLactationState* &pNewState);
        };
    }
}
#endif // Production_DairyHerdProductionStates_InvoluatedDryState_h
