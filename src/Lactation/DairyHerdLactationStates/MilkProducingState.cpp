#include "MilkProducingState.h"

// project
#include "../../DataStructures/DairyMammal.h"
#include "../../ExchangeInfoStructures/TechnicalConstants.h"
#include "../../ExchangeInfoStructures/FunctionalConstants.h"
#include "../../Tools/Tools.h"
#include "../../Lactation/DairyHerdLactationStates/DryState.h"

BOOST_CLASS_EXPORT(Lactation::DairyHerdLactationStates::MilkProducingState) // Recommended (mandatory if virtual serialization)

namespace Lactation
{
    namespace DairyHerdLactationStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyHerdLactationState); \
        ar & BOOST_SERIALIZATION_NVP(_plannedDryOffDate); \
        ar & BOOST_SERIALIZATION_NVP(_driedPeriodEffectToMilk); \
        ar & BOOST_SERIALIZATION_NVP(_waitingDaysForHerdNavigatorTest); \
        ar & BOOST_SERIALIZATION_NVP(_plannedTrimForLamenessPreventionDate); \

        IMPLEMENT_SERIALIZE_STD_METHODS(MilkProducingState);

        MilkProducingState::MilkProducingState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate, ExchangeInfoStructures::MilkProductCharacteristic driedPeriodEffectToMilk) : DairyHerdLactationState(beginDate
#ifdef _LOG
                                                                                                                                                                                                        , "Milk producing "
#endif // _LOG 
                                                                                                                                                                                                        , pAnim)
        {
            _driedPeriodEffectToMilk = driedPeriodEffectToMilk;
            pAnim->goInLactatingCowsBatch(beginDate);
            pAnim->setLactationStage(0);
            pAnim->setNotYetControledForThisLactation();

            // Ketosis sensitive state
            pAnim->enterInKetosisRiskPeriod(beginDate);
            
            // Lameness sensitive state
            pAnim->becomeLamenessSensitive(beginDate);
            
            // Preventive trimming to plan ?
            if (pAnim->needToTrimForLamenessPreventionWhenLactation())
            {
                _plannedTrimForLamenessPreventionDate = beginDate + boost::gregorian::date_duration(FunctionalConstants::Health::DELAY_AFTER_CALVING_FOR_PREVENTIVE_TRIMMING);
            }
        }

        void MilkProducingState::progress(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &simDate, DairyHerdLactationState* &pNewState)
        {
            // Is it the begining of a peri partum mastitis risk period ?
            boost::gregorian::date_duration duration(simDate - _beginDate);
            int days = duration.days();
            pAnim->setLactationStage(days);
            
            if (days == FunctionalConstants::Health::MAX_CALVING_DELAY_FOR_PERI_PARTUM_MASTITIS_RISK + 1)
            {
                // The mastistis risk change today, it's now a begin lactation risk
                pAnim->setLactationMastitisRiskPeriod(simDate);
            }
            
            // Herd navigator test
            if (pAnim->useHerdNavigator())
            {     
                if (--_waitingDaysForHerdNavigatorTest == 0)
                {
                    // It's time to do herd navigator test
                    pAnim->considerHerdNavigatorTest(simDate);
                    _waitingDaysForHerdNavigatorTest = FunctionalConstants::Health::HERD_NAVIGATOR_TEST_DAY_CYCLE;
                }
            }
            
            if (simDate == _plannedTrimForLamenessPreventionDate)
            {
                // Planned preventive trimming
                pAnim->toTrimForLamenessPrevention(simDate);
            }
            
            if (simDate == _plannedDryOffDate)
            {
                // Planned Dry off
                pNewState = new Lactation::DairyHerdLactationStates::DryState(pAnim, simDate);
            }
            else
            {
                // Calculate the milk quantity
                boost::gregorian::date_duration duration(simDate - _beginDate);
                unsigned int day = duration.days();
                float todayPonderedQuantity = pAnim->produceMilkForTheDay(day, simDate, _driedPeriodEffectToMilk, pNewState);
                _ponderedMilkQuantityForFullLactation += todayPonderedQuantity;
                if (days < (int)pAnim->getBreedingDelayAfterCalvingDecision())
                {
                    _ponderedMilkQuantityBetweenBeginAndBreedDate += todayPonderedQuantity;
                }
                else if (days == (int)pAnim->getBreedingDelayAfterCalvingDecision())
                {
                    pAnim->setLastBeginLactationPonderedMilkQuantity(_ponderedMilkQuantityBetweenBeginAndBreedDate, simDate);
                }
            }
        }

        MilkProducingState::~MilkProducingState()
        {
        }
                 
        bool MilkProducingState::isMilking()
        {
            return true;
        }
               
        float MilkProducingState::getFullLactationPonderedMilkQuantity()
        {
            return _ponderedMilkQuantityForFullLactation;
        }

        void MilkProducingState::planDryOffDate(const boost::gregorian::date &dryOffDate)
        {
            _plannedDryOffDate = boost::gregorian::date(dryOffDate);
        }

        void MilkProducingState::cancelPlanifiedDryOffDate()
        {
            _plannedDryOffDate = boost::gregorian::date();
        }
    }
}