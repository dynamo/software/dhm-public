#ifndef Production_DairyHerdLactationStates_MilkProducingState_h
#define Production_DairyHerdLactationStates_MilkProducingState_h

// project
#include "DairyHerdLactationState.h"
#include "../../ExchangeInfoStructures/MilkProductCharacteristic.h"
#include "../../ExchangeInfoStructures/FunctionalConstants.h"

namespace Lactation
{
    namespace DairyHerdLactationStates
    {
    class MilkProducingState : public DairyHerdLactationState
        {
            boost::gregorian::date _plannedDryOffDate;
            float _ponderedMilkQuantityBetweenBeginAndBreedDate = 0.0f;
            float _ponderedMilkQuantityForFullLactation = 0.0f;
            ExchangeInfoStructures::MilkProductCharacteristic _driedPeriodEffectToMilk;
            unsigned int _waitingDaysForHerdNavigatorTest = FunctionalConstants::Health::HERD_NAVIGATOR_TEST_DAY_CYCLE;
            boost::gregorian::date _plannedTrimForLamenessPreventionDate;

        DECLARE_SERIALIZE_STD_METHODS;
        
        protected:
            void concrete(){}; // To allow instanciation
            
        public:
            MilkProducingState(){};
            MilkProducingState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate, ExchangeInfoStructures::MilkProductCharacteristic driedPeriodEffectToMilk);
            virtual ~MilkProducingState();
            void progress(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &simDate, DairyHerdLactationState* &pNewState);
            void planDryOffDate(const boost::gregorian::date &dryOffDate) override;
            void cancelPlanifiedDryOffDate() override;
            bool isMilking() override;
            float getFullLactationPonderedMilkQuantity() override;
       };
    }
}
#endif // Production_DairyHerdProductionStates_MilkProducingState_h
