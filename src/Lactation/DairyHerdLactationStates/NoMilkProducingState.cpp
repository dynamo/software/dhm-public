#include "NoMilkProducingState.h"

// project
//#include "I_DairyHerdLactationAnimal.h"

BOOST_CLASS_EXPORT(Lactation::DairyHerdLactationStates::NoMilkProducingState) // Recommended (mandatory if virtual serialization)

namespace Lactation
{
    namespace DairyHerdLactationStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyHerdLactationState);

        IMPLEMENT_SERIALIZE_STD_METHODS(NoMilkProducingState);

        NoMilkProducingState::NoMilkProducingState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate) : DairyHerdLactationState(beginDate
#ifdef _LOG
                                                                                                                                                         , "No Milk producing "
#endif // _LOG 
                                                                                                                                                         , pAnim)
        {
            // No predicted end date to set, duration is infinite until a new calving
        }

        void NoMilkProducingState::progress(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &simDate, DairyHerdLactationState* &pNewState)
        {
            // Nothing to do except let time pass
        }
        
        NoMilkProducingState::~NoMilkProducingState()
        {
            
        }
    }
}
