#ifndef Production_DairyHerdProductionStates_NoMilkProducingState_h
#define Production_DairyHerdProductionStates_NoMilkProducingState_h

// project
#include "DairyHerdLactationState.h"

namespace Lactation
{
    namespace DairyHerdLactationStates
    {
    class NoMilkProducingState : public DairyHerdLactationState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        protected:
            void concrete(){}; // To allow instanciation
        public:
            NoMilkProducingState(){};
            NoMilkProducingState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate);
            virtual ~NoMilkProducingState();
            void progress(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &simDate, DairyHerdLactationState* &pNewState);
        };
    }
}
#endif // Production_DairyHerdProductionStates_NoMilkProducingState_h
