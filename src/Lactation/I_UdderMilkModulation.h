/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   I_UdderMilkModulation.h
 * Author: pgontier
 *
 * Created on 5 juin 2019, 14:20
 */

#ifndef I_UDDERMILKMODULATION_H
#define I_UDDERMILKMODULATION_H

#include "../ExchangeInfoStructures/MilkProductCharacteristic.h"
#include "../Tools/Random.h"

namespace Lactation
{
    class I_UdderMilkModulation
    {
    public:

        // virtual destructor for interface 
        virtual ~I_UdderMilkModulation() { }

        virtual void getRealDairyQuartersProduct(ExchangeInfoStructures::MilkProductCharacteristic &baseMilkProduction, Tools::Random &random, const boost::gregorian::date &theDate, unsigned int lactationRank, int lactationStage, std::vector<ExchangeInfoStructures::MilkProductCharacteristic> &vDairyQuarterMp, bool &mustWaitToDeliver) = 0;
        virtual void getKetosisEffectOnMilkProduction(const boost::gregorian::date &date, ExchangeInfoStructures::MilkProductCharacteristic &mc) = 0;
    };
} /* End of namespace Lactation */


#endif /* I_UDDERMILKMODULATION_H */

