#include "LactationManagement.h"

// standard
#include <iostream> // cout
#include <fstream>

// Project
#include "../Tools/Tools.h"
#include "../ExchangeInfoStructures/MilkProductCharacteristic.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"
#include "../ExchangeInfoStructures/TechnicalConstants.h"

namespace Lactation
{
    // Context modulation
    void LactationManagement::getMilkModulatedProductionOfTheDay (  ExchangeInfoStructures::MilkProductCharacteristic &updatedUdderMp,
                                                                    bool colostrum,
                                                                    ExchangeInfoStructures::MilkProductCharacteristic &toDeliver,
                                                                    ExchangeInfoStructures::MilkProductCharacteristic &toDiscard,
                                                                    bool oestrus,
                                                                    unsigned int day,
                                                                    ExchangeInfoStructures::MilkProductCharacteristic &milkSunnyAdjustmentOfTheDay, 
                                                                    FunctionalEnumerations::Lactation::MilkingFrequency &mf,
                                                                    ExchangeInfoStructures::MilkProductCharacteristic &currentDriedPeriodEffectToMilk,
                                                                    bool pregnant,
                                                                    Lactation::I_UdderMilkModulation* pDairyUdder,
                                                                    DataStructures::Foots* pFoots,
                                                                    const boost::gregorian::date &date,
                                                                    unsigned int lactationRank, 
                                                                    int lactationStage, 
                                                                    float &ponderedTodayMilkQuantity,
                                                                    ExchangeInfoStructures::MilkProductCharacteristic &milkProductionObjectiveFactor,
                                                                    Tools::Random &random
                                                                 )
    {
        
        // Objective production level
        updatedUdderMp *= milkProductionObjectiveFactor;
        
        // parity
        unsigned int correctedParity1 = lactationRank;
        if (correctedParity1 > FunctionalConstants::Lactation::PARITY_FOR_TP_DEGRADATION) correctedParity1 = FunctionalConstants::Lactation::PARITY_FOR_TP_DEGRADATION;
        updatedUdderMp.TP -= ((float)FunctionalConstants::Lactation::TP_LOSS_WITH_PARITY) * ((float)correctedParity1) /((float)FunctionalConstants::Lactation::PARITY_FOR_TP_DEGRADATION); // effect on TP
        
        unsigned int correctedParity2 = lactationRank;
        if (correctedParity2 > FunctionalConstants::Lactation::MILK_PRODUCT_FACTOR_DEPENDING_LACTATION_RANK.size()) correctedParity2 = FunctionalConstants::Lactation::MILK_PRODUCT_FACTOR_DEPENDING_LACTATION_RANK.size();
        updatedUdderMp.quantity *= FunctionalConstants::Lactation::MILK_PRODUCT_FACTOR_DEPENDING_LACTATION_RANK[correctedParity2-1];// effect on quantity
        
        // Tarissement
        updatedUdderMp.quantity *= currentDriedPeriodEffectToMilk.quantity;
        updatedUdderMp.TB += currentDriedPeriodEffectToMilk.TB;
        updatedUdderMp.TP += currentDriedPeriodEffectToMilk.TP;
        
        // Pregnancy
        if (pregnant)
        {
            if (day >= FunctionalConstants::Lactation::LACTATION_STAGE_3_FOR_PREGNANCY_EFFECT)
            {
                updatedUdderMp.quantity -= FunctionalConstants::Lactation::LACTATION_VALUE_3_FOR_PREGNANCY_EFFECT;
            }
            else if (day >= FunctionalConstants::Lactation::LACTATION_STAGE_2_FOR_PREGNANCY_EFFECT)
            {
                updatedUdderMp.quantity -= FunctionalConstants::Lactation::LACTATION_VALUE_2_FOR_PREGNANCY_EFFECT;
            }
            else if (day >= FunctionalConstants::Lactation::LACTATION_STAGE_1_FOR_PREGNANCY_EFFECT)
            {
                updatedUdderMp.quantity -= FunctionalConstants::Lactation::LACTATION_VALUE_1_FOR_PREGNANCY_EFFECT;
            }
            // else no effect
        }
        
        // oestrus
        if (oestrus)
        {
            updatedUdderMp.quantity *= FunctionalConstants::Lactation::OESTRUS_MILK_PRODUCTION_FACTOR;
        }
        
        // Sunny adjustments
        updatedUdderMp.quantity += milkSunnyAdjustmentOfTheDay.quantity;
        updatedUdderMp.TB += milkSunnyAdjustmentOfTheDay.TB;
        updatedUdderMp.TP += milkSunnyAdjustmentOfTheDay.TP;
        updatedUdderMp.SCC += milkSunnyAdjustmentOfTheDay.SCC;
        
        // Frequency
        if (mf == FunctionalEnumerations::Lactation::MilkingFrequency::onceADay)
        {
            updatedUdderMp.quantity *= FunctionalConstants::Lactation::MILKING_ONCE_A_DAY_QUANTITY_FACTOR;
            updatedUdderMp.TB += FunctionalConstants::Lactation::MILKING_ONCE_A_DAY_TB;
            updatedUdderMp.TP += FunctionalConstants::Lactation::MILKING_ONCE_A_DAY_TP;
            updatedUdderMp.SCC *= FunctionalConstants::Lactation::MILKING_ONCE_A_DAY_SCC_FACTOR;
        }
        else if (mf == FunctionalEnumerations::Lactation::MilkingFrequency::overTwiceADay)
        {
            updatedUdderMp.quantity *= FunctionalConstants::Lactation::MILKING_OVER_TWICE_A_DAY_QUANTITY_FACTOR;
        }
        
        // Ketosis
        pDairyUdder->getKetosisEffectOnMilkProduction(date, updatedUdderMp);
        
        // Lameness
        pFoots->getLamenessEffectOnMilkProduction(date, updatedUdderMp);
        
        // Alea
        updatedUdderMp.quantity += (random.rng_uniform() * FunctionalConstants::Lactation::RANDOM_DAY_MILK_QUANTITY_ADJUSTMENT * 2) - FunctionalConstants::Lactation::RANDOM_DAY_MILK_QUANTITY_ADJUSTMENT/2;
        updatedUdderMp.TB += random.ran_gaussian(0.0f, FunctionalConstants::Lactation::RANDOM_DAY_MILK_TB_SD_ADJUSTMENT);
        updatedUdderMp.TP += random.ran_gaussian(0.0f, FunctionalConstants::Lactation::RANDOM_DAY_MILK_TP_SD_ADJUSTMENT);
        updatedUdderMp.SCC += random.ran_gaussian(0.0f, FunctionalConstants::Lactation::RANDOM_DAY_MILK_SCC_SD_ADJUSTMENT);
                
        // Real quarter milk production, based on potential Mastitis effet
        std::vector<ExchangeInfoStructures::MilkProductCharacteristic> vDairyQuarterMp;
        bool waitForDeliver;
        pDairyUdder->getRealDairyQuartersProduct(updatedUdderMp, random, date, lactationRank, lactationStage, vDairyQuarterMp, waitForDeliver);
        
        // To deliver or to discard ?
        ExchangeInfoStructures::MilkProductCharacteristic dayProduct;
        for (unsigned int i = 0; i < vDairyQuarterMp.size(); i++)
        {
            dayProduct += vDairyQuarterMp[i];
        }
        if (day > 1) // If day = 1, the milk is for the new born calf, so not delivered neither discarded
        {
            if (colostrum or waitForDeliver)
            {
                toDiscard += dayProduct;
            }
            else
            {
                toDeliver +=dayProduct;
            }
        }
        
        // For herd begin lactation quantity mean
        ponderedTodayMilkQuantity = updatedUdderMp.quantity / FunctionalConstants::Lactation::MILK_PRODUCT_FACTOR_DEPENDING_LACTATION_RANK[correctedParity2-1];   
    }
    
    void LactationManagement::setSunnyAdjustmentOfTheDay(ExchangeInfoStructures::MilkProductCharacteristic &milkAdjustmentOfTheDay, const boost::gregorian::date &date)
    {
        milkAdjustmentOfTheDay.quantity =  FunctionalConstants::Lactation::SUMMER_MAX_QUANTITY_VALUE;
        milkAdjustmentOfTheDay.TB =  FunctionalConstants::Lactation::SUMMER_MAX_TB_VALUE;
        milkAdjustmentOfTheDay.TP =  FunctionalConstants::Lactation::SUMMER_MAX_TP_VALUE;
        milkAdjustmentOfTheDay.SCC =  FunctionalConstants::Lactation::SUMMER_MAX_SCC_VALUE;
        milkAdjustmentOfTheDay *= Tools::getSunnyRatio(date, Tools::hemisphere::north); // always north for the moment     
    }
    
    ExchangeInfoStructures::MilkProductCharacteristic LactationManagement::getDriedPeriodEffectToMilk(const boost::gregorian::date &currentDate, const boost::gregorian::date &lastDryOffDate, bool lactating, unsigned int lactationNumber)
    {
        ExchangeInfoStructures::MilkProductCharacteristic res;
        res.quantity = 1.0f;
        res.TB = 0.0f;
        res.TP = 0.0f;
        res.SCC = 0.0f;

        // calculate the current dried period to milk factor
        unsigned int duration = 0;
        if (!lactating)
        {
            // Dried period
            if (lastDryOffDate.is_not_a_date())
            {
                return res; // surely first lactation
            }
            else
            {
                // dried off duration
                duration = (currentDate - lastDryOffDate).days();
            }
        }
        
        // Quantity
        if (duration >= FunctionalConstants::Lactation::MINIMUM_DRIED_PERIOD_DURATION_WITHOUT_EFFECT_VALUE)
        {
            return res; // surely first lactation
        }
        res.quantity = 1 - (((duration * FunctionalConstants::Lactation::MILK_LOSS_A_RATIO_BECAUSE_OF_NO_DRIED_PERIOD) + FunctionalConstants::Lactation::MILK_LOSS_PERCENT_BECAUSE_OF_NO_DRIED_PERIOD) / 100);
        
        // TB and TP
        if (lactationNumber > 3) lactationNumber = 3;
        lactationNumber -= 2; // To target the good ref value
        float ratio = (float)(FunctionalConstants::Lactation::MINIMUM_DRIED_PERIOD_DURATION_WITHOUT_EFFECT_VALUE - duration)/(float)FunctionalConstants::Lactation::MINIMUM_DRIED_PERIOD_DURATION_WITHOUT_EFFECT_VALUE;
        res.TB = FunctionalConstants::Lactation::TB_GAIN_BECAUSE_OF_NO_DRIED_PERIOD[lactationNumber] * ratio;
        res.TP = FunctionalConstants::Lactation::TP_GAIN_BECAUSE_OF_NO_DRIED_PERIOD[lactationNumber] * ratio;
        // No effect on res.SCC
        return res;
    }
    
    float LactationManagement::getSCCMilkProductReductionQuantity(float SCC, unsigned int lactationRank, int lactationDay)
    {
        if (SCC > FunctionalConstants::Lactation::REFERENCE_BASE_SCC_FOR_MILK_QUANTITY_REDUCTION)
        {
#ifdef _LOG
            assert(lactationRank != 0);
#endif // _LOG

            // 3 and more
            if (lactationRank > 3) lactationRank = 3;
            const std::tuple<float, float, float> &abcParameters = FunctionalConstants::Lactation::SCC_FOR_MILK_QUANTITY_REDUCTION_EQUATION_PARITY_PARAMETER[lactationRank-1];
            float a = std::get<0>(abcParameters);
            float b = std::get<1>(abcParameters);
            float c = std::get<2>(abcParameters);
            return (a*std::pow(lactationDay,2) + b*lactationDay + c) * std::log(SCC/FunctionalConstants::Lactation::REFERENCE_BASE_SCC_FOR_MILK_QUANTITY_REDUCTION);
        }
        else
        {
            return 0.0f;
        }
    }
    
    float LactationManagement::calculateDeliveredMilkPrice(ExchangeInfoStructures::MilkProductCharacteristic &deliveredMilkKg, float collectedMilkTestResultsSCC, ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &currentAccountingModel)
    {
        float milkQuantityTon = deliveredMilkKg.quantity / 1000.00f;
        float tonSccPenality = getTonSccPenality(collectedMilkTestResultsSCC, currentAccountingModel);
        float tonPrice = currentAccountingModel.getCurrentPrice(FunctionalConstants::AccountingModel::KEY_MILK_TON_PRICE);
        
        // TB
        float TBRealQuality = deliveredMilkKg.TB;
        float TBQualityDelta = TBRealQuality - FunctionalConstants::AccountingModel::TB_BASIS_QUALITY_BONUS;
        float TBTonQualityBonus = TBQualityDelta * currentAccountingModel.getCurrentPrice(FunctionalConstants::AccountingModel::KEY_TB_QUALITY_BONUS);
        // TP
        float TPRealQuality = deliveredMilkKg.TP;
        float TPQualityDelta = TPRealQuality - FunctionalConstants::AccountingModel::TP_BASIS_QUALITY_BONUS;
        float TPTonQualityBonus = TPQualityDelta * currentAccountingModel.getCurrentPrice(FunctionalConstants::AccountingModel::KEY_TP_QUALITY_BONUS);
        
        float price = (tonPrice + TBTonQualityBonus + TPTonQualityBonus + tonSccPenality) * milkQuantityTon;

        return price;
    }
    
    float LactationManagement::getTonSccPenality(float sccLevel, ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &currentAccountingModel)
    {
        float levelSCCPenalty = 0.0f;
        if (sccLevel > FunctionalConstants::Lactation::LEVEL_3_FOR_SCC_PENALTY)
        {
            levelSCCPenalty = currentAccountingModel.getCurrentPrice(FunctionalConstants::AccountingModel::KEY_LEVEL_3_SCC_PENALTY);
        }
        else if (sccLevel > FunctionalConstants::Lactation::LEVEL_2_FOR_SCC_PENALTY)
        {
            levelSCCPenalty = currentAccountingModel.getCurrentPrice(FunctionalConstants::AccountingModel::KEY_LEVEL_2_SCC_PENALTY);
        }
        else if (sccLevel > FunctionalConstants::Lactation::LEVEL_1_FOR_SCC_PENALTY)
        {
            levelSCCPenalty = currentAccountingModel.getCurrentPrice(FunctionalConstants::AccountingModel::KEY_LEVEL_1_SCC_PENALTY);
        }
        return levelSCCPenalty;
    }
    


}
