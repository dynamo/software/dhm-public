#ifndef LactationManagement_h
#define LactationManagement_h

// standard
#include <vector>

// boost
#include <boost/date_time/gregorian/gregorian.hpp>

// project
#include "../ExchangeInfoStructures/MilkProductCharacteristic.h"
#include "../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "../ExchangeInfoStructures/DairyFarmAccountingModelParameters.h"
#include "../Tools/Random.h"
#include "I_UdderMilkModulation.h"
//#include "../Health/Diseases/Lameness/LamenessStates/I_LamenessProneFoots.h"
#include "../DataStructures/Foots.h"

namespace Lactation
{
    class LactationManagement
    {
    protected:
        virtual void concrete() = 0; // To ensure that it will not be instantiated

    public:
        static void getMilkModulatedProductionOfTheDay( ExchangeInfoStructures::MilkProductCharacteristic &updatedUdderMp,
                                                        bool colostrum,
                                                        ExchangeInfoStructures::MilkProductCharacteristic &toDeliver,
                                                        ExchangeInfoStructures::MilkProductCharacteristic &toDiscard,
                                                        bool oestrus,
                                                        unsigned int day,
                                                        ExchangeInfoStructures::MilkProductCharacteristic &milkSunnyAdjustmentOfTheDay,
                                                        FunctionalEnumerations::Lactation::MilkingFrequency &mf,
                                                        ExchangeInfoStructures::MilkProductCharacteristic &currentDriedPeriodEffectToMilk,
                                                        bool pregnant,
                                                        Lactation::I_UdderMilkModulation* pDairyUdder,
                                                        DataStructures::Foots* pFoots,
                                                        const boost::gregorian::date &date,
                                                        unsigned int lactationRank, 
                                                        int lactationStage, 
                                                        float &ponderedTodayMilkQuantity,
                                                        ExchangeInfoStructures::MilkProductCharacteristic &milkProductionObjectiveFactor,
                                                        Tools::Random &random
                                                        );

        static void setSunnyAdjustmentOfTheDay(ExchangeInfoStructures::MilkProductCharacteristic &milkAdjustmentOfTheDay, const boost::gregorian::date &date);
        static ExchangeInfoStructures::MilkProductCharacteristic getDriedPeriodEffectToMilk(const boost::gregorian::date &currentDate, const boost::gregorian::date &lastDryOffDate, bool lactating, unsigned int lactationNumber);
        static float getSCCMilkProductReductionQuantity(float SCC, unsigned int lactationRank, int lactationDay);
        static float calculateDeliveredMilkPrice(ExchangeInfoStructures::MilkProductCharacteristic &deliveredMilkKg, float collectedMilkTestResultsSCC, ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &currentAccountingModel);
    private:
        static float getTonSccPenality(float sccLevel, ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &currentAccountingModel);
    };
} /* End of namespace Lactation */
#endif // LactationManagement_h
