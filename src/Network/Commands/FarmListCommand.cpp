/* 
 * File:   FarmListCommand.cpp
 * Author: pgontier
 *
 * Created on 05 may 2023, 11:04
 */

#ifdef _DHM_SERVER
#include "FarmListCommand.h"
#include "../../Simulator/Controler.h"
#include "ExchangeInfoStructures/TechnicalConstants.h"
#include "ExchangeInfoStructures/DairyFarmParameters.h"
#include "Tools/Tools.h"
#include <boost/algorithm/string/replace.hpp>

namespace WebCommand
{
    FarmListCommand::FarmListCommand() : WebCommand()
    {
    }

    FarmListCommand::FarmListCommand(const FarmListCommand& orig) : WebCommand(orig)
    {
    }

    FarmListCommand::~FarmListCommand()
    {
    }

    std::string FarmListCommand::act(boost::property_tree::ptree &pt, std::string &clientAnswer)
    {
        std::string message = "";
        std::string comp = ""; // Complement of the answer
        
        std::map<std::string, ExchangeInfoStructures::Parameters::DairyFarmParameters> mSystemToSimulateParameterList;
        std::string dairyFarmPath = TechnicalConstants::RELATIVE_DAIRY_FARM_DEFAULT_FOLDER;
        Simulator::Controler::getSystemToSimulateParameterList(dairyFarmPath, mSystemToSimulateParameterList);
        
        for (auto farmParameter : mSystemToSimulateParameterList)
        {
            
            // Parameter serialization
            std::string serializedParam; 
            std::stringstream ss; // Create the file stream
            { // Mandatory
                boost::archive::xml_oarchive oa(ss); // Declare the xml archive            
                oa << BOOST_SERIALIZATION_NVP(farmParameter.second);   
            } // Mandatory
            serializedParam = ss.str();
            
            // Remote JSON control chars
//            boost::replace_all(serializedParam, "\"", "_q");
//            boost::replace_all(serializedParam, "\b", "_b");
//            boost::replace_all(serializedParam, "\f", "_f");
//            boost::replace_all(serializedParam, "\n", "_n");
//            boost::replace_all(serializedParam, "\r", "_b");
//            boost::replace_all(serializedParam, "\t", "_t");
//            boost::replace_all(serializedParam, "\\", "_bs");
            for (auto CONV : NetworkConstants::JSON_CONVERSION)
            {
                boost::replace_all(serializedParam, std::get<1>(CONV), std::get<0>(CONV));
            }
 
            comp +="\",\"" + farmParameter.first + "\":\"" + serializedParam;
        }
        
        // Returned message finalization
        return finalizeReturnedMessage(NetworkConstants::COMMAND_FARM_LIST, comp, message, clientAnswer);
    }
}
#endif // _DHM_SERVER
