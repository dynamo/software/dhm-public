/* 
 * File:   FarmListCommand.h
 * Author: pgontier
 *
 * Created on 05 may 2023, 11:04
 */
#ifdef _DHM_SERVER
#ifndef WebCommand_FarmListCommand_h
#define WebCommand_FarmListCommand_h

#include "WebCommand.h"

namespace WebCommand
{
    class FarmListCommand : public WebCommand
    {
    private:

    public:
        FarmListCommand();
        FarmListCommand(const FarmListCommand& orig);
        virtual ~FarmListCommand();
        virtual void concrete(){} // To ensure that class can be instantiated
        std::string act(boost::property_tree::ptree &pt, std::string &clientAnswer);
    };
}

#endif /* WebCommand_FarmListCommand_h */
#endif // _DHM_SERVER

