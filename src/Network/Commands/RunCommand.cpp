/* 
 * File:   RunCommand.cpp
 * Author: pgontier
 * 
 * Created on 24 mars 2023, 13:15
 */

#ifdef _DHM_SERVER
#include "RunCommand.h"
#include "../../Simulator/Controler.h"
#include "ExchangeInfoStructures/DairyFarmParameters.h"
#include "ExchangeInfoStructures/ProtocolParameters.h"
#include "Tools/Tools.h"

#include <boost/algorithm/string/replace.hpp>

namespace WebCommand
{
    RunCommand::RunCommand() : WebCommand()
    {
    }

    RunCommand::RunCommand(const RunCommand& orig) : WebCommand(orig)
    {
    }

    RunCommand::~RunCommand()
    {
    }
    
    void RunCommand::getFarmParameterFromSerialization(std::string &serializedParam, ExchangeInfoStructures::Parameters::DairyFarmParameters &param)
    {
        // Restore potential JSON control chars
//        boost::replace_all(serializedParam, "_q", "\"");
//        boost::replace_all(serializedParam, "_b", "\b");
//        boost::replace_all(serializedParam, "_f", "\f");
//        boost::replace_all(serializedParam, "_n", "\n");
//        boost::replace_all(serializedParam, "_b", "\r");
//        boost::replace_all(serializedParam, "_t", "\t");
//        boost::replace_all(serializedParam, "_bs", "\\");
        for (auto CONV : NetworkConstants::JSON_CONVERSION)
        {
            boost::replace_all(serializedParam, std::get<0>(CONV), std::get<1>(CONV));
        }
         
        // Parameter deserialization
        std::stringstream ss(serializedParam);
        assert(ss.good());
        { // Mandatory
            boost::archive::xml_iarchive ia(ss);
            ia >> BOOST_SERIALIZATION_NVP(param);
        } // Mandatory
    }

    std::string RunCommand::act(boost::property_tree::ptree &pt, std::string &clientAnswer)
    {
        // Concerned user
        std::string strUserNumber = pt.get<std::string>(NetworkConstants::USER);
        unsigned int userId = atoi(strUserNumber.c_str()); 
        
        // Parameters :
        std::string strRunNumber = pt.get<std::string>("runNumber");
        std::string strTotalDuration = pt.get<std::string>("totalDuration");
        std::string strSimYear = pt.get<std::string>("simYear");
        std::string strWarmDuration = pt.get<std::string>("warmDuration");
        std::string message = "";
        
        // Generate farm 1
        std::string dairyFarm1Name = "F1";
        std::string serializedParameter = pt.get<std::string>("farm1");
        ExchangeInfoStructures::Parameters::DairyFarmParameters farm1Parameters;
        getFarmParameterFromSerialization(serializedParameter, farm1Parameters);
        farm1Parameters.name = dairyFarm1Name;
        Simulator::Controler::generateSystemToSimulate(farm1Parameters, message, userId); // Creating a dairy farm  with the current parameters
        
        // Generate farm 2
        std::string dairyFarm2Name = "";
        if (message == "")
        {
            dairyFarm2Name = "F2";
            serializedParameter = pt.get<std::string>("farm2");
            ExchangeInfoStructures::Parameters::DairyFarmParameters farm2Parameters;
            getFarmParameterFromSerialization(serializedParameter, farm2Parameters);
            farm2Parameters.name = dairyFarm2Name;
            Simulator::Controler::generateSystemToSimulate(farm2Parameters, message, userId); // Creating a dairy farm  with the current parameters
        }
        
        // Protocols
        std::vector<std::string> protocols;
        std::string protocolName1 = "";
        ExchangeInfoStructures::Parameters::ProtocolParameters infoProtocol; // Default protocol parameters
        if (message == "")
        {
            protocolName1 = dairyFarm1Name;
            infoProtocol.name = protocolName1;
            infoProtocol.runNumber = atoi(strRunNumber.c_str());
            infoProtocol.simulationDuration = atoi(strTotalDuration.c_str());
            infoProtocol.simYear = atoi(strSimYear.c_str());
            infoProtocol.warmupDuration = atoi(strWarmDuration.c_str());
            infoProtocol.runableName = dairyFarm1Name;
            infoProtocol.discriminant1 = "reference";
            Simulator::Controler::createProtocol(infoProtocol, message, userId); // Creating a protocol with the current parameters
            protocols.push_back(protocolName1);
        }
        std::string protocolName2 = "";
        if (message == "")
        {
            protocolName2 = dairyFarm2Name;
            infoProtocol.name = protocolName2;
            infoProtocol.runableName = dairyFarm2Name;
            infoProtocol.discriminant1 = "variate";
            Simulator::Controler::createProtocol(infoProtocol, message, userId); // Creating a protocol with the current parameters
            protocols.push_back(protocolName2);
        }
        
        // Simulation
        std::string comp = ""; // Complement of the answer
        if (message == "")
        {
            std::map<std::string, std::vector<std::string>> mStreamResults;
            Simulator::Controler::simulateProtocols(protocols, mStreamResults, message, userId);
            
            // Write results
            for (auto itResult : mStreamResults)
            {
                unsigned int cpt = 0;
                for (auto itLines : itResult.second)
                {
                    std::string resultBefore = itLines;
                    std::string result = Tools::changeChar(resultBefore, ',', '.');
                    comp +="\",\"" + itResult.first + Tools::toString(cpt++) + "\":\"" + result;
                }
            }
        }
        
        // Cleaning DB
        //if (message == "")
        {
            Simulator::Controler::deleteProtocol(protocolName1, message, userId);
        }
        //if (message == "")
        {
            Simulator::Controler::deleteProtocol(protocolName2, message, userId);
        }
        //if (message == "")
        {
            Simulator::Controler::deleteSystemToSimulate(dairyFarm1Name, message, userId);
        }
        //if (message == "")
        {
            Simulator::Controler::deleteSystemToSimulate(dairyFarm2Name, message, userId);
        }

        // Returned message finalization
        return finalizeReturnedMessage(NetworkConstants::COMMAND_RUN, comp, message, clientAnswer);
     }
}
#endif // _DHM_SERVER
