/* 
 * File:   RunCommand.h
 * Author: pgontier
 *
 * Created on 24 mars 2023, 13:15
 */
#ifdef _DHM_SERVER
#ifndef WebCommand_RunCommand_h
#define WebCommand_RunCommand_h

#include "WebCommand.h"
#include "../../ExchangeInfoStructures/DairyFarmParameters.h"

namespace WebCommand
{
    class RunCommand : public WebCommand
    {
    private:
        static void getFarmParameterFromSerialization(std::string &serializedParam, ExchangeInfoStructures::Parameters::DairyFarmParameters &param);

    public:
        RunCommand();
        RunCommand(const RunCommand& orig);
        virtual ~RunCommand();
        virtual void concrete(){} // To ensure that class can be instantiated
        std::string act(boost::property_tree::ptree &pt, std::string &clientAnswer);
    };
}

#endif /* WebCommand_RunCommand_h */
#endif // _DHM_SERVER

