/* 
 * File:   WebCommand.cpp
 * Author: pgontier
 * 
 * Created on 24 mars 2023, 13:15
 */

#ifdef _DHM_SERVER
#include "WebCommand.h"
#include "Tools/Tools.h"

namespace WebCommand
{

    WebCommand::WebCommand()
    {
    }

    WebCommand::WebCommand(const WebCommand& orig)
    {
    }

    WebCommand::~WebCommand()
    {
    }
    
    std::string WebCommand::finalizeReturnedMessage(const std::string &COMMAND, std::string &comp, std::string &message, std::string &clientAnswer)
    {
        std::string result = "";
        std::string resPrefix = "\",\"" + NetworkConstants::RESULT + "\":\"";
        // Returned message
        if (message == "")
        {
            result = NetworkConstants::RESULT_OK;
            comp += resPrefix + result;
        }
        else
        {
            result = Tools::changeChar(message, '"', '\'');
            comp = resPrefix + result;
        }       
        clientAnswer = COMMAND + comp;
        return result;
    }
}
#endif // _DHM_SERVER
