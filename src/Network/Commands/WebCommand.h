/* 
 * File:   WebCommand.h
 * Author: pgontier
 *
 * Created on 24 mars 2023, 13:15
 */
#ifdef _DHM_SERVER
#ifndef WebCommand_WebCommand_h
#define WebCommand_WebCommand_h

#include <string>
#include <boost/property_tree/ptree.hpp>
#include "./Network/NetworkConstants.h"

namespace WebCommand
{
    class WebCommand
    {
    protected:
        std::string static finalizeReturnedMessage(const std::string &COMMAND, std::string &comp, std::string &message, std::string &clientAnswer);
    public:
        WebCommand();
        WebCommand(const WebCommand& orig);
        virtual ~WebCommand();
        virtual void concrete() = 0; // To ensure that class will not be instantiated
        virtual std::string act(boost::property_tree::ptree &pt, std::string &clientAnswer) = 0;
    };
}

#endif /* WebCommand_WebCommand_h */
#endif // _DHM_SERVER

