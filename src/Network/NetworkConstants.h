#ifndef NetworkConstants_h
#define NetworkConstants_h

namespace NetworkConstants
{
    const std::string CONF_FILE_NAME = "serverSetup.ini";
    const std::string USER = "user";
    const std::string COMMAND = "cmd";
    const std::string RESULT = "result";
    const std::string RESULT_OK = "ok";
    const std::string RESULT_NOK = "nok";
    const std::string COMMAND_RUN = "run";
    const std::string COMMAND_FARM_LIST = "farm_list";
    
    const std::vector<std::tuple<std::string,std::string>> JSON_CONVERSION {{"_q", "\""},
                                                                            {"_b", "\b"},
                                                                            {"_f", "\f"},
                                                                            {"_n", "\n"},
                                                                            {"_b", "\r"},
                                                                            {"_t", "\t"},
                                                                            {"_bs", "\\"}};
    
}
#endif // NetworkConstants_h

