#ifndef COMMON_CLIENT_CERTIFICATE_HPP
#define COMMON_CLIENT_CERTIFICATE_HPP

#include <boost/asio/ssl.hpp>
#include <string>
#include <iostream>
#include <fstream>
#include "../Tools/Display.h"

namespace ssl = boost::asio::ssl; // from <boost/asio/ssl.hpp>

namespace detail
{

inline void load_root_certificates(ssl::context& ctx, std::string &intermFile, boost::system::error_code& ec)
{
//    std::cout << "intermFile " << intermFile << std::endl;
    std::string cert = "";
    std::ifstream file(intermFile,  std::ios::in); 
    if(file.is_open())
    {       
        std::string line;
        while(getline(file, line))
        {
            cert += line + "\n";
        }
        file.close();

        ctx.add_certificate_authority(
            boost::asio::buffer(cert.data(), cert.size()), ec);
        if(ec)
            return;
    }
    else Console::Display::displayLineInConsole("-> Error : unable to read interm file " + intermFile, Console::ANSI_RED);
}

} // detail

// Load the root certificates into an ssl::context

inline
void
load_root_certificates(ssl::context& ctx, std::string &certifFile, boost::system::error_code& ec)
{
    detail::load_root_certificates(ctx, certifFile, ec);
}

inline
void
load_root_certificates(ssl::context& ctx, std::string &certifFile)
{
    boost::system::error_code ec;
    detail::load_root_certificates(ctx, certifFile, ec);
    if(ec)
        throw boost::system::system_error{ec};
}

#endif // COMMON_CLIENT_CERTIFICATE_HPP