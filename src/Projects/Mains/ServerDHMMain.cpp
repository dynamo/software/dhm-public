/*
   _____        _              _    _            _ _   _       __  __                                   
  |  __ \      (_)            | |  | |          | | | | |     |  \/  |                                  
  | |  | | __ _ _ _ __ _   _  | |__| | ___  __ _| | |_| |__   | \  / | __ _ _ __   __ _  __ _  ___ _ __ 
  | |  | |/ _` | | '__| | | | |  __  |/ _ \/ _` | | __| '_ \  | |\/| |/ _` | '_ \ / _` |/ _` |/ _ \ '__|
  | |__| | (_| | | |  | |_| | | |  | |  __/ (_| | | |_| | | | | |  | | (_| | | | | (_| | (_| |  __/ |   
  |_____/ \__,_|_|_|   \__, | |_|  |_|\___|\__,_|_|\__|_| |_| |_|  |_|\__,_|_| |_|\__,_|\__, |\___|_|   
                        __/ |                                                            __/ |          
                       |___/                                                            |___/      
 __          __  _                                                        _ _           _   _             
 \ \        / / | |                                                      | (_)         | | (_)            
  \ \  /\  / /__| |__    ___  ___ _ ____   _____ _ __    __ _ _ __  _ __ | |_  ___ __ _| |_ _  ___  _ __  
   \ \/  \/ / _ \ '_ \  / __|/ _ \ '__\ \ / / _ \ '__|  / _` | '_ \| '_ \| | |/ __/ _` | __| |/ _ \| '_ \ 
    \  /\  /  __/ |_) | \__ \  __/ |   \ V /  __/ |    | (_| | |_) | |_) | | | (_| (_| | |_| | (_) | | | |
     \/  \/ \___|_.__/  |___/\___|_|    \_/ \___|_|     \__,_| .__/| .__/|_|_|\___\__,_|\__|_|\___/|_| |_|
                                                             | |   | |                                    
                                                             |_|   |_|                                    

 * File:   ServerDHMMain.cpp
 * Author: Philippe Gontier
 *
 * Created on 05 april 2023
 * 
 */

// ref :
// simple : https://www.boost.org/doc/libs/1_82_0/libs/beast/example/websocket/server/sync/websocket_server_sync.cpp
// secure : https://www.boost.org/doc/libs/1_82_0/libs/beast/example/websocket/server/sync-ssl/websocket_server_sync_ssl.cpp

#ifdef _DHM_SERVER

#include "./Network/server_certificate.hpp"

#include <boost/beast/core.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/beast/websocket/ssl.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/stream.hpp>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <string>
#include <thread>

#include <map>
#include "./Network/Commands/RunCommand.h"
#include "./Network/Commands/FarmListCommand.h"
#include "../../Tools/Tools.h"
#include "../../Tools/Display.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "../../Simulator/Controler.h"

// Global values
std::map<std::string, WebCommand::WebCommand*> mCommands; // Web command dictionary

std::string parseMapToMessage(std::map<std::string, std::string> &mapToParse)
{
    std::string message = "{";
    for (auto line : mapToParse)
    {
        if (message.length() > 1)
        {
            // We already have lines
            message += ",";
        }
        message += "\"" + line.first + "\":\"" + line.second + "\"";
    }
    message += "}";
    
    return message;    
}


// Echoes back all received WebSocket messages
#ifdef _LOCAL
void do_session(boost::asio::ip::tcp::socket socket)
#else
void do_session(boost::asio::ip::tcp::socket socket, boost::asio::ssl::context& ctx)
#endif // _LOCAL
{
    std::string sClientIp = socket.remote_endpoint().address().to_string();
    unsigned short uiClientPort = socket.remote_endpoint().port();            
    try
    {
        // Construct the websocket stream around the socket
#ifdef _LOCAL
        boost::beast::websocket::stream<boost::asio::ip::tcp::socket> ws{std::move(socket)};
#else
        boost::beast::websocket::stream<boost::beast::ssl_stream<boost::asio::ip::tcp::socket&>> ws{socket, ctx};

        // Perform the SSL handshake
        ws.next_layer().handshake(boost::asio::ssl::stream_base::server);
#endif // _LOCAL

        // Set a decorator to change the Server of the handshake
        ws.set_option(boost::beast::websocket::stream_base::decorator(
            [](boost::beast::websocket::response_type& res)
            {
                res.set(boost::beast::http::field::server,
                    std::string(BOOST_BEAST_VERSION_STRING) +
#ifdef _LOCAL
                        " websocket-server-sync");
#else
                        " websocket-server-sync-ssl");
#endif // _LOCAL
            }));

        // Accept the websocket handshake
        ws.accept();

        while (true)
        {
            // This buffer will hold the incoming message
            boost::beast::flat_buffer buffer;

            // Read a message
            ws.read(buffer);

            // Console messages
            Console::Display::displayLineInConsole("Date: " + Console::Display::getCurrentStringTimestamp() + ", client IP: " + sClientIp + ":" + Tools::toString(uiClientPort));
            std::string receivedData = boost::beast::buffers_to_string(buffer.data());
//            Console::Display::displayLineInConsole("-> Data received: " + receivedData);


            // Parsing           
            boost::property_tree::ptree pt;
            std::istringstream iss(receivedData);
            boost::property_tree::read_json(iss, pt);
            
            // Client answer
            std::string clientAnswer = "";
            std::map<std::string, std::string> mClientAnswer;
            
            // User 
            std::string strUserNumber = pt.get<std::string>(NetworkConstants::USER);
            mClientAnswer[NetworkConstants::USER] = strUserNumber;
            
            // Command 
            std::string strCommand = pt.get<std::string>(NetworkConstants::COMMAND);
            auto itCommands = mCommands.find(strCommand);
            
            Console::Display::displayLineInConsole("-> command received from client " + strUserNumber + " : " + strCommand);

            std::string result = NetworkConstants::RESULT_NOK;
            if (itCommands != mCommands.end())
            {
                // Known command
                result = itCommands->second->act(pt, clientAnswer);
            }
            else
            {
                // Unknown command
                clientAnswer = "Unknown command !!!!";
            }

            // Give a response back
            mClientAnswer[NetworkConstants::COMMAND] = clientAnswer;
            ws.text(ws.got_text());
            std::string response = parseMapToMessage(mClientAnswer);
            ws.write(boost::asio::buffer(response));
            if (result != NetworkConstants::RESULT_OK)
            //if (true)
            {
                Console::Display::displayLineInConsole("-> Answered to client: " + response);
            }
            else
            {
                Console::Display::displayLineInConsole("-> done");
            }
        }
    }
    catch(boost::beast::system_error const& se)
    {
        // This indicates that the session was closed
        std::string errCode = Tools::toString(se.code());
        if(errCode != "system:10054" and errCode != "boost.beast.websocket:1")
        {
            std::string message = "Error: " + se.code().message();
            //tmpTools::saveLogMessage(message);
            Console::Display::displayLineInConsole(message);
        }

//        // This indicates that the session was closed
//        if(se.code() != boost::beast::websocket::error::closed)
//            std::cerr << "Error: " << se.code().message() << std::endl;
    }
    catch(std::exception const& e)
    {
        std::string what(e.what());
        std::string message = "Error: " + what;
        //tmpTools::saveLogMessage(message);
        Console::Display::displayLineInConsole(message);

//        std::cerr << "Error: " << e.what() << std::endl;
    }
}

//------------------------------------------------------------------------------

int main(int argc, char* argv[])
{
    // Preparing the log folder
    std::string logPath = TechnicalConstants::DEFAULT_RELATIVE_RESULT_FOLDER + TechnicalConstants::LOG_DIRECTORY;
    mkdir(logPath.c_str() DIR_RIGHT); 
    
    // Log file to save console
    std::string appName = "dhm_server";
    std::string logFileName = TechnicalConstants::DEFAULT_RELATIVE_RESULT_FOLDER + TechnicalConstants::LOG_DIRECTORY + TechnicalConstants::LOG_FILE_NAME + "_" + appName;
    Console::Display::init(logFileName);
    Simulator::Controler::initInstance();
    
    Console::Display::displayLineInConsole(R"!(+--------------------------------------------------------------------------------------------------------+)!", Console::ANSI_YELLOW);
    Console::Display::displayLineInConsole(R"!(|  _____        _              _    _            _ _   _       __  __                                    |)!", Console::ANSI_YELLOW);
    Console::Display::displayLineInConsole(R"!(| |  __ \      (_)            | |  | |          | | | | |     |  \/  |                                   |)!", Console::ANSI_YELLOW);
    Console::Display::displayLineInConsole(R"!(| | |  | | __ _ _ _ __ _   _  | |__| | ___  __ _| | |_| |__   | \  / | __ _ _ __   __ _  __ _  ___ _ __  |)!", Console::ANSI_YELLOW);
    Console::Display::displayLineInConsole(R"!(| | |  | |/ _` | | '__| | | | |  __  |/ _ \/ _` | | __| '_ \  | |\/| |/ _` | '_ \ / _` |/ _` |/ _ \ '__| |)!", Console::ANSI_YELLOW);
    Console::Display::displayLineInConsole(R"!(| | |__| | (_| | | |  | |_| | | |  | |  __/ (_| | | |_| | | | | |  | | (_| | | | | (_| | (_| |  __/ |    |)!", Console::ANSI_YELLOW);
    Console::Display::displayLineInConsole(R"!(| |_____/ \__,_|_|_|   \__, | |_|  |_|\___|\__,_|_|\__|_| |_| |_|  |_|\__,_|_| |_|\__,_|\__, |\___|_|    |)!", Console::ANSI_YELLOW);
    Console::Display::displayLineInConsole(R"!(|                       __/ |                                                            __/ |           |)!", Console::ANSI_YELLOW);
    Console::Display::displayLineInConsole(R"!(|                      |___/      ( W e b   s e r v e r   v e r s i o n )               |___/            |)!", Console::ANSI_YELLOW);
    Console::Display::displayLineInConsole(R"!(+--------------------------------------------------------------------------------------------------------+)!", Console::ANSI_YELLOW);
    Console::Display::displayLineInConsole(TechnicalConstants::STR_VERSION, Console::ANSI_YELLOW);
    
    // Command creation for dictionary
    mCommands[NetworkConstants::COMMAND_RUN] = new WebCommand::RunCommand();
    mCommands[NetworkConstants::COMMAND_FARM_LIST] = new WebCommand::FarmListCommand();
    
    try
    {
        std::string strConfFileName = TechnicalConstants::RELATIVE_TECHNICAL_DATA_FOLDER + NetworkConstants::CONF_FILE_NAME;
#ifdef _LOCAL
        std::string strIpAddress = "127.0.0.1";
        std::string strPort = "54000";
#else
        std::vector<std::vector<std::string>> readData;
        Tools::readFile(readData, strConfFileName, TechnicalConstants::EXPORT_SEP, TechnicalConstants::COMMENT);
        std::string strIpAddress = readData[0][0];
        std::string strPort = readData[0][1];
        std::string certifFile = readData[1][0];
        std::string keyFile = readData[2][0];
#endif // _LOCAL

                        
                        
        std::string ipConfig = "Server IP: " + strIpAddress + ":" + strPort;
        Console::Display::displayLineInConsole(ipConfig, Console::ANSI_WHITE);
     
        auto const address = boost::asio::ip::make_address(strIpAddress);
        boost::asio::ip::port_type const port = atoi(strPort.c_str());
        
        // The io_context is required for all I/O
        boost::asio::io_context ioc{1};

        Console::Display::displayLineInConsole("Waiting for messages...");    

#ifndef _LOCAL
        // The SSL context is required, and holds certificates
        boost::asio::ssl::context ctx{boost::asio::ssl::context::tlsv13};

        // This holds the certificate used by the server
        load_server_certificate(ctx, certifFile, keyFile);
#endif // _LOCAL

        // The acceptor receives incoming connections
        boost::asio::ip::tcp::acceptor acceptor{ioc, {address, port}};
        while (true)
        {
            // This will receive the new connection
            boost::asio::ip::tcp::socket socket{ioc};

            // Block until we get a connection
            acceptor.accept(socket);

            // Launch the session, transferring ownership of the socket
            std::thread(
                &do_session,
#ifdef _LOCAL
                std::move(socket)).detach();
#else
                std::move(socket),
                std::ref(ctx)).detach();
#endif // _LOCAL
        }
    }
    catch (const std::exception& e)
    {
        std::string what(e.what());
        std::string message = "Error: " + what;
        Console::Display::displayLineInConsole(message);
        return EXIT_FAILURE;

//        std::cerr << "Error: " << e.what() << std::endl;
//        return EXIT_FAILURE;
    }
    
    // Cleaning before exit
    // --------------------
    Simulator::Controler::leave();
    
    for (auto pComIt : mCommands)
    {
        delete pComIt.second;
    }
    mCommands.clear();
    
    Console::Display::clean();
    
    return 0;
}
#endif // _DHM_SERVER
