/*
    _____        _              _    _            _ _   _       __  __                                   
   |  __ \      (_)            | |  | |          | | | | |     |  \/  |                                  
   | |  | | __ _ _ _ __ _   _  | |__| | ___  __ _| | |_| |__   | \  / | __ _ _ __   __ _  __ _  ___ _ __ 
   | |  | |/ _` | | '__| | | | |  __  |/ _ \/ _` | | __| '_ \  | |\/| |/ _` | '_ \ / _` |/ _` |/ _ \ '__|
   | |__| | (_| | | |  | |_| | | |  | |  __/ (_| | | |_| | | | | |  | | (_| | | | | (_| | (_| |  __/ |   
   |_____/ \__,_|_|_|   \__, | |_|  |_|\___|\__,_|_|\__|_| |_| |_|  |_|\__,_|_| |_|\__,_|\__, |\___|_|   
                         __/ |                                                            __/ |          
                        |___/                                                            |___/      

   _____ _                  _       _                                      _ _           _   _             
  / ____| |                | |     | |                                    | (_)         | | (_)            
 | (___ | |_ __ _ _ __   __| | __ _| | ___  _ __   ___    __ _ _ __  _ __ | |_  ___ __ _| |_ _  ___  _ __  
  \___ \| __/ _` | '_ \ / _` |/ _` | |/ _ \| '_ \ / _ \  / _` | '_ \| '_ \| | |/ __/ _` | __| |/ _ \| '_ \ 
  ____) | || (_| | | | | (_| | (_| | | (_) | | | |  __/ | (_| | |_) | |_) | | | (_| (_| | |_| | (_) | | | |
 |_____/ \__\__,_|_| |_|\__,_|\__,_|_|\___/|_| |_|\___|  \__,_| .__/| .__/|_|_|\___\__,_|\__|_|\___/|_| |_|
                                                              | |   | |                                    
                                                              |_|   |_|                                    

 * File:   mainDHM.cpp
 * Author: Philippe Gontier
 *
 * Created on 21 septembre 2020
 * 
 */

#ifndef _DHM_SERVER

// std
#include <iostream>
#include <string>
#include <chrono>
#include <fstream>


#include <stdio.h>
#ifdef _WIN32
#include <windows.h>
#include <shlobj.h>
#endif

// Lib
#include <tclap/CmdLine.h>

// Import

// Dhm
#include "../../Simulator/Controler.h"
#include "../../ExchangeInfoStructures/FunctionalConstants.h"
#include "../../ExchangeInfoStructures/TechnicalConstants.h"
#include "../../ExchangeInfoStructures/DairyFarmParameters.h"
#include "../../ExchangeInfoStructures/ProtocolParameters.h"
#include "../../ExchangeInfoStructures/DairyFarmAccountingModelParameters.h"
#include "../../Tools/Display.h"

// Command lines
// Load farm files : --accountingDataPath ../data/AccountingData/ --dairyFarmDataPath D:/Simulateur/ForgeMIA/dhm-dev/data/DairyFarms/ --geneticCataloguePath ../data/GeneticCatalogues/ --protocolFileName ../data/Protocols/Test.csv --resultPath ../results/
// Load simulation management file : --simulationManagementFileName ../data/SimulationManagement/Contractualisation.csv --resultPath ../results/

void createFarmAndProtocols(std::vector<std::vector<std::string>>::iterator &itValues, std::vector<std::vector<std::string>>::iterator &itEnd, ExchangeInfoStructures::Parameters::ProtocolParameters &protocolParameter, ExchangeInfoStructures::Parameters::DairyFarmParameters &initialDairyFarmParameter, ExchangeInfoStructures::Parameters::DairyFarmParameters &evoluatingDairyFarmParameter, std::string &protocolName, unsigned int deep, unsigned int &totalRunNumber, unsigned int &totalProtocolNumer)
{
    char protId = 'A';
    
    std::vector<std::string> &line = *itValues;
    for (unsigned int iVal = 1; iVal < line.size(); iVal++)
    {
        std::string valueToSet = line[iVal];
        if (valueToSet == TechnicalConstants::BASIS_KEY)
        {
            initialDairyFarmParameter.getValueToExport(line[0], valueToSet); // Set the original parameter value
        }
        if (evoluatingDairyFarmParameter.setImportedValue(line[0], valueToSet)) // Set the concerned parameter value
        {
            protocolName[deep] = protId++;
            std::vector<std::vector<std::string>>::iterator nextItLine = itValues;
            nextItLine++;
            if (nextItLine != itEnd)
            {
                createFarmAndProtocols(nextItLine, itEnd, protocolParameter, initialDairyFarmParameter, evoluatingDairyFarmParameter, protocolName, deep+1, totalRunNumber, totalProtocolNumer); // recursive setting
            }
            else
            {
                // Dairy farm creation
                evoluatingDairyFarmParameter.name = protocolName;
                Simulator::Controler::generateSystemToSimulate(evoluatingDairyFarmParameter);

                // Protocol creation
                protocolParameter.name = protocolName;
                protocolParameter.runableName = evoluatingDairyFarmParameter.name;
                protocolParameter.discriminant1 = protocolName[0];
                protocolParameter.discriminant2 = protocolName[1];
                protocolParameter.discriminant3 = protocolName[2];
                protocolParameter.discriminant4 = protocolName[3];
                Simulator::Controler::createProtocol(protocolParameter); 
                totalRunNumber += protocolParameter.runNumber;
                totalProtocolNumer++; 
            }
        }
        else
        {
            // Unknown parameter key
            std::string warningMessage = "key = \"" + line[0] + "\" unknown and so is not considered";
            Console::Display::displayLineInConsole(warningMessage, Console::ANSI_YELLOW);
        }
    }
}

int main(int argc, char** argv)
{
    try
    {

        TCLAP::CmdLine cmd("Dairy Health Manager", ' ', TechnicalConstants::STR_VERSION);
        
//#ifndef _SENSIBILITY_ANALYSIS
        // * Path of accounting data (from where the exe file is)
        //TCLAP::ValueArg<std::string> accountingDataParam("","accountingDataPath","Path to the accounting data, default ../data/AccountingData/",false,"../data/AccountingData/","string");
        TCLAP::ValueArg<std::string> accountingDataParam("", "accountingDataPath", "Path to the accounting data, default " + TechnicalConstants::RELATIVE_ACCOUNTING_DATA_DEFAULT_FOLDER, false, TechnicalConstants::RELATIVE_ACCOUNTING_DATA_DEFAULT_FOLDER, "string");
        cmd.add(accountingDataParam);

        // * Path of dairy farm data (from where the exe file is)
        //TCLAP::ValueArg<std::string> dairyFarmParam("","dairyFarmDataPath","Path to the dairy farm data, default ../data/DairyFarms/",false,"../data/DairyFarms/","string");
        TCLAP::ValueArg<std::string> dairyFarmParam("", "dairyFarmDataPath", "Path to the dairy farm data, default " + TechnicalConstants::RELATIVE_DAIRY_FARM_DEFAULT_FOLDER, false, TechnicalConstants::RELATIVE_DAIRY_FARM_DEFAULT_FOLDER, "string");
        cmd.add(dairyFarmParam);

        // * Path of accounting data (from where the exe file is)
        //TCLAP::ValueArg<std::string> geneticCatalogueParam("","geneticCataloguePath","Path to the genetic catalogues, default ../data/GeneticCatalogues/",false,"../data/GeneticCatalogues/","string");
        TCLAP::ValueArg<std::string> geneticCatalogueParam("", "geneticCataloguePath", "Path to the genetic catalogues, default " + TechnicalConstants::RELATIVE_GENETIC_CATALOGUE_DATA_DEFAULT_FOLDER, false, TechnicalConstants::RELATIVE_GENETIC_CATALOGUE_DATA_DEFAULT_FOLDER, "string");
        cmd.add(geneticCatalogueParam);
        
       // * Path of management file, replacing all Farm, accounting model and protocol files (from where the exe file is)
        TCLAP::ValueArg<std::string> simulationManagementFileParam("", "simulationManagementFileName", "Name (with path) of the management file, default none", false, "", "string");
        cmd.add(simulationManagementFileParam);
        
//#else // _SENSIBILITY_ANALYSIS
//        // * File of the constant variation values (from where the exe file is)
//        TCLAP::ValueArg<std::string> constantAndDefaultValuesParam("", "constantAndDefaultValues", "File (with path) of the constant and default values, default ../data/ConstantAndDefaultValues/ModulationTable.csv", false, "../data/ConstantAndDefaultValues/ModulationTable.csv", "string");
//        cmd.add(constantAndDefaultValuesParam);
//
//         // * Name of the constant to variate
//        TCLAP::ValueArg<std::string> parameterKeyParam("", "parameterKey", "Parameter name for a specific (not fonctional) value (for example for sensibilit analysis)", false, "", "string");
//        cmd.add(parameterKeyParam);
//
//        // * Variation to apply to the concerned constant
//        TCLAP::ValueArg<std::string> parameterValueParam("", "parameterValue", "Specific value for the selected parameter name", false, "", "string");
//        cmd.add(parameterValueParam);
//        
//        TCLAP::SwitchArg maintainPreviousResultsBeforeSimulationSwitch("m", "maintain", "Maintain previous results before simulation", cmd, false);
//        
//#endif // _SENSIBILITY_ANALYSIS
        
        // * Path of protocols (from where the exe file is)
        TCLAP::ValueArg<std::string> protocolFileParam("", "protocolFileName", "Name (with path) of the protocol file, default ../data/Protocols/Protocols.csv", false, "../data/Protocols/Protocols.csv", "string");
        cmd.add(protocolFileParam);
        
        // * Path of accounting data (from where the exe file is)
#ifdef _WIN32
            char szPath[MAX_PATH] = ""; 
            SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, 0, szPath); // Get "documents" user path
            std::string strResultPath(szPath);
            strResultPath = Tools::changeChar(strResultPath, '\\','/') + "/DHM_Results/";
#else
            std::string strResultPath = TechnicalConstants::DEFAULT_RELATIVE_RESULT_FOLDER;
#endif
        std::string strPathMessage = "Path for the results, default " + strResultPath;
        TCLAP::ValueArg<std::string> resultPathParam("", "resultPath", strPathMessage, false, strResultPath, "string");
        cmd.add(resultPathParam);

        // Parse the argv array
        cmd.parse(argc, argv);
        
        // Collecting param values
        bool maintainPreviousResultsBeforeSimulation = false; // General case
//#ifndef _SENSIBILITY_ANALYSIS
        std::string accountingDataPath = accountingDataParam.getValue();
        std::string dairyFarmPath = dairyFarmParam.getValue();
        std::string geneticCataloguePath = geneticCatalogueParam.getValue();
        std::string simulationManagementFile = simulationManagementFileParam.getValue();
//#else // _SENSIBILITY_ANALYSIS
//        std::string parameterKey = parameterKeyParam.getValue();
//        std::string parameterValue = parameterValueParam.getValue();
//        maintainPreviousResultsBeforeSimulation = maintainPreviousResultsBeforeSimulationSwitch.getValue();
//#endif // _SENSIBILITY_ANALYSIS
        std::string protocolFile = protocolFileParam.getValue();
        std::string resultPath = resultPathParam.getValue();
        
        // Preparing the log folder
        std::string logPath = resultPath + TechnicalConstants::LOG_DIRECTORY;
        mkdir(logPath.c_str() DIR_RIGHT);  

        // Log file to save console     
#ifdef _LOG
        std::string appName = "dhm_log";
#elif  _MONORUN
        std::string appName = "dhm_monorun";
#elif  _SENSIBILITY_ANALYSIS
        std::string appName = "dhm_sa";
#else
        std::string appName = "dhm";
#endif
        std::string logFileName = resultPath + TechnicalConstants::LOG_DIRECTORY + TechnicalConstants::LOG_FILE_NAME + "_" + appName;
        Console::Display::init(logFileName);
        Simulator::Controler::initInstance();
//#ifndef _SENSIBILITY_ANALYSIS
        Console::Display::displayLineInConsole(R"!(+--------------------------------------------------------------------------------------------------------+)!", Console::ANSI_YELLOW);
        Console::Display::displayLineInConsole(R"!(|  _____        _              _    _            _ _   _       __  __                                    |)!", Console::ANSI_YELLOW);
        Console::Display::displayLineInConsole(R"!(| |  __ \      (_)            | |  | |          | | | | |     |  \/  |                                   |)!", Console::ANSI_YELLOW);
        Console::Display::displayLineInConsole(R"!(| | |  | | __ _ _ _ __ _   _  | |__| | ___  __ _| | |_| |__   | \  / | __ _ _ __   __ _  __ _  ___ _ __  |)!", Console::ANSI_YELLOW);
        Console::Display::displayLineInConsole(R"!(| | |  | |/ _` | | '__| | | | |  __  |/ _ \/ _` | | __| '_ \  | |\/| |/ _` | '_ \ / _` |/ _` |/ _ \ '__| |)!", Console::ANSI_YELLOW);
        Console::Display::displayLineInConsole(R"!(| | |__| | (_| | | |  | |_| | | |  | |  __/ (_| | | |_| | | | | |  | | (_| | | | | (_| | (_| |  __/ |    |)!", Console::ANSI_YELLOW);
        Console::Display::displayLineInConsole(R"!(| |_____/ \__,_|_|_|   \__, | |_|  |_|\___|\__,_|_|\__|_| |_| |_|  |_|\__,_|_| |_|\__,_|\__, |\___|_|    |)!", Console::ANSI_YELLOW);
        Console::Display::displayLineInConsole(R"!(|                       __/ |                                                            __/ |           |)!", Console::ANSI_YELLOW);
#ifdef _LOG
        Console::Display::displayLineInConsole(R"!(|                      |___/              ( L O G   o p t i o n )                       |___/            |)!", Console::ANSI_YELLOW);
#elif _FULL_RESULTS
        Console::Display::displayLineInConsole(R"!(|                      |___/     ( F u l l   r e s u l t s   o p t i o n )              |___/            |)!", Console::ANSI_YELLOW);
#elif _MONORUN
        Console::Display::displayLineInConsole(R"!(|                      |___/          ( M o n o r u n   o p t i o n )                   |___/            |)!", Console::ANSI_YELLOW);
#elif _SENSIBILITY_ANALYSIS
        Console::Display::displayLineInConsole(R"!(|                      |___/ ( S e n s i t i v i t y   a n a l y s i s   o p t i o n )  |___/            |)!", Console::ANSI_YELLOW);
#else
        Console::Display::displayLineInConsole(R"!(|                      |___/      ( K e y   r e s u l t s   o p t i o n )               |___/            |)!", Console::ANSI_YELLOW);
#endif // _LOG
        Console::Display::displayLineInConsole(R"!(+--------------------------------------------------------------------------------------------------------+)!", Console::ANSI_YELLOW);
        Console::Display::displayLineInConsole(TechnicalConstants::STR_VERSION, Console::ANSI_YELLOW);
//#endif // _SENSIBILITY_ANALYSIS
        
        
//#ifdef _SENSIBILITY_ANALYSIS
//        // Modulate the value concerned by sensibility analysis
////        Console::Display::displayLineInConsole("Simulation command: " + commandValue);
//        FunctionalConstants::Modules::modulateValue(parameterKey, parameterValue);
//        // List of farm parameters
//        std::map<std::string, std::string> mFarmCollection; // All the known farms
//#else // _SENSIBILITY_ANALYSIS
        
        unsigned int totalRunNumber = 0;

        if (simulationManagementFile == "")
        {
            // Simulation data comes from farm, accounting model and protols different files
            // -----------------------------------------------------------------------------
            
            Console::Display::displayLineInConsole("");
            Console::Display::displayLineInConsole("Dairy farm parameter loading:");
            Console::Display::displayLineInConsole("-----------------------------");

            std::map<std::string, ExchangeInfoStructures::Parameters::DairyFarmParameters> systemToSimulateParameterList;
            Simulator::Controler::getSystemToSimulateParameterList(dairyFarmPath, systemToSimulateParameterList); 

            // List of farm parameters
            std::map<std::string, std::string> mFarmCollection; // All the known farms
            for (auto farmParameter : systemToSimulateParameterList)
            {
                std::string farmName = farmParameter.first;
                std::string fileNameWithPath = dairyFarmPath + farmName + TechnicalConstants::DATA_FILE_EXTENSION;
                Console::Display::displayLineInConsole("File = " + fileNameWithPath);

                // Parametring the farm to simulate
                // --------------------------------
                ExchangeInfoStructures::Parameters::DairyFarmParameters &dairyFarmParameter = farmParameter.second; // Dairy Farm parameters
                std::string errorMessage = "";
                if (dairyFarmParameter.autoControl(errorMessage))
                {
                    // Creating the dairy farm with the current parameters
                    Console::Display::displayLineInConsole("  comment = " + dairyFarmParameter.comment);
                    Simulator::Controler::generateSystemToSimulate(dairyFarmParameter);
                    Console::Display::displayLineInConsole("-> ", Console::ANSI_RESET, false);
                    Console::Display::displayLineInConsole("ready to simulate",  Console::ANSI_GREEN);

                    // Add to collection
                    mFarmCollection[farmName] = dairyFarmParameter.comment;
                }
                else
                {
                    Console::Display::displayLineInConsole("-> Value error in farm \"" + fileNameWithPath + "\" : " + errorMessage, Console::ANSI_YELLOW);
                }
            }

            // Accounting models :
            // -------------------
            Console::Display::displayLineInConsole("");
            Console::Display::displayLineInConsole("Accounting models:");
            Console::Display::displayLineInConsole("------------------");
            std::vector<std::string> acountingModelsFileList;
            Tools::getFileNamesFromDirectory(accountingDataPath, acountingModelsFileList, (std::string&)TechnicalConstants::DATA_FILE_EXTENSION);
            // List of accounting models
//#endif // _SENSIBILITY_ANALYSIS
            std::map<std::string, std::string> accountingModelsCollection;
//#ifndef _SENSIBILITY_ANALYSIS
            for (std::vector<std::string>::iterator it = acountingModelsFileList.begin(); it != acountingModelsFileList.end(); it++)
            {
                std::string fileNameWithPath = *it;
                Console::Display::displayLineInConsole("File = " + fileNameWithPath);
                std::string accountingModelName = fileNameWithPath;
                accountingModelName = accountingModelName.erase(0, accountingDataPath.length()); // Remove the path
                accountingModelName = accountingModelName.erase(accountingModelName.length() - TechnicalConstants::DATA_FILE_EXTENSION.length(), TechnicalConstants::DATA_FILE_EXTENSION.length()); // Remove the extension
                ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters accountingModelParameters(accountingModelName, fileNameWithPath);

                // Creating a accounting model with the current parameters
                Simulator::Controler::createAccountingModel(accountingModelParameters);
                Console::Display::displayLineInConsole("-> ", Console::ANSI_RESET, false);
                Console::Display::displayLineInConsole("ready to be used",  Console::ANSI_GREEN);

                // Add to collection
                accountingModelsCollection[accountingModelName] = accountingModelParameters.comment;


            }
//#endif // _SENSIBILITY_ANALYSIS

            // Protocols :
            // -----------

            // Find protocol informations in file 
            std::vector<std::vector<std::string>> vProtocols;
            Tools::readFile(vProtocols, protocolFile, TechnicalConstants::EXPORT_SEP, "//");

            // Set the protocols
            if (vProtocols.size() > 1)
            {
                // The first line is the column header, not to consider
                vProtocols.erase(vProtocols.begin());

                // Creating the protocols
//#ifndef _SENSIBILITY_ANALYSIS
                Console::Display::displayLineInConsole("");
                Console::Display::displayLineInConsole("Protocols:");
                Console::Display::displayLineInConsole("----------");
//#endif // _SENSIBILITY_ANALYSIS
                std::map<std::string, ExchangeInfoStructures::Parameters::ProtocolParameters> createdProtocols;
#ifndef _MONORUN
                totalRunNumber = 0;
                for (unsigned int iProtocols = 0; iProtocols < vProtocols.size(); iProtocols++)
#else // _MONORUN
                totalRunNumber = 1;
                unsigned int iProtocols = 0;    
#endif // _MONORUN
                {
                    std::vector<std::string> &currentProtocol = vProtocols[iProtocols];
                    if (currentProtocol.size() >= 9) // May be a empty line in the file, ignored...
                    {
                        // Setting protocol values
                        ExchangeInfoStructures::Parameters::ProtocolParameters infoProtocol; // Default protocol parameters
                        infoProtocol.simulationDuration = std::atoi(currentProtocol[2].c_str());
                        infoProtocol.warmupDuration = std::atoi(currentProtocol[3].c_str());
                        infoProtocol.simMonth = std::atoi(currentProtocol[4].c_str());
                        infoProtocol.simYear = std::atoi(currentProtocol[5].c_str());
                        infoProtocol.runNumber = std::atoi(currentProtocol[6].c_str());
//#ifndef _SENSIBILITY_ANALYSIS
                        if (currentProtocol.size() >= 10) infoProtocol.discriminant1 = currentProtocol[9];
                        if (currentProtocol.size() >= 11) infoProtocol.discriminant2 = currentProtocol[10];
                        if (currentProtocol.size() >= 12) infoProtocol.discriminant3 = currentProtocol[11];
                        if (currentProtocol.size() == 13) infoProtocol.discriminant4 = currentProtocol[12];
//#else // _SENSIBILITY_ANALYSIS
//                        infoProtocol.discriminant1 = parameterKey;
//#endif // _SENSIBILITY_ANALYSIS

                        // Dairy farm to simulate
                        std::string farmName = currentProtocol[7];
                        std::vector<std::string> vFarmNamesForThisProtocol;

                        if (mFarmCollection.find(farmName) != mFarmCollection.end()   // Known farm
                                or farmName == "")                                  // whant to use default values
                        {
                            vFarmNamesForThisProtocol.push_back(farmName);
                        }
                        else if (farmName == TechnicalConstants::ALL_FARM_OPTION) 
                        {
                            // We want to simulate all known farms based on this protocol
                            for (auto theKnownFarm : mFarmCollection)
                            {
                                vFarmNamesForThisProtocol.push_back(theKnownFarm.first);
                            }
                        }
                        if (vFarmNamesForThisProtocol.size() == 0) 
                        {
                            Console::Display::displayLineInConsole("-> Warning for file \"" + protocolFile + "\" line " + Tools::toString(iProtocols + 1) + ", no dairy farm known for parameter \"" + currentProtocol[7] + "\", so protocol not considered", Console::ANSI_YELLOW);
                        }
                        else
                        {
                            for (auto theFarmToSimulate : vFarmNamesForThisProtocol)
                            {
                                farmName = theFarmToSimulate;
                                infoProtocol.runableName = farmName;
                                if (infoProtocol.discriminant1 == "") infoProtocol.discriminant1 = farmName;

                                // Accounting model to use :
                                std::string accountingModelName = currentProtocol[8];
                                if (accountingModelsCollection.find(accountingModelName) != accountingModelsCollection.end()   // Known accounting model
                                    or accountingModelName == "")                                                              // whant to use default values
                                {
                                    infoProtocol.name = currentProtocol[0];
                                    if (vFarmNamesForThisProtocol.size() > 1)
                                    {
                                        // Multi farm for this protocol
                                        infoProtocol.name += "_" + farmName;
                                    }
                                    if (createdProtocols.find(infoProtocol.name) == createdProtocols.end())
                                    {
                                        // Don't exists yet
                                        infoProtocol.accountingModelName = accountingModelName;
                                        std::string farmNameMessage = "";
                                        std::string dairyFarmCommentMessage = "";
                                        if (farmName == "")
                                        {
                                            farmNameMessage = "default farm parameters";
                                            dairyFarmCommentMessage = "no comment";
                                        }
                                        else
                                        {
                                            farmNameMessage = farmName;
                                            dairyFarmCommentMessage = mFarmCollection.find(farmName)->second;
                                        }
                                        std::string accountingModelNameMessage = accountingModelName;
                                        if (accountingModelNameMessage == "")
                                        {
                                            accountingModelNameMessage = "default accounting model parameters";
                                        }
                                        infoProtocol.comment = currentProtocol[1] + " for " + farmNameMessage +  " (" + dairyFarmCommentMessage + "), with accounting model " + infoProtocol.accountingModelName;

                                        // Creating a protocol with the current parameters
                                        Simulator::Controler::createProtocol(infoProtocol); 
                                        createdProtocols[infoProtocol.name] = infoProtocol;
//#ifndef _SENSIBILITY_ANALYSIS
                                        Console::Display::displayLineInConsole("Protocol \"" + infoProtocol.name + "\" (" + currentProtocol[1] + "), duration = " +  Tools::toString(infoProtocol.simulationDuration) + ", including warmup = " +  Tools::toString(infoProtocol.warmupDuration) + ", runNumber = " +  Tools::toString(infoProtocol.runNumber) + " -> ", Console::ANSI_RESET, false);
                                        Console::Display::displayLineInConsole("created", Console::ANSI_GREEN);
//#endif // _SENSIBILITY_ANALYSIS
#ifndef _MONORUN
                                        totalRunNumber += infoProtocol.runNumber;
#endif // _MONORUN
                                    }
                                    else
                                    {
                                        Console::Display::displayLineInConsole("-> Warning for file \"" + protocolFile + "\", name \"" + infoProtocol.name + "\" already exists, so protocol in boublon not considered", Console::ANSI_YELLOW);
                                    }
                                }
                                else
                                {
                                    Console::Display::displayLineInConsole("-> Warning for file \"" + protocolFile + "\" line " + Tools::toString(iProtocols + 1) + ", accounting model \"" + currentProtocol[8] + "\" doesn't exists, so protocol not considered", Console::ANSI_YELLOW);
                                }
                            }
                        }            
                    }
                    else
                    {
                        Console::Display::displayLineInConsole("-> Warning for file \"" + protocolFile + "\" line " + Tools::toString(iProtocols + 1) + " not understandable, so not considered", Console::ANSI_YELLOW);
                    }
                }
            }
            else
            {
                Console::Display::displayLineInConsole("!! no protocol available or the protocol file isn't present in the folder", Console::ANSI_RED);
            }
        }
        else
        {
            // Simulation data comes from unique file
            // --------------------------------------
            totalRunNumber = 0;

                
            // We have a simulation management file (including farm variations and protocol parameters), we import it
            std::vector<std::vector<std::string>> vSimulationManagementData;
            Tools::readFile(vSimulationManagementData, simulationManagementFile, TechnicalConstants::EXPORT_SEP);
            
            Console::Display::displayLineInConsole("");
            Console::Display::displayLineInConsole("Creating dairy farms and protocols regarding simulation management file :");
            Console::Display::displayLineInConsole("-------------------------------------------------------------------------");

            
            ExchangeInfoStructures::Parameters::ProtocolParameters protocolParameter; // Initial protocol
            ExchangeInfoStructures::Parameters::DairyFarmParameters initialDairyFarmParameter; // Initial default farm
            
            // Get data in simulation management file
            std::vector<std::vector<std::string>> vValueVariability;
            for (std::vector<std::vector<std::string>>::iterator it = vSimulationManagementData.begin(); it != vSimulationManagementData.end(); it++)
            {
                std::vector<std::string> simulationManagementData;
                std::vector<std::string> tempSimulationManagementData = *it;
                for (auto field : tempSimulationManagementData)
                {
                    if (field != "") simulationManagementData.push_back(field); // Killing empty fields
                }
                bool comment = false;
                if (simulationManagementData.size() >= 1) 
                {
                    comment = simulationManagementData[0].substr(0,2) == TechnicalConstants::COMMENT;
                }
                if (simulationManagementData.size() >= 2 and not comment)
                {
                    if (simulationManagementData[0] == TechnicalConstants::SIMULATION_DURATION_KEY)
                    {
                        protocolParameter.simulationDuration = std::atoi(simulationManagementData[1].c_str());
                    }
                    else if (simulationManagementData[0] == TechnicalConstants::WARMUP_DURATION_KEY)
                    {
                        protocolParameter.warmupDuration = std::atoi(simulationManagementData[1].c_str());
                    }
                    else if (simulationManagementData[0] == TechnicalConstants::SIMULATION_MONTH_KEY)
                    {
                        protocolParameter.simMonth = std::atoi(simulationManagementData[1].c_str());
                    }
                    else if (simulationManagementData[0] == TechnicalConstants::SIMULATION_YEAR_KEY)
                    {
                        protocolParameter.simYear = std::atoi(simulationManagementData[1].c_str());
                    }
                    else if (simulationManagementData[0] == TechnicalConstants::RUN_NUMBER_KEY)
                    {
                        protocolParameter.runNumber = std::atoi(simulationManagementData[1].c_str());
                    }
                    else if (simulationManagementData[0] == TechnicalConstants::FARM_NAME_KEY)
                    {
                        std::string farmName = simulationManagementData[1];
                        std::string fileNameWithPath = dairyFarmPath + farmName + TechnicalConstants::DATA_FILE_EXTENSION;
                        std::string geneticCataloguePath = TechnicalConstants::RELATIVE_GENETIC_CATALOGUE_DATA_DEFAULT_FOLDER;
                        ExchangeInfoStructures::Parameters::DairyFarmParameters importedDairyFarmParameter(farmName, fileNameWithPath, geneticCataloguePath); // imported dairy Farm parameters
                        initialDairyFarmParameter = importedDairyFarmParameter;
                    }
                    else if (simulationManagementData.size() == 2)
                    {
//                        std::cout << "simulationManagementData[0] = " << simulationManagementData[0] << std::endl;
                        // here, we have an unique value for a parameter, it is not a discriminent, so we just set this value
                        initialDairyFarmParameter.setImportedValue(simulationManagementData[0], simulationManagementData[1]);
                    }
                    else
                    {
//                        std::cout << "simulationManagementData.size() = " << simulationManagementData.size() << std::endl;
                         // Multi values, so discriminent
                        vValueVariability.push_back(simulationManagementData);
                    }
                }
            }
            
            // Generating farms and protocols
            std::string protocolName = "____";
            std::vector<std::vector<std::string>>::iterator itValues = vValueVariability.begin();
            std::vector<std::vector<std::string>>::iterator itEnd = vValueVariability.end();
            unsigned int totalProtocolNumer = 0;
            ExchangeInfoStructures::Parameters::DairyFarmParameters evoluatingDairyFarmParameter = initialDairyFarmParameter; // Recurcive evoluating farm parameters
            createFarmAndProtocols(itValues, itEnd, protocolParameter, initialDairyFarmParameter, evoluatingDairyFarmParameter, protocolName, 0, totalRunNumber, totalProtocolNumer);          
            
            Console::Display::displayLineInConsole("Importing file = " + simulationManagementFile);
            Console::Display::displayLineInConsole("-> ", Console::ANSI_RESET, false);
            std::string mess = "done, " + Tools::toString(totalProtocolNumer) + " farms and protocols to simulate, " + Tools::toString(totalRunNumber) + " runs to execute";
            Console::Display::displayLineInConsole(mess,  Console::ANSI_GREEN);
        }
           
#ifdef _LOG
        std::string strSav = resultPath + "//sav";
        Simulator::Controler::save(strSav);
#endif // _LOG

        // Simulation :
        // ------------
        if (totalRunNumber > 0)
        {
//#ifndef _SENSIBILITY_ANALYSIS
            std::chrono::time_point<std::chrono::system_clock> start, end;
            start = std::chrono::system_clock::now();
            Console::Display::displayLineInConsole("");
            Console::Display::displayLineInConsole("Simulation :");
            Console::Display::displayLineInConsole("------------");
//#endif // _SENSIBILITY_ANALYSIS
            Simulator::Controler::simulateAllProtocols(resultPath, maintainPreviousResultsBeforeSimulation);
//#ifndef _SENSIBILITY_ANALYSIS
            end = std::chrono::system_clock::now();
            float seconds = (std::chrono::duration_cast<std::chrono::milliseconds> (end-start).count()) / 1000.0f;
            Console::Display::displayLineInConsole("-> ", Console::ANSI_RESET, false);
            Console::Display::displayLineInConsole("success", Console::ANSI_GREEN);
            Console::Display::displayLineInConsole("  total run number = " + Tools::toString(totalRunNumber));
            Console::Display::displayLineInConsole("  simulation duration = " + Tools::toString(seconds) + " s");
            Console::Display::displayLineInConsole("  duration per run = " + Tools::toString(seconds/totalRunNumber) + " s");
            Console::Display::displayLineInConsole("  results are available in the folder \"" + resultPath + "\"");
//#endif // _SENSIBILITY_ANALYSIS
        }
        else
        {
            Console::Display::displayLineInConsole("-> No simulation to run", Console::ANSI_YELLOW);
        }
        
        // Cleaning the processus
        Simulator::Controler::leave();
//#ifndef _SENSIBILITY_ANALYSIS
#if(defined _WIN32 && !defined _LOG && !defined _MONORUN)
        Console::Display::displayLineInConsole("press enter to leave");
        std::getchar();
#endif // _WIN32 and !_LOG and !_MONORUN
//#endif // _SENSIBILITY_ANALYSIS
    }
    catch (TCLAP::ArgException &e)  // catch TCLAP arg exception
    {
        Console::Display::displayLineInConsole("error: " + e.error() + " for arg " + e.argId());
#ifdef _WIN32 
//        std::getchar();
#endif // _WIN32
        return 1;
    }
    catch (std::exception &e)  // catch any other exceptions
    {
        std::string what(e.what());
        std::string message = "error: std::exception " + what;
        Console::Display::displayLineInConsole(message);
#ifdef _WIN32 
//        std::getchar();
#endif // _WIN32
        return 1;
    }
    return 0; 
}
#endif // _DHM_SERVER

