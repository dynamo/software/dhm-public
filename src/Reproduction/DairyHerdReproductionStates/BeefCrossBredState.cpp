#include "BeefCrossBredState.h"

// project
//#include "I_DairyHerdReproductionAnimal.h"
#include "../../DataStructures/DairyCow.h"

BOOST_CLASS_EXPORT(Reproduction::DairyHerdReproductionStates::BeefCrossBredState) // Recommended (mandatory if virtual serialization)

namespace Reproduction
{
    namespace DairyHerdReproductionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyHerdReproductionState);

        IMPLEMENT_SERIALIZE_STD_METHODS(BeefCrossBredState);

        BeefCrossBredState::BeefCrossBredState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate) : DairyHerdReproductionState(beginDate
//        BeefCrossBredState::BeefCrossBredState(I_DairyHerdReproductionAnimal* pAnim, const boost::gregorian::date &beginDate) : DairyHerdReproductionState(beginDate
#ifdef _LOG
                                                                                                                                ,"Beef cross bred"
#endif // _LOG 
                                                                                                                                , pAnim)
        {
            // No predicted end date to set, duration is infinite until the farmer decision
            _predictedEndDate = boost::gregorian::date(boost::gregorian::pos_infin);
        }

        void BeefCrossBredState::progress(const boost::gregorian::date &simDate, Tools::Random &random, DairyHerdReproductionState* &pNewState)
        {
            // Nothing to do, the duration is infinite until the farmer decision
        }
        
        BeefCrossBredState::~BeefCrossBredState()
        {
        }
        
        void BeefCrossBredState::notifyMammalEnteringInNewState(const boost::gregorian::date &date)
        {
            // Base
            DairyHerdReproductionState::notifyMammalEnteringInNewState(date);
            
            // Notify mammal
            _pAppliesToDairyMammal->setSaleStatus(FunctionalEnumerations::Population::SaleStatus::beefCrossBred);
        }
    }
} /* End of namespace DataStructures::States */