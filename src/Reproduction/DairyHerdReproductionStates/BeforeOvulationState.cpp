#include "BeforeOvulationState.h"

// project
#include "OvulationState.h"
//#include "I_DairyHerdReproductionAnimal.h"
#include "../../DataStructures/DairyCow.h"

BOOST_CLASS_EXPORT(Reproduction::DairyHerdReproductionStates::BeforeOvulationState) // Recommended (mandatory if virtual serialization)


namespace Reproduction
{
    namespace DairyHerdReproductionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyHerdReproductionState); \
        ar & BOOST_SERIALIZATION_NVP(_isFirstOvulationAfterCalving); \

        IMPLEMENT_SERIALIZE_STD_METHODS(BeforeOvulationState);

        BeforeOvulationState::BeforeOvulationState(const boost::gregorian::date &beginDate, 
#ifdef _LOG
                                                                            const std::string &strState, 
#endif // _LOG 
                                                                            bool firstOvulationAfterCalving, DataStructures::DairyMammal* pAnim) : DairyHerdReproductionState(beginDate
#ifdef _LOG
                                                                                                                                , strState
#endif // _LOG 
                                                                                                                                , pAnim)
        {
            _isFirstOvulationAfterCalving = firstOvulationAfterCalving;
        }

        void BeforeOvulationState::progress(const boost::gregorian::date &simDate, Tools::Random &random, DairyHerdReproductionState* &pNewState)
        {
            // Forced ovulation ?
            DairyHerdReproductionState* pPotentialForcedOvulation = _pAppliesToDairyMammal->getForcedOvulation(simDate);
            if (pPotentialForcedOvulation != nullptr)
            {
                pNewState = pPotentialForcedOvulation;
            }
            else if (isOutOfTime(simDate))
            {
                pNewState = new OvulationState(_pAppliesToDairyMammal, simDate, nextCycleWillBeInterrupted(), _isFirstOvulationAfterCalving, false, _pAppliesToDairyMammal->getDeltaFertilityVetContract());  
            }
        }

        BeforeOvulationState::~BeforeOvulationState()
        {
        }
        
        bool BeforeOvulationState::nextCycleWillBeInterrupted()
        {
            // Generally no
            return false;
        }
    }
}
