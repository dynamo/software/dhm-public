#ifndef States_DairyHerdReproductionStates_BeforeOvulationState_h
#define States_DairyHerdReproductionStates_BeforeOvulationState_h

// project
#include "DairyHerdReproductionState.h"

namespace Reproduction
{
    namespace DairyHerdReproductionStates
    {
        class BeforeOvulationState : public DairyHerdReproductionState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        bool _isFirstOvulationAfterCalving = false;
        
        public:
            BeforeOvulationState(){};
            BeforeOvulationState(const boost::gregorian::date &beginDate
#ifdef _LOG
                                                                                        , const std::string &strState
#endif // _LOG 
                                                                                        , bool firstOvulationAfterCalving, DataStructures::DairyMammal* pAnim);
            virtual ~BeforeOvulationState();
            void progress(const boost::gregorian::date &simDate, Tools::Random &random, DairyHerdReproductionState* &pNewState);
            virtual bool nextCycleWillBeInterrupted();
        };
    }
}
#endif // States_DairyHerdReproductionStates_BeforeOvulationState_h
