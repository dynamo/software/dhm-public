#include "BetweenOvulationState.h"

// project
#include "OvulationState.h"
//#include "I_DairyHerdReproductionAnimal.h"
#include "../../DataStructures/DairyCow.h"

BOOST_CLASS_EXPORT(Reproduction::DairyHerdReproductionStates::BetweenOvulationState) // Recommended (mandatory if virtual serialization)

namespace Reproduction
{
    namespace DairyHerdReproductionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(BeforeOvulationState);

        IMPLEMENT_SERIALIZE_STD_METHODS(BetweenOvulationState);

        BetweenOvulationState::BetweenOvulationState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate, unsigned int duration
#ifdef _LOG
                                                                                                                                , const std::string &strState
#endif // _LOG 
                                                                                                                                 ) : BeforeOvulationState(beginDate
#ifdef _LOG
                                                                                                                                , strState
#endif // _LOG 
                                                                                                                                , false, pAnim)
        {
            boost::gregorian::date_duration dd(duration);
            _predictedEndDate = _beginDate + dd;
        }

        BetweenOvulationState::~BetweenOvulationState()
        {
        }
    }
} /* End of namespace DataStructures::States */
