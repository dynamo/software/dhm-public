#ifndef States_DairyHerdReproductionStates_BetweenOvulationState_h
#define States_DairyHerdReproductionStates_BetweenOvulationState_h

// project
#include "BeforeOvulationState.h"

namespace DataStructures
{
    class DairyMammal;
}

namespace Reproduction
{
    namespace DairyHerdReproductionStates
    {
        class BetweenOvulationState : public BeforeOvulationState
        {
        DECLARE_SERIALIZE_STD_METHODS;

        protected:
            void concrete(){}; // To allow instanciation

        public:
            BetweenOvulationState(){};
            BetweenOvulationState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate, unsigned int duration
#ifdef _LOG
                                                                                                                                , const std::string &strState
#endif // _LOG 
                                                                                                                                 );
            virtual ~BetweenOvulationState();
        };
    }
} /* End of namespace DataStructures::States */
#endif // States_DairyHerdReproductionStates_BetweenOvulationState_h
