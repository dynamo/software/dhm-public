#include "DairyHerdReproductionState.h"

// boost

// project
//#include "I_DairyHerdReproductionAnimal.h"
#include "../../DataStructures/DairyCow.h"

BOOST_CLASS_EXPORT(Reproduction::DairyHerdReproductionStates::DairyHerdReproductionState) // Recommended (mandatory if virtual serialization)

namespace Reproduction
{
    namespace DairyHerdReproductionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_NVP(_beginDate); \
        ar & BOOST_SERIALIZATION_NVP(_predictedEndDate); \
        ar & BOOST_SERIALIZATION_NVP(_pAppliesToDairyMammal); \
 
        IMPLEMENT_SERIALIZE_STD_METHODS(DairyHerdReproductionState);
        
        DairyHerdReproductionState::DairyHerdReproductionState(const boost::gregorian::date &beginDate, 
#ifdef _LOG
                                                                            const std::string &strState, 
#endif // _LOG 
                                                                            DataStructures::DairyMammal* pAnim)
        {
            _pAppliesToDairyMammal = (DataStructures::DairyCow*)pAnim;
            _beginDate = beginDate;
#ifdef _LOG
            _strState = strState;
#endif // _LOG 

        }
        
        bool DairyHerdReproductionState::isOutOfTime(const boost::gregorian::date &baseDate)
        {
            return baseDate >= _predictedEndDate;
        }
        
        DairyHerdReproductionState::~DairyHerdReproductionState()
        {
        }
        
        boost::gregorian::date &DairyHerdReproductionState::getBeginDate()
        {
            return _beginDate;
        }
        
        unsigned int DairyHerdReproductionState::getDayDurationUntilPredictedEndPregnancyDate(const boost::gregorian::date& date)
        {
            throw std::runtime_error("DairyHerdReproductionState::getDayDurationUntilPredictedEndPregnancyDate : not relevant state");
        }
        
        bool DairyHerdReproductionState::needTreatmentForAbnormalReproductiveSituation(ConcernedConditionType condition, const boost::gregorian::date &date, bool reproVetContract)
        {
            bool need = false;
            if (not _pAppliesToDairyMammal->isUndergoingCull() and _pAppliesToDairyMammal->isAdult())
            {
                need = (_pAppliesToDairyMammal->*condition)(date, reproVetContract);
            }
            return need;
        }
        
        bool DairyHerdReproductionState::needTreatmentForAbnormalNoOestrusDetection(const boost::gregorian::date &date, bool reproVetContract)
        {
            return needTreatmentForAbnormalReproductiveSituation(&DataStructures::DairyCow::isInAbnormalNoOestrusDetectionSituation, date, reproVetContract);
        }
        
        bool DairyHerdReproductionState::needTreatmentForAbnormalUnsuccessfulIA(const boost::gregorian::date &date, bool reproVetContract)
        {
            return needTreatmentForAbnormalReproductiveSituation(&DataStructures::DairyCow::isInAbnormalUnsuccessfulIASituation, date, reproVetContract);
        }
        
        bool DairyHerdReproductionState::needTreatmentForNegativePregnancyDiagnostic(const boost::gregorian::date &date, bool reproVetContract)
        {
            return needTreatmentForAbnormalReproductiveSituation(&DataStructures::DairyCow::isInNegativePregnancyDiagnosticSituation, date, reproVetContract);
        }
        
        void DairyHerdReproductionState::notifyMammalEnteringInNewState(const boost::gregorian::date &date)
        {
#ifdef _LOG
            _pAppliesToDairyMammal->addDairyHerdReproductionAnimalLog(date, getStrState());
#endif // _LOG 
        };
    } /* End of namespace Reproduction::DairyHerdReproductionStates */     
} /* End of namespace Reproduction */