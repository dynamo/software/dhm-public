#ifndef States_DairyHerdReproductionStates_DairyHerdReproductionState_h
#define States_DairyHerdReproductionStates_DairyHerdReproductionState_h

// boost
#include <boost/date_time/gregorian/gregorian.hpp>

// project
#include "../../Tools/Serialization.h"
#include "../../Tools/Random.h"

namespace DataStructures
{
    class DairyCow;
    class DairyMammal;
}
namespace Reproduction
{
    namespace DairyHerdReproductionStates
    {
        // class I_DairyHerdReproductionAnimal;
        
        typedef bool (DataStructures::DairyCow::* ConcernedConditionType)(const boost::gregorian::date&, bool reproVetContract);
            
        class DairyHerdReproductionState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        private:
            bool needTreatmentForAbnormalReproductiveSituation(ConcernedConditionType condition, const boost::gregorian::date &date, bool reproVetContract);

        protected:
            boost::gregorian::date _beginDate;
            boost::gregorian::date _predictedEndDate;
            //I_DairyHerdReproductionAnimal* _pAppliesToDairyMammal;
            DataStructures::DairyCow* _pAppliesToDairyMammal;
            
#ifdef _LOG
            std::string _strState = "";
#endif // _LOG 

            DairyHerdReproductionState(const boost::gregorian::date &beginDate
#ifdef _LOG
                                                                                        , const std::string &strState
#endif // _LOG 
                                                                                        , DataStructures::DairyMammal* pAnim);
            DairyHerdReproductionState(){};
            virtual void concrete() = 0; // To ensure that it will not be instantiated
            bool isOutOfTime(const boost::gregorian::date &baseDate);
        public:
            virtual ~DairyHerdReproductionState();
            virtual void progress(const boost::gregorian::date &simDate, Tools::Random &random, DairyHerdReproductionState* &pNewState) = 0;
            boost::gregorian::date &getBeginDate();
            virtual unsigned int getDayDurationUntilPredictedEndPregnancyDate(const boost::gregorian::date& date);
            virtual inline bool isPregnancy(){return false;}
            virtual inline boost::gregorian::date getPredictedEndPregnancyDate(){return boost::gregorian::date();}        
            virtual inline bool isOestrus(){return false;}
            virtual bool needTreatmentForAbnormalNoOestrusDetection(const boost::gregorian::date &date, bool reproVetContract);
            virtual bool needTreatmentForAbnormalUnsuccessfulIA(const boost::gregorian::date &date, bool reproVetContract);
            virtual bool needTreatmentForNegativePregnancyDiagnostic(const boost::gregorian::date &date, bool reproVetContract);
            virtual inline float getNegativeDGProba(){return 1.0f;}     
            virtual inline void considerKetosisCaseForDuration(
#ifdef _LOG
                                                    const boost::gregorian::date& date 
#endif // _LOG
                                                    ){}
#ifdef _LOG
            std::string inline &getStrState(){return _strState;}
#endif // _LOG 
            virtual void notifyMammalEnteringInNewState(const boost::gregorian::date &date);
        };
    }
} /* End of namespace DataStructures::States */
#endif // States_DairyHerdReproductionStates_DairyHerdReproductionState_h
