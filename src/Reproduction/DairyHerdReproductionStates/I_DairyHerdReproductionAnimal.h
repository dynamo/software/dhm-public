//#ifndef States_DairyHerdReproductionStates_IDairyHerdReproductionAnimal_h
//#define States_DairyHerdReproductionStates_IDairyHerdReproductionAnimal_h
//
//// boost
//#include <boost/date_time/gregorian/gregorian.hpp>
//
//// project
//#include "../../Tools/Serialization.h"
//#include "../../ExchangeInfoStructures/TemporalEvent.h"
//#include "../../ExchangeInfoStructures/FunctionalEnumerations.h"
//#include "OvulationState.h"
//
//namespace Reproduction
//{
//    namespace DairyHerdReproductionStates
//    {
//        class DairyHerdReproductionState;
//   
//        class I_DairyHerdReproductionAnimal
//        {
//        public:
//            
//            // virtual destructor for interface 
//            virtual ~I_DairyHerdReproductionAnimal() { }           
//            virtual unsigned int getUniqueIdForReproductionStates() = 0; // tmp
//            virtual unsigned int getDayForFirstCalvings() = 0;
//            virtual unsigned int getOvulationDuration() = 0;
//            virtual unsigned int getTheoricBetweenOvulationDuration() = 0;
//            virtual unsigned int getPostEarlyInterruptedDuration(
//#ifdef _LOG
//                                                    const boost::gregorian::date& date, 
//#endif // _LOG
//                                                    bool &interrupted) = 0;
//            virtual unsigned int getTheoricPregnancyDuration() = 0;
//            virtual unsigned int getPostPartumDuration(
//#ifdef _LOG
//                                                    const boost::gregorian::date& date, 
//#endif // _LOG
//                                                     bool ketosisPlaned, bool &interrupted) = 0;
//            
//            virtual FunctionalEnumerations::Reproduction::DairyCowParity getParity() = 0;
//            virtual void hasNewOvulation(const boost::gregorian::date &ovulationDate) = 0;
//            virtual bool goToInsemination(const boost::gregorian::date &inseminationDate, bool rightTime, float fertilityFactor) = 0;
//            virtual void enterInPregnancy(const boost::gregorian::date &beginDate) = 0;
//            virtual void notifyPredictedEndPregnancy(const boost::gregorian::date &predictedEndDate) = 0;
//            virtual void calve(const boost::gregorian::date &calvingDate, std::vector<FunctionalEnumerations::Genetic::Sex> &sexes, FunctionalEnumerations::Reproduction::EndPregnancyReason term) = 0;
//            virtual void looseEmbryo(const boost::gregorian::date &date) = 0;
//            virtual void abort(const boost::gregorian::date &date, bool increaseLactationRk) = 0;
//            virtual boost::gregorian::date_duration getDurationSinceBeginPregnancy(const boost::gregorian::date& date)=0;
//            virtual bool birthIsFemale() = 0;
//            virtual float getTwinningProba() =0;
//            virtual unsigned int getPubertyAge() = 0;
//            virtual bool isOvulationDetected(const boost::gregorian::date &simDate, bool first) = 0;
//            virtual bool isItTheDayForAnimalPlannedTemporalEvent(ExchangeInfoStructures::TemporalEvent::TemporalEventType theTypeEvent, const boost::gregorian::date &theDate) = 0; // Is the given date corresponding to a planned temporal event for a given event type ?
//            virtual bool hasToGoToReproduce(const boost::gregorian::date &simDate) = 0;
//            virtual bool hasTemporalEventPlanned(ExchangeInfoStructures::TemporalEvent::TemporalEventType theTypeEvent, const boost::gregorian::date &theDate)=0;
//            virtual void planPotentialSpecificityError(const boost::gregorian::date &date) = 0;
//            virtual bool isAdult() = 0;              
//            virtual bool isUndergoingCull() = 0;
//            virtual bool lactationIsOngoing() = 0;
//            virtual void setPeriPartumMastitisRiskPeriod(const boost::gregorian::date &date, bool calving) = 0;
//#ifdef _LOG
//            virtual void addDairyHerdReproductionAnimalLog(const boost::gregorian::date &date, const std::string &log) = 0;
//#endif // _LOG 
//            virtual void predictEndPregnancyConditions(const boost::gregorian::date &pregnancyDate, boost::gregorian::date &predictedEndPregnancyDate, FunctionalEnumerations::Reproduction::EndPregnancyReason &predictedEndPregnancyReason, std::vector<FunctionalEnumerations::Genetic::Sex> &vSexes, bool onlyForCalving) = 0;
//            virtual void belongingOvulatingGroup(bool val) = 0;
//            virtual void setSaleStatus(FunctionalEnumerations::Population::SaleStatus saleStatus) = 0;
//            virtual void resetDataDependingLastLactation() = 0; 
//            virtual void updateLastOestrusDetectionDate(const boost::gregorian::date &detectionDate) = 0;
//            virtual bool isInAbnormalNoOestrusDetectionSituation(const boost::gregorian::date &date) = 0;
//            virtual bool isInAbnormalUnsuccessfulIASituation(const boost::gregorian::date &date) = 0;
//            virtual bool isInNegativePregnancyDiagnosticSituation(const boost::gregorian::date &date) = 0;
//            virtual float getDeltaFertilityVetContract() = 0;
//            virtual Reproduction::DairyHerdReproductionStates::OvulationState* getForcedOvulation(const boost::gregorian::date &date) = 0;
//        };
//    } /* End of namespace Reproduction::DairyHerdReproductionStates */
//} /* End of namespace Reproduction */
//#endif // States_DairyHerdReproductionStates_IDairyHerdReproductionAnimal_h
