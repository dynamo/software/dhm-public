#include "MaleState.h"

// project
//#include "I_DairyHerdReproductionAnimal.h"
#include "../../DataStructures/DairyCow.h"

BOOST_CLASS_EXPORT(Reproduction::DairyHerdReproductionStates::MaleState) // Recommended (mandatory if virtual serialization)

namespace Reproduction
{
    namespace DairyHerdReproductionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyHerdReproductionState);

        IMPLEMENT_SERIALIZE_STD_METHODS(MaleState);

        MaleState::MaleState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate) : DairyHerdReproductionState(beginDate
#ifdef _LOG
                                                                                                                                ,"Male"
#endif // _LOG 
                                                                                                                                , pAnim)
        {
            // No predicted end date to set, duration is infinite until the farmer decision
            _predictedEndDate = boost::gregorian::date(boost::gregorian::pos_infin);
        }

        void MaleState::progress(const boost::gregorian::date &simDate, Tools::Random &random, DairyHerdReproductionState* &pNewState)
        {
            // Nothing to do, the duration is infinite until the farmer decision
        }
        
        MaleState::~MaleState()
        {
        }

        void MaleState::notifyMammalEnteringInNewState(const boost::gregorian::date &date)
        {
            // Base
            DairyHerdReproductionState::notifyMammalEnteringInNewState(date);
            
            // Notify mammal                       
            _pAppliesToDairyMammal->setSaleStatus(FunctionalEnumerations::Population::SaleStatus::male);
        }
    }
} /* End of namespace DataStructures::States */
