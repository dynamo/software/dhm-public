#include "OvulationState.h"

// project
#include "BetweenOvulationState.h"
#include "../../ExchangeInfoStructures/FunctionalConstants.h"
#include "PregnancyState.h"
//#include "I_DairyHerdReproductionAnimal.h"
#include "../../DataStructures/DairyCow.h"

BOOST_CLASS_EXPORT(Reproduction::DairyHerdReproductionStates::OvulationState) // Recommended (mandatory if virtual serialization)

namespace Reproduction
{
    namespace DairyHerdReproductionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyHerdReproductionState); \
        ar & BOOST_SERIALIZATION_NVP(_isFirst); \
        ar & BOOST_SERIALIZATION_NVP(_is100pcDetected); \
        ar & BOOST_SERIALIZATION_NVP(_medicFertilityDelta); \
        ar & BOOST_SERIALIZATION_NVP(_nextCycleWillBeInterrupted);

        IMPLEMENT_SERIALIZE_STD_METHODS(OvulationState);

        OvulationState::OvulationState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate, bool nextCycleWillBeInterrupted, bool isFirst, bool is100pcDetected, float medicFertilityDelta) : DairyHerdReproductionState(beginDate
#ifdef _LOG
                                                                                                                                                                                                            ,"Ovulation" 
#endif // _LOG 
                                                                                                                                                                                                            , pAnim)
        {
            _isFirst = isFirst;
            _is100pcDetected = is100pcDetected;
            _medicFertilityDelta = medicFertilityDelta;
            _nextCycleWillBeInterrupted = nextCycleWillBeInterrupted;
            boost::gregorian::date_duration dd(pAnim->getOvulationDuration());            
            _predictedEndDate = _beginDate + dd;
        }

        // Return true if the cow have been fecunded
        bool OvulationState::isFecunded(const boost::gregorian::date &simDate)
        {            
            bool isFecunded = false;
            if (_pAppliesToDairyMammal->hasToGoToReproduce(simDate))
            {
                bool b1 = _pAppliesToDairyMammal->isOvulationDetected(simDate, _isFirst);
                bool b2 = _pAppliesToDairyMammal->isItTheDayForAnimalPlannedTemporalEvent(ExchangeInfoStructures::TemporalEvent::TemporalEventType::IAMM, simDate);
                if (b1 or b2 or _is100pcDetected)
                {
                    isFecunded = _pAppliesToDairyMammal->goToInsemination(simDate, true, _medicFertilityDelta);
                }
            }
            return isFecunded;
        }

        void OvulationState::progress(const boost::gregorian::date &simDate, Tools::Random &random, DairyHerdReproductionState* &pNewState)
        {
             // Forced ovulation ?
            DairyHerdReproductionState* pPotentialForcedOvulation = _pAppliesToDairyMammal->getForcedOvulation(simDate);
            if (pPotentialForcedOvulation != nullptr)
            {
                pNewState = pPotentialForcedOvulation;
            }
            else if (isOutOfTime(simDate))
            {
                _pAppliesToDairyMammal->belongingOvulatingGroup(false);

                if (isFecunded(simDate))
                {
                    // fecundation : starting gestation
                    pNewState = new Reproduction::DairyHerdReproductionStates::PregnancyState(_pAppliesToDairyMammal, simDate, random); // Set the state                              
                }
                else
                {
                    if (_nextCycleWillBeInterrupted)
                    {
                        // Interupted cycle
                        pNewState = new Reproduction::DairyHerdReproductionStates::BetweenOvulationState(_pAppliesToDairyMammal, simDate, FunctionalConstants::Reproduction::INTERRUPTED_CYCLE_DURATION
#ifdef _LOG
                                                                                                                                                    , "Interrupted-ovulation"
#endif // _LOG 
                                                                                                                                                    ); // Set the state 
                    }
                    else
                    {
                        // normal cycle
                        pNewState = new Reproduction::DairyHerdReproductionStates::BetweenOvulationState(_pAppliesToDairyMammal, simDate, _pAppliesToDairyMammal->getTheoricBetweenOvulationDuration()
#ifdef _LOG
                                                                                                                                                    , "Between-ovulation"
#endif // _LOG 
                                                                                                                                                    ); // Set the state 
                    }
                }
            }
        }
        
        bool OvulationState::isOestrus()
        {
            return true;
        }

        OvulationState::~OvulationState()
        {
        }
        
        void OvulationState::notifyMammalEnteringInNewState(const boost::gregorian::date &date)
        {
            // Base
            DairyHerdReproductionState::notifyMammalEnteringInNewState(date);
            
            // Notify mammal
            _pAppliesToDairyMammal->hasNewOvulation(date);
            _pAppliesToDairyMammal->belongingOvulatingGroup(true);   
        }
    }
} /* End of namespace DataStructures::States */
