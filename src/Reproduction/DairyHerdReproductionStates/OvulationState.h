#ifndef _States_DairyHerdReproductionStates_OvulationState_h
#define _States_DairyHerdReproductionStates_OvulationState_h

// Project
#include "DairyHerdReproductionState.h"

namespace Reproduction
{
    namespace DairyHerdReproductionStates
    {
        class OvulationState : public DairyHerdReproductionState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        bool _isFirst = false;
        bool _is100pcDetected = false;
        float _medicFertilityDelta = 0.0f;
        bool _nextCycleWillBeInterrupted = false;
        
        private:
            bool isFecunded(const boost::gregorian::date &simDate);

    protected:
        void concrete(){}; // To allow instanciation

        public:
            OvulationState(){};
            OvulationState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate, bool nextCycleWillBeInterrupted, bool isFirst, bool is100pcDetected, float medicFertilityDelta);
            virtual ~OvulationState();
            void progress(const boost::gregorian::date &simDate, Tools::Random &random, DairyHerdReproductionState* &pNewState);
            bool isOestrus() override;
            void notifyMammalEnteringInNewState(const boost::gregorian::date &date) override;
        };
    }
} /* End of namespace DataStructures::States */
#endif // _States_DairyHerdReproductionStates_OvulationState_h
