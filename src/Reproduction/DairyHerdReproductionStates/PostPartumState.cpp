#include "PostPartumState.h"

// project
//#include "I_DairyHerdReproductionAnimal.h"
#include "../../DataStructures/DairyCow.h"

BOOST_CLASS_EXPORT(Reproduction::DairyHerdReproductionStates::PostPartumState) // Recommended (mandatory if virtual serialization)

namespace Reproduction
{
    namespace DairyHerdReproductionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(BeforeOvulationState); \
        ar & BOOST_SERIALIZATION_NVP(_nextCycleWillBeInterrupted); \
        ar & BOOST_SERIALIZATION_NVP(_fromEarlyInterrupted); \

        IMPLEMENT_SERIALIZE_STD_METHODS(PostPartumState);

        PostPartumState::PostPartumState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate, bool postEarlyInterruptedDuration)
                                                                                                                                : BeforeOvulationState(beginDate
#ifdef _LOG
                                                                                                                                , std::string("Post partum")
#endif // _LOG 
                                                                                                                                , true, pAnim)
        {
            _fromEarlyInterrupted = postEarlyInterruptedDuration;
            unsigned int duration;
            if (_fromEarlyInterrupted)
            {
                duration = _pAppliesToDairyMammal->getPostEarlyInterruptedDuration(
#ifdef _LOG
                                                    beginDate, 
#endif // _LOG
                                                    _nextCycleWillBeInterrupted);
            }
            else
            {
                duration = _pAppliesToDairyMammal->getPostPartumDuration(
#ifdef _LOG
                                                    beginDate, 
#endif // _LOG
                                                    false, _nextCycleWillBeInterrupted);
            }
            _predictedEndDate = _beginDate + boost::gregorian::date_duration(duration);
        }
        
        void PostPartumState::considerKetosisCaseForDuration(
#ifdef _LOG
                                                    const boost::gregorian::date& date 
#endif // _LOG
                                                    )
        {
            if (not _fromEarlyInterrupted)
            {
                unsigned int newDurationBecauseOfKetosis = _pAppliesToDairyMammal->getPostPartumDuration(
#ifdef _LOG
                                                    date, 
#endif // _LOG
                                                    true, _nextCycleWillBeInterrupted);
                _predictedEndDate = _beginDate + boost::gregorian::date_duration(newDurationBecauseOfKetosis);   
            }
        }

        PostPartumState::~PostPartumState()
        {
        }
        
        bool PostPartumState::nextCycleWillBeInterrupted()
        {
            return _nextCycleWillBeInterrupted;
        }

        void PostPartumState::notifyMammalEnteringInNewState(const boost::gregorian::date &date)
        {
            // Base
            DairyHerdReproductionState::notifyMammalEnteringInNewState(date);
            
            // Notify mammal
             // plann a IAMM post partum ?
            _pAppliesToDairyMammal->planPotentialSpecificityError(_beginDate);
            
            // Lactation dependant data reset
            _pAppliesToDairyMammal->resetDataDependingLastLactation();
        }
    }
} /* End of namespace DataStructures::States */
