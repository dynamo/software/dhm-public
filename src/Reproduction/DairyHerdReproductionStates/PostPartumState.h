#ifndef States_DairyHerdReproductionStates_PostPartumState_h
#define States_DairyHerdReproductionStates_PostPartumState_h

// project
#include "BeforeOvulationState.h"

namespace Reproduction
{
    namespace DairyHerdReproductionStates
    {
        class PostPartumState : public BeforeOvulationState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        bool _nextCycleWillBeInterrupted = false;
        bool _fromEarlyInterrupted = false;
        
        protected:
            void concrete(){}; // To allow instanciation

        public:
            PostPartumState(){};
            PostPartumState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate, bool postEarlyInterruptedDuration);
            virtual ~PostPartumState();
            bool nextCycleWillBeInterrupted() override;
            void considerKetosisCaseForDuration(
#ifdef _LOG
                                                    const boost::gregorian::date& date 
#endif // _LOG
                                                    ) override;
            void notifyMammalEnteringInNewState(const boost::gregorian::date &date) override;
        };
    }
} /* End of namespace DataStructures::States */
#endif // States_DairyHerdReproductionStates_PostPartumState_h
