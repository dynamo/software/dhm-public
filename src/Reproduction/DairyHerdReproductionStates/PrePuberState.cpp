#include "PrePuberState.h"

// project
#include "OvulationState.h"
//#include "I_DairyHerdReproductionAnimal.h"
#include "../../DataStructures/DairyCow.h"

BOOST_CLASS_EXPORT(Reproduction::DairyHerdReproductionStates::PrePuberState) // Recommended (mandatory if virtual serialization)

namespace Reproduction
{
    namespace DairyHerdReproductionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(BeforeOvulationState);

        IMPLEMENT_SERIALIZE_STD_METHODS(PrePuberState);

        PrePuberState::PrePuberState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate) : BeforeOvulationState(beginDate
#ifdef _LOG
                                                                                                                                ,"Pre-puber female"
#endif // _LOG 
                                                                                                                                , false, pAnim)
        { 
            boost::gregorian::date_duration dd(pAnim->getPubertyAge());      
            _predictedEndDate = _beginDate + dd;           
        }

        PrePuberState::~PrePuberState()
        {
        }
    }
} /* End of namespace DataStructures::States */
