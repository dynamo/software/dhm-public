#ifndef States_DairyHerdReproductionStates_PrePuberState_h
#define States_DairyHerdReproductionStates_PrePuberState_h

// project
#include "BeforeOvulationState.h"

namespace Reproduction
{
    namespace DairyHerdReproductionStates
    {
        class  PrePuberState : public BeforeOvulationState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        protected:
            void concrete(){}; // To allow instanciation
        public:
            PrePuberState(){};
            PrePuberState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate);
            virtual ~PrePuberState();
            inline bool needTreatmentForAbnormalNoOestrusDetection(const boost::gregorian::date &date, bool reproVetContract) override {return false;}
            inline bool needTreatmentForAbnormalUnsuccessfulIA(const boost::gregorian::date &date, bool reproVetContract) override {return false;}
            inline bool needTreatmentForNegativePregnancyDiagnostic(const boost::gregorian::date &date, bool reproVetContract) override {return false;}
        };
    }
} /* End of namespace DataStructures::States */
#endif // States_DairyHerdReproductionStates_PrePuberState_h
