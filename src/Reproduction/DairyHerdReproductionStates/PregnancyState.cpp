#include "PregnancyState.h"

// project
#include "PostPartumState.h"
//#include "I_DairyHerdReproductionAnimal.h"
#include "OvulationState.h"
#include "../../DataStructures/DairyCow.h"
#include "../../ExchangeInfoStructures/FunctionalConstants.h"

BOOST_CLASS_EXPORT(Reproduction::DairyHerdReproductionStates::PregnancyState) // Recommended (mandatory if virtual serialization)

namespace Reproduction
{
    namespace DairyHerdReproductionStates
    {
        // Macro to code statement for serialization methods 
        #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(DairyHerdReproductionState); \
        ar & BOOST_SERIALIZATION_NVP(_predictedEndPregnancyReason); \
        ar & BOOST_SERIALIZATION_NVP(_sexes); \
        ar & BOOST_SERIALIZATION_NVP(_onlyForCalving); \
        ar & BOOST_SERIALIZATION_NVP(_pregnancyDiagnosticDone); \
        ar & BOOST_SERIALIZATION_NVP(_dateForPeriPartumMastitisRiskPeriod); \

        IMPLEMENT_SERIALIZE_STD_METHODS(PregnancyState);

        PregnancyState::PregnancyState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate, Tools::Random &random, bool onlyForCalving) : DairyHerdReproductionState(beginDate
#ifdef _LOG
                                                                                                                                , "Pregnancy"
#endif // _LOG 
                                                                                                                                , pAnim)
        {    
            _onlyForCalving = onlyForCalving;
        }

        void PregnancyState::progress(const boost::gregorian::date &simDate, Tools::Random &random, DairyHerdReproductionState* &pNewState)
        {
            // Forced ovulation ?
            DairyHerdReproductionState* pPotentialForcedOvulation = _pAppliesToDairyMammal->getForcedOvulation(simDate);
            if (pPotentialForcedOvulation != nullptr)
            {
                _pAppliesToDairyMammal->looseEmbryo(simDate);
                pNewState = pPotentialForcedOvulation;
            }
            else
            {
                // Is it the begining of a peri partum mastitis risk period ?
                if (simDate == _dateForPeriPartumMastitisRiskPeriod)
                {
                    // The mastistis risk change today, it's now a peri Partum risk
                    _pAppliesToDairyMammal->setPeriPartumMastitisRiskPeriod(simDate, false);
                }
                if (isOutOfTime(simDate))
                {
                    if (_predictedEndPregnancyReason == FunctionalEnumerations::Reproduction::EndPregnancyReason::embryoLoose)
                    {
                        // Embryo loose
                        _pAppliesToDairyMammal->looseEmbryo(simDate);
                        pNewState = new PostPartumState(_pAppliesToDairyMammal, simDate, true);
                    }
                    else if (_predictedEndPregnancyReason == FunctionalEnumerations::Reproduction::EndPregnancyReason::abortion)
                    {
                        // Abortion
                        unsigned int pregnancyDuration = _pAppliesToDairyMammal->getDurationSinceBeginPregnancy(simDate).days();
                        if (pregnancyDuration < FunctionalConstants::Lactation::FIRST_DAY_FOR_ABORTION_COUNTING_LACTATION)
                        {
                            _pAppliesToDairyMammal->abort(simDate, false); // no lactation rank
                            pNewState = new PostPartumState(_pAppliesToDairyMammal, simDate, true);
                        }
                        else
                        {
                            _pAppliesToDairyMammal->abort(simDate, true); // lactation rank
                            _pAppliesToDairyMammal->setPeriPartumMastitisRiskPeriod(simDate, true); // The mastitis risk period begins
                            pNewState = new PostPartumState(_pAppliesToDairyMammal, simDate, false);
                        }
                    }
                    else if (_predictedEndPregnancyReason == FunctionalEnumerations::Reproduction::EndPregnancyReason::calving)
                    {
                        // Calving stage
                        // ---------------
                        _pAppliesToDairyMammal->calve(simDate, _sexes, _predictedEndPregnancyReason);
                        pNewState = new PostPartumState(_pAppliesToDairyMammal, simDate, false);
                    }
                    else
                    {
                        throw std::runtime_error("PregnancyState::progress : not relevant FunctionalEnumerations::Reproduction::EndPregnancyReason");
                    }
                }
            }
        }
        
        bool PregnancyState::isPregnancy()
        {
            return true;
        }

        unsigned int PregnancyState::getDayDurationUntilPredictedEndPregnancyDate(const boost::gregorian::date& date)
        {
            boost::gregorian::date_duration endPregnancyDuration(_predictedEndDate - date);
            return endPregnancyDuration.days();
        }

        PregnancyState::~PregnancyState()
        {
        }
        
        bool PregnancyState::needTreatmentForNegativePregnancyDiagnostic(const boost::gregorian::date &date, bool reproVetContract)
        {
            if (not _pregnancyDiagnosticDone)
            {
                _pregnancyDiagnosticDone = true;
                return DairyHerdReproductionState::needTreatmentForNegativePregnancyDiagnostic(date, reproVetContract);  
            }
            else
            {
                return false;
            }
        }
        
        void PregnancyState::notifyMammalEnteringInNewState(const boost::gregorian::date &date)
        {
            // Base
            DairyHerdReproductionState::notifyMammalEnteringInNewState(date);
            
            // Notify mammal
            // Creating the pregnancy
            _pAppliesToDairyMammal->enterInPregnancy(_beginDate);
            
            // predict end pregnantcy reason
            _pAppliesToDairyMammal->predictEndPregnancyConditions(_beginDate, _predictedEndDate, _predictedEndPregnancyReason, _sexes, _onlyForCalving);
            
            if (_predictedEndPregnancyReason == FunctionalEnumerations::Reproduction::EndPregnancyReason::calving)
            {
                // Determining the date for PeriPartum mastitis risk period
                _dateForPeriPartumMastitisRiskPeriod = _predictedEndDate - boost::gregorian::days(FunctionalConstants::Health::MAX_CALVING_PROXIMITY_FOR_INVOLUED_DRY_MASTITIS_RISK);
            }
        }
    }
} /* End of namespace DataStructures::States */