#ifndef States_DairyHerdReproductionStates_PregnancyState_h
#define States_DairyHerdReproductionStates_PregnancyState_h

// project
#include "DairyHerdReproductionState.h"
#include "../../Tools/Random.h"
#include "../../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "../../ExchangeInfoStructures/FunctionalConstants.h"

namespace Reproduction
{
    namespace DairyHerdReproductionStates
    {
        class PregnancyState : public DairyHerdReproductionState
        {
        DECLARE_SERIALIZE_STD_METHODS;
        
        FunctionalEnumerations::Reproduction::EndPregnancyReason _predictedEndPregnancyReason;
        std::vector<FunctionalEnumerations::Genetic::Sex> _sexes;
        boost::gregorian::date _dateForPeriPartumMastitisRiskPeriod;
        bool _onlyForCalving = false;
        bool _pregnancyDiagnosticDone = false;

        protected:
            void concrete(){}; // To allow instanciation

        public:
            PregnancyState(){};
            PregnancyState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate, Tools::Random &random, bool onlyForCalving = false);
            virtual ~PregnancyState();
            void progress(const boost::gregorian::date &simDate, Tools::Random &random, DairyHerdReproductionState* &pNewState);
            bool isPregnancy() override;
            inline boost::gregorian::date getPredictedEndPregnancyDate() override {return _predictedEndDate;}
            unsigned int getDayDurationUntilPredictedEndPregnancyDate(const boost::gregorian::date& date) override;
            inline bool needTreatmentForAbnormalNoOestrusDetection(const boost::gregorian::date &date, bool reproVetContract) override {return false;}
            inline bool needTreatmentForAbnormalUnsuccessfulIA(const boost::gregorian::date &date, bool reproVetContract) override {return false;}
            inline bool needTreatmentForNegativePregnancyDiagnostic(const boost::gregorian::date &date, bool reproVetContract) override;
            inline float getNegativeDGProba() override {return FunctionalConstants::Reproduction::SENSIBILITE_ERRORS_FOR_PREGNANCY_DIAGNOSTIC;}     
            void notifyMammalEnteringInNewState(const boost::gregorian::date &date) override;
        };
    }
} /* End of namespace DataStructures::States */
#endif // States_DairyHerdReproductionStates_PregnancyState_h
