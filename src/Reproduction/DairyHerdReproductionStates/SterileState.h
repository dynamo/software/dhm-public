#ifndef States_DairyHerdReproductionStates_SterileState_h
#define States_DairyHerdReproductionStates_SterileState_h

// project
#include "DairyHerdReproductionState.h"

namespace Reproduction 
{
    namespace DairyHerdReproductionStates
    {
        class SterileState : public DairyHerdReproductionState
        {       
            DECLARE_SERIALIZE_STD_METHODS;
        protected:
            void concrete(){}; // To allow instanciation
        public:
            SterileState(){};
            SterileState(DataStructures::DairyMammal* pAnim, const boost::gregorian::date &beginDate);
            virtual ~SterileState();
            void progress(const boost::gregorian::date &simDate, Tools::Random &random, DairyHerdReproductionState* &pNewState);
            inline bool needTreatmentForAbnormalNoOestrusDetection(const boost::gregorian::date &date, bool reproVetContract) override {return false;}
            inline bool needTreatmentForAbnormalUnsuccessfulIA(const boost::gregorian::date &date, bool reproVetContract) override {return false;}
            inline bool needTreatmentForNegativePregnancyDiagnostic(const boost::gregorian::date &date, bool reproVetContract) override {return false;}
            void notifyMammalEnteringInNewState(const boost::gregorian::date &date) override;
        };
    }
} /* End of namespace DataStructures::States */
#endif // States_DairyHerdReproductionStates_SterileState_h
