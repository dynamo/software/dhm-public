#include "ReproductionManagement.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"
#include "ExchangeInfoStructures/TreatmentTypeParameters.h"

// standard
#include <iostream> // cout

// project
#include "DairyHerdReproductionStates/OvulationState.h"
#include "../DataStructures/DairyMammal.h"

namespace Reproduction
{
    float ReproductionManagement::getMilkProductionFertilityFactor(float milkProductPeakQuantity)
    {
        float factor = 1.0f;
        if (milkProductPeakQuantity < FunctionalConstants::Reproduction::MEDIUM_PEAK_MILK_VALUE_FOR_FERTILITY_FACTOR)
        {            
            if (milkProductPeakQuantity < FunctionalConstants::Reproduction::LOW_PEAK_MILK_VALUE_FOR_FERTILITY_FACTOR)
            {
                factor = FunctionalConstants::Reproduction::FERTILITY_FACTOR_FOR_LOW_PEAK_MILK_VALUE;
            }
            else
            {
                factor = ((milkProductPeakQuantity - FunctionalConstants::Reproduction::LOW_PEAK_MILK_VALUE_FOR_FERTILITY_FACTOR)/(FunctionalConstants::Reproduction::MEDIUM_PEAK_MILK_VALUE_FOR_FERTILITY_FACTOR - FunctionalConstants::Reproduction::LOW_PEAK_MILK_VALUE_FOR_FERTILITY_FACTOR))*(1.0f - FunctionalConstants::Reproduction::FERTILITY_FACTOR_FOR_LOW_PEAK_MILK_VALUE) + FunctionalConstants::Reproduction::FERTILITY_FACTOR_FOR_LOW_PEAK_MILK_VALUE;
            }     
        }
        else if (milkProductPeakQuantity > FunctionalConstants::Reproduction::MEDIUM_PEAK_MILK_VALUE_FOR_FERTILITY_FACTOR)
        {
           if (milkProductPeakQuantity > FunctionalConstants::Reproduction::HIGH_PEAK_MILK_VALUE_FOR_FERTILITY_FACTOR)
            {
                factor = FunctionalConstants::Reproduction::FERTILITY_FACTOR_FOR_HIGH_PEAK_MILK_VALUE;
            }
            else
            {
                factor = ((milkProductPeakQuantity - FunctionalConstants::Reproduction::HIGH_PEAK_MILK_VALUE_FOR_FERTILITY_FACTOR)/(FunctionalConstants::Reproduction::MEDIUM_PEAK_MILK_VALUE_FOR_FERTILITY_FACTOR - FunctionalConstants::Reproduction::HIGH_PEAK_MILK_VALUE_FOR_FERTILITY_FACTOR))*(1.0f - FunctionalConstants::Reproduction::FERTILITY_FACTOR_FOR_HIGH_PEAK_MILK_VALUE) + FunctionalConstants::Reproduction::FERTILITY_FACTOR_FOR_HIGH_PEAK_MILK_VALUE;
            }
        }
        return factor;
    }
    
    void ReproductionManagement::getForcedOvulation(const boost::gregorian::date &date, DataStructures::DairyMammal *pMammal, ExchangeInfoStructures::Parameters::TreatmentTypeParameters &traitment)
    {
        const boost::gregorian::date ovulationDate = date + boost::gregorian::days(traitment.effectDelayMeanOrMin);
        Reproduction::DairyHerdReproductionStates::OvulationState* pOvulation = new Reproduction::DairyHerdReproductionStates::OvulationState(pMammal, ovulationDate, false, true, true, pMammal->getDeltaFertilityVetContract() + traitment.fertilityDelta);      
        pMammal->setFuturForcedOvulation(pOvulation);
    }
}