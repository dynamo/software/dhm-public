#ifndef ReproductionManagement_h
#define ReproductionManagement_h

// standard

// boost
#include <boost/date_time/gregorian/gregorian.hpp>

// project
#include "../ExchangeInfoStructures/TreatmentTypeParameters.h"

namespace DataStructures
{
    class DairyMammal;
}

namespace Reproduction
{
    class ReproductionManagement
    {
    protected:
        virtual void concrete() = 0; // To ensure that it will not be instantiated

    public:
        static float getMilkProductionFertilityFactor(float milkProductPeakQuantity);
        static void getForcedOvulation(const boost::gregorian::date &date, DataStructures::DairyMammal *pMammal, ExchangeInfoStructures::Parameters::TreatmentTypeParameters &traitment);
    };
} /* End of namespace Reproduction */
#endif // ReproductionManagement_h
