#include "DairyFarmResult.h"

// boost

// project
#include "../Tools/Tools.h"
#include "../ExchangeInfoStructures/TechnicalConstants.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"
#include "../ExchangeInfoStructures/MixtureCharacteristicParameters.h"

namespace Results
{
#ifdef _LOG
    std::map<unsigned int, std::map<unsigned int, DayAnimalLogInformations>> &DairyFarmResult::getFinalAnimalLogData()
    {
        return _mFinalAnimalLogData;
    }
#endif // _LOG 
    
    // Constructor
    DairyFarmResult::DairyFarmResult(std::string &discriminant1, std::string &discriminant2, std::string &discriminant3, std::string &discriminant4, boost::gregorian::date &beginDate, unsigned int runNumber) : Result(discriminant1, discriminant2, discriminant3, discriminant4, beginDate, runNumber)
    {
    } /* End of constructor */

    // Destructor
    DairyFarmResult::~DairyFarmResult()
    {
    } /* End of destructor */

#ifdef _LOG
//    void DairyFarmResult::resetCalibrationValues()
//    {
//        _managedHealthCostsForCalibration.clear();
//    }
//
//    std::vector<float> &DairyFarmResult::getManagedHealthCostsForCalibration()
//    {
//        return _managedHealthCostsForCalibration;
//    }
//
    // Farm Exploitation Week Informations
    std::vector<WeekAnimalPopulationInformations> &DairyFarmResult::getWeekAnimalPopulationInformations()
    {
        return _vWeekAnimalPopulationInformations;
    }

    void DairyFarmResult::toFileWeekAnimalPopulationInformations(std::vector<WeekAnimalPopulationInformations> &informations, std::string fileName)
    {
        if (informations.size()>0)
        {
            const std::string strSep = {TechnicalConstants::EXPORT_SEP};
            std::vector<std::string> linesForTheSaveFile;
            std::string currentFileLine; // data line

            // Column names
            currentFileLine = "Simulation_date" + strSep +
                              "Total_population" + strSep +
                              "Calf_population" + strSep +
                              "Heifer_population" + strSep +
                              "Adult_population" + strSep +
                              "Calve_sold_number" + strSep +
                              "Heifers_sold_number" + strSep +
                              "Adults_sold_number" + strSep +
                              "Death_number";
            // ...

            linesForTheSaveFile.push_back(currentFileLine);// Data line

            // Now, we iter all the data lines
            for (unsigned int i = 0; i < informations.size(); i++)
            {
                WeekAnimalPopulationInformations data = informations[i];

                currentFileLine = Tools::toString(data.simulationDate);
                currentFileLine += strSep + Tools::toString(data.getTotalPopulation());
                currentFileLine += strSep + Tools::toString(data.calfPopulation);
                currentFileLine += strSep + Tools::toString(data.heiferPopulation);
                currentFileLine += strSep + Tools::toString(data.adultPopulation);
                currentFileLine += strSep + Tools::toString(data.calveSold);
                currentFileLine += strSep + Tools::toString(data.heifersSold);
                currentFileLine += strSep + Tools::toString(data.adultsSold);
                currentFileLine += strSep + Tools::toString(data.deathNumber);
                // ...

                linesForTheSaveFile.push_back(currentFileLine);// Data line
            }
            Tools::writeFile(linesForTheSaveFile, fileName, true);        
        }
    }
#endif // _LOG

    ReproductionResults &DairyFarmResult::getReproductionResults()
    {
        return _reproductionResults;
    }

    LactationResults &DairyFarmResult::getLactationResults()
    {
        return _lactationResults;
    }

    HealthResults &DairyFarmResult::getHealthResults()
    {
        return _healthResults;
    }

#ifdef _LOG
    GeneticResults &DairyFarmResult::getGeneticResults()
    {
        return _geneticResults;
    }
#endif // _LOG

    PopulationResults &DairyFarmResult::getPopulationResults()
    {
        return _populationResults;
    }

#if defined _LOG || defined _FULL_RESULTS
    FeedingResults &DairyFarmResult::getFeedingResults()
    {
        return _feedingResults;
    }
#endif // _LOG || _FULL_RESULTS

    AccountingResults &DairyFarmResult::getAccountingResults()
    {
        return _accountingResults;
    }
#ifdef _LOG
    void DairyFarmResult::toFileReproductionResults(ReproductionResults &results, std::string &pathName)
    {
        // V1Age
        const std::string strSep = {TechnicalConstants::EXPORT_SEP};
        std::string columnNames = "Calving_date" + strSep + "Age" + strSep + "Id";
        generateAgeFile(results.V1Age, columnNames, std::string(pathName + TechnicalConstants::LOG_DIRECTORY + "V1Age.csv"));

        // IVC1
        columnNames = "Calving_date" + strSep + "Ovulation_date" + strSep + "IVC1" + strSep + "Id" + strSep + "Ketosis";
        generateIntervalFile(results.IVC1, columnNames, std::string(pathName + TechnicalConstants::LOG_DIRECTORY + "IVC1.csv"));

        // IVI1
        columnNames = "Calving_date" + strSep + "Insemination_date" + strSep + "Insemination_Type" + strSep + "IVI1" + strSep + "Id" + strSep + "Ketosis";
        generateInseminationCalvingIntervalFile(results.IVI1, columnNames, std::string(pathName + TechnicalConstants::LOG_DIRECTORY + "IVI1.csv"));

        // IIA1Return
        columnNames = "IA1_date" + strSep + "IA2_date" + strSep + "IIA1Return" + strSep + "Id" + strSep + "Ketosis";
        generateIntervalFile(results.IIA1Returns, columnNames, std::string(pathName + TechnicalConstants::LOG_DIRECTORY + "IIA1Returns.csv"));

        // IIA1IAF
        columnNames = "Calving_date" + strSep + "IA1_date" + strSep + "IVIAF" + strSep + "Id" + strSep + "Ketosis";
        generateIntervalFile(results.IIA1IAF, columnNames, std::string(pathName + TechnicalConstants::LOG_DIRECTORY + "IIA1IAF.csv"));

        // IVIAF
        columnNames = "Calving_date" + strSep + "Insemination_date" + strSep + "IVIAF" + strSep + "Id" + strSep + "Ketosis";
        generateIntervalFile(results.IVIAF, columnNames, std::string(pathName + TechnicalConstants::LOG_DIRECTORY + "IVIAF.csv"));

        // Insemination Result
        columnNames = "Insemination_date" + strSep + "Cow_parity" + strSep + "Insemination_Type" + strSep + "Male_breed" + strSep + "Insemination_number" + strSep + "Insemination_result" + strSep + "Id" + strSep + "Ketosis";
        generateInseminationResultsFile(results.InseminationResults, columnNames, std::string(pathName + TechnicalConstants::LOG_DIRECTORY + "InseminationResults.csv"));

        // IVV
        columnNames = "Previous_calving_date" + strSep + "Calving_date" + strSep + "IVV" + strSep + "Id" + strSep + "Ketosis";
        generateIntervalFile(results.IVV, columnNames, std::string(pathName + TechnicalConstants::LOG_DIRECTORY + "IVV.csv"));

        // Pregnancy duration
        columnNames = "Begin_date" + strSep + "End_date" + strSep + "Duration" + strSep + "Reason" + strSep + "BirthNumber";
        generateIntervalFile(results.PregnancyDurations, columnNames, std::string(pathName + TechnicalConstants::LOG_DIRECTORY + "PregnancyDurations.csv"));

        // Sex result
        columnNames = "Birth_date" + strSep + "Cow_parity" + strSep + "Female" + strSep + "Id";
        generateRatioFile(results.SexResults, columnNames, std::string(pathName + TechnicalConstants::LOG_DIRECTORY + "SexResults.csv"));
    }

    void DairyFarmResult::toFileGeneticResults(GeneticResults &results, std::string &pathName)
    {
        // none yet ...

        const std::string strSep = {TechnicalConstants::EXPORT_SEP};
        // VG results
        std::string columnNames = "Birth_date" + strSep + "Id" + strSep + "Born_in_herd" + strSep + "VG_Lait" + strSep + "VG_TB" + strSep + "VG_TP" + strSep + "VG_Fer" + strSep + "VG_MACL" + strSep + "VG_BHBlait" + strSep + "VG_RBi" + strSep + "VG_RBni";
        generateVGFile(results.FemaleVGResults, columnNames, std::string(pathName + TechnicalConstants::LOG_DIRECTORY + "CowGeneticValues.csv"), true);                                     
        columnNames = "Insemination_date" + strSep + "Breed" + strSep + "VG_Lait" + strSep + "VG_TB" + strSep + "VG_TP" + strSep + "VG_Fer" + strSep + "VG_MACL" + strSep + "VG_BHBlait" + strSep + "VG_RBi" + strSep + "VG_RBni";
        generateVGFile(results.MaleVGResults, columnNames, std::string(pathName + TechnicalConstants::LOG_DIRECTORY + "BullGeneticValues.csv"), false);                                     
    }

    void DairyFarmResult::toFileHealthResults(HealthResults &results, std::string &pathName)
    {
        // IIM
        generateIIMFile(results.IIMResults, std::string(pathName + TechnicalConstants::LOG_DIRECTORY + "IIM.csv"));

        // Ketosis
        generateKetosisFile(results.KetosisResults, std::string(pathName + TechnicalConstants::LOG_DIRECTORY + "Ketosis.csv"));

        // Lameness
        generateLamenessFile(results.NonInfectiousLamenessResults, std::string(pathName + TechnicalConstants::LOG_DIRECTORY + "NonInfectiousLameness.csv"));
        generateLamenessFile(results.InfectiousLamenessResults, std::string(pathName + TechnicalConstants::LOG_DIRECTORY + "InfectiousLameness.csv"));
    }

    void DairyFarmResult::toFilePopulationResults(PopulationResults &results, std::string &pathName)
    {
        // Nothing yet...
    }

    void DairyFarmResult::generateTransactionFile(std::vector<Transaction> &collect, std::string fileName)
    {
        const std::string strSep = {TechnicalConstants::EXPORT_SEP};
        std::string columnNames = "Transaction_date" + strSep + "Amount" + strSep + "Transaction_type";

        BEGIN_GENERATE_FILE_METHOD

            std::string currentFileLine = boost::gregorian::to_iso_extended_string(data.date);
            std::string amont = Tools::toString(data.amount);
            currentFileLine += strSep + Tools::changeChar(amont, '.', ',');
            currentFileLine += strSep + Tools::toString(data.comp);
            // ...

            currentFileLine += TechnicalConstants::EXPORT_SEP + getStrCampaign(data.date);
        END_GENERATE_FILE_METHOD
    }

    void DairyFarmResult::toFileAccountingResults(AccountingResults &results, std::string &pathName)
    {
        // Accounting acts
        generateTransactionFile(results.Transactions, std::string(pathName + TechnicalConstants::LOG_DIRECTORY + "Transactions.csv"));
    }

    void DairyFarmResult::generateAgeFile(std::vector<Age> &collect, std::string &columnNames, std::string fileName)
    {
        BEGIN_GENERATE_FILE_METHOD

            std::string currentFileLine = boost::gregorian::to_iso_extended_string(data.date);
            currentFileLine += strSep + Tools::toString(data.age);
            currentFileLine += strSep + Tools::toString(data.id);
            // ...

            currentFileLine += TechnicalConstants::EXPORT_SEP + getStrCampaign(data.date);
        END_GENERATE_FILE_METHOD
    }

    void DairyFarmResult::generateInseminationResultsFile(std::vector<InseminationResult> &collect, std::string &columnNames, std::string fileName)
    {
        BEGIN_GENERATE_FILE_METHOD

            std::string currentFileLine = boost::gregorian::to_iso_extended_string(data.date);
            currentFileLine += strSep + Tools::toString(data.parity);
            currentFileLine += strSep + Tools::toString(data.typeInsemination);
            currentFileLine += strSep + Tools::toString(data.maleBreed);
            currentFileLine += strSep + Tools::toString(data.number);
            currentFileLine += strSep + Tools::toString(data.result);
            currentFileLine += strSep + Tools::toString(data.id);
            currentFileLine += strSep + Tools::toString(data.ketosis);
            // ...

            currentFileLine += strSep + getStrCampaign(data.date);
        END_GENERATE_FILE_METHOD
    }

    void DairyFarmResult::generateDeathResultsStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated)
    {
        std::vector<DeathResult> &collect = _populationResults.DeathResults;
        std::vector<std::string> &vStreamMergedResults = mStrResults.find(TechnicalConstants::DEATH_RESULT_FILE_NAME)->second;

        BEGIN_BEGIN_GENERATE_STREAM_METHOD

        columnNames =   "Death_date" + strSep + "Age" + strSep + "Sex" + strSep + "Id" + strSep + "Born_in_herd" + strSep + "Reason";

        END_BEGIN_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS
            std::string currentStreamLine = boost::gregorian::to_iso_extended_string(data.date);
            currentStreamLine += strSep + Tools::toString(data.age);
            currentStreamLine += strSep + Tools::toString(data.sex);
            currentStreamLine += strSep + Tools::toString(data.id);
            currentStreamLine += strSep + Tools::toString(data.bornInHerd);
            currentStreamLine += strSep + Tools::toString(data.reason);
            // ...

            currentStreamLine += strSep + getStrCampaign(data.date);
        END_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS
    }

    void DairyFarmResult::generateExitResultsStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated)
    {
        std::vector<ExitResult> &collect = _populationResults.ExitResults;
        std::vector<std::string> &vStreamMergedResults = mStrResults.find(TechnicalConstants::EXIT_RESULT_FILE_NAME)->second;

        BEGIN_BEGIN_GENERATE_STREAM_METHOD

        columnNames =   "Exit_date" + strSep + "Age" + strSep + "Sex" + strSep + "Id" + strSep + "Born_in_herd" + strSep + "Reason";

        END_BEGIN_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS
            std::string currentStreamLine = boost::gregorian::to_iso_extended_string(data.date);
            currentStreamLine += strSep + Tools::toString(data.age);
            currentStreamLine += strSep + Tools::toString(data.sex);
            currentStreamLine += strSep + Tools::toString(data.id);
            currentStreamLine += strSep + Tools::toString(data.bornInHerd);
            currentStreamLine += strSep + data.reason;
            // ...

            currentStreamLine += TechnicalConstants::EXPORT_SEP + getStrCampaign(data.date);
        END_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS
    }

    void DairyFarmResult::generateRatioFile(std::vector<RatioResult> &collect, std::string &columnNames, std::string fileName)
    {
        BEGIN_GENERATE_FILE_METHOD

            std::string currentFileLine = boost::gregorian::to_iso_extended_string(data.date);
            currentFileLine += strSep + Tools::toString(data.parity);
            currentFileLine += strSep + Tools::toString(data.val);
            currentFileLine += strSep + Tools::toString(data.id);
            // ...

            currentFileLine += strSep + getStrCampaign(data.date);
        END_GENERATE_FILE_METHOD
    }

    void DairyFarmResult::generateInseminationCalvingIntervalFile(std::vector<InseminationCalvingInterval> &collect, std::string &columnNames, std::string fileName)
    {
        BEGIN_GENERATE_FILE_METHOD

            std::string currentFileLine = boost::gregorian::to_iso_extended_string(data.calvingDate);
            currentFileLine += strSep + boost::gregorian::to_iso_extended_string(data.inseminationdate);
            currentFileLine += strSep + Tools::toString(data.typeInsemination);                    
            currentFileLine += strSep + Tools::toString(data.getIntervalInDays());
            currentFileLine += strSep + Tools::toString(data.id);
            currentFileLine += strSep + Tools::toString(data.ketosis);
            // ...

            currentFileLine += strSep + getStrCampaign(data.inseminationdate);
        END_GENERATE_FILE_METHOD
    }

    void DairyFarmResult::generateIntervalFile(std::vector<Interval> &collect, std::string &columnNames, std::string fileName)
    {
        BEGIN_GENERATE_FILE_METHOD

            std::string currentFileLine = boost::gregorian::to_iso_extended_string(data.date1);
            currentFileLine += strSep + boost::gregorian::to_iso_extended_string(data.date2);
            currentFileLine += strSep + Tools::toString(data.getIntervalInDays());
            currentFileLine += strSep + Tools::toString(data.comp);
            currentFileLine += strSep + Tools::toString(data.comp2);
            // ...

            currentFileLine += strSep + getStrCampaign(data.date2);
        END_GENERATE_FILE_METHOD
    }

    void DairyFarmResult::generateVGFile(std::vector<GeneticResult> &collect, std::string &columnNames, std::string fileName, bool manageBornInHerd)
    {
        BEGIN_GENERATE_FILE_METHOD
            std::string currentFileLine = boost::gregorian::to_iso_extended_string(data.date);
            currentFileLine += strSep + Tools::toString(data.idOrBreed);
            if (manageBornInHerd) currentFileLine += strSep + Tools::toString(data.birth);
            std::string str = Tools::toString(data.VGLait);
            currentFileLine += strSep + Tools::changeChar(str, '.', ',');
            str = Tools::toString(data.VGTB);
            currentFileLine += strSep + Tools::changeChar(str, '.', ',');
            str = Tools::toString(data.VGTP);
            currentFileLine += strSep + Tools::changeChar(str, '.', ',');
            str = Tools::toString(data.VGFer);
            currentFileLine += strSep + Tools::changeChar(str, '.', ',');
            str = Tools::toString(data.VGMACL);
            currentFileLine += strSep + Tools::changeChar(str, '.', ',');
            str = Tools::toString(data.VGBHBlait);
            currentFileLine += strSep + Tools::changeChar(str, '.', ',');
            str = Tools::toString(data.VGRBi);
            currentFileLine += strSep + Tools::changeChar(str, '.', ',');
            str = Tools::toString(data.VGRBni);
            currentFileLine += strSep + Tools::changeChar(str, '.', ',');
            currentFileLine += strSep + getStrCampaign(data.date);
        END_GENERATE_FILE_METHOD
    }

    void DairyFarmResult::generateIIMFile(std::vector<IIMResult> &collect, std::string fileName)
    {
        const std::string strSep = {TechnicalConstants::EXPORT_SEP};
        std::string columnNames = "IIM_date" + strSep + "Day_of_reference_cycle" + strSep + "Quarter_number" + strSep + "Bacterium_type" + strSep + "Mastitis_severity" + strSep + "Id";

        BEGIN_GENERATE_FILE_METHOD

            std::string currentFileLine = boost::gregorian::to_iso_extended_string(data.date);
            currentFileLine += strSep + Tools::toString(data.dayOfTheReferenceCycle);                    
            currentFileLine += strSep + Tools::toString(data.dairyQuarterNumber);
            currentFileLine += strSep + Tools::toString(data.bacteriumType);
            currentFileLine += strSep + Tools::toString(data.mastitisSeverity);
            currentFileLine += strSep + Tools::toString(data.id);
            // ...

            currentFileLine += TechnicalConstants::EXPORT_SEP + getStrCampaign(data.date);
        END_GENERATE_FILE_METHOD
    }

    void DairyFarmResult::generateKetosisFile(std::vector<KetosisResult> &collect, std::string fileName)
    {
        const std::string strSep = {TechnicalConstants::EXPORT_SEP};
        std::string columnNames = "Ketosis_date" + strSep + "Ketosis_severity" + strSep + "Id";

        BEGIN_GENERATE_FILE_METHOD

            std::string currentFileLine = boost::gregorian::to_iso_extended_string(data.date);
            currentFileLine += strSep + Tools::toString(data.ketosisSeverity);
            currentFileLine += strSep + Tools::toString(data.id);
            // ...

            currentFileLine += TechnicalConstants::EXPORT_SEP + getStrCampaign(data.date);
        END_GENERATE_FILE_METHOD
    }
    
    void DairyFarmResult::generateLamenessFile(std::vector<LamenessResult> &collect, std::string fileName)
    {
        const std::string strSep = {TechnicalConstants::EXPORT_SEP};
        std::string columnNames = "Lameness_date" + strSep + "Lameness_severity" + strSep + "Cow_lactation_rank" + strSep + "Cow_lactation_stage" + strSep + "Id";

        BEGIN_GENERATE_FILE_METHOD

            std::string currentFileLine = boost::gregorian::to_iso_extended_string(data.date);
            currentFileLine += strSep + Tools::toString(data.lamenessSeverity);
            currentFileLine += strSep + Tools::toString(data.lactationRank);
            currentFileLine += strSep + Tools::toString(data.lactationStage);
            currentFileLine += strSep + Tools::toString(data.id);
            // ...

            currentFileLine += TechnicalConstants::EXPORT_SEP + getStrCampaign(data.date);
        END_GENERATE_FILE_METHOD
    }
#endif // _LOG

#if defined _LOG || defined _FULL_RESULTS
    void DairyFarmResult::generatePrimipareReproductionResultsBasedOnCalvingStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated)
    {

        std::vector<PrimipareReproductionResultBasedOnCalving> &collect = _reproductionResults.PrimipareReproductionResultsBasedOnCalving;
        std::vector<std::string> &vStreamMergedResults = mStrResults.find(TechnicalConstants::PRIMIPARE_REPRODUCTION_RESULTS_BASED_ON_CALVING_FILE_NAME)->second;

        BEGIN_BEGIN_GENERATE_STREAM_METHOD

        columnNames = "Id" + strSep + "Successful_calving_date" + strSep + "First_insemination_age" + strSep + "Successful_insemination_age" + strSep + "Calving_age" + strSep + "First_insemination_date" + strSep + "First_insemination_type" + strSep + "First_insemination_bull_breed" + strSep + "Insemination_quantity" + strSep + "Successful_insemination_date" + strSep + "Successful_insemination_number" + strSep + "Successful_insemination_type" + strSep + "Successful_insemination_bull_breed" + strSep + "First_insemination_successful_insemination_interval";

        END_BEGIN_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS
            std::string currentStreamLine = Tools::toString(data.id);            
            currentStreamLine += strSep + boost::gregorian::to_iso_extended_string(data.obtainedCalvingDate);
            currentStreamLine += strSep + Tools::toString(data.firstInseminationAge);
            currentStreamLine += strSep + Tools::toString(data.fertilizingInseminationAge);
            currentStreamLine += strSep + Tools::toString(data.obtainedCalvingAge);
            currentStreamLine += strSep + boost::gregorian::to_iso_extended_string(data.firstInseminationDate);
            currentStreamLine += strSep + Tools::toString(data.firstInseminationType);
            currentStreamLine += strSep + Tools::toString(data.firstInseminationMaleBreed);
            currentStreamLine += strSep + Tools::toString(data.inseminationQuantity);
            currentStreamLine += strSep + boost::gregorian::to_iso_extended_string(data.fertilizingInseminationDate);
            currentStreamLine += strSep + Tools::toString(data.fertilizingInseminationNumber);
            currentStreamLine += strSep + Tools::toString(data.fertilizingInseminationType);
            currentStreamLine += strSep + Tools::toString(data.fertilizingInseminationMaleBreed);
            currentStreamLine += strSep + Tools::toString(data.getFirstInseminationFertilizingInseminationInterval());
            // ...

            currentStreamLine += TechnicalConstants::EXPORT_SEP + getStrCampaign(data.obtainedCalvingDate);
        END_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS
    }

    void DairyFarmResult::generateMultipareReproductionResultsBasedOnCalvingStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated)
    {
        std::vector<MultipareReproductionResultBasedOnCalving> &collect = _reproductionResults.MultipareReproductionResultsBasedOnCalving;
        std::vector<std::string> &vStreamMergedResults = mStrResults.find(TechnicalConstants::MULTIPARE_REPRODUCTION_RESULTS_BASED_ON_CALVING_FILE_NAME)->second;

        BEGIN_BEGIN_GENERATE_STREAM_METHOD

        columnNames = "Id" + strSep + "Successful_calving_date" + strSep + "Previous_calving_date" + strSep + "Previous_successful_calving_interval" + strSep + "Previous_calving_rank" + strSep + "First_insemination_date" + strSep + "First_insemination_type" + strSep + "First_insemination_bull_breed" + strSep + "Previous_calving_first_insemination_interval" + strSep + "Insemination_quantity" + strSep + "Successful_insemination_date" + strSep + "Fertilizing_insemination_number" + strSep + "Successful_insemination_type" + strSep + "Successful_insemination_bull_breed" + strSep + "Previous_calving_successful_insemination_interval" + strSep + "First_insemination_successful_insemination_interval";

        END_BEGIN_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS
            std::string currentStreamLine = Tools::toString(data.id);
            currentStreamLine += strSep + boost::gregorian::to_iso_extended_string(data.obtainedCalvingDate);
            currentStreamLine += strSep + boost::gregorian::to_iso_extended_string(data.previousCalvingDate);
            currentStreamLine += strSep + Tools::toString(data.getPreviousObtainedCalvingInterval());
            currentStreamLine += strSep + Tools::toString(data.previousCalvingRank);
            currentStreamLine += strSep + boost::gregorian::to_iso_extended_string(data.firstInseminationDate);
            currentStreamLine += strSep + Tools::toString(data.firstInseminationType);
            currentStreamLine += strSep + Tools::toString(data.firstInseminationMaleBreed);
            currentStreamLine += strSep + Tools::toString(data.getPreviousCalvingFirstInseminationInterval());
            currentStreamLine += strSep + Tools::toString(data.inseminationQuantity);
            currentStreamLine += strSep + boost::gregorian::to_iso_extended_string(data.fertilizingInseminationDate);
            currentStreamLine += strSep + Tools::toString(data.fertilizingInseminationNumber);
            currentStreamLine += strSep + Tools::toString(data.fertilizingInseminationType);
            currentStreamLine += strSep + Tools::toString(data.fertilizingInseminationMaleBreed);
            currentStreamLine += strSep + Tools::toString(data.getPreviousCalvingFertilizingInseminationInterval());
            currentStreamLine += strSep + Tools::toString(data.getFirstInseminationFertilizingInseminationInterval());
            // ...

            currentStreamLine += TechnicalConstants::EXPORT_SEP + getStrCampaign(data.obtainedCalvingDate);
        END_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS
    }

    void DairyFarmResult::generateNullipareReproductionResultsBasedOnInseminationStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated)
    {
        std::vector<NullipareReproductionResultBasedOnInsemination> &collect = _reproductionResults.NullipareReproductionResultsBasedOnInsemination;
        std::vector<std::string> &vStreamMergedResults = mStrResults.find(TechnicalConstants::NULLIPARE_REPRODUCTION_RESULTS_BASED_ON_INSEMINATION_FILE_NAME)->second;

        BEGIN_BEGIN_GENERATE_STREAM_METHOD

        columnNames = "Id" + strSep + "First_insemination_age" + strSep + "First_insemination_date" + strSep + "Insemination_date" + strSep + "Insemination_type" + strSep + "Insemination_bull_breed" + strSep + "Insemination_quantity";

        END_BEGIN_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS
            std::string currentStreamLine = Tools::toString(data.id);
            currentStreamLine += strSep + Tools::toString(data.firstInseminationAge);
            currentStreamLine += strSep + boost::gregorian::to_iso_extended_string(data.firstInseminationDate);
            currentStreamLine += strSep + boost::gregorian::to_iso_extended_string(data.inseminationDate);
            currentStreamLine += strSep + Tools::toString(data.inseminationType);
            currentStreamLine += strSep + Tools::toString(data.inseminationMaleBreed);
            currentStreamLine += strSep + Tools::toString(data.inseminationQuantity);
            // ...

            currentStreamLine += TechnicalConstants::EXPORT_SEP + getStrCampaign(data.firstInseminationDate);
        END_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS
    }

    void DairyFarmResult::generatePrimiMultipareReproductionResultsBasedOnInseminationStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated)
    {
        std::vector<PrimiMultipareReproductionResultBasedOnInsemination> &collect = _reproductionResults.PrimiMultipareReproductionResultsBasedOnInsemination;
        std::vector<std::string> &vStreamMergedResults = mStrResults.find(TechnicalConstants::PRIMIMULTIPARE_REPRODUCTION_RESULTS_BASED_ON_INSEMINATION_FILE_NAME)->second;

        BEGIN_BEGIN_GENERATE_STREAM_METHOD

        columnNames = "Id" + strSep + "Previous_calving_date" + strSep + "Previous_calving_rank" + strSep + "First_insemination_date" + strSep + "Insemination_date" + strSep + "Insemination_type" + strSep + "Insemination_bull_breed" + strSep + "Previous_calving_first_insemination_interval" + strSep + "Insemination_quantity";

        END_BEGIN_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS
            std::string currentStreamLine = Tools::toString(data.id);
            currentStreamLine += strSep + boost::gregorian::to_iso_extended_string(data.previousCalvingDate);
            currentStreamLine += strSep + Tools::toString(data.previousCalvingRank);
            currentStreamLine += strSep + boost::gregorian::to_iso_extended_string(data.firstInseminationDate);
            currentStreamLine += strSep + boost::gregorian::to_iso_extended_string(data.inseminationDate);
            currentStreamLine += strSep + Tools::toString(data.inseminationType);
            currentStreamLine += strSep + Tools::toString(data.inseminationMaleBreed);
            currentStreamLine += strSep + Tools::toString(data.getPreviousCalvingFirstInseminationInterval());
            currentStreamLine += strSep + Tools::toString(data.inseminationQuantity);
            // ...

            currentStreamLine += TechnicalConstants::EXPORT_SEP + getStrCampaign(data.firstInseminationDate);
        END_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS  
    }
    
    void DairyFarmResult::generateMonthMilkResultsStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated, bool delivered)
    {
        std::vector<MilkResult> &collect = delivered ? _lactationResults.DeliveredMilkResults : _lactationResults.DiscardedMilkResults;
        std::vector<std::string> &vStreamMergedResults = mStrResults.find(delivered ? TechnicalConstants::DELIVERED_MILK_RESULTS_FILE_NAME : TechnicalConstants::DISCARDED_MILK_RESULTS_FILE_NAME)->second;

        BEGIN_BEGIN_GENERATE_STREAM_METHOD

        columnNames = "Date" + strSep + "Liter_Quantity";
        if (delivered)
        {
            columnNames += strSep + "Liter_TB" + strSep + "Liter_TP" + strSep + "SCC";
        }

        END_BEGIN_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS
            std::string currentStreamLine = boost::gregorian::to_iso_extended_string(data.date);
            std::string str = Tools::toString(data.literQuantity);
            currentStreamLine += strSep + Tools::changeChar(str, '.', ',');
            if (delivered)
            {
                str = Tools::toString(data.literTB);
                currentStreamLine += strSep + Tools::changeChar(str, '.', ',');
                str = Tools::toString(data.literTP);
                currentStreamLine += strSep + Tools::changeChar(str, '.', ',');
                str = Tools::toString(data.SCC);
                currentStreamLine += strSep + Tools::changeChar(str, '.', ',');
            }
            // ...

            currentStreamLine += TechnicalConstants::EXPORT_SEP + getStrCampaign(data.date);
        END_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS                  
    }
    
    // Annual mastitis results
    void DairyFarmResult::generateAnnualMastitisResultsStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated)
    {
        std::vector<AnnualMastitisResult> &collect = _healthResults.AnnualMastitisResults;
        std::vector<std::string> &vStreamMergedResults = mStrResults.find(TechnicalConstants::ANNUAL_MASTITIS_RESULTS_FILE_NAME)->second;

        BEGIN_BEGIN_GENERATE_STREAM_METHOD

        columnNames = "Herd_cow_average" + strSep + "Clinical_mastitis_per_100_cows" + strSep + "First_clinical_mastitis_per_100_cows" + strSep + "Herd_SCC" + strSep + "Under_300_SCC_per_100_controls";

        END_BEGIN_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS
            std::string currentStreamLine = Tools::toString(data.meanPresentAdultCowCount);
            currentStreamLine += strSep + Tools::toString(data.clinicalMastitisPer100cows);
            currentStreamLine += strSep + Tools::toString(data.firstClinicalMastitisPer100cows);
            std::string quantity = Tools::toString(data.herdSCC);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            currentStreamLine += strSep + Tools::toString(data.underRefSCCper100Controls);
            // ...

            currentStreamLine += TechnicalConstants::EXPORT_SEP + getStrCampaign(data.date);
        END_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS  
 
    }

    // Annual ketosis results
    void DairyFarmResult::generateAnnualKetosisResultsStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated)
    {                      
        std::vector<AnnualKetosisResult> &collect = _healthResults.AnnualKetosisResults;
        std::vector<std::string> &vStreamMergedResults = mStrResults.find(TechnicalConstants::ANNUAL_KETOSIS_RESULTS_FILE_NAME)->second;

        BEGIN_BEGIN_GENERATE_STREAM_METHOD

        columnNames = "Herd_cow_average" + strSep + "Clinical_ketosis_per_100_cows" + strSep + "First_subclinical_ketosis_per_100_cows" + strSep + "First_clinical_ketosis_per_100_cows";

        END_BEGIN_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS
            std::string currentStreamLine = Tools::toString(data.meanPresentAdultCowCount);
            currentStreamLine += strSep + Tools::toString(data.clinicalKetosisPer100cows);
            currentStreamLine += strSep + Tools::toString(data.firstSubclinicalKetosisPer100cows);
            currentStreamLine += strSep + Tools::toString(data.firstClinicalKetosisPer100cows);
            // ...

            currentStreamLine += TechnicalConstants::EXPORT_SEP + getStrCampaign(data.date);
        END_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS  
          
    }
    
    // Annual lameness results
    void DairyFarmResult::generateAnnualLamenessResultsStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated, bool infectious)
    {
        std::vector<AnnualLamenessResult> &collect = infectious ? _healthResults.AnnualInfectiousLamenessResults : _healthResults.AnnualNonInfectiousLamenessResults;
        std::vector<std::string> &vStreamMergedResults = mStrResults.find(infectious ? TechnicalConstants::ANNUAL_INFECTIOUS_LAMENESS_RESULTS_FILE_NAME : TechnicalConstants::ANNUAL_NON_INFECTIOUS_LAMENESS_RESULTS_FILE_NAME)->second;

        BEGIN_BEGIN_GENERATE_STREAM_METHOD

        columnNames = "Herd_cow_average" + strSep + "G1_lameness_per_100_cows" + strSep + "G2_lameness_per_100_cows";

        END_BEGIN_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS
            std::string currentStreamLine = Tools::toString(data.meanPresentAdultCowCount);
            currentStreamLine += strSep + Tools::toString(data.G1LamenessPer100cows);
            currentStreamLine += strSep + Tools::toString(data.G2LamenessPer100cows);
            // ...

            currentStreamLine += TechnicalConstants::EXPORT_SEP + getStrCampaign(data.date);
        END_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS                                    
    }
    
    void DairyFarmResult::getFeedingColumNames(std::string &columnNames, bool calf)
    {
        const std::string strSep = {TechnicalConstants::EXPORT_SEP};
        columnNames = "Date";
        if (calf)
        {
            columnNames += strSep + "Liter_discarded_milk" + strSep + "Liter_bulk_milk" + strSep + "Dehydrated_milk";
        }
        for (unsigned int i = 0; i <= FunctionalEnumerations::Feeding::ForageType::lastForageType; i++)
        {
            columnNames += strSep + ExchangeInfoStructures::Parameters::MixtureCharacteristicParameters::getStrForageType((FunctionalEnumerations::Feeding::ForageType)i);
        }
        for (unsigned int i = 0; i <= FunctionalEnumerations::Feeding::ConcentrateType::lastConcentrateType; i++)
        {
            columnNames += strSep + ExchangeInfoStructures::Parameters::MixtureCharacteristicParameters::getStrConcentrateType((FunctionalEnumerations::Feeding::ConcentrateType)i);
        }
        columnNames += strSep + "Minerals_and_vitamins";
    }
     
    void DairyFarmResult::generateCalfFeedingStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated)
    {
        std::vector<CalfFeedingResult> &collect = _feedingResults.calfFeedingResults;
        std::vector<std::string> &vStreamMergedResults = mStrResults.find(TechnicalConstants::CALF_FEEDING_RESULTS_FILE_NAME)->second;

        BEGIN_BEGIN_GENERATE_STREAM_METHOD

        getFeedingColumNames(columnNames, true);

        END_BEGIN_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS
            std::string currentStreamLine = boost::gregorian::to_iso_extended_string(data.date);
            std::string quantity = Tools::toString(data.discardedMilkQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            quantity = Tools::toString(data.bulkMilkQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            quantity = Tools::toString(data.dehydratedMilkQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            quantity = Tools::toString(data.maizeSilageQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            quantity = Tools::toString(data.grassQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            quantity = Tools::toString(data.hayQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            quantity = Tools::toString(data.grassSilageQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            quantity = Tools::toString(data.rapeseedQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            quantity = Tools::toString(data.sojaQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            quantity = Tools::toString(data.barleyQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            quantity = Tools::toString(data.mineralVitaminQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            // ...

            currentStreamLine += TechnicalConstants::EXPORT_SEP + getStrCampaign(data.date);
        END_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS                                    
    }

    void DairyFarmResult::generateFeedingStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated, bool heifer)
    {
        std::vector<FeedingResult> &collect = heifer ? _feedingResults.heiferFeedingResults : _feedingResults.cowFeedingResults;
        std::vector<std::string> &vStreamMergedResults = mStrResults.find(heifer ? TechnicalConstants::HEIFER_FEEDING_RESULTS_FILE_NAME : TechnicalConstants::COW_FEEDING_RESULTS_FILE_NAME)->second;

        BEGIN_BEGIN_GENERATE_STREAM_METHOD

        getFeedingColumNames(columnNames, false);

        END_BEGIN_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS
            std::string currentStreamLine = boost::gregorian::to_iso_extended_string(data.date);
            std::string quantity = Tools::toString(data.maizeSilageQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            quantity = Tools::toString(data.grassQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            quantity = Tools::toString(data.hayQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            quantity = Tools::toString(data.grassSilageQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            quantity = Tools::toString(data.rapeseedQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            quantity = Tools::toString(data.sojaQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            quantity = Tools::toString(data.barleyQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            quantity = Tools::toString(data.mineralVitaminQuantity);
            currentStreamLine += strSep + Tools::changeChar(quantity, '.', ',');
            // ...

            currentStreamLine += TechnicalConstants::EXPORT_SEP + getStrCampaign(data.date);
        END_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS                                    
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
#endif // _LOG || _FULL_RESULTS

    
    void DairyFarmResult::generateTechnicalAndEconomicalResultsStream(std::map<std::string, std::vector<std::string>>  &mStreamMergedResults, bool columnNamesAlreadyGenerated)
    {
        char decimalSeparator = ',';
        std::vector<std::string> &vStreamMergedResults = mStreamMergedResults.find(TechnicalConstants::TECHNICAL_AND_ECONOMICAL_RESULT_FILE_NAME)->second;

        const std::string strSep = {SEPARATOR};
        
        if (not columnNamesAlreadyGenerated)
        { 
            std::string columnNames = 
                    // General:
                    "Campaign"
                    + strSep + "Run_number"
                    + strSep + "Discriminant1"
//#ifndef _SENSIBILITY_ANALYSIS
#ifndef _DHM_SERVER
                    + strSep + "Discriminant2"
                    + strSep + "Discriminant3"
                    + strSep + "Discriminant4"
#endif // _DHM_SERVER
//#endif // _SENSIBILITY_ANALYSIS
                    // Reproduction:
                    + strSep + "IV_IA1" 
                    + strSep + "TRIA1" 
                    + strSep + "IVIAF" 
                    + strSep + "IVV" 
                    + strSep + "First_calving_age" 
                    + strSep + "IA1_count" 
                    + strSep + "IA_count" 
                    + strSep + "Reproduction_treatment_count" 
                    + strSep + "Reproduction_treatment_per_100_cows" 
                    + strSep + "Reproduction_culling_per_100_cows" 
                    
                    // Production:
                    + strSep + "Delivered_milk_liter_quantity"
                    + strSep + "Delivered_milk_liter_quantity_by_cow"
                    + strSep + "Produced_milk_liter_quantity_by_cow"
                    + strSep + "SCC"
                    + strSep + "TB_liter"
                    + strSep + "TP_liter"

                    // Health:
                    + strSep + "G1_Ketosis_occurency_by_cow" 
                    + strSep + "G2_Ketosis_occurency_by_cow"
                    + strSep + "Subclinical_Mastitis_occurency_by_cow"
                    + strSep + "Clinical_Mastitis_occurency_by_cow"
                    + strSep + "G1_Non_infectious_lameness_occurency_by_cow"
                    + strSep + "G2_Non_infectious_lameness_occurency_by_cow"
                    + strSep + "G1_Infectious_lameness_occurency_by_cow"
                    + strSep + "G2_Infectious_lameness_occurency_by_cow"
                    + strSep + "Mastitis_mortality_by_cow"
                    + strSep + "Ketosis_mortality_by_cow"
                    + strSep + "Non_infectious_lameness_mortality_by_cow"
                    + strSep + "Infectious_lameness_mortality_by_cow"
                    + strSep + "Health_treatment_count"
                    + strSep + "Curative_clinical_ketosis_vet_treatment_count"
                    + strSep + "Curative_clinical_ketosis_farmer_treatment_count"
                    + strSep + "Curative_subclinical_ketosis_vet_treatment_count"
                    + strSep + "Curative_subclinical_ketosis_farmer_treatment_count"
                    + strSep + "Curative_clinical_mastitis_vet_treatment_count"
                    + strSep + "Curative_clinical_mastitis_farmer_treatment_count"
                    + strSep + "Curative_subclinical_mastitis_vet_treatment_count"
                    + strSep + "Curative_subclinical_mastitis_farmer_treatment_count"
                    //+ strSep + "Preventive_ketosis_vet_treatment_count"
                    + strSep + "Preventive_ketosis_farmer_treatment_count"
                    //+ strSep + "Preventive_mastitis_vet_treatment_count"
                    + strSep + "Preventive_Mastitis_farmer_treatment_count"
                    + strSep + "Curative_clinical_lameness_vet_treatment_count"
                    + strSep + "Preventive_lameness_vet_treatment_count"
                    + strSep + "Unscheduled_cow_vet_care_count"
                    + strSep + "Scheduled_cow_farmer_care_count"
                    + strSep + "Unscheduled_cow_farmer_care_count"
                    + strSep + "Scheduled_vet_activity_count"
                    + strSep + "Unscheduled_vet_activity_count"
                    + strSep + "Scheduled_farmer_activity_count"
                    + strSep + "Unscheduled_farmer_activity_count"
                    + strSep + "Mastitis_culling_by_cow"
                    + strSep + "Ketosis_culling_by_cow"
                    + strSep + "Non_infectious_lameness_culling_by_cow"
                    + strSep + "Infectious_lameness_culling_by_cow"
                    
                    // Population:
                    + strSep + "Herd_cow_average"
                    + strSep + "Birth_count"
                    + strSep + "Male_sale_count"
                    + strSep + "Beef_crossed_bred_sale_count"
                    + strSep + "Sterile_female_sale_count"
                    + strSep + "Extra_female_sale_count"
                    + strSep + "Extra_pregnant_heifer_sale_count"
                    + strSep + "Heifer_bought_count"
                    + strSep + "Primipare_count"
                    + strSep + "Dead_calve_count"
                    + strSep + "Dead_calve_per_100_calves"
                    + strSep + "Dead_heifer_count"
                    + strSep + "Dead_adult_count"
                    + strSep + "Dead_adult_per_100_cows"
                    + strSep + "Infertility_not_bred_decision_count"
                    + strSep + "Lactation_rank_not_bred_decision_count"
                    + strSep + "Low_milk_product_not_bred_decision_count"
                    + strSep + "Culling_percent"
                    + strSep + "Health_culling_count"
                    + strSep + "Infertility_culling_count"
                    + strSep + "Lactation_rank_culling_count"
                    + strSep + "Low_milk_product_culling_count"
                    + strSep + "Longevity"
                    + strSep + "Time_between_decision_and_culling_average"
                    + strSep + "Lactation_stage_at_culling_average"
                    + strSep + "Day_milk_quantity_at_culling_average"
                    + strSep + "Max_milk_quantity_at_culling"
                    + strSep + "Min_lactation_stage_at_culling"
                    
                    // Accounting:
                    + strSep + "Gross_margin"
                    + strSep + "Total_products"
                    + strSep + "Milk_sales"
                    + strSep + "Cull_sales"
                    + strSep + "Heifer_sales"
                    + strSep + "Calf_sales"
                    + strSep + "Total_expenses"
                    + strSep + "Feed_costs"
                    + strSep + "Health_costs"
                    + strSep + "Mastitis_health_costs"
                    + strSep + "Ketosis_health_costs"
                    + strSep + "Curative_individual_infectious_lameness_costs"
                    + strSep + "Curative_individual_non_infectious_lameness_costs"
                    + strSep + "Curative_collective_infectious_lameness_costs"
                    + strSep + "Curative_collective_non_infectious_lameness_costs"
                    + strSep + "Curative_trimming_lameness_costs"
                    + strSep + "Overall_curative_lameness_costs"
                    + strSep + "Preventive_trimming_lameness_costs"
                    + strSep + "Foot_bath_lameness_costs"
                    + strSep + "Other_health_costs"
                    + strSep + "Reproduction_costs"
                    + strSep + "Population_costs"
                    + strSep + "Other_costs"
//#ifdef _SENSIBILITY_ANALYSIS
//                    // Sensitivity analysis:
//                    + strSep + "PUBERTY_MEAN_AGE" 
//                    + strSep + "FACTOR_2_CONCOMITANT_OESTRUS" 
//                    + strSep + "FACTOR_3_AND_MORE_CONCOMITANT_OESTRUS" 
//                    + strSep + "DEFAULT_SLIPPERY_FLOOR_VALUE"
//#endif // _SENSIBILITY_ANALYSIS
                    ;
            // Line storage
            vStreamMergedResults.push_back(columnNames);
        }
        
        std::vector<TechnicalReproductionResult>::iterator itReproCollect = _reproductionResults.TechnicalReproductionResults.begin();
        std::vector<TechnicalProductionResult>::iterator itProdCollect = _lactationResults.TechnicalProductionResults.begin();
        std::map<std::string,TechnicalHealthResult>::iterator itHealthCollect = _healthResults.TechnicalHealthResults.begin();
        std::vector<TechnicalPopulationResult>::iterator itPopCollect = _populationResults.TechnicalPopulationResults.begin();
        std::vector<EconomicalResult>::iterator itEcoCollect = _accountingResults.EconomicalResults.begin();
        
        while (itReproCollect != _reproductionResults.TechnicalReproductionResults.end()) // The other collections have the same data line number
        {
            std::string strValue = "";
            std::string currentStreamLine = "";

            // General:
            currentStreamLine += getStrCampaign((*itReproCollect).date);
            currentStreamLine += strSep + Tools::toString(_runNumber);
            currentStreamLine += strSep + _discriminant1;
//#ifndef _SENSIBILITY_ANALYSIS
#ifndef _DHM_SERVER
            currentStreamLine += strSep + _discriminant2;
            currentStreamLine += strSep + _discriminant3;
            currentStreamLine += strSep + _discriminant4;
#endif // _DHM_SERVER
//#endif // _SENSIBILITY_ANALYSIS

            // Reproduction:
            strValue = Tools::toString((*itReproCollect).meanIVIA1);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itReproCollect).succesfulIA1Percent);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itReproCollect).meanIVIAF);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itReproCollect).meanIVV);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itReproCollect).meanFirstCalvingAge);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            currentStreamLine += strSep + Tools::toString((*itReproCollect).nbIA1);
            currentStreamLine += strSep + Tools::toString((*itReproCollect).nbIA);
            currentStreamLine += strSep + Tools::toString((*itReproCollect).reproductionTroubleTreatmentCount);
            strValue = Tools::toString((*itReproCollect).reproductionTroubleTreatmentPercent);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itReproCollect).reproductionTroubleCullingPercent);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            
            // Production:
            strValue = Tools::toString((*itProdCollect).deliveredMilkLiterQuantity);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itProdCollect).deliveredMilkLiterQuantityByCow);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itProdCollect).productedMilkLiterQuantityByCow);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itProdCollect).SCC);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itProdCollect).TB);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itProdCollect).TP);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);

            // Health:
            strValue = Tools::toString(itHealthCollect->second.G1ketosisOccurencyByCalvingCow);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(itHealthCollect->second.G2ketosisOccurencyByCalvingCow);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(itHealthCollect->second.subclinicalMastitisOccurencyByCow);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(itHealthCollect->second.clinicalMastitisOccurencyByCow);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(itHealthCollect->second.G1NonInfectiousLamenessOccurencyByCow);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(itHealthCollect->second.G2NonInfectiousLamenessOccurencyByCow);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(itHealthCollect->second.G1InfectiousLamenessOccurencyByCow);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(itHealthCollect->second.G2InfectiousLamenessOccurencyByCow);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(itHealthCollect->second.mastitisMortalityByCow);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(itHealthCollect->second.ketosisMortalityByCow);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(itHealthCollect->second.nonInfectiousLamenessMortalityByCow);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(itHealthCollect->second.infectiousLamenessMortalityByCow);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.totalHealthTreatmentCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.curativeClinicalKetosisVetTreatmentCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.curativeClinicalKetosisFarmerTreatmentCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.curativeSubclinicalKetosisVetTreatmentCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.curativeSubclinicalKetosisFarmerTreatmentCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.curativeClinicalMastitisVetTreatmentCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.curativeClinicalMastitisFarmerTreatmentCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.curativeSubclinicalMastitisVetTreatmentCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.curativeSubclinicalMastitisFarmerTreatmentCount);
            //currentStreamLine += strSep + Tools::toString(itHealthCollect->second.preventiveKetosisVetTreatmentCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.preventiveKetosisFarmerTreatmentCount);
            //currentStreamLine += strSep + Tools::toString(itHealthCollect->second.preventiveMastitisVetTreatmentCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.preventiveMastitisFarmerTreatmentCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.curativeClinicalLamenessVetTreatmentCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.preventiveLamenessVetTreatmentCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.unscheduledCowVetCareCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.scheduledCowFarmerCareCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.unscheduledCowFarmerCareCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.scheduledVetActivityCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.unscheduledVetActivityCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.scheduledFarmerActivityCount);
            currentStreamLine += strSep + Tools::toString(itHealthCollect->second.unscheduledFarmerActivityCount);
            strValue = Tools::toString(itHealthCollect->second.mastitisTroubleCullingPercent);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(itHealthCollect->second.ketosisTroubleCullingPercent);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(itHealthCollect->second.nonInfectiousLamenessTroubleCullingPercent);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(itHealthCollect->second.infectiousLamenessTroubleCullingPercent);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);

            // Population:
            currentStreamLine += strSep + Tools::toString((*itPopCollect).meanPresentAdultCowCount);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).birthCount);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).malesSaleCount);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).beefCrossBredSaleCount);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).sterileFemalesSaleCount);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).extraFemalesSaleCount);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).extraPregnantHeifersSaleCount);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).heiferBoughtCount);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).primipareInHerdCount);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).deathCalveCount);
            strValue = Tools::toString((*itPopCollect).deathCalvePer100Calves);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).deathHeiferCount);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).deathAdultCount);                
            strValue = Tools::toString((*itPopCollect).deathAdultPer100Cows);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).notBredBecauseOfInfertilityCount);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).notBredBecauseOfLactationRankCount);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).notBredBecauseOfLowMilkProductCount);
            strValue = Tools::toString((*itPopCollect).cullingRate);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).healthCulledAnimalCount);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).infertilityCulledAnimalCount);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).lactationRankCulledAnimalCount);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).lowMilkProductCulledAnimalCount);
            strValue = Tools::toString((*itPopCollect).longevity);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).meanDaysBetweenDecisionAndCulling);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).meanLactationStageAtCulling);
            strValue = Tools::toString((*itPopCollect).meanMilkQuantityOfTheDayAtCulling);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itPopCollect).maxMilkQuantityOfTheDayAtCulling);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            currentStreamLine += strSep + Tools::toString((*itPopCollect).minLactationStageAtCulling);

            // Accounting:
            strValue = Tools::toString((*itEcoCollect).grossMargin);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).totalProducts);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).milkSales);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).cullSales);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).heiferSales);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).calfSales);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).totalExpenses);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).feedCosts);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).healthCosts);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).mastitisHealthCosts);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).ketosisHealthCosts);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).curativeIndividualInfectiousLamenessCosts);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).curativeIndividualNonInfectiousLamenessCosts);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).curativeCollectiveInfectiousLamenessCosts);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).curativeCollectiveNonInfectiousLamenessCosts);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).curativeTrimmingCosts);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).overallCurativeLamenessCosts);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).preventiveTrimmingCosts);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).footBathCosts);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).otherHealthCosts);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).reproductionCosts);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).populationCosts);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString((*itEcoCollect).otherCosts);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);

            // ...

//#ifdef _SENSIBILITY_ANALYSIS
//            // Sensitivity analysis:
//            currentStreamLine += strSep + Tools::toString(FunctionalConstants::Reproduction::PUBERTY_MEAN_AGE);
//            strValue = Tools::toString(FunctionalConstants::Reproduction::FACTOR_2_CONCOMITANT_OESTRUS);
//            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
//            strValue = Tools::toString(FunctionalConstants::Reproduction::FACTOR_3_AND_MORE_CONCOMITANT_OESTRUS);
//            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
//            currentStreamLine += strSep + Tools::toString(FunctionalConstants::Reproduction::DEFAULT_SLIPPERY_FLOOR_VALUE);
//#endif // _SENSIBILITY_ANALYSIS
            
            // Line storage
            vStreamMergedResults.push_back(currentStreamLine);
            
            // Next result line data
            itReproCollect++;
            itProdCollect++;
            itHealthCollect++;
            itPopCollect++;
            itEcoCollect++;
        }
    }

    void DairyFarmResult::generateMilkControlResultsStream(std::map<std::string, std::vector<std::string>>  &mStreamMergedResults, bool columnNamesAlreadyGenerated)
    {
        char decimalSeparator = ',';
        std::vector<MilkControlResult> &collect = _lactationResults.MilkControlResults;
        std::vector<std::string> &vStreamMergedResults = mStreamMergedResults.find(TechnicalConstants::MILK_CONTROL_RESULT_FILE_NAME)->second;
        
        const std::string strSep = {SEPARATOR};

#ifdef _LOG
        const unsigned int CLASS_NUMBER = 5;
#endif // _LOG

        if (not columnNamesAlreadyGenerated)
        { 
            std::string columnNames = 
                    // General:
                    "Campaign"
                    + strSep + "Run_number"
                    + strSep + "Discriminant1"
//#ifndef _SENSIBILITY_ANALYSIS
#ifndef _DHM_SERVER
                    + strSep + "Discriminant2"
                    + strSep + "Discriminant3"
                    + strSep + "Discriminant4"
#endif // _DHM_SERVER
//#endif // _SENSIBILITY_ANALYSIS

                    + strSep + "Date"
                    + strSep + "Herd_cow_count"
                    + strSep + "Milking_cow_count"
                    + strSep + "Average_milk_quantity_cow"
                    + strSep + "Average_lactation_stage"
                    + strSep + "Average_FC"
                    + strSep + "Average_PC"
                    + strSep + "Average_SCC"
                    + strSep + "SCC_" + Tools::toString(FunctionalConstants::Lactation::SCC_REFERENCE_FOR_MILK_CONTROL) + "_prevalence"
                    + strSep + "SCC_" + Tools::toString(FunctionalConstants::Lactation::SCC_REFERENCE_FOR_MILK_CONTROL) + "_incidence"
                    + strSep + "Detected_infectious_lameness_percent"
                    + strSep + "Infectious_G1_lameness_percent"
                    + strSep + "Infectious_G2_lameness_percent"
                    + strSep + "Detected_non_infectious_lameness_percent"
                    + strSep + "Non_infectious_G1_lameness_percent"
                    + strSep + "Non_infectious_G2_lameness_percent"
                    ;
        
#ifdef _LOG
            // Ketosis calibration data
            for (unsigned int data_class = 0; data_class < CLASS_NUMBER; data_class++)
            {
                std::string prefix = "";
                if (data_class == 0)
                {
                    prefix = "Controled_cow_range";
                }
                else if (data_class == 1)
                {
                    prefix = "G1ketosisPrevalence_range";
                }
                else if (data_class == 2)
                {
                    prefix = "G2ketosisPrevalence_range";
                }
                else if (data_class == 3)
                {
                    prefix = "G1ketosisIncidence_range";
                }
                else if (data_class == 4)
                {
                    prefix = "G2ketosisIncidence_range";
                }
                for (unsigned int i = 0; i < FunctionalConstants::Health::DAY_RANGES_FOR_KETOSIS_CALIBRATION.size(); i ++)
                {
                    std::string previousRange;
                    if (i == 0)
                    {
                        // no previous range
                        previousRange = "0";
                    }
                    else
                    {
                        // previous range
                        previousRange = Tools::toString(FunctionalConstants::Health::DAY_RANGES_FOR_KETOSIS_CALIBRATION[i-1]+1);
                    }
                    std::string dataName = strSep + prefix + previousRange + "to" + Tools::toString(FunctionalConstants::Health::DAY_RANGES_FOR_KETOSIS_CALIBRATION[i]) + "_days";
                    columnNames += dataName;
                }
            }
#endif // _LOG
            vStreamMergedResults.push_back(columnNames);
        }
        
        for (auto data : collect)
        {
            std::string strValue = "";
            std::string currentStreamLine = "";

            // General:
            currentStreamLine += getStrCampaign(data.date);
            currentStreamLine += strSep + Tools::toString(_runNumber);
            currentStreamLine += strSep + _discriminant1;
//#ifndef _SENSIBILITY_ANALYSIS
#ifndef _DHM_SERVER
            currentStreamLine += strSep + _discriminant2;
            currentStreamLine += strSep + _discriminant3;
            currentStreamLine += strSep + _discriminant4;
#endif // _DHM_SERVER
//#endif // _SENSIBILITY_ANALYSIS

            currentStreamLine += strSep + boost::gregorian::to_iso_extended_string(data.date);
            currentStreamLine += strSep + Tools::toString(data.presentAdultCowCount);
            currentStreamLine += strSep + Tools::toString(data.presentMilkingCowCount);
            strValue = Tools::toString(data.meanGrossMilkProductOfTheControledDay);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            currentStreamLine += strSep + Tools::toString(data.meanLactationStage);
            strValue = Tools::toString(data.milkTBOfTheControledDay);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(data.milkTPOfTheControledDay);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(data.milkSCCOfTheControledDay);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(data.SCCPrevalenceRatio);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(data.SCCIncidence);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(data.detectedInfectiousLamenessPercent);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(data.infectiousG1LamenessPercent);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(data.infectiousG2LamenessPercent);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(data.detectedNonInfectiousLamenessPercent);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(data.nonInfectiousG1LamenessPercent);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
            strValue = Tools::toString(data.nonInfectiousG2LamenessPercent);
            currentStreamLine += strSep + Tools::changeChar(strValue, '.', decimalSeparator);
#ifdef _LOG
            // Ketosis calibration data
            for (unsigned int data_class = 0; data_class < CLASS_NUMBER; data_class++)
            {
                std::vector<unsigned int> *pKetosisDataToGet = nullptr;
                if (data_class == 0)
                {
                    pKetosisDataToGet = &(data.concernedCowCount);
                }
                if (data_class == 1)
                {
                    pKetosisDataToGet = &(data.G1ketosisPrevalence);
                }
                else if (data_class == 2)
                {
                    pKetosisDataToGet = &(data.G2ketosisPrevalence);
                }
                else if (data_class == 3)
                {
                    pKetosisDataToGet = &(data.G1ketosisIncidence);
                }
                else if (data_class == 4)
                {
                    pKetosisDataToGet = &(data.G2ketosisIncidence);
                }
                for (unsigned int daysInd = 0; daysInd < FunctionalConstants::Health::DAY_RANGES_FOR_KETOSIS_CALIBRATION.size(); daysInd ++)
                {
                    currentStreamLine += strSep + Tools::toString((*pKetosisDataToGet)[daysInd]);
                }
            }
#endif // _LOG 
           // ...
            vStreamMergedResults.push_back(currentStreamLine); 
        } 
    }

#ifdef _LOG

    // save all the results
    void DairyFarmResult::runResultsToFile(const std::string &path)
    {
        // Make the sub directories
        std::string subRun = path + "Run" + Tools::toString(_runNumber) + '/';
        mkdir(subRun.c_str() DIR_RIGHT);           

        Result::runResultsToFile(subRun);

        std::string logPath = subRun + TechnicalConstants::LOG_DIRECTORY;
        mkdir(logPath.c_str() DIR_RIGHT);  

        // -------------------
        // Debug results
        // -------------------
        toFileGeneticResults(_geneticResults, subRun);         

        // Week results
        // ------------
        toFileWeekAnimalPopulationInformations(_vWeekAnimalPopulationInformations, logPath + "WeekAnimalPopulationInformations.csv");
        // --------

        // ----------------------
        // Dairy Cow Informations
        // ----------------------
        toFileReproductionResults(_reproductionResults, subRun);
        toFileHealthResults(_healthResults, subRun);         
        toFilePopulationResults(_populationResults, subRun);         
        toFileAccountingResults(_accountingResults, subRun);         

        // ----------
        // Global log
        // ----------
        // Animal log
        std::string animalLogPath = subRun + TechnicalConstants::ANIMAL_LOG_DIRECTORY;
        mkdir(animalLogPath.c_str() DIR_RIGHT);
        for (std::map<unsigned int, std::map<unsigned int, DayAnimalLogInformations>>::iterator itId = _mFinalAnimalLogData.begin(); itId != _mFinalAnimalLogData.end(); itId++)
        {
            std::string fileName = animalLogPath + Tools::toString(itId->first) + "_AnimalDataLog.csv";
            remove(fileName.c_str());

            // First title line
            std::vector<std::string> linesForTheSaveFile;
            linesForTheSaveFile.push_back(DayAnimalLogInformations::getTitle());

            // Now, we iter all the animal logs
            for (std::map<unsigned int, DayAnimalLogInformations>::iterator itAge = (itId->second).begin(); itAge != (itId->second).end(); itAge++)
            {
                linesForTheSaveFile.push_back((itAge->second).getDataLine());// Data line
            }
            Tools::writeFile(linesForTheSaveFile, fileName, true);        
        }
    }
#endif // _LOG 

    void DairyFarmResult::getResultStreams(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated)
    {
        generateTechnicalAndEconomicalResultsStream(mStrResults, columnNamesAlreadyGenerated); 
        generateMilkControlResultsStream(mStrResults, columnNamesAlreadyGenerated);     
#ifdef _LOG
        generateDeathResultsStream(mStrResults, columnNamesAlreadyGenerated);
        generateExitResultsStream(mStrResults, columnNamesAlreadyGenerated);
#endif // _LOG
#if defined _LOG || defined _FULL_RESULTS
        generatePrimipareReproductionResultsBasedOnCalvingStream(mStrResults, columnNamesAlreadyGenerated);
        generateMultipareReproductionResultsBasedOnCalvingStream(mStrResults, columnNamesAlreadyGenerated);
        generateNullipareReproductionResultsBasedOnInseminationStream(mStrResults, columnNamesAlreadyGenerated);
        generatePrimiMultipareReproductionResultsBasedOnInseminationStream(mStrResults, columnNamesAlreadyGenerated);
        generateMonthMilkResultsStream(mStrResults, columnNamesAlreadyGenerated, true);
        generateMonthMilkResultsStream(mStrResults, columnNamesAlreadyGenerated, false);
        generateAnnualMastitisResultsStream(mStrResults, columnNamesAlreadyGenerated);
        generateAnnualKetosisResultsStream(mStrResults, columnNamesAlreadyGenerated);
        generateAnnualLamenessResultsStream(mStrResults, columnNamesAlreadyGenerated, true);
        generateAnnualLamenessResultsStream(mStrResults, columnNamesAlreadyGenerated, false);              
        generateCalfFeedingStream(mStrResults, columnNamesAlreadyGenerated);
        generateFeedingStream(mStrResults, columnNamesAlreadyGenerated, true);
        generateFeedingStream(mStrResults, columnNamesAlreadyGenerated, false);
#endif // _LOG || _FULL_RESULTS
        
        columnNamesAlreadyGenerated = true;
    } /* End of namespace Results::DairyHerdResults */
} /* End of namespace Results */