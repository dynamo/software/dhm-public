#ifndef Results_DairyFarmResult_h
#define Results_DairyFarmResult_h

// standard
#include <map>
#include <vector>

// boost

// project
#include "Result.h"
#include "../ExchangeInfoStructures/FunctionalEnumerations.h"
#include "../ExchangeInfoStructures/FunctionalConstants.h"
#ifdef _LOG
#include "DayAnimalLogInformations.h"
#endif // _LOG 

#if defined _LOG || defined _FULL_RESULTS

#define BEGIN_BEGIN_GENERATE_FILE_METHOD \
    remove(fileName.c_str()); \
    if (collect.size()>0) \
    { 

#define END_BEGIN_GENERATE_FILE_METHOD \
        std::vector<std::string> linesForTheSaveFile; \
        const std::string strSep = {SEPARATOR}; \
        columnNames += strSep + "Campaign"; \
        linesForTheSaveFile.push_back(columnNames); \
        for (auto data : collect) \
        {

#define BEGIN_GENERATE_FILE_METHOD \
    BEGIN_BEGIN_GENERATE_FILE_METHOD \
    CONSOLE_FILE_COMMENT \
    END_BEGIN_GENERATE_FILE_METHOD
        
#define END_GENERATE_FILE_METHOD \
            linesForTheSaveFile.push_back(currentFileLine); \
        } \
        Tools::writeFile(linesForTheSaveFile, fileName, true); \
    }

#endif // _LOG || _FULL_RESULTS

        
#ifdef _LOG
#define CONSOLE_FILE_COMMENT \
        Console::Display::displayLineInConsole("Generating file: " + fileName);
#else // _LOG
#define CONSOLE_FILE_COMMENT
#endif // _LOG

namespace Results
{
    // -------------------
    // Development results
    // -------------------

    // Dairy Farm
    // -----------
#ifdef _LOG
    struct WeekAnimalPopulationInformations
    {  
        std::string simulationDate;
        unsigned int calfPopulation = 0;
        unsigned int heiferPopulation = 0;
        unsigned int adultPopulation = 0;
        unsigned int getTotalPopulation() { return calfPopulation + heiferPopulation + adultPopulation;}

        // Population Category Group count
        unsigned int calveSold = 0;
        unsigned int heifersSold = 0;
        unsigned int adultsSold = 0;
        unsigned int deathNumber = 0;
    };

    struct Interval
    {
        boost::gregorian::date date1;
        boost::gregorian::date date2;
        unsigned int comp;
        unsigned int comp2;
        unsigned int getIntervalInDays() { return (date2-date1).days();}
    };

    struct RatioResult
    {
        unsigned int id;
        boost::gregorian::date date;
        FunctionalEnumerations::Reproduction::DairyCowParity parity;
        bool val;
    };

    struct Age
    {
        unsigned int id;
        boost::gregorian::date date;
        unsigned int age;
    };

    struct InseminationResult
    {
        unsigned int id;
        boost::gregorian::date date;
        FunctionalEnumerations::Reproduction::DairyCowParity parity;
        FunctionalEnumerations::Reproduction::TypeInseminationSemen typeInsemination;
        FunctionalEnumerations::Genetic::Breed maleBreed;
        unsigned int number;
        bool result;
        unsigned int ketosis;
    };

    struct InseminationCalvingInterval
    {
        boost::gregorian::date calvingDate;
        boost::gregorian::date inseminationdate;
        FunctionalEnumerations::Reproduction::TypeInseminationSemen typeInsemination;
        unsigned int id;
        unsigned int ketosis;
        unsigned int getIntervalInDays() { return (inseminationdate-calvingDate).days();}
    };

    struct IIMResult
    {
        unsigned int id;
        boost::gregorian::date date;
        unsigned int dayOfTheReferenceCycle;
        unsigned int dairyQuarterNumber;
        FunctionalEnumerations::Health::BacteriumType bacteriumType;
        FunctionalEnumerations::Health::MastitisSeverity mastitisSeverity;
    };

    struct KetosisResult
    {
        unsigned int id;
        boost::gregorian::date date;
        FunctionalEnumerations::Health::KetosisSeverity ketosisSeverity;
    };

    struct LamenessResult
    {
        unsigned int id;
        boost::gregorian::date date;
        FunctionalEnumerations::Health::LamenessSeverity lamenessSeverity;
        unsigned int lactationRank;
        int lactationStage;
    };

    struct DeathResult
    {
        unsigned int id;
        boost::gregorian::date date;
        unsigned int age;
        FunctionalEnumerations::Genetic::Sex sex;
        bool bornInHerd;
        FunctionalEnumerations::Population::DeathReason reason;
    };

    struct ExitResult
    {
        unsigned int id;
        boost::gregorian::date date;
        unsigned int age;
        FunctionalEnumerations::Genetic::Sex sex;
        bool bornInHerd;
        std::string reason;
    };
#endif // _LOG
    
#if defined _LOG || defined _FULL_RESULTS

    // ------------
    // Full results
    // ------------

    // Reproduction (2.2.1.3)
    // ----------------------

    struct PrimipareReproductionResultBasedOnCalving
    {
        unsigned int id;
        boost::gregorian::date obtainedCalvingDate;
        unsigned int firstInseminationAge;
        unsigned int fertilizingInseminationAge;
        unsigned int obtainedCalvingAge;
        boost::gregorian::date firstInseminationDate;
        FunctionalEnumerations::Reproduction::TypeInseminationSemen firstInseminationType;
        FunctionalEnumerations::Genetic::Breed firstInseminationMaleBreed;
        unsigned int inseminationQuantity;
        boost::gregorian::date fertilizingInseminationDate;
        unsigned int fertilizingInseminationNumber;
        FunctionalEnumerations::Reproduction::TypeInseminationSemen fertilizingInseminationType;
        FunctionalEnumerations::Genetic::Breed fertilizingInseminationMaleBreed;
        unsigned int getFirstInseminationFertilizingInseminationInterval() { return (fertilizingInseminationDate-firstInseminationDate).days();}
    };
    
    struct MultipareReproductionResultBasedOnCalving
    {
        unsigned int id;
        boost::gregorian::date obtainedCalvingDate;
        boost::gregorian::date previousCalvingDate;
        unsigned int getPreviousObtainedCalvingInterval() { return (obtainedCalvingDate-previousCalvingDate).days();}
        unsigned int previousCalvingRank;
        boost::gregorian::date firstInseminationDate;
        FunctionalEnumerations::Reproduction::TypeInseminationSemen firstInseminationType;
        FunctionalEnumerations::Genetic::Breed firstInseminationMaleBreed;
        unsigned int getPreviousCalvingFirstInseminationInterval() { return (firstInseminationDate-previousCalvingDate).days();}
        unsigned int inseminationQuantity;
        boost::gregorian::date fertilizingInseminationDate;
        unsigned int  fertilizingInseminationNumber;
        FunctionalEnumerations::Reproduction::TypeInseminationSemen  fertilizingInseminationType;
        FunctionalEnumerations::Genetic::Breed fertilizingInseminationMaleBreed;
        unsigned int getPreviousCalvingFertilizingInseminationInterval() { return ( fertilizingInseminationDate-previousCalvingDate).days();}
        unsigned int getFirstInseminationFertilizingInseminationInterval() { return ( fertilizingInseminationDate- firstInseminationDate).days();}
    };
    
    struct PrimiMultipareReproductionResultBasedOnInsemination
    {
        unsigned int id;
        boost::gregorian::date previousCalvingDate;
        unsigned int previousCalvingRank;
        boost::gregorian::date firstInseminationDate;
        boost::gregorian::date inseminationDate;
        FunctionalEnumerations::Reproduction::TypeInseminationSemen inseminationType;
        FunctionalEnumerations::Genetic::Breed inseminationMaleBreed;
        unsigned int getPreviousCalvingFirstInseminationInterval() { return (firstInseminationDate-previousCalvingDate).days();}
        unsigned int inseminationQuantity;
    };

    struct NullipareReproductionResultBasedOnInsemination
    {
        unsigned int id;
        unsigned int firstInseminationAge;
        boost::gregorian::date firstInseminationDate;
        boost::gregorian::date inseminationDate;
        FunctionalEnumerations::Reproduction::TypeInseminationSemen inseminationType;
        FunctionalEnumerations::Genetic::Breed inseminationMaleBreed;
        unsigned int inseminationQuantity;
    };

#endif // _LOG || _FULL_RESULTS

    struct TechnicalReproductionResult
    {
        boost::gregorian::date date;
        float meanIVIA1 = 0.0f;
        float succesfulIA1Percent = 0.0f;
        float meanIVIAF = 0.0f;
        float meanIVV = 0.0f;
        float meanFirstCalvingAge = 0.0f;
        unsigned int nbIA1 = 0;
        unsigned int nbIA = 0;
        unsigned int reproductionTroubleTreatmentCount = 0;
        float reproductionTroubleTreatmentPercent = 0.0f;
        float reproductionTroubleCullingPercent = 0.0f;
    };

    struct ReproductionResults
    {
#ifdef _LOG                
        std::vector<Age> V1Age;
        std::vector<Interval> IVC1;
        std::vector<InseminationCalvingInterval> IVI1;
        std::vector<Interval> IIA1Returns;
        std::vector<Interval> IIA1IAF;
        std::vector<Interval> IVIAF;
        std::vector<Interval> IVV;
        std::vector<InseminationResult> InseminationResults;
        std::vector<Interval> PregnancyDurations;
        std::vector<RatioResult> SexResults;
#endif // _LOG
#if defined _LOG || defined _FULL_RESULTS
        std::vector<PrimipareReproductionResultBasedOnCalving> PrimipareReproductionResultsBasedOnCalving;
        std::vector<MultipareReproductionResultBasedOnCalving> MultipareReproductionResultsBasedOnCalving;
        std::vector<NullipareReproductionResultBasedOnInsemination> NullipareReproductionResultsBasedOnInsemination;
        std::vector<PrimiMultipareReproductionResultBasedOnInsemination> PrimiMultipareReproductionResultsBasedOnInsemination;
#endif // _LOG || _FULL_RESULTS
        std::vector<TechnicalReproductionResult> TechnicalReproductionResults;
    };

    // Lactation (2.2.2.3)
    // ----------------------
    struct MilkResult
    {
        boost::gregorian::date date;
        float literQuantity = 0.0f;
        float literTB = 0.0f;
        float literTP = 0.0f;
        float SCC = 0.0f;
    };

    struct MilkControlResult
    {
        boost::gregorian::date date;
        unsigned int presentAdultCowCount;
        unsigned int presentMilkingCowCount;
        float meanGrossMilkProductOfTheControledDay;
        float milkTBOfTheControledDay;
        float milkTPOfTheControledDay;
        float milkSCCOfTheControledDay;
        unsigned int meanLactationStage;
        float SCCPrevalenceRatio;
        float SCCIncidence;         
        float detectedInfectiousLamenessPercent;
        float infectiousG1LamenessPercent;
        float infectiousG2LamenessPercent;
        float detectedNonInfectiousLamenessPercent;
        float nonInfectiousG1LamenessPercent;
        float nonInfectiousG2LamenessPercent;

#ifdef _LOG
        // For calibration
        std::vector<unsigned int> concernedCowCount = std::vector<unsigned int> (FunctionalConstants::Health::DAY_RANGES_FOR_KETOSIS_CALIBRATION.size(), 0);
        std::vector<unsigned int> G1ketosisPrevalence = std::vector<unsigned int> (FunctionalConstants::Health::DAY_RANGES_FOR_KETOSIS_CALIBRATION.size(), 0);
        std::vector<unsigned int> G2ketosisPrevalence = std::vector<unsigned int> (FunctionalConstants::Health::DAY_RANGES_FOR_KETOSIS_CALIBRATION.size(), 0);
        std::vector<unsigned int> G1ketosisIncidence = std::vector<unsigned int> (FunctionalConstants::Health::DAY_RANGES_FOR_KETOSIS_CALIBRATION.size(), 0);
        std::vector<unsigned int> G2ketosisIncidence = std::vector<unsigned int> (FunctionalConstants::Health::DAY_RANGES_FOR_KETOSIS_CALIBRATION.size(), 0);
#endif // _LOG
    };

    struct TechnicalProductionResult
    {
        boost::gregorian::date date;
        float deliveredMilkLiterQuantity = 0.0f;
        float deliveredMilkLiterQuantityByCow = 0.0f;
        float productedMilkLiterQuantityByCow = 0.0f;
        float SCC = 0.0f;
        float TB = 0.0f;
        float TP = 0.0f;
    };

    struct LactationResults
    {
        std::vector<MilkControlResult> MilkControlResults;
        std::vector<MilkResult> DeliveredMilkResults;
        std::vector<MilkResult> DiscardedMilkResults;
        std::vector<TechnicalProductionResult> TechnicalProductionResults;
    };

    // Health (2.2.3.X.3)
    //----------------

    // Mastitis (2.2.3.1.3)
    struct AnnualMastitisResult
    {
        boost::gregorian::date date;
        unsigned int meanPresentAdultCowCount;
        unsigned int clinicalMastitisPer100cows;
        unsigned int firstClinicalMastitisPer100cows;
        float herdSCC;
        unsigned int underRefSCCper100Controls;
    };

    // Ketosis (2.2.3.2.3)
    struct AnnualKetosisResult
    {
        boost::gregorian::date date;
        unsigned int meanPresentAdultCowCount;
        unsigned int clinicalKetosisPer100cows;
        unsigned int firstSubclinicalKetosisPer100cows;
        unsigned int firstClinicalKetosisPer100cows;
    };

    // Lameness (x.x.x.x.x)
    struct AnnualLamenessResult
    {
        boost::gregorian::date date;
        unsigned int meanPresentAdultCowCount;
        unsigned int G1LamenessPer100cows;
        unsigned int G2LamenessPer100cows;
    };

    struct TechnicalHealthResult
    {
        boost::gregorian::date date;
        unsigned int calvingCount = 0;
        unsigned int G1ketosisOccurencyDueToCalvingCount = 0;
        unsigned int G2ketosisOccurencyDueToCalvingCount = 0;
        float G1ketosisOccurencyByCalvingCow = 0.0f;
        float G2ketosisOccurencyByCalvingCow = 0.0f;
        float subclinicalMastitisOccurencyByCow = 0.0f;
        float clinicalMastitisOccurencyByCow = 0.0f;
        float mastitisMortalityByCow = 0.0f;
        float ketosisMortalityByCow = 0.0f;
        float nonInfectiousLamenessMortalityByCow = 0.0f;
        float infectiousLamenessMortalityByCow = 0.0f;
        float G1NonInfectiousLamenessOccurencyByCow = 0.0f;
        float G2NonInfectiousLamenessOccurencyByCow = 0.0f;
        float G1InfectiousLamenessOccurencyByCow = 0.0f;
        float G2InfectiousLamenessOccurencyByCow = 0.0f;
        unsigned int totalHealthTreatmentCount = 0;
        unsigned int curativeClinicalKetosisVetTreatmentCount = 0;
        unsigned int curativeClinicalKetosisFarmerTreatmentCount = 0;
        unsigned int curativeSubclinicalKetosisVetTreatmentCount = 0;
        unsigned int curativeSubclinicalKetosisFarmerTreatmentCount = 0;
        unsigned int curativeClinicalMastitisVetTreatmentCount = 0;
        unsigned int curativeClinicalMastitisFarmerTreatmentCount = 0;
        unsigned int curativeSubclinicalMastitisVetTreatmentCount = 0;
        unsigned int curativeSubclinicalMastitisFarmerTreatmentCount = 0;
        //unsigned int preventiveKetosisVetTreatmentCount = 0;
        unsigned int preventiveKetosisFarmerTreatmentCount = 0;
        //unsigned int preventiveMastitisVetTreatmentCount = 0;
        unsigned int preventiveMastitisFarmerTreatmentCount = 0;
        unsigned int curativeClinicalLamenessVetTreatmentCount = 0;
        unsigned int preventiveLamenessVetTreatmentCount = 0;
        unsigned int unscheduledCowVetCareCount = 0;
        unsigned int scheduledCowFarmerCareCount = 0;
        unsigned int unscheduledCowFarmerCareCount = 0;
        unsigned int scheduledVetActivityCount = 0;
        unsigned int unscheduledVetActivityCount = 0;
        unsigned int scheduledFarmerActivityCount = 0;
        unsigned int unscheduledFarmerActivityCount = 0;
        float mastitisTroubleCullingPercent = 0.0f;
        float ketosisTroubleCullingPercent = 0.0f;
        float nonInfectiousLamenessTroubleCullingPercent = 0.0f;
        float infectiousLamenessTroubleCullingPercent = 0.0f;
    };

    struct HealthResults
    {
        std::vector<AnnualMastitisResult> AnnualMastitisResults;
        std::vector<AnnualKetosisResult> AnnualKetosisResults;
        std::vector<AnnualLamenessResult> AnnualNonInfectiousLamenessResults;
        std::vector<AnnualLamenessResult> AnnualInfectiousLamenessResults;
        std::map<std::string,TechnicalHealthResult> TechnicalHealthResults; // Key is campaign
#ifdef _LOG                
        std::vector<IIMResult> IIMResults;
        std::vector<KetosisResult> KetosisResults;
        std::vector<LamenessResult> NonInfectiousLamenessResults;
        std::vector<LamenessResult> InfectiousLamenessResults;
#endif // _LOG                
    };

    // Genetic (2.2.4.3)
    // -----------------
#ifdef _LOG                
    struct GeneticResult
    {
        boost::gregorian::date date;
        unsigned int idOrBreed;
        bool birth;
        float VGLait;
        float VGTB;
        float VGTP;
        float VGFer;
        float VGMACL;
        float VGBHBlait;
        float VGRBi;
        float VGRBni;
    };
#endif // _LOG                

    struct GeneticResults
    {
#ifdef _LOG                
        std::vector<GeneticResult> FemaleVGResults;
        std::vector<GeneticResult> MaleVGResults;
#endif // _LOG                
    };

    // Population (2.2.5.3)
    // --------------------
    struct TechnicalPopulationResult
    {
        boost::gregorian::date date;
        unsigned int meanPresentAdultCowCount = 0;
        unsigned int birthCount = 0;
        unsigned int malesSaleCount = 0;
        unsigned int beefCrossBredSaleCount = 0;
        unsigned int sterileFemalesSaleCount = 0;
        unsigned int extraFemalesSaleCount = 0;
        unsigned int extraPregnantHeifersSaleCount = 0;
        unsigned int primipareInHerdCount = 0;
        unsigned int heiferBoughtCount = 0;
        unsigned int deathCalveCount = 0;
        float deathCalvePer100Calves = 0.0f;
        unsigned int deathHeiferCount = 0;
        unsigned int deathAdultCount = 0;
        float deathAdultPer100Cows = 0.0f;
        unsigned int notBredBecauseOfInfertilityCount = 0;
        unsigned int notBredBecauseOfLactationRankCount = 0;
        unsigned int notBredBecauseOfLowMilkProductCount = 0;
        float cullingRate = 0.0f;
        unsigned int healthCulledAnimalCount = 0;
        unsigned int infertilityCulledAnimalCount = 0;
        unsigned int lactationRankCulledAnimalCount = 0;
        unsigned int lowMilkProductCulledAnimalCount = 0;
        float longevity = 0.0f;
        unsigned int meanDaysBetweenDecisionAndCulling = 0;
        unsigned int meanLactationStageAtCulling = 0;
        float meanMilkQuantityOfTheDayAtCulling = 0.0f;
        float maxMilkQuantityOfTheDayAtCulling = 0.0f;
        unsigned int minLactationStageAtCulling = 0;
    };

    struct PopulationResults
    {
#ifdef _LOG                
        std::vector<DeathResult> DeathResults;
        std::vector<ExitResult> ExitResults;
#endif // _LOG                
        std::vector<TechnicalPopulationResult> TechnicalPopulationResults;
    };

    // Feeding (2.2.6.3)
    // -----------------
    struct CalfFeedingResult
    {
        boost::gregorian::date date;
        float discardedMilkQuantity = 0.0f; // liter
        float bulkMilkQuantity = 0.0f; // liter
        float dehydratedMilkQuantity = 0.0f; // powder kg
        float maizeSilageQuantity = 0.0f;
        float grassQuantity = 0.0f;
        float hayQuantity = 0.0f;
        float grassSilageQuantity = 0.0f;
        float rapeseedQuantity = 0.0f;
        float sojaQuantity = 0.0f;
        float barleyQuantity = 0.0f;
        float mineralVitaminQuantity = 0.0f;
    };

    struct FeedingResult
    {
        boost::gregorian::date date;
        float maizeSilageQuantity = 0.0f;
        float grassQuantity = 0.0f;
        float hayQuantity = 0.0f;
        float grassSilageQuantity = 0.0f;
        float rapeseedQuantity = 0.0f;
        float sojaQuantity = 0.0f;
        float barleyQuantity = 0.0f;
        float mineralVitaminQuantity = 0.0f;
    };

#if defined _LOG || defined _FULL_RESULTS
    struct FeedingResults
    {
        std::vector<CalfFeedingResult> calfFeedingResults;
        std::vector<FeedingResult> heiferFeedingResults;
        std::vector<FeedingResult> cowFeedingResults;
    };
#endif // _LOG || _FULL_RESULTS

    // Accounting model (2.2.7.3)
    // --------------------------
#ifdef _LOG                
    struct Transaction
    {
        boost::gregorian::date date;
        float amount;
        std::string comp;
    };
#endif // _LOG
    
    struct EconomicalResult
    {
        boost::gregorian::date date;
        float grossMargin = 0.0f;
        float totalProducts = 0.0f;
        float milkSales = 0.0f;
        float cullSales = 0.0f;
        float heiferSales = 0.0f;
        float calfSales = 0.0f;
        float totalExpenses = 0.0f;
        float feedCosts = 0.0f;
        float healthCosts = 0.0f;
        float mastitisHealthCosts = 0.0f;
        float ketosisHealthCosts = 0.0f;
        float curativeIndividualInfectiousLamenessCosts = 0.0f;
        float curativeIndividualNonInfectiousLamenessCosts = 0.0f;
        float curativeCollectiveInfectiousLamenessCosts = 0.0f;
        float curativeCollectiveNonInfectiousLamenessCosts = 0.0f;
        float curativeTrimmingCosts = 0.0f;
        float overallCurativeLamenessCosts = 0.0f;
        float preventiveTrimmingCosts = 0.0f;
        float footBathCosts = 0.0f;
        float otherHealthCosts = 0.0f;
        float reproductionCosts = 0.0f;
        float populationCosts = 0.0f;
        float otherCosts = 0.0f;
    };

    struct AccountingResults
    {
#ifdef _LOG                
        std::vector<Transaction> Transactions;
#endif // _LOG                
        std::vector<EconomicalResult> EconomicalResults;
    };

    class DairyFarmResult : public Result
    {

#ifdef _LOG
        std::map<unsigned int, std::map<unsigned int, DayAnimalLogInformations>> _mFinalAnimalLogData;
#endif // _LOG 

    private:
        // Run results
        // -----------
        ReproductionResults _reproductionResults;
        LactationResults _lactationResults;
        HealthResults _healthResults;
#ifdef _LOG
        GeneticResults _geneticResults;
#endif // _LOG
        PopulationResults _populationResults;
#if defined _LOG || defined _FULL_RESULTS
        FeedingResults _feedingResults;
#endif // _LOG || _FULL_RESULTS
        AccountingResults _accountingResults;

#ifdef _LOG        
        // Calibration results
        // -------------------
//        std::vector<float> _managedHealthCostsForCalibration; // Calibration value for protocol level calculation
        std::vector<WeekAnimalPopulationInformations> _vWeekAnimalPopulationInformations; // Week results

        void generateAgeFile(std::vector<Age> &collect, std::string &columnNames, std::string fileName);
        void generateInseminationResultsFile(std::vector<InseminationResult> &collect, std::string &columnNames, std::string fileName);            
        void generateInseminationCalvingIntervalFile(std::vector<InseminationCalvingInterval> &collect, std::string &columnNames, std::string fileName);
        void generateRatioFile(std::vector<RatioResult> &collect, std::string &columnNames, std::string fileName);
        void generateIntervalFile(std::vector<Interval> &collect, std::string &columnNames, std::string fileName);
        void generateVGFile(std::vector<GeneticResult> &collect, std::string &columnNames, std::string fileName, bool manageBornInHerd);
        void generateIIMFile(std::vector<IIMResult> &collect, std::string fileName);
        void generateKetosisFile(std::vector<KetosisResult> &collect, std::string fileName);
        void generateLamenessFile(std::vector<LamenessResult> &collect, std::string fileName);
        void generateDeathResultsStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated);
        void generateExitResultsStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated);
        void generateTransactionFile(std::vector<Transaction> &collect, std::string fileName);
//        void generateMonthMilkFile(std::vector<MilkResult> &collect, std::string fileName, bool delivered);
//        void generateFeedingFile(std::vector<CalfFeedingResult> &collect, std::string columnNames, std::string fileName); // calf feeding results
//        void generateFeedingFile(std::vector<FeedingResult> &collect, std::string columnNames, std::string fileName); // Heifer and cow feeding results
//        void generateAnnualMastitisResultsFile(std::vector<AnnualMastitisResult> &collect, std::string fileName); // Annual mastitis results
//        void generateAnnualKetosisResultsFile(std::vector<AnnualKetosisResult> &collect, std::string fileName); // Annual Ketosis results
//        void generateAnnualLamenessResultsFile(std::vector<AnnualLamenessResult> &collect, std::string fileName); // Annual non infectious Lameness results
#endif // _LOG
          
#if defined _LOG || defined _FULL_RESULTS
        void generatePrimipareReproductionResultsBasedOnCalvingStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated);
        void generateMultipareReproductionResultsBasedOnCalvingStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated);
        void generateNullipareReproductionResultsBasedOnInseminationStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated);
        void generatePrimiMultipareReproductionResultsBasedOnInseminationStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated);
        void generateMonthMilkResultsStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated, bool delivered);
        void generateAnnualMastitisResultsStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated);
        void generateAnnualKetosisResultsStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated);
        void generateAnnualLamenessResultsStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated, bool infectious);
        static void getFeedingColumNames(std::string &columnNames, bool calf);
        void generateCalfFeedingStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated);
        void generateFeedingStream(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated, bool heifer);
#endif // _LOG || _FULL_RESULTS

        void generateTechnicalAndEconomicalResultsStream(std::map<std::string, std::vector<std::string>>  &mStreamMergedResults, bool columnNamesAlreadyGenerated);
        void generateMilkControlResultsStream(std::map<std::string, std::vector<std::string>> &mStreamMergedResults, bool columnNamesAlreadyGenerated);

    protected:
        void concrete(){}; // To allow instanciation

    public:
        DairyFarmResult(std::string &discriminant1, std::string &discriminant2, std::string &discriminant3, std::string &discriminant4, boost::gregorian::date &beginDate, unsigned int runNumber); // Constructor
        virtual ~DairyFarmResult(); // Destructor

        ReproductionResults &getReproductionResults();
        LactationResults &getLactationResults();
        HealthResults &getHealthResults();  
#ifdef _LOG
        GeneticResults &getGeneticResults();
#endif // _LOG
        PopulationResults &getPopulationResults();
#if defined _LOG || defined _FULL_RESULTS
        FeedingResults &getFeedingResults();
#endif // _LOG || _FULL_RESULTS
        AccountingResults &getAccountingResults();

#ifdef _LOG
        std::map<unsigned int, std::map<unsigned int, DayAnimalLogInformations>> &getFinalAnimalLogData();
//        void resetCalibrationValues() override;
        //std::vector<float> &getManagedHealthCostsForCalibration();
        std::vector<WeekAnimalPopulationInformations> &getWeekAnimalPopulationInformations(); // Dairy Herd Week Informations
        void toFileHealthResults(HealthResults &results, std::string &pathName);
        void toFileGeneticResults(GeneticResults &results, std::string &pathName);
        void toFileAccountingResults(AccountingResults &results, std::string &pathName);
        void toFilePopulationResults(PopulationResults &results, std::string &pathName);
        void toFileWeekAnimalPopulationInformations(std::vector<WeekAnimalPopulationInformations> &informations, std::string fileName);
        void runResultsToFile(const std::string &path) override;
        void toFileReproductionResults(ReproductionResults &results, std::string &pathName);
#endif // _LOG || _FULL_RESULTS
       
        void getResultStreams(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated);
    };
} /* End of namespace Results */
#endif // Results_DairyFarmResult_h
