

#ifdef _LOG
#include "DayAnimalLogInformations.h"

// Standard

// Project

BOOST_CLASS_EXPORT(Results::DayAnimalLogInformations) // Recommended (mandatory if virtual serialization)

namespace Results
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_NVP(simDate); \
        ar & BOOST_SERIALIZATION_NVP(age); \
        ar & BOOST_SERIALIZATION_NVP(PLDelivered); \
        ar & BOOST_SERIALIZATION_NVP(PLDiscarded); \
        ar & BOOST_SERIALIZATION_NVP(TB); \
        ar & BOOST_SERIALIZATION_NVP(TP); \
        ar & BOOST_SERIALIZATION_NVP(SCC); \
        ar & BOOST_SERIALIZATION_NVP(milkRation); \
        ar & BOOST_SERIALIZATION_NVP(forageRation); \
        ar & BOOST_SERIALIZATION_NVP(concentrateRation); \
        ar & BOOST_SERIALIZATION_NVP(pregnant); \
        ar & BOOST_SERIALIZATION_NVP(mastitisRisk); \
        ar & BOOST_SERIALIZATION_NVP(mastitisInfection); \
        ar & BOOST_SERIALIZATION_NVP(ketosisState); \
        ar & BOOST_SERIALIZATION_NVP(nonInfectiousLamenessState); \
        ar & BOOST_SERIALIZATION_NVP(infectiousLamenessState); \
        ar & BOOST_SERIALIZATION_NVP(batch); \
        ar & BOOST_SERIALIZATION_NVP(location); \
        ar & BOOST_SERIALIZATION_NVP(move); \
        ar & BOOST_SERIALIZATION_NVP(miscellaneous); \

    IMPLEMENT_SERIALIZE_STD_METHODS(DayAnimalLogInformations);
    
} /* End of namespace Results */

#endif // _LOG 
