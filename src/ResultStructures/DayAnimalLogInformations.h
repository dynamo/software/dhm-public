#ifndef Results_DayAnimalLogInformations_h
#define Results_DayAnimalLogInformations_h


#ifdef _LOG

// Boost
#include <boost/date_time/gregorian/gregorian.hpp>

// Standard
#include <string>

// Project
#include "../Tools/Tools.h"
#include "../Tools/Serialization.h"

namespace Results
{
    struct DayAnimalLogInformations
    {
        boost::gregorian::date simDate;
        unsigned int age = 0;
        float PLDelivered = 0.0f;
        float PLDiscarded = 0.0f;
        float TB = 0.0f;
        float TP = 0.0f;
        float SCC = 0.0f;
        float milkRation = 0.0f;
        float forageRation = 0.0f;
        float concentrateRation = 0.0f;
        float mineralVitaminRation = 0.0f;
        bool pregnant = false;
        std::string mastitisRisk = "";
        std::string mastitisInfection = "";
        bool clinicalMastitisSinceLactationBegin = false;
        std::string ketosisState = "";
        std::string nonInfectiousLamenessState = "";
        std::string infectiousLamenessState = "";
        int batch = -1;    
        int location = -1;
        std::string move = "";
        std::string miscellaneous;
        
        DECLARE_SERIALIZE_STD_METHODS;
        
    public:
        DayAnimalLogInformations(){}; // serialization constructor   
        static std::string getTitle() {return "simDate;age;PL del(kg);PL dis(kg);TB (g/kg);TP (g/kg);SCC (1000/ml);milk ration (kg);forage ration (kgMS);concentrate ration (kgMB);mineral vitamin ration (kgMB);pregnant;mastitisRisk;mastitis infection status;clinical mastitis Since Lactation Begin;ketosis status;non infectious lameness status;infectious lameness status;batch;location;move;miscellaneous";}
        std::string getDataLine()
        {
            std::string line = boost::gregorian::to_iso_extended_string(simDate);
            std::string tmp;
            line += ";" + Tools::toString(age);
            tmp = Tools::toString(PLDelivered); line += ";" + Tools::changeChar(tmp, '.', ',');
            tmp = Tools::toString(PLDiscarded); line += ";" + Tools::changeChar(tmp, '.', ',');
            tmp = Tools::toString(TB); line += ";" + Tools::changeChar(tmp, '.', ',');
            tmp = Tools::toString(TP); line += ";" + Tools::changeChar(tmp, '.', ',');
            tmp = Tools::toString(SCC); line += ";" + Tools::changeChar(tmp, '.', ',');
            tmp = Tools::toString(milkRation); line += ";" + Tools::changeChar(tmp, '.', ',');
            tmp = Tools::toString(forageRation); line += ";" + Tools::changeChar(tmp, '.', ',');
            tmp = Tools::toString(concentrateRation); line += ";" + Tools::changeChar(tmp, '.', ',');
            tmp = Tools::toString(mineralVitaminRation); line += ";" + Tools::changeChar(tmp, '.', ',');
            line += ";" + Tools::toString(pregnant);
            line += ";" + mastitisRisk;
            line += ";" + mastitisInfection;
            line += ";" + Tools::toString(clinicalMastitisSinceLactationBegin);
            line += ";" + ketosisState;
            line += ";" + nonInfectiousLamenessState;
            line += ";" + infectiousLamenessState;
            line += ";" + Tools::toString(batch);
            line += ";" + Tools::toString(location);
            line += ";" + Tools::toString(move);
            line += ";" + miscellaneous;
            return line;
        }
    };
}

#endif // _LOG 

#endif // Results_DayAnimalLogInformations_h
