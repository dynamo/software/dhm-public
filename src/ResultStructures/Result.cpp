//////////////////////////////////////////
//                                      //
// File : Result.cpp                    //
//                                      //
// Author : Philippe Gontier            //
//                                      //
// Version 1.0                          //
//                                      //
// Date : 01/10/2015                    //
//                                      //
//////////////////////////////////////////

#include "Result.h"

// Standard
#include <iostream>
#include <fstream>

// Project
#include "../Tools/Tools.h"

#ifdef _LOG
BOOST_CLASS_EXPORT(Results::Result) // Recommended (mandatory if virtual serialization)
#endif // _LOG 

namespace Results
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_NVP(_discriminant1); \
        ar & BOOST_SERIALIZATION_NVP(_discriminant2); \
        ar & BOOST_SERIALIZATION_NVP(_discriminant3); \
        ar & BOOST_SERIALIZATION_NVP(_discriminant4); \
        ar & BOOST_SERIALIZATION_NVP(_beginDate); \
        ar & BOOST_SERIALIZATION_NVP(_runNumber); \

    IMPLEMENT_SERIALIZE_STD_METHODS(Result);
        
    // constructor
    Result::Result(std::string &discriminant1, std::string &discriminant2, std::string &discriminant3, std::string &discriminant4, boost::gregorian::date &beginDate, unsigned int runNumber)
    {
        _discriminant1 = discriminant1;
        _discriminant2 = discriminant2;
        _discriminant3 = discriminant3;
        _discriminant4 = discriminant4;
        _beginDate = beginDate;
        _runNumber = runNumber;
    }
    
    Result::~Result()
    {
    }
    
    std::string Result::getStrCampaign(const boost::gregorian::date &date)
    {
        if (_beginDate.month() == 1)
        {
            // Begin of the simulation at the begin of a year
            return Tools::toString(date.year());
        }
        else
        {
            // Begin of the simulation during a year
            if (date >= boost::gregorian::date(date.year(), _beginDate.month(), _beginDate.day()))
            {
                return Tools::toString(date.year()) + '-' + Tools::toString(date.year()+1);
            }
            else
            {
                return Tools::toString(date.year()-1) + '-' + Tools::toString(date.year());
            }
        }
    }
    
#if defined _LOG || defined _FULL_RESULTS
    void Result::runResultsToFile(const std::string &path)
    {
    }  
#endif // _LOG || _FULL_RESULTS
} /* End of namespace Results */