//////////////////////////////////////////
//                                      //
// File : Result.h                      //
//                                      //
// Author : Philippe Gontier            //
//                                      //
// Version 1.0                          //
//                                      //
// Date : 01/10/2015                    //
//                                      //
//////////////////////////////////////////

#ifndef Results_Result_h
#define Results_Result_h

// boost
#include <boost/date_time/gregorian/gregorian.hpp>

// standard
#include <string>
#include <vector>

// project
#include "../Tools/Tools.h"
#include "../Tools/Serialization.h"

// Result field separator 
#ifndef SEPARATOR
#define SEPARATOR TechnicalConstants::EXPORT_SEP
#endif


// Macros for results editing
#define BEGIN_BEGIN_GENERATE_STREAM_METHOD \
    const std::string strSep = {SEPARATOR}; \
    std::string columnNames = ""; \

#define END_BEGIN_GENERATE_STREAM_METHOD_1 \
    if (collect.size()>0) \
    { \
        if (columnNames.length() != 0 and not columnNamesAlreadyGenerated) \
        { 

#define END_BEGIN_GENERATE_STREAM_METHOD_2 \
            vStreamMergedResults.push_back(columnNames); \
        } \
        for (auto data : collect) \
        { 

#define END_BEGIN_GENERATE_STREAM_METHOD \
        END_BEGIN_GENERATE_STREAM_METHOD_1 \
        END_BEGIN_GENERATE_STREAM_METHOD_2

#define END_BEGIN_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS \
        END_BEGIN_GENERATE_STREAM_METHOD_1 \
            columnNames += strSep + "Campaign" + strSep + "Run_number" + strSep + "Discriminant1" + strSep + "Discriminant2" + strSep + "Discriminant3" + strSep + "Discriminant4"; \
        END_BEGIN_GENERATE_STREAM_METHOD_2

#define END_GENERATE_STREAM_METHOD \
                vStreamMergedResults.push_back(currentStreamLine); \
            } \
        }

#define END_GENERATE_STREAM_METHOD_WITH_CAMPAIGN_AND_DISCRIMINANTS \
                currentStreamLine += SEPARATOR + Tools::toString(_runNumber); \
                currentStreamLine += SEPARATOR + _discriminant1; \
                currentStreamLine += SEPARATOR + _discriminant2; \
                currentStreamLine += SEPARATOR + _discriminant3; \
                currentStreamLine += SEPARATOR + _discriminant4; \
                vStreamMergedResults.push_back(currentStreamLine); \
            } \
        }

namespace Results
{    
    class Result
    {
    private :
        DECLARE_SERIALIZE_STD_METHODS;

        Result(){};// For serialization

    protected:
        std::string _discriminant1;
        std::string _discriminant2;
        std::string _discriminant3;
        std::string _discriminant4;
        boost::gregorian::date _beginDate;
        unsigned int _runNumber;
        
        virtual void concrete() = 0; // To ensure that it will not be instantiated
        
    public:
        std::string getStrCampaign(const boost::gregorian::date &date);
        Result(std::string &discriminant1, std::string &discriminant2, std::string &discriminant3, std::string &discriminant4, boost::gregorian::date &beginDate, unsigned int runNumber);
        virtual ~Result();// destructor
#ifdef _LOG
//        virtual void resetCalibrationValues(){}
#endif // _LOG 
#if defined _LOG || defined _FULL_RESULTS
        virtual void runResultsToFile(const std::string &path);
#endif // _LOG || _FULL_RESULTS
        virtual void getResultStreams(std::map<std::string, std::vector<std::string>> &mStrResults, bool &columnNamesAlreadyGenerated) = 0;
    }; /* End of class Result */
} /* End of namespace Results */
#endif // Results_Result_h
