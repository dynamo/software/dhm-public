//////////////////////////////////////////
//                                      //
// File : AccountingModel.cpp           //
//                                      //
// Author : Philippe Gontier            //
//                                      //
// Version 1.0                          //
//                                      //
// Date : 01/10/2015                    //
//                                      //
//////////////////////////////////////////

#include "AccountingModel.h"

// project

BOOST_CLASS_EXPORT(Simulator::AccountingModel) // Recommended (mandatory if virtual serialization)

namespace Simulator
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_NVP(_hasOriginAccountingModelInfo); \

    IMPLEMENT_SERIALIZE_STD_METHODS(AccountingModel);

    AccountingModel::AccountingModel()
    {
    }
    
    AccountingModel::AccountingModel(ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &info)
    {
        _hasOriginAccountingModelInfo = info;
    }
    
    std::string &AccountingModel::getName()
    {
        return _hasOriginAccountingModelInfo.name;
    }
    
    ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &AccountingModel::getValues()
    {
        return _hasOriginAccountingModelInfo;
    }
    
    AccountingModel::~AccountingModel()
    {
    }
} /* End of namespace Simulator */

