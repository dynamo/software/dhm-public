//////////////////////////////////////////
//                                      //
// File : AccountingModel.h             //
//                                      //
// Author : Philippe Gontier            //
//                                      //
// Version 1.0                          //
//                                      //
// Date : 01/10/2015                    //
//                                      //
//////////////////////////////////////////

#ifndef Simulator_AccountingModel_h
#define Simulator_AccountingModel_h

// standard
#include <string>
#include <vector>

// project
#include "../Tools/Serialization.h"
#include "../ExchangeInfoStructures/DairyFarmAccountingModelParameters.h"

namespace Simulator
{
    class AccountingModel
    {
        DECLARE_SERIALIZE_STD_METHODS;
        
    private:
        ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters _hasOriginAccountingModelInfo; // = nullptr;
        
    protected: 
        AccountingModel(); // Required for serialization
        
    public:
        AccountingModel(ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &info);
        std::string &getName();
        ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &getValues();
        virtual ~AccountingModel();
    };
} /* End of namespace Simulator */
#endif // Simulator_AccountingModel_h
