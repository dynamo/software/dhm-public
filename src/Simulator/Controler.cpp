////////////////////////////////////////
//                                    //
// File : Controler.cpp               //
//                                    //
// Author : Philippe Gontier          //
//                                    //
// Version 1.0                        //
//                                    //
// Date : 01/10/2015                  //
//                                    //
////////////////////////////////////////

#include "Controler.h"

// project
#ifdef _LOG
#include "../ExchangeInfoStructures/TechnicalConstants.h"
#endif // _LOG        
#include "../ExchangeInfoStructures/SystemToSimulateParameters.h"
#include "../ExchangeInfoStructures/AccountingModelParameters.h"
#include "../ExchangeInfoStructures/ProtocolParameters.h"
#include "../Tools/Tools.h"
#include "Model.h"

namespace Simulator
{    
    void Controler::initInstance()
    {
        // 1 : Quit the old instance
        delete s_pHasInstanceModel;

        // 2 : create the new empty
        s_pHasInstanceModel = new Model();
    }
    
    void Controler::initInstance(const std::string &fileName)
    {
        std::string message;
        return initInstance(fileName, message);
    }

    void Controler::initInstance(const std::string &fileName, std::string &message)
    {
        // 1 : Quit the old instance
        delete s_pHasInstanceModel;

        // 2 : create the new with user data
        s_pHasInstanceModel = new Model(fileName, message);
    } /* End of method initInstance */
    
    Model* Controler::getModel()
    {
        assert(s_pHasInstanceModel != nullptr);
        return s_pHasInstanceModel;  
    }
    
    bool Controler::save(const std::string &fileName)
    {
        bool res = true;
        try
        {
            getModel()->saveData(fileName);
        }
        catch (std::exception &e)
        {
            res = false;
        }   
        return res;
    }
    
    void Controler::leave()
    {
        delete s_pHasInstanceModel;
        s_pHasInstanceModel = nullptr;
    }
    
    void Controler::getSystemToSimulateParameterList(std::string &dairyFarmPath, std::map<std::string, ExchangeInfoStructures::Parameters::DairyFarmParameters> &mSystemToSimulateParameterList)
    {
        std::vector<std::string> fileList;
        Tools::getFileNamesFromDirectory(dairyFarmPath, fileList, (std::string&)TechnicalConstants::DATA_FILE_EXTENSION); 
        // List of farm parameters
        std::map<std::string, std::string> mFarmCollection; // All the known farms
        for (std::vector<std::string>::iterator it = fileList.begin(); it != fileList.end(); it++)
        {
            std::string fileNameWithPath = *it;

            // Formating the farm name based on the file name
            std::string farmName = fileNameWithPath;
            farmName = farmName.erase(0, dairyFarmPath.length()); // Remove the path
            farmName = farmName.erase(farmName.length() - TechnicalConstants::DATA_FILE_EXTENSION.length(), TechnicalConstants::DATA_FILE_EXTENSION.length()); // Remove the extension
            std::string geneticCataloguePath = TechnicalConstants::RELATIVE_GENETIC_CATALOGUE_DATA_DEFAULT_FOLDER;
            ExchangeInfoStructures::Parameters::DairyFarmParameters dairyFarmParameter(farmName, fileNameWithPath, geneticCataloguePath); // Dairy Farm parameters
            mSystemToSimulateParameterList[farmName] = dairyFarmParameter;
        }
    }

       
    void Controler::generateSystemToSimulate(ExchangeInfoStructures::Parameters::DairyFarmParameters &systemToSimulateInfo, unsigned int userId)
    {
        std::string message;
        return generateSystemToSimulate(systemToSimulateInfo, message, userId);  
    }
    
    void Controler::generateSystemToSimulate(ExchangeInfoStructures::Parameters::DairyFarmParameters &systemToSimulateInfo, std::string &message, unsigned int userId)
    {
        if (systemToSimulateInfo.autoControl(message))
        {
            getModel()->generateSystemToSimulate(userId, systemToSimulateInfo, message);  
        }
    }
   
    void Controler::deleteSystemToSimulate(std::string &name, unsigned int userId)
    {
        std::string message;
        deleteSystemToSimulate(name, message, userId);
    }
    
    void Controler::deleteSystemToSimulate(std::string &name, std::string &message, unsigned int userId)
    {
        getModel()->deleteSystemToSimulate(userId, name, message);
    }
        
    void Controler::createAccountingModel(ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &info, unsigned int userId)
    {
        std::string message;
        createAccountingModel(info, message, userId);
    }
    
    void Controler::createAccountingModel(ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &info, std::string &message, unsigned int userId)
    {
        if (info.autoControl(message))
        {
            getModel()->createAccountingModel(userId, info, message);
        }
    }

    void Controler::deleteAccountingModel(std::string &name, unsigned int userId)
    {
        std::string message;
        deleteAccountingModel(name, message, userId);
    }
    
    void Controler::deleteAccountingModel(std::string &name, std::string &message, unsigned int userId)
    {
        getModel()->deleteAccountingModel(userId, name, message);
    }
    
    void Controler::createProtocol(ExchangeInfoStructures::Parameters::ProtocolParameters &info, unsigned int userId)
    {
        std::string message;
        createProtocol(info, message, userId);
    }
    
    void Controler::createProtocol(ExchangeInfoStructures::Parameters::ProtocolParameters &info, std::string &message, unsigned int userId)
    {
        if (info.autoControl(message))
        {
            getModel()->createProtocol(userId, info, message);
        }
    }
        
    void Controler::getProtocolParameters(ExchangeInfoStructures::Parameters::ProtocolParameters &info, unsigned int userId)
    {
       std::string message;
       getProtocolParameters(info, message, userId);
    }
   
    void Controler::getProtocolParameters(ExchangeInfoStructures::Parameters::ProtocolParameters &info, std::string &message, unsigned int userId)
    {
       getModel()->getProtocolParameters(userId, info, message);
    }
   
    void Controler::deleteProtocol(std::string &name, unsigned int userId)
    {
        std::string message;
        deleteProtocol(name, message, userId);
    }
    
    void Controler::deleteProtocol(std::string &name, std::string &message, unsigned int userId)
    {
        getModel()->deleteProtocol(userId, name, message);
    }
    
        
    void Controler::simulateAllProtocols(const std::string &theResultPath, bool maintainPreviousResultsBeforeSimulation, unsigned int userId)
    {
#ifdef _LOG
            std::string logPath = theResultPath + TechnicalConstants::LOG_DIRECTORY;
            mkdir(logPath.c_str() DIR_RIGHT);  
#endif // _LOG        
        getModel()->simulateAllProtocols(userId, theResultPath, maintainPreviousResultsBeforeSimulation);
    }    
    
    void Controler::simulateProtocols(std::vector<std::string> &vProtocols, const std::string &theResultPath, unsigned int userId)
    {
        std::string message;
        simulateProtocols(vProtocols, theResultPath, message, userId);
    }    
    
    void Controler::simulateProtocols(std::vector<std::string> &vProtocols, const std::string &theResultPath, std::string &message, unsigned int userId)
    {
        getModel()->simulateProtocols(userId, vProtocols, theResultPath, message);
    }    
    
    void Controler::simulateProtocols(std::vector<std::string> &vProtocols, std::map<std::string, std::vector<std::string>> &mStreamResults, std::string &message
#if defined _LOG || defined _FULL_RESULTS
                                      , const std::string &theResultPath
#endif // _LOG || _FULL_RESULTS
                                      , unsigned int userId)
    {
        getModel()->simulateProtocols(userId, vProtocols, mStreamResults, message
#if defined _LOG || defined _FULL_RESULTS
                                      , theResultPath
#endif // _LOG || _FULL_RESULTS
                                     );
    }    
    
    Model* Controler::s_pHasInstanceModel = nullptr; 
} /* End of namespace Simulator */