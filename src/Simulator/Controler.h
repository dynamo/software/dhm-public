////////////////////////////////////////
//                                    //
// File : Controler.h                 //
//                                    //
// Author : Philippe Gontier          //
//                                    //
// Version 1.0                        //
//                                    //
// Date : 01/10/2015                  //
//                                    //
////////////////////////////////////////

#ifndef Simulator_Controler_h
#define Simulator_Controler_h

// standard
#include <vector>
#include <string>
#include <map>

namespace ExchangeInfoStructures
{
    namespace Parameters
    {
        class DairyFarmAccountingModelParameters;
        class DairyFarmParameters;
        class ProtocolParameters;
    }
}

namespace Simulator
{
    class Model;
    
    /// This class make it possible to simulate the husbandry system 
    class Controler
    {
        static Model* s_pHasInstanceModel;

    private:
        static Model* getModel(); // Give the single sub instance of the model

    public:
         
        // Simulator instance
        // ------------------
        /// Create a new empty instance
        static void initInstance();
        
        /// Create a new instance with data loading
        static void initInstance(
                                    /// Name of the file to import, with path and without extension
                                    const std::string &fileName);
         
        /// Create a new instance with data loading, reading potential error message
        static void initInstance(
                                    /// Name of the file to import, with path and without extension
                                    const std::string &fileName,
                                    /// Potential error message
                                    std::string &message); // new instance with data loading
        
        /// Save the current model data
        static bool save(
                                    /// Name of the file to save, with path and without extension
                                    const std::string &fileName);

        /// Leave the simulation and clean the memory
        static void leave();
        
        // Husbandry system
        // ----------------
        
        /// Get the list of existing system to simulate parameter files
        static void getSystemToSimulateParameterList(std::string &dairyFarmPath, std::map<std::string, ExchangeInfoStructures::Parameters::DairyFarmParameters> &mSystemToSimulateParameterList);

        /// Generate a system to simulate
        static void generateSystemToSimulate(
                                    /// System To Simulate parameters
                                    ExchangeInfoStructures::Parameters::DairyFarmParameters &systemToSimulateInfo,
                                    unsigned int userId = 0);  
        
        /// Generate a system to simulate, reading potential error message
        static void generateSystemToSimulate(
                                    /// Farm parameters
                                    ExchangeInfoStructures::Parameters::DairyFarmParameters &systemToSimulateInfo,        
                                    /// Potential error message
                                    std::string &message,
                                    unsigned int userId = 0);  
         
        /// Delete a System To Simulate
        static void deleteSystemToSimulate(
                                    /// Name of the Husbandry System to delete
                                    std::string &name, unsigned int userId = 0);
        
        /// Delete a System To Simulate, reading potential error message
        static void deleteSystemToSimulate(
                                    /// Name of the farm to delete
                                    std::string &name,
                                    /// Potential error message
                                    std::string &message, unsigned int userId = 0);
        
        // Accounting model
        // ----------------
        /// Create an accounting model
        static void createAccountingModel(
                                    /// Accounting model parameters
                                    ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &info, unsigned int userId = 0);
        
        /// Create an accounting model, reading potential error message
        static void createAccountingModel(
                                    /// Accounting model parameters
                                    ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &info,
                                    /// Potential error message
                                    std::string &message, unsigned int userId = 0); // Creating a Accounting Model
        
        /// Delete an accounting model
        static void deleteAccountingModel(
                                    /// Name of the accounting model to delete
                                    std::string &name, unsigned int userId = 0);
       
        /// Delete an accounting model, reading potential error message
        static void deleteAccountingModel(
                                    /// Name of the accounting model to delete
                                    std::string &name,
                                    /// Potential error message
                                    std::string &message, unsigned int userId = 0);
        
        // Protocol
        // --------
        /// Create a protocol
        static void createProtocol(
                                    /// Protocol parameters
                                    ExchangeInfoStructures::Parameters::ProtocolParameters &info, unsigned int userId = 0);
        
        /// Create a protocol, reading potential error message
        static void createProtocol(
                                    /// Protocol parameters
                                    ExchangeInfoStructures::Parameters::ProtocolParameters &info,
                                    /// Potential error message
                                    std::string &message, unsigned int userId = 0);
        
        /// Get a named protocol, reading potential error message
        static void getProtocolParameters(
                                    /// Protocol parameters to get
                                    ExchangeInfoStructures::Parameters::ProtocolParameters &info, unsigned int userId = 0);
         
        /// Get a named protocol, reading potential error message
        static void getProtocolParameters(
                                    /// Protocol parameters to get
                                    ExchangeInfoStructures::Parameters::ProtocolParameters &info,
                                    /// Potential error message
                                    std::string &message, unsigned int userId = 0);
        /// Delete a protocol
        static void deleteProtocol(
                                    /// Name of the protocol to delete
                                    std::string &name, unsigned int userId = 0);
       
        /// Delete a protocol, reading potential error message
        static void deleteProtocol(
                                    /// Name of the protocol to delete
                                    std::string &name,
                                    /// Potential error message
                                    std::string &message, unsigned int userId = 0);

        // Simulation
        // ----------
        /// Launch the simulation of all protocols
        static void simulateAllProtocols(
                                    /// Name of the path where put the results of the simulation
                                    const std::string &theResultPath, bool maintainPreviousResultsBeforeSimulation = false, unsigned int userId = 0);    
        
        /// Launch the simulation of some protocols and store results in files
        static void simulateProtocols(
                                   /// List of the names of protocols to simulate
                                   std::vector<std::string> &vProtocols,
                                    /// Name of the path where put the results of the simulation
                                   const std::string &theResultPath, unsigned int userId = 0);
        
        /// Launch the simulation of some protocols, reading potential error message
        static void simulateProtocols(
                                    /// List of the names of protocols to simulate
                                    std::vector<std::string> &vProtocols,
                                    /// Name of the path where put the results of the simulation
                                    const std::string &theResultPath,
                                    /// Potential error message
                                    std::string &message, unsigned int userId = 0);
        
        /// Launch the simulation of some protocols and store results in map
        static void simulateProtocols(
                                   /// List of the names of protocols to simulate
                                   std::vector<std::string> &vProtocols,
                                    /// Name of the path where put the results of the simulation
                                   std::map<std::string, std::vector<std::string>> &mStreamResults,
                                   /// Potential error message
                                   std::string &message
#if defined _LOG || defined _FULL_RESULTS
                                   , const std::string &theResultPath
#endif // _LOG || _FULL_RESULTS
                                   , unsigned int userId = 0);
        
        
    }; // Controler
} /* End of namespace Simulator */

#endif // Simulator_Controler_h
