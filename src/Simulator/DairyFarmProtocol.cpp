#include "DairyFarmProtocol.h"

// project
#include "../DataStructures/DairyFarm.h"

#ifdef _LOG
#include "../ExchangeInfoStructures/TechnicalConstants.h"
#include "../ResultStructures/DairyFarmResult.h"
#endif // _LOG      

BOOST_CLASS_EXPORT(Simulator::DairyFarmProtocol) // Recommended (mandatory if virtual serialization)

namespace Simulator
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Protocol); \

    IMPLEMENT_SERIALIZE_STD_METHODS(DairyFarmProtocol);

    DairyFarmProtocol::DairyFarmProtocol(ExchangeInfoStructures::Parameters::ProtocolParameters &ProtocolInfo, DataStructures::DairyFarm* pRo, AccountingModel* pAm) : Protocol(ProtocolInfo, pRo, pAm)
    {
    }
    
#ifdef _LOG
//    void DairyFarmProtocol::appendRunResults(Results::Result* pRes)
//    {
//        std::vector<float> runManagedHealthCosts = ((Results::DairyFarmResult*)pRes)->getManagedHealthCostsForCalibration();
//        _managedHealthCostsForCalibration.insert(_managedHealthCostsForCalibration.end(), runManagedHealthCosts.begin(), runManagedHealthCosts.end());
//    }
//    
    void DairyFarmProtocol::closeResults(const std::string path)
    {
        // Preparing the log folder
        std::string logPath = path + TechnicalConstants::LOG_DIRECTORY;
        mkdir(logPath.c_str() DIR_RIGHT);  
        
//        std::vector<std::string> linesForTheSaveFile;
//        std::string fileName = logPath + TechnicalConstants::MANAGED_HEALTH_COSTS_FOR_CALIBRATION_VALUES_FILE_NAME;
//        for (auto val : _managedHealthCostsForCalibration)
//        {
//            std::string line = Tools::toString(val);
//            linesForTheSaveFile.push_back(Tools::changeChar(line, '.', ','));
//        }     
//        Tools::writeFile(linesForTheSaveFile, fileName, true);        
        
//        // Clear results
//        _managedHealthCostsForCalibration.clear();
    }   
#endif // _LOG      
} /* End of namespace Simulator */

