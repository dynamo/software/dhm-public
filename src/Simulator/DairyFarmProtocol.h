#ifndef Simulator_DairyHerdProtocol_h
#define Simulator_DairyHerdProtocol_h

// project
#include "Protocol.h"

namespace DataStructures
{
    class DairyFarm;
}

namespace Simulator
{
    class DairyFarmProtocol : public Protocol
    {
        DECLARE_SERIALIZE_STD_METHODS;
        
#ifdef _LOG
//        std::vector<float> _managedHealthCostsForCalibration; // To calculate the calibration value for BASE_CALIBRATION_MANAGED_HEALTH_ANNUAL_COST_PER_COW
#endif // _LOG      

    protected: 
        DairyFarmProtocol(){}; // Required for serialization
        
    public:
        DairyFarmProtocol(ExchangeInfoStructures::Parameters::ProtocolParameters &ProtocolInfo, DataStructures::DairyFarm* pRo, AccountingModel* pAm);
        virtual ~DairyFarmProtocol(){}; // Required for serialization        
#ifdef _LOG
//        void appendRunResults(Results::Result* pRes) override;
        void closeResults(const std::string path) override;
#endif // _LOG      
    };
} /* End of namespace Simulator */
#endif // Simulator_DairyFarmProtocol_h
