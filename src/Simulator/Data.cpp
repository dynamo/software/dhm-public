////////////////////////////////////////
//                                    //
// File : Data.cpp                    //
//                                    //
// Author : Philippe Gontier          //
//                                    //
// Version 1.0                        //
//                                    //
// Date : 01/10/2015                  //
//                                    //
////////////////////////////////////////

#include "Data.h"

// project
#include "../DataStructures/DairyFarm.h"
#include "Protocol.h"
#include "AccountingModel.h"
#include "../ExchangeInfoStructures/LanguageDictionary.h"

BOOST_CLASS_EXPORT(Simulator::Data) // Recommended (mandatory if virtual serialization)

namespace Simulator
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_NVP(_mpHasAccountingModels); \
        ar & BOOST_SERIALIZATION_NVP(_mpHasProtocols); \
        ar & BOOST_SERIALIZATION_NVP(_mpManageSystemsToSimulate);
        
    IMPLEMENT_SERIALIZE_STD_METHODS(Data);

    Data::Data()
    {
    }
    
    Data::~Data()
    {
        // Systems to simulate
        for (auto itSTS : _mpManageSystemsToSimulate)
        {
            delete itSTS.second;
        }
        _mpManageSystemsToSimulate.clear();
        
        // Accounting Model Composed
        for (auto itAc : _mpHasAccountingModels)
        {
            delete itAc.second;
        }
        _mpHasAccountingModels.clear();
        
        // Protocol Composed
        for (auto itProt : _mpHasProtocols)
        {
            delete itProt.second;
        }
        _mpHasProtocols.clear();
    }
    
    void Data::addSystemToSimulate(DataStructures::DairyFarm* pSystemToSimulate)
    {
        assert (_mpManageSystemsToSimulate.find(pSystemToSimulate->getName()) == _mpManageSystemsToSimulate.end());
        _mpManageSystemsToSimulate[pSystemToSimulate->getName()] = pSystemToSimulate;
    }
    
    DataStructures::DairyFarm* Data::getSystemToSimulate(std::string name)
    {
        auto it = _mpManageSystemsToSimulate.find(name);
        if (it == _mpManageSystemsToSimulate.end())
        {
            return nullptr;
        }
        else
        {
            return it->second;
        }
    }
    void Data::deleteSystemToSimulate(std::string name, std::string &message)
    {
        auto it = _mpManageSystemsToSimulate.find(name);
        if (it != _mpManageSystemsToSimulate.end())
        {
            DataStructures::DairyFarm* pDF = it->second;
            deleteProtocolsOfSystemToSimulate(pDF);
            delete pDF;
            _mpManageSystemsToSimulate.erase(it);
        }
        else
        {
            message = ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_THE_SYSTEM_TO_SIMULATE) + name + ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_IS_UNKNOWN);
        }
    }
   
    void Data::addAccountingModel(AccountingModel* pAccountingModel)
    {
        if (_mpHasAccountingModels.find(pAccountingModel->getName()) == _mpHasAccountingModels.end())
        {
            _mpHasAccountingModels[pAccountingModel->getName()] = pAccountingModel;
        }
    }

    unsigned int Data::getAccountingModelNumber()
    {
        return _mpHasAccountingModels.size();
    }
    
    AccountingModel* Data::getAccountingModel(std::string name)
    {
        if (_mpHasAccountingModels.find(name) != _mpHasAccountingModels.end())
        {
            return _mpHasAccountingModels.find(name)->second;
        }
        else
        {
            return nullptr;
        }
    }

    void Data::deleteAccountingModel(std::string name, std::string &message)
    {
        auto it = _mpHasAccountingModels.find(name);
        if (it != _mpHasAccountingModels.end())
        {
            AccountingModel* pAM = it->second;
            deleteProtocolsOfAccountingModel(pAM);
            delete (it->second);
            _mpHasAccountingModels.erase(it);
        }
        else
        {
            message = ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_THE_ACCOUNTING_MODEL) + name + ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_IS_UNKNOWN);
        }
    } /* End of method deleteAccountingModel */

    void Data::addProtocol(Protocol* pProtocol)
    {
        if (_mpHasProtocols.find(pProtocol->getName()) == _mpHasProtocols.end())
        {
            _mpHasProtocols[pProtocol->getName()] = pProtocol;
        }
    }

    unsigned int Data::getProtocolNumber()
    {
        return _mpHasProtocols.size();
    }
    
    Protocol* Data::getProtocol(std::string name)
    {
        if (_mpHasProtocols.find(name) != _mpHasProtocols.end())
        {
            return _mpHasProtocols.find(name)->second;
        }
        else
        {
            return nullptr;
        }
    }       
        
    void Data::getAllProtocols(std::vector<Protocol*> &protocols)
    {
        for (auto itP : _mpHasProtocols)
        {
            protocols.push_back(itP.second);
        }
    }

    void Data::deleteProtocolsOfAccountingModel(AccountingModel* pAM)
    {
        for (std::map<std::string, Protocol*>::iterator itP = _mpHasProtocols.begin(); itP != _mpHasProtocols.end(); )
        {
            if ((itP->second)->getAccountingModel() == pAM) 
            {
                delete itP->second; // Delete the protocol
                itP = _mpHasProtocols.erase(itP); // Remove from the protocol list the protocol
            }
            else
            {
                itP++;
            }
        }
    }

    void Data::deleteProtocol(std::string name, std::string &message)
    {
        auto it = _mpHasProtocols.find(name);
        if (it != _mpHasProtocols.end())
        {
            delete (it->second);
            _mpHasProtocols.erase(it);
        }
        else
        {
            // Unknown protocol
            message = ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_THE_PROTOCOL) + name + ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_IS_UNKNOWN);
        }
    } /* End of method deleteProtocol */
    
    void Data::deleteProtocolsOfSystemToSimulate(DataStructures::DairyFarm* pSTS)
    {
        for (std::map<std::string, Protocol*>::iterator itP = _mpHasProtocols.begin(); itP != _mpHasProtocols.end(); )
        {
            if ((itP->second)->getSystemToSimulateName() == pSTS->getName()) 
            {
                delete itP->second; // Delete the protocol
                itP = _mpHasProtocols.erase(itP); // Remove from the protocol list the protocol
            }
            else
            {
                itP++;
            }
        }
    }
}