////////////////////////////////////////
//                                    //
// File : Data.h                      //
//                                    //
// Author : Philippe Gontier          //
//                                    //
// Version 1.0                        //
//                                    //
// Date : 01/10/2015                  //
//                                    //
////////////////////////////////////////

#ifndef Simulator_Data_h
#define Simulator_Data_h

// std
#include <string>
#include <vector>

// boost

// project
#include "../Tools/Serialization.h"

namespace DataStructures
{
    class DairyFarm;
    
} /* End of namespace DataStructures */

namespace Simulator
{
    class Protocol;
    class AccountingModel;
    
    class Data
    {
        DECLARE_SERIALIZE_STD_METHODS;
        
    private:
        
    protected: 
        
    public:
        Data(); // Required for serialization
        virtual ~Data(); // Required for serialization
 
        std::map<std::string, DataStructures::DairyFarm*> _mpManageSystemsToSimulate;
        std::map<std::string, AccountingModel*> _mpHasAccountingModels; // List of known Accounting Models
        std::map<std::string, Protocol*> _mpHasProtocols;

        // System to simulate
        void addSystemToSimulate(DataStructures::DairyFarm* DairyFarm);
        DataStructures::DairyFarm* getSystemToSimulate(std::string name);
        void deleteSystemToSimulate(std::string name, std::string &message);

        // Accounting model
        void addAccountingModel(AccountingModel* pAccountingModel);
        unsigned int getAccountingModelNumber();
        AccountingModel* getAccountingModel(std::string name);
        void deleteAccountingModel(std::string name, std::string &message);
        
        // Protocol
        void addProtocol(Protocol* pProtocol);
        unsigned int getProtocolNumber();
        Protocol* getProtocol(std::string name);       
        void getAllProtocols(std::vector<Protocol*> &protocols);
        void deleteProtocolsOfAccountingModel(AccountingModel* pAM);
        void deleteProtocol(std::string name, std::string &message);
        void deleteProtocolsOfSystemToSimulate(DataStructures::DairyFarm* pSTS);   
    };
} /* End of namespace Simulator */

#endif // Simulator_Data_h
