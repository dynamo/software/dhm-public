#include "Model.h"

// Sstandard
#include <stdio.h>

// Project
#include "../ExchangeInfoStructures/LanguageDictionary.h"
#include "../ExchangeInfoStructures/TechnicalConstants.h"
#include "../ExchangeInfoStructures/ProtocolParameters.h"
#include "../Tools/Tools.h"
#include "Run.h"
#include "../Genetic/GeneticManagement.h"
#include "../Health/HealthManagement.h"
#include "../DataStructures/DairyFarm.h"
#include "../DataStructures/DairyMammal.h"
#include "../ExchangeInfoStructures/EnDairyFarmDictionary.h"
#include "DairyFarmProtocol.h"
#include "../Tools/Display.h"

namespace Simulator
{
    // Constructor without data
    Model::Model()
    {
        preProcess();
    } /* End of constructor */
    
    Model::~Model()
    {
        ExchangeInfoStructures::LanguageDictionary::clear();

        // Deleting data
        for (std::map<unsigned int,Data*>::iterator it = _mpHasData.begin(); it != _mpHasData.end(); it++)
        {
            delete it->second;
        }
        _mpHasData.clear();
        
        postProcess(); // Post-process
        // ...
    }
    
    DataStructures::DairyFarm* Model::getSystemToSimulate(unsigned int userId, std::string &name)
    {
        return getData(userId)->getSystemToSimulate(name);
    }

    // Constructor of deserialization
    Model::Model(const std::string &fileName, std::string &message)
    {
        // Data deserialization
        std::string fullFileName = fileName + TechnicalConstants::BINARY_ARCHIVE_EXTENSION;
        try
        {
            std::ifstream ifs(fullFileName, std::ios_base::binary); // Create the file stream
            boost::archive::binary_iarchive ia(ifs); // Declare the binary archive            
            ia >> BOOST_SERIALIZATION_NVP(_mpHasData);
            ifs.close(); // Closing the file
        }
        catch (boost::archive::archive_exception &e)
        {
            message = "Can't load file " + fullFileName;
            std::string displayMessage("->Load exception: ");
            displayMessage += e.what();
            Console::Display::displayLineInConsole(displayMessage);
        }
        
        // global initializations,
        preProcess();
    }

    void Model::deleteSystemToSimulate(unsigned int userId, std::string &name, std::string &message)
    {
        getData(userId)->deleteSystemToSimulate(name, message);
    }
    
    AccountingModel* Model::getAccountingModel(unsigned int userId, std::string &name)
    {
        return getData(userId)->getAccountingModel(name);
    }

    // Delete an Accounting Model
    void Model::deleteAccountingModel(unsigned int userId, std::string &name, std::string &message)
    {
        getData(userId)->deleteAccountingModel(name, message);
    }

    unsigned int Model::getAccountingModelNumber(unsigned int userId)
    {
        return getData(userId)->getProtocolNumber();
    }

    void Model::createProtocol(unsigned int userId, ExchangeInfoStructures::Parameters::ProtocolParameters &info, std::string &message)
    {        
        if (getData(userId)->getProtocol(info.name) == nullptr)
        {
            // Dairy farm
            DataStructures::DairyFarm* pDF = nullptr;
            if (info.runableName != "")
            {
                pDF = getSystemToSimulate(userId, info.runableName);
            }
            else
            {
                // No specific dairy farm for this protocol, so we have to use the default one
                // Do the default dairy farm exists ?
                pDF = getSystemToSimulate(userId, (std::string&)TechnicalConstants::DEFAULT_DAIRY_FARM_NAME);
                if (pDF == nullptr)
                {
                    // It doesn't exists, we have to create it
                    ExchangeInfoStructures::Parameters::DairyFarmParameters iDefaultFarmInfo;
                    iDefaultFarmInfo.name = TechnicalConstants::DEFAULT_DAIRY_FARM_NAME;
                    generateSystemToSimulate(userId, iDefaultFarmInfo, message);
                    pDF = getSystemToSimulate(userId, iDefaultFarmInfo.name);
                }
            }
 
            // Accounting model
            if (pDF != nullptr)
            {
                AccountingModel* pAm = nullptr;
                if (info.accountingModelName != "")
                {
                    pAm = getAccountingModel(userId, info.accountingModelName);
                }
                else
                {
                    // No specific accounting model for this protocol, so we have to use the default one
                    pAm = getAccountingModel(userId, (std::string&)TechnicalConstants::DEFAULT_ACCOUNTING_MODEL_NAME);
                    if (pAm == nullptr)
                    {
                        // It doesn't exists, we have to create it
                        ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters iAccountingModelParameters;
                        iAccountingModelParameters.name = TechnicalConstants::DEFAULT_ACCOUNTING_MODEL_NAME;
                        createAccountingModel(userId, iAccountingModelParameters, message);
                        pAm = getAccountingModel(userId, iAccountingModelParameters.name);
                    }
                }
                // Create the asked protocol
                if (pAm != nullptr)
                {
                    getData(userId)->addProtocol(createNewProtocol(info, pDF, pAm)); // Add to the Protocol collection
                }
                else
                {
                    // Accounting model name unknown
                    message = ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_THE_ACCOUNTING_MODEL) + info.accountingModelName + ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_IS_UNKNOWN);
                }   
            }
            else
            {
                // Farm exploitation name unknown
                message = ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_THE_SYSTEM_TO_SIMULATE) + info.runableName + ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_IS_UNKNOWN);               
            }
        }
        else
        {
            // Protocol name already exists
            message = ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_THE_PROTOCOL) + info.name + ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_ALREADY_EXISTS);
        }
    }
    
    Protocol* Model::getProtocol(unsigned int userId, std::string &name)
    {
        return getData(userId)->getProtocol(name);
    }       
    
    void Model::getAllProtocols(unsigned int userId, std::vector<Protocol*> &protocols)
    {
        getData(userId)->getAllProtocols(protocols);
    }
    
    unsigned int Model::getProtocolNumber(unsigned int userId)
    {
        return getData(userId)->getProtocolNumber();
    }
    
    void Model::getProtocolParameters(unsigned int userId, ExchangeInfoStructures::Parameters::ProtocolParameters &info, std::string &message)
    {
        Protocol* pProt = getData(userId)->getProtocol(info.name);
        if (pProt != nullptr)
        {
           info = pProt->getProtocolInfo();
        }
        else
        {
            // Unknown protocol
            message = ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_THE_PROTOCOL) + info.name + ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_IS_UNKNOWN);
        }
    }
    
    void Model::deleteProtocol(unsigned int userId, std::string &name, std::string &message)
    {
        getData(userId)->deleteProtocol(name, message);
    }

    void Model::simulateProtocols(unsigned int userId, std::vector<std::string> &vProtocols, const std::string &theResultPath, std::string &message)
    {
        // Creating the result path
        mkdir(theResultPath.c_str() DIR_RIGHT);
      
        // Prepare result string list 
        deleteAllFileResults(theResultPath, userId);
        std::map<std::string, std::vector<std::string>> mStreamResults;
         
        // Launch the protocols
        simulateProtocols(userId, vProtocols, mStreamResults, message
#if defined _LOG || defined _FULL_RESULTS
                        , theResultPath
#endif // _LOG || _FULL_RESULTS
                        );
                
        // Write result files
        for (auto it : mStreamResults)
        {
            Tools::writeFile(it.second, theResultPath + it.first, true);        
        }
    }
    
    void Model::simulateProtocols(unsigned int userId, std::vector<std::string> &vProtocols, std::map<std::string, std::vector<std::string>> &mStreamResults, std::string &message
#if defined _LOG || defined _FULL_RESULTS
                        , const std::string &theResultPath
#endif // _LOG || _FULL_RESULTS
                                )
    {
        bool columnNamesAlreadyGenerated = false;
        mStreamResults = getResultFileList(userId);
         
        // Launch the protocols
#ifndef _MONORUN
        for (unsigned int indProtocol = 0; indProtocol < vProtocols.size(); indProtocol++)
#else // _MONORUN
        unsigned int indProtocol = 0;    
#endif // _MONORUN
        {
            Protocol * pP = getProtocol(userId, vProtocols[indProtocol]);
            if (nullptr != pP)
            {
                std::ostringstream s1;
                s1 << indProtocol + 1 << "/" << vProtocols.size();
                std::string strCount = s1.str();
                simulateProtocol(userId, pP, mStreamResults, strCount, columnNamesAlreadyGenerated
#if defined _LOG || defined _FULL_RESULTS
                        , theResultPath
#endif // _LOG || _FULL_RESULTS
                                );
            }
            else
            {
                // Unknown protocol
                message = ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_THE_PROTOCOL) + vProtocols[indProtocol] + ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_IS_UNKNOWN);
            }
        }
    }    
    
    
    void Model::deleteAllFileResults(const std::string &theResultPath, unsigned int userId)
    {
    
        std::map<std::string, std::vector<std::string>> vStrResults = getResultFileList(userId);
        for (auto it : vStrResults)
        {
            std::string fileName = theResultPath + it.first;
            remove(fileName.c_str());
        }
    }

    void Model::simulateAllProtocols(unsigned int userId, const std::string &theResultPath, bool maintainPreviousResultsBeforeSimulation)
    {
#ifdef _LOG
        doBeforeSimulation(theResultPath);
#endif // _LOG
        // Creating the result path
        mkdir(theResultPath.c_str() DIR_RIGHT);
        std::map<std::string, std::vector<std::string>> mStreamResults = getResultFileList(userId);
        
        // Lauch the protocols
        std::vector<Protocol*> vProtocols;
        getData(userId)->getAllProtocols(vProtocols);

        // Prepare result string list 
        if (not maintainPreviousResultsBeforeSimulation)
        {
            deleteAllFileResults(theResultPath, userId);
        }
        bool columnNamesAlreadyGenerated = maintainPreviousResultsBeforeSimulation;
        
        for (unsigned int indProtocol = 0; indProtocol < vProtocols.size(); indProtocol++)
        {
            std::ostringstream s1;
            s1 << indProtocol + 1 << "/" << vProtocols.size();
            std::string strCount = s1.str();
            simulateProtocol(userId, vProtocols[indProtocol], mStreamResults, strCount, columnNamesAlreadyGenerated
#if defined _LOG || defined _FULL_RESULTS
                            , theResultPath
#endif // _LOG || _FULL_RESULTS
                            );
        }
                
        // Write result files
        for (auto it : mStreamResults)
        {
            Tools::writeFile(it.second, theResultPath + it.first, false);        
        }
    }    

    // Parallelized deserialization
    void Model::simulateProtocol(unsigned int userId, Protocol* pP, std::map<std::string, std::vector<std::string>> &mStreamResults, std::string &strCount, bool &columnNamesAlreadyGenerated
#if defined _LOG || defined _FULL_RESULTS
                        , const std::string &theResultPath
#endif // _LOG || _FULL_RESULTS
                                )
    {        
        Console::Display::displayLineInConsole("Running protocol " + strCount + ", " + pP->getName() + ", " + pP->getComment());
        
#ifdef _LOG
        // Creating the protocol result directory
        const std::string PROTOCOL_RESULT_PATH = theResultPath + pP->getName() + '/';
        mkdir(PROTOCOL_RESULT_PATH.c_str() DIR_RIGHT);
#endif // _LOG
       
#ifndef _MONORUN
        // Serialization of the original object to simulate
        std::stringstream creatStream(std::ios_base::binary| std::ios_base::out| std::ios_base::in); // Stream of the only one serialization of the System to simulate (binary)
        serializeSystemToBeSimulated(userId, creatStream, pP); // The System To be Simulate so to simulate
#endif // _MONORUN

        // Run management
#ifndef _LOG
        Console::Display::displayLineInConsole("  Target: ", Console::ANSI_RESET, false);
#ifndef _MONORUN
        for (unsigned int i = 0; i < pP->getRunNumber(); i++)
#endif // _MONORUN
        {
            Console::Display::displayLineInConsole(".", Console::ANSI_RESET, false);
        }
        Console::Display::displayLineInConsole("");
        Console::Display::displayLineInConsole("  Done  : ", Console::ANSI_RESET, false);
#endif // _LOG
        
#if(!defined _LOG && !defined _MONORUN)
#pragma omp parallel for // Parallel run time for release mode only
#endif // !_LOG and !_MONORUN
#ifndef _MONORUN
        for (unsigned int currentRunIndex = 0; currentRunIndex <  pP->getRunNumber(); currentRunIndex++)
        {
            std::stringstream runStream(creatStream.str());
            Run* pTheRun = new Run(deserializeSystemToBeSimulated(runStream), pP, getTimeLapsDuration(), currentRunIndex 
#else // _MONORUN
        {
            unsigned int currentRunIndex = 0;
            Run* pTheRun = new Run((DataStructures::DairyFarm*)(getData(userId)->getSystemToSimulate(pP->getSystemToSimulateName())), pP, getTimeLapsDuration(), currentRunIndex                     
#endif // _MONORUN      
#ifndef _LOG
                                                                                    , getNewResult(pP->getDiscriminant1(), pP->getDiscriminant2(), pP->getDiscriminant3(), pP->getDiscriminant4(), pP->getBeginDate(), currentRunIndex)
#endif // _LOG      
                                                                                    , getNewResult(pP->getDiscriminant1(), pP->getDiscriminant2(), pP->getDiscriminant3(), pP->getDiscriminant4(), pP->getBeginDate(), currentRunIndex)); // run creation with the results to obtain    
#ifdef _LOG
            // tested code        
            DataStructures::SystemToSimulate* pDairyFarm = loadFarmData("D:/Simulateur/ForgeMIA/dhm-dev/data/BufferizedRuns/irun0");    
            Run* pTheTestRun = new Run(pDairyFarm, currentRunIndex                     
                                                                                    , getNewResult(pP->getDiscriminant1(), pP->getDiscriminant2(), pP->getDiscriminant3(), pP->getDiscriminant4(), pP->getBeginDate(), currentRunIndex)
                                                                                    , getNewResult(pP->getDiscriminant1(), pP->getDiscriminant2(), pP->getDiscriminant3(), pP->getDiscriminant4(), pP->getBeginDate(), currentRunIndex));

            Run* pTheRunToRun = pTheRun;
            //Run* pTheRunToRun = pTheTestRun;
#else // _LOG
            Run* pTheRunToRun = pTheRun; 
#endif // _LOG
            
            pTheRunToRun->action(
#ifdef _LOG
            PROTOCOL_RESULT_PATH
#endif // _LOG
                            );

            // Results
            Results::Result* pRes = pTheRunToRun->getObtainedResults();
          
            // Merging
#if(!defined _LOG && !defined _MONORUN)
#pragma omp critical
#endif // !_LOG and !_MONORUN
            {
                pRes->getResultStreams(mStreamResults, columnNamesAlreadyGenerated);                

            }
#ifdef _LOG  
            pP->appendRunResults(pRes);
#endif // _LOG      
            // Delete run
            delete pTheRunToRun; // delete because finished           
        }
#ifndef _LOG
        Console::Display::displayLineInConsole("");
#else
        pP->closeResults(PROTOCOL_RESULT_PATH);
#endif // _LOG      
    }
    
    void Model::saveData(const std::string &fileName)
    {
        try
        {
            std::string strFullFileName = fileName + TechnicalConstants::BINARY_ARCHIVE_EXTENSION;
            std::ofstream ofs(strFullFileName, std::ios_base::binary); // Create the file stream
            boost::archive::binary_oarchive oa(ofs); // Declare the binary archive            
            oa << BOOST_SERIALIZATION_NVP(_mpHasData);
            ofs.close(); // Closing the file
        }
        catch (std::exception &e)
        {
            std::string displayMessage("->Save exception: ");
            displayMessage += e.what();
            Console::Display::displayLineInConsole(displayMessage);
        }
    } 
        
    void Model::preProcess()
    {
        // static init
        // -----------

        // Data folder
        std::string technicalDataFolder = Tools::getExecutableFolder() + TechnicalConstants::RELATIVE_TECHNICAL_DATA_FOLDER;

        // Genetic
        Genetic::GeneticManagement::init();

        // Mastitis
        Health::HealthManagement::initMastitisRiskPeriodMatrix(technicalDataFolder);

        // Ketosis
        Health::HealthManagement::initKetosisRiskPeriodCurve(technicalDataFolder);

        // Lameness
        Health::HealthManagement::initLamenessRiskLactationPeriodCurve(technicalDataFolder);
        Health::HealthManagement::initLamenessStabulationRiskCurve(technicalDataFolder);

        // Languages
        new ExchangeInfoStructures::EnDairyFarmDictionary(); // English

        // Mortality
        DataStructures::DairyMammal::initAgeToMortalityRiskAgeMatrix();
    }

    void Model::postProcess()
    {
        // Languages
        ExchangeInfoStructures::LanguageDictionary::clear();
    }   
    void Model::serializeSystemToBeSimulated(unsigned int userId, std::stringstream &creatStream, Protocol* pP)
    {
        std::string systemName = pP->getSystemToSimulateName();
        DataStructures::DairyFarm* pSTS = (DataStructures::DairyFarm*)(getData(userId)->getSystemToSimulate(systemName));
        pSTS->displayInitialSystemInformations();
        boost::archive::binary_oarchive tss(creatStream);
        tss << BOOST_SERIALIZATION_NVP(pSTS);
    }
    
    DataStructures::SystemToSimulate* Model::deserializeSystemToBeSimulated(std::stringstream &creatStream)
    {
        DataStructures::DairyFarm* pSTS;
        creatStream.seekg(0);
        boost::archive::binary_iarchive ia(creatStream);
        ia >> BOOST_SERIALIZATION_NVP(pSTS);
        creatStream.seekg(0);
        return pSTS;
    }
    
    Results::DairyFarmResult* Model::getNewResult(std::string &discriminant1, std::string &discriminant2, std::string &discriminant3, std::string &discriminant4, boost::gregorian::date &beginDate, unsigned int currentRun)
    {
        return new Results::DairyFarmResult(discriminant1, discriminant2, discriminant3, discriminant4, beginDate, currentRun);
    }
    
    void Model::generateSystemToSimulate(unsigned int userId, ExchangeInfoStructures::Parameters::DairyFarmParameters &systemToSimulateInfo, std::string &message)
    {
        if (getData(userId)->getSystemToSimulate(systemToSimulateInfo.name) == nullptr)
        {
            DataStructures::DairyFarm* pDF = new DataStructures::DairyFarm(systemToSimulateInfo);
            getData(userId)->addSystemToSimulate(pDF);
        }
        else
        {
            message = ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_THE_SYSTEM_TO_SIMULATE) + systemToSimulateInfo.name + ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_ALREADY_EXISTS);
        }
    }
    
    Protocol* Model::createNewProtocol(ExchangeInfoStructures::Parameters::ProtocolParameters &info, DataStructures::DairyFarm* pDF, AccountingModel* pAm)
    {
        return new Simulator::DairyFarmProtocol(info, pDF, pAm);
    }
    
    void Model::createAccountingModel(unsigned int userId, ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &info, std::string &message)
    {
        if (getData(userId)->getAccountingModel(info.name) == nullptr)
        {
            getData(userId)->_mpHasAccountingModels[info.name] = new AccountingModel(info);
        }
        else
        {
            message = ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_THE_ACCOUNTING_MODEL) + info.name + ExchangeInfoStructures::LanguageDictionary::getMessage(ExchangeInfoStructures::KEY_ALREADY_EXISTS);
        }
    }

    Data* Model::getData(unsigned int userId)
    {
        Data* res = nullptr;
        std::map<unsigned int, Data*>::iterator it = _mpHasData.find(userId);
        if (_mpHasData.find(userId) == _mpHasData.end())
        {
            res = new Data();
            _mpHasData[userId] = res;
        }
        else
        { 
            res = (Data*)(it->second);
        }
        return res;
    } 
    
    boost::gregorian::date_duration Model::getTimeLapsDuration()
    {
        boost::gregorian::date_duration res(TechnicalConstants::TIME_LAPS_DURATION);
        return res;
    }
    
    std::map<std::string, std::vector<std::string>> Model::getResultFileList(unsigned int userId)
    {
        std::map<std::string, std::vector<std::string>> mResultFileList;  

        // First = file result name without path but with extention 
        mResultFileList[TechnicalConstants::TECHNICAL_AND_ECONOMICAL_RESULT_FILE_NAME] = std::vector<std::string>(); // Technical Reproduction Merged Results
        mResultFileList[TechnicalConstants::MILK_CONTROL_RESULT_FILE_NAME] = std::vector<std::string>(); // Milk Control Merged Results
    #ifdef _LOG
        mResultFileList[TechnicalConstants::DEATH_RESULT_FILE_NAME] = std::vector<std::string>(); // Death Merged Results
        mResultFileList[TechnicalConstants::EXIT_RESULT_FILE_NAME] = std::vector<std::string>(); // Exit Merged Results
    #endif // _LOG
#if defined _LOG || defined _FULL_RESULTS
        mResultFileList[TechnicalConstants::PRIMIPARE_REPRODUCTION_RESULTS_BASED_ON_CALVING_FILE_NAME] = std::vector<std::string>();
        mResultFileList[TechnicalConstants::MULTIPARE_REPRODUCTION_RESULTS_BASED_ON_CALVING_FILE_NAME] = std::vector<std::string>();
        mResultFileList[TechnicalConstants::NULLIPARE_REPRODUCTION_RESULTS_BASED_ON_INSEMINATION_FILE_NAME] = std::vector<std::string>();
        mResultFileList[TechnicalConstants::PRIMIMULTIPARE_REPRODUCTION_RESULTS_BASED_ON_INSEMINATION_FILE_NAME] = std::vector<std::string>();
        mResultFileList[TechnicalConstants::DELIVERED_MILK_RESULTS_FILE_NAME] = std::vector<std::string>();
        mResultFileList[TechnicalConstants::DISCARDED_MILK_RESULTS_FILE_NAME] = std::vector<std::string>();
        mResultFileList[TechnicalConstants::ANNUAL_MASTITIS_RESULTS_FILE_NAME] = std::vector<std::string>();
        mResultFileList[TechnicalConstants::ANNUAL_KETOSIS_RESULTS_FILE_NAME] = std::vector<std::string>();
        mResultFileList[TechnicalConstants::ANNUAL_NON_INFECTIOUS_LAMENESS_RESULTS_FILE_NAME] = std::vector<std::string>();
        mResultFileList[TechnicalConstants::ANNUAL_INFECTIOUS_LAMENESS_RESULTS_FILE_NAME] = std::vector<std::string>();
        mResultFileList[TechnicalConstants::CALF_FEEDING_RESULTS_FILE_NAME] = std::vector<std::string>();
        mResultFileList[TechnicalConstants::HEIFER_FEEDING_RESULTS_FILE_NAME] = std::vector<std::string>();
        mResultFileList[TechnicalConstants::COW_FEEDING_RESULTS_FILE_NAME] = std::vector<std::string>();
#endif // _LOG || _FULL_RESULTS

        return mResultFileList;
    }

#ifdef _LOG

    void Model::doBeforeSimulation(const std::string &theResultPath)
    {

        // What is special to prepare before launch simulations ?
        // ...
        mkdir(theResultPath.c_str() DIR_RIGHT);

        // Save mastitis risk period matrix
        Health::HealthManagement::saveMastitisRiskPeriodMatrix(theResultPath);

        // Save lameness risk periods
        Health::HealthManagement::saveLamenessRiskPeriodMatrix(theResultPath);

        // Save milk curves in files
        Genetic::GeneticManagement::saveAllMilkCurvesInFiles(theResultPath);
    
    }

    
    DataStructures::DairyFarm* Model::loadFarmData(const std::string &fileName)
    {
        
//            DataStructures::SystemToSimulate* Model::deserializeSystemToBeSimulated(std::stringstream &creatStream)
//    {
//        DataStructures::DairyFarm* pSTS;
//        creatStream.seekg(0);
//        boost::archive::binary_iarchive ia(creatStream);
//        ia >> BOOST_SERIALIZATION_NVP(pSTS);
//        creatStream.seekg(0);
//        return pSTS;
//    }
//
//        
        DataStructures::DairyFarm* pDairyFarm = nullptr;
        try
        {
            //std::string strFullFileName = fileName + TechnicalConstants::XML_ARCHIVE_EXTENSION;
            std::string strFullFileName = fileName + TechnicalConstants::BINARY_ARCHIVE_EXTENSION;
            std::ifstream ifs(strFullFileName, std::ios_base::binary); // Create the file stream
            {
//                boost::archive::xml_oarchive oa(ofs); // Declare the xml archive            
                boost::archive::binary_iarchive ia(ifs); // Declare the binary archive            
                ia >> BOOST_SERIALIZATION_NVP(pDairyFarm);
            }
            ifs.close(); // Closing the file
        }
        catch (std::exception &e)
        {
            std::string displayMessage("->Load exception: ");
            displayMessage += e.what();
            Console::Display::displayLineInConsole(displayMessage);
        }
        return pDairyFarm;
    } 

#endif // _LOG   

} /* End of namespace Simulator */
