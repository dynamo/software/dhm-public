////////////////////////////////////////
//                                    //
// File : Model.h                     //
//                                    //
// Author : Philippe Gontier          //
//                                    //
// Version 1.0                        //
//                                    //
// Date : 01/10/2015                  //
//                                    //
////////////////////////////////////////

#ifndef Simulator_Model_h
#define Simulator_Model_h

// standard
#include <vector>
#include <map>
#include <string>

// project
#include "Data.h"
#include "AccountingModel.h"
#include "Protocol.h"
#include "../ExchangeInfoStructures/DairyFarmAccountingModelParameters.h"
#include "../ExchangeInfoStructures/DairyFarmParameters.h"
#include "../ExchangeInfoStructures/ProtocolParameters.h"

namespace DataStructures
{
    class DairyFarm;
}

namespace Results
{
    class DairyFarmResult;
}

namespace Simulator
{
    class Model
    {
        
    protected:
        std::map<unsigned int,Data*> _mpHasData; // Key is the user id
              
    public:        
        Model(const std::string &fileName, std::string &message);
        Model(); // Constructor
        virtual ~Model(); // Destructor
    
    protected:
        Data* getData(unsigned int userId);
                
    private:  
        void preProcess(); // pre-process
        void postProcess();// Post-process

    public:
        void saveData(const std::string &fileName);
        
        DataStructures::DairyFarm* getSystemToSimulate(unsigned int userId, std::string &name);
        
        // System to simulate
        void generateSystemToSimulate(unsigned int userId, ExchangeInfoStructures::Parameters::DairyFarmParameters &SystemToSimulateInfo, std::string &message);
        void deleteSystemToSimulate(unsigned int userId, std::string &name, std::string &message);
        
        // Accounting Model
        void createAccountingModel(unsigned int userId, ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &info, std::string &message); // Creating a Accounting Model
        unsigned int getAccountingModelNumber(unsigned int userId);
        AccountingModel* getAccountingModel(unsigned int userId, std::string &name);
        void deleteAccountingModel(unsigned int userId, std::string &name, std::string &message); // Suppress a Accounting Model

        // Protocols
        void getProtocolParameters(unsigned int userId, ExchangeInfoStructures::Parameters::ProtocolParameters &info, std::string &message);
        Protocol* createNewProtocol(ExchangeInfoStructures::Parameters::ProtocolParameters &info, DataStructures::DairyFarm* pHS, AccountingModel* pAm);
        void createProtocol(unsigned int userId, ExchangeInfoStructures::Parameters::ProtocolParameters &info, std::string &message);
        unsigned int getProtocolNumber(unsigned int userId);
        Protocol* getProtocol(unsigned int userId, std::string &name); // Get a Protocol
        void getAllProtocols(unsigned int userId, std::vector<Protocol*> &protocols);
        void deleteProtocol(unsigned int userId, std::string &name, std::string &message); // Delete a protocol

        // Run
    private:
        //void simulateProtocol(unsigned int userId, Protocol* pP, const std::string &theResultPath, std::string &strCount, bool &columnNamesAlreadyGenerated);
        void simulateProtocol(unsigned int userId, Protocol* pP, std::map<std::string, std::vector<std::string>> &mStreamResults, std::string &strCount, bool &columnNamesAlreadyGenerated
#if defined _LOG || defined _FULL_RESULTS
                        , const std::string &theResultPath
#endif // _LOG || _FULL_RESULTS
                            );
        void deleteAllFileResults(const std::string &theResultPath, unsigned int userId);
    public:
        void simulateProtocols(unsigned int userId, std::vector<std::string> &vProtocols, const std::string &theResultPath, std::string &message);
        void simulateProtocols(unsigned int userId, std::vector<std::string> &vProtocols, std::map<std::string, std::vector<std::string>> &mStreamResults, std::string &message
#if defined _LOG || defined _FULL_RESULTS
                        , const std::string &theResultPath
#endif // _LOG || _FULL_RESULTS
                            );
        void simulateAllProtocols(unsigned int userId, const std::string &theResultPath, bool maintainPreviousResultsBeforeSimulation);
        void serializeSystemToBeSimulated(unsigned int userId, std::stringstream &creatStream, Protocol* pP);
        DataStructures::SystemToSimulate* deserializeSystemToBeSimulated(std::stringstream &creatStream);
#ifdef _LOG
        void doBeforeSimulation(const std::string &theResultPath);
        DataStructures::DairyFarm* loadFarmData(const std::string &fileName);
#endif // _LOG

        // Settings/results
        // ----------------
        boost::gregorian::date_duration getTimeLapsDuration();
        Results::DairyFarmResult* getNewResult(std::string &discriminant1, std::string &discriminant2, std::string &discriminant3, std::string &discriminant4, boost::gregorian::date &beginDate, unsigned int currentRun);
        std::map<std::string, std::vector<std::string>> getResultFileList(unsigned int userId);
    };   
} /* End of namespace Simulator */

#endif // Simulator_Model_h
