////////////////////////////////////
//                                //
// File : Protocol.cpp            //
//                                //
// Author : Philippe Gontier      //
//                                //
// Version 1.0                    //
//                                //
// Date : 01/10/2015              //
//                                //
////////////////////////////////////

#include "Protocol.h"

// project
#include "AccountingModel.h"
#include "../DataStructures/SystemToSimulate.h"

BOOST_CLASS_EXPORT(Simulator::Protocol) // Recommended (mandatory if virtual serialization)

namespace Simulator
{
    // Macro to code statement for serialization methods 
    #define STATEMENTS_SERIALIZE_STD_METHODS \
        ar & BOOST_SERIALIZATION_NVP(_hasOriginProtocolInfo); \
        ar & BOOST_SERIALIZATION_NVP(_preSimulationDate); \
        ar & BOOST_SERIALIZATION_NVP(_beginDate); \
        ar & BOOST_SERIALIZATION_NVP(_resultDate); \
        ar & BOOST_SERIALIZATION_NVP(_finishDate); \
        ar & BOOST_SERIALIZATION_NVP(_pHasAccountingModel); \
        ar & BOOST_SERIALIZATION_NVP(_concernsSystemToSimulateName); \

    IMPLEMENT_SERIALIZE_STD_METHODS(Protocol);

    Protocol::Protocol()
    {
    }
    
    Protocol::Protocol(ExchangeInfoStructures::Parameters::ProtocolParameters &info, DataStructures::SystemToSimulate* pSTS, AccountingModel* pAm)
    {
        _hasOriginProtocolInfo = info;
        _pHasAccountingModel = pAm;
        _beginDate = boost::gregorian::date{ boost::gregorian::greg_year(info.simYear), boost::gregorian::greg_month(info.simMonth), 1 };
        _resultDate = boost::gregorian::date{ boost::gregorian::greg_year(info.simYear + info.warmupDuration), boost::gregorian::greg_month(info.simMonth), 1 };
        _finishDate = boost::gregorian::date{ boost::gregorian::greg_year(info.simYear), boost::gregorian::greg_month(info.simMonth), 1 } + boost::gregorian::years(_hasOriginProtocolInfo.simulationDuration) - boost::gregorian::days(1);
        _concernsSystemToSimulateName = pSTS->getName();
        _preSimulationDate = boost::gregorian::date{ boost::gregorian::greg_year(info.simYear - pSTS->getYearNumberForPresimulation()), boost::gregorian::greg_month(info.simMonth), 1 };
    }

    std::string &Protocol::getSystemToSimulateName()
    {
        return _concernsSystemToSimulateName;
    }
    
    std::string &Protocol::getName()
    {
        return _hasOriginProtocolInfo.name;
    }
    
    std::string &Protocol::getComment()
    {
        return _hasOriginProtocolInfo.comment;
    }
    
    std::string &Protocol::getDiscriminant1()
    {
        return _hasOriginProtocolInfo.discriminant1;
    }

    std::string &Protocol::getDiscriminant2()
    {
        return _hasOriginProtocolInfo.discriminant2;
    }

    std::string &Protocol::getDiscriminant3()
    {
        return _hasOriginProtocolInfo.discriminant3;
    }

    std::string &Protocol::getDiscriminant4()
    {
        return _hasOriginProtocolInfo.discriminant4;
    }
    
    unsigned int Protocol::getSimulationDuration()
    {
        return _hasOriginProtocolInfo.simulationDuration;
    }
    
    unsigned int Protocol::getWarmupDuration()
    {
        return _hasOriginProtocolInfo.warmupDuration;
    }

    unsigned int Protocol::getRunNumber()
    {
        return _hasOriginProtocolInfo.runNumber;
    }

    ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &Protocol::getAccountingModelValues()
    {
        return _pHasAccountingModel->getValues();
    }
    
    boost::gregorian::date &Protocol::getPreSimulationDate()
    {
        return _preSimulationDate;
    }

    boost::gregorian::date &Protocol::getBeginDate()
    {
        return _beginDate;
    }
    
    boost::gregorian::date &Protocol::getResultDate()
    {
        return _resultDate;
    }
    
    boost::gregorian::date &Protocol::getFinishDate()
    {
        return _finishDate;
    }
    
    ExchangeInfoStructures::Parameters::ProtocolParameters &Protocol::getProtocolInfo()
    {
        return _hasOriginProtocolInfo;
    }
    
    AccountingModel* Protocol::getAccountingModel()
    {
         return _pHasAccountingModel;
    }
} /* End of namespace Simulator */

