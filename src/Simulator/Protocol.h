////////////////////////////////////
//                                //
// File : Protocol.h              //
//                                //
// Author : Philippe Gontier      //
//                                //
// Version 1.0                    //
//                                //
// Date : 01/10/2015              //
//                                //
////////////////////////////////////

#ifndef Simulator_Protocol_h
#define Simulator_Protocol_h

// boost

// standard
#include <string>
#include <vector>

// project
#include "../ExchangeInfoStructures/ProtocolParameters.h"

#ifdef _LOG
namespace Results
{
    class Result;
}
#endif // _LOG      

namespace DataStructures
{
    class SystemToSimulate;
}

namespace ExchangeInfoStructures
{
    namespace Parameters
    {
        class DairyFarmAccountingModelParameters;
    }
}

namespace Simulator
{
    class AccountingModel;
    class Protocol
    {
        DECLARE_SERIALIZE_STD_METHODS;
        
    private:
        ExchangeInfoStructures::Parameters::ProtocolParameters _hasOriginProtocolInfo;
        AccountingModel* _pHasAccountingModel = nullptr;    
        boost::gregorian::date _finishDate;
        std::string _concernsSystemToSimulateName = "";
        
    protected: 
        boost::gregorian::date _preSimulationDate;
        boost::gregorian::date _beginDate;
        boost::gregorian::date _resultDate;
        
        Protocol(); // Required for serialization
        
    public:
        Protocol(ExchangeInfoStructures::Parameters::ProtocolParameters &info, DataStructures::SystemToSimulate* pRo, AccountingModel* pAm);
        std::string &getName();
        std::string &getComment();
        std::string &getDiscriminant1();
        std::string &getDiscriminant2();
        std::string &getDiscriminant3();
        std::string &getDiscriminant4();
        unsigned int getRunNumber();
        unsigned int getSimulationDuration();
        unsigned int getWarmupDuration();
        ExchangeInfoStructures::Parameters::DairyFarmAccountingModelParameters &getAccountingModelValues();
        boost::gregorian::date &getPreSimulationDate();
        boost::gregorian::date &getBeginDate();
        boost::gregorian::date &getResultDate();
        boost::gregorian::date &getFinishDate();
        ExchangeInfoStructures::Parameters::ProtocolParameters &getProtocolInfo();
        AccountingModel* getAccountingModel();
        std::string &getSystemToSimulateName();
#ifdef _LOG
        virtual void appendRunResults(Results::Result* pRes){}
        virtual void closeResults(const std::string path){}
#endif // _LOG      
        virtual ~Protocol(){}; // Required for serialization    
    };
} /* End of namespace Simulator */
#endif // Simulator_Protocol_h
