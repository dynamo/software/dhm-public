////////////////////////////////////
//                                //
// File : Run.cpp                 //
//                                //
// Author : Philippe Gontier      //
//                                //
// Version 1.0                    //
//                                //
// Date : 01/10/2015              //
//                                //
////////////////////////////////////

#include "Run.h"

// boost

// Project
#include "../DataStructures/DairyFarm.h"
#include "../ResultStructures/Result.h"
#include "Protocol.h"
#include "../Tools/Display.h"

namespace Simulator
{
    Run::Run(DataStructures::SystemToSimulate* pSystemToSimulate, Protocol *pP, const boost::gregorian::date_duration &timeLapsduration, unsigned int number
#ifndef _LOG
                                                                                    , Results::Result* pPresimulationRes
#endif // _LOG
                                                                                    , Results::Result* pSimulationRes)
    {
        // Simulation results
        _pObtainsResults = pSimulationRes;
        
        // set system to simulate 
        _pSystemToSimulate = pSystemToSimulate;
        _pSystemToSimulate->setNum(number); // Init the System To Simulate number
        _pSystemToSimulate->setCurrentAccountingModel(pP->getAccountingModelValues());
        _pSystemToSimulate->setTimeLapsduration(timeLapsduration);
        _pSystemToSimulate->setResults(pSimulationRes
#ifndef _LOG
                                                                                    , pPresimulationRes
#endif // _LOG
                                                                                    );
        _pSystemToSimulate->setPresimulationHasToBeDone(true);
        _pSystemToSimulate->setBeginPresimulationDate(boost::gregorian::date(pP->getPreSimulationDate()));
        _pSystemToSimulate->setBeginSimulationDate(boost::gregorian::date(pP->getBeginDate()));
        _pSystemToSimulate->setBeginResultDate(boost::gregorian::date(pP->getResultDate()));
        _pSystemToSimulate->setEndSimulationDate(boost::gregorian::date(pP->getFinishDate()));
        
    }
    
    Run::Run(DataStructures::SystemToSimulate* pSystemToSimulate, unsigned int number
#ifndef _LOG
                                                                                    , Results::Result* pPresimulationRes
#endif // _LOG
                                                                                    , Results::Result* pSimulationRes)
    {
        // Simulation results
        _pObtainsResults = pSimulationRes;
        
        // set system to simulate 
        _pSystemToSimulate = pSystemToSimulate;
        //_pSystemToSimulate->setNum(number);
        _pSystemToSimulate->setResults(pSimulationRes
#ifndef _LOG
                                                                                    , pPresimulationRes
#endif // _LOG
                                                                                    );
        _pSystemToSimulate->setPresimulationHasToBeDone(false);
    }
    
    Run::~Run()// Destructor
    {
#ifndef _MONORUN
        delete _pSystemToSimulate;
        _pSystemToSimulate = nullptr;
#endif // _MONORUN
       
         delete _pObtainsResults;
        _pObtainsResults = nullptr;
    }
    
    unsigned int Run::getNumber()
    {
        return _pSystemToSimulate->getNum();
    }
    
    Results::Result* Run::getObtainedResults()
    {
        return _pObtainsResults;
    }
    
#if defined _LOG || defined _FULL_RESULTS
    void Run::toFile(const std::string &theResultPath)
    {
        _pObtainsResults->runResultsToFile(theResultPath);
    }
#endif // _LOG || _FULL_RESULTS

    void Run::action(
#ifdef _LOG
            const std::string &theResultPath
#endif // _LOG
            )
    {        
#ifdef _LOG
        Console::Display::displayLineInConsole("Run number: " + Tools::toString(getNumber()));
#endif // _LOG

        // Do simulation
        _pSystemToSimulate->action();
        
#ifdef _LOG
        
        // Close results
        Console::Display::displayLineInConsole("Result calculation...");
        _pSystemToSimulate->closeResults();
        // Generate result files
        toFile(theResultPath);
#else
        Console::Display::displayLineInConsole(".", Console::ANSI_RESET, false);      
#endif // _LOG       
    }
    
} /* End of namespace Simulator */
