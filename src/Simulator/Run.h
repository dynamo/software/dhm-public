////////////////////////////////////
//                                //
// File : Run.h                   //
//                                //
// Author : Philippe Gontier      //
//                                //
// Version 1.0                    //
//                                //
// Date : 01/10/2015              //
//                                //
////////////////////////////////////

#ifndef Simulator_Run_h
#define Simulator_Run_h

// boost
#include <boost/date_time/gregorian/gregorian.hpp>

namespace DataStructures
{
    class SystemToSimulate;
}
namespace ExchangeInfoStructures
{
    namespace Parameters
    {
        class DairyFarmAccountingModelParameters;
    }
}
namespace Results
{
    class Result;
}
namespace Simulator
{
    class Protocol;
    
    class Run
    {
    private:
    protected:
        DataStructures::SystemToSimulate* _pSystemToSimulate = nullptr; // Recreated objet to simulate
        Results::Result* _pObtainsResults = nullptr; // Simulation results, to publish

    public:
        Run(DataStructures::SystemToSimulate* pSystemToSimulate, Protocol* pP, const boost::gregorian::date_duration &timeLapsduration, unsigned int number
#ifndef _LOG
                                                                                    , Results::Result* pPresimulationRes
#endif // _LOG      
                                                                                    , Results::Result* pSimulationRes); // Constructor with general result to obtain
        Run(DataStructures::SystemToSimulate* pSystemToSimulate, unsigned int number
#ifndef _LOG
                                                                                    , Results::Result* pPresimulationRes
#endif // _LOG
                                                                                    , Results::Result* pSimulationRes);
        virtual ~Run(); // Destructor
        unsigned int getNumber();       
        void action(
#ifdef _LOG
            const std::string &theResultPath
#endif // _LOG
        );
        Results::Result* getObtainedResults();
#if defined _LOG || defined _FULL_RESULTS
        void toFile(const std::string &theResultPath);
#endif // _LOG || _FULL_RESULTS
        
     };
} /* End of namespace Simulator */
#endif // Simulator_Run_h