#include "Display.h"

#include <time.h>
#include <fstream>
#include <sstream>
#include <iostream>

namespace Console
{

#ifdef _WIN32    
    void Display::setTextColor(int color)
    {
        WORD wColor;

        HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
        CONSOLE_SCREEN_BUFFER_INFO csbi;
        if (GetConsoleScreenBufferInfo(hStdOut, &csbi))
        { 
            wColor = (csbi.wAttributes & 0xF0) + (color & 0x0F);
            SetConsoleTextAttribute(hStdOut, wColor);
        }
    }
#endif // _WIN32

    std::string Display::getCurrentStringTimestamp()
    {
        const unsigned int MAX_SIZE = 80;
        time_t timestamp = time(NULL);
        struct tm* pTime = localtime(&timestamp);
        char buffer[MAX_SIZE];
        strftime(buffer, MAX_SIZE, "%Y%m%d_%Hh%Mm%Ss", pTime);
        return std::string(buffer);
    }
    
    Display::Display(std::string &logFileName)
    {
        _logFileName = logFileName;
        _gdh = getCurrentStringTimestamp();
        _gdhLogFileName = _logFileName + "_" + _gdh + TechnicalConstants::LOG_FILE_EXTENSION;
        _stdCoutBuffer = std::cout.rdbuf();
    }
    
    void Display::init(std::string &logFileName)
    {
        s_pSingletonDisplay = new Display(logFileName);     
    }
    
    void Display::clean()
    {
        delete s_pSingletonDisplay;
        s_pSingletonDisplay = nullptr;
    }
    
    Display* Display::getInstance()
    {
        return s_pSingletonDisplay;
    }

    /// Display a colored line in the console and save line in the log file
    void Display::displayLineInConsole(
                                                /// line to display
                                                std::string line,
                                                /// text color
                                                std::string textColor,
                                                /// close the line
                                                bool endl)
    {
        s_pSingletonDisplay->_mutex.lock();
        // Log file name
        std::ofstream out_file(s_pSingletonDisplay->_gdhLogFileName, std::ofstream::app);
        // Streams
        std::stringstream ss;
        std::streambuf* ss_buffer = ss.rdbuf();
        std::cout.rdbuf(ss_buffer);

        // Fill common stream
        std::cout << line << std::flush;
        if (endl)
        {
            std::cout << std::endl;
        }

        // Copy common stream
        std::cout.rdbuf(s_pSingletonDisplay->_stdCoutBuffer);
        
#ifdef _WIN32
        setTextColor(WIN_COLORS.find(textColor)->second);
        std::cout << ss.str() << std::flush;
        setTextColor(WIN_COLORS.find(Console::ANSI_RESET)->second);
#else
        std::cout << textColor << ss.str() << Console::ANSI_RESET << std::flush;
#endif // _WIN32
        out_file << ss.str();     
        out_file.close();
        s_pSingletonDisplay->_mutex.unlock();
    }

    Display* Display::s_pSingletonDisplay = nullptr;
} /* End of namespace Console */


