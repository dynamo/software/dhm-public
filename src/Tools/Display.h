/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Display.h
 * Author: pgontier
 *
 * Created on 20 avril 2023, 07:38
 */

#ifndef Console_H
#define Console_H

#ifdef _WIN32
#include <windows.h> // color
#endif // _WIN32
#include <string>
#include <map>
#include <mutex>

#include "../ExchangeInfoStructures/TechnicalConstants.h"

namespace Console
{
const std::string ANSI_RESET = "\u001B[0m";
const std::string ANSI_BLACK = "\u001B[30m";
const std::string ANSI_RED = "\u001B[31m";
const std::string ANSI_GREEN = "\u001B[32m";
const std::string ANSI_YELLOW = "\u001B[33m";
const std::string ANSI_BLUE = "\u001B[34m";
const std::string ANSI_PURPLE = "\u001B[35m";
const std::string ANSI_CYAN = "\u001B[36m";
const std::string ANSI_WHITE = "\u001B[37m";
const std::map <std::string, int> WIN_COLORS {{ANSI_RESET,15},
                                                  {ANSI_BLACK,0},
                                                  {ANSI_RED,12},
                                                  {ANSI_GREEN,10},
                                                  {ANSI_YELLOW,14},
                                                  {ANSI_BLUE,9},
                                                  {ANSI_PURPLE,13},
                                                  {ANSI_CYAN,11},
                                                  {ANSI_WHITE,15}};

class Display
{
    static Display* s_pSingletonDisplay;
    std::mutex _mutex;

public:
    std::string _logFileName = "";
    std::string _gdh = "";
    std::string _gdhLogFileName = "";
    std::streambuf* _stdCoutBuffer = nullptr;


#ifdef _WIN32    
    static void setTextColor(int color);
#endif // _WIN32

    static std::string getCurrentStringTimestamp();
    Display(std::string &logFileName);
    static void init(std::string &logFileName);
    static void clean();
    static Display* getInstance();
    static void displayLineInConsole(
                                    /// line to display
                                    std::string line,
                                    /// text color
                                    std::string textColor = Console::ANSI_RESET,
                                    /// close the line
                                    bool endl = true);
};
}

#endif /* Console_H */

