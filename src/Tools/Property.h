#ifndef PROPERTY_H
#define PROPERTY_H

// Base : https://www.codeguru.com/cplusplus/implementing-a-property-in-c/

template<typename Container, typename ValueType>
class Property
{
private:
    Container* _pObject;  //– Pointer to the module that, contains the property —
    void (Container::*Set)(ValueType value); //– Pointer to set member function —
    ValueType (Container::*Get)(); //– Pointer to get member function —

protected: 
    Property(Container* pObject, void (Container::*pSet)(ValueType value), ValueType (Container::*pGet)())
    {
        _pObject = pObject;
        Set = pSet;
        Get = pGet;
    }

    //– Set the set member function that will change the value —
    void setter(void (Container::*pSet)(ValueType value))
    {
        Set = pSet;
    }
    
    //– Set the get member function that will retrieve the value —
    void getter(ValueType (Container::*pGet)())
    {
        Get = pGet;
    }
   
    //– To make possible to cast the property class to the internal type —
    operator ValueType()
    {
        return (_pObject->*Get)();
    }

    //– Overload the ‘=’ sign to set the value using the set member —
    ValueType operator =(const ValueType& value)
    {
        (_pObject->*Set)(value);
        return value;
    }
};

#define WRITE_PROPERTY_METHODS \
    void setter(void (Container::*pSet)(ValueType value)) \
    { \
        Property<Container, ValueType>::setter(pSet); \
    } \
    ValueType operator =(const ValueType& value) \
    { \
        return Property<Container, ValueType>::operator =(value); \
    }
    
#define READ_PROPERTY_METHODS \
    virtual void getter(ValueType (Container::*pGet)()) \
    { \
        Property<Container, ValueType>::getter(pGet); \
    } \
    operator ValueType() \
    { \
        return Property<Container, ValueType>::operator ValueType(); \
    }

template<typename Container, typename ValueType>
class WriteOnlyProperty : Property <Container, ValueType>
{  
public:
    WriteOnlyProperty<Container, ValueType>(Container* pObject, void (Container::*pSet)(ValueType value)) : Property<Container, ValueType>(pObject, pSet, nullptr){}
    WRITE_PROPERTY_METHODS;
};

template<typename Container, typename ValueType>
class ReadOnlyProperty : Property <Container, ValueType>
{
public:
    ReadOnlyProperty<Container, ValueType>(Container* pObject, ValueType (Container::*pGet)()) : Property<Container, ValueType>(pObject, nullptr, pGet){}
    READ_PROPERTY_METHODS;
};

template<typename Container, typename ValueType>
class WriteReadProperty : Property <Container, ValueType>
{
public:
    WriteReadProperty<Container, ValueType>(Container* pObject, void (Container::*pSet)(ValueType value), ValueType (Container::*pGet)()) : Property<Container, ValueType>(pObject, pSet, pGet){}
    WRITE_PROPERTY_METHODS;
    READ_PROPERTY_METHODS;
};

#define SETTER(member, property) \
void set##property(__typeof(member) val) \
{ \
    member = val; \
}

#define GETTER(member, property) \
__typeof(member) get##property() \
{ \
    return member; \
}

#define DECLARE_INIT_READ_ONLY_PROPERTY(property) \
void inline initProperty##property() \
{ \
    (property).getter(&CLASS_USING_PROPERTY::get##property); \
} 

#define DECLARE_INIT_WRITE_ONLY_PROPERTY(property) \
void inline initProperty##property() \
{ \
    (property).setter(&CLASS_USING_PROPERTY::set##property); \
} 

#define DECLARE_INIT_READ_WRITE_PROPERTY(property) \
void inline initProperty##property() \
{ \
    (property).setter(&CLASS_USING_PROPERTY::set##property); \
    (property).getter(&CLASS_USING_PROPERTY::get##property); \
} 

#define DECLARE_READ_ONLY_PROPERTY(property, member) \
public: \
    ReadOnlyProperty<CLASS_USING_PROPERTY,__typeof(member)> property = ReadOnlyProperty<CLASS_USING_PROPERTY,__typeof(member)>(this, &CLASS_USING_PROPERTY::get##property); \
    GETTER(member, property) \
    DECLARE_INIT_READ_ONLY_PROPERTY(property)

#define DECLARE_WRITE_ONLY_PROPERTY(property, member) \
public: \
    WriteOnlyProperty<CLASS_USING_PROPERTY,__typeof(member)> property = WriteOnlyProperty<CLASS_USING_PROPERTY,__typeof(member)>(this, &CLASS_USING_PROPERTY::set##property); \
    SETTER(member, property) \
    DECLARE_INIT_WRITE_ONLY_PROPERTY(property)

#define DECLARE_READ_WRITE_PROPERTY(property, member) \
public: \
     WriteReadProperty<CLASS_USING_PROPERTY,__typeof(member)> property = WriteReadProperty<CLASS_USING_PROPERTY,__typeof(member)>(this, &CLASS_USING_PROPERTY::set##property, &CLASS_USING_PROPERTY::get##property); \
    SETTER(member, property) \
    GETTER(member, property) \
    DECLARE_INIT_READ_WRITE_PROPERTY(property)

#endif /* PROPERTY_H */

