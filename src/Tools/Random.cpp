#include "Random.h"

// Standard
#include <assert.h> 
#include <math.h>
#include <boost/random/beta_distribution.hpp>

namespace Tools
{
    Random::Random(unsigned int seed)
    {
        _randomGenerator = std::mt19937(seed);
    }

    Random::~Random()
    {
    }
    
    Random::ValuesBasedOnMultinomialDistribution Random::getValuesBasedOnMultinomialDistribution(std::vector<unsigned int>& multinomialDistribution)
    {
        return Random::ValuesBasedOnMultinomialDistribution(multinomialDistribution, *this);
    }
    
    Random::ValuesBasedOnMultinomialDistribution::ValuesBasedOnMultinomialDistribution (const Random::ValuesBasedOnMultinomialDistribution &obj)
    {
        currentIndex = obj.currentIndex;
        distribuableValues = obj.distribuableValues;
    }

    Random::ValuesBasedOnMultinomialDistribution::ValuesBasedOnMultinomialDistribution(std::vector<unsigned int>& multinomialDistribution, Random &random)
    {
        for (unsigned int originValue = 0; originValue < multinomialDistribution.size(); originValue++)
        {
            for (unsigned int valueNumberInd = 0; valueNumberInd < multinomialDistribution[originValue]; valueNumberInd++)
            {
                if (distribuableValues.size() == 0)
                {
                    distribuableValues.push_back(originValue); // We push the first value
                }
                else
                {
                    std::vector<unsigned int>::iterator itTargetValues = distribuableValues.begin();
                    distribuableValues.insert(itTargetValues + random.rng_uniform_int(distribuableValues.size()), originValue); // We insert this value in a random position
                }
            }
        }
    }
            
    unsigned int Random::ValuesBasedOnMultinomialDistribution::getValue()
    {
        if (currentIndex == distribuableValues.size())
        {
            currentIndex = 0;
        }
        return distribuableValues[currentIndex++];
    }
    
    unsigned int Random::ValuesBasedOnMultinomialDistribution::getValueNumber()
    {
        return distribuableValues.size();
    }
    
    int Random::rng_uniform_int(const int size)
    {
        assert (size>0);
        std::uniform_int_distribution<> dis(0, size-1);            
        return dis(getGen());
    }

    std::mt19937 &Random::getGen()
    {
        return _randomGenerator;
    }
    
    bool Random::ran_bernoulli(const double p)
    {
        std::bernoulli_distribution dis(p);
        return dis(getGen());   
    }
    
    double Random::ran_normal_distribution(const double m, const double s)
    {
        std::normal_distribution<double> dis(m, s);
        return dis(getGen()); 
    }
    
    bool Random::ran_resultRegardingSensitivity(const double sensitivity)
    {
        return ran_bernoulli(sensitivity);
    }
    
    bool Random::ran_resultRegardingSpecificity(const double specificity)
    {
        return not ran_bernoulli(specificity);
    }

    int Random::ran_binomial(const double p, const int n)
    {
        std::binomial_distribution<> dis(n,p);
        return dis(getGen());
    }

    std::vector<unsigned int> Random::ran_multinomial(const int N, const std::vector<double>& vProbs)
    {
        std::vector<unsigned int> vNbs;
        vNbs.resize(vProbs.size(), 0);
        double sumProbs = 0.0;
        double sumNbs = 0;
        double norm = std::accumulate(vProbs.begin(), vProbs.end(), 0.0f);
        for (unsigned int i = 0; i < vProbs.size(); i++)
        {
            double dN = N;
            if(vProbs[i] > 0.0)
            {
                double dtmp = ((dN - sumNbs) * vProbs[i]) / (norm - sumProbs);
                //vNbs[i] = (unsigned int) (dtmp); 
                vNbs[i] = (unsigned int) (dtmp + 0.00001f); // 0.00001f due to marginal conversion diferencies
            }
            else
            {
                vNbs[i] = 0;
            }
            sumProbs += vProbs[i];
            sumNbs += vNbs[i];
            if (N - sumNbs == 0){
                break;
            }
        }
        return vNbs;
    }

    double Random::ran_flat(const double a, const double b)
    {
        std::uniform_real_distribution<> dis(a, b);
        return dis(getGen());
    }
    
    double Random::ran_gaussian(const double mean, const double stddev)
    {
        std::normal_distribution<> dis(mean,stddev);
        return dis(getGen());
    }
    
    double Random::rng_uniform()
    {
        return ran_flat(0.0f, 1.0f);
    }
    
    double Random::ran_beta (const double a, const double b)
    {
        boost::random::beta_distribution<> dist(a, b);
        return dist(getGen());
    }

    double Random::betaPertDistribution(double min, double max, double mode, double lambda = 4 )
    {
        // Inspired by https://www.riskamp.com/beta-pert

#ifdef _LOG
        assert (!(min > max || mode > max || mode < min ));
#endif // _LOG

        double range = max - min;
        if (range == 0 ) return( mode);
        double mu = (min + max + lambda *mode) / (lambda + 2 );
        double v = 0;
        if( mu == mode )
        {
            v = ( lambda / 2 ) + 1;
        }
        else
        {
            v = (( mu - min ) * ( 2 * mode - min - max )) / (( mode - mu ) * ( max - min ));
        }
        double w = ( v * ( max - mu )) / ( mu - min );
        return ran_beta (v, w) * range + min;
    }
    
    void Random::getBetaPertProba(double xMax, double xMostLikely, double leftOffset, double rightOffset, double integralToObtain, double lambda, std::vector<double> &results)
    {


        // Calculation of the offset
        double meanIntegralOffset = xMax * (leftOffset + rightOffset) / 2.0f;
        double integralToCalculate = integralToObtain - meanIntegralOffset;
        if (integralToCalculate <  0.0f)
        {
            integralToCalculate = integralToObtain; // If the offset is greater than the one to obtain, we don't care of the offset
        }

        // Monte carlo method : number of tests
        std::map <unsigned int, unsigned int> p; // Map to collect the result of all these tests
        const int nrolls = 1000000; // number of experiments (best precision level)
        for (unsigned int i = 0; i < nrolls; i++)
        {
            unsigned int res = betaPertDistribution(0, xMax, xMostLikely, lambda); // Making the test
            ++p[res]; // Store the result if the corresponding x
        }

        // Calculating the final results
        for (unsigned int i = 0; i < xMax; i++)
        {
            double xOffset; // Offset repartion between left and right offsets
            if (leftOffset > rightOffset)   xOffset = rightOffset + (leftOffset - rightOffset) * ((double)xMax-i-0.5) / xMax;
            else                            xOffset = leftOffset + (rightOffset - leftOffset) * ((double)i+0.5) / xMax;
            results.push_back(integralToCalculate * p[i] / ((double)nrolls) + xOffset);
        }
    }
    double *Random::vec_uniform (int n)
    {
        double *r = new double[n];

        for (int i = 0; i < n; i++ )
        {
            r[i] = rng_uniform();
        }
        return r;
    }

    //****************************************************************************
    double *Random::multinormal_sample ( int m, int n, double a[], double mu[])
    //****************************************************************************
    //
    //  Purpose:
    //
    //    MULTINORMAL_SAMPLE samples a multivariate normal distribution.
    //
    //  Discussion:
    //
    //    The multivariate normal distribution for the M dimensional vector X
    //    has the form:
    //
    //      pdf(X) = (2*pi*det(A))**(-M/2) * exp(-0.5*(X-MU)'*inverse(A)*(X-MU))
    //
    //    where MU is the mean vector, and A is a positive definite symmetric
    //    matrix called the variance-covariance matrix.
    //
    //  Licensing:
    //
    //    This code is distributed under the GNU LGPL license. 
    //
    //  Modified:
    //
    //    09 December 2009
    //
    //  Author:
    //
    //    John Burkardt
    //
    //  Parameters:
    //
    //    Input, int M, the dimension of the space.
    //
    //    Input, int N, the number of points.
    //
    //    Input, double A[M*M], the variance-covariance 
    //    matrix.  A must be positive definite symmetric.
    //
    //    Input, double MU[M], the mean vector.
    //
    //    Input/output, int *SEED, the random number seed.
    //
    //    Output, double MULTINORMAL_SAMPLE[M], the points.
    //
    // Downloaded on https://github.com/cenit/jburkardt/tree/master/normal_dataset
    // Adapted for DHM by Philippe Gontier
    
    {
        int i;
        int j;
        int k;
        double *r;
        double *x;
        double *y;
        //
        //  Compute the upper triangular Cholesky factor R of the variance-covariance
        //  matrix.
        //
        r = r8po_fa ( m, a );

        //
        //  Y = MxN matrix of samples of the 1D normal distribution with mean 0
        //  and variance 1.  
        //
        y = r8vec_normal_01_new ( m*n);
        //
        //  Compute X = MU + R' * Y.
        //
        x = new double[m*n];

        for ( j = 0; j < n; j++ )
        {
            for ( i = 0; i < m; i++ )
            {
                x[i+j*m] = mu[i];
                for ( k = 0; k < m; k++ )
                {
                    x[i+j*m] = x[i+j*m] + r[k+i*m] * y[k+j*m];
                }
            }
        }        

        delete [] r;
        delete [] y;

        return x;
    }

    //****************************************************************************80
    double *Random::r8po_fa ( int n, double a[] )
    //****************************************************************************80
    //
    //  Purpose:
    //
    //    R8PO_FA factors a R8PO matrix.
    //
    //  Discussion:
    //
    //    The R8PO storage format is appropriate for a symmetric positive definite 
    //    matrix and its inverse.  (The Cholesky factor of a R8PO matrix is an
    //    upper triangular matrix, so it will be in R8GE storage format.)
    //
    //    Only the diagonal and upper triangle of the square array are used.
    //    This same storage format is used when the matrix is factored by
    //    R8PO_FA, or inverted by R8PO_INVERSE.  For clarity, the lower triangle
    //    is set to zero.
    //
    //    The positive definite symmetric matrix A has a Cholesky factorization
    //    of the form:
    //
    //      A = R' * R
    //
    //    where R is an upper triangular matrix with positive elements on
    //    its diagonal.  
    //
    //  Licensing:
    //
    //    This code is distributed under the GNU LGPL license. 
    //
    //  Modified:
    //
    //    04 February 2004
    //
    //  Author:
    //
    //    Original FORTRAN77 version by Dongarra, Bunch, Moler, Stewart.
    //    C++ version by John Burkardt.
    //
    //  Reference:
    //
    //    Jack Dongarra, Jim Bunch, Cleve Moler, Pete Stewart,
    //    LINPACK User's Guide,
    //    SIAM, 1979,
    //    ISBN13: 978-0-898711-72-1,
    //    LC: QA214.L56.
    //
    //  Parameters:
    //
    //    Input, int N, the order of the matrix.
    //
    //    Input, double A[N*N], the matrix in R8PO storage.
    //
    //    Output, double R8PO_FA[N*N], the Cholesky factor in SGE
    //    storage, or NULL if there was an error.
    //
    // Downloaded on https://github.com/cenit/jburkardt/tree/master/normal_dataset
    // Adapted for DHM by Philippe Gontier
    {
        double *b;
        int i;
        int j;
        int k;
        double s;

        b = new double[n*n];

        for ( j = 0; j < n; j++ )
        {
            for ( i = 0; i < n; i++ )
            {
                b[i+j*n] = a[i+j*n];
            }
        }

        for ( j = 0; j < n; j++ )
        {
            for ( k = 0; k <= j-1; k++ )
            {
                for ( i = 0; i <= k-1; i++ )
                {
                    b[k+j*n] -= b[i+k*n] * b[i+j*n];
                }
                if (b[k+j*n] != 0.0f)
                {
#ifdef _LOG
                    assert (b[k+k*n] != 0);
#endif // _LOG
                    b[k+j*n] /= b[k+k*n];
                }
            }

            s = b[j+j*n];
            for ( i = 0; i <= j-1; i++ )
            {
                s -= pow(b[i+j*n], 2.0f);
            }
            if ( s < 0.0f )
            {
                s = 0.0f;
            }

            b[j+j*n] = sqrt ( s );
        }
        //
        //  Since the Cholesky factor is in R8GE format, zero out the lower triangle.
        //
        for ( i = 0; i < n; i++ )
        {
            for ( j = 0; j < i; j++ )
            {
                b[i+j*n] = 0.0;
            }
        }
        return b;
    }
    //****************************************************************************

    //****************************************************************************
    double *Random::r8vec_normal_01_new ( int n)
    //****************************************************************************
/*
  Purpose:

    R8VEC_NORMAL_01_NEW returns a unit pseudonormal R8VEC.

  Discussion:

    The standard normal probability distribution function (PDF) has
    mean 0 and standard deviation 1.

  Licensing:

    This code is distributed under the GNU LGPL license.

  Modified:

    06 August 2013

  Author:

    John Burkardt

  Parameters:

    Input, int N, the number of values desired.

    Input/output, int *SEED, a seed for the random number generator.

    Output, double R8VEC_NORMAL_01_NEW[N], a sample of the standard normal PDF.

  Local parameters:

    Local, double R[N+1], is used to store some uniform random values.
    Its dimension is N+1, but really it is only needed to be the
    smallest even number greater than or equal to N.

    Local, int X_LO, X_HI, records the range of entries of
    X that we need to compute.
 * 
 * Downloaded on https://people.sc.fsu.edu/~jburkardt/c_src/annulus_monte_carlo/r8vec_normal_01_new.c
 * Adapted for DHM by Philippe Gontier
 */
    {
        int i;
        int m;
        double *r;
        double *x;
        int x_hi;
        int x_lo;

        x = new double[n];
        /*
            Record the range of X we need to fill in.
        */
        x_lo = 1;
        x_hi = n;
        /*
            If we need just one new value, do that here to avoid null arrays.
        */
        if ( x_hi - x_lo + 1 == 1 )
        {
            r = vec_uniform ( 2);

            x[x_hi-1] = sqrt ( - 2.0 * log ( r[0] ) ) * cos ( 2.0 * M_PI * r[1] );

            delete [] r;
        }
        /*
            If we require an even number of values, that's easy.
        */
        else if ( ( x_hi - x_lo + 1 ) % 2 == 0 )
        {
            m = ( x_hi - x_lo + 1 ) / 2;

            r = vec_uniform ( 2*m);

            for ( i = 0; i <= 2*m-2; i = i + 2 )
            {
                x[x_lo+i-1] = sqrt ( - 2.0 * log ( r[i] ) ) * cos ( 2.0 * M_PI * r[i+1] );
                x[x_lo+i  ] = sqrt ( - 2.0 * log ( r[i] ) ) * sin ( 2.0 * M_PI * r[i+1] );
            }
            delete [] r;
        }
        /*
            If we require an odd number of values, we generate an even number,
            and handle the last pair specially, storing one in X(N).
        */
        else
        {
            x_hi = x_hi - 1;

            m = ( x_hi - x_lo + 1 ) / 2 + 1;

            r = vec_uniform ( 2*m);

            for ( i = 0; i <= 2*m-4; i = i + 2 )
            {
                x[x_lo+i-1] = sqrt ( - 2.0 * log ( r[i] ) ) * cos ( 2.0 * M_PI * r[i+1] );
                x[x_lo+i  ] = sqrt ( - 2.0 * log ( r[i] ) ) * sin ( 2.0 * M_PI * r[i+1] );
            }

            i = 2*m - 2;

            x[x_lo+i-1] = sqrt ( - 2.0 * log ( r[i] ) ) * cos ( 2.0 * M_PI * r[i+1] );

            delete [] r;
        }
      return x;
    }
    //****************************************************************************80
} /* End of namespace Tools */
