/* 
 * File:   Random.h
 * Author: pgontier
 * 
 * Created on 25 mai 2016
 */
#ifndef Random_h
#define Random_h

// standard
#include <map>
#include <sstream>
#include <random>
#include <thread> 

// Boost
#include <boost/math/distributions/beta.hpp>

// Project
#include "Tools.h"
#include "Serialization.h"




//class Example {
//    std::mt19937_64 rng_;
//
//    friend class boost::serialization::access;
//    template <typename Ar> void serialize(Ar& ar, unsigned /*version*/)  {
//        ar & rng_; 
//    }
//};

#include <stdexcept>












namespace Tools
{
    class Random
    {
//#define MT_TPARAMS typename UIntType, size_t w, size_t n, size_t m, size_t r, UIntType a, size_t u, UIntType d, size_t s, UIntType b, size_t t, UIntType c, size_t l, UIntType f
//#define MT_TARGLIST UIntType, w, n, m, r, a, u, d, s, b, t, c, l, f
//
//    template<typename Ar, MT_TPARAMS>
//    void load(Ar& ar, std::mersenne_twister_engine<MT_TARGLIST>& mt, unsigned) {
//        std::string text;
//        ar & text;
//        std::istringstream iss(text);
//
//        if (!(iss >> mt))
//            throw std::invalid_argument("mersenne_twister_engine state");
//    }
//
//    template<typename Ar, MT_TPARAMS>
//    void save(Ar& ar, std::mersenne_twister_engine<MT_TARGLIST> const& mt, unsigned) {
//        std::ostringstream oss;
//        if (!(oss << mt))
//            throw std::invalid_argument("mersenne_twister_engine state");
//        std::string text = oss.str();
//        ar & text;
//    }
//
//    template<typename Ar, MT_TPARAMS>
//    void serialize(Ar& ar, std::mersenne_twister_engine<MT_TARGLIST>& mt, unsigned version) {
//        if (typename Ar::is_saving())
//            save(ar, mt, version);
//        else
//            load(ar, mt, version);
//    }
//
//#undef MT_TPARAMS
//#undef MT_TARGLIST
//
    private:

        std::mt19937 _randomGenerator;

        std::mt19937 &getGen();
//        
//        
//    friend class boost::serialization::access;
//    template <typename Ar> void serialize(Ar& ar, unsigned /*version*/)  {
//        ar & BOOST_SERIALIZATION_NVP(_randomGenerator); 
//    }

        
    public:
        
        // To get values based on multinomial distribution
        struct ValuesBasedOnMultinomialDistribution
        {
            unsigned int currentIndex = 0;
            std::vector<unsigned int> distribuableValues;
        public:
            ValuesBasedOnMultinomialDistribution(){}
            ValuesBasedOnMultinomialDistribution (const ValuesBasedOnMultinomialDistribution &obj);        // copy constructor
            ValuesBasedOnMultinomialDistribution(std::vector<unsigned int>& multinomialDistribution, Random &random);
            unsigned int getValue();
            unsigned int getValueNumber();
        };
        
        /// Get a "ValuesBasedOnMultinomialDistribution" instance, to get successive and random values based on multinomial distribution
        Random::ValuesBasedOnMultinomialDistribution getValuesBasedOnMultinomialDistribution(
                /// base list aof the value number to obtain
                std::vector<unsigned int>& multinomialDistribution);
        
        double *vec_uniform ( int n);
               
        // -----------------------------------------------------------------------------------
        // Multivariate Normal Random Values
        // https://github.com/cenit/jburkardt/tree/master/normal_dataset
        double *r8po_fa ( int n, double a[] );
        double *r8vec_normal_01_new ( int n);
        double *multinormal_sample( int m, int n, double a[], double mu[]);
        // -----------------------------------------------------------------------------------

        /// Unique usable constructor, with random generator
        Random(
                /// seed to use
                unsigned int seed);
        
        Random(){};
        
        /// Standard destructor
        virtual ~Random();
        
        /// Give a real random value from 0.0f to 1.0f, based on an uniform distribution 
        double rng_uniform();
        
        // Give a random integer value from 0 to size parameter, based on an uniform distribution 
        int rng_uniform_int(
                            /// max integer value
                            const int size);
       
        /// Give a random boolean value, based on a given probability 
        bool ran_bernoulli( 
                            /// probability
                            const double proba);
        
        /// Give a normal distribution 
        double ran_normal_distribution(
                            /// mean
                            const double m,
                            /// standard deviation
                            const double s);
        
        /// Give a sensitivity result 
        bool ran_resultRegardingSensitivity(
                            /// sensibility ratio
                            const double sensitivity
        );
        
        /// Give a specificity result
        bool ran_resultRegardingSpecificity(
                            /// specificity ratio
                            const double specificity
        );
        
        /// Give a real random value from first value to a second one, based on an uniform distribution 
        double ran_flat(
                            /// first value
                            const double a,
                            /// second value
                            const double b);
        
        /// Give a real random value based on a normal distribution , knowing the mean and the standard deviation
        double ran_gaussian(
                            /// mean
                            const double mean,
                            /// standard deviation
                            const double stddev);
        
        /// Give a random integer value 
        int ran_binomial(
                            /// probability of a trial generating true
                            const double p,
                            /// number of trials
                            const int n);

        /// Give a list of a number of random integer values, distributed regarding a normalized vector of probabilities
        std::vector<unsigned int> ran_multinomial(
                            /// number of values to give
                            const int n,
                            /// normalized vector of probabilities
                            const std::vector<double>& vProbs);
        
        /// Give a real random value depending on alpha and beta parameters
        double ran_beta (
                            /// alpha
                            const double a,
                            /// beta
                            const double b);
        
        /// Return a value based on a Beta pert distribution
        double betaPertDistribution(
                                    /// Minimum x value
                                    double min, 
                                    /// Maximum x value
                                    double max,
                                    /// Most likely x value
                                    double mode,
                                    /// Controls the scale of the distribution (the peakedness).
                                    double lambda);
        
        /// Return a probability vector based on a Beta pert distribution
        void getBetaPertProba(
                                /// number of values
                                double xMax,
                                /// peak position
                                double xMostLikely,
                                /// reference begin value (to join previous values)
                                double leftOffset,
                                /// reference begin value (to join next values)
                                double rightOffset,
                                /// monte carlo global integral value
                                double integralToObtain,
                                /// lambda
                                double lambda,
                                /// results vector
                                std::vector<double> &results);        
    }; // class Random
    
    /// Give a normal distribution of a multivariate dataset
    template<typename T>
    inline static void getMultivariateNormalRandomValue (
                                    /// mean vector
                                    const std::map<T, float> &moyenne,
                                    /// covariance matrix
                                    const std::map<T,std::map<T, float>> &covarianceMatrix,
                                    /// retul vector
                                    std::map<T, float> &result,
                                    /// Random instance
                                    Tools::Random &rand)
    {
        int m = moyenne.size(); // spatial dimension.

        // Mean vector conversion
        double *mu = new double[m]; // mean
        unsigned int i = 0;
        for (typename std::map<T, float>::const_iterator it = moyenne.begin(); it != moyenne.end(); it++)
        {
            mu[i] = it->second;
            i++;
        }

        // Variance-covariance matrix conversion
        double *a = new double[m*m]; // The converted variance-covariance matrix.
        unsigned int j = 0;
        for (typename std::map<T,std::map<T, float>>::const_iterator itj = covarianceMatrix.begin(); itj != covarianceMatrix.end(); itj++)
        {
            i = 0;
            for (typename std::map<T, float>::const_iterator iti = itj->second.begin(); iti != itj->second.end(); iti++)
            {
                a[i+j*m] = iti->second;
                i++;
            }
            j++;
        }

        // Compute the data.
        double *x = rand.multinormal_sample ( m, 1, a, mu);

        // Result conversion
        i = 0;
        for (typename std::map<T, float>::const_iterator it = moyenne.begin(); it != moyenne.end(); it++)
        {
            result[it->first] = x[i++];
        }

        // Memory free
        delete [] a;
        delete [] mu;
        delete [] x;
    }
} /* End of namespace Tools */
#endif // Random_h
