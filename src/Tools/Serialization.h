/* 
 * File:   Serialization.h
 * Author: pgontier
 * 
 * Created on 25 mai 2016
 */

#ifndef Tools_Serialization_h
#define Tools_Serialization_h

// std
#include <iostream>

// Project
#include "../Tools/Display.h"

// Boost
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#if defined _DHM_SERVER || defined _LOG
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#endif // _DHM_SERVER || _LOG
/*
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
*/
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/queue.hpp>
#include <boost/date_time/gregorian/greg_serialize.hpp> // If the boost::gregorian::date is used and to be serialized
#include <boost/date_time/gregorian/formatters.hpp>
#include <boost/serialization/export.hpp>

// Macros to declare standard serialization methods

#define DECLARE_SERIALIZE_BIN_METHODS \
void serialize(boost::archive::binary_iarchive &ar, unsigned int version); \
void serialize(boost::archive::binary_oarchive &ar, unsigned int version); 

#if defined _DHM_SERVER || defined _LOG
#define DECLARE_SERIALIZE_XML_METHODS \
void serialize(boost::archive::xml_iarchive &ar, unsigned int version); \
void serialize(boost::archive::xml_oarchive &ar, unsigned int version);
#endif // _DHM_SERVER || _LOG

/*
#define DECLARE_SERIALIZE_TXT_METHODS \
void serialize(boost::archive::text_iarchive &ar, unsigned int version); \
void serialize(boost::archive::text_oarchive &ar, unsigned int version);
*/

#if defined _DHM_SERVER || defined _LOG
#define DECLARE_SERIALIZE_STD_METHODS \
private: \
friend class boost::serialization::access; \
DECLARE_SERIALIZE_BIN_METHODS \
DECLARE_SERIALIZE_XML_METHODS
#else // _DHM_SERVER || _LOG
#define DECLARE_SERIALIZE_STD_METHODS \
private: \
friend class boost::serialization::access; \
DECLARE_SERIALIZE_BIN_METHODS
#endif // _DHM_SERVER || _LOG

/*
DECLARE_SERIALIZE_TXT_METHODS
*/
// Macro to generate code for the method of a serialization archive type
#define IMPLEMENT_SERIALIZE_STD_METHOD_FOR_ARCHIVE(theClass, theArchiveType) \
void theClass::serialize(theArchiveType &ar, unsigned int version) \
{ \
        try \
        { \
            STATEMENTS_SERIALIZE_STD_METHODS \
        } \
        catch (std::exception &e) \
        { \
            std::string what(e.what()); \
            std::string tiClass(typeid(theClass).name()); \
            std::string tiArchive(typeid(theArchiveType).name()); \
            std::string message = "serialization exception in class " + tiClass + ", " + tiArchive + ", " + what; \
            Console::Display::displayLineInConsole(message); \
            throw (e); \
        } \
} \


// Macro to implement standard serialization methods 
#define IMPLEMENT_SERIALIZE_BIN_METHODS(theClass) \
IMPLEMENT_SERIALIZE_STD_METHOD_FOR_ARCHIVE(theClass, boost::archive::binary_iarchive) \
IMPLEMENT_SERIALIZE_STD_METHOD_FOR_ARCHIVE(theClass, boost::archive::binary_oarchive)


#if defined _DHM_SERVER || defined _LOG
#define IMPLEMENT_SERIALIZE_XML_METHODS(theClass) \
IMPLEMENT_SERIALIZE_STD_METHOD_FOR_ARCHIVE(theClass, boost::archive::xml_iarchive) \
IMPLEMENT_SERIALIZE_STD_METHOD_FOR_ARCHIVE(theClass, boost::archive::xml_oarchive)
#endif // _DHM_SERVER || _LOG

/*#define IMPLEMENT_SERIALIZE_TXT_METHODS(theClass) \
IMPLEMENT_SERIALIZE_STD_METHOD_FOR_ARCHIVE(theClass, boost::archive::text_iarchive) \
IMPLEMENT_SERIALIZE_STD_METHOD_FOR_ARCHIVE(theClass, boost::archive::text_oarchive)
*/

#if defined _DHM_SERVER || defined _LOG
#define IMPLEMENT_SERIALIZE_STD_METHODS(theClass) \
IMPLEMENT_SERIALIZE_BIN_METHODS(theClass) \
IMPLEMENT_SERIALIZE_XML_METHODS(theClass)
#else // _DHM_SERVER || _LOG
#define IMPLEMENT_SERIALIZE_STD_METHODS(theClass) \
IMPLEMENT_SERIALIZE_BIN_METHODS(theClass)
#endif // _DHM_SERVER || _LOG

/*
IMPLEMENT_SERIALIZE_TXT_METHODS(theClass)
*/
#endif // Tools_Serialization_h
