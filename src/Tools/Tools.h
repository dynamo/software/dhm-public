/* 
 * File:   Tools.h
 * Author: pgontier
 * 
 * Created on 25 mai 2016
 */

#ifndef Tools_h
#define Tools_h

#ifndef M_PI
#define M_PI	3.14159265358979323846 // with windows OS, M_PI is not defined during compilation...
#endif // M_PI

// standard
#ifdef _WIN32
#include <windows.h> // color
#endif // _WIN32
#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <fstream>      // std::ifstream
#ifdef _WIN32
#include <io.h> // mkdir
#define DIR_RIGHT
#else
#include <sys/stat.h> // mkdir
#define DIR_RIGHT ,S_IRWXU|S_IRWXG|S_IRWXO
#endif

#ifdef _WIN32
#include <filesystem>
#else

// boost
#include <boost/filesystem/operations.hpp>
#endif // _WIN32
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/algorithm/string.hpp>

#include "Display.h"

namespace Tools
{
    // Note : Template's implementation can't be in a cpp file ...
    
    /// To sort objects by a weight
    template <class T> class Classifiable
    {
    public:
        void* _pObj;
        T _weight;
        Classifiable (void* pObj, T weight) : _pObj(pObj), _weight(weight){}
        bool operator < ( const Classifiable & other)
        {
            return _weight < other._weight;
        }
        virtual ~Classifiable(){}
    };

    /// Convert to default string all kind of object
    template <typename T>
    inline static std::string toString (
                            /// object to convert
                            const T toConvert)
    {
        std::ostringstream oss; 
        oss << toConvert; 
        return oss.str(); 
    }
    
    /// Calculation of the median of a vector
    template <typename T>
    inline static T calculateMedian(std::vector<T>& toCalculate)
    {
        unsigned int size = toCalculate.size();
        assert (size != 0);
        std::sort(toCalculate.begin(), toCalculate.end());
        if ((size % 2) == 0)
        {
            return (toCalculate[(size / 2) - 1] + toCalculate[size / 2]) / (T)2;
        }
        else 
        {
            return toCalculate[size / 2];
        }
    }
    
    /// Variance-Covariance matrix calculation
    template<typename T>
    inline static std::map<T,std::map<T, float>> getVarianceCovarianceMatrix (
                                                                                /// standard deviations
                                                                                const std::map<T, float> &sd,
                                                                                /// correlations
                                                                                const std::map<T,std::map<T, float>> &correlations)
    {
        // Creating all the empty values in the result matrix
        std::map<T, std::map<T, float>> jmap; // = varianceContravarianceMatrix;
        for (typename std::map<T,std::map<T, float>>::const_iterator itj = correlations.begin(); itj != correlations.end(); itj++)
        {
            std::map<T, float> imap;

            for (typename std::map<T, float>::const_iterator iti = itj->second.begin(); iti != itj->second.end(); iti++)
            {
                imap[iti->first] = iti->second * sd.find(iti->first)->second * sd.find(itj->first)->second; // Correl(i,j) * SD(i) * SD(j) 
            }
            jmap[itj->first] = imap;
        }
        return jmap;
    }

    /// Check the global percent value 
    inline static bool testPercent(
                                    /// vector of values to check
                                    std::vector<unsigned int> &vToTest)
    {
        unsigned int percent = 0;
        for (unsigned int i = 0; i < vToTest.size(); i++)
        {
            percent += vToTest[i];
        }
        return (percent == 100);
    }
    
    /// Control if a string is only composed by numbers
    inline static bool testIfNumericValue(
                                            /// string to check
                                            std::string &strToTest)
    {
        std::string::const_iterator it = strToTest.begin();
        while (it != strToTest.end() && std::isdigit(*it)) ++it;
        return !strToTest.empty() && it == strToTest.end();
    }

    /// Control if a string is composed by a decimal number
    inline static bool testIfDecimalValue(
                                            /// string to check
                                            std::string &strToTest)
    {
        bool res = true;
        try
        {
            float valTest = 0.0;
            valTest = atof(strToTest.c_str());
            std::string strCtrl = toString<float>(valTest);
            res = (strCtrl == strToTest);
        }
        catch (std::exception &e)
        {
            res = false;
        }
        return res;
    }

    /// Delete the spaces at the begining of transmitted string
    inline static void deleteFirstSpaces(
                                            /// string to work on
                                            std::string &theOriStr)
    {
        while (theOriStr.length()>0 && theOriStr[0] == ' ')
        {
            theOriStr.erase(0);
        }
    }
    
    /// Delete the spaces at the end of transmitted string
    inline static void deleteLastSpaces(
                                            /// string gto work on
                                            std::string &theOriStr)
    {
        while (theOriStr.length()>0 && theOriStr[theOriStr.length()-1] == ' ')
        {
            theOriStr.erase(theOriStr.length()-1);
        }
    }

    /// Change a char for another in a string
    inline static std::string changeChar(
                                            /// string to work on
                                            std::string &theStr,
                                            /// char to replace
                                            char inialChar,
                                            /// replace value
                                            char toChange)
    {
        std::string res = "";
        for (unsigned int iStr = 0; iStr < theStr.length(); iStr++)
        {
            char c = theStr[iStr];
            if (c != inialChar) res += c;
            else res += toChange;
        }
        return res;
    }

    /// Give the tab character 
    inline static char tab()
    {
        return '\t';
    }
        
    /// Separate a global string in individual strings, based on a given separator
    inline static void separateCells(
                                    /// global string to separate
                                    std::string &theReadenL,
                                    /// line to store
                                    std::vector<std::string> &cells,
                                    /// separator char
                                    char sep)
    {
        int pos = 0;
        do
        {
            std::string cellContent;
            pos = theReadenL.find(sep);
            if (pos != -1)
            {
                cellContent = theReadenL.substr(0, pos); // regular cells
                theReadenL.erase(0, cellContent.length()+1);// Delete from the readen line
            }
            else
            {
                cellContent = theReadenL; // last cell
            }
            cells.push_back(cellContent);
        }
        while(pos != -1);
    }

    inline static void getFileNamesFromDirectory(std::string &directory, std::vector<std::string> &fileList, std::string &extension)
    {  
#ifdef _WIN32
        for (auto const& p : std::filesystem::directory_iterator{directory}) 
#else
        for(auto& p: boost::filesystem::directory_iterator(directory))
#endif // _WIN32
        {
            std::string strFileName = Tools::toString(p);
            if (strFileName.find(extension) != std::string::npos) // It is a file to scan ?
            {
                boost::erase_all(strFileName, "\""); // delete the '"'
                boost::replace_all(strFileName, "\\\\", "\\"); // delete the double '\'
                boost::replace_all(strFileName, "\\\\", "\\"); // again
                fileList.push_back(strFileName);
            }
        }
    }

    /// fill the (created if not exists) file with given lines
    inline static void writeFile(
                                    /// lines to store in the file
                                    std::vector<std::string> &vLines,
                                    /// file name
                                    std::string strFileName,
                                    /// new file
                                    bool newFile)
    {
        std::ofstream file;
        if (newFile)
        {
            file.open(strFileName.c_str(), std::ios::trunc);
        }
        else
        {
            file.open(strFileName.c_str(), std::ios::app);
        }
        if (not file.is_open()) throw std::runtime_error("ERROR: can't open "+ strFileName +" file.");

        for (auto it : vLines)
        {
            file << it << std::endl; 
        }
        file.close();
    }

    /// Read the first en second line of each files mentionned
    inline static void displayFileColumnNamesAndFirstLines(std::vector<std::string> &fileNames, const char separator)
    {
        for (std::vector<std::string>::iterator it = fileNames.begin(); it != fileNames.end(); it++)
        {
            std::string &fileName = *it;
            std::vector<std::string> lines;
            std::ifstream file(fileName,  std::ios::in); 
            if(file.is_open())
            {       
                std::string line;

                 // First line is header
                getline(file, line);
                std::vector<std::string> headerValues;
                separateCells(line, headerValues, separator);

                 // Second line is first occurency
                getline(file, line);
                std::vector<std::string> firstLineValues;
                separateCells(line, firstLineValues, separator);

                // Now display header and first value
                std::cout << "File " << fileName << std::endl;
                for (unsigned int i = 0; i < headerValues.size(); i++)
                {
                    std::cout << headerValues[i] << " = " << firstLineValues[i] << std::endl;
                }
            }
            file.close();
        }
    }

    /// Read a file and push the lines (separated in fields) in a vector of strings
    inline static void readFile(
                                /// read fields of the file
                                std::vector<std::vector<std::string>> &vLines,
                                /// file name to read
                                std::string strFileName,
                                /// separator char
                                char separator,
                                /// commented lines in file
                                std::string strCommentedLine = "")
    {
        std::ifstream file(strFileName,  std::ios::in); 
        if(file.is_open())
        {       
            std::string line;
            while(getline(file, line))
            {
                if (line.size() != 0)
                {
                    if (line.size() > 0 and not (strCommentedLine.length() > 0 and line.length() >= strCommentedLine.length() and line.substr(0,strCommentedLine.length()) == strCommentedLine))
                    {
                        std::vector<std::string> cells;
                        separateCells(line, cells, separator);
                        vLines.push_back(cells);
                    }
                }
            }
            file.close();
        }
        else Console::Display::displayLineInConsole("-> Error : unable to read the file " + strFileName, Console::ANSI_RED);
    }
    
    /// Read a file having a first field as key for a data map, and each other column are string to put in a vector as value
//    inline static void readFile(
//                                std::map<std::string, std::vector<std::string>> &mData,
//                                std::string strFileName,
//                                char separator,
//                                unsigned int fieldCount,
//                                bool headerLine)
    inline static void readFile(
                                /// read fields of the file
                                std::map<std::string, std::vector<std::string>> &mData,
                                /// file name to read
                                std::string strFileName,
                                /// separator char
                                char separator,
                                /// is there a header line ?
                                bool headerLine)
    {
        std::ifstream file(strFileName,  std::ios::in); 
        if(file.is_open())
        {       
            std::string line;
            unsigned int currentReadLineNumber = 0;
            while(getline(file, line))
            {
                if (currentReadLineNumber++ != 0 or not headerLine)
                {
                    // This is a data line
                    std::vector<std::string> vCells;
                    separateCells(line, vCells, separator);
                    std::vector<std::string> vData;
                    assert(vCells.size() > 1);
                    for (unsigned int lineField = 1; lineField < vCells.size(); lineField++)
                    {
                        vData.push_back(vCells[lineField]);                                
                    }
                    mData[vCells[0]] = vData;
                }
            }
            file.close();
        }
        else Console::Display::displayLineInConsole("-> Error : unable to read the file " + strFileName, Console::ANSI_RED);
    }
    
    /// Get the executable folder
    inline static std::string getExecutableFolder()
    {
        char buffer[MAX_PATH];
	GetModuleFileNameA(NULL, buffer, MAX_PATH);
	std::string::size_type pos = std::string(buffer).find_last_of("\\/") + 1;
        std::string executableFolder = std::string(buffer).substr(0, pos);
        return executableFolder;
    }

    
    /// Fill a vector with all the distribution fileds from a file
    inline static bool importDistribution(
                                            /// file to read
                                            std::string fileToRead,
                                            /// vector to fill
                                            std::vector<double> &distributionToSet,
                                            /// separator char
                                            char separator)
    {
        std::ifstream fichierBis(fileToRead, std::fstream::in); // Open file in reading mode
        // Check if the file is open!
        if(!fichierBis)
        {
            Console::Display::displayLineInConsole("[ERROR] Unable to open the file of the file " + fileToRead + " for distribution import ...");
        }
        else
        {
            // For each line
            std::string lineBis;
            std::getline(fichierBis, lineBis);
            std::vector<std::string> values;
            Tools::separateCells(lineBis, values, separator);
            for (unsigned int i = 0; i < values.size(); i++)
            {
                distributionToSet.push_back(atof(values[i].c_str()));
            }
        }
        return distributionToSet.size() > 0;
    }
    
    /// Normal characteristics structure for the multimodal distribution
    struct NormalCharacteristics
    {
        /// Position of the peak
        int peakPos;
        /// Deviation of the peak
        double peakDeviation;
        /// Ratio of the peak
        double peakRatio; 
    };
    
    /// give a multimodal distribution
    inline static void getMultiModalDistribution(
                                                    /// peaks characteristics to integrate
                                                    std::vector<NormalCharacteristics> &peaks,
                                                    /// number of samples (x))
                                                    unsigned int sampleNumber,
                                                    /// if the fist sample must corresponds to the last
                                                    bool cycle,
                                                    /// result vector
                                                    std::vector<double> &result,
                                                    /// expected sum of all the sample values
                                                    double norm = 1.0f)
    {
        const float PIx2 = 2 * M_PI;
        result.clear();

        // Preparing the multidistributions
        std::vector<std::vector<double>> basicDistributions;
        for (unsigned int peakIndex = 0; peakIndex < peaks.size(); peakIndex++)
        {
            basicDistributions.push_back(std::vector<double>());
        }
        
        // Calculation of each distribution
        double distribSum = 0.0f;
        double peakRatioSum = 0.0f;

        for (unsigned int peakIndex = 0; peakIndex < peaks.size(); peakIndex++)
        {
            NormalCharacteristics nc = peaks[peakIndex];
            peakRatioSum += nc.peakRatio;
            std::vector<double> &basicDistribution = basicDistributions[peakIndex];
            for (unsigned int sampleIndex = 0; sampleIndex != sampleNumber; sampleIndex++)
            {
                double res = 0.0f;
                unsigned int nbCalc = 1;
                if (cycle)
                {
                    nbCalc = 3;
                }
                for (unsigned int indCalc = 0; indCalc < nbCalc; indCalc++)
                {
                    int posToWorkWith = nc.peakPos;
                    if (indCalc == 1)
                    {
                        posToWorkWith -= sampleNumber;
                    }
                    else if (indCalc == 2)
                    {
                        posToWorkWith += sampleNumber;
                    }
                    int lowLimit = posToWorkWith - nc.peakDeviation;
                    int hiLimit = posToWorkWith + nc.peakDeviation;
                    if ((int)sampleIndex >= lowLimit && (int)sampleIndex < hiLimit)
                    {
                        double angle = PIx2 * ( (float)(sampleIndex - (posToWorkWith - nc.peakDeviation)) / (2 * nc.peakDeviation) ); // en radians
                        res += (1 - cos(angle))*peaks.size()*nc.peakRatio/nc.peakDeviation;
                    }
                }
                distribSum+=res;
                basicDistribution.push_back(res);
            }   
        }
        
        // Offset
        if (peaks.size() > 0)
        {
            double offset = (distribSum *(1 - peakRatioSum)/peakRatioSum) / sampleNumber;
            // Agregation and reduction of all the peak results
            for (unsigned int sampleIndex = 0; sampleIndex < sampleNumber; sampleIndex++)
            {
                double tmpResult = 0.0f;
                for (unsigned int peakIndex = 0; peakIndex < peaks.size(); peakIndex++)
                {
                    tmpResult += basicDistributions[peakIndex][sampleIndex];  
                }
                double reduction = peaks.size()*((tmpResult + offset) * (norm))/(distribSum + (offset*sampleNumber));
                result.push_back(reduction/peaks.size());
            }
        }
        else
        {
            for (unsigned int sampleIndex = 0; sampleIndex < sampleNumber; sampleIndex++)
            {
                 result.push_back(1/(float)sampleNumber);
            }
        }
    }
    
    /// Creating the peak caracteristics for a Fourier square distribution
    inline static void getFourierSquareDistribution(
                                                        /// number of samples (x))
                                                        unsigned int sampleNumber,
                                                        /// expected precision
                                                        unsigned int dimensionNumber,
                                                        /// period number
                                                        unsigned int periodNumber,
                                                        /// result vector
                                                        std::vector<double> &result)
    {
        if (periodNumber > 0)
        {
            // calculate the peaks
            std::vector<NormalCharacteristics> peaks;
            unsigned int mainPeak = sampleNumber/4;
            unsigned int mainDeviation = mainPeak*2;
            double mainRatio = (1.28 * (double)periodNumber); //sqrt(2); // arbitrary value to have an ofset to 0, to be adjusted if needed...

            for (unsigned int dimensionNumberIndex = 0; dimensionNumberIndex < dimensionNumber; dimensionNumberIndex++)
            {
                unsigned int fact = (dimensionNumberIndex*2)+1;
                fact *=periodNumber;
                for (unsigned int dimensionIndex = 0; dimensionIndex < fact; dimensionIndex++)
                {
                    NormalCharacteristics nc;
                    nc.peakPos = round(((double)mainPeak*(((double)dimensionIndex*4) + 1))/(double)fact);
                    nc.peakDeviation = ((double)mainDeviation)/(double)fact;
                    nc.peakRatio = (double)mainRatio/(double)(fact*fact);
                    peaks.push_back(nc);
                }
            }
            
            // Calculate the result
            getMultiModalDistribution(peaks, sampleNumber, true, result);
        }
    }
    
    /// Creating normal distribution based on a param file
    inline static void getDistributionFromParamFile(
                                                        /// file to load
                                                        std::string &paramFileName,
                                                        /// result vector
                                                        std::vector<double> &result)
    {
        std::ifstream paramFile(paramFileName, std::fstream::in); // Open file in reading mode
	if (paramFile)
        {
            std::string currentLine;
            
            // read the first line for sample number
            unsigned int sampleNumber;
            char curveType;
            double dimensionNumberOrExtrapart;
            unsigned int period;
            std::getline(paramFile, currentLine);
            std::stringstream ss(currentLine);
            ss >> sampleNumber;
            ss >> curveType;
            ss >> dimensionNumberOrExtrapart;
            ss >> period;
            
            if (curveType == 'F') // fourier square
            {
                getFourierSquareDistribution(sampleNumber, ((int)dimensionNumberOrExtrapart), period, result);
            }
            else
            {
                std::vector<NormalCharacteristics> peaks;
                
                // Import the normal caracteristics
                std::getline(paramFile, currentLine);
                do
                {
                    std::stringstream ss(currentLine);
                    NormalCharacteristics nc;
                    ss >> nc.peakPos;
                    ss >> nc.peakDeviation;
                    ss >> nc.peakRatio;
                    peaks.push_back(nc),
                    std::getline(paramFile, currentLine);
                }
                while(currentLine!="");
                getMultiModalDistribution(peaks, sampleNumber, (curveType == '1'), result, dimensionNumberOrExtrapart);
            }
        }
        paramFile.close();
    }
      
    /// Calculate a woord curve
    inline static float getWoodCurve(
                                        /// a wood parameter
                                        float a,
                                        /// b wood parameter
                                        float b,
                                        /// c wood parameter
                                        float c,
                                        /// time
                                        unsigned int t)
    {
       return a * pow((float)t, b) * exp(-1 * (float)t * c);

    }
    
    /// Counting the line number and the file number in a path (for source documentation)
    inline static void countLineAndSourceFileNumber(
                                                        /// path to check
                                                        std::string path,
                                                        /// line number result
                                                        unsigned int &lineNumber,
                                                        /// file number result
                                                        unsigned int &fileNumber)
    {
        // init :
        lineNumber = 0;
        fileNumber = 0;
       
        const std::string strTmpFileName = path + "out.tmp"; // tmp res file
        std::string tmpDir = "dir /S /B " + path + " > " + strTmpFileName; // Preparing dir cmd
        system(tmpDir.c_str()); // dir cmd

        std::ifstream dirStream(strTmpFileName.c_str());
        if(dirStream.is_open())
        {
            std::string lineDir;
            while(std::getline(dirStream,lineDir)) // Looking at line wich finishes with the good extension
            {
                if (lineDir.substr(lineDir.length()-2) == ".h" or lineDir.substr(lineDir.length()-4) == ".cpp") // It is a file to scan ?
                {
                    if (lineDir.substr(lineDir.length()-2) == ".h")
                    {
                        fileNumber++;
                    }
                    
                    // line counting
                    std::ifstream fileStream(lineDir.c_str());
                    if(fileStream.is_open())
                    {
                        std::string lineFile;
                        while(std::getline(fileStream,lineFile)) // Looking at line wich finishes with the good extension
                        {
                            lineNumber++;
                        }
                        fileStream.close();
                    }
                }           
            }
            // delete the tmp file
            dirStream.close();
            std::string delTmp = "del " + strTmpFileName;
            system(delTmp.c_str()); // delete cmd
        }
    }
    
    
    /// Get a proportional factor value, based on min and max values
    inline static float getProportionalFactor(float requestValue, const std::tuple<float, float, float, float> &minAndMaxValues)
    {
        float res = 0.0f;
        
        // Limit parameters must be :
        // - std::get<0>(minAndMaxValues) : min value
        // - std::get<1>(minAndMaxValues) : min factor
        // - std::get<2>(minAndMaxValues) : max value
        // - std::get<3>(minAndMaxValues) : max factor
        
        if (requestValue < std::get<0>(minAndMaxValues))
        {
            // Min factor
            res = std::get<1>(minAndMaxValues);
        }
        else if (requestValue > std::get<2>(minAndMaxValues))
        {
            // Max factor
            res = std::get<3>(minAndMaxValues);
        }
        else
        {
            // Calculation
            res = ((std::get<3>(minAndMaxValues) - std::get<1>(minAndMaxValues))/(std::get<2>(minAndMaxValues) - std::get<0>(minAndMaxValues)))*(requestValue - std::get<0>(minAndMaxValues)) + std::get<1>(minAndMaxValues);
        }
        return res;
    }
    
    // Cartography
    // -----------
    
    // Earth hemisphere
    enum hemisphere
    {
        north = 0, 
        south = 1
    };
    
    /// Degree to radian conversion
    inline static float convertRad (
                                    /// degre to convert
                                    float degree)
    {
        return (M_PI * degree)/(float)180;
    }
    
    /// Distance calculation
    inline static float distance(
                                    /// latitude (degree) of the first point
                                    float lat_a_degree,
                                    /// longitude (degree) of the first point
                                    float lon_a_degree,
                                    /// latitude (degree) of the second point
                                    float lat_b_degree,
                                    /// longitude (degree) of the second point
                                    float lon_b_degree)
    {
        float earthRadius = 6371000.0f;
        double lat_a = convertRad(lat_a_degree);
        double lon_a = convertRad(lon_a_degree);
        double lat_b = convertRad(lat_b_degree);
        double lon_b = convertRad(lon_b_degree);
        return earthRadius * (M_PI/2 - std::asin( std::sin(lat_b) * std::sin(lat_a) + std::cos(lon_b - lon_a) * std::cos(lat_b) * std::cos(lat_a)));
    }
    
    /// Give the sunny ratio (from -1 winter to +1 summer)
    inline static float getSunnyRatio(
                                        /// date to consider
                                        const boost::gregorian::date &date,
                                        /// concerned hemisphere
                                        Tools::hemisphere hem)
    {
        float res = cos(2 * M_PI * date.day_of_year() /(boost::gregorian::gregorian_calendar::is_leap_year(date.year()) ? 366 : 365)); // radians
        if (hem == Tools::hemisphere::north) res = -res;
        
        return res;
    }
}

#endif // Tools_h
