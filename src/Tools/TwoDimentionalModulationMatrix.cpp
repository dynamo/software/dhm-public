//#include "TwoDimentionalModulationMatrix.h"
//
//// Standard
//
//BOOST_CLASS_EXPORT(Tools::TwoDimentionalModulationMatrix) // Recommended (mandatory if virtual serialization)
//
//namespace Tools
//{
//    // Macro to code statement for serialization methods 
//    #define STATEMENTS_SERIALIZE_STD_METHODS \
//        ar & BOOST_SERIALIZATION_NVP(_minX); \
//        ar & BOOST_SERIALIZATION_NVP(_maxX); \
//        ar & BOOST_SERIALIZATION_NVP(_minY); \
//        ar & BOOST_SERIALIZATION_NVP(_maxY); \
//        ar & BOOST_SERIALIZATION_NVP(_valuedAndPonderatedModulationMatrix); \
//        
//    IMPLEMENT_SERIALIZE_STD_METHODS(TwoDimentionalModulationMatrix);
//
//    TwoDimentionalModulationMatrix::TwoDimentionalModulationMatrix(const float(&fRemarquableX)[3], const unsigned int (&remarquableY)[3], const float (&remarquableValues)[3][3])
//    {
//        unsigned int remarquableX[3];
//        remarquableX[0] = fRemarquableX[0]*10;
//        remarquableX[1] = fRemarquableX[1]*10;
//        remarquableX[2] = fRemarquableX[2]*10;
//
//        for (unsigned int x = remarquableX[0];;)
//        {
//            // First : remarquable values at their place, the other values are empty (0.0f)
//            std::map<unsigned int, float> tmpYMap;
//            for (unsigned int y = remarquableY[0];;)
//            {
//                if      (x == remarquableX[0] && y == remarquableY[0]) {tmpYMap[y] = remarquableValues[0][0] * fRemarquableX[0];}
//                else if (x == remarquableX[0] && y == remarquableY[1]) {tmpYMap[y] = remarquableValues[0][1] * fRemarquableX[0];}
//                else if (x == remarquableX[0] && y == remarquableY[2]) {tmpYMap[y] = remarquableValues[0][2] * fRemarquableX[0];}
//                else if (x == remarquableX[1] && y == remarquableY[0]) {tmpYMap[y] = remarquableValues[1][0] * fRemarquableX[1];}
//                else if (x == remarquableX[1] && y == remarquableY[1]) {tmpYMap[y] = remarquableValues[1][1] * fRemarquableX[1];}
//                else if (x == remarquableX[1] && y == remarquableY[2]) {tmpYMap[y] = remarquableValues[1][2] * fRemarquableX[1];}
//                else if (x == remarquableX[2] && y == remarquableY[0]) {tmpYMap[y] = remarquableValues[2][0] * fRemarquableX[2];}
//                else if (x == remarquableX[2] && y == remarquableY[1]) {tmpYMap[y] = remarquableValues[2][1] * fRemarquableX[2];}
//                else if (x == remarquableX[2] && y == remarquableY[2]) {tmpYMap[y] = remarquableValues[2][2] * fRemarquableX[2];}
//                else {assert(false);}
//
//                if (y == remarquableY[0])
//                {
//                    _minY = y;
//                    y = remarquableY[1];
//                }
//                else if (y == remarquableY[1])
//                {
//                    y = remarquableY[2];
//                    _maxY = y;
//                }
//                else break;     
//            }
//
//            // Second : vertical remarquable x value calculation (for each y excepted remarquables)
//            for (unsigned int y = remarquableY[0]; y <= remarquableY[2]; y++)
//            {
//                unsigned int baseYMin = 0;
//                unsigned int baseYMax = 0;
//                if (y > remarquableY[0] and y < remarquableY[1])
//                {
//                    baseYMin = remarquableY[0];
//                    baseYMax = remarquableY[1];
//                }
//                else if (y > remarquableY[1] and y < remarquableY[2])
//                {
//                    baseYMin = remarquableY[1];
//                    baseYMax = remarquableY[2];
//                }
//                if (baseYMin != 0 and baseYMax != 0)
//                {
//                    float xyValue =   tmpYMap.find(baseYMin)->second
//                                    + (tmpYMap.find(baseYMax)->second - tmpYMap.find(baseYMin)->second)
//                                    * (y-baseYMin)
//                                    / (baseYMax - baseYMin);
//                    tmpYMap[y] = xyValue;
//                }
//            }
//            _valuedAndPonderatedModulationMatrix[x] = tmpYMap;
//            if (x == remarquableX[0])
//            {
//                _minX = x;
//                x = remarquableX[1];
//            }
//            else if (x == remarquableX[1])
//            {
//                x = remarquableX[2];
//                _maxX = x;
//            }
//            else break;     
//        }
//
//        // Third : vertical non remarquable Between x columns (all y)
//        for (unsigned int x = remarquableX[0]+1; x < remarquableX[2]; x++)
//        {
//            unsigned int baseXMin = 0;
//            unsigned int baseXMax = 0;
//
//            if (x < remarquableX[1])
//            {
//                baseXMin = remarquableX[0];
//                baseXMax = remarquableX[1];
//            }
//            else if (x > remarquableX[1])
//            {
//                baseXMin = remarquableX[1];
//                baseXMax = remarquableX[2];
//            }
//            if (baseXMin != 0 and baseXMax != 0)
//            {
//                std::map<unsigned int, float> &leftYMap = _valuedAndPonderatedModulationMatrix.find(baseXMin)->second;
//                std::map<unsigned int, float> currentYMap;
//                std::map<unsigned int, float> &rightYMap = _valuedAndPonderatedModulationMatrix.find(baseXMax)->second;
//
//                for (unsigned int y = remarquableY[0]; y <= remarquableY[2]; y++)
//                {
//                    float xyValue =   leftYMap.find(y)->second
//                                    + (rightYMap.find(y)->second - leftYMap.find(y)->second)
//                                    * (x-baseXMin)
//                                    / (baseXMax - baseXMin);
//                    currentYMap[y] = xyValue;
//                }
//                _valuedAndPonderatedModulationMatrix[x] = currentYMap;
//            }
//        }
//    }
//
//    float TwoDimentionalModulationMatrix::getVal(float preventionLevel, unsigned int month)
//    {
//        unsigned int iPreventionLevel = preventionLevel * 10;
//        if (iPreventionLevel < _minX) {iPreventionLevel = _minX;}
//        else if (iPreventionLevel > _maxX) {iPreventionLevel = _maxX;}
//        if (month < _minY) {month = _minY;}
//        else if (month > _maxY) {month = _maxY;}
//
//        return _valuedAndPonderatedModulationMatrix.find(iPreventionLevel)->second.find(month)->second;
//    }
//} /* End of namespace Tools */
