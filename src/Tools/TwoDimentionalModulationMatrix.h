///* 
// * File:   Random.h
// * Author: pgontier
// * 
// * Created on 25 mai 2016
// */
//#ifndef TwoDimentionalModulationMatrix_h
//#define TwoDimentionalModulationMatrix_h
//
//// standard
//
//// Boost
//
//// Project
//#include "Serialization.h"
//
//namespace Tools
//{
//    class TwoDimentionalModulationMatrix
//    {
//        // This matrix needs to know :
//        // as integer values :
//        // - the 3 remarquables x
//        // - the 3 remarquables y
//        // as float values :
//        // - the 3x3 dimension remarquable value matrix :
//        //         Y1       Y2       Y3
//        // X1  {{x1_y1,   x1_y2,   x1_y3}
//        // X2   {x2_y1,   x2_y2,   x2_y3}
//        // X3   {x3_y1,   x3_y2,   x3_y3}}
//
//        unsigned int _minX;
//        unsigned int _maxX;
//        unsigned int _minY;
//        unsigned int _maxY;
//
//        // serialization
//        DECLARE_SERIALIZE_STD_METHODS;
//
//        std::map<unsigned int, std::map<unsigned int, float>> _valuedAndPonderatedModulationMatrix;
//        
//        TwoDimentionalModulationMatrix(){} // For serialization
//
//    public:
//        TwoDimentionalModulationMatrix(const float(&fRemarquableX)[3], const unsigned int (&remarquableY)[3], const float (&remarquableValues)[3][3]);
//        float getVal(float preventionLevel, unsigned int month);
//    };
//} /* End of namespace Tools */
//#endif // TwoDimentionalModulationMatrix_h
